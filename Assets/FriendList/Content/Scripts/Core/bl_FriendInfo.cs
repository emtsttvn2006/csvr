﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class bl_FriendInfo : MonoBehaviour {
//	public Text FromText = null;
	public Text LevelText = null;
	public Text NameText = null;
	public Text StatusText = null;
//	public Image StatusImage = null;
//	public Text RatingText = null;
	public Button JoinButton = null;
	public Button RemoveButton = null;
	//public Button RemoveButton = null;
	[Space(5)]
	public Color OnlineColor = new Color(0, 0.9f, 0, 0.9f);
	public Color OffLineColor = new Color(0.9f, 0, 0, 0.9f);
	
	private string roomName = string.Empty;
	public string friendName = string.Empty;
	
	public void GetInfo(FriendInfo info)
	{
		string nameFriend ="";
		for(int i =0 ; i< CSVR_GameSetting.relationship.Count;i++)
		{
			Debug.Log(info.Name);
			Debug.Log( CSVR_GameSetting.relationship[i].CreateId);
			Debug.Log( CSVR_GameSetting.relationship[i].PartnerId);
			if(info.Name == CSVR_GameSetting.relationship[i].CreateId || info.Name == CSVR_GameSetting.relationship[i].PartnerId){
				NameText.text = CSVR_GameSetting.relationship[i].Partner.DisplayName;
				LevelText.text = CSVR_GameSetting.relationship[i].Partner.AccountLevel.ToString();
				break;
			}
//			else
//				NameText.text  = "";
		}
			
		if (StatusText != null) {
			StatusText.text = (info.IsOnline) ? "Online" : "Offline"; 
			StatusText.color = (info.IsOnline) ? OnlineColor : OffLineColor;

		}


		JoinButton.interactable = ((info.IsInRoom) ? true : false);
		roomName = info.Room;
		friendName = info.Name;
		
		
	}
	public void SetStatus(bool isOnline){
		StatusText.text = (isOnline) ? "Online" : "Offline"; 
		StatusText.color = (isOnline) ? OnlineColor : OffLineColor;
		JoinButton.interactable = ((isOnline) ? true : false);
	}
	//	Kết Bạn
	/// <summary>
	/// 
	/// </summary>
	public void JoinRoom()
	{
		if (!string.IsNullOrEmpty(roomName))
		{
			PhotonNetwork.JoinRoom(roomName);

		}
	}
	
	public void InviteRoom()
	{
		if (!string.IsNullOrEmpty(friendName))
		{
//			CSVR_UIChatManager.instance.InviteToJoinRoom(friendName);
		}
	}
	
	/// <summary>
	/// 
	/// </summary>
	public void Remove()
	{
//		PlayFabManager.instance.RemoveFriend(friendName);
//		
//		CSVR_UIFriend.instance.RemoveFriend(friendName);
	}
}