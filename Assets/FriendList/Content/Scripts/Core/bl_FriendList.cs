﻿//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections.Generic;
//
//public class bl_FriendList : MonoBehaviour {
//
//    [HideInInspector]
//    public List<string> Friends = new List<string>();
////    public GameObject m_FriendCanvas = null;
//    [Space(5)]
//    [Range(1,60)]
//    public float UpdateEvery = 15f;
//
//    public const string FriendSaveKey = "LSFriendList";
//    private char splitChar = '|';
//
//    public const string FriendListName = "LSFriendList";
//    [HideInInspector] public bool WaitForEvent = false;
//
//    /// <summary>
//    /// 
//    /// </summary>
//    void Awake()
//    {
//        this.gameObject.name = FriendListName;
//       gameObject.SetActive(false);
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    void GetFriendsStore()
//    {
//        //Get all friends saved 
//        string cacheFriend = PlayerPrefs.GetString(FriendSaveKey, "Null");
//        if (!string.IsNullOrEmpty(cacheFriend))
//        {
//            string[] splitFriends = cacheFriend.Split(splitChar);
//            Friends.AddRange(splitFriends);
//        }
//        //Find all friends names in photon list.
//        if (Friends.Count > 0)
//        {
//            PhotonNetwork.FindFriends(Friends.ToArray());
//            //Update the list UI 
//            this.GetComponent<bl_FriendListUI>().UpdateFriendList(true);
//        }
//        else
//        {
//            Debug.Log("Anyone friend store");
//            return;
//        }
//    }
//
//    /// <summary>
//    /// Call For Update List of friends.
//    /// </summary>
//    void UpdateList()
//    {
//        if (!PhotonNetwork.connected || PhotonNetwork.Friends == null)
//            return;
//        if (Friends.Count > 0)
//        {
//            if (Friends.Count > 1 && Friends.Contains("Null"))
//            {
//                Friends.Remove("Null");
//                Friends.Remove("Null");
//                SaveFriends();
//            }
//            PhotonNetwork.FindFriends(Friends.ToArray());
//        }
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    public void SaveFriends()
//    {
//        string allfriends = string.Join(splitChar.ToString(), Friends.ToArray());
//        PlayerPrefs.SetString(FriendSaveKey, allfriends);
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    /// <param name="field"></param>
//    public void AddFriend(InputField field)
//    {
//        string t = field.text;
//        if (string.IsNullOrEmpty(t))
//            return;
//
//        Friends.Add(t);
//        PhotonNetwork.FindFriends(Friends.ToArray());
//        this.GetComponent<bl_FriendListUI>().UpdateFriendList(true);
//        SaveFriends();
//        WaitForEvent = true;
//
//        field.text = string.Empty;
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    /// <param name="field"></param>
//    public void AddFriend(string friend)
//    {
//        Friends.Add(friend);
//        PhotonNetwork.FindFriends(Friends.ToArray());
//        this.GetComponent<bl_FriendListUI>().UpdateFriendList(true);
//        SaveFriends();
//        WaitForEvent = true;
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    /// <param name="friend"></param>
//    public void RemoveFriend(string friend)
//    {
//        if (Friends.Contains(friend))
//        {
//            Friends.Remove(friend);
//            SaveFriends();
//            if (Friends.Count > 0)
//            {
//                if(Friends.Count > 1 && Friends.Contains("Null"))
//                {
//                    Friends.Remove("Null");
//                    Friends.Remove("Null");
//                    SaveFriends();
//                }
//                PhotonNetwork.FindFriends(Friends.ToArray());
//            }
//            else
//            {
//                AddFriend("Null");
//                PhotonNetwork.FindFriends(Friends.ToArray());
//            }
//
//            this.GetComponent<bl_FriendListUI>().UpdateFriendList(true);
//            WaitForEvent = true;
//        }
//        else { Debug.Log("This user doesnt exist"); }
//    }
//    /// <summary>
//    /// 
//    /// </summary>
//    void OnJoinedLobby()
//    {
//        Debug.Log("Friend Start");
//        GetFriendsStore();
//        InvokeRepeating("UpdateList", 1, UpdateEvery);
//        gameObject.SetActive(true);
//    }
//}