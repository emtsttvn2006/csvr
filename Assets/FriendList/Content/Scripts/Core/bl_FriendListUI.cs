﻿//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections.Generic;
//
//public class bl_FriendListUI : MonoBehaviour
//{
//
//    public GameObject FriendUIPrefab = null;
//    public Transform PanelList = null;
//    public Text FriendsCountText = null;
//
//    private List<bl_FriendInfo> cacheFriendsInfo = new List<bl_FriendInfo>();
//    private int OnlineCount = 0;
//
//    /// <summary>
//    /// Instance UI in Friend panel for each friends
//    /// </summary>
//    /// <param name="friends"></param>
//    public void InstanceFriendList(string[] friends)
//    {
//        for (int i = 0; i < friends.Length; i++)
//        {
//            GameObject f = Instantiate(FriendUIPrefab) as GameObject;
//
//            f.transform.SetParent(PanelList, false);
//        }
//    }
//
//    /// <summary>
//    /// This is called each time that Friend list is update
//    /// // while updating a friends list, Photon will temporarily set
//    //isOnline and isInRoom to false
//    // if you update on a timer, you will notice state rapidly
//    //switching between offline and online
//    // therefore, we will store online state and room in a
//    //dictionary and wait until an update is actually received
//    // and store the updated value
//    /// </summary>
//    public void OnUpdatedFriendList()
//    {
//        if (!PhotonNetwork.connected || PhotonNetwork.Friends == null || !PhotonNetwork.insideLobby)
//        {
//            if (PhotonNetwork.Friends.Count <= 0) { CleanCacheList(); }
//            return;
//        }
//
//        if (cacheFriendsInfo.Count <= 0 && PhotonNetwork.Friends != null || this.GetComponent<bl_FriendList>().WaitForEvent)
//        {
//            UpdateFriendList(true);
//        }
//        else
//        {
//            UpdateFriendList();
//        }
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    public void UpdateFriendList(bool instance = false)
//    {
//        if (PhotonNetwork.Friends == null || !PhotonNetwork.insideLobby)
//        {
//            return;
//        }
//
//        FriendInfo[] friends = PhotonNetwork.Friends.ToArray();
//
//        if (instance)
//        {
//            CleanCacheList();
//            if (friends.Length > 0)
//            {
//                for (int i = 0; i < friends.Length; i++)
//                {
//                    if (friends[i].Name != "Null")
//                    {
//                        GameObject f = Instantiate(FriendUIPrefab) as GameObject;
//                        bl_FriendInfo info = f.GetComponent<bl_FriendInfo>();
//                        info.GetInfo(friends[i]);
//                        cacheFriendsInfo.Add(info);
//
//                        f.transform.SetParent(PanelList, false);
//                    }
//                }
//            }
//        }
//        else//Just update list
//        {
//            for (int i = 0; i < cacheFriendsInfo.Count; i++)
//            {
//                if (cacheFriendsInfo[i] != null && friends[i].Name != "Null")
//                {
//                    cacheFriendsInfo[i].GetInfo(friends[i]);
//                }
//            }
//        }
//        UpdateCount(friends);
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    void UpdateCount(FriendInfo[] friends)
//    {
//        OnlineCount = 0;
//        foreach (FriendInfo info in friends)
//        {
//            if (info.IsOnline)
//            {
//                OnlineCount++;
//            }
//        }
//        if (FriendsCountText != null)
//        {
//            FriendsCountText.text = OnlineCount + "/" + friends.Length;
//        }
//    }
//
//    /// <summary>
//    /// 
//    /// </summary>
//    void CleanCacheList()
//    {
//        if (cacheFriendsInfo.Count > 0)
//        {
//            for (int i = 0; i < cacheFriendsInfo.Count; i++)
//            {
//                Destroy(cacheFriendsInfo[i].gameObject);
//            }
//        }
//        cacheFriendsInfo.Clear();
//    }
//}