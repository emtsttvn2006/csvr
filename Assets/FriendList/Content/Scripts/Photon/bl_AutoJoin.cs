﻿using UnityEngine;
using UnityEngine.UI;

public class bl_AutoJoin : Photon.MonoBehaviour {

    public string PlayerName = "Player";
    public byte Version = 1;
    public Text PlayerNameText;
    [Space(5)]
    public GameObject PlayerNameInput;

    public void Start()
    {
        PhotonNetwork.autoJoinLobby = true;    // we join randomly. always. no need to join a lobby to get the list of rooms.
    }

    // to react to events "connected" and (expected) error "failed to join random room", we implement some methods. PhotonNetworkingMessage lists all available methods!

    public virtual void OnConnectedToMaster()
    {
        
    }

    public virtual void OnPhotonRandomJoinFailed()
    {
        
        PhotonNetwork.CreateRoom(null, new RoomOptions() { maxPlayers = 4 }, null);
    }

    public void SendPlayerName(InputField field)
    {
        string t = field.text;
        if (string.IsNullOrEmpty(t))
            return;

        PhotonNetwork.playerName = t;
		PhotonNetwork.ConnectUsingSettings ("0.1");
//        PhotonNetwork.ConnectUsingSettings(Version + "." + Application.loadedLevel);

        PlayerNameInput.SetActive(false);
    }
    // the following methods are implemented to give you some context. re-implement them as needed.

    public virtual void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
       
    }

    public void OnJoinedRoom()
    {
        
    }

    public void OnJoinedLobby()
    {
        
        PlayerNameText.text = "Your Name is: <b>" + PhotonNetwork.playerName + "</b>";
    }
}