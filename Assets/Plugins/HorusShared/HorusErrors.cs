﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Horus
{
	public enum HorusErrorCode
	{
		NeedLogin = -4,
		LoginError = -2,
		CreateAccountError = -1,
		Unknown = 1,
		Success = 0,
		ServiceUnavailable = 9999
	}

	public delegate void ErrorCallback(HorusError error);

	public class HorusError
	{
		public int HttpCode;
		public string HttpStatus;
		public HorusErrorCode Error;
		public string ErrorMessage;
//		public Dictionary<string, List<string> > ErrorDetails;
		public object CustomData;

		[ThreadStatic]
		private static StringBuilder _tempSb;
		public string GenerateErrorReport()
		{
			if (_tempSb == null)
				_tempSb = new StringBuilder();
			_tempSb.Length = 0;
//			_tempSb.Append(ErrorMessage);
//			if (ErrorDetails != null)
//				foreach (var pair in ErrorDetails)
//					foreach (var msg in pair.Value)
//						_tempSb.Append("\n").Append(pair.Key).Append(": ").Append(msg);
			return _tempSb.ToString();
		}
	}
}
