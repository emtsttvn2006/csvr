﻿using UnityEngine;
using System.Collections;

public class CSVR_RoomHostClient : Photon.MonoBehaviour {
	public static CSVR_RoomHostClient instance;
	
	
	float WaitingTimeInit = 5.0f;
	float WaitingTimeRecheck = 1.0f;
	float WaitingTimeLeft;
	int WaitingTimeMax = 3;
	int WaitingCheckCount = 0;
	
	
	// Use this for initialization
	void Awake () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
		#if DEV_DATA
		//Testing ();
		#endif
		
		if (WaitingCheckCount > 0) {
			if (WaitingTimeLeft < 0) {			
				CheckWaitedRoom ();
				WaitingCheckCount --;
				WaitingTimeLeft = WaitingTimeMax;
			} else {
				WaitingTimeLeft -= Time.deltaTime;
			}
		}
	}
	
	public void Testing() {
		
		//Tao demo room
        //if (Input.GetKeyUp("r")) {
        //    CSVR_GameSetting.roomName = "002_Thuy_Dien";
        //    CSVR_GameSetting.modeGame = "CSVR_TeamDeathmatch";
        //    ClientRequestRoom(10,CSVR_GameSetting.modeGame , CSVR_GameSetting.roomName , CSVR_GameSetting.accountLevel);
			
        //}
		
	}
	//Gui yeu cau tao room den roomhostmanager
	public void ClientRequestRoom(byte numberOfPlayers, string mode, string map, int level) {
		Debug.Log ("ClientRequestRoom");
//		CSVR_UIChatManager.instance.SendCommand (CSVR_RoomHostService.RoomhostManagerPlayfabID, "ClientRequestRoom,"+ numberOfPlayers.ToString() + "," + mode.Trim()+","+map.Trim()+","+level.ToString());
		WaitingTimeLeft = WaitingTimeInit;
		WaitingCheckCount = WaitingTimeMax;
	}
	
	
	// Check xem co room da yeu cau chua
	
	public void CheckWaitedRoom() {	
		Debug.Log ("CheckWaitedRoom");
		WaitingCheckCount = 0;

//		if (CSVR_UIChatManager.instance.CheckChannel ("E35EEF8430CC3324:" + PlayFabManager.instance.playerID) && CSVR_UIChatManager.instance.CheckChannel (PlayFabManager.instance.playerID + ":E35EEF8430CC3324")) {
//			CSVR_MPConnection.instance.JoinRoom ();
//		} else {
//			CSVR_MPConnection.instance.CreateNewRoom(10, CSVR_GameSetting.modeGame, CSVR_GameSetting.roomName, CSVR_GameSetting.accountLevel);
//		}
	}
	
	
	// Nhạaan thong bao server full
    public void OnServerFull()
    {
        //Debug.Log("Server Full");
        WaitingCheckCount = 0;
		CSVR_MPConnection.instance.CreateNewRoom(11, CSVR_GameSetting.modeName, CSVR_GameSetting.mapName, CSVR_GameSetting.accountLevel,CSVR_GameSetting.hostName);
    }
    //public void OnServerFull() {
    //    Debug.Log("Server Full");
    //    WaitingCheckCount = 0;
    //}
	/*
	public void OnReceivedRoomListUpdate(){
		Debug.Log("OnReceivedRoomListUpdate");
		foreach (RoomInfo ri in PhotonNetwork.Room) {
			Debug.Log(ri.name);
		}
	}
*/
}
