﻿using UnityEngine;
using System.Collections;

public class CSVR_RoomHostService : Photon.MonoBehaviour {

	public static CSVR_RoomHostService localInstance;
	
	string RequesterPlayID = "";

	public static string Map = "";
	public static string RoomNameToJoin = "";
	public static int NumberOfPlayers;
	public static string Mode;
	public static string Host;
	public static int Level;
	

	public static float WaitingTimeInterval = 1.0f;
	public static int WaitingCheckMax = 10;
	public static int WaitingCheckCount = 0;

	public static bool TryingRoomVIP = false;

	CSVR_RoomChat mRoomChat;
	CSVR_RoomChat RoomChat {
		get {
			if (mRoomChat == null) {
				mRoomChat = GetComponent<CSVR_RoomChat> ();
			}
			return mRoomChat;
		}

	}

	#if DEV_DATA
	public static string RoomhostManagerPlayfabID = "E35EEF8430CC3324"; //Account roomhostmanager
	//public static string RoomhostManagerPlayfabID = "8FC2BD36E5293EA0"; //Account 1roomhostmanager (Long dung)
	#else
	public static string RoomhostManagerPlayfabID = "E35EEF8430CC3324";
	#endif
	// Use this for initialization
	void Awake () {
		if (photonView.isMine) {
			localInstance = this;
		}
	}

	// Use this for initialization
	void Start () {
#if !VIRTUAL_ROOM
		if (photonView.isMine) {
			ClientRequestRoom(CSVR_GameSetting.maxPlayer, 
			                  CSVR_GameSetting.modeName,
			                  CSVR_GameSetting.mapName,
			                  CSVR_GameSetting.accountLevel,
							  CSVR_GameSetting.hostName,
							  CSVR_GameSetting.IsQuickJoinedRoom
			                  );
		}
#endif
	}

	public static void Clear() {
		Map = "";
		RoomNameToJoin = "";
		WaitingCheckCount = 0;
	}
	
	// Update is called once per frame
	void Update () {

#if DEV_DATA
		Testing ();
#endif
	}

	public void Testing() {
		/*
		//Tao demo room
		if (Input.GetKeyUp("r") && photonView.isMine &&(CSVR_UIChatManager.instance.userName == RoomhostManagerPlayfabID) ) {
			//CSVR_GameSetting.roomName = "002_Thuy_Dien";
			//CSVR_GameSetting.modeGame = "CSVR_TeamDeathmatch";
			//ClientRequestRoom(10,CSVR_GameSetting.modeGame , CSVR_GameSetting.roomName , CSVR_GameSetting.accountLevel);
			//OnClientRoomRequested("testabc", "ClientRequestRoom,10,CSVR_TeamDeathmatch,002_Thuy_Dien,1 ");
		}
		
*/
	}


	//Gui yeu cau tao room den roomhostmanager
	public void ClientRequestRoom(int numberOfPlayers, string mode, string map, int level,string host, bool isQuickJoin) {
		Clear ();
		RoomChat.SendRoomMessage ("RoomHostService,Request,"+ numberOfPlayers.ToString () + "," + mode.Trim () + "," + map.Trim () + "," + level.ToString () + "," + host.Trim () + "," + (isQuickJoin?"1":"0"));
		//CSVR_UIChatManager.instance.SendCommand (RoomhostManagerPlayfabID, "ClientRequestRoom,"+ numberOfPlayers.ToString() + "," + mode.Trim()+","+map.Trim()+","+level.ToString());
	}


	//PhotonPlayer[] availHosters = null;
	//Khi roomhostmanager nhan duoc yeu cau tao room
	public void OnClientRoomRequested(string command) {
		string[] roomPrams = command.Split (',');

		for (int i = 1; i < roomPrams.Length; i++) {
			Debug.Log(roomPrams[i] + " index " + i.ToString());
		}

		//MapToJoin = map;
		//RoomNameToUse = RequesterPlayID;
		NumberOfPlayers = int.Parse(roomPrams[2]);
		Mode = roomPrams[3];
		Map = roomPrams[4];
		Level = int.Parse(roomPrams[5]);
		Host = roomPrams[6];
		CSVR_GameSetting.IsQuickJoinedRoom = roomPrams [7].Trim () == "1" ? true : false;
		//Thong bao de tat ca leaveRoom de sang room moi
		photonView.RPC ("LeaveAndJoin",PhotonTargets.All, HorusManager.instance.playerID + System.DateTime.Now.ToString(),CSVR_GameSetting.IsQuickJoinedRoom);

		/*
		availHosters = PhotonNetwork.otherPlayers;
		
		if (availHosters != null && availHosters.Length > 0) {
			Debug.Log("hoster  " + availHosters[0].name);
		
			// Nếu còn hoster
			//Forward the command to the first available hoster
			photonView.RPC("OnManagerRequestRoom", 
			               availHosters[0],
			               fromusername,
			               int.Parse(roomPrams[1]),
			               roomPrams[2], 
			               roomPrams[3],
			               int.Parse(roomPrams[4])); 
		} else {
			// Thông báo là server full
			CSVR_UIChatManager.instance.SendCommand (fromusername, "ServerFull");
		}
*/

	}

	[PunRPC]

	public void LeaveAndJoin(string roomNameToJoin, bool isQuickJoin) {
		CSVR_GameSetting.IsQuickJoinedRoom = isQuickJoin;
		RoomNameToJoin = roomNameToJoin;
		WaitingCheckCount = WaitingCheckMax;
		PhotonNetwork.LeaveRoom ();
	}


	[PunRPC]
	public void OnManagerRequestRoom(string RequesterPlayID,int numberOfPlayers, string mode, string map, int level) {

		if (!photonView.isMine) {
			return;
		}

		Debug.Log ("OnManagerRequestRoom");
		//MapToJoin = map;
		//RoomNameToUse = RequesterPlayID;
		NumberOfPlayers = numberOfPlayers;
		Mode = mode;
		Level = level;

		PhotonNetwork.LeaveRoom ();

	}

	void  OnPhotonJoinRoomFailed() {
		if (CSVR_GameSetting.UseRoomVIP) {
			Debug.LogError("Server Full hoac Room muon join Full. Try again");
			/*foreach (RoomInfo ri in PhotonNetwork.GetRoomList()) {
				Debug.Log(ri.name);
			}*/
		}
	}

	public static string[] GetCommandLineArgs() {
		
		//return  new string[] {"roomVIP.exe", "roomhost00001", "123456" };
		return System.Environment.GetCommandLineArgs ();
	}
}
