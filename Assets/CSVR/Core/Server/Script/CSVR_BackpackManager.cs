﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Json;


#if DEV_DATA
[System.Serializable]
#endif
public class CSVR_Backpack{
	// Lưu các thông tin về các vũ khí của một user vào đây. Có thể dùng lại các class CSVR_GunInfo,...
	public CSVR_GunInfo 		Gun = new CSVR_GunInfo();
	public CSVR_GunInfo 		Pistol = new CSVR_GunInfo();
	public CSVR_MeleInfo		Melee = new CSVR_MeleInfo();
	public CSVR_GrenadeInfo[]	Grenades = new CSVR_GrenadeInfo[3];
	public CSVR_ArmorInfo[]		Armors = new CSVR_ArmorInfo[2];


}

//#if DEV_DATA
//[System.Serializable]
//#endif
public class CSVR_BackpackManager:MonoBehaviour {

	private class GetUserInventoryEquipResult{
		public List<ItemInstance> InventoryResult { get; set;}
	}

	public delegate void BackpackDoneCallback();
	public int BackpacksCount = 2;
	//public int BackpackGotCount = 0;

	public CSVR_Backpack[] Backpacks;


	BackpackDoneCallback CallbackFunction;

	private int errorCount = CSVR_GameSetting.errorCountCallBack;
	private string _playfabID;


	// Gọi hàm này để bắt đầu lấy thông tin veư backpack, lưu BackpacksvBacBackpackskpackacksBackpacksao array Backpacks

	public void GetBackpackInfo(string PlayfabID, BackpackDoneCallback newCallbackFunction) {
		_playfabID = PlayfabID;
		Backpacks = new CSVR_Backpack[BackpacksCount];
		for (int i = 0; i < BackpacksCount; i++) {
			Backpacks[i] = new CSVR_Backpack();
		}

		CallbackFunction = newCallbackFunction;

		GetUserInventoryInGameRequest request = new GetUserInventoryInGameRequest ();
		request._id = PlayfabID;
		HorusClientAPI.GetUserInventory(request, OnGetUserInventory, OnPlayFabError_Get);
	}

	private void OnGetUserInventory(GetUserInventoryInGameResult result)
	{
		errorCount = CSVR_GameSetting.errorCountCallBack;

		List<ItemInstance> inventory = result.Inventory;
		CatalogItem itemShop;

		if (inventory != null && inventory.Count > 0) 
		{
//			Debug.Log (inventory.Count);
			for (int i = 0; i < inventory.Count; i++) {
				string[] equip = inventory [i].EquipGroup.Split("."[0]);
				bool itemUpgrade = false;
				if(inventory [i].ItemId.Contains("_up")){
					itemUpgrade = true;
				}

				switch (inventory [i].ItemClass) {
				case GameConstants.VNITEMCLASS_RifleWeapon:
				case GameConstants.VNITEMCLASS_SniperWeapon:
				case GameConstants.VNITEMCLASS_ShotGunWeapon:
				case GameConstants.VNITEMCLASS_SMGWeapon:
				case GameConstants.VNITEMCLASS_MachineWeapon:
					CSVR_GunInfo _gunInfo;
					try {
						//truong customdata duoc them moi o inventory. luc truoc phai lay thong qua shop
						_gunInfo = HorusSimpleJson.DeserializeObject<CSVR_GunInfo> (inventory [i].CustomData.ToString ());
					} catch {
						//neu loi thi lay qua shop
						if (itemUpgrade) {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.upgradeItems, inventory [i].ItemId);
						} else {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopMainGun, inventory [i].ItemIdModel);
						}
						_gunInfo = HorusSimpleJson.DeserializeObject<CSVR_GunInfo> (itemShop.CustomData.ToString ());
					}
					if (inventory [i].accessories != null) {
						if (inventory [i].accessories.scope != null && inventory [i].accessories.scope.ContainsKey ("pluginModelId")) {
							_gunInfo.ScopeID = inventory [i].accessories.scope ["pluginModelId"];
						}
						if (inventory [i].accessories.magazine != null && inventory [i].accessories.magazine.ContainsKey ("pluginModelId")) {
							_gunInfo.MagazineID = inventory [i].accessories.magazine ["pluginModelId"];
						}
						if (inventory [i].accessories.grip != null && inventory [i].accessories.grip.ContainsKey ("pluginModelId")) {
							_gunInfo.GripID = inventory [i].accessories.grip ["pluginModelId"];
						}
						if (inventory [i].accessories.muzzle != null && inventory [i].accessories.muzzle.ContainsKey ("pluginModelId")) {
							_gunInfo.MuzzleID = inventory [i].accessories.muzzle ["pluginModelId"];
						}
					}
					_gunInfo.ItemID = inventory [i].ItemIdModel;
					Backpacks [int.Parse(equip[0]) - 1].Gun = _gunInfo;

					break;
				case GameConstants.VNITEMCLASS_PistolWeapon:
					CSVR_GunInfo _pistolInfo;
					try {
						//truong customdata duoc them moi o inventory. luc truoc phai lay thong qua shop
						_pistolInfo = HorusSimpleJson.DeserializeObject<CSVR_GunInfo> (inventory [i].CustomData.ToString ());
					} catch {
						//neu loi thi lay qua shop
						if (itemUpgrade) {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.upgradeItems, inventory [i].ItemId);
						} else {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopPistolGun, inventory [i].ItemIdModel);
						}
						_pistolInfo = HorusSimpleJson.DeserializeObject<CSVR_GunInfo> (itemShop.CustomData.ToString ());
					}
					if (inventory [i].accessories != null) {
						if (inventory [i].accessories.scope != null && inventory [i].accessories.scope.ContainsKey ("pluginModelId")) {
							_pistolInfo.ScopeID = inventory [i].accessories.scope ["pluginModelId"];
						}
						if (inventory [i].accessories.magazine != null && inventory [i].accessories.magazine.ContainsKey ("pluginModelId")) {
							_pistolInfo.MagazineID = inventory [i].accessories.magazine ["pluginModelId"];
						}
						if (inventory [i].accessories.grip != null && inventory [i].accessories.grip.ContainsKey ("pluginModelId")) {
							_pistolInfo.GripID = inventory [i].accessories.grip ["pluginModelId"];
						}
						if (inventory [i].accessories.muzzle != null && inventory [i].accessories.muzzle.ContainsKey ("pluginModelId")) {
							_pistolInfo.MuzzleID = inventory [i].accessories.muzzle ["pluginModelId"];
						}
					}
					_pistolInfo.ItemID = inventory [i].ItemIdModel;
					Backpacks [int.Parse(equip[0]) - 1].Pistol = _pistolInfo;

					break;
				case GameConstants.VNITEMCLASS_MeleeWeapon:
					CSVR_MeleInfo _meleeInfo;
					try{
						//truong customdata duoc them moi o inventory. luc truoc phai lay thong qua shop
						_meleeInfo = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo> (inventory [i].CustomData.ToString ());
					}
					catch{
						//neu loi thi lay qua shop
						if (itemUpgrade) {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.upgradeItems, inventory [i].ItemId);
						} else {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopMeleGun, inventory [i].ItemIdModel);
						}
						_meleeInfo = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo> (itemShop.CustomData.ToString ());
					}
						
					_meleeInfo.ItemID = inventory [i].ItemIdModel;
					Backpacks [int.Parse(equip[0]) - 1].Melee = _meleeInfo;
					break;
				case GameConstants.VNITEMCLASS_GrenadeWeapon:
					CSVR_GrenadeInfo _grenadeInfo;
					try{
						//truong customdata duoc them moi o inventory. luc truoc phai lay thong qua shop
						_grenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo> (inventory [i].CustomData.ToString ());
					}
					catch{
						//neu loi thi lay qua shop
						if (itemUpgrade) {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.upgradeItems, inventory [i].ItemId);
						} else {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopGrenadeGun, inventory [i].ItemIdModel);
						}
						_grenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo> (itemShop.CustomData.ToString ());
					}

					_grenadeInfo.ItemID = inventory [i].ItemIdModel;
					Backpacks [int.Parse(equip[0]) - 1].Grenades [0] = new CSVR_GrenadeInfo ();
					Backpacks [int.Parse(equip[0]) - 1].Grenades [0] = _grenadeInfo;
					break;
				case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
					CSVR_GrenadeInfo _oneGrenadeInfo;
					try{
						//truong customdata duoc them moi o inventory. luc truoc phai lay thong qua shop
						_oneGrenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo> (inventory [i].CustomData.ToString ());
					}
					catch{
						//neu loi thi lay qua shop
						if (itemUpgrade) {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.upgradeItems, inventory [i].ItemId);
						} else {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopGrenadeGun, inventory [i].ItemIdModel);
						}
						_oneGrenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo> (itemShop.CustomData.ToString ());
					}
						
					_oneGrenadeInfo.ItemID = inventory [i].ItemIdModel;
					Backpacks [int.Parse(equip[0]) - 1].Grenades [1] = new CSVR_GrenadeInfo ();
					Backpacks [int.Parse(equip[0]) - 1].Grenades [1] = _oneGrenadeInfo;
					break;
				case GameConstants.VNITEMCLASS_HelmetWeapon:
				case GameConstants.VNITEMCLASS_ArmorWeapon:
					CSVR_ArmorInfo _armorInfo;
					try{
						//truong customdata duoc them moi o inventory. luc truoc phai lay thong qua shop
						_armorInfo = HorusSimpleJson.DeserializeObject<CSVR_ArmorInfo> (inventory [i].CustomData.ToString ());
					}
					catch{
						//neu loi thi lay qua shop
						if (itemUpgrade) {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.upgradeItems, inventory [i].ItemId);
						} else {
							itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopArmorGun, inventory [i].ItemIdModel);
						}
						_armorInfo = HorusSimpleJson.DeserializeObject<CSVR_ArmorInfo> (itemShop.CustomData.ToString ());
					}
						
					_armorInfo.ItemID = inventory [i].ItemIdModel;
					Backpacks [int.Parse(equip[0]) - 1].Armors [0] = new CSVR_ArmorInfo ();
					Backpacks [int.Parse(equip[0]) - 1].Armors [0] = _armorInfo;
					break;
				}
			}
		}

		//cho con dao mặc đinh Level 0 khi người chơi không mang vũ khí 
		itemShop = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopMeleGun, "77_999_Knife");

		CSVR_MeleInfo _meleeInfoDefault = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo> (itemShop.CustomData.ToString ());
		_meleeInfoDefault.ItemID = "77_999_Knife";

		for (int i = 0; i < BackpacksCount; i++) {
			if(Backpacks[i].Melee.ItemID == null)
				Backpacks[i].Melee = _meleeInfoDefault;
		}

		CallbackFunction ();
	}

	private void OnPlayFabError_Get(HorusError error)
	{
		if (errorCount > 0) {
			errorCount--;
			GetBackpackInfo (_playfabID, CallbackFunction);

		} else {
			ErrorView.instance.ShowPopupError ("Lấy dữ liệu người dùng thất bại.");
		}

	}

}
