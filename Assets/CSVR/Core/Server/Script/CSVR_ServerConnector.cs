﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Internal;
using Horus.Json;
using UnityEngine.UI;



public class CSVR_ServerConnector : Photon.MonoBehaviour
{
	public static CSVR_ServerConnector instance = null;

	public vp_PlayerEventHandler m_Player;
	vp_PlayerInventory m_Inventory;
	public vp_WeaponHandler m_WeaponHandler;
	vp_MPNetworkPlayer m_MPNetworkPlayer;
	public string PlayfabID = "";
	public string PlayfabName = "";
	public string Level = "";
	public string CurrentWeapon = "";
	public int idTeams;
	public List<string>[] itemWithID; // itemWithID[0] sẽ không dùng cho đỡ nhầm lẫn
	public bool Loaded = false;
	public bool masterWeaponLoaded = true;
	public string[] strBar1, strBar2;
	bool toBroadCastPlaygfabID = false;


	public bool LoadingBackpackStarted = false;
	public bool LoadingBackpackDone = false;
	public CSVR_BackpackManager BackpackManager;
	bool StartingWeaponSet = false;
	[SerializeField] int m_CurrentBackpackIndex = -1 ;//Bắt đầu từ 0 cho backpack 1. -1 co nghĩa là chưa set backpack nào
	public int CurrentBackpackIndex {
		get {return m_CurrentBackpackIndex;}
		set {m_CurrentBackpackIndex = value;}
	} 

	[SerializeField] int m_BackpackIndexToSet = -2 ;// Đánh dấu để set backpack này khi load xong
	public int BackpackIndexToSet {
		get {return m_BackpackIndexToSet;}
		set {m_BackpackIndexToSet = value;}
	}

	bool ToSetBackpack = false;

	#region Tắt bật nút chuyển balo

	//Tắt bật nút chuyển balo
	public Vector3 m_OriginalSpawnPoint;
	public Vector3 OriginalSpawnPoint {
		set {
			m_AlreadyFire = false;
			m_AlreadyPickGun = false;
			BackpackChangeEnable = false;
			m_OriginalSpawnPoint = value;}

		get{return m_OriginalSpawnPoint;}
	}
	public bool BackpackChangeEnable = false;


	public bool m_AlreadyFire = false;
	public bool AlreadyFire {
		set {
			if (value && !m_AlreadyFire) {
				m_AlreadyFire = value;
				Transmit_BackpackChangeState(false);
			} 
		}
	}

	public bool m_AlreadyPickGun = false;
	public bool AlreadyPickGun {
		set {
			if (value && !m_AlreadyPickGun) {
				m_AlreadyPickGun = value;
				Transmit_BackpackChangeState(false);
			} 
		}
	}

	void Transmit_BackpackChangeState(bool IsEnable) {
		if (PhotonNetwork.isMasterClient) {
			photonView.RPC("Receive_BackpackChangeState", PhotonTargets.All, IsEnable);
		}
	}

	[PunRPC]
	void Receive_BackpackChangeState(bool IsEnable) {
		BackpackChangeEnable = IsEnable;
		CSVR_FPInputMobile controller = GetComponent<CSVR_FPInputMobile> ();
		if (controller != null) {
			controller.InitBackpackButton(IsEnable);
		}
	}

	[PunRPC]
	void Receive_OriginalSpawnPoint(Vector3 newOriginalPoint) {
		if (PhotonNetwork.isMasterClient) {
			OriginalSpawnPoint = newOriginalPoint;
		}
	}

	void Check_BackpackChangeState() {
		//Chi check o master
		if (LoadingBackpackDone &&  !m_AlreadyFire && !m_AlreadyPickGun && PhotonNetwork.isMasterClient && OriginalSpawnPoint != null) {
			//Debug.Log ("Check_BackpackChangeState");
			if (Vector3.Distance (transform.position, OriginalSpawnPoint) > CSVR_GameSetting.bagSwitchEnableRadius) {
				if (BackpackChangeEnable)
					Transmit_BackpackChangeState (false);
			} else {
				if (!BackpackChangeEnable)
					Transmit_BackpackChangeState (true);
			}
		}
	}
	#endregion
	#region bật tắt nút đặt bomb
//	public Vector3 m_OriginalBombSiteA;
//	public Vector3 OriginalBombSiteA {
//		set {
//			m_AlreadyFire = false;
//			m_AlreadyPickGun = false;
//			BombChangeEnable = false;
//			m_OriginalSpawnPoint = value;}
//
//		get{return m_OriginalBombSiteA;}
//	}
//	public bool BombChangeEnable = false;
//	void Check_BombChangeState() {
//		//Chi check o master
//		if (LoadingBackpackDone && !m_AlreadyFire && !m_AlreadyPickGun && PhotonNetwork.isMasterClient && OriginalSpawnPoint != null) {
//			//Debug.Log ("Check_BackpackChangeState");
//			if (Vector3.Distance (transform.position, OriginalSpawnPoint) > CSVR_GameSetting.bagSwitchEnableRadius) {
//				if (BackpackChangeEnable)
//					Transmit_BackpackChangeState (false);
//			} else {
//				if (!BackpackChangeEnable)
//					Transmit_BackpackChangeState (true);
//			}
//		}
//	}
	#endregion


	void Start()
	{
		instance = this;

		LoadingBackpackStarted = false;
		LoadingBackpackDone = false;

		//Khoi tao array cua balo
		itemWithID = new List<string>[20]; // itemWithID[0] sẽ không dùng cho đỡ nhầm lẫn
		for (int i = 0; i < itemWithID.Length; i ++) {
			itemWithID[i] = new List<string>();
		}


		strBar1 = new string[5];
		strBar2 = new string[5];
		m_Player = transform.GetComponent<vp_PlayerEventHandler>();
		m_Inventory = transform.GetComponent<vp_PlayerInventory>();
		m_Inventory.m_Misc.ResetOnRespawn = false;
		m_MPNetworkPlayer = transform.GetComponent<vp_MPNetworkPlayer>();
		m_WeaponHandler = transform.GetComponent<vp_WeaponHandler>();
		//PhotonPlayer player = PhotonNetwork.player;

		//Ham nay danh cho Master
		// LoadWeapon_Master();
		idTeams = m_MPNetworkPlayer.TeamNumber;
		//Ham nay duoc goi boi client, ko phai master
		RequestPlayfabID_Client();
		//Invoke("PlessWaitCheckID",2f);


		Receive_BackpackChangeState(false);//Ban dau tat nut chang backpack


		//Neu la local player
		if (GetComponent<vp_MPLocalPlayer>() != null) {
			SetPlayfabID_Local(HorusManager.instance.playerID,AccountManager.instance.displayName);
			CSVR_UIPlay.instance.longPressEventTrigger.Init(idTeams);
			photonView.RPC("Receive_OriginalSpawnPoint", PhotonTargets.MasterClient, transform.position);
		}
		Level = CSVR_GameSetting.accountLevel.ToString();
	}
	void PlessWaitCheckID()
	{
		PhotonPlayer[] players = PhotonNetwork.playerList;

		for (int i = 0; i < players.Length; i++)
		{
			if (string.Compare(players[i].name, PlayfabName) == 0)
			{
				if (players[i].customProperties["Level"].ToString()!=null)
					Level = players[i].customProperties["Level"].ToString();
			}
		}
	}
	void Update()
	{

		Check_BackpackChangeState ();

		//Check if weapons have to be loaded. 
		if (!LoadingBackpackStarted && PlayfabID.Trim() != "" )
		{
			LoadingBackpackStarted = true;
			//			Debug.Log("Chuan bi lay backpack");
			BackpackManager = gameObject.AddComponent<CSVR_BackpackManager>();
			BackpackManager.GetBackpackInfo(PlayfabID, LoadingBackpackDone_Callback);

		}

		if (ToSetBackpack && BackpackIndexToSet >= 0 && LoadingBackpackDone) {
			SetBackpack(BackpackIndexToSet);
		}



		//Gui PlayfabID cho tat ca cac may khac de cac may vao sau nay cung co PlayID
		if (toBroadCastPlaygfabID && PhotonNetwork.isMasterClient && (PlayfabID != ""))
		{
			m_MPNetworkPlayer.photonView.RPC("ReceivePlayfabID", PhotonTargets.Others, PlayfabID, PlayfabName);
			toBroadCastPlaygfabID = false;
		}


		if (Input.GetKeyUp("t")) {
			Debug.Log("t pressed");
			GetComponent<CSVR_ServerConnector>().DropC4 (); //Roi C4 ra neu co
		}
	
	}



	void LoadingBackpackDone_Callback() {
		//		Debug.Log (transform.name + " LoadingBackpackDone_Callback");
		LoadingBackpackDone = true;

		//try
		{
			//Neu la local player thi request de doi backpack
			if (GetComponent<vp_MPLocalPlayer> () != null) {
				m_MPNetworkPlayer.RequestSetBackpack (CSVR_GameSetting.bagStartIndex);
			} 
			//Neu la remote thi xem co weapon nao da enable truoc do thi update info
			else {
				if (CurrentBackpackIndex >= 0 && CurrentBackpackIndex < BackpackManager.Backpacks.Length) {
					UpdateInventory ();

					//Set lai vu khi sau khi co duoc thong tin cua backpack
					if (m_WeaponHandler.LastWeaponTrySet != m_WeaponHandler.CurrentWeaponIndex) {
						//						Debug.Log ("Set vu khi sau backpack " + m_WeaponHandler.LastWeaponTrySet.ToString ());

						m_Player.SetWeapon.TryStart (m_WeaponHandler.LastWeaponTrySet);
					}
				}
				/*
				vp_Weapon w = GetComponent<vp_WeaponHandler>().CurrentWeapon;
				if (w != null) {
					w.transform.GetComponent<CSVR_WeaponShooter>().SetWeaponInfo();
				}
				*/
			}
		}
		//		catch{
		//			//nhân vật vào xong thoát ra liền sẽ về đây
		//		}



	}

	public vp_UnitBankInstance m_MainGun_UnitBankInstance;

	//Ham nay ko nen gọi ở thường xuyên ở Update
	public vp_UnitBankInstance MainGun_UnitBankInstance{
		get {
			m_MainGun_UnitBankInstance = null;
			foreach (vp_UnitBankInstance ubi in m_Inventory.UnitBankInstances){
				CatalogItem item =  CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopItems, ubi.Type.name);

				if (item == null) 	continue;

				switch (item.ItemClass)
				{
				case GameConstants.VNITEMCLASS_RifleWeapon:
				case GameConstants.VNITEMCLASS_SniperWeapon:
				case GameConstants.VNITEMCLASS_ShotGunWeapon:
				case GameConstants.VNITEMCLASS_SMGWeapon:
				case GameConstants.VNITEMCLASS_MachineWeapon:
					m_MainGun_UnitBankInstance = ubi;
					break;
				}
			}
			//}
			return m_MainGun_UnitBankInstance;
		}
		set{
			//		Debug.Log("MainGun_UnitBankInstance Set");
			m_MainGun_UnitBankInstance = value;
		}
	}

	public vp_UnitType MainGun_Unit;

	public vp_UnitBankInstance m_Pistol_UnitBankInstance;
	public vp_UnitBankInstance Pistol_UnitBankInstance{
		get {
			//	if (m_Pistol_UnitBankInstance == null) {
			m_Pistol_UnitBankInstance = null;
			foreach (vp_UnitBankInstance ubi in m_Inventory.UnitBankInstances){
				CatalogItem item =  CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopItems, ubi.Type.name);
				if (item == null) 	continue;
				switch (item.ItemClass)
				{
				case GameConstants.VNITEMCLASS_PistolWeapon:
					m_Pistol_UnitBankInstance = ubi;
					break;
				}
			}
			//	}
			return m_Pistol_UnitBankInstance;
		}
		set{
			m_Pistol_UnitBankInstance = value;
		}
	}


	public vp_UnitType Pistol_Unit;

	public CSVR_GunInfo PickedGun;
	//public CSVR_GunInfo PickedPistol;

	public void SetBackpack(int backpackNumber, bool MasterToBroadcast = true)
	{

		//		Debug.Log (transform.name + " SetBackpack ");
		PickedGun = null;
		//	PickedPistol = null;

		ToSetBackpack = false;

		if (backpackNumber < 0) {
			return;

		}

		//		Debug.Log (transform.name + " SetBackpack 1");

		//Neu chua load backpack xong
		if (!LoadingBackpackDone) {
			BackpackIndexToSet = backpackNumber;
			ToSetBackpack = true;
			return;
		} 
		//		Debug.Log (transform.name + " SetBackpack 2");

		if ((backpackNumber >= BackpackManager.Backpacks.Length) && (BackpackManager.Backpacks [backpackNumber] == null)) {
			BackpackIndexToSet =  CurrentBackpackIndex ;//Cancel loading
			return;
		}

		//		Debug.Log (transform.name + " SetBackpack 3");


		BackpackIndexToSet =  backpackNumber;
		CurrentBackpackIndex = backpackNumber;

		UpdateInventory ();

		SetNewGun ();


		//m_Inventory.SaveInitialState();
		//transform.GetComponent<vp_WeaponHandler> ().StartWeapon = transform.GetComponent<vp_WeaponHandler> ().CurrentWeaponIndex;



		if (GetComponent<vp_MPLocalPlayer>() != null) {
			CSVR_GameSetting.bagCurrentIndex = (byte)CurrentBackpackIndex;
		}


		if (PhotonNetwork.isMasterClient && MasterToBroadcast) {
			//Thong bao cho cac may khac cung SetBackpack
			m_MPNetworkPlayer.RemoteSetBackpack(backpackNumber);
		}

		CSVR_FPInputMobile fpinput = GetComponent<CSVR_FPInputMobile> ();
		if  (fpinput != null) {
			fpinput.ConfigWeaponUI(backpackNumber);
		}
	}


	public void SetNewGun(){
		//Cam vu khi
		StartingWeaponSet = false;
		//Set Gun
		if (BackpackManager.Backpacks [CurrentBackpackIndex].Gun != null && BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID != null) {

			//Set main weapon

			if (!StartingWeaponSet) {
				StartingWeaponSet = true;
				m_Player.SetWeapon.NextAllowedStartTime = Time.time;
				m_Player.SetWeaponByName.Try (BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID);

			}

			if (GetComponent<vp_MPLocalPlayer> () != null) {
				CSVR_GameSetting.mainGun = BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID;
				CurrentWeapon = CSVR_GameSetting.mainGun;
			}
		} 
		//Set Pistol
		else if (BackpackManager.Backpacks [CurrentBackpackIndex].Pistol != null && BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID != null) {

			//Set pistol weapon
			if (!StartingWeaponSet) {
				StartingWeaponSet = true;
				m_Player.SetWeapon.NextAllowedStartTime = Time.time;
				m_Player.SetWeaponByName.Try (BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID);
			}
			if (GetComponent<vp_MPLocalPlayer>() != null) { 
				CSVR_GameSetting.mainGun =  BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID;
				CurrentWeapon = CSVR_GameSetting.mainGun;
			}		
		}
		//Set Knife
		else if (BackpackManager.Backpacks [CurrentBackpackIndex].Melee != null && BackpackManager.Backpacks [CurrentBackpackIndex].Melee.ItemID != null) {

			//Set main weapon
			if (!StartingWeaponSet) {
				StartingWeaponSet = true;
				m_Player.SetWeapon.NextAllowedStartTime = Time.time;
				m_Player.SetWeaponByName.Try (BackpackManager.Backpacks [CurrentBackpackIndex].Melee.ItemID);
			}
			if (GetComponent<vp_MPLocalPlayer>() != null) {
				CSVR_GameSetting.mainGun =  BackpackManager.Backpacks [CurrentBackpackIndex].Melee.ItemID;
				CurrentWeapon = CSVR_GameSetting.mainGun;
			}
		}
	}

	void UpdateInventory()  {
		m_Inventory.Clear ();




		//Phai nap truoc cac vu khi khac de ko set cac vu khi khac nua
		if (System.Convert.ToBoolean (photonView.owner.customProperties ["BombCarrier"])) {
			Debug.Log (PlayfabID + "mang bom");
			CarryExtraWeapon_C4 ();
		}

		//Set Gun
		if (BackpackManager.Backpacks [CurrentBackpackIndex].Gun != null && BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID != null) {


			//Debug.Log(BackpackManager.Backpacks [backpackNumber].Gun.ItemID);

			m_Inventory.TryGiveItem (CSVR_AssetManager.itemTypeDic [BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID], 0);
			AddWeaponTransformDynamic (BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID,
				BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ScopeID,
				BackpackManager.Backpacks [CurrentBackpackIndex].Gun.MagazineID,
				BackpackManager.Backpacks [CurrentBackpackIndex].Gun.GripID,
				BackpackManager.Backpacks [CurrentBackpackIndex].Gun.MuzzleID);

			//Thông số



			//Băng đạn G4

			string[] g4 = BackpackManager.Backpacks [CurrentBackpackIndex].Gun.G4.Split (new char[]{'/'});
			//Debug.Log("bang dan " + g4[0] );
			//Debug.Log("bang dan ngoai " + g4[1] );

			MainGun_UnitBankInstance = (vp_UnitBankInstance)m_Inventory.GetItem (CSVR_AssetManager.itemTypeDic [BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID], 0);
			MainGun_UnitBankInstance.Count = int.Parse (g4 [0]);
			//	Debug.Log ("MainGun_UnitBankInstance.Count " + MainGun_UnitBankInstance.Count.ToString() );
			//MainGun_UnitBankInstance.Count = 44;
			MainGun_UnitBankInstance.Capacity = int.Parse (g4 [0]);
			MainGun_UnitBankInstance.PlayfabGunCapacity = int.Parse (g4 [0]);
			//MainGun_UnitBankInstance.Capacity = 44;
			//	Debug.Log ("MainGun_UnitBankInstance.Capacity " + MainGun_UnitBankInstance.Capacity.ToString() );




			MainGun_Unit = (CSVR_AssetManager.itemTypeDic [BackpackManager.Backpacks [CurrentBackpackIndex].Gun.ItemID] as vp_UnitBankType).Unit;
			m_Inventory.TryGiveUnits (MainGun_Unit, int.Parse (g4 [1]));

		} else {
			MainGun_UnitBankInstance = null;
			MainGun_Unit = null;
		}

		//Set Pistol
		if (BackpackManager.Backpacks [CurrentBackpackIndex].Pistol != null && BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID != null) {
			//Debug.Log(BackpackManager.Backpacks [backpackNumber].Pistol.ItemID);

			m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID], 0);
			AddWeaponTransformDynamic (BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID);



			//Thông số
			//Sát thương  G1


			//Băng đạn G4

			string[] g4 = BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.G4.Split(new char[]{'/'});
			//Debug.Log("bang dan " + g4[0] );
			//Debug.Log("bang dan ngoai " + g4[1] );

			Pistol_UnitBankInstance = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic[BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID], 0);
			Pistol_UnitBankInstance.Count = int.Parse( g4[0] );
			Pistol_UnitBankInstance.Capacity = int.Parse(g4[0]);
			Pistol_UnitBankInstance.PlayfabGunCapacity = int.Parse (g4 [0]);

			Pistol_Unit = (CSVR_AssetManager.itemTypeDic[BackpackManager.Backpacks [CurrentBackpackIndex].Pistol.ItemID] as vp_UnitBankType).Unit;
			m_Inventory.TryGiveUnits(Pistol_Unit, int.Parse(g4[1]) );


		}else {
			Pistol_UnitBankInstance = null;
			Pistol_Unit = null;
		}

		//Set Knife
		if (BackpackManager.Backpacks [CurrentBackpackIndex].Melee != null && BackpackManager.Backpacks [CurrentBackpackIndex].Melee.ItemID != null) {
			//Debug.Log(BackpackManager.Backpacks [backpackNumber].Melee.ItemID);

			m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[BackpackManager.Backpacks [CurrentBackpackIndex].Melee.ItemID], 0);
			AddWeaponTransformDynamic (BackpackManager.Backpacks [CurrentBackpackIndex].Melee.ItemID);


		}

		//Set Grenade
		for (int g = 0; g < BackpackManager.Backpacks [CurrentBackpackIndex].Grenades.Length; g++ ) {
			if (BackpackManager.Backpacks [CurrentBackpackIndex].Grenades[g] != null && BackpackManager.Backpacks [CurrentBackpackIndex].Grenades[g].ItemID != null && BackpackManager.Backpacks [CurrentBackpackIndex].Grenades[g].ItemID != null ) {
				//				Debug.Log("Set luu dan " + BackpackManager.Backpacks [backpackNumber].Grenades[g].ItemID );

				m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[BackpackManager.Backpacks [CurrentBackpackIndex].Grenades[g].ItemID], 0);
				AddWeaponTransformDynamic (BackpackManager.Backpacks [CurrentBackpackIndex].Grenades[g].ItemID);

			}
		}



	}



	//Them weapon transform neu chua co
	void AddWeaponTransformDynamic(string weaponName,string scopeName = null,string magazineName = null,string gripName = null,string muzzleName = null) {
		//Debug.Log ("AddWeaponTransformDynamic " + weaponName);

		//Neu co weapon do trong FPSCamera roi thi thoi



		//Neu chua co thi them vu khi nay vao
		if (m_Player.IsLocal.Get ()) {
			//			Debug.Log ("AddWeaponTransformDynamic Local player");
			if (weaponName == "00_000_C4") {
				CSVR_FPInputMobile controller = GetComponent<CSVR_FPInputMobile> ();
				if (controller != null) {
					controller.HUD_OnConfigC4_Run ();
				}
			}
			Transform weaponTransform = transform.FindChild ("FPSCamera/" + weaponName + "Transform");
			if (weaponTransform != null) {
				if (weaponTransform.GetComponent<vp_Weapon>() != null && !m_WeaponHandler.Weapons.Contains(weaponTransform.GetComponent<vp_Weapon>() ) ) {
					m_WeaponHandler.Weapons.Add(weaponTransform.GetComponent<vp_Weapon>());
				}
				return;
			}

			//			Debug.Log ("AddWeaponTransformDynamic 2");
			//Transform foundWeapon = CSVR_AssetManager.instance.StandardCharacter_Local.transform.FindChild ("FPSCamera/" + weaponName);
			GameObject foundWeapon = Resources.Load ("Weapon/Local/" + weaponName) as GameObject;
			if (foundWeapon != null) {
				//				Debug.Log ("AddWeaponTransformDynamic 3");
				/*GameObject newWeapon = Instantiate (foundWeapon.gameObject);
				

				newWeapon.name = newWeapon.name.Substring (0, newWeapon.name.Length - 7);
				newWeapon.transform.SetParent (transform.FindChild ("FPSCamera"));
				newWeapon.transform.localPosition = foundWeapon.localPosition;
				newWeapon.transform.localRotation = foundWeapon.localRotation;
				
				newWeapon.SetActive (true);
				*/
				GameObject newWeapon = Instantiate (foundWeapon);

				newWeapon.name = weaponName;
				newWeapon.transform.SetParent (transform.FindChild ("FPSCamera"));
				newWeapon.transform.localPosition = Vector3.zero;
				newWeapon.transform.localRotation = Quaternion.identity;

				vp_FPWeapon fpWeapon = newWeapon.GetComponent<vp_FPWeapon> ();
				fpWeapon.Controller = GetComponent<CharacterController> ();
				// instance accessories to weapon

				if (scopeName != null) {
					fpWeapon.LoadAccessoriesByName (scopeName,1);
				}
				if (magazineName != null) {
					fpWeapon.LoadAccessoriesByName (magazineName,2);
				}
				if (gripName != null) {
					fpWeapon.LoadAccessoriesByName (gripName,3);
				}
				if (muzzleName != null) {
					fpWeapon.LoadAccessoriesByName (muzzleName,4);
				}
				//end
				newWeapon.SetActive (true);

				//Refresh lai danh sach transform cua vu khi
				m_WeaponHandler.Weapons.Add(newWeapon.GetComponent<vp_Weapon>());
				m_MPNetworkPlayer.InitShooters ();


			}
			else {
				Debug.LogError ("Khong tim thay o Resources/Local vu khi co ten " + weaponName);
			}
		} else {
			if (m_WeaponHandler.GunBone == null) {
				Debug.LogError ("GunBone o WeaponHandler cua " + transform.name + "chua set");
				return;
			}

			Transform weaponTransform = transform.FindChild ("Weapons/" + weaponName);
			if (weaponTransform != null) {
				if (weaponTransform.GetComponent<vp_Weapon>() != null && !m_WeaponHandler.Weapons.Contains(weaponTransform.GetComponent<vp_Weapon>() ) ) {
					m_WeaponHandler.Weapons.Add(weaponTransform.GetComponent<vp_Weapon>());
				}
				return;
			}

			//			Debug.Log ("AddWeaponTransformDynamic 2");
			/*
			Transform foundWeapon = CSVR_AssetManager.instance.StandardCharacter_Remote.transform.FindChild ("Weapons/" + weaponName);
			Transform foundThirdWeapon = CSVR_AssetManager.instance.StandardCharacter_Remote.transform.FindChild ("Gunbone/" + weaponName);
			if (foundWeapon != null && foundThirdWeapon != null) {
				//				Debug.Log ("AddWeaponTransformDynamic 3");
				GameObject newWeapon = Instantiate (foundWeapon.gameObject);

				//newWeapon.name = newWeapon.name.Substring (0, newWeapon.name.Length - 7);
				newWeapon.name = weaponName;
				newWeapon.transform.SetParent (transform.FindChild ("Weapons"));
				newWeapon.transform.localPosition = foundWeapon.localPosition;
				newWeapon.transform.localRotation = foundWeapon.localRotation;

			


				GameObject newThirdWeapon = Instantiate (foundThirdWeapon.gameObject);

				//newThirdWeapon.name = newWeapon.name.Substring (0, foundThirdWeapon.name.Length - 7);
				newThirdWeapon.name = weaponName;
				newThirdWeapon.transform.SetParent (m_WeaponHandler.GunBone);
				newThirdWeapon.transform.localPosition = foundThirdWeapon.localPosition;
				newThirdWeapon.transform.localRotation = foundThirdWeapon.localRotation;

				newWeapon.GetComponent<vp_Weapon> ().Weapon3rdPersonModel = newThirdWeapon;
				
				newWeapon.SetActive (true);
				newThirdWeapon.SetActive (true);

				//Refresh lai danh sach transform cua vu khi
				m_WeaponHandler.Weapons.Add(newWeapon.GetComponent<vp_Weapon>());
				m_MPNetworkPlayer.InitShooters ();
			} 

*/			

			//			Debug.Log ("AddWeaponTransformDynamic 2");
			GameObject foundWeapon = Resources.Load ("Weapon/Remote/FPS/" + weaponName) as GameObject;
			GameObject foundThirdWeapon = Resources.Load ("Weapon/Remote/Third/" + weaponName) as GameObject;
			if (foundWeapon != null && foundThirdWeapon != null) {

				GameObject newWeapon = Instantiate (foundWeapon);

				newWeapon.name = weaponName;
				newWeapon.transform.SetParent (transform.FindChild ("Weapons"));
				newWeapon.transform.localPosition = Vector3.zero;
				newWeapon.transform.localRotation = Quaternion.identity;



				GameObject newThirdWeapon = Instantiate (foundThirdWeapon);

				newThirdWeapon.name = weaponName;
				newThirdWeapon.transform.SetParent (m_WeaponHandler.GunBone);
				newThirdWeapon.transform.localPosition = Vector3.zero;
				newThirdWeapon.transform.localRotation = Quaternion.identity;


				vp_Weapon cWeapon = newWeapon.GetComponent<vp_Weapon> ();
				cWeapon.Weapon3rdPersonModel = newThirdWeapon;

				vp_WeaponAccessories acs = newThirdWeapon.GetComponent<vp_WeaponAccessories> ();

				if (acs != null) {
					//Debug.Log ("0. vp_WeaponAccessories khac null");
					// instance accessories to weapon
					if (scopeName != null) {
						//Debug.Log ("1.1 scopeName: "+scopeName);
						GameObject accessoriesScopeGO = Resources.Load ("Accessories/Remote/Third/" + scopeName) as GameObject;

						Transform accessoriesScopeTransform = acs.accessoriesScopeTransform;
						//Debug.Log ("1.2 accessoriesScopeTransform: "+accessoriesScopeTransform +" / "+accessoriesScopeGO );
						if (accessoriesScopeTransform != null && accessoriesScopeGO != null) {
							
							GameObject newAcs = Instantiate (accessoriesScopeGO);
							newAcs.transform.SetParent (accessoriesScopeTransform, false);
							//xoa thang default
							Destroy (accessoriesScopeTransform.GetChild (0).gameObject);
							newAcs.transform.localPosition = Vector3.zero;
							newAcs.transform.localRotation = Quaternion.identity;
							newAcs.SetActive (true);
						}
					}

					if (magazineName != null) {
						//Debug.Log ("2.1 magazineName: "+magazineName);
						GameObject accessoriesMagazineGO = Resources.Load ("Accessories/Remote/Third/" + magazineName) as GameObject;

						Transform accessoriesMagazineTransform = acs.accessoriesMagazineTransform;
						//Debug.Log ("2.2 accessoriesMagazineTransform: "+accessoriesMagazineTransform +" / "+accessoriesMagazineGO );
						if (accessoriesMagazineTransform != null && accessoriesMagazineGO != null) {
							
							GameObject newAcs = Instantiate (accessoriesMagazineGO);
							newAcs.transform.SetParent (accessoriesMagazineTransform, false);
							//xoa thang default
							Destroy (accessoriesMagazineTransform.GetChild (0).gameObject);
							newAcs.transform.localPosition = Vector3.zero;
							newAcs.transform.localRotation = Quaternion.identity;
							newAcs.SetActive (true);
						}
					}

					if (gripName != null) {
						//Debug.Log ("3.1 gripName: "+gripName);
						GameObject accessoriesGripGO = Resources.Load ("Accessories/Remote/Third/" + gripName) as GameObject;

						Transform accessoriesGripTransform = acs.accessoriesGripTransform;
						//Debug.Log ("3.2 accessoriesGripTransform: "+accessoriesGripTransform +" / "+accessoriesGripGO );
						if (accessoriesGripTransform != null && accessoriesGripGO != null) {
							
							GameObject newAcs = Instantiate (accessoriesGripGO);
							newAcs.transform.SetParent (accessoriesGripTransform, false);
							//xoa thang default
							Destroy (accessoriesGripTransform.GetChild (0).gameObject);
							newAcs.transform.localPosition = Vector3.zero;
							newAcs.transform.localRotation = Quaternion.identity;
							newAcs.SetActive (true);
						}
					}

					if (muzzleName != null) {
						//Debug.Log ("4.1 muzzleName: "+muzzleName);
						GameObject accessoriesMuzzleGO = Resources.Load ("Accessories/Remote/Third/" + muzzleName) as GameObject;

						Transform accessoriesMuzzleTransform = acs.accessoriesMuzzleTransform;
						//Debug.Log ("4.2 accessoriesMuzzleTransform: "+accessoriesMuzzleTransform +" / "+accessoriesMuzzleGO );
						if (accessoriesMuzzleTransform != null && accessoriesMuzzleGO != null) {
							
							GameObject newAcs = Instantiate (accessoriesMuzzleGO);
							newAcs.transform.SetParent (accessoriesMuzzleTransform, false);
							string clasItem = muzzleName.Substring(0, 4);
							if(clasItem.Contains("444_"))
								Destroy (accessoriesMuzzleTransform.GetChild (0).gameObject);
							
							newAcs.transform.localPosition = Vector3.zero;
							newAcs.transform.localRotation = Quaternion.identity;
							newAcs.SetActive (true);
						}
					}
				}


				newWeapon.SetActive (true);
				newThirdWeapon.SetActive (true);

				//Refresh lai danh sach transform cua vu khi
				m_WeaponHandler.Weapons.Add(cWeapon);
				m_MPNetworkPlayer.InitShooters ();
			} 
			else {
				Debug.LogError ("Khong tim thay o Resources/Remote vu khi co ten " + weaponName);
			}
		}
	}

	public void RestoreBackpack() {
		//Debug.Log (transform.name + " RestoreBackpack ");
		if(m_Inventory != null)
			m_Inventory.Clear ();
		SetBackpack (CurrentBackpackIndex, false);

	}

	int DroppedGunID = 0; //Dùng ID để nhặt đúng súng

	public vp_UnitBankInstance FindUnitBankInstance(string WeaponName )
	{
		vp_UnitBankInstance  foundInstance = null;
		foreach (vp_UnitBankInstance ubi in m_Inventory.UnitBankInstances){
			if (ubi.Type.name == WeaponName) {
				foundInstance = ubi;
			}
		}

		return foundInstance;
	}

	public Transform WeaponTransform;

	public void DropWeapon(string nameWeapon)
	{
		//Debug.Log ("DropWeapon " + nameWeapon);

		GameObject objSung;
		//nameWeapon = m_Player.CurrentWeaponName.Get();

		if (nameWeapon == "") return;


		//Neu la vu khi thong thuong
		{
			item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopItems, nameWeapon);



			switch (item.ItemClass)
			{
			case GameConstants.VNITEMCLASS_RifleWeapon:
			case GameConstants.VNITEMCLASS_SniperWeapon:
			case GameConstants.VNITEMCLASS_ShotGunWeapon:
			case GameConstants.VNITEMCLASS_SMGWeapon:
			case GameConstants.VNITEMCLASS_MachineWeapon:
			case GameConstants.VNITEMCLASS_PistolWeapon:
				DroppedGunID++;

				objSung = Resources.Load(nameWeapon + "/" + nameWeapon) as GameObject;
				GameObject DroppedGun = GameObject.Instantiate(objSung, new Vector3(transform.position.x, transform.position.y+0.1f, transform.position.z), transform.rotation) as GameObject;
				SetWeaponInfo wi = DroppedGun.GetComponent<SetWeaponInfo>();
				wi.Count = m_Player.CurrentWeaponAmmoCount.Get();
				wi.Capacity = m_Player.CurrentWeaponMaxAmmoCount.Get();
				wi.InternalCount = m_Player.CurrentWeaponClipCount.Get();
				wi.DroppedGunID = DroppedGunID;
				wi.ClassGun = item.ItemClass;
				wi.PlayfabID = GetComponent<CSVR_ServerConnector>().PlayfabID;

				//Tim thong tin vu khi cua local
				WeaponTransform =  transform.FindChild("FPSCamera/" + nameWeapon+"Transform/"+nameWeapon);
				if (WeaponTransform != null) {
					CSVR_FPWeaponShooter ws = WeaponTransform.GetComponent<CSVR_FPWeaponShooter>();
					wi.WeaponDamage = ws.WeaponDamage;
					wi.WeaponRange = ws.WeaponRange;

					// Thông số giật		
					wi.AimAngle_Step1.x = ws.AimAngle_Step1.x; 
					wi.AimAngle_Step1.y = ws.AimAngle_Step1.y;
					wi.AimAngleIncrease_Step1 = ws.AimAngleIncrease_Step1;
					wi.AimAngle_Step2.x = ws.AimAngle_Step2.x;
					wi.AimAngle_Step2.y = ws.AimAngle_Step2.y;
					wi.AimAngleIncrease_Step2 = ws.AimAngleIncrease_Step2;
					wi.AimAngle_Step3.x = ws.AimAngle_Step3.x ;
					wi.AimAngle_Step3.y = ws.AimAngle_Step3.y;
					wi.AimAngleIncrease_Step3 = ws.AimAngleIncrease_Step3;
					wi.AimAngleDecrease = ws.AimAngleDecrease;
					wi.BulletAngleMin = ws.BulletAngleMin;
					wi.BulletAngleMax = ws.BulletAngleMax;
					wi.BulletAngleIncrease = ws.BulletAngleIncrease;
					wi.BulletAngleDecrease = ws.BulletAngleDecrease;


					//Thông số khác
					wi.ReloadDuration =  WeaponTransform.GetComponent<vp_FPWeaponReloader>().ReloadDuration;
					wi.weightWeapon =  WeaponTransform.GetComponent<vp_FPWeaponReloader>().weightWeapon;
					wi.ProjectileFiringRate = ws.ProjectileFiringRate;
					wi.ProjectileTapFiringRate = ws.ProjectileTapFiringRate;

				} else {
					WeaponTransform =  transform.FindChild("Weapons/" + nameWeapon);
					if (WeaponTransform != null) {
						CSVR_WeaponShooter ws = WeaponTransform.GetComponent<CSVR_WeaponShooter>();
						wi.WeaponDamage = ws.WeaponDamage;
						wi.WeaponRange = ws.WeaponRange;

						// Thông số giật		
						wi.AimAngle_Step1.x = ws.AimAngle_Step1.x; 
						wi.AimAngle_Step1.y = ws.AimAngle_Step1.y;
						wi.AimAngleIncrease_Step1 = ws.AimAngleIncrease_Step1;
						wi.AimAngle_Step2.x = ws.AimAngle_Step2.x;
						wi.AimAngle_Step2.y = ws.AimAngle_Step2.y;
						wi.AimAngleIncrease_Step2 = ws.AimAngleIncrease_Step2;
						wi.AimAngle_Step3.x = ws.AimAngle_Step3.x ;
						wi.AimAngle_Step3.y = ws.AimAngle_Step3.y;
						wi.AimAngleIncrease_Step3 = ws.AimAngleIncrease_Step3;
						wi.AimAngleDecrease = ws.AimAngleDecrease;
						wi.BulletAngleMin = ws.BulletAngleMin;
						wi.BulletAngleMax = ws.BulletAngleMax;
						wi.BulletAngleIncrease = ws.BulletAngleIncrease;
						wi.BulletAngleDecrease = ws.BulletAngleDecrease;


						//Thông số khác
						wi.ReloadDuration =  WeaponTransform.GetComponent<vp_WeaponReloader>().ReloadDuration;
						//wi.weightWeapon =  WeaponTransform.GetComponentInChildren<vp_WeaponReloader>().weightWeapon; //Chua co
						wi.ProjectileFiringRate = ws.ProjectileFiringRate;
						wi.ProjectileTapFiringRate = ws.ProjectileTapFiringRate;
					}
					else {
						Debug.LogError("Không lấy được thông tin vũ khí rơi");
					}

				}

				break;
			default:
				break;
			}
		}

	}

	public void RemoveWeapon(string WeaponName) {
				Debug.Log ("RemoveWeapon " + WeaponName);
		vp_UnitBankInstance WeaponInstanceToDrop = FindUnitBankInstance (WeaponName);
		//		Debug.Log ("RemoveWeapon tim thay" + WeaponInstanceToDrop.Type.name);
		if (WeaponInstanceToDrop != null) {
			if (MainGun_UnitBankInstance != null &&
				WeaponInstanceToDrop.Type.name == MainGun_UnitBankInstance.Type.name) {
				//				Debug.Log ("Remove vu khi chinh " + MainGun_UnitBankInstance.Type.name);
				MainGun_UnitBankInstance = null;
			} else if (Pistol_UnitBankInstance != null &&
				WeaponInstanceToDrop.Type.name == Pistol_UnitBankInstance.Type.name) {
				//				Debug.Log ("Remove vu khi pistol " + Pistol_UnitBankInstance.Type.name);
				Pistol_UnitBankInstance = null;
			} else {
				//				Debug.Log ("Vu khi muon vut khong phai main hay pistol");
			}



			m_Inventory.TryRemoveUnitBank (WeaponInstanceToDrop);

			//Có thể ko cần remote đạn vì nếếu nhặt súng thì sẽ cập nhật số đạn của súng mới	
			//m_Inventory.TryRemoveUnits (WeaponInstanceToDrop.UnitType, m_Inventory.GetUnitCount (WeaponInstanceToDrop.UnitType));

		} else {
			Debug.Log ("khong tim thay vu khi de remove");
		}

	}

	public void SwapGun(string ClassGun, CSVR_GunInfo newGunInfo, int Count, int Capacity, int InternalCount) {
		//Debug.Log (transform.name + "SwapGun");
		CurrentWeapon = newGunInfo.ItemID;
		switch (ClassGun) {
		case GameConstants.VNITEMCLASS_RifleWeapon:
		case GameConstants.VNITEMCLASS_SniperWeapon:
		case GameConstants.VNITEMCLASS_ShotGunWeapon:
		case GameConstants.VNITEMCLASS_SMGWeapon:
		case GameConstants.VNITEMCLASS_MachineWeapon:

			if (MainGun_UnitBankInstance != null) {
				Debug.Log ("Vut vu khi ");
				DropWeapon(MainGun_UnitBankInstance.Type.name);
				RemoveWeapon(MainGun_UnitBankInstance.Type.name);
			}


			m_Inventory.TryGiveItem (CSVR_AssetManager.itemTypeDic [newGunInfo.ItemID], 0);
			AddWeaponTransformDynamic (newGunInfo.ItemID);

			MainGun_UnitBankInstance = 
				(vp_UnitBankInstance)m_Inventory.GetItem (
					CSVR_AssetManager.itemTypeDic [newGunInfo.ItemID], 0);
			MainGun_UnitBankInstance.Count = Count;
			MainGun_UnitBankInstance.Capacity = Capacity;
			MainGun_UnitBankInstance.PlayfabGunCapacity = Capacity;

			MainGun_Unit = (CSVR_AssetManager.itemTypeDic [newGunInfo.ItemID] as vp_UnitBankType).Unit;
			m_Inventory.TrySetUnitCount(MainGun_Unit, InternalCount);
			//m_Inventory.TryGiveUnits (MainGun_Unit, InternalCount);
			PickedGun = newGunInfo;
			m_Player.SetWeapon.NextAllowedStartTime = Time.time;
			m_Player.SetWeaponByName.Try (newGunInfo.ItemID);



			break;
		case GameConstants.VNITEMCLASS_PistolWeapon:
			if (Pistol_UnitBankInstance != null) DropWeapon(Pistol_UnitBankInstance.Type.name);


			m_Inventory.TryGiveItem (CSVR_AssetManager.itemTypeDic [newGunInfo.ItemID], 0);
			AddWeaponTransformDynamic (newGunInfo.ItemID);

			Pistol_UnitBankInstance = 
				(vp_UnitBankInstance)m_Inventory.GetItem (
					CSVR_AssetManager.itemTypeDic [newGunInfo.ItemID], 0);
			Pistol_UnitBankInstance.Count = Count;
			Pistol_UnitBankInstance.Capacity = Capacity;
			Pistol_UnitBankInstance.PlayfabGunCapacity = Capacity;

			Pistol_Unit = (CSVR_AssetManager.itemTypeDic [newGunInfo.ItemID] as vp_UnitBankType).Unit;
			m_Inventory.TrySetUnitCount(Pistol_Unit, InternalCount);
			//m_Inventory.TryGiveUnits (Pistol_Unit, InternalCount);
			//PickedPistol = newGunInfo;
			PickedGun = newGunInfo;
			m_Player.SetWeapon.NextAllowedStartTime = Time.time;
			m_Player.SetWeaponByName.Try (newGunInfo.ItemID);



			break;
		}
	}



	[PunRPC]
	void Receive_LootGun(string DroppedGunPlayfabID, int DroppedGunID) {
		Debug.Log (transform.name + " Receive_LootGun");

		SetWeaponInfo newWeaponInfo;
		GameObject[] DroppedWeapons = GameObject.FindGameObjectsWithTag ("Weapon");
		foreach (GameObject DroppedWeapon in DroppedWeapons) {
			newWeaponInfo = DroppedWeapon.GetComponent<SetWeaponInfo> ();
			if (newWeaponInfo != null && newWeaponInfo.PlayfabID == DroppedGunPlayfabID && newWeaponInfo.DroppedGunID == DroppedGunID) {

				//Set thong so
				PickedGun = new CSVR_GunInfo ();

				PickedGun.ItemID = newWeaponInfo.WeaponID;

				// Damage
				PickedGun.G1 = newWeaponInfo.WeaponDamage;


				// Thông số giật		
				PickedGun.R1X = newWeaponInfo.AimAngle_Step1.x; 
				PickedGun.R1Y = newWeaponInfo.AimAngle_Step1.y;
				PickedGun.R1 = newWeaponInfo.AimAngleIncrease_Step1;
				PickedGun.R2X = newWeaponInfo.AimAngle_Step2.x;
				PickedGun.R2Y = newWeaponInfo.AimAngle_Step2.y;
				PickedGun.R2 = newWeaponInfo.AimAngleIncrease_Step2;
				PickedGun.R3X = newWeaponInfo.AimAngle_Step3.x;
				PickedGun.R3Y = newWeaponInfo.AimAngle_Step3.y;
				PickedGun.R3 = newWeaponInfo.AimAngleIncrease_Step3;
				PickedGun.R0 = newWeaponInfo.AimAngleDecrease;
				PickedGun.A1 = newWeaponInfo.BulletAngleMin;
				PickedGun.A2 = newWeaponInfo.BulletAngleMax;
				PickedGun.A3 = newWeaponInfo.BulletAngleIncrease;
				PickedGun.A4 = newWeaponInfo.BulletAngleDecrease;


				//Thông số khác
				PickedGun.G3 = newWeaponInfo.ReloadDuration.ToString ();
				PickedGun.G2 = newWeaponInfo.ProjectileFiringRate;
				PickedGun.G4 = newWeaponInfo.Capacity.ToString () + "/" + newWeaponInfo.InternalCount.ToString ();

				SwapGun (newWeaponInfo.ClassGun, PickedGun, newWeaponInfo.Count, newWeaponInfo.Capacity, newWeaponInfo.InternalCount);

				GameObject.Destroy (DroppedWeapon);


				photonView.RPC ("FromMaster_LootGun", PhotonTargets.Others, 
					newWeaponInfo.Count,
					newWeaponInfo.Capacity,
					newWeaponInfo.InternalCount,				               
					newWeaponInfo.AimAngle_Step1.x,
					newWeaponInfo.AimAngle_Step1.y,
					newWeaponInfo.AimAngleIncrease_Step1,				               
					newWeaponInfo.AimAngle_Step2.x,
					newWeaponInfo.AimAngle_Step2.y,
					newWeaponInfo.AimAngleIncrease_Step2,				               
					newWeaponInfo.AimAngle_Step3.x,
					newWeaponInfo.AimAngle_Step3.y,
					newWeaponInfo.AimAngleIncrease_Step3,  
					newWeaponInfo.AimAngleDecrease,
					newWeaponInfo.BulletAngleMin,
					newWeaponInfo.BulletAngleMax,
					newWeaponInfo.BulletAngleIncrease,
					newWeaponInfo.BulletAngleDecrease,				               
					newWeaponInfo.WeaponDamage,
					newWeaponInfo.WeaponRange,		
					newWeaponInfo.ReloadDuration,
					newWeaponInfo.weightWeapon,
					newWeaponInfo.ProjectileFiringRate,
					newWeaponInfo.ProjectileTapFiringRate,
					newWeaponInfo.WeaponID,
					newWeaponInfo.ClassGun,
					newWeaponInfo.PlayfabID,
					newWeaponInfo.DroppedGunID
				);


				break;
			}
		}


	}

	[PunRPC]
	void Receive_LootC4() {
		GameObject[] DroppedC4s = GameObject.FindGameObjectsWithTag ("C4");
		if (DroppedC4s.Length > 0) {
			LootC4();
		}
	}

	public void LootC4() {
		Debug.Log ("LootC4");
		if (!PhotonNetwork.isMasterClient) return;

		if (photonView.owner.customProperties["Team"] != null){
			if (int.Parse(photonView.owner.customProperties["Team"].ToString()) != 1)				
				return; //Phai la cuop moi duoc cam bom
		}

		//Debug.Log ("LootC4 1");

		//Neu co nguoi dang cam bom roi thi thoi
		foreach (PhotonPlayer player in PhotonNetwork.playerList)
		{

			if (System.Convert.ToBoolean (player.customProperties ["BombCarrier"])) {
				Debug.Log (player.name + "mang bom. Khong de nguoi moi cam bom");
				return;
			} 	
		}

		ExitGames.Client.Photon.Hashtable properties1 = new ExitGames.Client.Photon.Hashtable();

		properties1.Add("BombCarrier", true);

		photonView.owner.SetCustomProperties (properties1);

	}

	[PunRPC]
	void Receive_BombSite(string bombSiteName) {
		vp_SADMaster.BombSiteName = bombSiteName;
	}

	[PunRPC]
	void Receive_DefuseBomb(bool dangGo, bool thanhCong) {
		vp_SADMaster.C4IsDefuse = dangGo;
		if (thanhCong) {
			vp_SADMaster.Team2Death = thanhCong;
			//Xoa het bom tren scene
			GameObject[] DroppedC4s = GameObject.FindGameObjectsWithTag ("C4");
			foreach (GameObject droppedC4 in DroppedC4s) {
				try{
				Destroy (droppedC4.transform.parent.gameObject);
				}catch{
					continue;
				}
			}
		}
	}

	[PunRPC]
	void FromMaster_LootGun( int Count, 
		int Capacity, 
		int InternalCount,	                        
		float AimAngle_Step1X, 
		float AimAngle_Step1Y, 
		float AimAngleIncrease_Step1,	                        
		float AimAngle_Step2X, 
		float AimAngle_Step2Y, 
		float AimAngleIncrease_Step2,	                        
		float AimAngle_Step3X, 
		float AimAngle_Step3Y, 
		float AimAngleIncrease_Step3,
		float AimAngleDecrease,
		float BulletAngleMin,
		float BulletAngleMax,
		float BulletAngleIncrease,
		float BulletAngleDecrease,	                        
		float WeaponDamage,
		float WeaponRange, 
		float ReloadDuration,
		float weightWeapon,
		float ProjectileFiringRate,
		float ProjectileTapFiringRate,
		string WeaponID,
		string ClassGun,
		string DroppedGunPlayfabID,
		int DroppedGunID) {

		Debug.Log (transform.name + " FromMaster_LootGun" + WeaponID);

		CSVR_GunInfo gi = new CSVR_GunInfo();

		gi.ItemID = WeaponID;
		gi.G1 = WeaponDamage;

		gi.R1X = AimAngle_Step1X; 
		gi.R1Y = AimAngle_Step1Y;
		gi.R1 = AimAngleIncrease_Step1;

		gi.R2X = AimAngle_Step2X; 
		gi.R2Y = AimAngle_Step2Y;
		gi.R2 = AimAngleIncrease_Step2;

		gi.R0 = AimAngleDecrease;
		gi.A1 = BulletAngleMin;
		gi.A2 = BulletAngleMax;
		gi.A3 = BulletAngleIncrease;
		gi.A4 = BulletAngleDecrease;


		//Thông số khác
		gi.G3 = ReloadDuration.ToString();
		gi.G2 = ProjectileFiringRate ;

		gi.G4 = Capacity.ToString () + "/" + InternalCount.ToString ();

		SwapGun(ClassGun, gi, Count, Capacity, InternalCount);

		SetWeaponInfo newWeaponInfo;
		GameObject[] DroppedWeapons = GameObject.FindGameObjectsWithTag ("Weapon");
		foreach (GameObject DroppedWeapon in DroppedWeapons) {
			newWeaponInfo = DroppedWeapon.GetComponent<SetWeaponInfo>();
			if (newWeaponInfo != null && newWeaponInfo.PlayfabID == DroppedGunPlayfabID && newWeaponInfo.DroppedGunID == DroppedGunID) {
				GameObject.Destroy(DroppedWeapon);
				break;
			}
		}
	}



















	public void LoadWeapon_Master()
	{
		if (PhotonNetwork.isMasterClient)
		{
			masterWeaponLoaded = false;
			Loaded = false;
		}
	}

	public void SetPlayfabID_Local(string newPlayfabID, string newPlayfabName)
	{
		PlayfabID = newPlayfabID.Trim();
		PlayfabName = newPlayfabName.Trim();
		PhotonNetwork.player.name = PlayfabName;
		//Do co truong hop la master bi dut ket noi thi may khac se lam master, cho nen PlayfabID can duoc luu ten tat ca cac nhan vat o cac may       
		m_MPNetworkPlayer.photonView.RPC("ReceivePlayfabID", PhotonTargets.Others, newPlayfabID, newPlayfabName);
	}

	[PunRPC]
	void ReceivePlayfabID(string newPlayfabID, string newPlayfabName, PhotonMessageInfo info)
	{
		PlayfabID = newPlayfabID.Trim();
		PlayfabName = newPlayfabName.Trim();
		PlessWaitCheckID ();
	}
	CatalogItem item;
	CSVR_GunInfo gunManager;
	public void InitWeapon_Master(int backpackNumber)
	{


		if (PhotonNetwork.isMasterClient )
		{
			List<string> equippedItemArray = itemWithID[backpackNumber];

			if (equippedItemArray.Count == 0) return; 

			m_Inventory.Clear (); //Xóa Inventory cũ, tức là backpack cũ
			m_MPNetworkPlayer.photonView.RPC("ReceiveClearInventory", PhotonTargets.Others); // Báo các máy khác cũng xóa


			//string[] equippedItemArray = equippedItems.Trim().Split (new char[] {','});
			foreach (string equippedItem in equippedItemArray)
			{

				//Because of Split, equippedItem maybe = ""
				if (equippedItem != "")
				{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopItems, equippedItem);

					if (item.ItemClass != GameConstants.VNITEMCLASS_MeleeWeapon 
						&& item.ItemClass != GameConstants.VNITEMCLASS_GrenadeWeapon 
						&& item.ItemClass != GameConstants.VNITEMCLASS_HelmetWeapon
						&& item.ItemClass != GameConstants.VNITEMCLASS_ArmorWeapon)
					{

						gunManager = HorusSimpleJson.DeserializeObject<CSVR_GunInfo>(item.CustomData.ToString());
						string[] strBulletInfo = gunManager.G4.Trim().Split(new char[] { '/' });
						m_Inventory.TryGiveUnits(CSVR_AssetManager.itemTypeBullet[equippedItem + "Bullet"], int.Parse(strBulletInfo[1]));//

						m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[equippedItem], 0);

						vp_UnitBankInstance AddWeapon = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic[equippedItem], 0);
						AddWeapon.Count = int.Parse(strBulletInfo[0]);
						AddWeapon.Capacity = int.Parse(strBulletInfo[0]);
						TransmitInventoryInfo(m_MPNetworkPlayer.ID, equippedItem, int.Parse(strBulletInfo[1]), int.Parse(strBulletInfo[0]), int.Parse(strBulletInfo[0]));
						//if (transform.root.name.Contains("2")) {
						//	Debug.Break();
						//}
					}
					else if (item.ItemClass != GameConstants.VNITEMCLASS_MeleeWeapon 
						&& item.ItemClass != GameConstants.VNITEMCLASS_HelmetWeapon
						&& item.ItemClass != GameConstants.VNITEMCLASS_ArmorWeapon)
					{

						m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[equippedItem], 0);
						m_Inventory.TryGiveUnits(CSVR_AssetManager.itemTypeBullet[equippedItem + "Bullet"], 0);
						TransmitInventoryGet(m_MPNetworkPlayer.ID, equippedItem, 0);
					}
					else if (item.ItemClass == GameConstants.VNITEMCLASS_MeleeWeapon)
					{

						m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[equippedItem], 0);
						TransmitInventoryKnife(m_MPNetworkPlayer.ID, equippedItem, 0);
					}
				}
			}

			for (int i = 0; i < strBar1.Length; i++)
			{
				TransmitListWeapon(m_MPNetworkPlayer.ID, strBar1[i], i);
				TransmitListWeapon2(m_MPNetworkPlayer.ID, strBar2[i], i);
			}
			//			Debug.Log
			Loaded = true;
			TransmitLoadWeapon(m_MPNetworkPlayer.ID, Loaded);

			//m_Inventory.SaveInitialState();
		}

	}

	public void Transmit_InitWeapon_Master(int backpackNumber) {
		m_MPNetworkPlayer.photonView.RPC("Receive_InitWeapon_Master", PhotonTargets.MasterClient, backpackNumber);
	}

	[PunRPC] void Receive_InitWeapon_Master(int backpackNumber) {
		InitWeapon_Master(backpackNumber);
	}

	[PunRPC] void ReceiveClearInventory() {
		m_Inventory.Clear (); //Xóa Inventory cũ, tức là backpack cũ
	}



	public void TransmitLoadWeapon(int playerID, bool check)
	{
		// m_MPNetworkPlayer.photonView.RPC("ReceiveCheckLoad", PhotonTargets.Others, playerID, Loaded);

	}
	/*
    [PunRPC]

    void ReceiveCheckLoad(int PlayerID, bool checkLoadWeapon, PhotonMessageInfo info)
    {
        m_MPNetworkPlayer.Transform.GetComponent<CSVR_ServerConnector>().Loaded = checkLoadWeapon;
        if (GetComponent<CSVR_FPInputMobile>() != null)
        {
            GetComponent<CSVR_FPInputMobile>().ConfigWeaponUI();
            GetComponent<CSVR_FPInputMobile>().checkBL = true;
        }
    }
    */
	public void TransmitListWeapon(int playerID, string strWeaponBar1, int index)
	{
		m_MPNetworkPlayer.photonView.RPC("ReceiveListWeapon", PhotonTargets.Others, playerID, strWeaponBar1, index);
	}
	[PunRPC]
	void ReceiveListWeapon(int PlayerID, string str1, int index, PhotonMessageInfo info)
	{
		m_MPNetworkPlayer.Transform.GetComponent<CSVR_ServerConnector>().strBar1[index] = str1;
	}
	public void TransmitListWeapon2(int playerID, string strWeaponBar2, int index)
	{
		m_MPNetworkPlayer.photonView.RPC("ReceiveListWeapon2", PhotonTargets.Others, playerID, strWeaponBar2, index);
	}
	[PunRPC]
	void ReceiveListWeapon2(int PlayerID, string str2, int index, PhotonMessageInfo info)
	{

		m_MPNetworkPlayer.Transform.GetComponent<CSVR_ServerConnector>().strBar2[index] = str2;
	}
	public void GetWeapon(string WeaponName, int Amount, int MaxAmount, string BulletName, string WeaponNameRemove, string BulletRemove, int MaxAmountRemove)
	{
		//vp_UnitBankInstance AddWeapon = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic[WeaponName], 0);
		/*
        if (AddWeapon != null)
        {
            AddWeapon.Count = Amount;
            TransmitInventoryBullet(m_MPNetworkPlayer.ID, WeaponName, Amount);
        }
        else
        {*/
		if (WeaponNameRemove != null)
		{
			m_Inventory.TryRemoveUnits(CSVR_AssetManager.itemTypeBullet[WeaponNameRemove + "Bullet"], MaxAmountRemove);
			m_Inventory.TryRemoveItem(CSVR_AssetManager.itemTypeDic[WeaponNameRemove], 0);
			RemoveInventoryInfo(m_MPNetworkPlayer.ID, WeaponNameRemove, MaxAmountRemove);
		}
		m_Inventory.TryGiveUnits(CSVR_AssetManager.itemTypeBullet[WeaponName + "Bullet"], MaxAmount);
		m_Inventory.TryGiveItem(CSVR_AssetManager.itemTypeDic[WeaponName], 0);
		vp_UnitBankInstance AddWeaponNew = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic[WeaponName], 0);
		AddWeaponNew.Count = Amount;
		TransmitInventoryGet(m_MPNetworkPlayer.ID, WeaponName, MaxAmount);

		//}
		transform.GetComponent<PhotonView>().RPC("DestroyWeapon", PhotonTargets.All, WeaponName + "(Clone)");
	}
	/*
    public void RemoveWeapon(string nameWeapon, int _amount, int _maxAmount)
    {
        GameObject objWeapon;
        objWeapon = Resources.Load(nameWeapon + "/" + nameWeapon) as GameObject;
        GameObject initWeapon = GameObject.Instantiate(objWeapon, new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z + 3), transform.rotation) as GameObject;
        initWeapon.GetComponent<SetWeaponInfo>().Amount = _amount;
        initWeapon.GetComponent<SetWeaponInfo>().MaxAmount = _maxAmount;
        TransmitInventoryRemove(m_MPNetworkPlayer.ID, nameWeapon, _amount, _maxAmount);
    }

    public void RemoveWeaponRemote(string nameWeapon, int _amount, int _maxAmount)
    {
        GameObject objWeapon;
        objWeapon = Resources.Load(nameWeapon + "/" + nameWeapon) as GameObject;
        GameObject initWeapon = GameObject.Instantiate(objWeapon, new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z + 3), transform.rotation) as GameObject;
        initWeapon.GetComponent<SetWeaponInfo>().Amount = _amount;
        initWeapon.GetComponent<SetWeaponInfo>().MaxAmount = _maxAmount;
    }
    void TransmitInventoryRemove(int id, string nameWeapon, int _amount, int _maxAmount)
    {
        m_MPNetworkPlayer.photonView.RPC("ReceiveInventoryRemove", PhotonTargets.Others,id, nameWeapon, _amount, _maxAmount);
    }
    [PunRPC]
    void ReceiveInventoryRemove(int playerID,string nameWeapon, int _amount, int _maxAmount, PhotonMessageInfo info)
    {
        m_MPNetworkPlayer.Transform.GetComponent<CSVR_ServerConnector>().RemoveWeaponRemote(nameWeapon, _amount, _maxAmount);
    }
    */
	//Destroy flag mode FlagCapture
	public void TransmitDestroyFlag(string strFlagName)
	{
		m_MPNetworkPlayer.photonView.RPC("DestroyFlag", PhotonTargets.All, strFlagName);
	}
	[PunRPC]
	void DestroyFlag(string obj, PhotonMessageInfo info)
	{
		Destroy(GameObject.Find(obj));
	}


	//------------------end-----------------
	[PunRPC]
	void DestroyWeapon(string obj, PhotonMessageInfo info)
	{
		Destroy(GameObject.Find(obj));
		//m_Player.SetWeaponByName.Try(obj);
		if (GetComponent<CSVR_FPGetGun>() != null)
		{
			CSVR_FPGetGun.getgun.tbnNhatSung.gameObject.SetActive(false);
		}
	}
	public void TransmitInventoryBullet(int playerID, string equippedItem, int maxAmount)
	{
		if (equippedItem != "")
		{
			//				Debug.Log("Adding " + equippedItem + "into player's inventory");
			m_MPNetworkPlayer.photonView.RPC("ReceiveInventoryBullet", PhotonTargets.Others, equippedItem, maxAmount, playerID);
		}


		//	
	}
	/// <summary>
	/// </summary>
	public void TransmitInventoryInfo(int playerID, string equippedItem, int _maxAmount, int _amount, int _amountCount)
	{
		int itemAmount = 0;

		if (equippedItem != "")
		{
			//				Debug.Log("Adding " + equippedItem + "into player's inventory");
			m_MPNetworkPlayer.photonView.RPC("ReceiveInventoryInfo", PhotonTargets.Others, equippedItem, itemAmount, playerID, _maxAmount, _amount, _amountCount);
		}
	}
	public void TransmitInventoryGet(int playerID, string equippedItem, int _maxAmount)
	{
		int itemAmount = 0;

		if (equippedItem != "")
		{
			//				Debug.Log("Adding " + equippedItem + "into player's inventory");
			m_MPNetworkPlayer.photonView.RPC("ReceiveInventoryGet", PhotonTargets.Others, equippedItem, itemAmount, playerID, _maxAmount);
		}
	}
	[PunRPC]
	void ReceiveInventoryBullet(string itemTypeID, int itemAmount, int playerID, PhotonMessageInfo info)
	{
		vp_UnitBankInstance AddWeapon = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic[itemTypeID], 0);
		AddWeapon.Count = itemAmount;
	}
	[PunRPC]
	void ReceiveInventoryInfo(string itemTypeID, int itemAmount, int playerID, int MaxAmount, int Amount, int AmountCount, PhotonMessageInfo info)
	{

		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryGiveUnits(CSVR_AssetManager.itemTypeBullet[itemTypeID + "Bullet"], MaxAmount);
		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryGiveItem(CSVR_AssetManager.itemTypeDic[itemTypeID], itemAmount);
		vp_UnitBankInstance AddWeapon = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic[itemTypeID], 0);
		AddWeapon.Count = Amount;
		AddWeapon.Capacity = AmountCount;
		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().SaveInitialState();
	}
	[PunRPC]
	void ReceiveInventoryGet(string itemTypeID, int itemAmount, int playerID, int MaxAmount, PhotonMessageInfo info)
	{
		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryGiveItem(CSVR_AssetManager.itemTypeDic[itemTypeID], itemAmount);
		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryGiveUnits(CSVR_AssetManager.itemTypeBullet[itemTypeID + "Bullet"], MaxAmount);

	}

	public void TransmitInventoryKnife(int playerID, string equippedItem, int _maxAmount)
	{
		int itemAmount = 0;

		if (equippedItem != "")
		{
			//				Debug.Log("Adding " + equippedItem + "into player's inventory");
			m_MPNetworkPlayer.photonView.RPC("ReceiveInventoryKnife", PhotonTargets.Others, equippedItem, itemAmount, playerID, _maxAmount);
		}
	}
	[PunRPC]
	void ReceiveInventoryKnife(string itemTypeID, int itemAmount, int playerID, int MaxAmount, PhotonMessageInfo info)
	{
		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryGiveItem(CSVR_AssetManager.itemTypeDic[itemTypeID], itemAmount);
	}
	//hàm gỡ bỏ vũ khí khỏi Inventory(Viên)
	public void RemoveInventoryInfo(int playerID, string equippedItem, int _maxAmount)
	{
		int itemAmount = 0;

		if (equippedItem != "")
		{
			//				Debug.Log("Adding " + equippedItem + "into player's inventory");
			m_MPNetworkPlayer.photonView.RPC("RemoveInventory", PhotonTargets.Others, equippedItem, itemAmount, playerID, _maxAmount);
		}


		//	
	}

	[PunRPC]
	void RemoveInventory(string itemTypeID, int itemAmount, int playerID, int MaxAmount, PhotonMessageInfo info)
	{

		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryRemoveItem(CSVR_AssetManager.itemTypeDic[itemTypeID], itemAmount);
		m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().TryRemoveUnits(CSVR_AssetManager.itemTypeBullet[itemTypeID + "Bullet"], MaxAmount);
		//m_MPNetworkPlayer.Transform.GetComponent<vp_Inventory>().T(CSVR_AssetManager.itemTypeDic[itemTypeID], itemAmount);

		//player.Transform.GetComponent<vp_Inventory> ().SaveInitialState ();


	}
	//End
	public void TransmitAmorInfo(int playerID, float _maxAmor)
	{
		m_MPNetworkPlayer.photonView.RPC("TranmitAmor", PhotonTargets.Others, playerID, _maxAmor);
	}
	[PunRPC]
	void TranmitAmor(int PlayerID, float maxAmor, PhotonMessageInfo info)
	{
		m_MPNetworkPlayer.Transform.GetComponent<vp_DamageHandler>().MaxAmor = maxAmor;
		m_MPNetworkPlayer.Transform.GetComponent<vp_DamageHandler>().CurrentAmor = maxAmor;
	}
	//Ham nay duoc goi boi client, ko phai master
	public void RequestPlayfabID_Client()
	{

		if (!PhotonNetwork.isMasterClient)
		{
			m_MPNetworkPlayer.photonView.RPC("BroadcastPlayfabID_Master", PhotonTargets.MasterClient);
		}

	}

	[PunRPC]
	void BroadcastPlayfabID_Master(PhotonMessageInfo info)
	{
		toBroadCastPlaygfabID = true;
	}
	/* 
	void TestMasterSwitch() {
		if (Input.GetKeyUp ("m") && (m_MPNetworkPlayer is vp_MPLocalPlayer)) {
			bool result = PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}


	}
*/
	public void DropC4() {		
		//Debug.Log ("DropC4");
		//Neu khong phai dang cam bom thi thoi

		if (!System.Convert.ToBoolean (photonView.owner.customProperties ["BombCarrier"])) {
			Debug.Log ("Toi dang ko cam bom nen khong the drop");
			return;
		} 	

		if (photonView.isMine) {
			
			ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable ();
			properties.Add ("BombCarrier", false);
			photonView.owner.SetCustomProperties (properties);

			SetNewGun ();

		}

	}

	void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps) {

		if (CSVR_GameSetting.modeName == "CSVR_SearchAndDestroy") {

			//Neu khong phai player nay co thay doi thi thoi
			if ((playerAndUpdatedProps [0] as PhotonPlayer).ID != photonView.owner.ID)
				return;


			if (System.Convert.ToBoolean (photonView.owner.customProperties ["BombCarrier"])) {
				Debug.Log (PlayfabID + "mang bom");
				CarryExtraWeapon_C4 ();

				//Xoa het bom tren scene
				GameObject[] DroppedC4s = GameObject.FindGameObjectsWithTag ("C4");
				foreach (GameObject droppedC4 in DroppedC4s) {
					Destroy (droppedC4);
				}

			} else {
				Debug.Log (PlayfabID + "khong mang bom");
				if (m_Inventory != null && CheckHaveBombC4()) {
					Debug.Log (PlayfabID + "khong mang bom 1");
					GameObject objSung = Resources.Load ("00_000_C4/00_000_C4") as GameObject;
					GameObject DroppedGun = GameObject.Instantiate (objSung, new Vector3 (transform.position.x, transform.position.y + 0.1f, transform.position.z), transform.rotation) as GameObject;

					RemoveWeapon ("00_000_C4");
				}
			}
		}
	}

	public bool CheckHaveGrenade(){
		for (int i = 0; i < 3; i++) {
			if (BackpackManager.Backpacks [CurrentBackpackIndex].Grenades [i] != null
			    && BackpackManager.Backpacks [CurrentBackpackIndex].Grenades [i].ItemID != null
			    && BackpackManager.Backpacks [CurrentBackpackIndex].Grenades [i].ItemID != string.Empty
			) {
				vp_UnitBankInstance AddWeapon = (vp_UnitBankInstance)m_Inventory.GetItem(CSVR_AssetManager.itemTypeDic [BackpackManager.Backpacks [CurrentBackpackIndex].Grenades [i].ItemID], 0);
//				Debug.Log ("<b>0. CheckHaveGrenade Count </b>" + AddWeapon.Count);
//				Debug.Log ("<b>0. CheckHaveGrenade Capacity </b>" + AddWeapon.Capacity);
				//Debug.Log ("<b>0. CheckHaveGrenade Capacity </b>" + AddWeapon.);
				if (AddWeapon.Count == 0 && AddWeapon.Capacity == 0)
					return false;
			}
		}
		return true;

	}
	public bool CheckHaveBombC4(){
		//Debug.Log ("<b>0. CheckHaveBombC4 </b>" + m_Inventory.HaveItem (CSVR_AssetManager.itemTypeDic ["00_000_C4"]));
		return m_Inventory.HaveItem (CSVR_AssetManager.itemTypeDic ["00_000_C4"]);

	}

	void CarryExtraWeapon_C4() {
		//Debug.Log (transform.name + "CarryExtraWeapon_C4");
		CarryExtraWeapon ("00_000_C4");
	}

	void CarryExtraWeapon(string extraItemID) {
		//Debug.Log (transform.name + "CarryExtraWeapon");
		if (m_Inventory == null)
			return;
		//Debug.Log (transform.name + "CarryExtraWeapon 1");
		if (m_Player.SetWeapon.Active) {
			Invoke ("CarryExtraWeapon_C4",0.3f);
			return;
		};
		if (!m_Inventory.HaveItem (CSVR_AssetManager.itemTypeDic [extraItemID])) {
			m_Inventory.TryGiveItem (CSVR_AssetManager.itemTypeDic [extraItemID], 0);
			AddWeaponTransformDynamic (extraItemID);
		}
		//4s4kur4y0
//		StartingWeaponSet = true;
//		//Set main weapon
//		m_Player.SetWeapon.NextAllowedStartTime = Time.time;
//		m_Player.SetWeaponByName.Try(extraItemID);
//
//		CurrentWeapon = extraItemID;
//
//
	}


	void OnMasterClientSwitched() {
		//Cap nhat lai ten
		#if UNITY_EDITOR
		// update gameobject name in editor hierarchy view with
		// local/remote and master/client status
		gameObject.name =  m_MPNetworkPlayer.ID.ToString()
			+ ((m_MPNetworkPlayer.ID == PhotonNetwork.player.ID) ? " (LOCAL)" : "")
			+ (((photonView.owner.isMasterClient) ? "(MASTER)" : ""))
			;
		gameObject.name = gameObject.name.Replace(")(", ", ");
		#endif
	}

	/*
	void OnApplicationPause(bool pause){
		#if !VIRTUAL_ROOM
		if (pause) {
			//Debug.Log ("Application pause");
			PlayFabManager.instance.OnReportPlayerQuitGame ();
			//	#if !UNITY_EDITOR
			if (PhotonNetwork.isMasterClient) {
				PhotonPlayer chosenNewMaster = CSVR_MPConnection.instance.GetNewMasterCandidate ();
				if (chosenNewMaster != null && chosenNewMaster.ID != PhotonNetwork.masterClient.ID) {
					PhotonNetwork.SetMasterClient (chosenNewMaster);
				}
			}
			//Tam thoi thoat room
			PhotonNetwork.LeaveRoom ();			
			PlayFabManager.instance.gameModeInstance.transform.FindChild (CSVR_GameSetting.modeGame).gameObject.SetActive (false);
			HomeScreen.states = States.Lobby;
			Application.LoadLevel ("GameOver");
			
			//De send luon cac command
			PhotonNetwork.networkingPeer.SendOutgoingCommands ();
		} 
        //else {
        //    Debug.Log ("Application pause not true");
        //}
#endif

	}
    */
}
