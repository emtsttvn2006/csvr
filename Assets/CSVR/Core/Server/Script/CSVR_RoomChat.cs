﻿using UnityEngine;
using System.Collections;

public class CSVR_RoomChat :  Photon.MonoBehaviour {

	//toPlayer == null thi gui den Others

	public void SendRoomMessage(string message, PhotonPlayer toPlayer = null) {
		if (photonView.isMine) {
			if (PhotonNetwork.inRoom) {
				if (toPlayer != null) {
					photonView.RPC("OnReceiveRoomMessage", toPlayer, message);
				} else {
					photonView.RPC("OnReceiveRoomMessage", PhotonTargets.MasterClient, PhotonNetwork.player.name, message);
				}
			}
		}
	}

	[PunRPC]
	public void OnReceiveRoomMessage(string fromPlayer, string message) {
	
			Debug.Log("OnReceiveMessage " + message + " from " + fromPlayer);
			if (message.Contains("RoomHostService")) {
				GetComponent<CSVR_RoomHostService>().OnClientRoomRequested(message);
			}
	
	}
}
