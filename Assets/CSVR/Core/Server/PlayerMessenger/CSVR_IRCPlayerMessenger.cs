﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using yIRC;
using Horus.Internal;
using UnityEngine.SceneManagement;
using LitJson;

public class CSVR_IRCPlayerMessenger : CSVR_PlayerMessenger, IrcClient.IListConsumer<ChannelInfo>
{
    public IrcClient irc = null;

    public override event Delegate_OnMessage OnMessageCalls;

    // List of chat history for all channels and private messages
    private Dictionary<string, string> channelsText = new Dictionary<string, string>();
    
    // List of scrolls positions for all channels and private messages
    private Dictionary<string, Vector2> channelsScrolls = new Dictionary<string, Vector2>();

    public float timeTry = 1.0f;

    /*
    public void Start()
    {
        channelsText["SERVER"] = ""; // initialize server log
    }
    */
    public void Add(ChannelInfo entry)
    {
        Log(String.Format("Channel entry: {0}", entry.Channel));
    }

    public void Clear()
    {
        // No need to clear something
    }

    public void Finished()
    {
        // Nothing to do here
    }
    
    /// <summary>
    /// Connect to the server; Subscribe to important events, Join channel
    /// </summary>
    public override void Connect()
    {
        if (timeTry <= 10.0f)
        {
            Debug.Log("Chat Connect");
            DisConnect();
            ResetOnMessage();
            CSVR_UIWaitingIcon.instance.Open();
            Invoke("DoConnect", timeTry);
        } else
        {
            ErrorView.instance.ShowPopupError("Thử thoát game vào lại!!");
        }
    }

    public void DoConnect()
    {
        timeTry += 2f;
        irc = new IrcClient();
        irc.Encoding = System.Text.Encoding.UTF8;
        irc.SendDelay = 200; // set send delay (flood protection)
        irc.ActiveChannelSyncing = true;

        irc.OnError += irc_OnError;
        irc.OnRawMessage += irc_OnRawMessage;
        irc.OnQueryMessage += irc_OnQueryMessage;
        irc.OnConnected += irc_OnConnected;
        irc.OnJoin += irc_OnJoin;

        irc.RegisterChannelListConsumer(this);

        try
        {
            irc.Connect(serverName, port);
        }
        catch (ConnectionException e)
        {
            if (DebugError) Debug.Log("Couldn't connect! Reason: " + e.Message);
        }

        try
        {
            string nickname = "u" + HorusManager.instance.playerUserName;
            bool loop = nickname.Contains(" ");
            while (loop)
            {
                nickname = nickname.Remove(nickname.IndexOf(" "), 1);
                loop = nickname.Contains(" ");
            }
            this.GlobalUser = new GlobalUser(nickname, nickname, "null", AccountManager.instance.accountLevel.ToString());
            irc.Login(GlobalUser.NickName, GlobalUser.NickName, 0, GlobalUser.NickName);
            irc.Join("#global");

            if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "")
            {
                irc.Join("#" + HorusManager.instance.Clan.clanName);
            }
        }
        catch (ConnectionException e)
        {
            if (DebugError) Debug.Log(e.Message);
        }
        catch (Exception e)
        {
            if (DebugError) Debug.Log("Error occurred! Message: " + e.Message);
            if (DebugError) Debug.Log("Exception: " + e.StackTrace);
        }
    }

    public override bool IsConnected()
    {
        return (this.irc != null && this.irc.IsConnected);
    }
    
    public override void DisConnect()
    {
        if (DebugError) Debug.Log("Chat DisConnect");
        if (irc != null && irc.IsConnected)
            irc.Disconnect();
    }

    public override void OnMessage(string sender, string message, string channel)
    {
        if (OnMessageCalls != null)
        {
            OnMessageCalls(sender, message, channel);
        }
    }

    public override void SendMessage(string receiver, string message)
    {
        if (irc != null && irc.IsConnected && this.GlobalUser != null)
        {
            message = JsonMapper.ToJson(GlobalUser) + message;

            if (receiver == "#global")
            {
                irc.SendMessage(SendType.Message, "#global", message, Priority.Critical);
            } else if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "" && receiver == "#" + HorusManager.instance.Clan.clanName)
            {
                irc.SendMessage(SendType.Message, "#" + HorusManager.instance.Clan.clanName, message, Priority.Critical);
            }
            else
            {
                receiver = "u" + receiver;
                if (IndexChannel(receiver) == -1)
                {
                    this.irc.Join(receiver);
                }
                irc.SendMessage(SendType.Message, receiver, message, Priority.Medium);
            }
        }
    }

    void Update()
    {
        // Update irc: send and receive messages
        if (irc != null && irc.IsConnected)
            irc.ListenOnce(false);
    }    

    void Log(string message)
    {
        channelsText["SERVER"] += message;
        if (DebugError) Debug.Log(message);
    }
    
    /// <summary>
    /// Handle all messages here. 
    /// For In-Game chat simply log all messages and show only messages for selected channel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void irc_OnRawMessage(object sender, IrcEventArgs e)
    {
        if (CSVR_UIChat.instance.DebugError) Debug.Log ("irc_OnRawMessage");

        if (!string.IsNullOrEmpty(e.Data.Nick))
        {
            // Put all non-channel messages in "SERVER" tab
            string channel = string.IsNullOrEmpty(e.Data.Channel) || e.Data.Type != ReceiveType.ChannelMessage ? "NOCHANNEL" : e.Data.Channel;
            string message = string.IsNullOrEmpty(e.Data.Message) ? "" : e.Data.Message;
            OnMessage(e.Data.Nick, message, channel);
        }
    }
    
    public void ResetOnMessage()
    {
        if (OnMessageCalls != null)
        {
            foreach (Delegate d in OnMessageCalls.GetInvocationList())
            {
                OnMessageCalls -= (Delegate_OnMessage)d;
            }
        }
    
    }
    
    /// <summary>
    /// Analyze private messages
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void irc_OnQueryMessage(object sender, IrcEventArgs e)
    {
        if (!channelsText.ContainsKey(e.Data.Nick))
            channelsText[e.Data.Nick] = ""; // Initiate dictionary entry
        else if (channelsText[e.Data.Nick].Length > 2)
            channelsText[e.Data.Nick] += "\r\n"; // Or append new line characters

        string message = string.Format("<{0}> {1}", e.Data.Nick, e.Data.Message);
        channelsText[e.Data.Nick] += message;
        // Scroll down on new message
        if (channelsScrolls.ContainsKey(e.Data.Nick))
            channelsScrolls[e.Data.Nick] = new Vector2(0.0f, float.MaxValue);
    }

    void irc_OnError(object sender, ErrorEventArgs e)
    {
        CSVR_UIWaitingIcon.instance.Close();
        if (DebugError) Debug.LogError(e.ErrorMessage);
        ErrorView.instance.ShowPopupError("Kết nối Chat thất bại!! Hãy thử lại!!");
    }

    /// <summary>
    /// Close connection on application exit
    /// </summary>
    void OnApplicationQuit()
    {
        StartCoroutine(StopIRC());
    }

    private IEnumerator StopIRC()
    {
        if (irc != null && irc.IsConnected)
        {
            irc.Quit("Application close");
            yield return new WaitForSeconds(1.0f);
            try
            {
                irc.Disconnect();
            }
            catch (Exception)
            {
            }
        }
        yield break;
    }

    void irc_OnConnected(object sender, EventArgs e)
    {
        CSVR_UIWaitingIcon.instance.Close();        
        OnMessageCalls += CSVR_UIChat.instance.UIChat_OnMessage;
    }

    void irc_OnJoin(object sender, JoinEventArgs e)
    {
        CSVR_UIWaitingIcon.instance.Close();
        CSVR_UIChat.instance.UIChat_OnMessage(HorusManager.instance.playerUserName, "vào!", "#global");
        SendMessage("#global", "vào!");
    }

    int IndexChannel(string channel)
    {
        int index = -1;
        string[] ChannelNames = this.irc.GetChannels();
        for (int i = 0; i < ChannelNames.Length; i++)
        {
            if (ChannelNames[i] == channel)
            {
                index = i;
                break;
            }
        }
        return index;
    }
}
