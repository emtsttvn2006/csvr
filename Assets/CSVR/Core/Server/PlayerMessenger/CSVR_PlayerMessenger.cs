﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using yIRC;

public abstract class CSVR_PlayerMessenger : MonoBehaviour {

    public static CSVR_PlayerMessenger instance;

    public bool isChatPanelCalled = true;

    public delegate void Delegate_OnMessage(string sender, string message, string channel);
	public virtual event Delegate_OnMessage OnMessageCalls;

    public GlobalUser GlobalUser;

#if DEV_TEST
	public string serverName = "devirc.csvro.com";
#else
    public string serverName = "pubirc.csvro.com";
#endif	
	public string channel = "#global";
	public int port = 6667;

    public bool DebugError = true;    

    // Use this for initialization
    void Awake () {
        instance = this;
    }

    public virtual bool IsConnected()
    {
        return false;
    }

	public virtual void Connect() {
		
	}

	public virtual void DisConnect() {

	}

	public virtual void SendMessage(string receiver, string message) {
        
    }

	public virtual void OnMessage(string sender, string message, string channel) {
		Debug.Log (sender + " says " + message + " on " + channel);

		if (OnMessageCalls != null) {
			OnMessageCalls (sender, message, channel);
		}
	}    

	void OnLevelWasLoaded(int levelIndex) {
		if (SceneManager.GetActiveScene().name != "Home") {			
			this.DisConnect ();
		}
	}
}
