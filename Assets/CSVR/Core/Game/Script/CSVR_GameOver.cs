﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_GameOver : BaseScreen {
	
//	// Use this for initialization
//	void Start () {
//		//UpdateDragThreshold.instance.SetPixelDrag (5);
//		vp_Timer time =  FindObjectOfType(typeof(vp_Timer)) as vp_Timer;
//        try
//        {
//            Destroy(time.gameObject);
//        }
//        catch
//        {
//            Destroy(this.gameObject);
//        }
//		vp_MPMaster.m_Instance = null;
//		vp_MPMaster.Phase = vp_MPMaster.GamePhase.NotStarted;
//		
//		vp_MPPickupManager.m_Instance = null;
//		
//		vp_MPPlayerSpawner.m_Instance = null;
//		
//		vp_MPTeamManager.m_Instance = null;
//		
//		vp_MPLocalPlayer.m_Instance = null;
//		//		
//		vp_MPNetworkPlayer.Players.Clear ();
//		//vp_MPNetworkPlayer.PlayersByID.Clear ();
//		
//		CSVR_UIPlay uiPlay = CSVR_ApplicationManager.instance.transform.root.FindChild("CSVR_PlayUI1").GetComponent<CSVR_UIPlay>();
//		
//		Canvas cans = uiPlay.canvas;
//		
//		cans.renderMode = RenderMode.WorldSpace;
//
//		cans.GetComponent<RectTransform> ().localScale = Vector3.zero;
//		//cans.GetComponent<RectTransform> ().transform.position = new Vector3 (0, 500, 0);
//		CSVR_ApplicationManager.instance.Background.gameObject.SetActive (true);
//		CSVR_ApplicationManager.instance.camUI.gameObject.SetActive (true);
//		CSVR_ApplicationManager.instance.camera2D.gameObject.SetActive (false);
//		CSVR_ApplicationManager.instance.UIHome.gameObject.SetActive (true);
//		CSVR_ApplicationManager.instance.IsEndGame ();
//		GameObject[] gos = GameObject.FindGameObjectsWithTag("Player");
//		for (int i = 0; i<gos.Length; i++) {
//			Destroy(gos[i]);
//		}
//		GameObject[] gosPolice = GameObject.FindGameObjectsWithTag("PolicePlayer");
//		for (int i = 0; i< gosPolice.Length; i++) {
//			Destroy(gosPolice[i]);
//		}
//		GameObject[] gosTerrorism = GameObject.FindGameObjectsWithTag("TerrorisPlayer");
//		for (int i = 0; i<gosTerrorism.Length; i++) {
//			Destroy(gosTerrorism[i]);
//		}
//		
//		Application.LoadLevel("CSVR_Lobby");
//		
//		if(CSVR_UILevelUp.instance.isLevelUp)
//			CSVR_ApplicationManager.applicationState = CSVR_ApplicationState.LEVELUP;
//		else
//			CSVR_ApplicationManager.applicationState = CSVR_ApplicationState.ENDGAME;
//		
//	}
	//Trường 9/4/2016
	void Awake () {
		
		
		vp_Timer time =  FindObjectOfType(typeof(vp_Timer)) as vp_Timer;

		try
		{
			Destroy(time.gameObject);
		}
		catch
		{

			Destroy(this.gameObject);
		}
		vp_MPMaster.m_Instance = null;
		vp_MPMaster.Phase = vp_MPMaster.GamePhase.NotStarted;
		
		vp_MPPickupManager.m_Instance = null;
		
		vp_MPPlayerSpawner.m_Instance = null;
		
		vp_MPTeamManager.m_Instance = null;
		
		vp_MPLocalPlayer.m_Instance = null;


		vp_MPNetworkPlayer.instance = null;

		vp_MPNetworkPlayer.Players.Clear ();
		GameObject[] gosAI = GameObject.FindGameObjectsWithTag("TeamAI");
		for (int i = 0; i<gosAI.Length; i++) {
			Destroy(gosAI[i]);
		}
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Player");
		for (int i = 0; i<gos.Length; i++) {
			Destroy(gos[i]);
		}
		GameObject[] gosPolice = GameObject.FindGameObjectsWithTag("PolicePlayer");
		for (int i = 0; i< gosPolice.Length; i++) {
			Destroy(gosPolice[i]);
		}
		GameObject[] gosTerrorism = GameObject.FindGameObjectsWithTag("TerrorisPlayer");
		for (int i = 0; i<gosTerrorism.Length; i++) {
			Destroy(gosTerrorism[i]);
		}
		
		UI_BackToHome ();
	}
	void UI_BackToHome(){
		SceneLoader.sceneToLoad = Scenes.Home;
		CallChangeScene ();
	}
}
