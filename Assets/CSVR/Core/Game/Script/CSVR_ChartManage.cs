﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CSVR_ChartManage : MonoBehaviour {

	public Image[] AvatarPlayerTeam1,AvatarPlayerTeam2;
	public Text[] NamePlayerTeam1, NamePlayerTeam2,KillerTeam1,KillerTeam2,DeadTeam1,DeadTeam2,HeadShotTeam1,HeadShotTeam2;

	// Use this for initialization
    
    private void OnEnable()
    {
		//xóa dữ liệu người chơi trước khi cập nhật.
		ClearPortraits ();
		// cập nhật dữ liệu người chơi sau 2 giây.
		InvokeRepeating("ScoreBoardView", 0f,2f);
    }

	private void OnDisable()
	{
		//dừng cập nhật dữ liệu người chơi.
		CancelInvoke();
	}

	private void ClearPortraits ()
	{
		for (int i = 0; i < 5; i++)
		{
			NamePlayerTeam1[i].text = "";
			KillerTeam1[i].text = "";
			DeadTeam1[i].text = "";
			HeadShotTeam1[i].text = "";
			
			NamePlayerTeam2[i].text = "";
			KillerTeam2[i].text = "";
			DeadTeam2[i].text = "";
			HeadShotTeam2[i].text = "";
		}
	}

	private void ScoreBoardView(){
		int indexTeam1 = 0;
		int indexTeam2 = 0;
		int k = 0;
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{	

			if (p == null)
				continue;

			if (p.TeamNumber == 1 || (p.TeamNumber == 0 && p.ID % 2 != 0))
			{

				NamePlayerTeam1[indexTeam1].text = vp_MPNetworkPlayer.GetName(p.photonView.ownerId).ToString();
				AvatarPlayerTeam1[indexTeam1].sprite = AvatarPlayer(NamePlayerTeam1[indexTeam1].text);
				KillerTeam1[indexTeam1].text = p.Stats.Get("Frags").ToString();
				DeadTeam1[indexTeam1].text = p.Stats.Get("Deaths").ToString();
				HeadShotTeam1[indexTeam1].text = p.Stats.Get("HeadShot").ToString();
				if (indexTeam1 == 1)
					indexTeam1 = CSVR_GameSetting.AI_Police;
				else
					indexTeam1++;
			}
			else
			{
				NamePlayerTeam2[indexTeam2].text = vp_MPNetworkPlayer.GetName(p.photonView.ownerId).ToString();
				AvatarPlayerTeam2[indexTeam2].sprite = AvatarPlayer(NamePlayerTeam2[indexTeam2].text);
				KillerTeam2[indexTeam2].text = p.Stats.Get("Frags").ToString();
				DeadTeam2[indexTeam2].text = p.Stats.Get("Deaths").ToString();
				HeadShotTeam2[indexTeam2].text = p.Stats.Get("HeadShot").ToString();
				if (indexTeam2 == 1)
					indexTeam2 = CSVR_GameSetting.AI_Terrorist;
				else
					indexTeam2++;
			}

		}
		foreach (CSVR_UIAIInfo a in CSVR_GameSetting.AI_InGame)
		{	
			if (a.AI_TeamNumber == 1 )
			{

				NamePlayerTeam1[indexTeam1].text = a.AI_Name;
				//AvatarPlayerTeam1[indexTeam1].sprite = AvatarPlayer(NamePlayerTeam1[indexTeam1].text);
				KillerTeam1[indexTeam1].text = a.AI_Kill.ToString();
				DeadTeam1[indexTeam1].text = a.AI_Death.ToString();
				HeadShotTeam1[indexTeam1].text = a.AI_HeadShot.ToString();
				indexTeam1++;
			}
			else
			{
				NamePlayerTeam2[indexTeam2].text = a.AI_Name;
				//AvatarPlayerTeam1[indexTeam1].sprite = AvatarPlayer(NamePlayerTeam1[indexTeam1].text);
				KillerTeam2[indexTeam2].text = a.AI_Kill.ToString();
				DeadTeam2[indexTeam2].text = a.AI_Death.ToString();
				HeadShotTeam2[indexTeam2].text = a.AI_HeadShot.ToString();
				indexTeam2++;
			}
		}
	}
	private Sprite AvatarPlayer(string name){
		PhotonPlayer[] players = PhotonNetwork.playerList;
		for (int i = 0; i < players.Length; i++)
		{
			if(players[i].customProperties["PlayFabName"].ToString() == name){
				byte[] pngBytes = players[i].customProperties["Avatar"] as byte[];
				Texture2D tex = new Texture2D(128, 128);
				tex.LoadImage(pngBytes);
				Sprite playerAvatarHUD = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2());
				return playerAvatarHUD;
				break;
			}
		}
		return null;
	}
	
}
