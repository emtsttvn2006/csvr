﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

	public class CSVR_SingleGameManager : MonoBehaviour {
		public GameObject userPrefab;
		//public GameObject userInstance;
		public GameObject[] spawnPoints_Robber;
		public GameObject[] spawnPoints_Police;
	

		public int[]	AIMaxAmount;
		public string[] AIPrefabs;

	//	public static Dictionary<string,int> AIAmountDic;
		//public Dictionary<GameObject,string> AIPrefabDic;


		// Use this for initialization
		void Start () {


		}

		void OnLevelWasLoaded(int level) {
			if (level != 0) {
				ResetGame();
			}
		}

		public void ResetGame() {
			//AIPrefabDic = new Dictionary<GameObject,string> ();
			//Clearn
            foreach (GameObject instance in GameObject.FindGameObjectsWithTag("Police"))
            {
				Destroy(instance);
			}

            foreach (GameObject instance in GameObject.FindGameObjectsWithTag("Terrorism"))
            {
				Destroy(instance);
			}



			//Respawn
            spawnPoints_Police = GameObject.FindGameObjectsWithTag("Police");
            spawnPoints_Robber = GameObject.FindGameObjectsWithTag("Terrorism");



			if (spawnPoints_Police.Length > 0) { 
			
				GameObject.Instantiate (userPrefab, spawnPoints_Police[Random.Range(0, spawnPoints_Police.Length)].transform.position, Quaternion.identity );
				
				//AI Police
				for (int j = 0; j < AIMaxAmount [0]; j++) { 
					GameObject newAI = (GameObject)GameObject.Instantiate (Resources.Load (AIPrefabs [0]), spawnPoints_Police[Random.Range(0, spawnPoints_Police.Length)].transform.position, Quaternion.Euler(0, 180, 0) );
					//AIPrefabDic.Add (newAI, AIPrefabs [0]);							
				}
				
				//AI Robber
				for (int j = 0; j < AIMaxAmount [1]; j++) { 
					GameObject newAI = (GameObject)GameObject.Instantiate (Resources.Load (AIPrefabs [1]), spawnPoints_Robber[Random.Range(0, spawnPoints_Robber.Length)].transform.position,Quaternion.Euler(0, 0, 0) );
					//AIPrefabDic.Add (newAI, AIPrefabs [1]);							
				}
			}
		}
		
	

		public void respawnAI(GameObject oldAI) {
			GameObject newAI = null;
		
			//if  ( AIPrefabDic [oldAI] == AIPrefabs [0]) {
			if (oldAI.tag == "Police") {
				newAI = (GameObject)GameObject.Instantiate (Resources.Load (AIPrefabs [0]), spawnPoints_Police [Random.Range (0, spawnPoints_Police.Length)].transform.position, Quaternion.Euler (0, 180, 0));
			}
		//	} else if  ( AIPrefabDic [oldAI] == AIPrefabs [1]) {
			else {

				newAI = (GameObject)GameObject.Instantiate (Resources.Load (AIPrefabs [1]), spawnPoints_Robber[Random.Range(0, spawnPoints_Robber.Length)].transform.position, Quaternion.Euler(0, 0, 0) );
			}

			if (newAI != null) {
			//	AIPrefabDic.Add (newAI, AIPrefabDic[oldAI]);
			//	AIPrefabDic.Remove (oldAI);
				Destroy (oldAI);
			}



		}
	}
