﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_ShowCharts : MonoBehaviour {
    
    public Text txtName, txtK, txtD, txtH, txtG;
    public Image iconClan, iconDeath;

    public void GetChart(string name, string k, string d,string h)
    {
        txtName.text = name;
        txtK.text = k;
        txtD.text = d;
        txtH.text = h;
    }
}
