﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_DisibleAllUI : MonoBehaviour {
    Color newCol, getColor;
    float AngleIncrease;
    public float CrosshairWidth;
    public Text txtName;

    float LineCrosshair;
    public GameObject PlayUI;
	// Use this for initialization
	void Start () {
        PlayUI.GetComponent<CSVR_GetInfo>().enabled = true;
        StartCoroutine("plessLoadLevel");
        

        LineCrosshair = (Crosshair[0].transform.GetComponent<RectTransform>().sizeDelta.y/2f);
	}
    IEnumerator plessLoadLevel()
    {
        yield return new WaitForSeconds(0.2f);
        CSVR_GetInfo.instance.txtMaxKill.text = CSVR_GameSetting.maxSkill.ToString();
    }
	// Update is called once per frame
    public float CrosshairTopWidth;
    public float CrosshairTopHeight;
    public Image objCrosshair;
    public Image[] Crosshair;
    float txtSize;
    float _localScale;
    float tdx, tdy, _width;
    void Update()
    {
		if (CSVR_FPWeaponShooter.instance != null) {
			AngleIncrease = CSVR_FPWeaponShooter.instance.currentBulletAngle - CSVR_FPWeaponShooter.instance.BulletAngleMin;
			_localScale = AngleIncrease + CSVR_FPWeaponShooter.instance.BulletAngleMin;
			objCrosshair.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
			CrosshairWidth = 21 * _localScale;
			objCrosshair.GetComponent<RectTransform> ().sizeDelta = new Vector2 (CrosshairWidth, CrosshairWidth);
			tdx = ((CrosshairWidth / 2f)) + LineCrosshair;
			tdy = ((CrosshairWidth / 2f)) + LineCrosshair;
			Crosshair [0].transform.localPosition = new Vector3 (0, tdy, 0);
			Crosshair [1].transform.localPosition = new Vector3 (0, -tdy, 0);
			Crosshair [2].transform.localPosition = new Vector3 (tdx, 0, 0);
			Crosshair [3].transform.localPosition = new Vector3 (-tdx, 0, 0);
		}
    }
}
