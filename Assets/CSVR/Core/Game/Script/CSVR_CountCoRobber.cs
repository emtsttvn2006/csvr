﻿using UnityEngine;
using System.Collections;

public class CSVR_CountCoRobber : MonoBehaviour {
    int CountCo=0;
    public GameObject obj;
    void OnTriggerEnter(Collider other)
    {
        string name = "";
        name = other.gameObject.GetComponent<CSVR_PlayerRespawner>().SpawnPointTag;
        if (other.gameObject.tag == "PlayerFlag" && name == "Terrorism")
        {
            CountCo = CountCo + 1;
            other.gameObject.tag = "Player";
            if (CountCo == 5)
            {
                GameObject.Instantiate(obj, transform.position, transform.rotation);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {        
        other.gameObject.tag = "Player";
    }
	
}
