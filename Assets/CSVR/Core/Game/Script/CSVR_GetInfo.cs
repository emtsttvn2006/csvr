﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_GetInfo : Photon.MonoBehaviour
{
	public static CSVR_GetInfo instance = null;
	//begin info player, heath amor and name.
	public Image imgHeath, imgAmor;
	public Text txtHeath, txtAmor,txtName;
	//end info player.

	//begin info mode time...
	public Text _time, _kill1, _kill2, txtMaxKill;
	public int CountPoliceKill = 0, CountTerrorism = 0;
	//end info mode.

	//begin round
	public Image imgRound;
	public Sprite[] listRoundIcon;
	//end round

	//begin photon ping
	public Image pingIcon;
	public Text pingText;
	public Sprite[] listPingIcon;
	//end photon ping.

	//begin mode deathmatch, tìm thằng kill nhiều nhất.
	public int MaxFrags = 0;
	//end mode deathmatch.

	//begin time change master
	private float nextChange = 0.0F;
	//end time change master.

	private vp_MPNetworkPlayer _cachePlayer;

	private void Start()
	{
		
		instance = this;
		#if !VIRTUAL_ROOM
		_cacheRound = -1;
		//đếm thời gian của trận đấu
		InvokeRepeating("DrawTime",1f,1f);
		//xem ping người chơi
		InvokeRepeating("DrawPing",1f,5f);

		switch (CSVR_GameSetting.modeName)
		{
		case "CSVR_Deathmatch":
			txtMaxKill.text = CSVR_GameSetting.maxSkill.ToString();
			InvokeRepeating("DrawDeathMatch",1f,1f);
			break;
		case "CSVR_AITeamDeathmatch":
			InvokeRepeating("DrawAITeamDeathMatch",1f,1f);
			txtMaxKill.text = CSVR_GameSetting.maxSkill.ToString();
			break;
		case "CSVR_TeamDeathmatch":
			InvokeRepeating("DrawTeamDeathMatch",1f,1f);
			txtMaxKill.text = CSVR_GameSetting.maxSkill.ToString();
			break;
		case "CSVR_SearchAndDestroy":
		case "CSVR_DeadOrAlive":
			_kill1.text = CSVR_GameSetting.policeWin.ToString();
			_kill2.text = CSVR_GameSetting.terroristWin.ToString();
			txtMaxKill.text = CSVR_GameSetting.maxWin.ToString();
			break;
		case "CSVR_BaseCapture":
			DrawBaseCapture();
			txtMaxKill.text = vp_BCMaster.BCGoalPoints.ToString();
			break;
		case "CSVR_ZombieSurvival":
			((vp_ZombieSurvival)vp_ZombieSurvival.Instance).RoundsPassed = ((vp_ZombieSurvival)vp_ZombieSurvival.Instance).RoundStart - 1;
			txtMaxKill.text = ((vp_ZombieSurvival)vp_ZombieSurvival.Instance).MaxRound.ToString();
			DrawZombieSurvival(((vp_ZombieSurvival)vp_ZombieSurvival.Instance).RoundStart.ToString(), ((vp_ZombieSurvival)vp_ZombieSurvival.Instance).LivesLeft.ToString());
			CSVR_GetInfo.instance.NewRound (((vp_ZombieSurvival)vp_ZombieSurvival.Instance).RoundsPassed);
			break;
		}
		#endif
	}
	private void OnDestroy(){
		CancelInvoke();
	}

	#region info player
	public void ShowPlayerName(string _name,int teamNumber)
	{
		txtName.text=_name;
		if (teamNumber == 1)
		{
			txtName.color = new Color(0, 0.625f, 1, 1);
		}
		else
		{
			txtName.color = new Color(1, 0, 0, 1);
		}
	}
	public void ShowHeath(int _heath)
	{
		if (_heath > 0)
		{
			imgHeath.fillAmount = _heath / 100f;
			txtHeath.text = _heath.ToString();
		}
		else
		{
			imgHeath.fillAmount = 0;
			txtHeath.text = "0";
		}
	}
	public void ShowAmor(int _amor)
	{
		if (_amor > 0)
		{
			imgAmor.fillAmount = _amor / 100f;
			txtAmor.text = _amor.ToString();
		}
		else
		{
			imgAmor.fillAmount = 0;
			txtAmor.text = "0";
		}
	}
	#endregion

	#region info mode
	private int SortMaxFrags()
	{
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{
			if (p == null) continue;

			if (MaxFrags < int.Parse(p.Stats.Get("Frags").ToString()) )
			{
				MaxFrags = int.Parse(p.Stats.Get("Frags").ToString());
			}
		}

		foreach (CSVR_UIAIInfo a in CSVR_GameSetting.AI_InGame)
		{	
			if ( a == null) continue;

			if (MaxFrags < a.AI_Kill )
			{
				MaxFrags = a.AI_Kill;
			}
		}
		return MaxFrags;
	}
	private void DrawDeathMatch()
	{
		if (_cachePlayer == null) {
			foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values) {
				if (p == null)
					continue;
				if (p.TeamNumber == 0 && p.Player.IsLocal.Get ()) {
					_cachePlayer = p;
				}
			}
		}
		_kill1.text = _cachePlayer.Stats.Get ("Frags").ToString ();
		_kill2.text = SortMaxFrags ().ToString ();
	}
	private void DrawBaseCapture()
	{
		_kill1.text = vp_BCMaster.BCTeam1Scores.ToString();
		_kill2.text = vp_BCMaster.BCTeam1Scores.ToString();
	}
	private void DrawAITeamDeathMatch()
	{
		_kill1.text = vp_AITDMMaster.Team1KillCount.ToString();
		_kill2.text = vp_AITDMMaster.Team2KillCount.ToString();
	}
	private void DrawTeamDeathMatch()
	{
		_kill1.text = vp_TDMMaster.Team1KillCount.ToString();
		_kill2.text = vp_TDMMaster.Team2KillCount.ToString();
	}
	public void DrawSearchAnDestroy(string team1Kill,string team2Kill)
	{
		_kill1.text = team1Kill;
		_kill2.text = team2Kill;
	}
	public void DrawZombieSurvival(string RoundsPassed,string LivesLeft)
	{
		_kill1.text =  RoundsPassed;
		_kill2.text =  LivesLeft;
	}
	#endregion

	#region info time	
	string GetFormattedTime(float t)
	{

		t = Mathf.Max(0, t);
		int hours = ((int)t) / 3600;
		int minutes = (((int)t) - (hours * 3600)) / 60;
		int seconds = ((int)t) % 60;

		string s = "";
		s += (hours > 0) ? hours.ToString() + ":" : "";
		s += ((minutes > 0) ? (minutes < 10) ? "0" + minutes + ":" : minutes + ":" : "");
		s += (seconds < 10) ? "0" + seconds : seconds.ToString();
		return s;

	}
	void DrawTime()
	{

		string s = GetFormattedTime(vp_MPClock.TimeLeft);
		_time.text = s;
		if (PhotonNetwork.isMasterClient && vp_MPClock.TimeLeft < 0){
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
		}
	}
	#endregion

	bool isCallBack = true;
	void DrawPing()
	{
		if (PhotonNetwork.GetPing () < 130) {
			pingText.color = Color.green;
			pingIcon.sprite = listPingIcon [0];
		} else if (PhotonNetwork.GetPing () > 230) {
			pingText.color = Color.red;
			pingIcon.sprite = listPingIcon [2];
		} else {
			pingText.color = Color.yellow;
			pingIcon.sprite = listPingIcon [1];
		}
		//tam thoi bo mang lag chuyen master
//#if  !VIRTUAL_ROOM
		//sau 30s mới cho gọi hàm ChoiseMasterClient để tránh hiện tượng lag game.
//		if (isCallBack && vp_MPClock.TimeLeft < 250 && PhotonNetwork.isMasterClient && PhotonNetwork.GetPing () > 150 && (nextChange == 0 || Time.time > nextChange)) {
//			StartCoroutine("CallBackChangeMaster");
//		}
//#endif

		pingText.text = PhotonNetwork.GetPing () + " ms";

	}
    //IEnumerator CallBackChangeMaster(){
    //    isCallBack = false;
    //    yield return new WaitForSeconds (5f);
    //    if (vp_MPClock.TimeLeft > 1 && vp_MPClock.TimeLeft < 250 && PhotonNetwork.isMasterClient && PhotonNetwork.GetPing () > 150 && (nextChange == 0 || Time.time > nextChange)) {
    //        isCallBack = true;
    //        nextChange = Time.time + 15;
    //        PlayFabManager.instance.ChoiseMasterClient();
    //    }
    //}

	//HUD hiệp đấu ở các mode chơi theo hiệp
	int _cacheRound = -1;
	public void NewRound(){
		if (_cacheRound == -1) {
			_cacheRound = vp_MPMaster.Round;
		} else {
			_cacheRound++;
		}
		imgRound.enabled = true;
		imgRound.sprite = listRoundIcon [_cacheRound];
		StartCoroutine("DisableRoundHUD");
	}

	public void NewRound(int roundID)
	{
		imgRound.enabled = true;
		imgRound.sprite = listRoundIcon [roundID];
		StartCoroutine("DisableRoundHUD");
	}

	IEnumerator DisableRoundHUD(){
		yield return new WaitForSeconds (1.5f);
		imgRound.enabled = false;
	}

	// mode basecapture
	public  void Update_BCScoreUI1(int score){
		_kill1.text = score.ToString();
	}
	
	public  void Update_BCScoreUI2(int score){
		_kill2.text = score.ToString();
	}
	//endmode
}
