﻿using UnityEngine;
using System.Collections;

public class CSVR_GDMMaster : vp_MPMaster
{
    public static bool checkGunDM;
	// Use this for initialization
	void Start () {
        checkGunDM = true;
	
	}

    [PunRPC]
    protected override void ReceiveFreeze(PhotonMessageInfo info)
    {

        if (!info.sender.isMasterClient)
            return;

        base.ReceiveFreeze(info);

//        vp_DMDemoScoreBoard.ShowScore = true;

    }


    /// <summary>
    /// 
    /// </summary>
    [PunRPC]
    protected override void ReceiveUnFreeze(PhotonMessageInfo info)
    {

        if (!info.sender.isMasterClient)
            return;

        base.ReceiveUnFreeze(info);

       // vp_DMDemoScoreBoard.ShowScore = false;

    }
}
