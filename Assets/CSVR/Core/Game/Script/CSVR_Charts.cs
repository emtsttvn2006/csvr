﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_Charts : MonoBehaviour
{
    Button btnResult;

    int CountClick;
	void Start () {
        CountClick = 0;

		btnResult = CSVR_UIPlay.instance.btnResult;
        btnResult.onClick.AddListener(() => { ShowChart(); });
	}
    
    public void ShowChart()
    {
        CountClick++;
        if (CountClick > 1)
        {
            CountClick = 0;
        }
        if (CountClick == 1)
        {
			CSVR_UIPlay.instance.ScoreBoard.gameObject.SetActive(true);
            //uiPlay.ScoreBoard.gameObject.GetComponent<CSVR_ChartManage>().Inits();
        }
        else
        {
			CSVR_UIPlay.instance.ScoreBoard.gameObject.SetActive(false); 
        }
    }
}
