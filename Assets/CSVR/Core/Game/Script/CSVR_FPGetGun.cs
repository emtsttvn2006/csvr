﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CSVR_FPGetGun : Photon.MonoBehaviour
{
	int BiginTime, CurrentTime;
	public Button tbnNhatSung;
	Text txtNameWeapon;
	public static CSVR_FPGetGun getgun = null;
	public string IDPlay = "";
	public static bool _nhatSung;
	bool  checkSkill;
	Vector3 vec3;
	Quaternion dir;
	public int HeadShot, StarIndex;
	GameObject Weapon, Bullet;
	CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
	CSVR_ServerConnector serverConnectorInstance
	{
		get
		{
			if (m_serverConnectorInstance == null)
			{
				m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
			}
			return m_serverConnectorInstance;
		}
	} 
	
	private vp_PlayerEventHandler m_Player = null;	// should never be referenced directly
	protected vp_PlayerEventHandler Player	// lazy initialization of the event handler field
	{
		get
		{
			if (m_Player == null)
				m_Player = transform.GetComponent<vp_PlayerEventHandler>();
			return m_Player;
		}
	}

	LongPressEventTrigger longPressEventTrigger;

	void OnEnable()
	{
		checkSkill = false;
		StarIndex = 1;
		timeLeft = 0f;
	}
	void OnDisable()
	{
		checkSkill = false;
		StarIndex = 1;
		timeLeft = 0f;
	}
	float timeLeft;// -= Time.deltaTime;
	
	void Start()
	{
		getgun = this;        
		tbnNhatSung = CSVR_UIPlayManage.init.icon;
		txtNameWeapon = CSVR_UIPlayManage.init.nameWeapon;
		tbnNhatSung.onClick.AddListener(() => Request_LootGun());
		longPressEventTrigger = CSVR_UIPlay.instance.longPressEventTrigger;

		foreach (Transform obj in CSVR_UIPlayManage.init.InitBadge.transform)
		{
			if (obj != null)
			{
				Destroy(obj.gameObject);
			}
		}
			
	}
	void OnTriggerEnter(Collider other)
	{
		
		if (!Player.Dead.Active && other.gameObject.tag == "Weapon") 
		{
			Weapon = other.gameObject;
			SetWeaponInfo WeaponInfo = other.gameObject.GetComponent<SetWeaponInfo> ();
			txtNameWeapon.text = WeaponInfo.WeaponName;
			tbnNhatSung.image.sprite = Resources.Load<Sprite> ("Sprites/UIWeapon/" + WeaponInfo.WeaponID + "_icon");
			tbnNhatSung.gameObject.SetActive (true);
			
			CurrentDroppedGunPlayfabID = WeaponInfo.PlayfabID;
			CurrentDroppedGunID = WeaponInfo.DroppedGunID;
		} 
		else if(!Player.Dead.Active && other.gameObject.tag == "C4") 
		{
			if (serverConnectorInstance.idTeams == 2) {
				if (!vp_SADMaster.C4IsPlanted) {
					Weapon = other.gameObject;
					txtNameWeapon.text = "Bomb C4";
					tbnNhatSung.image.sprite = Resources.Load<Sprite> ("Sprites/UIWeapon/00_000_C4_icon");
					tbnNhatSung.gameObject.SetActive (true);
				}
			} else {
				if (vp_SADMaster.C4IsPlanted ) {
					longPressEventTrigger.gameObject.SetActive(true);
				} 
			}
		}
		else if(!Player.Dead.Active &&( other.gameObject.tag == GameConstants.VNMODESAD_BombSiteA || other.gameObject.tag == GameConstants.VNMODESAD_BombSiteB)) 
		{
			if (serverConnectorInstance.idTeams == 2) {
				if (!vp_SADMaster.C4IsPlanted && System.Convert.ToBoolean (photonView.owner.customProperties ["BombCarrier"]) && CSVR_GrenadeShooter.instanceC4 != null ) {
					longPressEventTrigger.bombSiteName = other.gameObject.tag;
					longPressEventTrigger.gameObject.SetActive(true);
				} 
			}
		}
	}
	
	void OnTriggerExit(Collider other)
	{

		if (other.gameObject.tag == "Weapon" || other.gameObject.tag == "C4")
		{
			tbnNhatSung.gameObject.SetActive(false);
			longPressEventTrigger.gameObject.SetActive(false);
		}
		else if( (other.gameObject.tag == GameConstants.VNMODESAD_BombSiteA ||  other.gameObject.tag == GameConstants.VNMODESAD_BombSiteB) && serverConnectorInstance.idTeams == 2 ) 
		{
			//Debug.Log ("ra ngoai diem dat bomb");
			longPressEventTrigger.gameObject.SetActive(false);
			if(CSVR_GrenadeShooter.instanceC4 != null)
				CSVR_GrenadeShooter.instanceC4.StopAnimC4();

		}
	}
	
	//không phải load resource nhiều lần cho một súng
	string _cacheNameWeapon = "";
	Sprite _cacheIconWeapon = null;
	public void TransmitKillerInfo(string id, string str1, string str2, string level, string str3, int HitPart)//string shooter_Name, string gunID
	{
		if (id == gameObject.GetComponent<CSVR_ServerConnector>().PlayfabID)
		{
			
			switch(CSVR_GameSetting.modeName)
			{
			case "CSVR_Deathmatch":
			case "CSVR_AITeamDeathmatch":
			case "CSVR_TeamDeathmatch":
            case "CSVR_BaseCapture":
				CSVR_UIPlayManage.init.InfoKiller.SetActive(true);
				CSVR_UIPlay.instance.UIPlayState(false);
				vp_Timer.In(3, delegate()
				            {
					CSVR_UIPlayManage.init.InfoKiller.SetActive(false);
					CSVR_UIPlay.instance.UIPlayState(true);
				});
				break;
			}
			
			if(_cacheNameWeapon == "" || _cacheNameWeapon != str1){
				_cacheNameWeapon = str1;
				_cacheIconWeapon = Resources.Load<Sprite> ("Sprites/UIWeapon/" + str1+"_icon");
			}
			
			CSVR_UIPlayManage.init.ShowInfoKiller(str3, str2, level,_cacheIconWeapon , HitPart);
		}
	}
	public void ShowHuyHieu(int HitPart, float damage, string id,string idDead,string idWeapon)
	{       
		
		if (id == serverConnectorInstance.PlayfabID && idDead != serverConnectorInstance.PlayfabID)
		{
			if (serverConnectorInstance.BackpackManager.Backpacks[serverConnectorInstance.CurrentBackpackIndex].Grenades[0] != null && idWeapon == serverConnectorInstance.BackpackManager.Backpacks[serverConnectorInstance.CurrentBackpackIndex].Grenades[0].ItemID.ToString() )
			{
				
				CSVR_UIPlayManage.init.InitGrenade();
			}
			else if (serverConnectorInstance.BackpackManager.Backpacks[serverConnectorInstance.CurrentBackpackIndex].Melee != null && idWeapon == serverConnectorInstance.BackpackManager.Backpacks[serverConnectorInstance.CurrentBackpackIndex].Melee.ItemID   )
			{
				
				CSVR_UIPlayManage.init.InitKnifeShot();
			}
			else
			{
				if (HitPart == 0 && damage >= 100)
				{
					
					CSVR_UIPlayManage.init.InitHeadShotFull();
					gameObject.GetComponent<vp_MPLocalPlayer>().FireHeadShot();
				}
				else if (HitPart == 0 && damage < 100)
				{
					
					CSVR_UIPlayManage.init.InitHeadShot();
					gameObject.GetComponent<vp_MPLocalPlayer>().FireHeadShot();
				}
				else
				{
					
					CSVR_UIPlayManage.init.InitStar(StarIndex);
					
				}
			}
			
			if (!checkSkill)
			{
				StarIndex = 1;
				checkSkill = true;
			}
			else
			{
				if (StarIndex < 6)
				{
					StarIndex++;
				}
				checkSkill = true;
			}
			timeLeft = 10f;
		}
	}
	void Update()
	{
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0)
		{
			checkSkill = false;
		}
		
		//De phong truong hop sung bi destroyed truoc khi TriggerExit
		if (Weapon == null) {
			tbnNhatSung.gameObject.SetActive (false);
		}

		//Testing
//		if (Input.GetKeyUp("t")) {
//			DropC4();
//		}
	}
	int amountRemove, maxAmountRemove;
	string strWeaponRemove;
	
	public string CurrentDroppedGunPlayfabID;
	public int CurrentDroppedGunID;
	
	
	//Yeu cau Loot gun, gui den master
	void Request_LootGun() {
		if (Weapon.tag == "C4") {
			photonView.RPC("Receive_LootC4", PhotonTargets.MasterClient);
			CSVR_UIPlay.instance.btnGetC4.gameObject.SetActive (true);
		} else {
			photonView.RPC("Receive_LootGun", PhotonTargets.MasterClient, CurrentDroppedGunPlayfabID, CurrentDroppedGunID);
		}
	}

	//demo
//	void DropC4() {
//		GetComponent<CSVR_ServerConnector> ().DropC4 ();
//	}
}
