﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_Crosshair : MonoBehaviour {

    Color newCol, getColor;
    float AngleIncrease;
    public float CrosshairWidth;
    public Text txtName;
//    public GameObject CrosshairObject;
    float LineCrosshair;
    public GameObject PlayUI;

	// Use this for initialization
	void Start () {
        
//        CrosshairObject.SetActive(false);
        PlayUI.GetComponent<CSVR_GetInfo>().enabled = true;
        //StartCoroutine("plessLoadLevel");
        LineCrosshair = (Crosshair[0].transform.GetComponent<RectTransform>().sizeDelta.y/2f);
        
	}
//
//    IEnumerator plessLoadLevel()
//    {
//        yield return new WaitForSeconds(0.2f);
//        CSVR_GetInfo.instance.txtMaxKill.text = CSVR_GameSetting.maxSkill.ToString();
//    }
	// Update is called once per frame
    public float CrosshairTopWidth;
    public float CrosshairTopHeight;
    public Image objCrosshair;
    public Image[] Crosshair;
    float txtSize;
    float _localScale;
    float tdx, tdy, _width;
    int _pixel=11;
    float _time;
    bool _setTime=false;
    bool increaseTime = false;
    void SetCrosshair()
    {
        CrosshairWidth = _pixel * _localScale;
        if (increaseTime == false)
        {
            CrosshairWidth = CrosshairWidth - _pixel / 2;
            increaseTime = true;
        }
        else
        {
            CrosshairWidth = CrosshairWidth + _pixel / 2;
            increaseTime = false;
        }
    }
    void Update()
    {
        
		if (CSVR_FPWeaponShooter.instance != null) {
            
            AngleIncrease = (CSVR_FPWeaponShooter.instance.currentBulletAngle * 2);// -CSVR_FPWeaponShooter.instance.BulletAngleMin;
            _localScale = AngleIncrease;// +CSVR_FPWeaponShooter.instance.BulletAngleMin;
			objCrosshair.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);
           
            if (CSVR_FPWeaponShooter.instance.AttackState && !_setTime)
            {
                InvokeRepeating("SetCrosshair", 0.0f, 0.05f);
                _setTime = true;
            }
            else if (!CSVR_FPWeaponShooter.instance.AttackState)
            {
                CrosshairWidth = _pixel * _localScale;
                _setTime = false;
            }
			objCrosshair.GetComponent<RectTransform> ().sizeDelta = new Vector2 (CrosshairWidth, CrosshairWidth);
			tdx = ((CrosshairWidth / 2f)) + LineCrosshair;
			tdy = ((CrosshairWidth / 2f)) + LineCrosshair;
			Crosshair [0].transform.localPosition = new Vector3 (0, tdy, 0);
			Crosshair [1].transform.localPosition = new Vector3 (0, -tdy, 0);
			Crosshair [2].transform.localPosition = new Vector3 (tdx, 0, 0);
			Crosshair [3].transform.localPosition = new Vector3 (-tdx, 0, 0);

			if(CSVR_FPCamera.CrosshairTargetObject_Name != string.Empty){
				if(CSVR_FPCamera.CrosshairTargetObject_Enemy){
					txtName.color = Color.red;
					txtName.text = CSVR_FPCamera.CrosshairTargetObject_Name + " " + CSVR_FPCamera.CrosshairTargetObject_Heath;
//					txtName.text = CSVR_FPCamera.CrosshairTargetObject_Name;
					Crosshair [0].color = Color.red;
					Crosshair [1].color = Color.red;
					Crosshair [2].color = Color.red;
					Crosshair [3].color = Color.red;
				}else{
					txtName.color = Color.white;
					txtName.text = CSVR_FPCamera.CrosshairTargetObject_Name + " " + CSVR_FPCamera.CrosshairTargetObject_Heath;
//					txtName.text = CSVR_FPCamera.CrosshairTargetObject_Name;
					Crosshair [0].color = Color.green;
					Crosshair [1].color = Color.green;
					Crosshair [2].color = Color.green;
					Crosshair [3].color = Color.green;
				}
			}else{
				if(txtName.text != ""){
					txtName.text = "";
					Crosshair [0].color = Color.green;
					Crosshair [1].color = Color.green;
					Crosshair [2].color = Color.green;
					Crosshair [3].color = Color.green;
				}
			}

		}
    }
    
}
