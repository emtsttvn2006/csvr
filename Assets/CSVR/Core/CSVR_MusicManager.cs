﻿using UnityEngine;
using System.Collections;

public class CSVR_MusicManager : MonoBehaviour {
	
	public static CSVR_MusicManager instance;
	
	public AudioClip eff_Popup;
	public AudioClip eff_Error;
	public AudioClip eff_Inv;
	
	public AudioClip home_eff_Start;
	public AudioClip home_ChangeCharacter;
	public AudioClip home_CharacterInfo;
	public AudioClip home_WeaponInfo;
	public AudioClip home_UpgradeWeapon;
	public AudioClip home_Bag;
	public AudioClip home_ChatClick;
	public AudioClip home_ChatNew;
	public AudioClip home_Chest;
	public AudioClip home_Shop;
	public AudioClip home_Back;
	public AudioClip home_Menu;
	public AudioClip home_TabGroup;
	
	public AudioClip mode_Map;
	
	public AudioClip room_KickOut;
	public AudioClip room_Fast;
	
	public AudioClip shop_BuySuccess;
	public AudioClip shop_BuyEffect;
	public AudioClip shop_BuySFaile;

	public AudioClip shop_Add;
	
	public AudioClip other_LevelUp;
	//29.03.2016
	public AudioClip inv_UpSuccess;
	public AudioClip inv_UpSFaile;
	
	
	
	public AudioClip menuClip;
	public AudioClip modeClip;
	
	public AudioClip inHomeClip;
	public AudioClip changeGroupClip;
	public AudioClip xuatkichClip;
	public AudioClip selectClip;
	public AudioClip selectMapClip;
	public AudioClip selectModeClip;
	public AudioClip touchClip;
	
	
	public AudioClip buyClip;
	public AudioClip extendSuccessClip;
	public AudioClip extendFaileClip;
	
	public AudioClip warringClip;
	public AudioSource musicSource;
	public AudioSource efxSource;
	/// <summary>
	/// Initialization method.
	/// Gets data from PlayerPrefs, prevents GameObject destruction, initializes static reference.
	/// </summary>
	void Awake()
	{
		instance = this;
		DontDestroyOnLoad(this.gameObject);
		
	}
	
}
