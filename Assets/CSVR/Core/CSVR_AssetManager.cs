﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum CSVR_GameSide{
	SIDE_POLICE,
	SIDE_ROBBER
}
public class CSVR_AssetManager : MonoBehaviour {

	public static CSVR_AssetManager instance;

	public  GameObject StandardCharacter_Local;
	public  GameObject StandardCharacter_Remote;

	public string[] itemIDs;
	//public string[] itemNames;
	//public Sprite[] itemIconShop;
	//public Sprite[] itemIconGame;
	public vp_ItemType[] itemTypes;
    public vp_UnitType[] itemUnit;

	//public static Dictionary<string,Sprite> itemIconDic;
    //public static Dictionary<string, Sprite> itemIconWeaponGame;
	//public static Dictionary<string,string> itemNameDic;
//	public static Endgame.ImageList itemIconList;
	public static Dictionary<string,vp_ItemType> itemTypeDic;
    public static Dictionary<string, vp_UnitType> itemTypeBullet;
	//public  string[] basicItemIDs; //Everyone has basic items, dont have to buy


	//public  string[] mapNames;

//	public string[] playerTypeNames;
//	public GameObject[] playerOfflineTypes;
//	public vp_MPPlayerType[] playerOnlineTypes;
//	public CSVR_GameSide[] playerSides;

//	public static Dictionary<string,GameObject> playerOfflineTypeDic;
//	public static Dictionary<string,vp_MPPlayerType> playerOnlineTypeDic;
//	public static Dictionary<string,CSVR_GameSide> playerSideDic;
//	public Sprite avatarPlayer;
//	public Texture2D avatarPlayertex;
		// Use this fơeor initialization
	void Awake () {
		instance = this;
        UnityEngine.Object[] items = Resources.LoadAll("ItemTypes/Weapons");
        UnityEngine.Object[] itemsAmmo = Resources.LoadAll("ItemTypes/Ammo");
        itemIDs=new string[items.Length];
        itemTypes=new vp_ItemType[items.Length];
        itemUnit= new vp_UnitType[itemsAmmo.Length];
        for (int i = 0; i < items.Length; i++) {
            itemIDs [i] = items [i].name;
            itemTypes[i]=(vp_ItemType)items[i];
        }
        for (int j = 0; j < itemsAmmo.Length; j++) {
            itemUnit[j]=(vp_UnitType)itemsAmmo[j];
        }
       
		//itemNameDic = new Dictionary<string,string> ();
		itemTypeDic = new Dictionary<string,vp_ItemType> ();
		//itemIconDic = new Dictionary<string,Sprite>();
        //itemIconWeaponGame = new Dictionary<string, Sprite>();
        itemTypeBullet = new Dictionary<string, vp_UnitType>();
		for (int i = 0; i < itemIDs.Length; i++ ) {
			itemTypeDic.Add(itemIDs[i], itemTypes[i]);
		}
        for (int i = 0; i < itemIDs.Length; i++)
        {
            itemTypeBullet.Add(itemIDs[i]+"Bullet", itemUnit[i]);
        }



//		playerOfflineTypeDic = new  Dictionary<string,GameObject> ();
//		playerOnlineTypeDic = new  Dictionary<string,vp_MPPlayerType> ();
//		playerSideDic = new Dictionary<string, CSVR_GameSide>();

//		for (int i = 0; i < playerTypeNames.Length; i++ ) {		
//			playerOfflineTypeDic.Add(playerTypeNames[i], playerOfflineTypes[i]);
//			playerOnlineTypeDic.Add(playerTypeNames[i], playerOnlineTypes[i]);
//			playerSideDic.Add(playerTypeNames[i], playerSides[i]);
//		}
	}
	/*
	void OnLevelWasLoaded(int levelIndex) {
		if (SceneManager.GetActiveScene().name == "Home") {
			//Load nhan vat mau
			if (StandardCharacter_Local == null) {
				StandardCharacter_Local = Instantiate (Resources.Load ("StandardCharacter/StandardCharacter_Local", typeof(GameObject))) as GameObject;
				StandardCharacter_Local.transform.SetParent (transform);
			}
			if (StandardCharacter_Remote == null) {
				StandardCharacter_Remote = Instantiate(Resources.Load("StandardCharacter/StandardCharacter_Remote", typeof(GameObject))) as GameObject;
				StandardCharacter_Remote.transform.SetParent (transform);
			}
		}
	}*/
}
