using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public class LanguagePack
{
	private List<string> stringKeys = new List<string>();
	public List<string> Keys { get { return stringKeys; } }
	
	private List<string> strings = new List<string>();
	public List<string> Strings { get { return strings; } }
	
	public bool AddNewString(string key, string text)
	{
		if(!stringKeys.Contains(key))
		{
			this.stringKeys.Add(key);
			this.strings.Add(text);
			return true;
		}
		else
		{
			
			return false;
		}
	}
	
	public void RemoveString(string key)
	{
		int index = stringKeys.IndexOf(key);
		strings.RemoveAt(index);
		stringKeys.RemoveAt(index);
	}
	
	public string GetString(string key)
	{
		if(stringKeys.Contains(key))
		{
			return strings[stringKeys.IndexOf(key)];
		}
		else
		{
			//Debug.LogWarning("The Key: \""+key+"\" was not found...");
			return "BAD KEY";
		}
	}
}

public enum Language {
	English,
	Vietnamese
}


public static class LanguageManager
{
	public static bool PackLoaded = false;
	public static Language CurrentLanguage = Language.English;
	private static LanguagePack loadedLanguagePack = null;
	public static LanguagePack CurrentLanguagePack { get { return loadedLanguagePack; } }
	public static string LanguagePackDirectory = "/CSVR/Core/Localization/Languages/";
	
	static string buffer = "";
	//static XmlSerializer serializer;
	
	public static string GetText(string key)
	{
		if(PackLoaded)
		{
			buffer = loadedLanguagePack.GetString(key);
			buffer = buffer.Replace("#n", System.Environment.NewLine);
			
			return buffer;
		}
		else
		{
			return "INIT FAIL";
		}
	}

	
	public static void LoadLanguageFile(Language language)
	{
		loadedLanguagePack = LoadToPack(language);
	}
	
	public static void CreateTemplateFile(Language language)
	{		
		LoadLanguageFile(language);
	}
	
	public static LanguagePack LoadToPack(Language language)
	{
		CurrentLanguage = language;
		
		try
		{
			string loadName = System.Enum.GetName(typeof(Language), language);
			string filePath = LanguagePackDirectory + loadName;

			LanguagePack pk = new LanguagePack();
			
			// Read in the STRING contents of xmlFile (TextAsset)	
			TextAsset xmlFile;
			string t;
		
			StreamReader sr = new StreamReader(Application.dataPath + filePath + ".txt");
			t = sr.ReadToEnd();
			sr.Close();

			
			using(StringReader f = new StringReader(t))
			{
				string line;
				while((line = f.ReadLine()) != null)
				{
					try
					{
						pk.AddNewString((string)(line.Split(new string[] { " = " }, System.StringSplitOptions.None)[0]), line.Split(new string[] { " = " }, System.StringSplitOptions.None)[1]);
						//Debug.Log(line);
					} 
					catch 
					{

					}
				}	
			}
			
			PackLoaded = true;
			
			return pk;
		} catch {
			
			PackLoaded = false;
			
			return null;
		}
	}

}
