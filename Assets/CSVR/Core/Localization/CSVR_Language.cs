//using UnityEngine;
//using System.Collections;
//using UnityEngine.UI;
//
//public class CSVR_Language : MonoBehaviour 
//{
//	public Language defaultLanguage = Language.English;
//	//UI Login
//	[SerializeField]
//	Text	UI_Login_username,UI_Login_password,UI_Login_login,UI_Login_loginwithfacebook,UI_Login_register;
//	//UI Register
//	[SerializeField]
//	Text	UI_Register_register,UI_Register_name,UI_Register_password,UI_Register_repassword,UI_Register_email,UI_Register_agreewithCSVR,UI_Register_back,UI_Register_create;
//	//UI Home
//	[SerializeField]
//	Text	UI_Home_event,UI_Home_inventory,UI_Home_shop;
//	[SerializeField]
//	Text UI_Home_reward_1,UI_Home_reward_2,UI_Home_reward_3,UI_Home_reward_4;
//	[SerializeField]
//	Text UI_Home_unlockat_5,UI_Home_unlockat_10,UI_Home_unlockat_15,UI_Home_unlockat_20;
//	//UI Home Group
//	[SerializeField]
//	Text	UI_Home_Group_1_armor,UI_Home_Group_1_mele,UI_Home_Group_1_maingun,UI_Home_Group_1_pistol,UI_Home_Group_1_grenade;
//	[SerializeField]
//	Text	UI_Home_Group_2_armor,UI_Home_Group_2_mele,UI_Home_Group_2_maingun,UI_Home_Group_2_pistol,UI_Home_Group_2_grenade;
//	[SerializeField]
//	Text	UI_Home_Group_3_armor,UI_Home_Group_3_mele,UI_Home_Group_3_maingun,UI_Home_Group_3_pistol,UI_Home_Group_3_grenade;
//	
//	private int prevSel = 0;
//	private int selection = 0;
//	private string[] selections;
//	
//	void Start()
//	{
//		// Initialize the language manager with English language
//		LanguageManager.LoadLanguageFile(defaultLanguage);
//		selections = new string[] { LanguageManager.GetText("english"), LanguageManager.GetText("vietnamese")};
//		ChangeLanguageText();
//	}
//
//	public void VNLanguage()
//	{
//		selection = 1;
//		ChangeLanguage();
//	}
//	public void ENLanguage()
//	{
//		selection = 0;
//		ChangeLanguage();
//	}
//	private void ChangeLanguage()
//	{
//		if(prevSel != selection)
//		{
//			prevSel = selection;
//			
//			switch(selection)
//			{
//			case 0:
//				LanguageManager.LoadLanguageFile(Language.English);
//				break;
//			case 1:
//				LanguageManager.LoadLanguageFile(Language.Vietnamese);
//				break;
//			}
////			Debug.Log("switch language");
//			// In this instance we want the buttons to change there text to display in the selected language, so again we use the "LanguageManager.GetText(string key)" method
//			selections = new string[] { LanguageManager.GetText("english"), LanguageManager.GetText("vietnamese")};
//			ChangeLanguageText();
//		}
//	}
//	public void ChangeLanguageText()
//	{
//		//UI Login
//		UI_Login_username.text = LanguageManager.GetText("username");
//		UI_Login_password.text = LanguageManager.GetText("password");
//		UI_Login_login.text = LanguageManager.GetText("login");
//		UI_Login_loginwithfacebook.text = LanguageManager.GetText("loginwithfacebook");
//		UI_Login_register.text = LanguageManager.GetText("register");
//
//		//UI Register
//		UI_Register_register.text = LanguageManager.GetText("register");
//		UI_Register_name.text = LanguageManager.GetText("username");
//		UI_Register_password.text = LanguageManager.GetText("password");
//		UI_Register_repassword.text = LanguageManager.GetText("repassword");
//		UI_Register_email.text = LanguageManager.GetText("email");
//		UI_Register_agreewithCSVR.text = LanguageManager.GetText("agreewithCSVR");
//		UI_Register_back.text = LanguageManager.GetText("back");
//		UI_Register_create.text = LanguageManager.GetText("create");
//
//		//UI Home
//		UI_Home_event.text = LanguageManager.GetText("event");
//		UI_Home_inventory.text = LanguageManager.GetText("inventory");
//		UI_Home_shop.text = LanguageManager.GetText("shop");
//		UI_Home_reward_1.text = UI_Home_reward_2.text = UI_Home_reward_3.text = UI_Home_reward_4.text = LanguageManager.GetText("reward");
//		UI_Home_unlockat_5.text = LanguageManager.GetText("unlockat5");
//		UI_Home_unlockat_10.text = LanguageManager.GetText("unlockat10");
//		UI_Home_unlockat_15.text = LanguageManager.GetText("unlockat15");
//		UI_Home_unlockat_20.text = LanguageManager.GetText("unlockat20");
//		//UI Home inventory
//		UI_Home_Group_1_armor.text = UI_Home_Group_2_armor.text = UI_Home_Group_3_armor.text = LanguageManager.GetText("armor");
//		UI_Home_Group_1_mele.text = UI_Home_Group_2_mele.text = UI_Home_Group_3_mele.text = LanguageManager.GetText("mele");
//		UI_Home_Group_1_maingun.text = 	UI_Home_Group_2_maingun.text = 	UI_Home_Group_3_maingun.text = LanguageManager.GetText("maingun");
//		UI_Home_Group_1_pistol.text = UI_Home_Group_2_pistol.text = UI_Home_Group_3_pistol.text = LanguageManager.GetText("pistol");
//		UI_Home_Group_1_grenade.text = UI_Home_Group_2_grenade.text = UI_Home_Group_3_grenade.text = LanguageManager.GetText("grenade");
//
//	}
//
//}
