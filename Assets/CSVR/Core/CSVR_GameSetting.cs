﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Horus;
using Horus.ClientModels;
using Horus.Internal;



public enum CSVR_ViewMode{
	VIEW_NONE,
	VIEW_2D_MODE,
	VIEW_VR_MODE
}

public enum CSVR_ControlMode{		
	CONTROL_TOUCH,
	CONTROL_GAMEPAD,
	CONTROL_MOUSE, 
	CONTROL_HORUS
}

public enum CSVR_NetworkMode{		
	ONLINE,
	LAN,
	OFFLINE
}

public enum CSVR_GameMode{		
	ONLINEBASIC,
	MULTITEAM,
	DEATHMATCH,
	SINGLE
}

public static class TransformEx {
	public static Transform Clear(this Transform transform)
	{
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
		return transform;
	}
	public static Transform ClearLogOut(this Transform transform)
	{
		//		for(int i = 0;i < transform.childCount - 1;i++)
		//		{
		//			GameObject.Destroy(transform);
		//		}
		//		Debug.Log()
		int i = 1,lenght = transform.childCount;
		foreach (Transform child in transform) {
			if(i != lenght)
				GameObject.Destroy(child.gameObject);
			
			i++;
		}
		return transform;
	}
}


public class CSVR_GameSetting : MonoBehaviour {

    public static string currentPlayerTypeName = "11111_999_Swat"; //Ten cua nhan vat se dung trong game

	public static bool AI_Enable = true;
	public static int AI_Police = 2;
	public static int AI_Terrorist = 2;
	public static List<CSVR_UIAIInfo> AI_InGame = new List<CSVR_UIAIInfo>();

	public static byte errorCountCallBack = 3;
	public static int accountBaned = 0;
	//begin setting view
	public static float TayCamSung;
	//end setting view

	public static string LastLogin;
	//	public static bool isLoggedIn = false;
	//	public static bool isFirstPeron = true;
	public static bool isAutoLogin = false;
	public static string username = "";
	public static string password = "";
	public static int StateControl=0;
	//	public static int loginType;
	
	//	public   const int NOSAVED_LOGIN = 0;
	//	public   const int PLAYFAB_LOGIN = 1;
	//	public   const int FACEBOOK_LOGIN = 2;
	//	public   const int GOOGLE_LOGIN = 3;
	
	public static CSVR_NetworkMode networkMode = CSVR_NetworkMode.OFFLINE;
	
	public static CSVR_GameMode gameMode = CSVR_GameMode.SINGLE;
	
	//View settings
	public static CSVR_ViewMode viewMode = CSVR_ViewMode.VIEW_2D_MODE;
	
	//Input setting
	public const string HORUS_RESET_KEY = "`";
	public const string HORUS_TRIGGER_KEY = "-";
	
	public static CSVR_ControlMode controlMode = CSVR_ControlMode.CONTROL_TOUCH;
	
	public static float gyroScale = 1.0f; //1.0 is the minimum
	public static float lookSensitivityScale { 
		get {
			float temp = 0;
			switch (CSVR_GameSetting.controlMode) {
			case CSVR_ControlMode.CONTROL_TOUCH:
				//temp =  lookSensitivityScale_Raw*1.5f;
				temp =  lookSensitivityScale_Raw*lookSensitivitySpeed*2;
				break;
			case CSVR_ControlMode.CONTROL_GAMEPAD:
				temp =  lookSensitivityScale_Raw;
				break;
			default:
				temp =  lookSensitivityScale_Raw;
				break;
				
			}
			return temp;
		}
		
	}
    public static bool showDebug = true;
    public static void DebugLog(string str)
    {
        if (showDebug) {
            Debug.Log (str);
        }
        
    }
	public static float music = 1;
	public static float soundEfx = 1;
	
	public static float normalFOV = 60f;
	public static float zoomFOV = 30f;
	
	public static float lookSensitivityScale_Raw = 0.25f;
	
	public static float gyroYawAbsolute = 0;
	public static float gyroPitchAbsolute = 0;
	public static Quaternion gyroRotAbsolute = Quaternion.identity;
	
	public static float gyroYawRelative = 0;
	public static float gyroPitchRelative = 0;
	
	public static bool gyroEnabled = true;
	public static float lookSensitivitySpeed = 0.5f;
	public static float zoomSensitivitySpeed = 0.5f;
	
	public static bool AutoTargetEnabled = true;
	
	public static float lookSensitivitySpeedZoom = 1;
	
	static int[] _experienceForEveryLevel = new int[] { 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 
		100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 
		200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 
		300, 310, 320, 330, 340, 350, 360, 370, 380, 390,
		400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 
		500, 510, 520, 530, 540, 550, 560, 570, 850, 590, 
		600, 610, 620, 630, 640, 650, 660, 670, 680, 690, 
		700, 710, 720, 730, 740, 750, 760, 770, 780, 790, 
		800, 810, 820, 830, 840, 850, 860, 870, 880, 890, 
		900, 910, 920, 930, 940, 950, 960, 970, 980, 990, 
		1000};
	public static int[] experienceForEveryLevel
	{
		get { return _experienceForEveryLevel; }
	}
	static string[] _experienceForEveryLevelName = new string[] { "Học viên", "Học viên 1", "Học viên 2", "Học viên 3", "Học viên 4", "Học viên 5",
		"Binh nhì ","Binh nhì 1", "Binh nhì 2", "Binh nhì 3", "Binh nhì 4",
		"Binh nhất ","Binh nhất 1", "Binh nhất 2", "Binh nhất 3", "Binh nhất 4", 
		"Hạ sĩ", "Hạ sĩ 1", "Hạ sĩ 2", "Hạ sĩ 3", "Hạ sĩ 4", 
		"Trung sĩ", "Trung sĩ 1", "Trung sĩ 2", "Trung sĩ 3", "Trung sĩ 4", 
		"Trung sĩ tham mưu","Trung sĩ tham mưu 1", "Trung sĩ tham mưu 2", "Trung sĩ tham mưu 3", "Trung sĩ tham mưu 4", 
		"Trung sĩ nhất","Trung sĩ nhất 1", "Trung sĩ nhất 2", "Trung sĩ nhất 3", "Trung sĩ nhất 4",
		"Thượng sĩ","Thượng sĩ 1", "Thượng sĩ 2", "Thượng sĩ 3", "Thượng sĩ 4",
		"Thiếu úy","Thiếu úy 1", "Thiếu úy 2", "Thiếu úy 3", "Thiếu úy 4",
		"Trung úy","Trung úy 1", "TTrung úy 2", "Trung úy 3", "Trung úy 4",
		"Đại úy","Đại úy 1", "Đại úy 2", "Đại úy 3", "Đại úy 4",
		"Thiếu tá","Thiếu tá 1", "Thiếu tá 2", "Thiếu tá 3", "Thiếu tá 4",
		"Trung tá","Trung tá 1", "Trung tá 2", "Trung tá 3", "Trung tá 4",
		"Đại tá","Đại tá 1", "Đại tá 2", "Đại tá 3", "Đại tá 4",
		"Chuẩn tướng","Chuẩn tướng 1", "Chuẩn tướng 2", "Chuẩn tướng 3", "Chuẩn tướng 4",
		"Thiếu tướng","Thiếu tướng 1", "Thiếu tướng 2", "Thiếu tướng 3", "Thiếu tướng 4",
		"Trung tướng","Trung tướng 1", "Trung tướng 2", "Trung tướng 3", "Trung tướng 4",
		"Đại tướng","Đại tướng 1", "Đại tướng 2", "Đại tướng 3", "Đại tướng 4",
		"Nguyên soái","Nguyên soái 1", "Nguyên soái 2", "Nguyên soái 3", "Nguyên soái 4"};
	public static string[] experienceForEveryLevelName
	{
		get { return _experienceForEveryLevelName; }
	}
	public static string accountStartDay;
	public static int accountLevel =0;
	public static int accountExp =0 ;
	public static int accountWin =0;
	public static int accountLost =0 ;
	public static int accountKill =0 ;
	public static int accountDead =0;
	public static int accountHeadShot =0 ;
	public static int accountRevenge =0 ;
	public static int accountRating = 0;
	//Game Constants
	public static string accountLevelKey = "AccountLevel";
	public static string accountExpKey = "AccountExp";
	public static string accountWinKey = "AccountWin";
	public static string accountLostKey = "AccountLost";
	public static string accountKillKey = "AccountKill";
	public static string accountDeadKey = "AccountDead";
	public static string accountHeadShotKey = "AccountHeadShot";
	public static string accountRevengeKey = "AccountRevenge";
	public static string accountRatingKey = "AccountRating";
	//AI Setting
	//	public static float AIRespawnDelay = 5;
	//
	public static string mainGun = "77_999_Knife";
	//game setting
	//	public static string playerType = ""; 
	//	public static string gameMap = ""; 
	//	public static RoomInfo[] roomList;
	//	public static uint roomListVersion = 0;
	public static bool TuTaoPhong = false;
	//public static bool UseRoomVIP = false;
	public static bool UseRoomVIP = false;
	public static bool IsQuickJoinedRoom = false;
	public static float RoomVIPCloseTimeout = 20.0f;

	static string[] _roomName = new string[] { "Team Watery Server", "24/7 Deathmatch", "BestClanEver - Beginners only", "ESL Server 1", "ESL Server 2", "Sky Arena Developers",
		"Test Server", "Yay my first server" };
	public static string[] roomName
	{
		get { return _roomName; }
	}

	public static string hostName = "";
	public static string modeName = "CSVR_TeamDeathmatch";
	public static string mapName = "Map_Ngatu";
	public static int IDPlayMode = 1, maxPlayer = 11, timeMatch = 300, maxSkill= 30, timeRepawn = 3, maxFlag, IDMode = 100;
	public static int selectPlayModeValue = 0, selectPlayerValue = 0, selectMaxSkillValue = 0, selectTimeMatchValue = 0, selectTimeRepawnValue = 3, selectModeGameValue = 1, selectMapGameValue = 1, selectModeGameValueTemp = 1, selectMapGameValueTemp = 1;
	
	//mode search and destroy(C4)
	public static int maxWin = 5;
	public static int policeWin = 0;
	public static int terroristWin = 0;
	//mode basecapture (cu diem)
	public static int BCGoalPoints = 1000;
	//	public static int BCTeam1Scores = 0;
	//	public static int BCTeam2Scores = 0;
	
	//roomvip
	public static string UserRequestRoom;

	//Shop 
	public static List<CatalogItem> shopCharacter;
	public static List<CatalogItem> shopAccessories;
	public static List<CatalogItem> shopItems;
	public static List<CatalogItem> shopItemsPeriod;
	public static List<CatalogItem> upgradeItems;



	public static List<CatalogItem> shopMainGun;
	public static List<CatalogItem> shopPistolGun;
	public static List<CatalogItem> shopMeleGun;
	public static List<CatalogItem> shopArmorGun;
	public static List<CatalogItem> shopGrenadeGun;


//	public static List<CatalogItem> shopACMuzzle;
//	public static List<CatalogItem> shopACGrip;
//	public static List<CatalogItem> shopACMagazine;
//	public static List<CatalogItem> shopACScope;

	public static List<CatalogItem> shopBag;
	public static byte bagCount = 2;
	public static byte bagStartIndex = 0; //Index cua balo đầu tiên khi vào game
	public static byte bagCurrentIndex = 0; //Index cua balo hien tai
	public static float bagSwitchEnableRadius = 10f;// Bán kính được phép bật nút chuyển balo

	public static Dictionary<string, CSVR_FriendInfo> friendInfos = new Dictionary<string, CSVR_FriendInfo>();
	//Inventory
	public static Dictionary<string,int> virtualCurrency; 
	//	public static List<ItemInstance> inventoryItems;
	//public static List<ItemInstance> inventoryItemsNew = new List<ItemInstance>();
	
	public static List<PlayFab.ClientModels.FriendInfo>  friends;
	public static List<string>  friendsListName = new List<string> ();

	public static List<RelationshipInfo>  relationship;


	public static CSVR_GunInfo FindGunInfoInUpgradeItems(string itemId){
		for(int i = 0;i < upgradeItems.Count;i++){
			if(upgradeItems[i].ItemId == itemId)
			{
				if(upgradeItems[i].CustomData != null)
				{
					CSVR_GunInfo gun = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GunInfo>(upgradeItems[i].CustomData);
					
					return gun;
				}
			}
		}
		return null;
	}
	
	public static CSVR_MeleInfo FindMeleeInfoInUpgradeItems(string itemId){
		for(int i = 0;i < upgradeItems.Count;i++){
			if(upgradeItems[i].ItemId == itemId)
			{
				if(upgradeItems[i].CustomData != null)
				{
					CSVR_MeleInfo gun = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_MeleInfo>(upgradeItems[i].CustomData);
					
					return gun;
				}
			}
		}
		return null;
	}

	public static CatalogItem FindItemInListShopItems(List<CatalogItem> list,string itemId)
	{
		for(int i = 0;i < list.Count;i++){
			if(list[i].ItemId == itemId)
				return list[i];
		}
		return null;
	}

	public static void SaveSetting() {
		
		PlayerPrefs.SetString ("CSVR_GameSetting.username", username);
		PlayerPrefs.SetString ("CSVR_GameSetting.password", password);
		PlayerPrefs.SetInt("CSVR_GameSetting.viewMode",(int) CSVR_GameSetting.viewMode);
		//PlayerPrefs.SetInt("CSVR_GameSetting.viewMode",(int)CSVR_ViewMode.VIEW_2D_MODE);
		PlayerPrefs.SetInt("CSVR_GameSetting.controlMode",(int) CSVR_GameSetting.controlMode);
		
		
		PlayerPrefs.SetString("CSVR_GameSetting.gyroEnabled",gyroEnabled.ToString());
		PlayerPrefs.SetFloat("CSVR_GameSetting.gyroScale",gyroScale);
		PlayerPrefs.SetFloat("CSVR_GameSetting.lookSensitivitySpeed",lookSensitivitySpeed);
		
		PlayerPrefs.SetFloat("CSVR_GameSetting.music",music);
		PlayerPrefs.SetFloat("CSVR_GameSetting.soundEfx",soundEfx);
		
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis0", CSVR_HorusInput.mappedAxis[0]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis1", CSVR_HorusInput.mappedAxis[1]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis2", CSVR_HorusInput.mappedAxis[2]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis3", CSVR_HorusInput.mappedAxis[3]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis4", CSVR_HorusInput.mappedAxis[4]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis5", CSVR_HorusInput.mappedAxis[5]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis6", CSVR_HorusInput.mappedAxis[6]);
		PlayerPrefs.SetString ("CSVR_HorusInput.mappedAxis7", CSVR_HorusInput.mappedAxis[7]);
	}
	
	
	public static void LoadSetting() {
		try{
			isAutoLogin = bool.Parse(PlayerPrefs.GetString("CSVR_GameSetting.isAutoLogin"));
		}catch{
			isAutoLogin = false;
		}
		username = PlayerPrefs.GetString ("CSVR_GameSetting.username");
		password = PlayerPrefs.GetString ("CSVR_GameSetting.password");
		//CSVR_UIMessageText.instance.SetAlphaText("Login with " + username);
		if(PlayerPrefs.HasKey("CSVR_GameSetting.viewMode"))				
		{		
			CSVR_GameSetting.viewMode = (CSVR_ViewMode) PlayerPrefs.GetInt("CSVR_GameSetting.viewMode");
		} 
		
		if(PlayerPrefs.HasKey("CSVR_GameSetting.controlMode"))				
		{		
			CSVR_GameSetting.controlMode = (CSVR_ControlMode) PlayerPrefs.GetInt("CSVR_GameSetting.controlMode");
		} 
		
		if(PlayerPrefs.HasKey("CSVR_GameSetting.gyroEnabled"))				
		{		
			gyroEnabled = bool.Parse(PlayerPrefs.GetString("CSVR_GameSetting.gyroEnabled"));
			gyroScale = PlayerPrefs.GetFloat("CSVR_GameSetting.gyroScale");
			
		} 
		
		lookSensitivitySpeed = PlayerPrefs.GetFloat("CSVR_GameSetting.lookSensitivitySpeed");
		
		music = PlayerPrefs.GetFloat("CSVR_GameSetting.music");
		
		CSVR_HorusInput.mappedAxis[0] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis0", "Joy Axis 1");
		CSVR_HorusInput.mappedAxis[1] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis1", "Joy Axis 2");
		CSVR_HorusInput.mappedAxis[2] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis2", "Joy Axis 3");
		CSVR_HorusInput.mappedAxis[3] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis3", "Joy Axis 4");
		CSVR_HorusInput.mappedAxis[4] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis4", "Joy Axis 5");
		CSVR_HorusInput.mappedAxis[5] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis5", "Joy Axis 6");
		CSVR_HorusInput.mappedAxis[6] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis6", "Joy Axis 7");
		CSVR_HorusInput.mappedAxis[7] = PlayerPrefs.GetString ("CSVR_HorusInput.mappedAxis7", "Joy Axis 8");
		
	}
	
}
