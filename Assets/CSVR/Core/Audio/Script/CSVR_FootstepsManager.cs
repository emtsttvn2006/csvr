﻿using UnityEngine;


[System.Serializable]
public struct FootstepRig {
	public Transform foot;
	public Vector3 offset;
	public float radius;
}



//[RequireComponent(typeof(AudioSource))]
public class CSVR_FootstepsManager : MonoBehaviour {
	
	
	AudioSource src;
	public AudioClip sound;

	//Config tiếng bước chân
	float MaxDistanceFootAudio = 30;
	float KhoangCachThoiGianMin = 1f;
	float ThoiGianLanTruoc = 0f;
	public float SaiChanToiDa = 1.2f;
	public float TocDoKhongTieng = 0.5f;
	float SaiChanHienTai = 0f;
	bool PrevGrounded = false;

	protected vp_Controller m_Controller = null;
	protected vp_Controller Controller
	{
		get
		{
			if (m_Controller == null)
			{
				m_Controller = transform.root.GetComponent<vp_Controller>();
			}
			return m_Controller;
		}
	}


	void Start() {
		//Tao 1 object rieng, la child cua nhan vat
		GameObject FootAudio = new GameObject ();
		FootAudio.name = "FootAudio";
		FootAudio.transform.parent = gameObject.transform;
		FootAudio.transform.localPosition = Vector3.zero;

		src = FootAudio.AddComponent<AudioSource>();
		src.volume = 0.15f;
		src.rolloffMode = AudioRolloffMode.Linear;
		src.maxDistance = MaxDistanceFootAudio;
		src.minDistance = 0f;
		src.spatialBlend = 1.0f; 
		src.clip = sound;
	}



	
	public void Update() {
		#if !VIRTUAL_ROOM
		if (Controller.Grounded) {
			if ( Controller.m_Velocity.magnitude  > TocDoKhongTieng) {
				if (SaiChanHienTai > SaiChanToiDa) {
					src.Stop();
					src.Play();
					SaiChanHienTai = 0f;


				} else {
					
					SaiChanHienTai += Time.deltaTime * Controller.m_Velocity.magnitude;
				}
			} 
			if (PrevGrounded == false) {
				{
					if ((Time.time - ThoiGianLanTruoc) > KhoangCachThoiGianMin) 
					{
						src.clip = sound;
						src.Stop();
						src.Play();
						SaiChanHienTai = 0f;
						ThoiGianLanTruoc = Time.time;
					}

				}
			}



		} 


		PrevGrounded = Controller.Grounded;
		#endif

	}
}

