﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


	public class CSVR_HorusInput : MonoBehaviour {
		public static CSVR_HorusInput instance;
		
		public static Quaternion controllerRotation;
		public  Quaternion controllerRotationOffset = Quaternion.identity;

		public static bool toCalibrate = false;	
		
		
		public float x1,y1,z1,w1,x2,y2,z2,w2;

		public float[] axisVals = new float[20];
		public static string[] mappedAxis = new string[8]; 
		//public Transform testObject;
		
		void Awake() {
			instance = this;
			//mappedAxis = new string[]{"Joy Axis 1", "Joy Axis 2", "Joy Axis 3", "Joy Axis 4", "Joy Axis 5", "Joy Axis 6", "Joy Axis 7", "Joy Axis 8"};
		}
		// Use this for initialization
		void Start () {
			//calibrate ();
			controllerRotationOffset = Quaternion.identity;
		}
	/*	
		//ToDO: Do we need phoneRotation?
		// Update is called once per frame
		void LateUpdate () {
			
			
			
			//	if ((mhr_Settings.controlMode == mhr_Settings.CONTROL_HORUS) && (mhr_Settings.viewMode == mhr_Settings.VIEW_3D_MODE)) {
			if ((CSVR_GameSetting.controlMode == CSVR_ControlMode.CONTROL_HORUS)) {


				//If in the axis identification mode
				if (Input.GetKey(CSVR_GameSetting.HORUS_RESET_KEY) && Input.GetKey(CSVR_GameSetting.HORUS_TRIGGER_KEY)) {
					


					axisVals[0] = Input.GetAxisRaw("Joy Axis 1");
					axisVals[1] = Input.GetAxisRaw("Joy Axis 2");
					axisVals[2] = Input.GetAxisRaw("Joy Axis 3");
					axisVals[3] = Input.GetAxisRaw("Joy Axis 4");
					axisVals[4] = Input.GetAxisRaw("Joy Axis 5");
					axisVals[5] = Input.GetAxisRaw("Joy Axis 6");
					axisVals[6] = Input.GetAxisRaw("Joy Axis 7");
					axisVals[7] = Input.GetAxisRaw("Joy Axis 8");
					axisVals[8] = Input.GetAxisRaw("Joy Axis 9");
					axisVals[9] = Input.GetAxisRaw("Joy Axis 10");
					axisVals[10] = Input.GetAxisRaw("Joy Axis 11");
					axisVals[11] = Input.GetAxisRaw("Joy Axis 12");
					axisVals[12] = Input.GetAxisRaw("Joy Axis 13");
					axisVals[13] = Input.GetAxisRaw("Joy Axis 14");
					axisVals[14] = Input.GetAxisRaw("Joy Axis 15");
					axisVals[15] = Input.GetAxisRaw("Joy Axis 16");
					//axisVals[16] = Input.GetAxisRaw("Joy Axis 17");
					//axisVals[17] = Input.GetAxisRaw("Joy Axis 18");
					//axisVals[18] = Input.GetAxisRaw("Joy Axis 19");
					//axisVals[19] = Input.GetAxisRaw("Joy Axis 20");

			//	Debug.Log(axisVals[1]);
			//	Debug.Log(axisVals[19]);

					//Mark the axes with no input
					for (int i = 0; i< 20; i++) {
						if (axisVals[i] == 0f) {
							axisVals[i] = 10f; //Make it realy high
						}
					}

					Array.Sort<float>(axisVals);

					for (int i = 0; i< 8; i++) {
						if (axisVals[i] == Input.GetAxisRaw("Joy Axis 1")) {
							mappedAxis[i] =  "Joy Axis 1";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 2")) {
							mappedAxis[i] =  "Joy Axis 2";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 3")) {
							mappedAxis[i] =  "Joy Axis 3";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 4")) {
							mappedAxis[i] =  "Joy Axis 4";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 5")) {
							mappedAxis[i] =  "Joy Axis 5";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 6")) {
							mappedAxis[i] =  "Joy Axis 6";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 7")) {
							mappedAxis[i] =  "Joy Axis 7";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 8")){
								mappedAxis[i] =  "Joy Axis 8";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 9")){
							mappedAxis[i] =  "Joy Axis 9";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 10")) {
							mappedAxis[i] =  "Joy Axis 10";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 11")) {
							mappedAxis[i] =  "Joy Axis 11";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 12")) {
							mappedAxis[i] =  "Joy Axis 12";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 13")) {
							mappedAxis[i] =  "Joy Axis 13";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 14")) {
							mappedAxis[i] =  "Joy Axis 14";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 15")) {
							mappedAxis[i] =  "Joy Axis 15";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 16")) {
							mappedAxis[i] =  "Joy Axis 16";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 17")) {
							mappedAxis[i] =  "Joy Axis 17";
						}else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 18")){
							mappedAxis[i] =  "Joy Axis 18";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 19")){
							mappedAxis[i] =  "Joy Axis 19";
						} else if (axisVals[i] == Input.GetAxisRaw("Joy Axis 20")){
							mappedAxis[i] =  "Joy Axis 20";
						}
						
					}

				}
				//If reset
				else if (Input.GetKey(CSVR_GameSetting.HORUS_RESET_KEY)) {
					
					
					controllerRotationOffset = CSVR_GameSetting.gyroRotAbsolute* Quaternion.Inverse(new Quaternion (x1, y1, z1, w1));
					//oculusYawOffset = CSVR.CSVR_Camera.camRotation.eulerAngles.y; //To save the yaw of Oculus when the reset button is pressed
					
				}
				//If normal mode
				else {

//				Debug.Log(mappedAxis[0]);
					x1 = Input.GetAxisRaw(mappedAxis[0]);
					y1 = Input.GetAxisRaw (mappedAxis[1]);
					z1 = Input.GetAxisRaw (mappedAxis[2]);
					w1 = Input.GetAxisRaw (mappedAxis[3]);
					
					x2 = (Input.GetAxisRaw(mappedAxis[4])+1)/64;
					y2 = (Input.GetAxisRaw (mappedAxis[5])+1)/64;
					z2 = (Input.GetAxisRaw (mappedAxis[6])+1)/64;
					w2 = (Input.GetAxisRaw (mappedAxis[7])+1)/64;

					x1 += x2;
					y1 += y2;
					z1 += z2;
					w1 += w2;
					
					
					
					//controllerQuat = weaponGroupRotation.rotation;
					controllerRotation =  controllerRotationOffset * ( new Quaternion (x1, y1, z1, w1) ) ;

					//if (testObject != null) {
						//testObject.rotation = controllerRotation;
					//}
				}



			}
		}
*/
	}
