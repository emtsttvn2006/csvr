﻿using UnityEngine;
using System.Collections;

namespace CSVR {
	public class CSVR_GyroTrack : MonoBehaviour {

//		// Use this for initialization
//		void Start () {
//		
//		}
		
		// Update is called once per frame
		void Update () {
			CSVR_GameSetting.gyroYawRelative = transform.eulerAngles.y - CSVR_GameSetting.gyroYawAbsolute;
			CSVR_GameSetting.gyroYawAbsolute = transform.eulerAngles.y;

			//Check if moving is too fast or the value loops
			if (Mathf.Abs (CSVR_GameSetting.gyroYawRelative) > 90f) {
				CSVR_GameSetting.gyroYawRelative = 0f;
			}

			CSVR_GameSetting.gyroRotAbsolute = transform.rotation;
			CSVR_GameSetting.gyroPitchRelative = transform.eulerAngles.x - CSVR_GameSetting.gyroPitchAbsolute;
			CSVR_GameSetting.gyroPitchAbsolute = transform.eulerAngles.x;
			if (Mathf.Abs (CSVR_GameSetting.gyroPitchRelative) > 90f) {
				CSVR_GameSetting.gyroPitchRelative = 0f;
			}
		}
	}
}
