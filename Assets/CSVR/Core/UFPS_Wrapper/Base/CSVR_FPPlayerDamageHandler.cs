﻿using UnityEngine;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes; 

public class CSVR_FPPlayerDamageHandler : vp_FPPlayerDamageHandler
{

    private float HeathTemp = 0f;
	CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
	CSVR_ServerConnector serverConnectorInstance{
		get {
			if (m_serverConnectorInstance == null) {
				m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
			}
			return m_serverConnectorInstance;
		}
	}
    vp_MPNetworkPlayer m_NetworkPlayer; // Dung de lay thong tin ve nguoi ban
    vp_MPNetworkPlayer m_NetWork
    {
        get
        {
            if (m_NetworkPlayer == null)
            {
                m_NetworkPlayer = transform.root.GetComponent<vp_MPNetworkPlayer>();
            }
            return m_NetworkPlayer;
        }
    } 

    public Animator animController;
    CSVR_PlayerRespawner respawn;
    /// <summary>
    /// registers this component with the event handler (if any)
    /// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();
        animController = GetComponentInChildren<Animator>();
        respawn = GetComponent<CSVR_PlayerRespawner>();

		//Chong cheat
		AntiCheat_CurrentHealth.RandomizeCryptoKey ();

    }

    /// <summary>
    /// 
    /// </summary>

//	public override void Damage(vp_DamageInfo damageInfo)
//    {
//		//Debug.Log ("FP Damage");
//
//		//Chong cheat
//		CurrentHealth = AntiCheat_CurrentHealth;
//
//		//Debug.Log (damageInfo.team);
//		//Debug.Log (serverConnectorInstance.idTeams);
//		// Nếu đây  là master. Để hạn chế lag khiến master khó chết, Damage được gọi qua Receive_HitMaster từ các máy khác
//	//	Debug.Log (damageInfo.Type);
//		if (damageInfo.Type != vp_DamageInfo.DamageType.Fall && damageInfo.Type != vp_DamageInfo.DamageType.Explosion) {
//			if (PhotonNetwork.player.isMasterClient) {
//				return;
//			}
//
//			if (damageInfo.team != 0 && damageInfo.team == serverConnectorInstance.idTeams ) return;
//		}
//
//
//        if (!respawn.RespawnNoActionTimeFinished)
//            return;
//        HeathTemp = CurrentHealth;
//        if (CurrentHealth <= 0.0f)
//            return;
//
//        base.Damage(damageInfo);
//
//
//        //Debug.Log("Trừ máu máu master ");
//        animController.SetTrigger("IsTakeDamage");
//        // Goi ham truyen tin ve cai chet
//        if (CurrentHealth <= 0.0f)
//        {
//            vp_MPMaster.Instance.TransmitDeathInfo(damageInfo.ShooterPlayfabID, damageInfo.ShooterName, damageInfo.GunID,
//                                                   serverConnectorInstance.PlayfabID, serverConnectorInstance.PlayfabName, damageInfo.HitPart, HeathTemp, damageInfo.Level, damageInfo.GunName, damageInfo.team, serverConnectorInstance.idTeams, damageInfo.idphoton, m_NetWork.ID);
//
//            
//            //respawn.KillCameraTarget = damageInfo.Source;
//        }
//    }


	/// <summary>
	/// Kien: cho ca AI va Player
	/// </summary>
	public override void Damage(vp_DamageInfo damageInfo)
	{
		//Debug.Log ("FP Damage");
		//Chong cheat
		CurrentHealth = AntiCheat_CurrentHealth;

		//Debug.Log (damageInfo.team);
		//Debug.Log (serverConnectorInstance.idTeams);
		// Nếu đây  là master. Để hạn chế lag khiến master khó chết, Damage được gọi qua Receive_HitMaster từ các máy khác
		//	Debug.Log (damageInfo.Type);
		//kienbb:neu ma source la AI thi bo qua check master
		if(damageInfo.Source.tag == "TeamAI"){
			if (damageInfo.team != 0 && damageInfo.team == serverConnectorInstance.idTeams ) return;
		}
		else
		//endkien
		if (damageInfo.Type != vp_DamageInfo.DamageType.Fall && damageInfo.Type != vp_DamageInfo.DamageType.Explosion) {
			if (PhotonNetwork.player.isMasterClient) {
				return;
			}
			if (damageInfo.team != 0 && damageInfo.team == serverConnectorInstance.idTeams ) return;
		}
		//kienbb: point de bat dau tinh toan tru mau
		//endkien
		if (!respawn.RespawnNoActionTimeFinished)
			return;
		HeathTemp = CurrentHealth;
		if (CurrentHealth <= 0.0f)
			return;

		base.Damage(damageInfo);


		//Debug.Log("Trừ máu máu master ");
		animController.SetTrigger("IsTakeDamage");
		// Goi ham truyen tin ve cai chet
		if (CurrentHealth <= 0.0f)
		{
			vp_MPMaster.Instance.TransmitDeathInfo(damageInfo.ShooterPlayfabID, damageInfo.ShooterName, damageInfo.GunID,
				serverConnectorInstance.PlayfabID, serverConnectorInstance.PlayfabName,
				damageInfo.HitPart, HeathTemp, damageInfo.Level, damageInfo.GunName,
				damageInfo.team, serverConnectorInstance.idTeams, damageInfo.idphoton, m_NetWork.ID);


			//respawn.KillCameraTarget = damageInfo.Source;
		}
	}



	Transform shooterSource = null;
	CSVR_ServerConnector shooterServerConnector = null;
	public void Receive_HitMaster(float Damage,
	                       int Type,
	                       string ShooterPlayfabID,
	                       string ShooterName,
	                       string GunID,
	                       int HitPart,
	                       string Level,
	                       int Team,
	                       string GunName,
                            int idPhoton,
	                       Vector3 vec) {
		
	//	Debug.Log ("CSVR_FPPlayerDamgaeg Receive_HitMaster");
		//Chong cheat
		CurrentHealth = AntiCheat_CurrentHealth;

		//Tim source cua vien dan
		shooterSource = null;
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values) {
			if  (p == null) continue;
			shooterServerConnector = p.transform.GetComponent<CSVR_ServerConnector>();
			if (shooterServerConnector != null && shooterServerConnector.PlayfabID == ShooterPlayfabID) {
				shooterSource = p.transform;
			}
		}

		//Neu khong tim thay thi khong tru mau. Giai phap an toan
		if (shooterSource == null)
			return; 

		vp_DamageInfo damageInfo = new vp_DamageInfo(Damage, 
		                                             shooterSource,
		                                             (vp_DamageInfo.DamageType)Type,
		                                             ShooterPlayfabID,
		                                             ShooterName,
		                                             GunID,
		                                             HitPart,
		                                             Level,
		                                             Team,
		                                             GunName,
                                                     idPhoton,
		                                             vec); 
		
		
		if (damageInfo.team != 0 && damageInfo.team == serverConnectorInstance.idTeams) return;
		if (!respawn.RespawnNoActionTimeFinished)
			return;
        HeathTemp = CurrentHealth;
		if (CurrentHealth <= 0.0f)
			return;
		base.Damage(damageInfo);
		AntiCheat_CurrentHealth = CurrentHealth; //Chong cheat
		//Debug.Log("Trừ máu máu master ");
		animController.SetTrigger("IsTakeDamage");
		// Goi ham truyen tin ve cai chet
		if (CurrentHealth <= 0.0f)
		{
			vp_MPMaster.Instance.TransmitDeathInfo(damageInfo.ShooterPlayfabID, damageInfo.ShooterName, damageInfo.GunID,
                                                   serverConnectorInstance.PlayfabID, serverConnectorInstance.PlayfabName, damageInfo.HitPart, HeathTemp, damageInfo.Level, damageInfo.GunName, damageInfo.team, serverConnectorInstance.idTeams, damageInfo.idphoton, m_NetWork.ID);
		
		}
		
	}

}
