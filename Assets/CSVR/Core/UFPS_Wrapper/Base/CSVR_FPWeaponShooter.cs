﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CSVR_FPWeaponShooter : vp_FPWeaponShooter
{
    Image iconWeapon;
    public TypeWeaponSniper _weaponSniper;
    public int idWeapon;
    //Extra data
    //public Vector3 FlashPoint = Vector3.zero;


    public int RecoilStage = 0;
    public int RecoilStep = 0;
    public int RecoilStepMax = 0;

    //Step 1
    public Vector2 AimAngle_Step1;
    public float AimAngleIncrease_Step1;

    //Step 2
    public Vector2 AimAngle_Step2;
    public float AimAngleIncrease_Step2;

    //Step 3
    public Vector2 AimAngle_Step3;
    public float AimAngleIncrease_Step3;


    public float AimAngleDecrease;
	public float AimAngleDecrease_OnStop = 15f;

    public float currentAimAngleY;
    public float currentAimAngleX;

    public float AimAngleIncreaseX;
    public float AimAngleIncreaseY;

	public float AimAngleDecreaseX = 0f;
	public float AimAngleDecreaseY = 0f;

    public float CameraAngleIncreaseX;
    public float CameraAngleIncreaseY;
    public float CameraFollowRatioX = 0.5f;
    public float CameraFollowRatioY = 0.5f;
    public float CameraAngleY;


    public float BulletAngleMin;
    public float BulletAngleMax;
    public float BulletAngleIncrease;
    public float BulletAngleDecrease;
    public bool AttackState=false;

    public float currentBulletAngle;
	CSVR_WeaponRemoteAnim _weaponAnim;
    public static CSVR_FPWeaponShooter instance=null;
    vp_FPController m_Controler = null;
    vp_FPController Controler
    {
        get
        {
            if (m_Controler == null)
            {
                m_Controler = transform.root.GetComponent<vp_FPController>();
            }
            return m_Controler;
        }
    }
    vp_UnitBankType bank;
    string recoil;
   // CSVR_GunInfo gunManager;
    CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
	CSVR_ServerConnector serverConnectorInstance{
		get {
			//if (m_serverConnectorInstance == null) {
				m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
			//}
			return m_serverConnectorInstance;
		}
	}
    vp_MPNetworkPlayer m_NetworkPlayer; // Dung de lay thong tin ve nguoi ban
    vp_MPNetworkPlayer m_NetWork
    {
        get
        {
            if (m_NetworkPlayer == null)
            {
                m_NetworkPlayer = transform.root.GetComponent<vp_MPNetworkPlayer>();
            }
            return m_NetworkPlayer;
        }
    } 
    public string GunID,GunName; //Can phai khoi tao thong so nay o Inspector
    public string ClassGunID;

	public float WeaponDamage = 0f;
	public float WeaponRange = 1.5f;
    public float WeaponHight = 0f;
	// <summary>
	/// 
	/// </summary>
/*	public override void Start()
	{
		Debug.Log ("CSVR_FPWeaponShooter.Start");
		base.Start();
		//SetWeaponInfo ();
	}
*/

    /// <summary>
    /// 
    /// </summary>
    public void SetAnim(bool KnifeAttack)
    {
        if (KnifeAttack) {
            AnimationFire = clipAnim;
            MuzzleFlash.SetActive (false);
        } else {
            AnimationFire = clipAnimFire;
            MuzzleFlash.SetActive (true);
        }
    }
    protected override void OnEnable()
    {

        instance = this;
        base.OnEnable();
		_weaponAnim = this.GetComponent<CSVR_WeaponRemoteAnim> ();
		currentAimAngleX = currentAimAngleY = 0f;
		SetWeaponInfo ();
		currentBulletAngle = BulletAngleMin;
		ConfigPlayUI();
        //Debug.Log("Ground  "+Controler.Grounded);
    }

	public void ConfigPlayUI () {
		if (CSVR_UIPlay.instance == null)
			return;

		CSVR_FPInputMobile.Timeline = ProjectileTapFiringRate;         
		if (GetComponent<CSVR_GrenadeShooter>() == null)
		{
			CSVR_GrenadeShooter.instance = null;
		}
		if (GetComponent<CSVR_Snipper>() != null)
		{
//			Debug.Log ("_weaponSniper  "+_weaponSniper);
			if (_weaponSniper == TypeWeaponSniper.AWP)
			{
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(false);
				CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(false);
				CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(false);
			}
			else if (_weaponSniper == TypeWeaponSniper.SCAR20)
			{
				CSVR_UIPlay.instance.imgSniperCrosshair.SetActive(false);
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(false);
				CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(true);
			}
			else if (_weaponSniper == TypeWeaponSniper.AUG)
			{
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(true);
				CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(true);
			}
		}            
		else
		{
			CSVR_GameSetting.lookSensitivitySpeedZoom = 1f;
			CSVR_Snipper.instance = null;
			CSVR_UIPlay.instance.imgSniperCrosshair.SetActive(false);
			
			if (serverConnectorInstance != null && serverConnectorInstance.LoadingBackpackDone)
			{
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(true);
			}
			CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(false);
			CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(false);
		}
		Invoke("OnLoadMotor", 1);
		CSVR_UIPlay.instance.iconWeapon.GetComponent<CSVR_LoadIcon>().SetIcon(Resources.Load<Sprite>("Sprites/UIWeapon/"+GunID+"_icon"));
	}

	void OnLoadMotor() {
        if (!Player.Crouch.Active)
        {
            Controler.MotorAcceleration = (1 - WeaponHight / 20f) * 0.28f;
        }
	}


	public void SetWeaponInfo() {
	//	Debug.Log ("SetWeaponInfo");
		//Các thông số trên WeaponShooter
		if (serverConnectorInstance == null)
			return;
		WeaponRange = 500f;
		if (  serverConnectorInstance.BackpackManager != null && serverConnectorInstance.LoadingBackpackDone) {
//			Debug.Log (serverConnectorInstance.CurrentBackpackIndex);
	//		Debug.Log (serverConnectorInstance.BackpackManager.Backpacks.Length);
			//Neu la sung nhặt
			if (serverConnectorInstance.PickedGun != null && serverConnectorInstance.PickedGun.ItemID == GetComponent<vp_ItemIdentifier>().name) {
				SetGunInfo(serverConnectorInstance.PickedGun);
			} 
		/*	//Neu la sung luc nhat duoc
			else if (serverConnectorInstance.PickedPistol != null && serverConnectorInstance.PickedPistol.ItemID == GetComponent<vp_ItemIdentifier>().name) {
				SetGunInfo(serverConnectorInstance.PickedPistol);
			}*/
			// Nếu là súng chính
			else if ( (serverConnectorInstance.CurrentBackpackIndex < serverConnectorInstance.BackpackManager.Backpacks.Length) 
			         &&  (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex] !=null)){
				if (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun !=null 
				    && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun.ItemID != string.Empty
					&& serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun.ItemID != null
				    && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun.ItemID 
				    ==  GetComponent<vp_ItemIdentifier>().name ) 
				{
					//Debug.Log("Set Gun info " + GetComponent<vp_ItemIdentifier>().name);
					SetGunInfo(serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun);
				} 
				//Neu la pistol
				else if (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol !=null 
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol.ItemID != string.Empty
					&& serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol.ItemID != null
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol.ItemID 
				         ==  GetComponent<vp_ItemIdentifier>().name ) 
				{
					SetGunInfo(serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol);
				} 
				
				//Neu la melee
				else if (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee !=null 
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee.ItemID != string.Empty
						 && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee.ItemID != null
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee.ItemID 
				         ==  GetComponent<vp_ItemIdentifier>().name ) 
				{
					SetMeleeInfo(serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee);
				} 
				//Neu la grenade
				else {
					
					foreach (CSVR_GrenadeInfo gInfo in serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Grenades ) 
					{
						if (gInfo != null && gInfo.ItemID != string.Empty && gInfo.ItemID != null && GetComponent<vp_ItemIdentifier>().name == gInfo.ItemID) {
							SetGrenadeInfo(gInfo);
							break; //Thoi ko set cac grenade khac
						}
					}
				} 
			}
			
		}

		BackupWeaponInfo ();
	}


	//Step 1
	public Vector2 AimAngle_Step1_bku;
	public float AimAngleIncrease_Step1_bku;

	//Step 2
	public Vector2 AimAngle_Step2_bku;
	public float AimAngleIncrease_Step2_bku;

	//Step 3
	public Vector2 AimAngle_Step3_bku;
	public float AimAngleIncrease_Step3_bku;


	public float AimAngleDecrease_bku;
	public float AimAngleDecrease_OnStop_bku = 15f;


	public float BulletAngleMin_bku;
	public float BulletAngleMax_bku;
	public float BulletAngleIncrease_bku;
	public float BulletAngleDecrease_bku;


	public void BackupWeaponInfo() {
		AimAngle_Step1_bku = AimAngle_Step1;
		AimAngleIncrease_Step1_bku = AimAngleIncrease_Step1;

		//Step 2
		AimAngle_Step2_bku = AimAngle_Step2;
		AimAngleIncrease_Step2_bku = AimAngleIncrease_Step2;

		//Step 3
		AimAngle_Step3_bku = AimAngle_Step3;
		AimAngleIncrease_Step3_bku = AimAngleIncrease_Step3;


		AimAngleDecrease_bku = AimAngleDecrease;
		AimAngleDecrease_OnStop_bku = AimAngleDecrease_OnStop;


		BulletAngleMin_bku = BulletAngleMin;
		BulletAngleMax_bku = BulletAngleMax;
		BulletAngleIncrease_bku = BulletAngleIncrease;
		BulletAngleDecrease_bku = BulletAngleDecrease;
	}

	public void ApplyAccFactor(float f) {
		AimAngle_Step1 = f*AimAngle_Step1_bku;
		AimAngleIncrease_Step1 = f*AimAngleIncrease_Step1_bku ;

		//Step 2
		AimAngle_Step2 = f*AimAngle_Step2_bku;
		AimAngleIncrease_Step2 = f*AimAngleIncrease_Step2_bku ;

		//Step 3
		AimAngle_Step3 = f*AimAngle_Step3_bku ;
		AimAngleIncrease_Step3 = f*AimAngleIncrease_Step3_bku ;


		AimAngleDecrease = f*AimAngleDecrease_bku ;
		AimAngleDecrease_OnStop = f*AimAngleDecrease_OnStop_bku ;


		BulletAngleMin = f*BulletAngleMin_bku ;
		BulletAngleMax = f*BulletAngleMax_bku;
		BulletAngleIncrease = f*BulletAngleIncrease_bku;
		BulletAngleDecrease = f*BulletAngleDecrease_bku;
	}

	public void SetGrenadeInfo(CSVR_GrenadeInfo newGrenadeInfo) {
		//Debug.Log ("SetGunInfo");
        //Debug.Log("newGrenadeInfo.A1  " + newGrenadeInfo.A1);
		// Damage
		WeaponDamage = newGrenadeInfo.Damge;
		WeaponRange = newGrenadeInfo.Ranges;
        WeaponHight = newGrenadeInfo.Weight;
        ProjectileFiringRate = newGrenadeInfo.Speed;
        ProjectileTapFiringRate = newGrenadeInfo.Speed;
        BulletAngleMin = newGrenadeInfo.A1;
        BulletAngleMax = newGrenadeInfo.A2;
        BulletAngleIncrease = newGrenadeInfo.A3;
        BulletAngleDecrease = newGrenadeInfo.A2;
		
	}

	public void SetMeleeInfo(CSVR_MeleInfo newMeleeInfo) {
		//Debug.Log ("SetGunInfo");
		
		// Damage
		WeaponDamage = newMeleeInfo.Damge;
		WeaponRange = newMeleeInfo.Ranges;
        WeaponHight = newMeleeInfo.Weight;
        ProjectileFiringRate = newMeleeInfo.Speed;
        ProjectileTapFiringRate = newMeleeInfo.Speed;
        BulletAngleMin = newMeleeInfo.A1;
        BulletAngleMax = newMeleeInfo.A2;
        BulletAngleIncrease = newMeleeInfo.A3;
        BulletAngleDecrease = newMeleeInfo.A2;
		
	}

	public void SetGunInfo(CSVR_GunInfo newGunInfo) {
//		Debug.Log (transform.name + "CSVR_FPWeaponShooter.SetGunInfo ");

		// Damage
		WeaponDamage = newGunInfo.G1;

		//So dan trong bang
		//transform.root.GetComponent<vp_PlayerInventory>().GetUnitBankInstanceOfWeapon(this.Weapon).Capacity = int.Parse (newGunInfo.G4);
		string[] g4 = newGunInfo.G4.Split (new char[]{'/'});
        //Debug.Log ("g4   "+newGunInfo.G4);
		//Debug.Log(g4[0]);
		transform.root.GetComponent<vp_PlayerInventory>().GetUnitBankInstanceOfWeapon(this.Weapon).PlayfabGunCapacity = int.Parse (g4[0]);

		// Thông số giật		
		AimAngle_Step1.x = newGunInfo.R1X; 
		AimAngle_Step1.y = newGunInfo.R1Y;
		AimAngleIncrease_Step1 = newGunInfo.R1;
		AimAngle_Step2.x = newGunInfo.R2X;
		AimAngle_Step2.y = newGunInfo.R2Y;
		AimAngleIncrease_Step2 = newGunInfo.R2;
		AimAngle_Step3.x = newGunInfo.R3X;
		AimAngle_Step3.y = newGunInfo.R3Y;
		AimAngleIncrease_Step3 = newGunInfo.R3;
		AimAngleDecrease = newGunInfo.R0;
        BulletAngleMin = newGunInfo.A1;
        BulletAngleMax = newGunInfo.A2;
        BulletAngleIncrease = newGunInfo.A3;
        BulletAngleDecrease = newGunInfo.A2;
        BulletAngleMinTmp = BulletAngleMin;
		//Thông số khác
		gameObject.GetComponent<vp_FPWeaponReloader>().ReloadDuration = float.Parse(newGunInfo.G3);
		gameObject.GetComponent<vp_FPWeaponReloader>().weightWeapon = newGunInfo.G5;
		ProjectileFiringRate = newGunInfo.G2;
		ProjectileTapFiringRate = newGunInfo.G2;
        WeaponHight = newGunInfo.G5;

	}
    /// <summary>
    /// spawns one or more projectiles in a customizable conical
    /// pattern. NOTE: this does not send the projectiles flying.
    /// the spawned gameobjects need to have their own movement
    /// logic
    /// </summary>
    protected override void SpawnProjectiles()
    {
        //uiPlay.txtAmmoCount.text = m_Player.CurrentWeaponAmmoCount.Get().ToString();
        //uiPlay.txtMaxAmmoCount.text = m_Player.CurrentWeaponClipCount.Get().ToString();
        if (ProjectilePrefab == null)
            return;

        // will only trigger on local player in multiplayer
		if (m_SendFireEventToNetworkFunc != null) {
			//Debug.Log ("CSVR_FPWeaponShooter.SpawnProjectiles m_SendFireEventToNetworkFunc");
			m_SendFireEventToNetworkFunc.Invoke ();
		} /*else {
			//Debug.Log ("CSVR_FPWeaponShooter.SpawnProjectiles m_SendFireEventToNetworkFunc khong co");
		}*/

        m_CurrentFirePosition = GetFirePosition();
        m_CurrentFireRotation = GetFireRotation();
        m_CurrentFireSeed = GetFireSeed();

        // when firing a single projectile per discharge (pistols, machineguns)
        // this loop will only run once. if firing several projectiles per
        // round (shotguns) the loop will iterate several times. the fire seed
        // is the same for every iteration, but is multiplied with the number
        // of iterations to get a unique, deterministic seed for each projectile.
        for (int v = 0; v < ProjectileCount; v++)
        {

            GameObject p = null;

            p = (GameObject)vp_Utility.Instantiate(ProjectilePrefab, m_CurrentFirePosition, m_CurrentFireRotation);
			//Debug.Log("Vi tri xuat phat vien dan ");
			//Debug.Log(p.transform.position.ToString("F10"));
			//Debug.Log(p.transform.rotation.ToString("F10"));
            CSVR_HitscanBullet hitscanbullet = p.GetComponent<CSVR_HitscanBullet>();
            CSVR_HitscanKnife hitscanknife = p.GetComponent<CSVR_HitscanKnife>();
			CSVR_Grenade grenade = p.GetComponent<CSVR_Grenade>();
            if (hitscanbullet != null)
            {

                //Dua thong tin vao vien dan
                hitscanbullet.Shooter_PlayfabID = serverConnectorInstance.PlayfabID;
                hitscanbullet.Shooter_Name = serverConnectorInstance.PlayfabName;
                hitscanbullet.GunID = GunID;
                hitscanbullet.GunName = GunName;
                hitscanbullet.Level = serverConnectorInstance.Level;
                hitscanbullet.idTeam = serverConnectorInstance.idTeams;
                hitscanbullet.SetSource(transform.root);
				hitscanbullet.DamTest = WeaponDamage;
                hitscanbullet.idPhotonView = m_NetWork.ID;
				
				SetSpread(m_CurrentFireSeed * (v + 1), p.transform);
            }
            else if (hitscanknife != null)
            {

                //Dua thong tin vao vien dan
                hitscanknife.Shooter_PlayfabID = serverConnectorInstance.PlayfabID;
                hitscanknife.Shooter_Name = serverConnectorInstance.PlayfabName;
                hitscanknife.GunID = GunID;
                hitscanknife.GunName = GunName;
                hitscanknife.Level = serverConnectorInstance.Level;
                hitscanknife.idTeam = serverConnectorInstance.idTeams;
                hitscanknife.SetSource(transform.root);

				hitscanknife.DamTest = WeaponDamage;
				hitscanknife.Range = WeaponRange;
                hitscanknife.idPhotonView = m_NetWork.ID;

            }
            else if (grenade != null)
            {

                //Dua thong tin vao luu dan
                grenade.Shooter_PlayfabID = serverConnectorInstance.PlayfabID;
                grenade.Shooter_Name = serverConnectorInstance.PlayfabName;
                grenade.GunID = GunID;

				grenade.GunName = GunName;
				grenade.Level = serverConnectorInstance.Level;
				grenade.idTeam = serverConnectorInstance.idTeams;
                
                grenade.SetSource(transform.root);

				grenade.Damage = WeaponDamage;
				grenade.Range = WeaponRange;
				//Yo_SearchAndDestroy
				if (GunID == "00_000_C4") {
					grenade.Damage = 1000;
					grenade.Range = 50;
				}

                grenade.idPhotonView = m_NetWork.ID;

            }
            
            // uiPlay.txtAmmoCount.text = m_Player.CurrentWeaponAmmoCount.Get().ToString();
            // TIP: uncomment this to debug-draw bullet paths and points of impact
            //DrawProjectileDebugInfo(v);

            //Bo SetSource o day vi da co SetSource o tren. Neu de o day thi se tu ban trung minh
            //p.SendMessage ("SetSource", (ProjectileSourceIsRoot ? Root : Transform), SendMessageOptions.DontRequireReceiver);
            p.transform.localScale = new Vector3(ProjectileScale, ProjectileScale, ProjectileScale);	// preset defined scale

            

        }

		//Thông báo cho ServerConnector để tắt nút Backpack
		serverConnectorInstance.AlreadyFire = true;

    }

    /// <summary>
    /// applies conical twist to the target transform according
    /// to a certain seed and this shooter's 'ProjectileSpread'
    /// </summary>
	
    public override void SetSpread(int seed, Transform target)
    {
        

        Random.seed = seed;
		target.Rotate(-currentAimAngleX , 0, 0);
		target.Rotate(0, currentAimAngleY, 0);
        //Aim angle
		if (RecoilStage == 0 && AimAngleIncrease_Step1 != 0) {
			
			//Caculate steps for the step 1
			AimAngleIncreaseX = AimAngleIncrease_Step1 * ((AimAngle_Step1.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
			AimAngleIncreaseY = AimAngleIncrease_Step1 * ((AimAngle_Step1.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));


			RecoilStepMax = 1 + Mathf.Max(
				(int)(Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
				(int)(Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
				);
		


			//Do the first step
			RecoilStage = 1;
			RecoilStep = 0;

		}

        switch (RecoilStage)
        {
            case 1:
                if (RecoilStep < RecoilStepMax)
                {

                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    CameraAngleIncreaseX = AimAngleIncreaseX * CameraFollowRatioX;
                    CameraAngleIncreaseY = AimAngleIncreaseY * CameraFollowRatioY;

                    
					if (AimAngleIncrease_Step2 > 0f) {
						RecoilStep++;
					}
					//Chi cho giat o buoc 1 neu toc do buoc 2 =0
					else {
						
						
						//Tinh toan lai toc do tang
						AimAngleIncreaseX = AimAngleIncrease_Step1 * ((AimAngle_Step1.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
						AimAngleIncreaseY = AimAngleIncrease_Step1 * ((AimAngle_Step1.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
		
					}
					
				}

                
				
                else
                {
                    //Caculate steps for the step 2
                    AimAngleIncreaseX = AimAngleIncrease_Step2 * ((AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    AimAngleIncreaseY = AimAngleIncrease_Step2 * ((AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    RecoilStepMax = Mathf.Max((int)(Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
                                          (int)(Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
                                          );

                    RecoilStage = 2;
                    //Do the first step
                    RecoilStep = 0;
                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    CameraAngleIncreaseX = AimAngleIncreaseX * CameraFollowRatioX;
                    CameraAngleIncreaseY = AimAngleIncreaseY * CameraFollowRatioY;
                }
                break;
            case 2:
                if (RecoilStep < RecoilStepMax)
                {

                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    CameraAngleIncreaseX = AimAngleIncreaseX * CameraFollowRatioX;
                    CameraAngleIncreaseY = AimAngleIncreaseY * CameraFollowRatioY;

                    RecoilStep++;

                }
                else
                {
                    //Caculate steps for the step 3
                    AimAngleIncreaseX = AimAngleIncrease_Step3 * ((AimAngle_Step3.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step3.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step3.y - currentAimAngleY) + 0.01f));
                    AimAngleIncreaseY = AimAngleIncrease_Step3 * ((AimAngle_Step3.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step3.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step3.y - currentAimAngleY) + 0.01f));
                    RecoilStepMax = Mathf.Max((int)(Mathf.Abs(AimAngle_Step3.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
                                              (int)(Mathf.Abs(AimAngle_Step3.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
                                              );

                    RecoilStage = 3;
                    //Do the first step
                    RecoilStep = 0;
                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    CameraAngleIncreaseX = AimAngleIncreaseX * CameraFollowRatioX;
                    CameraAngleIncreaseY = AimAngleIncreaseY * CameraFollowRatioY;
                }
                break;

            case 3:
                if (RecoilStep < RecoilStepMax)
                {

                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    CameraAngleIncreaseX = AimAngleIncreaseX * CameraFollowRatioX;
                    CameraAngleIncreaseY = AimAngleIncreaseY * CameraFollowRatioY;

                    RecoilStep++;

                }
                else
                {
                    //Caculate steps for the step 2
                    AimAngleIncreaseX = AimAngleIncrease_Step2 * ((AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    AimAngleIncreaseY = AimAngleIncrease_Step2 * ((AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    RecoilStepMax = Mathf.Max((int)(Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
                                              (int)(Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
                                              );

                    RecoilStage = 2;
                    //Do the first step
                    RecoilStep = 0;
                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    CameraAngleIncreaseX = AimAngleIncreaseX * CameraFollowRatioX;
                    CameraAngleIncreaseY = AimAngleIncreaseY * CameraFollowRatioY;
                }
                break;

            default:
                break;
        }


		currentBulletAngle = Mathf.Max(BulletAngleMin, currentBulletAngle);

        if (currentBulletAngle < BulletAngleMax)
        {
            currentBulletAngle += BulletAngleIncrease;
        } 	
       
		currentBulletAngle = Mathf.Min(BulletAngleMax, currentBulletAngle);
             

        target.Rotate(0, 0, Random.Range(0, 360));									// first, rotate up to 360 degrees around z for circular spread
        //target.Rotate(0, Random.Range(-currentBulletAngle / 2f, currentBulletAngle / 2f), 0);		// then rotate around y with user defined deviation
		//Debug.Log (currentBulletAngle.ToString("F6"));
		Random.seed = seed;
		target.Rotate(0, Random.Range(-currentBulletAngle , currentBulletAngle ), 0);		// then rotate around y with user defined deviation
        
        if(CSVR_Snipper.instance!=null)
        {
            if (m_MuzzleFlash != null && CSVR_Snipper.instance.HoldZoomAuto)
            {
                m_MuzzleFlash.gameObject.SetActive(false);
            }
            else if (CSVR_Snipper.instance.WeaponType== TypeWeapon.SCAR20)
            {
                m_MuzzleFlash.gameObject.SetActive(true);
            }
        }

		//Debug.Log ("Final target rotation ");
		//Debug.Log (target.rotation.ToString("F6"));
         
    }
    public Quaternion GetCurrentAimRot()
    {

        return (GetFireRotation() * Quaternion.Euler(-currentAimAngleX, 0, 0) * Quaternion.Euler(0, currentAimAngleY, 0));

    }
    public float BulletAngleMinTmp;
    bool checkJump;
    /// <summary>
    /// 
    /// </summary>
    protected override void LateUpdate()
    {
        base.LateUpdate();
        if (Player.Crouch.Active)
        {
            ApplyAccFactor(0.5f);
        }
        else
        {
            ApplyAccFactor(1.0f);
            
        }
        if (Player.Attack.Active)
        {
            AttackState = true;
        }
        else
        {
            AttackState = false;            
        }
        if (Player.InputMoveVector.Get() != Vector2.zero && !checkJump)
        {
            if (Mathf.Abs(Player.InputMoveVector.Get().x) > Mathf.Abs(Player.InputMoveVector.Get().y))
            {
                float currentBullet = BulletAngleMinTmp + ((BulletAngleMax - BulletAngleMinTmp) * (Mathf.Abs(Player.InputMoveVector.Get().x) / 2));
                if (BulletAngleMin >= BulletAngleMinTmp)
                    currentBulletAngle = currentBullet;
            }
            else
            {
                float currentBullet = BulletAngleMinTmp + ((BulletAngleMax - BulletAngleMinTmp) * (Mathf.Abs(Player.InputMoveVector.Get().y) / 2));
                if (currentBullet >= BulletAngleMinTmp)
                    BulletAngleMin = currentBullet;
            }
        }
        if (Player.Jump.Active)
        {
            checkJump = true;
            currentBulletAngle = BulletAngleMax;
        }
        else if (checkJump && Controler.Grounded)
        {
            currentBulletAngle = BulletAngleMin;
            checkJump = false;
        }
        
		if (!Player.Attack.Active) {
			RecoilStage = 0;
			RecoilStep = 0;
		}

		if (currentAimAngleX != 0f) {
			if (RecoilStage > 0) {
				AimAngleDecreaseX = Time.deltaTime * AimAngleDecrease * ((currentAimAngleX) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
				AimAngleDecreaseY = Time.deltaTime * AimAngleDecrease * ((currentAimAngleY) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
			} else {

				AimAngleDecreaseX = Time.deltaTime * AimAngleDecrease_OnStop * ((currentAimAngleX) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
				AimAngleDecreaseY = Time.deltaTime * AimAngleDecrease_OnStop * ((currentAimAngleY) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));

			}
			if ( Mathf.Abs(currentAimAngleX) > Mathf.Abs(AimAngleDecreaseX) ) {
				currentAimAngleX -= AimAngleDecreaseX;
				currentAimAngleY -= AimAngleDecreaseY;
				
				CameraAngleIncreaseX -= AimAngleDecreaseX * CameraFollowRatioX;
				CameraAngleIncreaseY -= AimAngleDecreaseY * CameraFollowRatioY;
			} else {
				CameraAngleIncreaseX = -currentAimAngleX;
				CameraAngleIncreaseY = -currentAimAngleY;
				currentAimAngleX = 0f;
				currentAimAngleY = 0f;
			}
		}
        //Debug.Log("Player.CurrentWeaponMaxAmmoCount.Get() " + Player.CurrentWeaponAmmoCount.Get());
		if (currentBulletAngle > BulletAngleMin && !Player.Attack.Active)
        {
            currentBulletAngle -= BulletAngleDecrease * (Time.deltaTime);
            currentBulletAngle = Mathf.Max(BulletAngleMin, currentBulletAngle);
        }
        else if (currentBulletAngle < BulletAngleMin && !Player.Attack.Active)
        {
            currentBulletAngle = BulletAngleMin;
        }
        //if (currentBulletAngle > BulletAngleMin && Player.Attack.Active && Player.CurrentWeaponAmmoCount.Get() < 1)
        //{
        //    Player.Attack.TryStop();

        //}

      //  this.transform.localRotation = Quaternion.identity;
    }
    void SetCrosshair()
    {
        currentBulletAngle = BulletAngleMin;
        checkJump = false;
    }
	protected virtual bool CanStart_Attack()
	{
		//Debug.Log (" FPShooter CanStart_Attack");
		

		// can't attack while reloading

	

		if (ClassGunID.ToLower ().Contains ("shotgun")) {
			return true;
			
		} else {
			if (m_Player.Reload.Active)
				return false;
		}
		
		// attacking is allowed
		return true;
		
	}

	//Horus end

}
