﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using CnControls;

public class CSVR_FPInputMobile : vp_FPInputMobile
{
    //public static CSVR_FPInputMobile instance = null;
    //Load sung vào balo
    public Dictionary<int, string> ListWeaponLoad;
    public bool masterWeaponLoaded = false;
    public bool CheckmasterWeaponLoaded = false;
    CSVR_PlayerRespawner InitRepawn;
    vp_MPNetworkPlayer m_MPNetworkPlayer;
//    CSVR_UIPlay CSVR_UIPlay.instance;
    Canvas cans;
	Button btnNext, btnPrev,btnGetC4, btnGrenade, btnCrouch, btnZoom, btnStopZoom, btnReload,btnReloadGun;
    public GameObject btnJump,loadBarObject;
    SimpleJoystick joystickMove, joustickFire;
    bool checkAmorBackpack,checkFireDown;
    public float m_FormattedHealth;
    public Image imgLoadbar;
    public string[] ListWeapon;
	public TypeWeaponSniper _WeaponSniper;
    public Button tbnBackpack;
    public bool checkBL;
    public string[] strBackpack1,strTG1, strBackpack2,strTG2;
    string FormattedHealth
    {
        get
        {
            m_FormattedHealth = (FPPlayer.Health.Get());
            if (m_FormattedHealth < 1.0f)
                m_FormattedHealth = (FPPlayer.Dead.Active ? Mathf.Min(m_FormattedHealth, 0.0f) : 1.0f);
            if (FPPlayer.Dead.Active && m_FormattedHealth > 0.0f)
                m_FormattedHealth = 0.0f;
            return ((int)m_FormattedHealth).ToString();
        }

    }
    CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
    CSVR_ServerConnector serverConnectorInstance
    {
        get
        {
            if (m_serverConnectorInstance == null)
            {
                m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
            }
            return m_serverConnectorInstance;
        }
    } 
	float xHorizontal,yVertical;
//	void Awake() {
//		tbnBackpack = CSVR_UIPlayManage.init.BackpackIcon;
//		tbnBackpack.GetComponent<Image>().sprite = CSVR_UIPlayManage.init.listIconBackpack[0];
//		tbnBackpack.onClick.AddListener(() => NextBackpack());
//	}

	public void InitBackpackButton( bool isEnable){
		tbnBackpack = CSVR_UIPlayManage.init.BackpackIcon;
		tbnBackpack.gameObject.SetActive (isEnable);
//		tbnBackpack.GetComponent<Image>().sprite = CSVR_UIPlayManage.init.listIconBackpack[0];
		tbnBackpack.onClick.RemoveAllListeners ();
		tbnBackpack.onClick.AddListener(() => NextBackpack());
	}
    bool checkStateAttack;
    public override void Start()
    {

        base.Start();
        strBackpack1 = new string[5];
        strBackpack2 = new string[5];
        //_time = 0f;
        //instance = this;

        ListWeaponLoad = new Dictionary<int, string>();
        for (int i = 1; i < 3; i++)
        {
            ListWeaponLoad.Add(i, "");
        }
//        CSVR_UIPlay.instance = CSVR_ApplicationManager.instance.transform.root.FindChild("CSVR_PlayUI1").GetComponent<CSVR_UIPlay>();
		loadBarObject = CSVR_UIPlay.instance.objLoadbar;
		imgLoadbar = CSVR_UIPlay.instance.loadbar;
		CSVR_UIPlay.instance.longPressEventTrigger.FPPlayer  = FPPlayer;


        CSVR_UIPlay.instance.iconWeapon.GetComponent<CSVR_LoadIcon>().SetIcon(CSVR_UIPlay.instance.iconWeapon.GetComponent<CSVR_LoadIcon>().DefaultSprite);
		cans = CSVR_UIPlay.instance.canvas;
        cans.renderMode = RenderMode.ScreenSpaceOverlay;
        InitRepawn = transform.GetComponent<CSVR_PlayerRespawner>();
        m_MPNetworkPlayer = transform.GetComponent<vp_MPNetworkPlayer>();
       // serverWeapon.Level = "Lv: "+CSVR_GameSetting.accountLevel.ToString();
		btnZoom = CSVR_UIPlay.instance.btnZoom;
		btnStopZoom = CSVR_UIPlay.instance.btnStopZoom;
		btnJump = CSVR_UIPlay.instance.btnJump;
		btnReload = CSVR_UIPlay.instance.btnReload;
		btnReloadGun = CSVR_UIPlay.instance.btnReloadGun;
		btnGetC4 = CSVR_UIPlay.instance.btnGetC4;
		Button btnCloseC4 = btnGetC4.transform.GetChild(0).GetComponent<Button>();
		btnGrenade = CSVR_UIPlay.instance.btnGrenade;
		btnCrouch = CSVR_UIPlay.instance.btnCrouch;

		btnCloseC4.onClick.AddListener(()=> { CloseC4();});
		btnGetC4.onClick.AddListener(()=> { SetC4();});
        CSVR_UIPlay.instance.btnKnifeAttack.onClick.AddListener (()=>{
            checkStateAttack=true;
            CSVR_FPWeaponShooter.instance.SetAnim(checkStateAttack);
            KnifeAttack();
        });
        btnGrenade.onClick.AddListener(() => { SetGrenade(); });
        btnCrouch.onClick.AddListener(() => { InputCrouch(); });
        btnZoom.onClick.AddListener(() =>
        {
            if (CSVR_Snipper.instance != null && !FPPlayer.Reload.Active)
            {
                CSVR_Snipper.instance.OnStart_Zoom();
            }
        });
        btnStopZoom.onClick.AddListener(() =>
        {
            ZoomHoldFired = true;
            if (CSVR_Snipper.instance != null)
            {
                CSVR_Snipper.instance.OnStop_Zoom();
            }
        });
        btnReload.onClick.AddListener(() =>
        {
            if (FPPlayer.Attack.Active) return;
            FPPlayer.Reload.TryStart();
        });

		btnReloadGun.onClick.AddListener(() =>
		                              {
			ReloadWeapon();
		});
        //Jump
        var jumpEventTrigger = btnJump.GetComponent<EventTrigger>();
        var JumpDown = new EventTrigger.TriggerEvent();
        JumpDown.AddListener(data => { data.Use(); PlayerJump(); });
        var jumpUp = new EventTrigger.TriggerEvent();
        jumpUp.AddListener(data => { data.Use(); FPPlayer.Jump.TryStop(); });

        jumpEventTrigger.triggers.Add(new EventTrigger.Entry { callback = JumpDown, eventID = EventTriggerType.PointerDown });
        jumpEventTrigger.triggers.Add(new EventTrigger.Entry { callback = jumpUp, eventID = EventTriggerType.PointerUp });
		btnNext = CSVR_UIPlay.instance.btnNextWeapon;
		btnPrev = CSVR_UIPlay.instance.btnPrevWeapon;

        btnNext.onClick.AddListener(() => NextWeapon());
        btnPrev.onClick.AddListener(() => PrevWeapon());
        if (CSVR_GameSetting.IDMode == 102)
        {
            btnNext.gameObject.SetActive(false);
            btnPrev.gameObject.SetActive(false);
            btnReloadGun.gameObject.SetActive(false);

        }
        else
        {
            btnNext.gameObject.SetActive(true);
            btnPrev.gameObject.SetActive(true);
            btnReloadGun.gameObject.SetActive(true);
        }
		//MiniMap
//		bl_MiniMap miniMap = CSVR_UIPlay.instance.miniMap;
//		miniMap.SetTarget (this.gameObject);
//		miniMap.HorusRenderType = GetComponent<vp_MPNetworkPlayer>().TeamNumber;
        if (MapCanvasController.Instance != null)
        {
            MapCanvasController.Instance.playerTransform = this.gameObject.transform;
            MapCanvasController.Instance.teamNumber = GetComponent<vp_MPNetworkPlayer>().TeamNumber;
        }

		//End
        Invoke("ShowPlayerName",1.5f);
    }
    void ShowPlayerName()
    {

        CSVR_GetInfo.instance.ShowPlayerName(serverConnectorInstance.PlayfabName, serverConnectorInstance.idTeams);
    }
    protected override void Update()
    {
        if (!InitRepawn.RespawnNoActionTimeFinished)
        {
            imgLoadbar.fillAmount += 0.3333f * Time.deltaTime;
        }
        if (FPPlayer.Attack.Active && checkBL)
        {
            tbnBackpack.gameObject.SetActive(false);
            checkBL = false;
        }
        CSVR_GetInfo.instance.ShowHeath(int.Parse(FormattedHealth));
        // manage input for GUI
        //UpdateCursorLock();

        // toggle pausing and abort if paused
        //UpdatePause();

        if (FPPlayer.Pause.Get() == true)
            return;

        // --- NOTE: everything below this line will be disabled on pause! ---

        if (!m_AllowGameplayInput)
            return;

        // interaction
        InputInteract();

        // manage input for moving
        InputMove();
        //InputRun();
        //InputJump();
        //InputCrouch();

        // manage input for weapons
        InputAttack();
        //InputReload();
        //InputSetWeapon();

        // manage camera related input
       // InputCamera();
    }
   

	public void NextBackpack() {
        if (serverConnectorInstance.CurrentBackpackIndex < serverConnectorInstance.BackpackManager.BackpacksCount - 1)
        {
            SetBackpack(serverConnectorInstance.CurrentBackpackIndex + 1);
			tbnBackpack.transform.GetChild(0).GetComponent<Text>().text = (serverConnectorInstance.CurrentBackpackIndex + 1).ToString();
		} else {
			SetBackpack (0);
			tbnBackpack.transform.GetChild(0).GetComponent<Text>().text = "1";
		}

	}

	public void SetBackpack(int BackpackNumber)
	{		
		//Debug.Log ("FPInput SetBackpack " + BackpackNumber.ToString());
		m_MPNetworkPlayer.RequestSetBackpack (BackpackNumber);

	}

	public void HUD_OnConfigC4_Run() {
		if (serverConnectorInstance.CheckHaveBombC4 ()) {
			btnGetC4.gameObject.SetActive(true);
		}else{
			btnGetC4.gameObject.SetActive(false);
		}
	}
	public void HUD_OnConfigGrenade_Run() {
		if (serverConnectorInstance.CheckHaveGrenade ()) {
			btnGrenade.gameObject.SetActive(true);
		}else{
			btnGrenade.gameObject.SetActive(false);
		}
	}
	public void ConfigWeaponUI(int BackpackNumber) {
		HUD_OnConfigC4_Run ();

		HUD_OnConfigGrenade_Run ();

	}
	private float m_NextAllowedReloadWeaponTime = 0;
	private bool isNext = true;
    private void ReloadWeapon()
    {

        if (CSVR_Snipper.instance != null)
        {
            CSVR_Snipper.instance.OnStop_Zoom();
        }
		if (Time.time - m_NextAllowedReloadWeaponTime >= 0.5f) {
			m_NextAllowedReloadWeaponTime = Time.time;
			if (isNext) {
				FPPlayer.SetNextWeapon.Try ();
				isNext = false;
			} else {
				FPPlayer.SetPrevWeapon.Try ();
				isNext = true;
			}
		}
        /*
        switch (nextBackpack)
        {
            case 1:
                if (strBackpack1[0] == null && strBackpack1[1] == null ) return;
                if (GetWeaponID <= 1)
                {
                    GetWeaponID++;
                    if (GetWeaponID > 1)
                    {
                        GetWeaponID = 0;
                    }
                }
                else if (GetWeaponID > 1)
                {
                    GetWeaponID = 0;
                }
                while (strBackpack1[GetWeaponID] == null && GetWeaponID < 2)
                {

                    GetWeaponID++;
                    if (GetWeaponID == 2)
                    {
                        GetWeaponID = 0;
                    }
                }
                FPPlayer.SetWeaponByName.Try(strBackpack1[GetWeaponID]);
                break;
            case 2:
                if (strBackpack2[0] == null && strBackpack2[1] == null) return;
                if (GetWeaponID <= 1)
                {
                    GetWeaponID++;
                    if (GetWeaponID > 1)
                    {
                        GetWeaponID = 0;
                    }
                }
                else if (GetWeaponID > 1)
                {
                    GetWeaponID = 0;
                }
                while (strBackpack2[GetWeaponID] == null && GetWeaponID < 2)
                {
                    GetWeaponID++;
                    if (GetWeaponID == 2)
                    {
                        GetWeaponID = 0;
                    }
                }
                FPPlayer.SetWeaponByName.Try(strBackpack2[GetWeaponID]);
                break;
            default:
                break;
        }
        */
    }
    
    protected override void UpdateCursorLock()
    {
        //Empty;
    }
	void SetC4(){
		FPPlayer.SetWeaponByName.Try (GameConstants.VNMODESAD_BombC4);

	}
	void CloseC4(){
		serverConnectorInstance.DropC4 ();
		btnGetC4.gameObject.SetActive (false);
	}
    void SetGrenade()
    {
		if (CSVR_Snipper.instance != null)
		{
			CSVR_Snipper.instance.OnStop_Zoom();
		}
		for(int i=0;i<3;i++){
            if (serverConnectorInstance.BackpackManager.Backpacks[serverConnectorInstance.CurrentBackpackIndex].Grenades[i].ItemID != "")
            {
				if (FPPlayer.SetWeaponByName.Try (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Grenades [i].ItemID))
					return;
				else
					continue;
//				Yo.Log ("Lay bom", .ToString());
//                FPPlayer.SetWeaponByName.Try(serverConnectorInstance.BackpackManager.Backpacks[serverConnectorInstance.CurrentBackpackIndex].Grenades[i].ItemID);
				//return;
			}
		}

     /*   switch (nextBackpack)
        {
            case 1:
                if (strBackpack1[3] !=null)
                {
                    GetWeaponID = 3;
                    FPPlayer.SetWeaponByName.Try(strBackpack1[3]);
                }
                break;
            case 2:
                if (strBackpack2[3] !=null)
                {
                    GetWeaponID = 3;
                    FPPlayer.SetWeaponByName.Try(strBackpack2[3]);
                }
                break;
            default:
                break;
        }
*/
    }


    protected override void InputMove()
    {

        // NOTE: you could also use 'GetAxis', but that would add smoothing
        // to the input from both Ultimate FPS and from Unity, and might
        // require some tweaking in order not to feel laggy
        if (CSVR_GameSetting.controlMode == CSVR_ControlMode.CONTROL_TOUCH)
        {
            //FPPlayer.InputMoveVector.Set (new Vector2 (vp_Input.GetAxisRaw ("Horizontal"), vp_Input.GetAxisRaw ("Vertical")));
            //Debug.Log(moveStick.GetVec());
            //Bảo sửa
            //FPPlayer.InputMoveVector.Set(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")));
            //FPPlayer.InputMoveVector.Set(new Vector2(moveStick.GetVec().x, moveStick.GetVec().y));
            //along
            //FPPlayer.InputMoveVector.Set(moveStick.GetVec());
			xHorizontal= CnInputManager.GetAxis("Horizontal");
			yVertical = CnInputManager.GetAxis("Vertical");
            //if(xHorizontal < 1 && xHorizontal > 0){
            //    xHorizontal = 1;
            //}
            //if(yVertical < 1 && yVertical > 0){
            //    yVertical= 1;
            //}
			FPPlayer.InputMoveVector.Set(new Vector2(xHorizontal, yVertical));

        }

    }
    void NextWeapon()
    {
		if (FPPlayer.Dead.Active)
			return;
		
        if (CSVR_Snipper.instance != null)
        { 
            CSVR_Snipper.instance.OnStop_Zoom();
        }
		FPPlayer.SetNextWeapon.Try ();
    }
    void PrevWeapon()
    {
		if (FPPlayer.Dead.Active)
			return;
		
        if (CSVR_Snipper.instance != null)
        {
            CSVR_Snipper.instance.OnStop_Zoom();
        }
		FPPlayer.SetPrevWeapon.Try ();
    }

    /*
    protected override void InputSetWeapon()
    {
		
        if (CSVR_GameSetting.controlMode == CSVR_ControlMode.CONTROL_TOUCH) {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Screen.lockCursor = false;
            }
			if (Input.GetKeyDown(KeyCode.Q)) {
				FPPlayer.SetPrevWeapon.Try();
			}


            if (Input.GetKeyDown(KeyCode.E))
					FPPlayer.SetNextWeapon.Try();

        } else {
            if (cInput.GetButtonDown("Next Weapon"))
                FPPlayer.SetPrevWeapon.Try();
			
            if (cInput.GetButtonDown("Previous Weapon"))
                FPPlayer.SetNextWeapon.Try();
        }	
    }
    */
    //  public bool checkAttack;
    public static float Timeline = 0f;
    bool ZoomHoldFired = false;
    bool checkAttackPistol=false;
    protected void KnifeAttack ()
    {
        FPPlayer.Fire.Try ();
    }
    protected override void InputAttack()
    {
        if (checkStateAttack) {
            CSVR_FPWeaponShooter.instance.SetAnim (false);
            checkStateAttack = false;
        }
        if (CSVR_FPWeaponShooter.instance == null) return;
        CSVR_UIPlay.instance.txtMaxAmmoCount.text = FPPlayer.CurrentWeaponAmmoCount.Get() + " / " + FPPlayer.CurrentWeaponClipCount.Get();
        if (FPPlayer.CurrentWeaponAmmoCount.Get() <= 0 && CSVR_FPWeaponShooter.instance.ClassGunID != GameConstants.VNITEMCLASS_MeleeWeapon)
        {
            FPPlayer.Attack.TryStop();
            return;
        }

        if (FPPlayer.Reload.Active)
        {
			//Neu ko co dan thi khong cho ban, ke ca shotgun
			if (FPPlayer.CurrentWeaponAmmoCount.Get () <= 0) {

				return;
			} else {
				if (SimpleJoystick.fireAndMove)
				{
					FPPlayer.WaitingToAttack = true;
					//m_MPNetworkPlayer.RemoteUpdate_WaitingToAttack();
				}
			}
           
        } 
        
        if (CSVR_GameSetting.controlMode == CSVR_ControlMode.CONTROL_TOUCH)
        {
            //Neu o che do ban thuong
			if (CSVR_Snipper.instance == null && CSVR_GrenadeShooter.instance == null && CSVR_GrenadeShooter.instanceC4 == null)
            {
				
				
				if (CSVR_FPWeaponShooter.instance.ClassGunID == GameConstants.VNITEMCLASS_PistolWeapon)
                {
					
                    if (SimpleJoystick.fireAndMove && !checkAttackPistol)
                    {

                        FPPlayer.Attack.TryStart();
                        checkAttackPistol = true;
                        SimpleJoystick.fireAndMove = false;
                    }
                    else if (!SimpleJoystick.fireAndMove)
                    {
                        //Debug.Log("vao đây ko");
                        checkAttackPistol = false;
                        FPPlayer.Attack.TryStop();
                    }
                }
                else
                {
                    if (SimpleJoystick.fireAndMove)
                    {
                        FPPlayer.Attack.TryStart();
                    }
                    else
                    {
                        FPPlayer.Attack.TryStop();
                    }
                }
            }
            //Che do ban sniper
            else if (CSVR_Snipper.instance != null)
            {
                if (CSVR_FPWeaponShooter.instance._weaponSniper == TypeWeaponSniper.AWP)
                {
                    //Bat dau nhan
                    Timeline += Time.deltaTime;
                    if (SimpleJoystick.fireAndMove)
                    {
                        if (Timeline < CSVR_FPWeaponShooter.instance.ProjectileTapFiringRate) return;
                        //	Debug.Log("Fire Pressed");
                        if (!FPPlayer.Zoom.Active)
                        {
                            //	Debug.Log("Zoom not active");
                            FPPlayer.Zoom.TryStart();
                            CSVR_Snipper.instance.HoldZoom = true;
                            ZoomHoldFired = false;
                        }
                        else
                        {
                            //		Debug.Log("Zoom active - fire");
                            //Neu ko phai zoom vi giu nut Fire thi ban
                            if (!CSVR_Snipper.instance.HoldZoom)
                            {
                                FPPlayer.Attack.TryStart();
                            }
                        }
                    }
                    else
                    {

                        //Neu zoom vi nhan nut fire thi bo zoom di
                        if (CSVR_Snipper.instance.HoldZoom)
                        {
                            //	Debug.Log("Fire not Pressed - Hold zoom");
                            if (!ZoomHoldFired)
                            {
								FPPlayer.Fire.Try ();
                                //FPPlayer.Attack.TryStart(); //Ban ngay sau khi thoi nhan nut Fire
                                Timeline = 0;
                                CSVR_Snipper.instance.OnStop_Zoom();
                                ZoomHoldFired = true;
                            }

                            if (ZoomHoldFired)
                            {

                                FPPlayer.Attack.TryStop();
                            };

                            if (!FPPlayer.Attack.Active)
                            {
                                FPPlayer.Zoom.TryStop();

                            }

                            if (!FPPlayer.Zoom.Active)
                            {
                                CSVR_Snipper.instance.HoldZoom = false;
                            }

                        }
                        else
                        {
							//Debug.Log("Else Fire not Pressed - Hold zoom");
                            FPPlayer.Attack.TryStop();

                        }

                    }
                }
                else if (CSVR_FPWeaponShooter.instance._weaponSniper == TypeWeaponSniper.SCAR20 || CSVR_FPWeaponShooter.instance._weaponSniper == TypeWeaponSniper.AUG)
                {

                    if (SimpleJoystick.fireAndMove)
                    {
                        //Debug.Log(FPPlayer.Attack.NextAllowedStartTime);
                        FPPlayer.Attack.TryStart();
                    }
                    else
                    {
						//Debug.Log("Else WeaponType Fire not Pressed - Hold zoom");
                        FPPlayer.Attack.TryStop();
                    }
                }

            }
			//Che do nam luu dan
			else if (CSVR_GrenadeShooter.instance != null)//đây là lựu đạn hoặc c4
            {
				if(!CSVR_GrenadeShooter.instance.isC4)//đây là lựu đạn
				{
	                if (SimpleJoystick.fireAndMove)
	                {
	                    if (!checkFireDown)
	                    {
	                        CSVR_GrenadeShooter.instance.PleassWaitFire();
	                    }
	                    checkFireDown = true;
	                }
	                else
	                {
	                    //Neu zoom vi nhan nut fire thi bo zoom di
	                    if (checkFireDown)
	                    {
	                        FPPlayer.Attack.TryStart(); //Ban ngay sau khi thoi nhan nut Fire
	                        checkFireDown = false;
							//ConfigWeaponUI(serverConnectorInstance.CurrentBackpackIndex);
							// btnGrenade.gameObject.SetActive(false);
							HUD_OnConfigGrenade_Run();
	                    }
	                    if (!checkFireDown)
	                    {
	                        FPPlayer.Attack.TryStop();
	                        
	                    }
					}
				}
            }
        }

        if ((CSVR_FPWeaponShooter.instance != null) && (CSVR_FPWeaponShooter.instance.ClassGunID == GameConstants.VNITEMCLASS_MeleeWeapon))
        {
            CSVR_UIPlay.instance.txtMaxAmmoCount.text = string.Empty;
            return;
        }
		
    }
	float PlessTimeC4;

    void PlayerJump()
    {
        
        if (FPPlayer.Crouch.Active)
        {
            FPPlayer.Crouch.TryStop();
            CountCrouchPressed = 0;
            btnCrouch.GetComponent<Image>().sprite = CSVR_UIPlay.instance.idle;
        }
         
        FPPlayer.Jump.TryStart();
    }
    int CountCrouchPressed = 0;
    void InputCrouch()
    {

        if (CSVR_GameSetting.controlMode == CSVR_ControlMode.CONTROL_TOUCH)
        {
            CountCrouchPressed++;
            if (CountCrouchPressed > 1)
            {
                CountCrouchPressed = 0;
            }
            if (CountCrouchPressed == 1)
            {
                FPPlayer.Crouch.TryStart();
                btnCrouch.GetComponent<Image>().sprite = CSVR_UIPlay.instance.Crouch;
            }
            else
            {
                FPPlayer.Crouch.TryStop();
                btnCrouch.GetComponent<Image>().sprite = CSVR_UIPlay.instance.idle;
            }
        }
    }

}
