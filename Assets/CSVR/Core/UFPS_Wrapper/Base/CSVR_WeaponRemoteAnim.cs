using UnityEngine;
using System.Collections;

public class CSVR_WeaponRemoteAnim : MonoBehaviour 
{
	protected CSVR_BodyAnimator m_BodyAnimator;
	public enum Type
	{
		Melee,
		Pistol,
		Rifle,
		Thrown,
		SMG01, //MP5 Anim
		SMG02, 
		Shotgun,
		Sniper,
		Heavy,
        DieuCay,
		Boom
	}
	
	public enum Grip
	{
		OneHanded,
		TwoHanded,
		TwoHandedHeavy
	}
    
	public Type AnimationType;
	public Grip AnimationGrip;

	void Awake()
	{
		
		m_BodyAnimator = transform.root.GetComponentInChildren<CSVR_BodyAnimator>();
	}

	void OnEnable()
	{
		if (m_BodyAnimator != null )
		m_BodyAnimator.SetupAnimator((int)AnimationType);
	}

	protected vp_EventHandler m_EventHandler = null;
	public vp_EventHandler EventHandler
	{
		get
		{
			if (m_EventHandler == null)
				m_EventHandler = (vp_EventHandler)transform.root.GetComponentInChildren(typeof(vp_EventHandler));
			return m_EventHandler;
		}
	}

	protected vp_PlayerEventHandler m_Player = null;
	protected vp_PlayerEventHandler Player
	{
		get
		{
			if (m_Player == null)
			{
				if (EventHandler != null)
					m_Player = (vp_PlayerEventHandler)EventHandler;
			}
			return m_Player;
		}
	}
}
