﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_WeaponShooter : vp_WeaponShooter
{

    //Extra data
    public Vector3 FlashPoint = Vector3.zero;


    public int RecoilStage = 0;
    public int RecoilStep = 0;
    public int RecoilStepMax = 0;

    //Step 1
    public Vector2 AimAngle_Step1;
    public float AimAngleIncrease_Step1;

    //Step 2
    public Vector2 AimAngle_Step2;
    public float AimAngleIncrease_Step2;

    //Step 3
    public Vector2 AimAngle_Step3;
    public float AimAngleIncrease_Step3;


    public float AimAngleDecrease;
	public float AimAngleDecrease_OnStop = 30f;
	public float AimAngleDecreaseX = 0f;
	public float AimAngleDecreaseY = 0f;

    public float currentAimAngleY;
    public float currentAimAngleX;

    float AimAngleIncreaseX;
    float AimAngleIncreaseY;

    public float BulletAngleMin;
    public float BulletAngleMax;
    public float BulletAngleIncrease;
    public float BulletAngleDecrease;

    public float currentBulletAngleMin;
    public float currentBulletAngle;
    public float currentBulletAngleMax; //To use externally


    string recoil;
    CSVR_GunInfo gunManager;

	CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
	CSVR_ServerConnector serverConnectorInstance{
		get {
			if (m_serverConnectorInstance == null) {
				m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
			}
			return m_serverConnectorInstance;
		}
	}

    vp_MPNetworkPlayer m_NetworkPlayer; // Dung de lay thong tin ve nguoi ban
    vp_MPNetworkPlayer m_NetWork
    {
        get
        {
            if (m_NetworkPlayer == null)
            {
                m_NetworkPlayer = transform.root.GetComponent<vp_MPNetworkPlayer>();
            }
            return m_NetworkPlayer;
        }
    } 
    public string GunID,GunName; //Can phai khoi tao thong so nay o Inspector
    public string ClassGunID;

	public float m_WeaponDamage = 0f;
	public float WeaponDamage {
		get {
			return m_WeaponDamage;
		}

		set{m_WeaponDamage = value;}

	}
	public float WeaponRange = 1.5f;

    
	//Tao tia dan
	public CSVR_BulletTracer mBulletTracer_Instance = null;
	public CSVR_BulletTracer BulletTracer_Instance {
		get {
			if (mBulletTracer_Instance == null) {
				mBulletTracer_Instance = Instantiate (Resources.Load ("BulletTracer_Veo") as GameObject).GetComponent<CSVR_BulletTracer>();
				mBulletTracer_Instance.FireRate = 1f/ProjectileFiringRate;
			}

			return mBulletTracer_Instance;
		}

	}

	public Transform BulletTracerStartPoint;
    

	/// <summary>
	/// 
	/// </summary>
	public override void Start()
	{
		
		base.Start();
		//SetWeaponInfo ();
	}

    //void OnDisable() {
    ////	Debug.Log (transform.name + " OnDisable");
    //}

    /// <summary>
    /// 
    /// </summary>
    protected override void OnEnable()
    {
	//	Debug.Log ("Remote OnEnable weaponshooter");
        base.OnEnable();
       
	//	Debug.Log ("OnEnable " + GetComponent<vp_ItemIdentifier>().name);

        if (GetComponent<CSVR_Weapon>() != null)
        {
            BulletTracerStartPoint = GetComponent<CSVR_Weapon>().Weapon3rdPersonModel.transform;
        }
	
		currentAimAngleX = currentAimAngleY = 0f;	

		SetWeaponInfo ();
		currentBulletAngle = BulletAngleMin;

		//Debug.Break ();

    }
   
	public void SetWeaponInfo() {
		
		
		//Các thông số trên WeaponShooter
		if (serverConnectorInstance == null)
			return;
		WeaponRange = 500f;
		if (  serverConnectorInstance.BackpackManager != null && serverConnectorInstance.LoadingBackpackDone) {
			//Neu la sung nhặt
			if (serverConnectorInstance.PickedGun != null && serverConnectorInstance.PickedGun.ItemID == GetComponent<vp_ItemIdentifier>().name) {
				SetGunInfo(serverConnectorInstance.PickedGun);
			} 
		/*	//Neu la sung luc nhat duoc
			else if (serverConnectorInstance.PickedPistol != null && serverConnectorInstance.PickedPistol.ItemID == GetComponent<vp_ItemIdentifier>().name) {
				SetGunInfo(serverConnectorInstance.PickedPistol);
			}*/
			// Nếu là súng chính
			else if ( (serverConnectorInstance.CurrentBackpackIndex < serverConnectorInstance.BackpackManager.Backpacks.Length) 
			         &&  (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex] !=null)){
				if (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun !=null 
				    && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun.ItemID != string.Empty
				    && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun.ItemID 
				    ==  GetComponent<vp_ItemIdentifier>().name ) 
				{
//					Debug.Log("Set Gun info " + GetComponent<vp_ItemIdentifier>().name);
					SetGunInfo(serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Gun);
				} 
				//Neu la pistol
				else if (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol !=null 
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol.ItemID != string.Empty
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol.ItemID 
				         ==  GetComponent<vp_ItemIdentifier>().name ) 
				{
					SetGunInfo(serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Pistol);
				} 
				
				//Neu la melee
				else if (serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee !=null 
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee.ItemID != string.Empty
				         && serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee.ItemID 
				         ==  GetComponent<vp_ItemIdentifier>().name ) 
				{
					SetMeleeInfo(serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Melee);
				} 
				//Neu la grenade
				else {
					
					foreach (CSVR_GrenadeInfo gInfo in serverConnectorInstance.BackpackManager.Backpacks [serverConnectorInstance.CurrentBackpackIndex].Grenades ) 
					{
						if (gInfo != null && gInfo.ItemID != string.Empty && GetComponent<vp_ItemIdentifier>().name == gInfo.ItemID) {
							SetGrenadeInfo(gInfo);
							break; //Thoi ko set cac grenade khac
						}
					}
				} 
			}
			
		} else {
			Debug.Log (transform.name + " SetWeaponInfo Chua load backpack xong");				
		}

		BackupWeaponInfo ();
	}

	//Step 1
	public Vector2 AimAngle_Step1_bku;
	public float AimAngleIncrease_Step1_bku;

	//Step 2
	public Vector2 AimAngle_Step2_bku;
	public float AimAngleIncrease_Step2_bku;

	//Step 3
	public Vector2 AimAngle_Step3_bku;
	public float AimAngleIncrease_Step3_bku;


	public float AimAngleDecrease_bku;
	public float AimAngleDecrease_OnStop_bku = 15f;


	public float BulletAngleMin_bku;
	public float BulletAngleMax_bku;
	public float BulletAngleIncrease_bku;
	public float BulletAngleDecrease_bku;


	public void BackupWeaponInfo() {
		AimAngle_Step1_bku = AimAngle_Step1;
		AimAngleIncrease_Step1_bku = AimAngleIncrease_Step1;

		//Step 2
		AimAngle_Step2_bku = AimAngle_Step2;
		AimAngleIncrease_Step2_bku = AimAngleIncrease_Step2;

		//Step 3
		AimAngle_Step3_bku = AimAngle_Step3;
		AimAngleIncrease_Step3_bku = AimAngleIncrease_Step3;


		AimAngleDecrease_bku = AimAngleDecrease;
		AimAngleDecrease_OnStop_bku = AimAngleDecrease_OnStop;


		BulletAngleMin_bku = BulletAngleMin;
		BulletAngleMax_bku = BulletAngleMax;
		BulletAngleIncrease_bku = BulletAngleIncrease;
		BulletAngleDecrease_bku = BulletAngleDecrease;
	}

	public void ApplyAccFactor(float f) {
		AimAngle_Step1 = f*AimAngle_Step1_bku;
		AimAngleIncrease_Step1 = f*AimAngleIncrease_Step1_bku ;

		//Step 2
		AimAngle_Step2 = f*AimAngle_Step2_bku;
		AimAngleIncrease_Step2 = f*AimAngleIncrease_Step2_bku ;

		//Step 3
		AimAngle_Step3 = f*AimAngle_Step3_bku ;
		AimAngleIncrease_Step3 = f*AimAngleIncrease_Step3_bku ;


		AimAngleDecrease = f*AimAngleDecrease_bku ;
		AimAngleDecrease_OnStop = f*AimAngleDecrease_OnStop_bku ;


		BulletAngleMin = f*BulletAngleMin_bku ;
		BulletAngleMax = f*BulletAngleMax_bku;
		BulletAngleIncrease = f*BulletAngleIncrease_bku;
		BulletAngleDecrease = f*BulletAngleDecrease_bku;
	}

	public void SetGrenadeInfo(CSVR_GrenadeInfo newGrenadeInfo) {
		//Debug.Log ("SetGrenadeInfo");
		
		// Damage
		WeaponDamage = newGrenadeInfo.Damge;
		WeaponRange = newGrenadeInfo.Ranges;
		
	}
	
	public void SetMeleeInfo(CSVR_MeleInfo newMeleeInfo) {
		//Debug.Log ("SetMeleeInfo");
		
		// Damage
		WeaponDamage = newMeleeInfo.Damge;
		WeaponRange = newMeleeInfo.Ranges;
		
	}

	public void SetGunInfo(CSVR_GunInfo newGunInfo) {
		
//		Debug.Log ("SetGunInfo");
		// Damage
		WeaponDamage = newGunInfo.G1;	

		//So dan trong bang
		//transform.root.GetComponent<vp_PlayerInventory>().GetUnitBankInstanceOfWeapon(this.Weapon).Capacity = int.Parse (newGunInfo.G4);
		string[] g4 = newGunInfo.G4.Split (new char[]{'/'});
		//Debug.Log(g4[0]);
		transform.root.GetComponent<vp_PlayerInventory>().GetUnitBankInstanceOfWeapon(this.Weapon).PlayfabGunCapacity = int.Parse (g4[0]);

		// Thông số giật		
		AimAngle_Step1.x = newGunInfo.R1X; 
		AimAngle_Step1.y = newGunInfo.R1Y;
		AimAngleIncrease_Step1 = newGunInfo.R1;
		AimAngle_Step2.x = newGunInfo.R2X;
		AimAngle_Step2.y = newGunInfo.R2Y;
		AimAngleIncrease_Step2 = newGunInfo.R2;
		AimAngle_Step3.x = newGunInfo.R3X;
		AimAngle_Step3.y = newGunInfo.R3Y;
		AimAngleIncrease_Step3 = newGunInfo.R3;
		AimAngleDecrease = newGunInfo.R0;
		BulletAngleMin = newGunInfo.A1;
		BulletAngleMax = newGunInfo.A2;
		BulletAngleIncrease = newGunInfo.A3;
		BulletAngleDecrease = newGunInfo.A2;
	
		
		//Thông số khác
		gameObject.GetComponent<vp_WeaponReloader>().ReloadDuration = float.Parse(newGunInfo.G3);
		ProjectileFiringRate = newGunInfo.G2;
		ProjectileTapFiringRate = newGunInfo.G2;


	}



    /// <summary>
    /// spawns one or more projectiles in a customizable conical
    /// pattern. NOTE: this does not send the projectiles flying.
    /// the spawned gameobjects need to have their own movement
    /// logic
    /// </summary>
    protected override void SpawnProjectiles()
    {

        if (ProjectilePrefab == null)
            return;

        // will only trigger on local player in multiplayer
        if (m_SendFireEventToNetworkFunc != null)
            m_SendFireEventToNetworkFunc.Invoke();

        m_CurrentFirePosition = GetFirePosition();
        m_CurrentFireRotation = GetFireRotation();
        m_CurrentFireSeed = GetFireSeed();

        // when firing a single projectile per discharge (pistols, machineguns)
        // this loop will only run once. if firing several projectiles per
        // round (shotguns) the loop will iterate several times. the fire seed
        // is the same for every iteration, but is multiplied with the number
        // of iterations to get a unique, deterministic seed for each projectile.
        for (int v = 0; v < ProjectileCount; v++)
        {

            GameObject p = null;
            p = (GameObject)vp_Utility.Instantiate(ProjectilePrefab, m_CurrentFirePosition, m_CurrentFireRotation);
			//Debug.Log("Vi tri xuat phat vien dan ");
			//Debug.Log(p.transform.position.ToString("F10"));
			//Debug.Log(p.transform.rotation.ToString("F10"));
            CSVR_HitscanBullet hitscanbullet = p.GetComponent<CSVR_HitscanBullet>();
           


            if (hitscanbullet != null)
            {

                //Dua thong tin vao vien dan
                hitscanbullet.Shooter_PlayfabID = serverConnectorInstance.PlayfabID;
                hitscanbullet.Shooter_Name = serverConnectorInstance.PlayfabName;
                hitscanbullet.GunID = GunID;
                hitscanbullet.GunName = GunName;
                hitscanbullet.Level = serverConnectorInstance.Level;
                hitscanbullet.idTeam = serverConnectorInstance.idTeams;
                hitscanbullet.SetSource(transform.root);
				hitscanbullet.DamTest = WeaponDamage;
                hitscanbullet.idPhotonView = m_NetWork.ID;

				//Tao tia dan
				hitscanbullet.mBulletTracer_Instance = BulletTracer_Instance;
				hitscanbullet.mBulletTracer_StartPoint = BulletTracerStartPoint.position;

				SetSpread(m_CurrentFireSeed * (v + 1), p.transform);
            }
            else {
				CSVR_HitscanKnife hitscanknife = p.GetComponent<CSVR_HitscanKnife>();
				if (hitscanknife != null)
	            {

	                //Dua thong tin vao vien dan
	                hitscanknife.Shooter_PlayfabID = serverConnectorInstance.PlayfabID;
	                hitscanknife.Shooter_Name = serverConnectorInstance.PlayfabName;
	                hitscanknife.GunID = GunID;
	                hitscanknife.GunName = GunName;
	                hitscanknife.Level = serverConnectorInstance.Level;
	                hitscanknife.idTeam = serverConnectorInstance.idTeams;
	                hitscanknife.SetSource(transform.root);

					hitscanknife.DamTest = WeaponDamage;
					hitscanknife.Range = WeaponRange;
                    hitscanknife.idPhotonView = m_NetWork.ID;
	            }
	            else {
					CSVR_Grenade grenade = p.GetComponent<CSVR_Grenade>();
					if (grenade != null)
		            {

		                //Dua thong tin vao luu dan
		                grenade.Shooter_PlayfabID = serverConnectorInstance.PlayfabID;
		                grenade.Shooter_Name = serverConnectorInstance.PlayfabName;
		                grenade.GunID = GunID;

						grenade.GunName = GunName;
						grenade.Level = serverConnectorInstance.Level;
						grenade.idTeam = serverConnectorInstance.idTeams;

		                grenade.SetSource(transform.root);

						grenade.Damage = WeaponDamage;
						grenade.Range = WeaponRange;
						//Yo_SearchAndDestroy
						if (GunID == "00_000_C4") {
							grenade.Damage = 1000;
							grenade.Range = 50;
						}
                        grenade.idPhotonView = m_NetWork.ID;
		            }
				}
			}
            

            // TIP: uncomment this to debug-draw bullet paths and points of impact
            //DrawProjectileDebugInfo(v);

            //p.SendMessage ("SetSource", (ProjectileSourceIsRoot ? Root : Transform), SendMessageOptions.DontRequireReceiver);
            p.transform.localScale = new Vector3(ProjectileScale, ProjectileScale, ProjectileScale);	// preset defined scale

            

        }

		//Thông báo cho ServerConnector để tắt nút Backpack
		serverConnectorInstance.AlreadyFire = true;


    }

    /// <summary>
    /// applies conical twist to the target transform according
    /// to a certain seed and this shooter's 'ProjectileSpread'
    /// </summary>
    public override void SetSpread(int seed, Transform target)
    {
		if (Player.Crouch.Active) {
			ApplyAccFactor (0.5f);
		} else {
			ApplyAccFactor (1.0f);
		}

        Random.seed = seed;
		target.Rotate(-currentAimAngleX, 0, 0);
		target.Rotate(0, currentAimAngleY, 0);

        //Aim angle
		if (RecoilStage == 0 && AimAngleIncrease_Step1 != 0) {

			//Caculate steps for the step 1
			AimAngleIncreaseX = AimAngleIncrease_Step1 * ((AimAngle_Step1.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
			AimAngleIncreaseY = AimAngleIncrease_Step1 * ((AimAngle_Step1.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
			RecoilStepMax = 1 + Mathf.Max(
				(int)(Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
				(int)(Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
			);

			//Do the first step
			RecoilStage = 1;
			RecoilStep = 0;

		}
        switch (RecoilStage)
        {
           
            case 1:
                if (RecoilStep < RecoilStepMax)
                {

                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

					if (AimAngleIncrease_Step2 > 0f) {
						RecoilStep++;
					}
					//Chi cho giat o buoc 1 neu toc do buoc 2 =0
					else {
						
						
						//Tinh toan lai toc do tang
						AimAngleIncreaseX = AimAngleIncrease_Step1 * ((AimAngle_Step1.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
						AimAngleIncreaseY = AimAngleIncrease_Step1 * ((AimAngle_Step1.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step1.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step1.y - currentAimAngleY) + 0.01f));
						
					}
                }
                else
                {
                    //Caculate steps for the step 2
                    AimAngleIncreaseX = AimAngleIncrease_Step2 * ((AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    AimAngleIncreaseY = AimAngleIncrease_Step2 * ((AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    RecoilStepMax = Mathf.Max((int)(Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
                                              (int)(Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
                                              );

                    RecoilStage = 2;
                    //Do the first step
                    RecoilStep = 0;
                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;
                }
                break;
            case 2:
                if (RecoilStep < RecoilStepMax)
                {

                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    RecoilStep++;

                }
                else
                {
                    //Caculate steps for the step 3
                    AimAngleIncreaseX = AimAngleIncrease_Step3 * ((AimAngle_Step3.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step3.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step3.y - currentAimAngleY) + 0.01f));
                    AimAngleIncreaseY = AimAngleIncrease_Step3 * ((AimAngle_Step3.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step3.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step3.y - currentAimAngleY) + 0.01f));
                    RecoilStepMax = Mathf.Max((int)(Mathf.Abs(AimAngle_Step3.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
                                              (int)(Mathf.Abs(AimAngle_Step3.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
                                              );

                    RecoilStage = 3;
                    //Do the first step
                    RecoilStep = 0;
                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;
                }
                break;

            case 3:
                if (RecoilStep < RecoilStepMax)
                {

                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;

                    RecoilStep++;

                }
                else
                {
                    //Caculate steps for the step 2
                    AimAngleIncreaseX = AimAngleIncrease_Step2 * ((AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    AimAngleIncreaseY = AimAngleIncrease_Step2 * ((AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) + Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) + 0.01f));
                    RecoilStepMax = Mathf.Max((int)(Mathf.Abs(AimAngle_Step2.x - currentAimAngleX) / (Mathf.Abs(AimAngleIncreaseX) + 0.01f)),
                                              (int)(Mathf.Abs(AimAngle_Step2.y - currentAimAngleY) / (Mathf.Abs(AimAngleIncreaseY) + 0.01f))
                                              );

                    RecoilStage = 2;
                    //Do the first step
                    RecoilStep = 0;
                    currentAimAngleX += AimAngleIncreaseX;
                    currentAimAngleY += AimAngleIncreaseY;
                }
                break;

            default:
                break;
        }
		currentBulletAngle = Mathf.Max(BulletAngleMin, currentBulletAngle);

        if (Player.InputMoveVector.Get() != Vector2.zero)
        {
            currentBulletAngle = BulletAngleMax;
        }
        else if (currentBulletAngle < BulletAngleMax)
        {
            currentBulletAngle += BulletAngleIncrease;

        }

		currentBulletAngle = Mathf.Min(BulletAngleMax, currentBulletAngle);


        target.Rotate(0, 0, Random.Range(0, 360));
		Random.seed = seed;
		target.Rotate(0, Random.Range(-currentBulletAngle, currentBulletAngle), 0);	
    }

    public Quaternion GetCurrentAimRot()
    {

        return (GetFireRotation() * Quaternion.Euler(-currentAimAngleX, 0, 0) * Quaternion.Euler(0, currentAimAngleY, 0));

    }

    /// <summary>
    /// 
    /// </summary>
    protected override void LateUpdate()
    {	
        base.LateUpdate();
		if (!Player.Attack.Active) {
			RecoilStage = 0;
			RecoilStep = 0;
		}
		
		if (currentAimAngleX != 0f) {
			if (RecoilStage > 0) {
				AimAngleDecreaseX = Time.deltaTime * AimAngleDecrease * ((currentAimAngleX) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
				AimAngleDecreaseY = Time.deltaTime * AimAngleDecrease * ((currentAimAngleY) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
			} else {
				
				AimAngleDecreaseX = Time.deltaTime * AimAngleDecrease_OnStop * ((currentAimAngleX) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
				AimAngleDecreaseY = Time.deltaTime * AimAngleDecrease_OnStop * ((currentAimAngleY) / (Mathf.Abs (currentAimAngleX) + Mathf.Abs (currentAimAngleY) + 0.01f));
				
			}
			if ( Mathf.Abs(currentAimAngleX) > Mathf.Abs(AimAngleDecreaseX) ) {
				currentAimAngleX -= AimAngleDecreaseX;
				currentAimAngleY -= AimAngleDecreaseY;

			} else {

				currentAimAngleX = 0f;
				currentAimAngleY = 0f;
			}
		}
		if (currentBulletAngle > BulletAngleMin && !Player.Attack.Active)
		{
			currentBulletAngle -= BulletAngleDecrease * (Time.deltaTime);
			currentBulletAngle = Mathf.Max(BulletAngleMin, currentBulletAngle);
		}
    }
    
	protected override void OnStop_Attack()
	{
		base.OnStop_Attack ();
		BulletTracer_Instance.HideTracer();
	}

	protected void OnStart_Attack()
	{
		BulletTracer_Instance.ShowTracer ();
	}
}
