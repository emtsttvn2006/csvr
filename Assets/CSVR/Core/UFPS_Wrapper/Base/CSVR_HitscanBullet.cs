﻿using UnityEngine;
using System.Collections;

public class CSVR_HitscanBullet : vp_HitscanBullet
{
    //Thong tin ve nguoi ban    
    public string Shooter_PlayfabID;
    public string Shooter_Name;
    public string GunID,GunName;
    public string Level;
    public int idTeam;
    public int idPhotonView;
    int HitPart = 1;//0 - Head, 1 - Body . Thong tin ve cho va cham
    public float XG;

    public float DamAmor;
    public float DamTest;

	float LucBan = 30;

	//Tao tia dan
	public Vector3 mBulletTracer_StartPoint = Vector3.zero;
	public CSVR_BulletTracer mBulletTracer_Instance = null;


	//Config tieng lỗ đạn
	float MaxImpactAudio = 2;

	//Config Xuyen tuong
	float MinDamXuyenTuong = 130; // Damage tối thiểu để có thể xuyên
	float MaxWallThickNess = 0.2f; // Do day cua tuong co the xuyen


	//Dung noi bo
	bool CanPenetrate = true;
	RaycastHit m_InverseHit;
	Ray m_InverseRay;
	Transform m_InverseBullet; //De tao vet dan phia ben kia tuong
	Transform m_NewBullet; //De tao vet dan phia ben kia tuong
	float m_NewRange; //Range moi duoc tao ra de 1 vien dan moi di tiep
	Vector3 m_RangeEngPoint;
	float m_WallThickness = 0f;
	//bool m_NoHit = false; //De tat DoHit()


    void Start()
    {
        m_Initialized = true;
		m_Audio.rolloffMode = AudioRolloffMode.Linear;
		m_Audio.maxDistance = MaxImpactAudio;
		m_Audio.minDistance = 0f;
		m_Audio.volume = 1f;
		m_Audio.spatialBlend = 1.0f;  
		if (m_ImpactSounds.Count > 0) {
			m_Audio.clip = m_ImpactSounds[0];
		}
        DoHit();
    }


    public enum DamagePart
    {
        Head,
        Body
    }

    /// <summary>
    /// 
    /// </summary>
 /*   void OnEnable()
    {
		Debug.Log ("CSVR_HitscanBullet.OnEnable");
        if (!m_Initialized)
            return;

        //DoHit();

    }
    */
    //public int dam;

	protected int LayerMaskBullet = (1 << vp_Layer.Ragdoll | 1 << vp_Layer.Default) ;

	public override void DoHit()
	{
//		Debug.Log ("DoHit");
		//if (m_NoHit) return; //Khi dan di xuyen qua sang ben kia thi clone cua dan khong DoHit() nua

		Ray ray = new Ray(m_Transform.position, m_Transform.forward);
	  
	    CheckRay:
			//Debug.DrawRay(m_RangeEngPoint, -(m_Transform.forward.normalized*Range), Color.yellow, 20);
		if (Physics.Raycast (ray, out m_Hit, Range, LayerMaskBullet)) {
			if (m_Hit.transform.root == m_Source) {
				ray = new Ray (m_Hit.point + m_Transform.forward.normalized * 0.1f, m_Transform.forward);
				goto CheckRay;

			} 



			//Tao tia dan
			if (mBulletTracer_Instance != null) {
				mBulletTracer_Instance.SetDirection (mBulletTracer_StartPoint, m_Hit.point);
			}

			m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider (m_Hit.collider);

			if (m_TargetDHandler is CSVR_PlayerDamageHandler) {
				((CSVR_PlayerDamageHandler)m_TargetDHandler).RagdollHandler.HitRigidBody = m_Hit.rigidbody;
				((CSVR_PlayerDamageHandler)m_TargetDHandler).RagdollHandler.HitImpact = transform.forward * LucBan;
			}



			switch (m_Hit.collider.tag) {
			case "Head":
			//	Debug.Log("Head");
			//	Debug.Log(DamTest);
			//	Debug.Log(m_TargetDHandler.MaxAmor);
			//	Debug.Log(XG);
				Damage = (DamTest * 1) * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
				HitPart = 0;
				if (Damage > 0) {
					if ((m_TargetDHandler != null) && (m_Source != null)) {
						//Ham nay truyen thong tin ve damage va nguoi ban va GunID
                        m_TargetDHandler.Damage(new vp_DamageInfo(Damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, idTeam, GunName,idPhotonView, m_Hit.point));
					}
				}
				Destroy (gameObject);
				break;
			case "Femoral":
				HitPart = 1;               
				DamAmor = DamTest * 0.25f;
				Damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
				vp_DamageHandler.DamageAmor = DamAmor;
				if (Damage > 0) {
					if ((m_TargetDHandler != null) && (m_Source != null)) {
                        m_TargetDHandler.Damage(new vp_DamageInfo(Damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, idTeam, GunName, idPhotonView, m_Hit.point));
					}
				}                
				Destroy (gameObject);
				break;
			case "Leg":
				HitPart = 1;
			//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(m_Hit.collider);
				DamAmor = DamTest * 0.2f;
				Damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
				vp_DamageHandler.DamageAmor = DamAmor;
				if (Damage > 0) {
					if ((m_TargetDHandler != null) && (m_Source != null)) {
						//Ham nay truyen thong tin ve damage va nguoi ban va GunID
                        m_TargetDHandler.Damage(new vp_DamageInfo(Damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, idTeam, GunName, idPhotonView, m_Hit.point));
					}
				}                    
				Destroy (gameObject);
				break;
			case "Chest":
				HitPart = 1;
			//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(m_Hit.collider);
				DamAmor = DamTest * 0.4f;
				Damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
				vp_DamageHandler.DamageAmor = DamAmor;
				if (Damage > 0) {
					if ((m_TargetDHandler != null) && (m_Source != null)) {
						//Ham nay truyen thong tin ve damage va nguoi ban va GunID
                        m_TargetDHandler.Damage(new vp_DamageInfo(Damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, idTeam, GunName, idPhotonView, m_Hit.point));
					}
				}
                
				Destroy (gameObject);
				break;
			case "Stomach":
				HitPart = 1;
			//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(m_Hit.collider);
				DamAmor = DamTest * 0.3f;
				Damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
				vp_DamageHandler.DamageAmor = DamAmor;
				if (Damage > 0) {
					if ((m_TargetDHandler != null) && (m_Source != null)) {
						//Ham nay truyen thong tin ve damage va nguoi ban va GunID
                        m_TargetDHandler.Damage(new vp_DamageInfo(Damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, idTeam, GunName, idPhotonView, m_Hit.point));
					}
				}
                
				Destroy (gameObject);
				break;
			default:
				//Nếu là đạn xuyên tường thì phải sinh ra 1 bullet moi
				if (DamTest > MinDamXuyenTuong && CanPenetrate) {
					//Debug.Log ("Chuẩn bị xuyên tường");
					m_RangeEngPoint = ray.GetPoint (Range);
					m_InverseRay = new Ray (m_RangeEngPoint, -(m_Transform.forward)); //Raycast chiều ngược lại
					//Debug.DrawRay(m_RangeEngPoint, -(m_Transform.forward.normalized*Range), Color.red, 20);

					//Chỉ raycast vào object này. Nếu không raycast được tức là tường quá dày và bỏ qua
					if (m_Hit.collider.Raycast(m_InverseRay, out m_InverseHit, Range)) {
						//Debug.Log ("Raycast ngược lại");
						m_WallThickness = Vector3.Distance (m_Hit.point, m_InverseHit.point);
//						Debug.Log (m_WallThickness);
						m_NewRange =  Range - Vector3.Distance(m_Transform.position, m_Hit.point) - MaxWallThickNess; 
						
						//Neu m_NewRange > 0, tuc la vien dan co the di tiep
						if (m_NewRange > 0 && m_WallThickness < MaxWallThickNess) {
							/*	//Tạo 1 vết đạn ở chỗ ben kia tuong
							m_InverseBullet = Instantiate(transform.root.gameObject).transform;
							m_InverseBullet.GetComponent<CSVR_HitscanBullet> ().m_NoHit = true; //Bullet nay chi de tao vet dan ben kia tuong
							
							//Dua vet dan den dung vi tri
							m_InverseBullet.parent = m_InverseHit.transform;
							m_InverseBullet.localPosition = m_InverseHit.transform.InverseTransformPoint (m_InverseHit.point);
							m_InverseBullet.rotation = Quaternion.LookRotation (m_InverseHit.normal);	
							
							Destroy (m_InverseBullet.gameObject, 5); //Hen gio de huy vet dan nay
							*/
							//Tao 1 vien dan moi de di tiep
							m_NewBullet = Instantiate(transform.root.gameObject).transform;
							m_NewBullet.position = m_InverseHit.point;
							
							//Range bi ngan lai, tuy theo cho bi trung dan, do day cua tuong va do cung cua tuong
							m_NewBullet.GetComponent<CSVR_HitscanBullet> ().Range = m_NewRange;
							m_NewBullet.GetComponent<CSVR_HitscanBullet> ().CanPenetrate = false; //Dan moi nay ko xuyen tuong nua
						}											
					}					
				}
				// move this gameobject instance to the hit object
				Vector3 scale = m_Transform.localScale;	// save scale to handle scaled parent objects
				m_Transform.parent = m_Hit.transform;
				m_Transform.localPosition = m_Hit.transform.InverseTransformPoint(m_Hit.point);
				m_Transform.rotation = Quaternion.LookRotation(m_Hit.normal);					// face away from hit surface
				if (m_Hit.transform.lossyScale == Vector3.one)								// if hit object has normal scale
					m_Transform.Rotate(Vector3.forward, Random.Range(0, 360), Space.Self);	// spin randomly
				else
				{
					// rotated child objects will get skewed if the parent object has been
					// unevenly scaled in the editor, so on scaled objects we don't support
					// spin, and we need to unparent, rescale and reparent the decal.
					m_Transform.parent = null;
					m_Transform.localScale = scale;
					m_Transform.parent = m_Hit.transform;
				}
				/*
				//De cho vien dan hien ra
				m_Transform.localPosition = new Vector3(m_Transform.localPosition.x,
				                                        m_Transform.localPosition.y, 
				                                        m_Transform.localPosition.z*0.95f);
				                                        */
				
				// spawn dust effect
				if (m_DustPrefab != null)
					vp_Utility.Instantiate (m_DustPrefab, m_Transform.position, m_Transform.rotation);
                if (m_ImpactPrefab != null)
                    vp_Utility.Instantiate(m_ImpactPrefab, m_Transform.position, m_Transform.rotation);
				// Di chuyen den cho hit va xoay cho dung huong
				// face away from hit surface
				
				
				// play impact sound
				
				
				m_Audio.Stop ();
				m_Audio.Play ();
				
				
				
				//Tao tia dan
				if (mBulletTracer_Instance != null) {
					mBulletTracer_Instance.SetDirection (mBulletTracer_StartPoint, m_Hit.point);
				}



				
				
				
				// if bullet is visible (i.e. has a decal), queue it for deletion later
				if (m_Renderer != null) {
					vp_DecalManager.Add (gameObject);
                    Invoke("DestroyGameObject", 1);
				} else
					vp_Timer.In (1, TryDestroy);		// we have no renderer, so destroy object in 1 sec

				break;
            

			}
		}
		else {
			//Tao tia dan
			if (mBulletTracer_Instance != null) {
				//Debug.Log ("Goi ShowTracer");
				mBulletTracer_Instance.SetDirection (mBulletTracer_StartPoint, ray.GetPoint(Range));
			}
			vp_Utility.Destroy(gameObject);	// hit nothing, so self destruct immediately
		}

		/*
        Ray ray = new Ray(m_Transform.position, m_Transform.forward);

        // if this bullet was fired by the local player, don't allow it to hit the local player


        if ((m_Source != null) && (m_Source.gameObject.layer == vp_Layer.LocalPlayer))
            LayerMask = vp_Layer.Mask.BulletBlockers;

        if (Physics.Raycast(ray, out m_Hit, Range, LayerMask))
        {
            if (m_Hit.transform.root == m_Source) return;
            // NOTE: we can't bail out of this if-statement based on !collider.isTrigger,
            // because that would make bullets _disappear_ if they hit a trigger. to make a
            // trigger not interfere with bullets, put it in the layer: 'vp_Layer.Trigger'
            // (default: 27)

            // move this gameobject instance to the hit object
            Vector3 scale = m_Transform.localScale;	// save scale to handle scaled parent objects
            m_Transform.parent = m_Hit.transform;
            m_Transform.localPosition = m_Hit.transform.InverseTransformPoint(m_Hit.point);
            m_Transform.rotation = Quaternion.LookRotation(m_Hit.normal);					// face away from hit surface
            if (m_Hit.transform.lossyScale == Vector3.one)								// if hit object has normal scale
                m_Transform.Rotate(Vector3.forward, Random.Range(0, 360), Space.Self);	// spin randomly
            else
            {
                // rotated child objects will get skewed if the parent object has been
                // unevenly scaled in the editor, so on scaled objects we don't support
                // spin, and we need to unparent, rescale and reparent the decal.
                m_Transform.parent = null;
                m_Transform.localScale = scale;
                m_Transform.parent = m_Hit.transform;
            }

            // if hit object has physics, add the bullet force to it
            Rigidbody body = m_Hit.collider.attachedRigidbody;
            if (body != null && !body.isKinematic)
                body.AddForceAtPosition(((ray.direction * Force) / Time.timeScale) / vp_TimeUtility.AdjustedTimeScale, m_Hit.point);

            // spawn impact effect
            

            // spawn dust effect
            if (m_DustPrefab != null)
                vp_Utility.Instantiate(m_DustPrefab, m_Transform.position, m_Transform.rotation);
			// Di chuyen den cho hit va xoay cho dung huong
						// face away from hit surface


			// play impact sound
		

			m_Audio.Stop();
			m_Audio.Play();



			//Tao tia dan
			if (mBulletTracer_Instance != null) {
				mBulletTracer_Instance.SetDirection (mBulletTracer_StartPoint, m_Hit.point);
			}



			// if bullet is visible (i.e. has a decal), queue it for deletion later
			if (m_Renderer != null) {
				vp_DecalManager.Add (gameObject);
				Destroy (gameObject, 15f);
			} else
				vp_Timer.In (1, TryDestroy);		// we have no renderer, so destroy object in 1 sec

        }
		else {
			//Tao tia dan
			if (mBulletTracer_Instance != null) {
				//Debug.Log ("Goi ShowTracer");
				mBulletTracer_Instance.SetDirection (mBulletTracer_StartPoint, ray.GetPoint(Range));
			}
            vp_Utility.Destroy(gameObject);	// hit nothing, so self destruct immediately
		}
		*/

    }
    void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}
