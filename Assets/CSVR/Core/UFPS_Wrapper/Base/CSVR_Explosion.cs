﻿using UnityEngine;
using System.Collections;
//using System;
[RequireComponent(typeof(AudioSource))]

public class CSVR_Explosion : vp_Explosion {
	
	//Thong tin ve nguoi ban    
	public string Shooter_PlayfabID;
	public string Shooter_Name;
	public string GunID,GunName;
	public string Level;
	public int idTeam;
    public int idPhotonView = 0;
	/// <summary>
	/// 
	/// </summary>
	public override void DoDamage()
	{
		//		Debug.Log("DoDamage");
		m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(m_TargetCollider);

		// *0.f vì thay đổi cơ chế damage, cho phép damage nhiều bộ phận.
		if (m_TargetDHandler != null)
		{
            m_TargetDHandler.Damage(new vp_DamageInfo((DistanceModifier * Damage * 0.1f), Source, vp_DamageInfo.DamageType.Explosion, Shooter_PlayfabID, Shooter_Name, GunID, 1, Level, idTeam, GunName, idPhotonView));
		}
		else if (!RequireDamageHandler)
		{
			// target is known to have no damagehandler -> send damage the 'Unity way'.
			// this works with targets that have a custom script with the standard
			// method: "Damage(float damage)"
			//(DistanceModifier * Damage)
			m_TargetCollider.gameObject.BroadcastMessage(DamageMessageName, (DistanceModifier * Damage*0.1f), SendMessageOptions.DontRequireReceiver);
		}
		
	}
	
	
	/// <summary>
	/// spawns effects, applies forces and damage to close by objects
	/// and flags the explosion for removal
	/// </summary>
	public override void DoExplode()
	{


		//		Debug.Log ("DoExplode");
		m_HaveExploded = true;
		
		// spawn effects gameobjects
		foreach (GameObject fx in FXPrefabs)
		{
			if (fx != null)
			{
				#if UNITY_EDITOR
				Component[] c;
				c = fx.GetComponents<vp_Explosion>();	// OK from a performance perspective because it only occurs in editor
				if (c.Length > 0)
					Debug.LogError("Error: vp_Explosion->FXPrefab must not be a vp_Explosion (risk of infinite loop).");
				else
					#endif
					vp_Utility.Instantiate(fx, Transform.position, Quaternion.identity);
			}
		}
		
		// clear the list of affected objects in case this explosion has been pooled
		m_RootTransformsHitByThisExplosion.Clear();
		
		// apply shockwave to all rigidbodies and players within range, but
		// ignore small and walk-thru objects such as debris, triggers and water
		//		Debug.Log ("Radius " + Radius.ToString());
		Collider[] colliders = Physics.OverlapSphere(Transform.position, Radius, vp_Layer.Mask.DamageMark);
		
		foreach (Collider hit in colliders)
		{
			//			Debug.Log("Grenade hit " + hit.gameObject.name);
			if (hit.gameObject.isStatic)
				continue;
			m_DistanceModifier = 0.0f;
			//			Debug.Log("Grenade hit 1");
			if ((hit != null) && (hit != this.GetComponent<Collider>()))
			{
				m_TargetCollider = hit;
				m_TargetTransform = hit.transform;
				
				// --- add camera shake ---
				
				//				Debug.Log("Grenade hit 2");
				// --- abort if we have no line of sight to target ---
				if (TargetInCover())
					continue;
				AddUFPSCameraShake();
				//				Debug.Log("Grenade hit 3");
				// --- try to add force ---
				m_TargetRigidbody = hit.GetComponent<Rigidbody>();
				if (m_TargetRigidbody != null)		// target has a rigidbody: apply force using Unity physics
					AddRigidbodyForce();
				else 								// target has no rigidbody. try and apply force using UFPS physics
					AddUFPSForce();
				
				// --- try to add damage ---
				
				/*if (!AllowMultiDamage)
				{
					// abort if this explosion has already affected the target collider's root object
					if (!m_RootTransformsHitByThisExplosion.ContainsKey(m_TargetCollider.transform.root))
					{
						m_RootTransformsHitByThisExplosion.Add(m_TargetCollider.transform.root, null);	// remember that we have processed this target
						DoDamage();
					}
				}
				else
				{*/
				DoDamage();
				//}
				
				//Debug.Log(m_TargetTransform.name + Time.time);	// SNIPPET: to dump affected objects
				
			}
			
		}

		//Yo_SearchAndDestroy
		if (GunID == "00_000_C4"){
			vp_SADMaster.Team1Death = true;
		}

		// play explosion sound
		Audio.clip = Sound;
		Audio.pitch = Random.Range(SoundMinPitch, SoundMaxPitch) * Time.timeScale;
		if (!Audio.playOnAwake)
			Audio.Play();		
	}
}
