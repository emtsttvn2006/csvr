﻿using UnityEngine;
using System.Collections;
using CnControls;
using ParagonAI;

public class CSVR_FPCamera : vp_FPCamera
{
	//bool initViewAndControl = false;

	public Camera weaponCam = null;

	public override void Awake()
	{
		//			Debug.Log ("CSVR_FPCamera.Awake");
		
		StateManager.SetState("Idle", enabled);
		
		FPController = Root.GetComponent<vp_FPController>();
		
		// run 'SetRotation' with the initial rotation of the camera. this is important
		// when not using the spawnpoint system (or player rotation will snap to zero yaw)
		SetRotation(new Vector2(Transform.eulerAngles.x, Transform.eulerAngles.y));
		
		// set parent gameobject layer to 'LocalPlayer', so camera can exclude it
		// this also prevents shell casings from colliding with the charactercollider
		Parent.gameObject.layer = vp_Layer.LocalPlayer;
		
		// TODO: removed for multiplayer. evaluate consequences
		//foreach (Transform b in Parent)
		//{
		//	if (b.gameObject.layer != vp_Layer.RemotePlayer)
		//		b.gameObject.layer = vp_Layer.LocalPlayer;
		//}
		
		// main camera initialization
		// render everything except body and weapon
		//Horus begin
		//GetComponent<Camera>().cullingMask &= ~((1 << vp_Layer.LocalPlayer) | (1 << vp_Layer.Weapon));
		GetComponent<Camera>().cullingMask &= ~((1 << vp_Layer.LocalPlayer));
		//Horus end
		GetComponent<Camera>().depth = 0;
		
		// weapon camera initialization
		// find a regular Unity Camera component existing in a child
		// gameobject to the FPSCamera's gameobject. if we don't find
		// a weapon cam, that's OK (some games don't have weapons)

		foreach (Transform t in Transform)
		{
			weaponCam = (Camera)t.GetComponent(typeof(Camera));
			if (weaponCam != null)
			{
				weaponCam.transform.localPosition = Vector3.zero;
				weaponCam.transform.localEulerAngles = Vector3.zero;
				weaponCam.clearFlags = CameraClearFlags.Depth;

				//Bo dong nay de khong hien vu khi luc dau
				//weaponCam.cullingMask = (1 << vp_Layer.Weapon);	// only render the weapon
				weaponCam.cullingMask = 0;

				weaponCam.depth = 1;
				weaponCam.farClipPlane = 100;
				weaponCam.nearClipPlane = 0.01f;
				weaponCam.fieldOfView = 50;
				break;
			}
		}
		
		// create springs for camera motion
		
		// --- primary position spring ---
		// this is used for all sorts of positional force acting on the camera
		m_PositionSpring = new vp_Spring(Transform, vp_Spring.UpdateMode.Position, false);
		m_PositionSpring.MinVelocity = 0.00001f;
		m_PositionSpring.RestState = PositionOffset;
		
		// --- secondary position spring ---
		// this is mainly intended for positional force from recoil, stomping and explosions
		m_PositionSpring2 = new vp_Spring(Transform, vp_Spring.UpdateMode.PositionAdditiveLocal, false);
		m_PositionSpring2.MinVelocity = 0.00001f;
		
		// --- rotation spring ---
		// this is used for all sorts of angular force acting on the camera
		m_RotationSpring = new vp_Spring(Transform, vp_Spring.UpdateMode.RotationAdditiveLocal, false);
		m_RotationSpring.MinVelocity = 0.00001f;

		
	}
	
	public override void Start()
	{
        
		StateManager.Reset();
		Refresh();
		
		
		// snap the camera to its start values when first activated
		SnapSprings();
		SnapZoom();
		//Horus begin
		
		CSVR_UIPlay.instance.SwitchViewMode(CSVR_GameSetting.viewMode);
		//CSVR_UIPlay.instance.SwitchControlMode (CSVR_GameSetting.controlMode);
		//applicationManagerInstance.m_CrossHair.raycastCameraTransform = transform.FindChild("WeaponCamera");
		//Horus end
		//		Debug.Log ("CSVR_FPCamera.Start Done");

		//Auto target
		ViewBlockLayer = 1 << vp_Layer.Default;

		//horus begin
		CrosshairTargetObject_Name = string.Empty;
		CrosshairTargetObject_Heath = string.Empty;
		CrosshairTargetObject_Enemy = false;
//		if (PlayerPrefs.HasKey ("CSVRChangeTayCamSung")) {
//			CSVR_GameSetting.TayCamSung = PlayerPrefs.GetFloat("CSVRChangeTayCamSung");
//		} else {
//			CSVR_GameSetting.TayCamSung = 1;
//		}
//
//		if(CSVR_GameSetting.TayCamSung == 1)
//			this.transform.GetComponent<Transform> ().localScale = new Vector3 (1, 1, 1);
//		else
//			this.transform.GetComponent<Transform> ().localScale = new Vector3 (-1, 1, 1);
		//horus end
		
	}

	protected override void LateUpdate()
	{
		//Debug.Log (AutoTargetTag);
		
		if (Time.timeScale == 0.0f)
			return;
		
		// fetch the FPSController's SmoothPosition. this reduces jitter
		// by moving the camera at arbitrary update intervals while
		// controller and springs move at the fixed update interval
		m_Transform.position = FPController.SmoothPosition;
		
		// apply current spring offsets
		if (Player.IsFirstPerson.Get())
			m_Transform.localPosition += (m_PositionSpring.State + m_PositionSpring2.State);
		else
			m_Transform.localPosition += (m_PositionSpring.State +
			                              (Vector3.Scale(m_PositionSpring2.State, Vector3.up)));	// don't shake camera sideways in third person
		
		// prevent camera from intersecting objects
		if (HasCollision)
			DoCameraCollision();
		
		// rotate the parent gameobject (i.e. player model)
		// NOTE: this rotation does not pitch the player model, it only applies yaw
		Quaternion xQuaternion = Quaternion.AngleAxis(m_Yaw, Vector3.up);
		Quaternion yQuaternion = Quaternion.AngleAxis(0, Vector3.left);
		Parent.rotation =
			vp_MathUtility.NaNSafeQuaternion((xQuaternion * yQuaternion), Parent.rotation);
		
		// pitch and yaw the camera
		yQuaternion = Quaternion.AngleAxis(-m_Pitch, Vector3.left);
		Transform.rotation =
			vp_MathUtility.NaNSafeQuaternion((xQuaternion * yQuaternion), Transform.rotation);
		
		// roll the camera
		Transform.localEulerAngles +=
			vp_MathUtility.NaNSafeVector3(Vector3.forward * m_RotationSpring.State.z);
		
		// third person
		Update3rdPerson();
		
		
		//Horus begin
		switch (CSVR_GameSetting.controlMode)
		{
		case CSVR_ControlMode.CONTROL_HORUS:
			CSVR_UIPlay.instance.updateCamera(CSVR_GameSetting.gyroRotAbsolute, transform.position);
			break;
			
		case CSVR_ControlMode.CONTROL_GAMEPAD:
		case CSVR_ControlMode.CONTROL_TOUCH:
		default:


                 
			CSVR_UIPlay.instance.updateCamera(transform.rotation, transform.position);
			break;
		}
		
	}
	
	
	
	
	protected override void UpdateInput()
	{


        if (Player.Dead.Active)
            return;
		switch (CSVR_GameSetting.controlMode)
		{
			
		case CSVR_ControlMode.CONTROL_HORUS:
			
			m_Yaw = CSVR_HorusInput.controllerRotation.eulerAngles.y;
			m_Pitch = CSVR_HorusInput.controllerRotation.eulerAngles.x;
			if (m_Pitch > 180f)
			{
				m_Pitch = m_Pitch - 360f;
			}
			
			
			break;
			
		case CSVR_ControlMode.CONTROL_GAMEPAD:
		case CSVR_ControlMode.CONTROL_TOUCH:
		default:
			switch (CSVR_GameSetting.viewMode)
			{
				
			case CSVR_ViewMode.VIEW_2D_MODE:
				//Neu bat che do auto target
				if (CSVR_GameSetting.AutoTargetEnabled) {
					
					
					UpdateInputAutoTarget ();
					
					m_Yaw += AutoTargetInputLook.y; 
					m_Pitch += AutoTargetInputLook.x; 
					
					m_Yaw += CnInputManager.GetAxis ("Touch Horizontal") / 4 * CSVR_GameSetting.lookSensitivitySpeed * CSVR_GameSetting.lookSensitivitySpeedZoom * 0.6f;
					m_Pitch += CnInputManager.GetAxis ("Touch Vertical") * -1 / 4 * CSVR_GameSetting.zoomSensitivitySpeed * CSVR_GameSetting.lookSensitivitySpeedZoom * 0.6f;
					AutoTargetLostTrack = true; //Moi lan nha nut bat, thi lan sau se tim ke dich moi
					
					//Cap nhat trang thai cua nut
					PrevAimPadPressed = Touchpad._isCurrentlyTweaking;
					
					
				}
				//Neu ko auto target
				{
					
					if (SimpleJoystick.fireAndMove) {
						m_Yaw += CnInputManager.GetAxis ("Touch Horizontal") / 4 * CSVR_GameSetting.lookSensitivitySpeed * CSVR_GameSetting.lookSensitivitySpeedZoom * 0.6f;
						m_Pitch += CnInputManager.GetAxis ("Touch Vertical") * -1 / 4 * CSVR_GameSetting.zoomSensitivitySpeed * CSVR_GameSetting.lookSensitivitySpeedZoom * 0.6f;
					}
					//Neu nut ban khong duoc giu thi nhu binh thuong
					else {
						m_Yaw += CnInputManager.GetAxis ("Touch Horizontal") * CSVR_GameSetting.lookSensitivitySpeed * CSVR_GameSetting.lookSensitivitySpeedZoom * 0.6f;
						if (CSVR_FPWeaponShooter.instance != null) {
							CSVR_FPWeaponShooter.instance.CameraAngleY = m_Pitch += CnInputManager.GetAxis ("Touch Vertical") * -1 * CSVR_GameSetting.zoomSensitivitySpeed * CSVR_GameSetting.lookSensitivitySpeedZoom * 0.6f;
						}
					}
				}
				if (CSVR_FPWeaponShooter.instance != null) {
					m_Yaw += CSVR_FPWeaponShooter.instance.CameraAngleIncreaseY;
					m_Pitch -= CSVR_FPWeaponShooter.instance.CameraAngleIncreaseX;
					CSVR_FPWeaponShooter.instance.CameraAngleIncreaseX = CSVR_FPWeaponShooter.instance.CameraAngleIncreaseY = 0f;
				}
				if (CSVR_GameSetting.gyroEnabled)
				{
					
					m_Yaw += CSVR_GameSetting.gyroYawRelative * CSVR_GameSetting.gyroScale;
					
					m_Pitch += CSVR_GameSetting.gyroPitchRelative * CSVR_GameSetting.gyroScale;
				}
				
				
				break;
				
			case CSVR_ViewMode.VIEW_VR_MODE:
				
				m_Yaw = CSVR_GameSetting.gyroYawAbsolute;
				if (CSVR_GameSetting.gyroPitchAbsolute > 180f)
				{
					m_Pitch = CSVR_GameSetting.gyroPitchAbsolute - 360f;
				}
				else
				{
					m_Pitch = CSVR_GameSetting.gyroPitchAbsolute;
				}
				break;
			default:
				break;
			}
			break;
		}
		
		
		
		
		//		Debug.Log (m_Pitch);
		//Horus end
		// clamp angles
		m_Yaw = m_Yaw < -360.0f ? m_Yaw += 360.0f : m_Yaw;
		m_Yaw = m_Yaw > 360.0f ? m_Yaw -= 360.0f : m_Yaw;
		m_Yaw = Mathf.Clamp(m_Yaw, RotationYawLimit.x, RotationYawLimit.y);
		m_Pitch = m_Pitch < -360.0f ? m_Pitch += 360.0f : m_Pitch;
		m_Pitch = m_Pitch > 360.0f ? m_Pitch -= 360.0f : m_Pitch;
		m_Pitch = Mathf.Clamp(m_Pitch, -RotationPitchLimit.x, -RotationPitchLimit.y);
		
	}
	
	/// <summary>
	/// 
	/// </summary>
	protected override void FixedUpdate()
	{
		if (Time.timeScale == 0.0f)
			return;
		//base.FixedUpdate();
		
		
		
		//	UpdateZoom();
		
		//	UpdateSwaying();
		
		//UpdateBob();
		
		//	UpdateEarthQuake();
		
		//	UpdateShakes();
		
		UpdateSprings();
		
	}
	
	
	
	/*****
 * AUTO TARGET
 * 
 */
	//Config cua auto target
	float AutoTargetSpeed = 40f; //Chinh toc do auto target o day. Nen dua sang CSVR_GameSetting
	//Bo check radius xung quanh nhan vat. Chuyen sang check viewport
	//float AutoTargetRadius = 2.0f; //Ban kinh xung quanh nhan vat se bat che do auto 
	float AutoTargetRadius = 0.1f; //Ban kinh xung quanh nhan vat (dua tren viewport) se bat che do auto 
	float AutoTargetTimeMax = 10f; //Thoi gian bat auto sau moi lan di chuyen tay





	//Cac bien dung trong luc choi
	
	int mTeamNumber = -1; //Gia tri ban dau de biet chua duoc khoi tao
	int TeamNumber {
		get {
			if (mTeamNumber == -1) {
				vp_MPNetworkPlayer t = transform.root.GetComponent<vp_MPNetworkPlayer>();
				if (t!= null) {
					mTeamNumber = t.TeamNumber;
				}
			}
			return mTeamNumber;
		}
		
		set {mTeamNumber = value;}
	}

	bool AutoTargetLostTrack = true;
	bool AutoTargetOn = true;

	float OffAutoTargetTimer = 0f;

	Vector3 WorldCentrePoint;

	public static bool IsUsingSniper = false;
	
	
	bool PrevAimPadPressed = false;

	//GameObject[] AutoTargetObject_All;
	Transform AutoTargetObject_Selected;
	
	Vector2 AutoTargetInputLook;
	//	float AutoTargetLastTimeSearch = 0f;
	float ClosestTargetDistance = 0f;
	float ClosestEnemyDistance = 0f;

	//int ClosestTargetIndex = 0;
	Vector3 ViewPortPoint;
	float ViewPortDistance = 0f;
	Vector2 ViewPortCentre = new Vector2 (0.5f, 0.5f);
	
	Camera m_CurrentCameraInstance = null;
	Vector3 AutoTargetCorrectDirection;
	
	public void ResetAutoTarget() {
		//Debug.Log ("ResetAutoTarget from " + transform.root.name);
		AutoTargetOn = false;
		AutoTargetObject_Selected = null;
		AutoTargetInputLook = Vector2.zero;
	}
	
	//Lay script camera dang dung
	Camera GetCamera()
	{
		if (m_CurrentCameraInstance == null)
		{
			if (CSVR_UIPlay.instance.camera2D.gameObject.GetActive())
			{
				m_CurrentCameraInstance = CSVR_UIPlay.instance.camera2D.GetComponent<Camera>();
			}
//			else
//			{
//				m_CurrentCameraInstance = CSVR_UIPlay.instance.cameraSBS.GetChild(0).GetComponent<Camera>();
//			}
			
		}
		return m_CurrentCameraInstance;
	}

	//Ray ViewRay;
	RaycastHit RayBlockHit;
	int ViewBlockLayer;

	Vector3 ViewportPoint;


	vp_PlayerEventHandler PlayerTemp;
	CSVR_AutotargetMarker AIPlayerTemp;

	bool IsTargetDead(Transform target) 
	{
		PlayerTemp = target.root.GetComponent<vp_PlayerEventHandler> ();
		AIPlayerTemp = target.root.GetComponent<CSVR_AutotargetMarker> ();
		//Check if dead

		if ((AIPlayerTemp != null && AIPlayerTemp.DeadActive) || (PlayerTemp != null && PlayerTemp.Dead.Active))
		{
			return true;
		} else
			return false;
	}




	bool InAutoRange(Transform target)
	{
		
		/* Phien ban cu
		WorldCentrePoint = GetCamera ().ViewportToWorldPoint ( new Vector3(0.5f, 0.5f, 
		                                     Vector3.Distance(transform.position, target.position) ) );

		return (Vector3.Distance (WorldCentrePoint, target.position) < AutoTargetRadius);
		*/

		ViewPortPoint = GetCamera().WorldToViewportPoint(target.position);

		ViewPortDistance = Vector2.Distance(
			ViewPortCentre,
			new Vector2(ViewPortPoint.x, ViewPortPoint.y)
		) ;

		return (ViewPortDistance < AutoTargetRadius);

	}

	bool ViewBlocked (Transform target)  {

		//Kiem tra xem co bi vuong gi ko

		if (Physics.Raycast(transform.position, (target.position - transform.position),Vector3.Distance(target.position , transform.position), ViewBlockLayer)) 
		{		
			//Debug.Log("Enemy is blocked");
			return true;
		}
		return false;
	}

//	//Kiểm tra xem có đang trỏ vào enemy rồi ko, tinh ca viền
//	RaycastHit RayPointingHit;
//	Ray RayPointing;
//	LayerMask RagdollLayer = 1 << vp_Layer.Ragdoll;
//	vp_MPNetworkPlayer TargetPlayerInfo;
//	bool IsPointingToEnemy ()  {
//		
//
//		RayPointing = GetCamera ().ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0f));
//		if (Physics.Raycast(RayPointing, out RayPointingHit,  500, RagdollLayer )) 
//		{	
//			TargetPlayerInfo = RayPointingHit.transform.root.GetComponent<vp_MPNetworkPlayer>();
//
//			if (TargetPlayerInfo != null) {
//				if (TeamNumber != 0 || TargetPlayerInfo.TeamNumber != TeamNumber )				   
//				{
//					return true;
//				}
//			} else {
//				return false;
//			}
//		}
//		return false;
//	}


	/// <summary>
	/// Kien: check point enemy voi ca player va AI
	/// </summary>
	//Kiểm tra xem có đang trỏ vào enemy rồi ko, tinh ca viền
	RaycastHit RayPointingHit;
	Ray RayPointing;
//	LayerMask RagdollLayer = (1 << vp_Layer.Ragdoll | 1 << vp_Layer.RemotePlayer);
	LayerMask RagdollLayer = (1 << vp_Layer.Ragdoll);
//	LayerMask RagdollLayer = (1 << vp_Layer.RemotePlayer);
	vp_MPNetworkPlayer TargetPlayerInfo;
	CSVR_AutotargetMarker AISetupInfo;
	bool IsPointingToEnemy ()
	{
		RayPointing = GetCamera ().ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0f));
		if (Physics.Raycast(RayPointing, out RayPointingHit,  500, RagdollLayer )) 
		{	
			TargetPlayerInfo = RayPointingHit.transform.root.GetComponent<vp_MPNetworkPlayer>();
			if (TargetPlayerInfo != null)
			{
				if (TeamNumber != 0 || TargetPlayerInfo.TeamNumber != TeamNumber)
				{
//					print ("dang chia vao Player");
					return true;
				}
				else 
				{
					return false;
				}
			}

		}
		return false;
	}

	RaycastHit RayPointingHitAI;
	bool IsPointingToAIEnemy ()
	{
		RayPointing = GetCamera ().ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0f));
		if (Physics.Raycast(RayPointing, out RayPointingHitAI,  500, RagdollLayer )) 
		{	
			AISetupInfo = RayPointingHitAI.transform.root.GetComponent<CSVR_AutotargetMarker>();
			if (AISetupInfo != null)
			{
				if (TeamNumber != 0 && AISetupInfo.TeamNumber == TeamNumber)
				{
					//					print ("dang chia vao Bot Player");
					return false;
				}
				else 
				{
					return true;
				}
			}			

		}
		return false;
	}


//	public void UpdateInputAutoTarget()
//		
//	{
//		//Neu dang chia vao enemy roi thi thoi
//		if (IsPointingToEnemy())  {
//			AutoTargetInputLook = Vector2.zero;
//			return;
//		}
//
//
//		OffAutoTargetTimer -= Time.deltaTime;
//
//		if (Touchpad._isCurrentlyTweaking) {
//			if (
//				CnInputManager.GetAxis ("Touch Horizontal") == 0 &&
//				CnInputManager.GetAxis ("Touch Vertical") == 0
//			    ) {	
//				//Bat auto target tro lai va hen gio tat
//				if (!AutoTargetOn) {
//					AutoTargetOn = true;
//					//Set timer here
//					OffAutoTargetTimer = AutoTargetTimeMax;
//				}
//
//			} 
//			//Tat auto target ngay lap tuc
//			else {
//				AutoTargetOn = false;
//				AutoTargetObject_Selected = null;
//				AutoTargetInputLook = Vector2.zero;
//			}
//		} 
//		//Neu vua moi nhac tay
//		else if (PrevAimPadPressed) {
//			//Bat auto target tro lai va hen gio tat
//			if (!AutoTargetOn) {
//				AutoTargetOn = true;
//				//Set timer here
//				OffAutoTargetTimer = AutoTargetTimeMax;
//			}
//
//		}
//
//		//Tat sau 1 thoi gian, sau khi da nhac tay
//		if (AutoTargetOn && OffAutoTargetTimer < 0) { 
//			AutoTargetOn = false;
//			AutoTargetObject_Selected = null;
//			AutoTargetInputLook = Vector2.zero;
//		}
//
//		if (!AutoTargetOn) {
//			AutoTargetInputLook = Vector2.zero;
//			return;
//		}
//
//		//Cap nhat view ray
//		//ViewRay = new Ray (transform.position, transform.forward);
//		//Debug.DrawRay (transform.position, transform.forward*150f);
//
//		if ( (AutoTargetObject_Selected != null) && (!InAutoRange(AutoTargetObject_Selected) || ViewBlocked(AutoTargetObject_Selected ) || IsTargetDead(AutoTargetObject_Selected)) ) {
//			AutoTargetObject_Selected = null;
//		}
//
//		if (AutoTargetObject_Selected == null) {
//		//	Debug.Log("Searching for target");
//			//Tim target moi
//
//			ClosestTargetDistance = float.MaxValue;
//			ClosestEnemyDistance = float.MaxValue;
//			
//			foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values) {
//				//Debug.Log("Check target");
//				//Neu ko phai co tag cua enemy thi continue
//				if ( p == null || (TeamNumber != 0 && p.TeamNumber == TeamNumber )
//				    || 
//				    (p.transform.root == transform.root)
//				    )
//				{
//					//Debug.Log("Khong phai enemy");
//					continue;
//				}
//
//				//Enemy o xa hon 1 target da tim duoc
//				if (Vector3.Distance(p.transform.position, transform.position) > ClosestEnemyDistance ) {
//					continue;
//				}
//
//
//				if (p.Player.Dead.Active || !InAutoRange( p.transform.GetComponentInChildren<CSVR_BodyAnimator>().LowestSpineBone.transform )) {
//					//Debug.Log("Eney not in auto range");
//					continue;
//				}
//
//
//			 
//				//Debug.Log("Check collider");
//				foreach(Collider c in p.GetComponentInChildren<vp_RagdollHandler>().Colliders) {
//					ViewPortPoint = GetCamera().WorldToViewportPoint(c.transform.position);
//					
//					ViewPortDistance = Vector2.Distance(
//						ViewPortCentre,
//						new Vector2(ViewPortPoint.x, ViewPortPoint.y)
//						) ;
//					if (
//						ViewPortDistance
//						< ClosestTargetDistance
//						) 
//					{
//
//						
//						if (!ViewBlocked(c.transform)) 
//						{	ClosestTargetDistance = ViewPortDistance;
//							AutoTargetObject_Selected = c.transform;
//						}
//						
//					}
//					
//				}
//
//				if (AutoTargetObject_Selected != null) {
//					ClosestEnemyDistance = Vector3.Distance(AutoTargetObject_Selected.position, transform.position);
//				}
//
//				
//			}
//			
//		} 
//	
//		/*
//		Chỉ để test
//		GameObject dummy =  GameObject.FindGameObjectWithTag ("dummyTarget");
//
//		if (dummy != null)
//			AutoTargetObject_Selected = dummy.transform;
//
//*/
//		//Tinh toán xem di tâm ngắm bao nhiêu
//		//Neu la sung thuong thi auto khong dang khong giu nut
//		if (AutoTargetObject_Selected != null )
//		{
//			
//			
//			//Ngam vao dau head
//			AutoTargetCorrectDirection = Quaternion.LookRotation(AutoTargetObject_Selected.transform.position - transform.position).eulerAngles;
//			
//			
//			/*
//            if (AutoTargetCorrectDirection.y < 0)
//                AutoTargetCorrectDirection.y += 360f;
//            if (m_Yaw < 0)
//                m_Yaw += 360f;
//    */
//			AutoTargetInputLook.y = (AutoTargetCorrectDirection.y - m_Yaw) % 360f;
//			
//			//Chon duong di ngan nhat
//			if (AutoTargetInputLook.y > 180f)
//			{
//				AutoTargetInputLook.y = AutoTargetInputLook.y - 360f; //m_Yaw phai giam dan
//			}
//			else if (AutoTargetInputLook.y < -180f)
//			{
//				AutoTargetInputLook.y = 360f + AutoTargetInputLook.y; //m_Yaw phai tang len
//			}
//			
//			//	if (m_Pitch < 0)
//			//		m_Pitch += 180f;
//
//			if (AutoTargetCorrectDirection.x > 180f) {
//				AutoTargetCorrectDirection.x -= 360f; 
//			}
//
//			//Debug.Log("Auto target Pitch " + AutoTargetCorrectDirection.x.ToString() + ", " +  m_Pitch.ToString() );
//			AutoTargetInputLook.x = (AutoTargetCorrectDirection.x - m_Pitch);
//			
//			if (AutoTargetInputLook.magnitude > AutoTargetSpeed*Time.deltaTime)
//			{
//				AutoTargetInputLook = AutoTargetInputLook.normalized * AutoTargetSpeed*Time.deltaTime;
//			}
//			
//			
//			
//			/*
//            Debug.Log ("AutoTargetCorrectDirection.y " + AutoTargetCorrectDirection.y.ToString());
//            Debug.Log ("m_Yaw " + m_Yaw.ToString());
//            Debug.Log ("AutoTargetInputLook.y " + AutoTargetInputLook.y.ToString());
//    */
//
//			
//		}
//		else
//		{
//			AutoTargetInputLook = Vector2.zero;
//			//AutoTargetLastTimeSearch = Time.time;
//
//		}
//	}
    
	/// <summary>
	/// Kien: update AutotargetInput cho ca AI va Player
	/// </summary>
	public void UpdateInputAutoTarget()

	{
		//Neu dang chia vao enemy roi thi thoi
		if (IsPointingToEnemy() || IsPointingToAIEnemy())  {
			AutoTargetInputLook = Vector2.zero;
			return;
		}

		OffAutoTargetTimer -= Time.deltaTime;

		if (Touchpad._isCurrentlyTweaking) {
			if (
				CnInputManager.GetAxis ("Touch Horizontal") == 0 &&
				CnInputManager.GetAxis ("Touch Vertical") == 0
			) {	
				//Bat auto target tro lai va hen gio tat
				if (!AutoTargetOn) {
					AutoTargetOn = true;
					//Set timer here
					OffAutoTargetTimer = AutoTargetTimeMax;
				}

			} 
			//Tat auto target ngay lap tuc
			else {
				AutoTargetOn = false;
				AutoTargetObject_Selected = null;
				AutoTargetInputLook = Vector2.zero;
			}
		} 
		//Neu vua moi nhac tay
		else if (PrevAimPadPressed) {
			//Bat auto target tro lai va hen gio tat
			if (!AutoTargetOn) {
				AutoTargetOn = true;
				//Set timer here
				OffAutoTargetTimer = AutoTargetTimeMax;
			}

		}

		//Tat sau 1 thoi gian, sau khi da nhac tay
		if (AutoTargetOn && OffAutoTargetTimer < 0) { 
			AutoTargetOn = false;
			AutoTargetObject_Selected = null;
			AutoTargetInputLook = Vector2.zero;
		}

		if (!AutoTargetOn) {
			AutoTargetInputLook = Vector2.zero;
			return;
		}

		//Cap nhat view ray
		//ViewRay = new Ray (transform.position, transform.forward);
		//Debug.DrawRay (transform.position, transform.forward*150f);
		if ( (AutoTargetObject_Selected != null) && (!InAutoRange(AutoTargetObject_Selected) || ViewBlocked(AutoTargetObject_Selected ) || IsTargetDead(AutoTargetObject_Selected) && (!IsPointingToEnemy () || !IsPointingToAIEnemy())) ) 
		{
			AutoTargetObject_Selected = null;
		}

		if (AutoTargetObject_Selected == null) 
		{
			//	Debug.Log("Searching for target");
			//Tim target moi

			ClosestTargetDistance = float.MaxValue;
			ClosestEnemyDistance = float.MaxValue;

			//			kienbb: check voi enemy trong list AI player
			foreach (CSVR_AutotargetMarker p in CSVR_AutotargetMarker.AIPlayer.Values)
			{
				//				Debug.Log("Check target");
				//				Neu ko phai co tag cua enemy thi continue
				if ( p == null || (TeamNumber != 0 && p.TeamNumber == TeamNumber )
					|| 
					(p.transform.root == transform.root)
				)
				{
					//					Debug.Log("Khong phai enemy " + p.name);
					continue;
				}

				//				Enemy o xa hon 1 target da tim duoc
				if (Vector3.Distance(p.transform.position, transform.position) > ClosestEnemyDistance )
				{
					continue;
				}

				if (p.DeadActive || !InAutoRange( p.MiddleBodyTransform))
				{
					//					Debug.Log("Enemy not in auto range");
					continue;
				}

				//				Debug.Log("Check collider Player");
				foreach(Collider c in p.BodyColliders)
				{
					ViewPortPoint = GetCamera().WorldToViewportPoint(c.transform.position);

					ViewPortDistance = Vector2.Distance(
						ViewPortCentre,
						new Vector2(ViewPortPoint.x, ViewPortPoint.y)
					) ;
					if (
						ViewPortDistance
						< ClosestTargetDistance
					) 
					{

						if (!ViewBlocked(c.transform)) 
						{	ClosestTargetDistance = ViewPortDistance;
							AutoTargetObject_Selected = c.transform;
//							Debug.Log ("AutoTargetObject_Selected = " + c.transform.root.name);
						}
					}
				}

				if (AutoTargetObject_Selected != null) 
				{
					ClosestEnemyDistance = Vector3.Distance(AutoTargetObject_Selected.position, transform.position);
				}
			}

			//kienbb: check voi enemy trong list player
			foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
			{
				//Debug.Log("Check target");
				//Neu ko phai co tag cua enemy thi continue
				if ( p == null || (TeamNumber != 0 && p.TeamNumber == TeamNumber )
					|| 
					(p.transform.root == transform.root)
				)
				{
					//					Debug.Log("Khong phai enemy " + p.name);
					continue;
				}

				//Enemy o xa hon 1 target da tim duoc
				if (Vector3.Distance(p.transform.position, transform.position) > ClosestEnemyDistance )
				{
					continue;
				}

				if (p.Player.Dead.Active || !InAutoRange( p.transform.GetComponentInChildren<CSVR_BodyAnimator>().LowestSpineBone.transform ))
				{
					//Debug.Log("Enemy not in auto range");
					continue;
				}

				//Debug.Log("Check collider Player");
				foreach(Collider c in p.GetComponentInChildren<vp_RagdollHandler>().Colliders)
				{
					ViewPortPoint = GetCamera().WorldToViewportPoint(c.transform.position);

					ViewPortDistance = Vector2.Distance(
						ViewPortCentre,
						new Vector2(ViewPortPoint.x, ViewPortPoint.y)
					) ;
					if (
						ViewPortDistance
						< ClosestTargetDistance
					) 
					{

						if (!ViewBlocked(c.transform)) 
						{	ClosestTargetDistance = ViewPortDistance;
							AutoTargetObject_Selected = c.transform;
						}
					}
				}

				if (AutoTargetObject_Selected != null) 
				{
					ClosestEnemyDistance = Vector3.Distance(AutoTargetObject_Selected.position, transform.position);
				}
			}

//			if (AutoTargetObject_Selected != null) 
//			{
//				ClosestEnemyDistance = Vector3.Distance(AutoTargetObject_Selected.position, transform.position);
//			}
//

		} 

		/*
		Chỉ để test
		GameObject dummy =  GameObject.FindGameObjectWithTag ("dummyTarget");

		if (dummy != null)
			AutoTargetObject_Selected = dummy.transform;

*/
		//Tinh toán xem di tâm ngắm bao nhiêu
		//Neu la sung thuong thi auto khong dang khong giu nut
		if (AutoTargetObject_Selected != null )
		{


			//Ngam vao dau head
			AutoTargetCorrectDirection = Quaternion.LookRotation(AutoTargetObject_Selected.transform.position - transform.position).eulerAngles;


			/*
            if (AutoTargetCorrectDirection.y < 0)
                AutoTargetCorrectDirection.y += 360f;
            if (m_Yaw < 0)
                m_Yaw += 360f;
    */
			AutoTargetInputLook.y = (AutoTargetCorrectDirection.y - m_Yaw) % 360f;

			//Chon duong di ngan nhat
			if (AutoTargetInputLook.y > 180f)
			{
				AutoTargetInputLook.y = AutoTargetInputLook.y - 360f; //m_Yaw phai giam dan
			}
			else if (AutoTargetInputLook.y < -180f)
			{
				AutoTargetInputLook.y = 360f + AutoTargetInputLook.y; //m_Yaw phai tang len
			}

			//	if (m_Pitch < 0)
			//		m_Pitch += 180f;

			if (AutoTargetCorrectDirection.x > 180f) {
				AutoTargetCorrectDirection.x -= 360f; 
			}

			//Debug.Log("Auto target Pitch " + AutoTargetCorrectDirection.x.ToString() + ", " +  m_Pitch.ToString() );
			AutoTargetInputLook.x = (AutoTargetCorrectDirection.x - m_Pitch);

			if (AutoTargetInputLook.magnitude > AutoTargetSpeed*Time.deltaTime)
			{
				AutoTargetInputLook = AutoTargetInputLook.normalized * AutoTargetSpeed*Time.deltaTime;
			}



			/*
            Debug.Log ("AutoTargetCorrectDirection.y " + AutoTargetCorrectDirection.y.ToString());
            Debug.Log ("m_Yaw " + m_Yaw.ToString());
            Debug.Log ("AutoTargetInputLook.y " + AutoTargetInputLook.y.ToString());
    */


		}
		else
		{
			AutoTargetInputLook = Vector2.zero;
			//AutoTargetLastTimeSearch = Time.time;

		}
	}


//	private Ray CrosshairTargetObject_Ray;
//	private RaycastHit CrosshairTargetObject_Hit;
//	private int layerMaskT = 1 << 29 ;
//	private CSVR_ServerConnector serverConnector;
//	private CSVR_PlayerDamageHandler playerDamageHandler;
//	public static string CrosshairTargetObject_Name = string.Empty;
//	public static string CrosshairTargetObject_Heath = string.Empty;
//	public static bool CrosshairTargetObject_Enemy = true;
    
//	protected override void Update()
//	{
//		base.Update();
//
//		CrosshairTargetObject_Ray = new Ray(transform.position, transform.forward);
//
//		if (Physics.Raycast(CrosshairTargetObject_Ray, out CrosshairTargetObject_Hit,250,layerMaskT))
//		{
//			if ((CrosshairTargetObject_Hit.collider.tag == "PolicePlayer" || CrosshairTargetObject_Hit.collider.tag == "TerrorisPlayer") && !ViewBlocked(CrosshairTargetObject_Hit.transform))
//			{
//				if(serverConnector == null){
//					serverConnector = CrosshairTargetObject_Hit.collider.gameObject.GetComponent<CSVR_ServerConnector>();
//					if (CrosshairTargetObject_Hit.collider.gameObject.GetComponent<CSVR_PlayerDamageHandler>() != null)
//					{
//						playerDamageHandler = CrosshairTargetObject_Hit.collider.gameObject.GetComponent<CSVR_PlayerDamageHandler>();
//						CrosshairTargetObject_Heath = (int)playerDamageHandler.CurrentHealth + "%";
//					}
//
//					CrosshairTargetObject_Name = serverConnector.PlayfabName;
//
//					//Đấu đơn
//					if(serverConnector.idTeams == 0){
//						CrosshairTargetObject_Enemy = true;
//						return;
//					}
//					//Đấu đội
//					if(serverConnector.idTeams == TeamNumber){
//						CrosshairTargetObject_Enemy = false;
//					}else{
//						CrosshairTargetObject_Enemy = true;
//					}
//				}
//			}
//		}else{
//			if(serverConnector != null){
//				serverConnector = null;
//				playerDamageHandler = null;
//				CrosshairTargetObject_Name = string.Empty;
//				CrosshairTargetObject_Heath = string.Empty;
//			}
//		}
//	}

	/// <summary>
	/// Kien: cho ca AI va Player
	/// </summary>
	private Ray CrosshairTargetObject_Ray;
	private RaycastHit CrosshairTargetObject_Hit;
	private int layerMaskT = (1 << vp_Layer.Ragdoll | 1 << vp_Layer.RemotePlayer) ;
	private CSVR_ServerConnector serverConnector;
	private vp_DamageHandler playerDamageHandler;
	public static string CrosshairTargetObject_Name = string.Empty;
	public static string CrosshairTargetObject_Heath = string.Empty;
	public static bool CrosshairTargetObject_Enemy = true;

	//kienbb: cho AI
	CSVR_AutotargetMarker AIAutotargetMarker;
	protected override void Update()
	{
		base.Update();

		CrosshairTargetObject_Ray = new Ray(transform.position, transform.forward);

		if (Physics.Raycast (CrosshairTargetObject_Ray, out CrosshairTargetObject_Hit, 250, layerMaskT)) 
		{
			if(CrosshairTargetObject_Hit.collider.transform.root == transform.root)
				return;
			if ((CrosshairTargetObject_Hit.collider.transform.root.CompareTag(GameConstants.VNTAG_Police) || CrosshairTargetObject_Hit.collider.transform.root.CompareTag(GameConstants.VNTAG_Terrorist)) && !ViewBlocked (CrosshairTargetObject_Hit.transform))
			{
				if (serverConnector == null)
				{
					serverConnector = CrosshairTargetObject_Hit.collider.transform.root.GetComponent<CSVR_ServerConnector> ();

					playerDamageHandler = CrosshairTargetObject_Hit.collider.transform.root.GetComponent<CSVR_PlayerDamageHandler> ();
					if (playerDamageHandler != null) 
					{
						CrosshairTargetObject_Heath = (int)playerDamageHandler.CurrentHealth + "%";
					}

					CrosshairTargetObject_Name = serverConnector.PlayfabName;

//					Đấu đơn
					if (serverConnector.idTeams == 0) 
					{
//						print ("dau don, khong doi mau tam");
						CrosshairTargetObject_Enemy = true;
						return;
					}
//					Đấu đội
					if (serverConnector.idTeams == TeamNumber) 
					{
//						print ("dau doi, cung team");
						CrosshairTargetObject_Enemy = false;
					}
					else 
					{
//						print ("dau doi, khac team");
						CrosshairTargetObject_Enemy = true;
					}


				}
			}


			if (CrosshairTargetObject_Hit.collider.transform.root.CompareTag ("TeamAI") && !ViewBlocked (CrosshairTargetObject_Hit.transform))
			{
				if(AIAutotargetMarker == null)
				{
					AIAutotargetMarker = CrosshairTargetObject_Hit.transform.root.GetComponent<CSVR_AutotargetMarker> ();
					CrosshairTargetObject_Heath = ((int)AIAutotargetMarker.CurrentHP).ToString () + "%";
					CrosshairTargetObject_Name = AIAutotargetMarker.CurrentName;

					if (AIAutotargetMarker.TeamNumber == 0) {
						CrosshairTargetObject_Enemy = true;
						return;
					}
//					//					Đấu đơn
//					if (AIAutotargetMarker.TeamNumber == 0) 
//					{
//						CrosshairTargetObject_Enemy = true;
//						return;
//					}
//					Đấu đội
					if (AIAutotargetMarker.TeamNumber == TeamNumber) {
						CrosshairTargetObject_Enemy = false;
					} else {
						CrosshairTargetObject_Enemy = true;
					}

				}
			}

		} 
		else 
		{
			if(serverConnector != null)
			{
				serverConnector = null;
				playerDamageHandler = null;
			}
			CrosshairTargetObject_Enemy = false;
			CrosshairTargetObject_Name = string.Empty;
			CrosshairTargetObject_Heath = string.Empty;

			AIAutotargetMarker = null;
		}

	}





}

