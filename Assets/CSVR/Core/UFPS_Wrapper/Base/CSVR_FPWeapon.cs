﻿using UnityEngine;
using System.Collections;

public class CSVR_FPWeapon : vp_FPWeapon {

	/// <summary>
	/// 
	/// </summary>
	protected override void FixedUpdate()
	{
		
		if (Time.timeScale != 0.0f)
		{
			
			UpdateZoom();
	/*		
			UpdateSwaying();
			
			UpdateBob();
			
			UpdateEarthQuake();

			UpdateStep();

			UpdateShakes();

			UpdateRetraction();
*/
			UpdatePosition();

		}
		
	}

	/// <summary>
	/// 
	/// </summary>
	protected  void UpdatePosition()
	{
		
		m_PositionSpring.FixedUpdate();
		m_PositionPivotSpring.FixedUpdate();

		
	}

}
