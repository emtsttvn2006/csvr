﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;


public class CSVR_FPWeaponHandler : vp_FPWeaponHandler
{
    public Transform weaponGroup;

    /*
    public override void InitWeaponLists()
    {
		Debug.Log (transform.name + "CSVR_FPWeaponHandler.InitWeaponLists");
        // first off, always store all weapons contained under our main FPS camera (if any)
        List<vp_Weapon> camWeapons = null;

        vp_FPCamera camera = transform.GetComponentInChildren<vp_FPCamera>();

        if (camera != null)
        {
            //Horus begin
            //camWeapons = GetWeaponList(Camera.main.transform);
            camWeapons = GetWeaponList(weaponGroup);
            //Horus end
            if ((camWeapons != null) && (camWeapons.Count > 0))
                m_WeaponLists.Add(camWeapons);

        }

        List<vp_Weapon> allWeapons = new List<vp_Weapon>(transform.GetComponentsInChildren<vp_Weapon>());

        // if the camera weapons were all the weapons we have, return
        if ((camWeapons != null) && (camWeapons.Count == allWeapons.Count))
        {
            Weapons = m_WeaponLists[0];
            return;
        }

        // identify every unique gameobject that holds weapons as direct children
        List<Transform> weaponContainers = new List<Transform>();
        foreach (vp_Weapon w in allWeapons)
        {

            if ((camera != null) && camWeapons.Contains(w))
                continue;
            if (!weaponContainers.Contains(w.Parent))
                weaponContainers.Add(w.Parent);
        }

        // create one weapon list for every container found
        foreach (Transform t in weaponContainers)
        {
            List<vp_Weapon> weapons = GetWeaponList(t);
            DeactivateAll(weapons);
            m_WeaponLists.Add(weapons);
        }

        // abort and disable weapon handler in case no weapons were found
        if (m_WeaponLists.Count < 1)
        {
            
            enabled = false;
            return;
        }

        // start out with the first weapon list by default. on a 1st person
        // player, this would typically be the weapons stored under the camera
        Weapons = m_WeaponLists[0];

    }

   */
}
