﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_RagdollHandler : vp_RagdollHandler
{

    public CSVR_BodyAnimator BodyAnimator;
    public Animator Animator;

    public Rigidbody HitRigidBody;
    public Vector3 HitImpact;

    protected override void Awake()
    {

        // verify that we have all the required components
        // NOTE: CharacterController and vp_FPCamera are optional and only
        // pertain to local player
        if (((Colliders == null) || (Colliders.Count == 0)) ||
            ((Rigidbodies == null) || (Rigidbodies.Count == 0)) ||
            ((Transforms == null) || (Transforms.Count == 0)) ||
            (Animator == null) ||
            (BodyAnimator == null))
        {

            this.enabled = false;
            return;
        }

        SaveStartPose();	// why is this not referenced? evaluate!

    }
    protected override void Start()
    {

        SetRagdoll(false);
        BodyAnimator = GetComponent<CSVR_BodyAnimator>();
        Animator = GetComponent<Animator>();
    }
    public override void SetRagdoll(bool enabled = true)
    {
        if (enabled)
        {

            // only allow ragdolling of dead players. this cancels any postponed
            // ragdolling when player has come back alive
            if (!Player.Dead.Active)
                return;
            /*
            // only allow one 'postponement' at a time
            PostponeTimer.Cancel();

            if (Animator.GetBool("IsGrounded"))	// fetching grounding from the Animator is more reliable for (lerped) remote players
            {
                Debug.Log ("SetRagdoll 1");
                vp_Timer.In(0.1f, () => SetRagdoll(true), PostponeTimer);
                return;
            }
            //else Debug.Log("Grounded -> RAGDOLLING");
            */

        }
        // --- toggle components that conflict with ragdoll physics ---


        if (Animator != null)
        {
            Animator.enabled =!enabled;
        }

        if (BodyAnimator != null)
            BodyAnimator.enabled = !enabled;
        if (Controller != null)
            Controller.EnableCollider(!enabled);

        // --- disable / enable rigidbodies and colliders ---
       
            foreach (Rigidbody r in Rigidbodies)
            {
                r.isKinematic = !enabled;
            }
        if (enabled == false)
        {
            foreach (Collider c in Colliders)
            {
                c.enabled = true;
                c.isTrigger = true;
            }
        }
        else
        {

            foreach (Collider c in Colliders)
            {
                c.enabled = true;
                c.isTrigger = false;
            }
        }

        // if disabling, restore the initial state of all the rigidbodies
        if (!enabled)
        {
            RestoreStartPose();
        }
        else {
            //Tạo 1 lực thẳng xuống đầu để xác nằm vật luôn luôn
            if  (HitRigidBody != null && HitImpact !=null) {
                HitRigidBody.AddForce(HitImpact, ForceMode.VelocityChange);
                }
        }
    }

    protected override void SaveStartPose()
    {
        foreach (Transform t in Transforms)
        {
            if (!TransformRotations.ContainsKey(t))
                TransformRotations.Add(t.transform, t.localRotation);
            if (!TransformPositions.ContainsKey(t))
                TransformPositions.Add(t.transform, t.localPosition);
        }
    }

    protected override void OnStart_Dead()
    {

        m_TimeOfDeath = Time.time;	// store time of death for death camera (if any)

        // ragdoll! but not until next frame for more reliable 'grounded' state
        vp_Timer.In(0, () => { SetRagdoll(true); });
    }


    /// <summary>
    /// this triggers when the player respawns, disabling ragdoll
    /// physics and restoring all limbs to their initial state
    /// </summary>
    protected override void OnStop_Dead()
    {
        SetRagdoll(false);

        Player.OutOfControl.Stop();

    }
}
