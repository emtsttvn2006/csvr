﻿using UnityEngine;
using System.Collections;

public class CSVR_FPBodyAnimator : CSVR_BodyAnimator
{
    protected vp_FPController m_FPController = null;
    protected vp_FPController FPController
    {
        get
        {
            if (m_FPController == null)
            {
                m_FPController = transform.root.GetComponent<vp_FPController>();
            }
            return m_FPController;
        }
    }
    protected override void UpdateGrounding()
    {
        // TODO: now supported in vp_Controller, remove?
        m_Grounded = FPController.Grounded;

    }
}
