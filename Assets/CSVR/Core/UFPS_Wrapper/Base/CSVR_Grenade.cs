﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]


public class CSVR_Grenade : MonoBehaviour
{
    Material mat;
	public float LifeTime = 3.0f;
	public float RigidbodyForce = 10.0f;		// this force will be applied to the rigidbody when spawned
	public float RigidbodySpin = 0.0f;			// this much random torque will be applied to rigidbody when spawned.
	// NOTE: spin is currently not recommended for use with the UFPS multiplayer add-on, since rigidbodies are not yet serialized!

	public float Damage = 0f;
	public float Range = 0f;

	//Thong tin ve nguoi ban    
	public string Shooter_PlayfabID;
	public string Shooter_Name;
	public string GunID,GunName;
    public int idPhotonView = 0;
	public string Level;
	public int idTeam;

	protected Rigidbody m_Rigidbody = null;
	protected Transform m_Source = null;				// immediate cause of the damage
	protected Transform m_OriginalSource = null;		// initial cause of the damage


	public GameObject [] DeathSpawnObjects = null;	
	
	/// <summary>
	/// 
	/// </summary>
	protected virtual void Awake()
	{
		gameObject.layer = LayerMask.NameToLayer ("Grenade");
		m_Rigidbody = GetComponent<Rigidbody>();
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	void Start()
	{
		//Yo_SearchAndDestroy
		if (GunID == GameConstants.VNMODESAD_BombC4){
			vp_SADMaster.C4IsPlanted = true;
			CSVR_UIPlay.instance.btnGetC4.gameObject.SetActive (false);
			if (MapCanvasController.Instance != null) {
				
				if (vp_SADMaster.BombSiteName == GameConstants.VNMODESAD_BombSiteA) {
					UnityEngine.UI.Image a = MapCanvasController.Instance.MarkerGroup.transform.FindChild (GameConstants.VNMODESAD_BombSiteA).GetComponent<UnityEngine.UI.Image> ();
					if (a != null) {
						a.sprite = MapCanvasController.Instance.bombSiteSprites [1];
					}
				} 
				if (vp_SADMaster.BombSiteName == GameConstants.VNMODESAD_BombSiteB) {
					UnityEngine.UI.Image b = MapCanvasController.Instance.MarkerGroup.transform.FindChild (GameConstants.VNMODESAD_BombSiteB).GetComponent<UnityEngine.UI.Image> ();
					if (b != null) {
						b.sprite = MapCanvasController.Instance.bombSiteSprites [3];
					}
				}
			}
		}
        //m_Rigidbody.AddForce(transform.forward * RigidbodyForce); 
		// destroy the grenade object in 'lifetime' seconds. this will only work
		// if the object has a vp_DamageHandler-derived component on it
		try{
			vp_Timer.In(LifeTime, delegate()
			            {
				foreach (GameObject o in DeathSpawnObjects)
				{
					if (o != null)
					{
						try{
							GameObject g = (GameObject)vp_Utility.Instantiate(o, transform.position, transform.rotation);

	                    
							CSVR_Explosion explosion =  g.GetComponent<CSVR_Explosion>();
							if (explosion != null) { 
							
								//Dua thong tin vao luu dan
								explosion.Shooter_PlayfabID = Shooter_PlayfabID;
								explosion.Shooter_Name = Shooter_Name;
								explosion.GunID = GunID;

								explosion.GunName = GunName;
								explosion.Level = Level;
								explosion.idTeam = idTeam;

								explosion.Damage = Damage;
								explosion.Radius = Range;
		                        explosion.idPhotonView = idPhotonView;
								}
							}catch{
								//Destroy(this.transform.root.gameObject);
								return;
							}
	                     
					}
				}
			
				Destroy(this.transform.root.gameObject);
			});
		}catch{
			Destroy(this.transform.root.gameObject);
			//return;
			//bomb da duoc go
		}

	}
    //static float Force = 0f;
    
	/// <summary>
	/// 
	/// </summary>
	protected virtual void OnEnable()
	{

     /*   if (CSVR_FPWeaponShooter.instance == null) return;
        if (CSVR_FPWeaponShooter.instance.CameraAngleY < 0)
        {            
            Force = (RigidbodyForce / 2f) + (Mathf.Abs(CSVR_FPWeaponShooter.instance.CameraAngleY) * 0.16f);
        }
        else if (CSVR_FPWeaponShooter.instance.CameraAngleY > 0)
        {
            Force = (RigidbodyForce / 2f) - (CSVR_FPWeaponShooter.instance.CameraAngleY * 0.25f);
        }
        else
        {
            Force = (RigidbodyForce / 2f);
        }
		if (m_Rigidbody == null)
			return;
		
		// apply force on spawn
		if (RigidbodyForce != 0.0f)
            m_Rigidbody.AddForce((transform.forward * Force), ForceMode.Impulse); 
		if (RigidbodySpin != 0.0f)
			m_Rigidbody.AddTorque(Random.rotation.eulerAngles * RigidbodySpin);
		*/

		if (m_Rigidbody == null)
			return;

		//Debug.Log ("Goc nem luu " + transform.forward.ToString());

		// apply force on spawn
		if (RigidbodyForce != 0.0f)
			m_Rigidbody.AddForce((transform.forward * RigidbodyForce*(  Mathf.Min( transform.forward.y + 1f, 1.5f)*Mathf.Min( transform.forward.y + 1f, 1.5f) ) ),
			                     ForceMode.Impulse); 
		
	}
	
	/// <summary>
	/// sets the inflictor (original source) of any resulting damage.
	/// this is called by the 'vp_Shooter' script and is picked up by
	/// various other scripts, especially in UFPS multiplayer add-on.
	/// NOTE: this method must be called between spawning, and before
	/// 'Start' is called.
	/// </summary>
	public void SetSource(Transform source)
	{
		m_Source = transform;
		m_OriginalSource = source;
	}
	/*
	void OnCollisionEnter(Collision collision) {
		//Debug.Log ("Luu dan cham " + collision.transform.name);		
		foreach (ContactPoint contact in collision.contacts) {
			Debug.Log(contact.point.ToString("F10"));
		}
	}
	*/
	
}