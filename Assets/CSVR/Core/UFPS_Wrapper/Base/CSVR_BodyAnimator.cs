﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animator))]

public class CSVR_BodyAnimator : vp_BodyAnimator 
{

//  Original file already contain all below properties
//  --------------------------------------------------
//	protected int ForwardAmount;
//	protected int StrafeAmount;

//	protected int WeaponGripIndex;
//	protected int WeaponTypeIndex;
//	protected int IsReloading;
//	protected int IsMoving;
//	protected int IsAttacking;
//  --------------------------------------------------
	protected int PitchAmout;

	protected int IsIdling;
	protected int IsJumping;
	protected int IsCrouch;

	protected int IsGetGun;
	
	protected int IsDead;
	protected int IsFalldown;

	protected int IsTakeDamage;


	public vp_Controller  Controller;


	protected override void Awake()
	{
		Controller = transform.root.GetComponent<vp_Controller> ();

		if (!IsValidSetup())
			return;
		
		InitHashIDs();
		
		InitHeadLook();
		
		InitMaxSpeeds();
        
		//Player.IsFirstPerson.Set(false);	// TEMP: checking to see if commenting this out causes trouble



		
	}



	protected override void InitHashIDs()
	{
		//Store float decide to move forward, aim up or down and move right or left
		ForwardAmount = Animator.StringToHash("Forward");
		PitchAmount = Animator.StringToHash("Pitch");
		StrafeAmount = Animator.StringToHash("Strafe");
		
		//Store boolean decide to animating player event
		IsMoving = Animator.StringToHash("IsMoving");
		IsIdling = Animator.StringToHash("IsIdling");
		IsAttacking = Animator.StringToHash("IsAttacking");
		IsJumping = Animator.StringToHash("IsJumping");
		IsCrouch = Animator.StringToHash("IsCrouch");
		IsReloading = Animator.StringToHash("IsReloading");
		IsGetGun = Animator.StringToHash("IsGetGun");
		IsGrounded = Animator.StringToHash("IsGrounded");
		
		//Store boolean decide to choose which sub-state machine will using
		WeaponGripIndex = Animator.StringToHash("WeaponGripIndex");
		WeaponTypeIndex = Animator.StringToHash("WeaponTypeIndex");
		//Store trigger decide to animating death states
		IsDead = Animator.StringToHash("IsDead");
		IsFalldown = Animator.StringToHash("IsFalldown");
		IsTakeDamage = Animator.StringToHash("IsTakeDamage");
	}

	public void SetupAnimator(int CurrentWeaponType)
	{
		Animator.SetInteger(WeaponTypeIndex, CurrentWeaponType);        
	}
	protected override void UpdateAnimator()
	{
        //Animator.SetInteger(WeaponTypeIndex, Player.CurrentWeaponIndex.Get());
        //Animator.SetBool(IsJumping, Player.Jump.Active);
		Animator.SetBool(IsCrouch, Player.Crouch.Active);
		Animator.SetBool(IsReloading, Player.Reload.Active);
		Animator.SetBool(IsGrounded,m_Grounded);
		
		
		Animator.SetFloat(ForwardAmount, m_CurrentForward);
		Animator.SetFloat(StrafeAmount, m_CurrentStrafe);
		Animator.SetFloat(PitchAmount, (-Player.Rotation.Get().x) / 90.0f);
	}

	protected override void UpdateAnimationSpeeds()
	{

		//Turn
		if (Time.time > m_NextAllowedUpdateTurnTargetTime)
		{
			m_CurrentTurnTarget = ((Mathf.DeltaAngle(m_PrevBodyYaw, m_BodyYaw)) * (Player.Crouch.Active ? CROUCHTURNMODIFIER : TURNMODIFIER));
			m_NextAllowedUpdateTurnTargetTime = Time.time + 0.05f;
		}

		if (Player.Platform.Get() == null  || !Player.IsLocal.Get())
		{
			m_CurrentTurn = m_CurrentTurnTarget;
			if (Mathf.Round(Transform.root.eulerAngles.y) == Mathf.Round(m_LastYaw))
				m_CurrentTurn *= 0.6f;
			m_LastYaw = Transform.root.eulerAngles.y;
			m_CurrentTurn = vp_MathUtility.SnapToZero(m_CurrentTurn);
		}
		else
			m_CurrentTurn = 0.0f;	// turn logic does not work very well locally on platforms

		//Forward motion, assign current forward amount to local velocity over delta time multi move modifier
		m_CurrentForward = Mathf.Lerp(m_CurrentForward, GetForwardDirection(), Time.deltaTime * 50);
		m_CurrentForward = Mathf.Abs(m_CurrentForward) > 0.03f ? m_CurrentForward : 0.0f;

		//Strafe motion, assign current strafe amount to player input vector over delta timel
		m_CurrentStrafe = Mathf.Lerp(m_CurrentStrafe, GetStrafeDirection(), Time.deltaTime * 50);
		m_CurrentStrafe = Mathf.Abs(m_CurrentStrafe) > 0.03f ? m_CurrentStrafe : 0.0f;

		if(Mathf.Abs(m_CurrentForward) > 0.02f || 
		   Mathf.Abs(m_CurrentStrafe) > 0.02f)
		{
			Animator.SetBool(IsMoving,true);
			
		}
		else
		{
			Animator.SetBool(IsMoving,false);			
		}
	}

	protected override float GetStrafeDirection()
	{
		return Player.InputMoveVector.Get().x;
	}

	protected float GetForwardDirection()
	{
		return Player.InputMoveVector.Get().y;
	}

	protected override void OnStart_Attack()
	{
		
		// TEMP: time body animation better to throwing weapon projectile spawn
		if (Player.CurrentWeaponType.Get() == (int)vp_Weapon.Type.Thrown)
		{
			vp_Timer.In(WeaponHandler.CurrentWeapon.GetComponent<vp_Shooter>().ProjectileSpawnDelay * 0.7f, () =>
			            {
				m_AttackDoneTimer.Cancel();
				Animator.SetBool(IsAttacking, true);
				OnStop_Attack();
				
			});
		}
		else
			Animator.SetBool(IsAttacking, true);
	}
	
	protected override void OnStop_Attack()
	{
		vp_Timer.In(0.5f,delegate(){
			Animator.SetBool(IsAttacking, false);
			RefreshWeaponStates();
		}, m_AttackDoneTimer);
	}

	protected override void OnStart_OutOfControl()
	{
		Animator.SetBool(IsJumping,Player.Jump.Active);
	}

	protected override void OnStart_Reload()
	{
		Animator.SetBool(IsReloading,true);
	}
   
	protected override void LateUpdate()
	{
		
		if (Time.timeScale == 0.0f)
			return;
		
		if (!m_IsValid)
		{
			this.enabled = false;
			return;
		}
        
        /*
        if (!m_Player.Dead.Active && m_Player.Health.Get() < 0f)
        {
            Animator.SetTrigger("Die");
        }
        Animator.SetBool("isDead", m_Player.Dead.Active);
         */
		UpdatePosition();
		
		UpdateGrounding();
		
//		UpdateBody();
		
//		UpdateSpine();

		UpdateAnimator();
		
	    UpdateAnimationSpeeds();
		
//	    UpdateDebugInfo();
		
	    UpdateHeadPoint();
		
	}

	protected override void UpdateGrounding()
	{
		// TODO: now supported in vp_Controller, remove?
		m_Grounded = Controller.Grounded;

		
	}
}
