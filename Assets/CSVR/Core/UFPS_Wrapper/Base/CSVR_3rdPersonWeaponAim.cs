﻿using UnityEngine;
using System.Collections;

public class CSVR_3rdPersonWeaponAim : vp_3rdPersonWeaponAim {
	/// <summary>
	/// 
	/// </summary>
	protected override void Awake()
	{
	
		
		#if UNITY_EDITOR
		KeepAiming = false;
		#endif
		m_DefaultRotation = Transform.localRotation;
		
		// store reference up- and forward directions
		if ((LowerArmObj == null) || (HandObj == null))
		{
			
			this.enabled = false;
			return;
		}
		
		Quaternion parentRotInv = Quaternion.Inverse(LowerArmObj.rotation);
		m_ReferenceLookDir = (parentRotInv * Root.rotation * Vector3.forward);
		m_ReferenceUpDir = (parentRotInv * Root.rotation * Vector3.up);
		
		// save difference between initial rotation and world rotation
		Quaternion currentRot = HandObj.rotation;
		HandObj.rotation = Root.rotation;
		Quaternion targetRot = HandObj.rotation;
		HandObj.rotation = currentRot;
		m_HandBoneRotDif = Quaternion.Inverse(targetRot) * currentRot;
		
	}

	protected override void LateUpdate()
	{
		
	}

	protected override void OnEnable()
	{

		
		base.OnEnable ();
	} 

	protected override void OnDisable()
	{

		
		base.OnDisable ();
	} 

}
