﻿using UnityEngine;
using System.Collections;

public class CSVR_BulletTracer : MonoBehaviour {

	//public float mLifeTime = 0.05f;
	//public LineRenderer mLineRenderer;
	public ParticleSystem mParticleSystem = null;

	public float FireRate = 0f;
	public bool mFirstShot = false;

	public ParticleSystem BulletParticleSystem{
		get {
			if (mParticleSystem == null) {
				mParticleSystem = GetComponent<ParticleSystem>();
			}
			return mParticleSystem;
		}
	}

	void OnEnable() {
		

		HideTracer ();
	}

	public void ShowTracer() {
		//BulletParticleSystem.emissionRate = FireRate;
		mFirstShot = true;

	}

	public void SetDirection(Vector3 StartPoint, Vector3 EndPoint) {
		if (mFirstShot) {
			transform.position = StartPoint;
			transform.LookAt (EndPoint);
			transform.position += transform.forward*4f;
			BulletParticleSystem.startLifetime = Vector3.Distance (transform.position, EndPoint)/  BulletParticleSystem.startSpeed;
			//BulletParticleSystem.Play ();
			BulletParticleSystem.Emit (1);
			mFirstShot = false;
		}

	}
	
	public void HideTracer()
	{/*
		vp_Timer.In (0.5f/ FireRate, delegate() {
			Debug.Log ("HideTracer delay");
			//BulletParticleSystem.emissionRate = 0f;
			mFirstShot = false;
			BulletParticleSystem.Stop();
		});
*/
	}

}
