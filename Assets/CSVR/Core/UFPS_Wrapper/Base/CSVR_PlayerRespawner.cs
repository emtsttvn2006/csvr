using UnityEngine;
using System.Collections;

public class CSVR_PlayerRespawner : vp_PlayerRespawner
{
	public bool ReadyToPlay = false; //Đã sẵn sàng xong hết các thứ để có thể chơi
	bool FirstSpawn = true;
	public bool RespawnNoActionTimeFinished = true;
	public float RespawnNoActionTime = 5;
	public GameObject respawnEffect;
	CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
	CSVR_ServerConnector serverConnectorInstance
	{
		get
		{
			if (m_serverConnectorInstance == null)
			{
				m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
			}
			return m_serverConnectorInstance;
		}
	} 

	vp_MPNetworkPlayer m_NetworkPlayer; // Dung de lay thong tin ve nguoi ban
	vp_MPNetworkPlayer m_NetWork
	{
		get
		{
			if (m_NetworkPlayer == null)
			{
				m_NetworkPlayer = transform.root.GetComponent<vp_MPNetworkPlayer>();
			}
			return m_NetworkPlayer;
		}
	}  



	public Renderer efx_Respaw;
	protected override void Awake()
	{
		switch (CSVR_GameSetting.modeName)
		{
		case "CSVR_ZombieSurvival":
			m_ObstructionSolver = ObstructionSolver.Wait;
			ObstructionRadius = 0f;
			break;
		default:
			m_ObstructionSolver = ObstructionSolver.AdjustPlacement;
			ObstructionRadius = 1f;
			break;
		}
		base.Awake();
		//instance = this;
	}
	protected override void OnEnable()
	{


		base.OnEnable();

		RespawnNoActionTimeFinished = false;

		OnFirstSpawn ();

	}

	public override void RespawnMode()
	{

		if(CSVR_UIPlayManage.init != null)
			CSVR_UIPlayManage.init.DisableMission();
		if(CSVR_UIPlay.instance != null)
			CSVR_UIPlay.instance.UI_OnEnable();
		if(CSVR_UIKillCam.instance != null)
			CSVR_UIKillCam.instance.UI_OnDisable();

		OnPlayerRespawn ();

		ReadyToPlay = false;
		RespawnNoActionTimeFinished = false;
		base.Respawn();   

		CSVR_FPCamera localCamera = GetComponentInChildren<CSVR_FPCamera> ();
		if (localCamera != null) localCamera.ResetAutoTarget();

		//if (PhotonNetwork.isMasterClient) {
		serverConnectorInstance.RestoreBackpack();
		//}

		serverConnectorInstance.OriginalSpawnPoint = transform.position; //De enable disable balo

	}

	public override void Respawn()
	{
		//Debug.Log ("Bắt đầu Respawn from " + transform.name);
		OnPlayerRespawn ();

		ReadyToPlay = false;
		RespawnNoActionTimeFinished = false;
		base.Respawn();   

		CSVR_FPCamera localCamera = GetComponentInChildren<CSVR_FPCamera> ();
		if (localCamera != null) localCamera.ResetAutoTarget();

		//if (PhotonNetwork.isMasterClient) {
		serverConnectorInstance.RestoreBackpack();
		//}

		serverConnectorInstance.OriginalSpawnPoint = transform.position; //De enable disable balo

	}


	IEnumerator DisableRespawnEffect(float _timeDelay)
	{
		if (serverConnectorInstance.PlayfabID == HorusManager.instance.playerID) {
			switch (CSVR_GameSetting.modeName) {
			case "CSVR_DeadOrAlive":
			case "CSVR_SearchAndDestroy":
				CSVR_GetInfo.instance.NewRound ();
				break;
			}
		}
		yield return new WaitForSeconds(_timeDelay);
		StopRespawnEffect ();
	}


	public void StopRespawnEffect() {
		if (respawnEffect != null)
		{			
			respawnEffect.SetActive(false);
		}

		//Dua doan nay vao day de ko bi mat mau luc hoi sinh
		try{

			if (GetComponent<CSVR_FPInputMobile>() != null)
			{
				GetComponent<CSVR_FPInputMobile>().imgLoadbar.fillAmount = 0;
				GetComponent<CSVR_FPInputMobile>().loadBarObject.SetActive(false);                
			}
		}
		catch{

		}

		RespawnNoActionTimeFinished = true;

		//Đoạn này để buộc nhân vật này các máy khác phải dừng khoảng thời gian hồi sinh, nếu chưa dừng do lag
		if (PhotonNetwork.isMasterClient) {
			GetComponent<vp_MPNetworkPlayer>().TransmitStopRespawnEffect();
		}
	}

	public void OnStop_SetWeapon()
	{
		//Debug.Log ("CSVR_PlayerRespawner.OnStop_SetWeapon from " + transform.name);
		if (!ReadyToPlay) {
			if (FirstSpawn) {
				FirstSpawn = false;
				OnFirstSpawnReady();
			} else {
				OnPlayerReady();
			}
		}
	}

	//Hàm nOnOOnFirstSpawn()nFirstSpawn()FirstSpawn()ay duoc goi luc dau tran, va chi duoc goi 1 lan
	public void OnFirstSpawn() {
		if(efx_Respaw != null)
			efx_Respaw.enabled = false;
	}
	//Hàm này tương đương OnPlayerReady nhưng chỉ đc gọi một lần lúc đầu trận
	public void OnFirstSpawnReady() {
		vp_MPRemotePlayer remotePlayer = GetComponent<vp_MPRemotePlayer>();
		if (remotePlayer == null) {
			CSVR_UIPlay.instance.UI_OnEnable();
		}
		if (GetComponent<CSVR_FPInputMobile>() != null)
		{
			GetComponent<vp_FPPlayerDamageHandler>().checkDead = false;
			if (!RespawnNoActionTimeFinished) {
				GetComponent<CSVR_FPInputMobile>().loadBarObject.SetActive(true);
			}
			GetComponent<CSVR_FPGetGun>().tbnNhatSung.gameObject.SetActive(false);
			GetComponent<CSVR_FPGetGun>().StarIndex = 1;
		}
		ReadyToPlay = true;
		switch (CSVR_GameSetting.modeName)
		{
		case "CSVR_Deathmatch":
		case "CSVR_AITeamDeathmatch":
		case "CSVR_TeamDeathmatch":
		case "CSVR_BaseCapture":
		case "CSVR_ZombieSurvival":
			StartCoroutine(DisableRespawnEffect(RespawnNoActionTime));
			break;
		case "CSVR_DeadOrAlive":
		case "CSVR_SearchAndDestroy":
			if (PhotonNetwork.playerList.Length > 1 && vp_MPMaster.Phase == vp_MPMaster.GamePhase.Playing && vp_MPClock.TimeLeft - vp_MPMaster.Instance.GameLength < -30) 
			{
				StartCoroutine(DisableRespawnEffect(0));
				//vào sau 30 giây thì chuyển sang chế độ view sử lí ở máy master
				if (PhotonNetwork.isMasterClient) {
					transform.GetComponent<vp_DamageHandler>().CurrentHealth = -1;
					transform.GetComponent<vp_DamageHandler>().AntiCheat_CurrentHealth = -1;
					SendMessage("Die");
					vp_MPMaster.Instance.TransmitDeathInfo("", "", "",
					                                       "", "", 0, 0, "", "", 0, 0, -1, m_NetWork.ID);
				}else{
					if (remotePlayer == null) {
						CSVR_UIPlay.instance.UI_OnDisable ();
						GameObject instance = Instantiate (Resources.Load ("UI/CSVR_KillCam") as GameObject);
						instance.transform.localScale = Vector3.one;
						instance.GetComponent<CSVR_UIKillCam> ().Provide = transform;
						instance.transform.position = new Vector3(transform.position.x,transform.position.y+3,transform.position.z);
						int team = int.Parse (PhotonNetwork.player.customProperties ["Team"].ToString ());
						if (team == 0) {
							instance.GetComponent<CSVR_UIKillCam> ().TagTargets = "PolicePlayer";
						} else {
							instance.GetComponent<CSVR_UIKillCam> ().TagTargets = "TerrorisPlayer";
						}
						instance.gameObject.SetActive (true);
					}
				}
			}else{
				StartCoroutine(DisableRespawnEffect(RespawnNoActionTime));
			}
			break;
		}
	}

	//Hàm này để xử lý sự kiện nhân vật bắt đầu hồi sinh. Có thể cho nhân vật ko hiện ra, bắn không chết
	public void OnPlayerRespawn() {
		//Debug.Log ("OnPlayerRespawn from " + transform.name);
		if(efx_Respaw != null)
			efx_Respaw.enabled = false;
		//Đoạn dưới đây chỉ là ví dụ
		//vp_MPRemotePlayer remotePlayer = GetComponent<vp_MPRemotePlayer>();
		//if (remotePlayer == null) {
		//    Debug.Log("Đây là remote");
		//} else {
		//    Debug.Log("Đây là local");
		//}
	}

	//Hàm này để xử lý sự kiện khi nhân vật sẵn sàng bắn giết
	public void OnPlayerReady() {
		if(efx_Respaw != null)
			efx_Respaw.enabled = true;
		//Bật respawn effect
		if (!RespawnNoActionTimeFinished) {
			respawnEffect.SetActive (true);
		}
		if (GetComponent<CSVR_FPInputMobile>() != null)
		{
			GetComponent<vp_FPPlayerDamageHandler>().checkDead = false;
			if (!RespawnNoActionTimeFinished) {
				GetComponent<CSVR_FPInputMobile>().loadBarObject.SetActive(true);
			}
			GetComponent<CSVR_FPGetGun>().tbnNhatSung.gameObject.SetActive(false);
			GetComponent<CSVR_FPGetGun>().StarIndex = 1;
		}


		//Bat dau thoi gian ko bi ban, ko ban duoc
		StartCoroutine(DisableRespawnEffect(RespawnNoActionTime));


		ReadyToPlay = true;

		//Đoạn dưới đây chỉ là ví dụ
		//vp_MPRemotePlayer remotePlayer = GetComponent<vp_MPRemotePlayer>();
		//if (remotePlayer == null) {
		//    Debug.Log("Đây là remote");
		//} else {
		//    Debug.Log("Đây là local");
		//}

		//Dùng Debug.Break để tét xem hàm này được gọi đúng thời điểm chưa
		//Debug.Break ();
	}

	protected override void Die()
	{
		switch (CSVR_GameSetting.modeName)
		{
		case "CSVR_Deathmatch":
		case "CSVR_AITeamDeathmatch":
		case "CSVR_TeamDeathmatch":
		case "CSVR_BaseCapture":
		case "CSVR_ZombieSurvival":
			vp_Timer.In(3f, PickSpawnPoint, m_RespawnTimer);
			break;
		case "CSVR_SearchAndDestroy":
			if(vp_MPMaster.Phase == vp_MPMaster.GamePhase.Playing)
				serverConnectorInstance.DropC4 ();
			break;
		}
	}
}
