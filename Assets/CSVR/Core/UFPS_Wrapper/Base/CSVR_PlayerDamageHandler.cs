﻿using UnityEngine;
using System.Collections;
using CodeStage.AntiCheat.ObscuredTypes; 

public class CSVR_PlayerDamageHandler : vp_PlayerDamageHandler
{

    private float HeathTemp = 0f;
    public GameObject HeathEffect;
    CSVR_RagdollHandler mRagdollHandler;
    public float m_FormattedHealth;
    public CSVR_RagdollHandler RagdollHandler
    {
        get
        {

            if (mRagdollHandler == null)
            {
                mRagdollHandler = GetComponentInChildren<CSVR_RagdollHandler>();
            }
            return mRagdollHandler;
        }

    }
    string FormattedHealth
    {
        get
        {
            m_FormattedHealth = (Player.Health.Get());
            if (m_FormattedHealth < 1.0f)
                m_FormattedHealth = (Player.Dead.Active ? Mathf.Min(m_FormattedHealth, 0.0f) : 1.0f);
            if (Player.Dead.Active && m_FormattedHealth > 0.0f)
                m_FormattedHealth = 0.0f;
            return ((int)m_FormattedHealth).ToString();
        }

    }

	CSVR_ServerConnector m_serverConnectorInstance; // Dung de lay thong tin ve nguoi ban
	CSVR_ServerConnector serverConnectorInstance{
		get {
			if (m_serverConnectorInstance == null) {
				m_serverConnectorInstance = transform.root.GetComponent<CSVR_ServerConnector>();
			}
			return m_serverConnectorInstance;
		}
	}
    public Animator animController;
    CSVR_PlayerRespawner respawn;
	float tempHeath = 100;
    /// <summary>
    /// registers this component with the event handler (if any)
    /// </summary>
    protected override void OnEnable()
    {
        base.OnEnable();
        animController = GetComponentInChildren<Animator>();
        respawn = GetComponent<CSVR_PlayerRespawner>();

		//Chong cheat
		AntiCheat_CurrentHealth.RandomizeCryptoKey ();
    }

	protected override void Reset()
	{
		base.Reset();
		tempHeath = 100;
//		Debug.Log  ("==================Reset================");
	}

	vp_MPRemotePlayer _remotePlayer;

	vp_MPRemotePlayer RemotePlayer {
		get {
			if (_remotePlayer == null) {
				_remotePlayer = GetComponent<vp_MPRemotePlayer>();
			}
			return _remotePlayer;
		}
	}


	public override void Damage(vp_DamageInfo damageInfo)
	{
//		Debug.Log ("Damage");
		//Chong cheat
		CurrentHealth = AntiCheat_CurrentHealth;

		//Debug.Log (damageInfo.team );
		//Debug.Log (serverConnectorInstance.idTeams );
		if (damageInfo.Type != vp_DamageInfo.DamageType.Fall && damageInfo.Type != vp_DamageInfo.DamageType.Explosion) {
			if (damageInfo.team != 0 && damageInfo.team == serverConnectorInstance.idTeams) return;
		}

		if (!respawn.RespawnNoActionTimeFinished)
		{
			//Debug.Log ("Damage1 ");
			return;
		}
        HeathTemp = CurrentHealth;
		//Debug.Log (CurrentHealth);
        if (CurrentHealth <= 0)
            return;
		//Debug.Log ("Damage 2");
		base.Damage(damageInfo);
		
		//Debug.Log (RemotePlayer.photonView.owner.isMasterClient);
		if (damageInfo.Source.gameObject.layer == vp_Layer.LocalPlayer && RemotePlayer.photonView.owner.isMasterClient) {
			RemotePlayer.Transmit_HitMaster( 
			               damageInfo.Damage,
			               (int)damageInfo.Type,
			               damageInfo.ShooterPlayfabID,
			               damageInfo.ShooterName,
			               damageInfo.GunID,
			               damageInfo.HitPart,
			               damageInfo.Level,
			               damageInfo.team,
			               damageInfo.GunName,
                           damageInfo.idphoton,
			               damageInfo.vec
			               );   			              
		}
        if (damageInfo.vec==Vector3.zero)
        {
            GameObject.Instantiate(HeathEffect, new Vector3(transform.position.x,transform.position.y+1,transform.position.z), transform.rotation);
        }
        else
        {
            GameObject.Instantiate(HeathEffect, damageInfo.vec, transform.rotation);
        }
		animController.SetTrigger("IsTakeDamage");
		// Goi ham truyen tin ve cai chet
		if (CurrentHealth <= 0.0f)
		{
			vp_MPMaster.Instance.TransmitDeathInfo(damageInfo.ShooterPlayfabID, damageInfo.ShooterName, damageInfo.GunID,
                                                   serverConnectorInstance.PlayfabID, serverConnectorInstance.PlayfabName, damageInfo.HitPart, HeathTemp, damageInfo.Level, damageInfo.GunName, damageInfo.team, serverConnectorInstance.idTeams,damageInfo.idphoton,RemotePlayer.ID);
//            Debug.Log("HeathTemp = " + HeathTemp);
           
		}
		
		if(tempHeath<=0 ||  CurrentHealth <= 0)
			return;
		
		tempHeath-= damageInfo.Damage;
		/* Code nay tat di vi vao trong thi lai return
        if (vp_Gameplay.isMaster)
        {
            CSVR_FPGetGun.getgun.ShowHeath((int)damageInfo.Damage, damageInfo.ShooterPlayfabID);
        }
        */
		
	}

	//Thong bao den master la master o may nay vua bi ban. Ham nay nham giam lag 
	void Transmit_HitMaster() {

	}

}
