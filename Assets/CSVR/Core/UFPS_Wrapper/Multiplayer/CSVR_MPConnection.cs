﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Horus;
using Horus.ClientModels;
//using ExitGames.Client.Photon.LoadBalancing;

public class CSVR_MPConnection : vp_MPConnection
{
	//LoadbalancingPeer lbClient;

	public static CSVR_MPConnection instance;

	int systemMemorySize;
	
	List<PhotonPlayer> playersInRoom;
	
	TeamTypes myTeam;
	
	bool isDisconect = false;
	bool isJoinRoom = false;
	
	const int policeTeamCount = 0, terroristTeamCount = 0;
	public GameObject gameModeInstance;
	bool tuTaoPhong = false;

	protected override void Start()
	{
		base.Start();
		
		systemMemorySize = SystemInfo.systemMemorySize;
		
		instance = this;

#if VIRTUAL_ROOM
		StayConnected = true;
#endif
		gameModeInstance = HorusManager.instance.gameModeInstance;
	}

	float ConnectionCheckInternal = 5f;
	float ConnectionCheckTimer = 5f;
	
	protected override void UpdateConnectionState()
	{
#if !VIRTUAL_ROOM
		if (!StayConnected)
			return;
		
		if (PhotonNetwork.connectionStateDetailed != m_LastPeerState)
		{
//			Debug.Log ("UpdateConnectionState");
			if (PhotonNetwork.connectionStateDetailed == PeerState.DisconnectingFromGameserver)
			{
				Disconnect();
				Connect();
				m_LastPeerState = PeerState.Uninitialized;
			}
			//            string s = PhotonNetwork.connectionStateDetailed.ToString();
			//            s = ((PhotonNetwork.connectionStateDetailed == PeerState.Joined) ? "--- " + s + " ---" : s);
		}
		m_LastPeerState = PhotonNetwork.connectionStateDetailed;
#else
		//Debug.Log(PhotonNetwork.connectionStateDetailed);
	//	if(Application.loadedLevelName != "Login"){
		if (IsDisconnectedFromPhoton) {
			if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated) {
				if (ConnectionCheckTimer < 0) {
					IsDisconnectedFromPhoton = false;
					Disconnect ();
					Connect ();
					m_LastPeerState = PeerState.Uninitialized;

					ConnectionCheckTimer = ConnectionCheckInternal;

				} else {
					ConnectionCheckTimer = ConnectionCheckTimer - Time.deltaTime;
				}
			} else {
				m_LastPeerState = PhotonNetwork.connectionStateDetailed;
			}
		}
#endif
	}
	public void SelectedPhotonServer(string ip){

		PhotonNetwork.PhotonServerSettings.UseMyServer (ip, 5055, "master");

	}
	public void SelectedPhotonServer(){

		#if CSVR_PHOTON_CLOUD_EU
		PhotonNetwork.PhotonServerSettings.UseCloud ("03b1d5d9-ab9d-431b-ad49-aace5ee39f27",CloudRegionCode.eu);
		#elif CSVR_PHOTON_CLOUD_US
		PhotonNetwork.PhotonServerSettings.UseCloud ("03b1d5d9-ab9d-431b-ad49-aace5ee39f27",CloudRegionCode.us);
		#elif CSVR_PHOTON_CLOUD_ASIA
		PhotonNetwork.PhotonServerSettings.UseCloud ("03b1d5d9-ab9d-431b-ad49-aace5ee39f27",CloudRegionCode.asia);
		#elif CSVR_PHOTON_CLOUD_JP
		PhotonNetwork.PhotonServerSettings.UseCloud ("03b1d5d9-ab9d-431b-ad49-aace5ee39f27",CloudRegionCode.jp);
		#elif CSVR_PHOTON_CLOUD_AU
		PhotonNetwork.PhotonServerSettings.UseCloud ("03b1d5d9-ab9d-431b-ad49-aace5ee39f27",CloudRegionCode.au);		
		#elif CSVR_PHOTON_S1
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S2
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S3
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S4
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S5
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S6
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S7
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S8
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "master");
		#elif CSVR_PHOTON_S9
		PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.38", 5055, "master");
		#elif CSVR_PHOTON_S10
		PhotonNetwork.PhotonServerSettings.UseMyServer ("103.53.168.68", 5055, "master");
		#endif
	}

	public override void Connect()
	{
		if (!PhotonNetwork.insideLobby) {
			//Debug.Log ("-1. Kết nối máy chủ, đang kết nối lại...");
			StayConnected = true;
			PhotonNetwork.AuthValues = new AuthenticationValues (HorusManager.instance.playerID);
			PhotonNetwork.ConnectUsingSettings ("1.0");
		} else {
//			StayConnected = false;
			isDisconect = true;
			//Debug.Log ("0. Đổi kết nối máy chủ, đang kết nối lại...");
			//Disconnect ();
			PhotonNetwork.Disconnect();
			Connect ();
			//m_LastPeerState = PeerState.Uninitialized;
		}
	}

	void OnConnectedToPhoton(){
		//Debug.Log ("0. OnConnectedToPhoton");
	}

	//Day la nhan message theo kieu chat moi.
	//Can chuyen sang CSVR_UIChatManager
	public void OnMessage(string sender, string message, string channel)
    {
		
	}

	int JoinLobbyTimesLeft = 2; //Phai la so chan. Day la so lan join lobby. = so lan host room / 2
	#if VIRTUAL_ROOM
	void OnLeftLobby() {
		CSVR_RoomHostServiceStatus.CurrentLobbyName = "";
		CSVR_RoomHostServiceStatus.CurrentLobbyType = "";
	}

	#endif

	void OnJoinedLobby()
	{
		//Yo.Log ("Ahihi: ", "vô lobby mới");
#if !VIRTUAL_ROOM
		if (isDisconect && isJoinRoom) {
			isDisconect = false; 
			isJoinRoom = false;
			JoinRoom ();
		}
		if (CSVR_GameSetting.friendsListName.Count > 0) {
			PhotonNetwork.FindFriends (CSVR_GameSetting.friendsListName.ToArray());        		
		} 

		if (CSVR_GameSetting.UseRoomVIP) { 

			//string.Format ("isQuickJoinedRoom: {0} TuTaoPhong: {1}", CSVR_UIMultiplayerLobby.instance.isQuickJoinedRoom, CSVR_GameSetting.TuTaoPhong);
			if (CSVR_GameSetting.IsQuickJoinedRoom) {
				if (CSVR_RoomHostService.RoomNameToJoin != "" && CSVR_RoomHostService.WaitingCheckCount > 0) {
					TryJoinRoomVIP();

				}
			}	
			else {
				if (CSVR_RoomHostService.RoomNameToJoin != "" ) {
							CreateNewRoom((byte)CSVR_RoomHostService.NumberOfPlayers,
								CSVR_RoomHostService.Mode,
								CSVR_RoomHostService.Map,
								CSVR_RoomHostService.Level,
								CSVR_RoomHostService.Host,
								CSVR_RoomHostService.RoomNameToJoin);
								
				}	
			}
		}
#else
		CSVR_RoomHostServiceStatus.CurrentLobbyName = PhotonNetwork.lobby.Name;
		CSVR_RoomHostServiceStatus.CurrentLobbyType = PhotonNetwork.lobby.Type.ToString();

		// Dem xem so lan con lai chay room vip truoc khi restart app 
        if (JoinLobbyTimesLeft == 0)
        {
            Debug.Log("Chay lai app");
            CSVRManager.instance.AppRestart();
            return;
        }
        else
        {
            JoinLobbyTimesLeft--;
        }

		if (CSVR_RoomHostService.Map.Trim() == "") {
			CreateRoomHostRoom();

//			ExitGames.Client.Photon.Hashtable rp = new ExitGames.Client.Photon.Hashtable();
//			rp.Add("isRoomHostRoom", "true");			
//
//			RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = 2 , customRoomProperties = rp};
//			PhotonNetwork.CreateRoom(null, roomOptions, TypedLobby.Default); 
//			//Kiem tra xem ve scene Home chua	
//			if (SceneManager.GetActiveScene().name != "Home") {
//				PhotonNetwork.LoadLevel("Home");
//			} 
		}else {
			
		if (CSVR_GameSetting.IsQuickJoinedRoom) {	
				CSVR_GameSetting.maxPlayer = CSVR_RoomHostService.NumberOfPlayers;
				CSVR_GameSetting.modeName = CSVR_RoomHostService.Mode;
				CSVR_GameSetting.mapName = CSVR_RoomHostService.Map;
				CSVR_GameSetting.hostName = CSVR_RoomHostService.Host;

				gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive (true);

				CreateNewRoom((byte)CSVR_RoomHostService.NumberOfPlayers,
				CSVR_RoomHostService.Mode,
				CSVR_RoomHostService.Map,
				CSVR_RoomHostService.Level,
				CSVR_RoomHostService.Host,
				CSVR_RoomHostService.RoomNameToJoin);

				CSVR_RoomHostService.Clear(); //Để quay lại lobby thì tạo lai doi de tao roomVIP
						
			} else {
				TryJoinRoomVIP();
				
			}
		}
#endif

	}	
	
	void CreateRoomHostRoom() {

		ExitGames.Client.Photon.Hashtable customParams = new ExitGames.Client.Photon.Hashtable();
		customParams.Add(GameConstants.Vip, "true");	

		RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = 2 , customRoomProperties = customParams};
		roomOptions.customRoomPropertiesForLobby = new string[] {GameConstants.Vip };

		TypedLobby sqlLobby = new TypedLobby("", LobbyType.SqlLobby);
		PhotonNetwork.CreateRoom(null, roomOptions, sqlLobby);

		//Kiem tra xem ve scene Home chua	
		if (SceneManager.GetActiveScene().name != "Home") {
			PhotonNetwork.LoadLevel("Home");
		} 
	}	


	public void CreateNewRoom(byte numberOfPlayers, string mode, string map, int level,string host, string roomName="")
	{
		SetupAIInCreate ();
		//Debug.Log ("CreateNewRoom..."+numberOfPlayers +"mode "+);
		ExitGames.Client.Photon.Hashtable customParams = new ExitGames.Client.Photon.Hashtable();
		customParams.Add(GameConstants.Mode, mode);
		customParams.Add (GameConstants.Map, map);
		customParams.Add(GameConstants.SkillLevel, level);
		customParams.Add(GameConstants.Host, host);
		//.random ten phong
		customParams.Add(GameConstants.Name, CSVR_GameSetting.roomName[Random.Range(0,8)]);
		Debug.Log (host);
		Debug.Log (CSVR_GameSetting.roomName[Random.Range(0,8)]);

		RoomOptions options = new RoomOptions();
		options.isVisible = (CSVR_GameSetting.selectPlayModeValue == 0) ? true : false;
		options.maxPlayers = numberOfPlayers;
		options.customRoomProperties = customParams;
		options.customRoomPropertiesForLobby = new string[] { GameConstants.Mode, GameConstants.Map, GameConstants.SkillLevel,GameConstants.Host,GameConstants.Name };
		//Yo.JsonLog ("json", customParams);
		TypedLobby sqlLobby = new TypedLobby("", LobbyType.SqlLobby);
		PhotonNetwork.CreateRoom(roomName, options, sqlLobby);
		//lbClient.OpCreateRoom(roomName, options, sqlLobby);
	}



	public void  JoinRoom(string roomName = "")
	{
#if !VIRTUAL_ROOM
		isJoinRoom = true;
		//Neu khong ghi ten room cu the
		if (roomName.Trim () == "") {
			
			string mode = CSVR_GameSetting.modeName;
			string map = CSVR_GameSetting.mapName;
			int level = CSVR_GameSetting.accountLevel;
			
			if (PhotonNetwork.connected && PhotonNetwork.insideLobby) {
				//Room Properties Matchmaking
				ExitGames.Client.Photon.Hashtable expectedProperties = new ExitGames.Client.Photon.Hashtable();
				expectedProperties.Add( GameConstants.Mode, mode );
				if(map != "Map_Random")
					expectedProperties.Add( GameConstants.Map, map );

				//room vip
				PhotonNetwork.JoinRandomRoom( expectedProperties, 11 );

                //Tutorial
                /*TutorialManager.instance.Finish("TEAMFIGHT");
                CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = false;
                */
            } else {
				isDisconect = true;
				Debug.Log ("Mất kết nối máy chủ, đang kết nối lại...");
				Disconnect ();
				Connect ();
				m_LastPeerState = PeerState.Uninitialized;
				
			}
		} else {
			PhotonNetwork.JoinRoom(roomName);
		}
#else

#endif
		
	}
	
	public void OnQuickJoinedRoom(bool status)
	{
		CSVR_GameSetting.IsQuickJoinedRoom = status;
	}
	
	private void OnPhotonRandomJoinFailed()
	{

	#if !VIRTUAL_ROOM

		if (CSVR_GameSetting.UseRoomVIP) {
			if (CSVR_RoomHostService.TryingRoomVIP) {
				Debug.Log("Server Room VIP full");
				//nếu Server Room VIP full thì tạo phòng	
				//CreateNewRoom((byte)CSVR_GameSetting.maxPlayer,CSVR_GameSetting.modeGame,CSVR_GameSetting.roomName,CSVR_GameSetting.accountLevel,CSVR_GameSetting.hostName);
				
				CSVR_RoomHostService.TryingRoomVIP = false;

			} else {
				CSVR_RoomHostService.TryingRoomVIP = true;
				
				//PhotonNetwork.JoinRandomRoom(); //Tim room o default lobby, chinh la lobby chua roomVIP

				//Room Properties Matchmaking
				ExitGames.Client.Photon.Hashtable expectedProperties = new ExitGames.Client.Photon.Hashtable();
				expectedProperties.Add( GameConstants.Vip, "true" );
				PhotonNetwork.JoinRandomRoom( expectedProperties,2);
			}
		} else {
			//this.CreateNewRoom (10, CSVR_GameSetting.modeName, CSVR_GameSetting.mapName, CSVR_GameSetting.accountLevel,CSVR_GameSetting.hostName);
			this.CreateNewRoom (11, CSVR_GameSetting.modeName, CSVR_GameSetting.mapName, CSVR_GameSetting.accountLevel,CSVR_GameSetting.hostName);
		}
	#endif

		
		
	}
	
	private void  OnPhotonJoinRoomFailed() {

#if !VIRTUAL_ROOM
		if (CSVR_GameSetting.UseRoomVIP) 
		{
			if (CSVR_RoomHostService.RoomNameToJoin.Trim() == "") {
				Debug.LogError("RoomVIP full. Khong the tao room nua");
		} 
		else 
		{
			CSVR_RoomHostService.WaitingCheckCount --; 
			if (CSVR_RoomHostService.WaitingCheckCount > 0) 
			{
				Invoke("TryJoinRoomVIP", CSVR_RoomHostService.WaitingTimeInterval);
			} 
			else 
			{
				Debug.LogError("Doi mai khong thay room VIP dau. Can co thong bao");
			}
		}

		}
#else
		CSVR_RoomHostService.WaitingCheckCount --; 
		if (CSVR_RoomHostService.WaitingCheckCount > 0) 
		{
			Invoke("TryJoinRoomVIP", CSVR_RoomHostService.WaitingTimeInterval);
		} 
		else 
		{
			Debug.LogError("Doi mai khong thay room VIP dau. Can co thong bao");
			CreateRoomHostRoom();
		}
#endif
	}

	private void TryJoinRoomVIP() {
		PhotonNetwork.JoinRoom(CSVR_RoomHostService.RoomNameToJoin);
	}

#if VIRTUAL_ROOM
void OnLeftRoom() {
	CSVR_RoomHostServiceStatus.CurrentRoom = "";
	CSVR_RoomHostServiceStatus.C9 = "false";
}
#endif
	private void OnJoinedRoom()
	{
//	Debug.Log ("OnJoinedRoom " + PhotonNetwork.room.name);
		//RoooooomVIP
		CSVR_RoomHostService.TryingRoomVIP = false;
		CSVR_RoomHostService.Clear(); //De sau khi thoat tran ko vao lai nua
#if VIRTUAL_ROOM
	CSVR_RoomHostServiceStatus.CurrentRoom = PhotonNetwork.room.name;
	CSVR_RoomHostServiceStatus.C9 = PhotonNetwork.room.customProperties.ContainsKey(GameConstants.Vip).ToString();
#endif

		if (PhotonNetwork.room.customProperties.ContainsKey (GameConstants.Vip)) {
#if VIRTUAL_ROOM
			vp_MPPlayerSpawner.FreshNewRoom = true;


#endif
			PhotonNetwork.Instantiate ("CSVR_RoomHostService", Vector3.zero, Quaternion.identity, 0);
			
			return;
		} else {
#if VIRTUAL_ROOM
			if (PhotonNetwork.playerList.Length <= 1) {
				Invoke("RoomVIPTimeoutClose", CSVR_GameSetting.RoomVIPCloseTimeout);
			}
#endif
		}

		
		RequestOnInfoChanged();
		ChoiseMode ();

		vp_Gameplay.isMaster = PhotonNetwork.isMasterClient;


		//Tắt FriendView nếu được bật
		if(CSVR_UIFriend.instance){
			CSVR_UIFriend.instance.UI_OnDisable();
		}
		//ắt InviteFriendPopup nếu được bật
		if(CSVR_UIInviteFriendPopup.instance){
			CSVR_UIInviteFriendPopup.instance.UI_OnDisable();
		}
		
		int policeTeamCountTemp = policeTeamCount;
		int terroristTeamCountTemp = terroristTeamCount;
		
		playersInRoom = new List<PhotonPlayer>(PhotonNetwork.playerList);
		
		byte[] bytes = AccountManager.instance.avatarPlayerTexture.EncodeToPNG();
		
		ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
		//properties.Add("HorusId", HorusManager.instance.playerID);
		properties.Add("UserName", HorusManager.instance.playerUserName);
		properties.Add("PlayFabName", AccountManager.instance.displayName);
		properties.Add("Level", CSVR_GameSetting.accountLevel);
		properties.Add("Avatar", bytes);
		properties.Add("Ready", "false");
		properties.Add("TuTaoPhong", CSVR_GameSetting.TuTaoPhong);
		properties.Add("Ram", systemMemorySize);
		properties.Add("Ping", PhotonNetwork.GetPing());
		properties.Add("FacebookID", HorusManager.instance.userFacebookInfo);
		properties.Add("BombCarrier", false);


		properties.Add("PlayerTypeName", CSVR_GameSetting.currentPlayerTypeName);

		#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && VIRTUAL_ROOM
		myTeam = TeamTypes.RoomHostPlayer; //Team cho room host
		properties.Add("Team", myTeam.GetHashCode());
		#else

		for (int i = 0; i < playersInRoom.Count; i++)
		{
			if (!playersInRoom[i].isLocal && playersInRoom[i].customProperties["Team"] != null){
				if (int.Parse(playersInRoom[i].customProperties["Team"].ToString()) == 0)
					policeTeamCountTemp += 1;
				else if (int.Parse(playersInRoom[i].customProperties["Team"].ToString()) == 1)
					terroristTeamCountTemp += 1;
			}
		}
		if (terroristTeamCountTemp < policeTeamCountTemp)
			myTeam = TeamTypes.Terrorist;
		else
			myTeam = TeamTypes.Police;

		properties.Add("Team", myTeam.GetHashCode());
		#endif
		PhotonNetwork.player.SetCustomProperties(properties);

		bool mapReady = false;

		for (int i = 0; i < playersInRoom.Count; i++)
		{
			if (playersInRoom[i].isMasterClient)
			{
				mapReady = System.Convert.ToBoolean(playersInRoom[i].customProperties["Ready"]);
				tuTaoPhong = System.Convert.ToBoolean (playersInRoom [i].customProperties ["TuTaoPhong"]);
				break;
			}
		}
		
		if (!PhotonNetwork.isMasterClient) {
			ExitGames.Client.Photon.Hashtable propertiesNew = new ExitGames.Client.Photon.Hashtable();
			propertiesNew.Add("TuTaoPhong",tuTaoPhong);
			PhotonNetwork.player.SetCustomProperties(propertiesNew);
		}

		if (CSVR_GameSetting.modeName == "CSVR_ZombieSurvival") {
			Invoke("OnNetworkTransmitRespawnAll", 5f);
			CSVR_UILoadingPVE.instance.Open ();

			return;
		}

		if (tuTaoPhong){
			// ở trạng thái chờ, phòng do người chơi tạo không phải chơi nhanh.
			this.OnPropertiesChanged();
			if(mapReady){
				OnNetworkLoading();
			}
		}
		else
		{
			OnNetworkLoading();
			if(!mapReady){
				photonView.RPC("OnNetworkLoadingMap", PhotonTargets.Others);
			}
		}

	}
	
	public void LeaveRoom()
	{
		OnQuickJoinedRoom(false);
		PhotonNetwork.LeaveRoom();
		gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(false);
	}
	
	public void ChangeTeam(TeamTypes newTeam, bool syncNetwork)
	{
		myTeam = newTeam;
		
		ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
		properties.Add("Team", myTeam.GetHashCode());
		PhotonNetwork.player.SetCustomProperties(properties);
		
		if (syncNetwork)
		{
			this.OnPropertiesChanged();
		}
	}	


	//RoomVIP


	private void UpdateRoomProfile(){
		//Debug.Log ("UpdateRoomProfile");
		Dictionary<string,Dictionary<string,string>> playerInRoom = new Dictionary<string, Dictionary<string, string>> ();

		PhotonPlayer[] players = PhotonNetwork.playerList;
		for (int i = 0; i < players.Length; i++) {
			Dictionary<string, string> d = new Dictionary<string, string>();
			try{
				d.Add("team", players[i].customProperties["Team"].ToString());
			}catch{
				d.Add("team", "-1");
			}
			playerInRoom.Add (players [i].customProperties ["PlayFabName"].ToString (), d);
		}

		RoomUpdateRequest request = new RoomUpdateRequest ();
		request.apiKey = HorusManager.instance.userAPIKey;
		request.serverId = "s1";
		request.lobbyId = "l1";
		request.roomId = request.serverId + "-" + request.lobbyId + "-" + PhotonNetwork.room.name;
		request.playerCount = PhotonNetwork.room.playerCount;
		request.players = playerInRoom;
		request.customParams = PhotonNetwork.room.customProperties;
	//	Yo.JsonLog ("RoomUpdateRequest: ",  request);
		HorusClientAPI.RoomUpdate (request,
			(result) => {
			//	Yo.JsonLog ("RoomUpdateResult: ", result);
			},
			(error) => {

			}
		);
	}
	public void OnPropertiesChanged()
	{
		photonView.RPC ("OnNetworkPropertiesChanged", PhotonTargets.All);
	}
	
	[PunRPC]
	private void OnNetworkPropertiesChanged()
	{
		Debug.Log ("OnNetworkPropertiesChanged");
		try{
		/*
#if VIRTUAL_ROOM
			//Neu dang o Home va la che do moi ban
			if (!CSVR_UIMultiplayerLobby.instance.isQuickJoinedRoom && PhotonNetwork.isMasterClient && SceneManager.GetActiveScene().name == "Home") {
				Debug.Log("Chuyen master cho player khac " + PhotonNetwork.otherPlayers[0].name);
				//Chon 1 player khac lam master
						PhotonNetwork.SetMasterClient(PhotonNetwork.otherPlayers[0]);
			}
#endif*/
			if (Application.loadedLevelName == "Home") {
				CSVR_UIMatchRoomScreen.instance.OnRoomPropertiesChanged(PhotonNetwork.room);
			}
		}catch{
			return;
		}
	}
	
	private void RequestOnInfoChanged()
	{
		photonView.RPC("Request_OnNetworkInfoChanged", PhotonTargets.MasterClient);
	}
	
	[PunRPC]
	private void Request_OnNetworkInfoChanged()
	{
		if (!PhotonNetwork.isMasterClient)
			return;

		string str_AIInGame = Horus.Json.JsonWrapper.SerializeObject (CSVR_GameSetting.AI_InGame, Horus.Internal.HorusUtil.ApiSerializerStrategy);

		photonView.RPC("Receive_OnRoomInfoChanged", PhotonTargets.Others,str_AIInGame, CSVR_GameSetting.selectPlayModeValue, CSVR_GameSetting.modeName, CSVR_GameSetting.mapName, CSVR_GameSetting.maxPlayer, CSVR_GameSetting.maxSkill, CSVR_GameSetting.timeMatch);
	}

	[PunRPC]
	private void Receive_OnRoomInfoChanged(string jsonAI, int valuePlayMode,string modeName,string mapName,int maxPlayer,int maxKill,int maxTime)
	{
		CSVR_GameSetting.AI_InGame = new List<CSVR_UIAIInfo>();
		CSVR_GameSetting.AI_InGame = Horus.Json.JsonWrapper.DeserializeObject<List<CSVR_UIAIInfo>>(jsonAI);
        if(Application.loadedLevelName == "Home" && CSVR_GameSetting.AI_InGame.Count > 0)
            CSVR_UILoadingMap.instance.HienThi();
        
		CSVR_GameSetting.modeName = modeName;
		CSVR_GameSetting.mapName = mapName;
		CSVR_GameSetting.maxPlayer = maxPlayer;
		CSVR_GameSetting.maxSkill = maxKill;
		CSVR_GameSetting.timeMatch = maxTime;
		ChoiseMode();

		if (Application.loadedLevelName == "Home" && tuTaoPhong) {
			CSVR_UIMatchRoomScreen.instance.OnRoomInfoChanged (PhotonNetwork.isMasterClient);
		}
	}

	private void ChoiseMode(){
		gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(true);
	}
	public void SetReady()
	{
#if !VIRTUAL_ROOM
		if (CSVR_GameSetting.UseRoomVIP) {
			//Yeu cau room host chuyen sang lam master va bat dau tran
			photonView.RPC("RoomhostToStartGame", PhotonTargets.All);
		} else {			
			TransmitRespawnAll ();
		}
#else
		if (PhotonNetwork.isMasterClient) {
			TransmitRespawnAll ();
		} else {
			StartGameAfterBecomingMaster = true;
			PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
#endif
	}

	bool StartGameAfterBecomingMaster = false;

	[PunRPC]
	private void RoomhostToStartGame()
	{
#if VIRTUAL_ROOM
		if (PhotonNetwork.isMasterClient) {
			SetReady();
		} else {
			StartGameAfterBecomingMaster = true;
			PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
#endif
	}
	
	public void TransmitRespawnAll()
	{
		photonView.RPC("OnNetworkLoading", PhotonTargets.All);
	}

	[PunRPC]
	private void OnNetworkLoadingMap()
	{
		try{
			if(Application.loadedLevelName == "Home")
				CSVR_UILoadingMap.instance.HienThi();
		}catch{
			return;
		}
	}
	
	[PunRPC]
	private void OnNetworkLoading()
	{

		Invoke("OnNetworkTransmitRespawnAll", 5f);
		CSVR_UILoadingMap.instance.HienThi();
	}

	private void OnNetworkTransmitRespawnAll()
	{
		ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
		properties.Add("Ready", true);
		properties.Add("Map", CSVR_GameSetting.selectMapGameValueTemp);
		PhotonNetwork.player.SetCustomProperties(properties);
		

		// TODO: use PhotonNetwork.LoadLevel to load level while automatically pausing network queue
		// ("call this in OnJoinedRoom to make sure no cached RPCs are fired in the wrong scene")
		// also, get level from room properties / master
		
		// send spawn request to master client
		string name = "Unnamed";
		
		// sent as RPC instead of in 'OnPhotonPlayerConnected' because the
		// MasterClient does not run the latter for itself + we don't want
		// to do the request on all clients
		
		//	Debug.Log ("CSVR_GameSetting.selectModeGameValue" + CSVR_GameSetting.modeGame);
		PhotonView pv = GameObject.Find(CSVR_GameSetting.modeName).GetComponent<PhotonView>();
		//if (FindObjectOfType<vp_MPMaster>())	// in rare cases there might not be a vp_MPMaster, for example: a chat lobby
		pv.RPC("RequestInitialSpawnInfo", PhotonTargets.MasterClient, PhotonNetwork.player, 0, name);

		
		
	}
	
	[PunRPC]
	public void GetPlayerPing(){
		PhotonNetwork.player.SetCustomProperties( new ExitGames.Client.Photon.Hashtable(){ { "Ping", PhotonNetwork.GetPing()} } );
	}
	
	//Đây là hàm chọn máy làm master kế tiếp. Hiện tại là tìm máy nào khác máy này. Cần tùy chỉnh hàm này theo RAM và Ping
    //public PhotonPlayer GetNewMasterCandidate() {
    //    //ram máy trên 2G
    //    int ram = 1800;
    //Begin:
    //        List<PhotonPlayer> playersInRoom = new List<PhotonPlayer>(PhotonNetwork.playerList);
    //    if (playersInRoom.Count > 1) {
    //        photonView.RPC("GetPlayerPing", PhotonTargets.All);
    //        PhotonPlayer playerMaster = playersInRoom[0];
    //        float min = 200;
    //        for (int i = 1; i < playersInRoom.Count; i++)
    //        {
    //            if (!playersInRoom[i].isMasterClient && (int)playersInRoom[i].customProperties["Ram"] > ram && (int)playersInRoom[i].customProperties["Ping"] < min)
    //            {
    //                playerMaster = playersInRoom[i]; 
    //                min = (int)playersInRoom[i].customProperties["Ping"];
    //            }
    //        }
    //        if(playerMaster ==  playersInRoom[0] && ram == 1800){
    //            //ram máy trên 1G
    //            ram = 800;
    //            goto Begin;
    //        }else if(playerMaster ==  playersInRoom[0] && ram == 800){
    //            //trường họp còn lại lấy ping cao nhất
    //            ram = 0;
    //            goto Begin;
    //        }else if(playerMaster ==  playersInRoom[0] && playerMaster.isMasterClient){
    //            playerMaster = playersInRoom[1];
    //        }
    //        return playerMaster;
			
    //    }
    //    return null;
    //}

    //public PhotonPlayer GetNewMasterCandidateLag() {
    //    //ram máy trên 2G
    //    int ram = 1800;
    //BeginGetNewMasterCandidateLag:
    //        List<PhotonPlayer> playersInRoom = new List<PhotonPlayer>(PhotonNetwork.playerList);
    //    if (playersInRoom.Count > 1) {
    //        photonView.RPC("GetPlayerPing", PhotonTargets.All);
    //        PhotonPlayer playerMaster = playersInRoom[0];
    //        float min = 200;
    //        for (int i = 1; i < playersInRoom.Count; i++)
    //        {
    //            if (!playersInRoom[i].isMasterClient && (int)playersInRoom[i].customProperties["Ram"] > ram && (int)playersInRoom[i].customProperties["Ping"] < min)
    //            {
    //                playerMaster = playersInRoom[i]; 
    //                min = (int)playersInRoom[i].customProperties["Ping"];
    //            }
    //        }
    //        if(playerMaster ==  playersInRoom[0] && ram == 1800){
    //            //ram máy trên 1G
    //            ram = 800;
    //            goto BeginGetNewMasterCandidateLag;

    //        }else if(playerMaster ==  playersInRoom[0] && ram == 800){
    //            return null;
    //        }
    //        return playerMaster;
    //    }
    //    return null;
    //}

	bool PlayersAlreadyJoined = false;
	void RoomVIPTimeoutClose() {
	
		if (PlayersAlreadyJoined) return;

		if (vp_MPMaster.Instance != null) {
			vp_MPMaster.Instance.CloseRoomVIP ();
		}
	}

	void OnPhotonPlayerDisconnected(PhotonPlayer netPlayer)
	{	
		//Debug.Log ("OnPhotonPlayerDisconnected");
		//RoomVIP
		//Neu dang o trong room chuan bi cho roomVIP
		if (PhotonNetwork.room.customProperties.ContainsKey(GameConstants.Vip)) {

		} else {
			#if VIRTUAL_ROOM
			if (PhotonNetwork.room.playerCount <= 1) { 
			//Debug.Log("Dong do player thoat rom");
			PhotonNetwork.room.visible  = false;
			vp_MPMaster.Instance.CloseRoomVIP();
			return;
			}
			#endif
			if (PhotonNetwork.isMasterClient)
				UpdateRoomProfile ();

			if (!System.Convert.ToBoolean(netPlayer.customProperties["Ready"]))
			{
				playersInRoom.Remove(netPlayer);
				if (tuTaoPhong) this.OnPropertiesChanged();
				vp_Gameplay.isMaster = PhotonNetwork.isMasterClient;
			}
			else
			{
				vp_Gameplay.isMaster = PhotonNetwork.isMasterClient;
			}			
		}

	}
	void OnPhotonPlayerConnected() {
		if (PhotonNetwork.room.customProperties.ContainsKey (GameConstants.Vip)) {

		} else {
			if (PhotonNetwork.isMasterClient)
				UpdateRoomProfile ();
			
			if (PhotonNetwork.room.playerCount > 1) {
				PlayersAlreadyJoined = true;
			}
		}
	}
	void OnMasterClientSwitched() {
		
		if (PhotonNetwork.isMasterClient) { 
			if (!System.Convert.ToBoolean(PhotonNetwork.player.customProperties["Ready"]))
			{
				if (tuTaoPhong) this.OnPropertiesChanged();
			}
#if VIRTUAL_ROOM
			if (StartGameAfterBecomingMaster) {
				SetReady();
				StartGameAfterBecomingMaster = false;
			} else {
				//Neu dang o Home va la che do moi ban
			if (!CSVR_GameSetting.IsQuickJoinedRoom && SceneManager.GetActiveScene().name == "Home" && PhotonNetwork.otherPlayers.Length > 0) {
					//Debug.Log("Chuyen master cho player khac " + PhotonNetwork.otherPlayers[0].name);
					//Chon 1 player khac lam master
					PhotonNetwork.SetMasterClient(PhotonNetwork.otherPlayers[0]);
					OnPropertiesChanged();
				}
			}
#endif
		
			

		} else {
		}
	}

	bool IsDisconnectedFromPhoton = false;
	void OnDisconnectedFromPhoton () {
//		Debug.Log ("OnDisconnectedFromPhoton");
		IsDisconnectedFromPhoton = true;
	}

	void OnConnectionFail() {
//		Debug.Log ("OnConnectionFail");
	}


	private void SetupAIInCreate()
	{
		vp_MPMaster master = gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).GetComponent<vp_MPMaster> ();
		if (master.AITeamList.Length < 1)
			return;
		CSVR_GameSetting.AI_Police = master.AITeamList [0].TeamCount;
		CSVR_GameSetting.AI_Terrorist = master.AITeamList [1].TeamCount;
		CSVR_GameSetting.AI_Enable = master.AIEnabled;
		int AI_InGameCount = CSVR_GameSetting.AI_Police + CSVR_GameSetting.AI_Terrorist;
		CSVR_GameSetting.AI_InGame.Clear();
		if (CSVR_GameSetting.AI_Enable) {
			for (int i = 0; i < AI_InGameCount; i++) {
				CSVR_UIAIInfo ai = new CSVR_UIAIInfo ();
				ai.AI_TeamNumber = (i < CSVR_GameSetting.AI_Police) ? 1 : 2;
				ai.AI_Name = NamePrefix [(int)Random.Range (0, NamePrefix.Length)] + " " + NameMiddle [(int)Random.Range (0, NameMiddle.Length)] + " " + NameSuffix [(int)Random.Range (0, NameSuffix.Length)];
				ai.AI_ID = "BKMAI_"+i;
				ai.AI_Avatar = 0;
				ai.AI_Level = Random.Range (0, 10);
				CSVR_GameSetting.AI_InGame.Add (ai);
			}
		}
	}



	private string[] NamePrefix = new string[]{
//		"(✖╭╮✖)",
//		"⊙︿⊙",
//		"⊙﹏⊙",
//		"(>_<)",
//		"(►.◄)",
//		"≧◔◡◔≦",
//		"✿◕ ‿ ◕✿",
//		"o(╥﹏╥)o",
//		"(`･ω･´)",
//		"凸(¬‿¬)凸",
//		"¯(©¿©) /¯",
//		"◖♪_♪|◗",
//		"Ƹ̴Ӂ̴Ʒ",
//		"╚(•⌂•)╝",
//		"εїз",
//		"[̲̅$̲̅(̲̅2οο̲̅)̲̅$̲̅]",
//		"[̲̅$̲̅(̲̅ιοο̲̅)̲̅$̲̅]",
//		"┣▇▇▇═─",
//		"☻",
//		"❤",
//		"™ˆ",
		"",
	};

	private string[] NameMiddle = new string[]{
		"†imCầnCóE",
		"†ìñh♥Ảø",
		"Forevër",
		"ђa†♥ßµï♥иђỏ",
		"maŽΩÖm",
		"ĐåîÇøng†µ",
		"Sky♥Kü†€",
		"ÐáñhMấ†☼Luv",
		"Nhõx♥Çry",
		"Thần♣Tiên",
		"Hòang•†ử",
		"aиђ ¢ầи €м",
		"†hïếuGîa",
		"þéßj",
		"MrSiro",
		"Durex",
		"SjnDy ",
		"Tao",
		"ßấtÇần",
//		"__",
	};


	private string[] NameSuffix = new string[]{
//		"(✖╭╮✖)",
//		"⊙︿⊙",
//		"⊙﹏⊙",
//		"(>_<)",
//		"(►.◄)",
//		"≧◔◡◔≦",
//		"✿◕ ‿ ◕✿",
//		"o(╥﹏╥)o",
//		"(`･ω･´)",
//		"凸(¬‿¬)凸",
//		"¯(©¿©) /¯",
//		"◖♪_♪|◗",
//		"Ƹ̴Ӂ̴Ʒ",
//		"╚(•⌂•)╝",
//		"εїз",
//		"[̲̅$̲̅(̲̅2οο̲̅)̲̅$̲̅]",
//		"[̲̅$̲̅(̲̅ιοο̲̅)̲̅$̲̅]",
//		"┣▇▇▇═─",
//		"☻",
//		"❤",
//		"™ˆ",
//		"ˆ™",
		"",
	};


}
