﻿using UnityEngine;
using System.Collections;

public class CSVR_MapManager : MonoBehaviour {
	public string MapPrefab;
	public string PlayUI;
    public Sprite minimap;
	GameObject PlayUIInstance;
	GameObject MapInstance;
	// Use this for initialization
	void Awake () {
		//Tắt đồ họa
		#if !VIRTUAL_ROOM

		MapInstance = Instantiate (Resources.Load (MapPrefab), Vector3.zero, Quaternion.identity) as GameObject;
		PlayUIInstance = Instantiate (Resources.Load (PlayUI), Vector3.zero, Quaternion.identity) as GameObject;
		PlayUIInstance.GetComponentInChildren<MapBackground> ().CapturePosition = MapInstance.transform;
        PlayUIInstance.GetComponentInChildren<MapBackground> ().imgMiniMap=minimap;

		#endif
	}


	// Use this for initialization
	void Start () {
		vp_MPMaster.Instance.InstanceAI ();

	}
	
}
