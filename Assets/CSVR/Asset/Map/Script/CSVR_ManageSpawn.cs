﻿using UnityEngine;
using System.Collections;

public class CSVR_ManageSpawn : MonoBehaviour {
    public GameObject SpawnDeath, SpawnTeam;
	// Use this for initialization
	void Awake () {

        if (CSVR_GameSetting.IDMode == 101)
        {
            SpawnDeath.SetActive(true);
            SpawnTeam.SetActive(false);
        }
        else
        {
            SpawnTeam.SetActive(true);
            SpawnDeath.SetActive(false);
        }
	
	}
}
