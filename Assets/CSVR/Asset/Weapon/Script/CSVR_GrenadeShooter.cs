﻿using UnityEngine;
using System.Collections;

public class CSVR_GrenadeShooter : MonoBehaviour
{

    public AnimationClip anim;
	public AnimationClip animIdle;
	public bool isC4;
	//dung cho bomb thuong
    public static CSVR_GrenadeShooter instance = null;
	//dung cho c4
	public static CSVR_GrenadeShooter instanceC4 = null;

    protected virtual void OnEnable()
    {
		if(isC4)
			instanceC4 = this;
		else
			instance = this;
    }
    protected virtual void OnDisable()
    {
		if(isC4)
			instanceC4 = null;
		else
			instance = null;
    }
    public void PleassWaitFire()
    {
        if (anim != null)
        {
            if (WeaponAnimation[anim.name] == null)
                Debug.LogError("Error (" + this + ") No animation named '" + anim.name + "' is listed in this prefab. Make sure the prefab has an 'Animation' component which references all the clips you wish to play on the weapon.");
            else
            {
                WeaponAnimation.Play(anim.name);
            }
        }
    }
	//mode c4
	public void PleassWaitC4()
	{
		if (anim != null)
		{
			if (WeaponAnimation[anim.name] == null)
				Debug.LogError("Error (" + this + ") No animation named '" + anim.name + "' is listed in this prefab. Make sure the prefab has an 'Animation' component which references all the clips you wish to play on the weapon.");
			else
			{
				WeaponAnimation.Play(anim.name);
				WeaponAnimation[anim.name].speed = 0.7f;
			}
		}
	}
	public void StopAnimC4()
	{
		if (animIdle != null)
		{
			if (WeaponAnimation[animIdle.name] == null)
				Debug.LogError("Error (" + this + ") No animation named '" + anim.name + "' is listed in this prefab. Make sure the prefab has an 'Animation' component which references all the clips you wish to play on the weapon.");
			else
			{
				WeaponAnimation.Stop(anim.name);
				WeaponAnimation.Play(animIdle.name);
			}
		}
	}

    protected vp_FPWeapon m_FPWeapon = null;			// the weapon affected by the shooter
    public vp_FPWeapon FPWeapon
    {
        get
        {
            if (m_FPWeapon == null)
                m_FPWeapon = transform.GetComponent<vp_FPWeapon>();
            return m_FPWeapon;
        }
    }
    public Animation WeaponAnimation
    {
        get
        {
            if (m_WeaponAnimation == null)
            {
                if (FPWeapon == null)
                    return null;
                if (FPWeapon.WeaponModel == null)
                    return null;
                m_WeaponAnimation = FPWeapon.WeaponModel.GetComponent<Animation>();
            }
            return m_WeaponAnimation;
        }
    }
    Animation m_WeaponAnimation = null;
}
