﻿using UnityEngine;
using System.Collections;

public class CSVR_LoadMaterial : MonoBehaviour {
    public Material fpArms;    
    Transform hideWhenZooming;
    void OnEnable()
    {
        string weaponObjectName = GetComponent<vp_FPWeapon>().WeaponPrefab.name + "(Clone)";
        hideWhenZooming = transform.FindChild(weaponObjectName);
        if (hideWhenZooming!= null)
        {
            hideWhenZooming.FindChild("fpArms").GetComponent<SkinnedMeshRenderer>().material = fpArms;
        }
    }
}
