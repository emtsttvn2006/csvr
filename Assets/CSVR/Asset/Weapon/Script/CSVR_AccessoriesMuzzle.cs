﻿using UnityEngine;
using System.Collections;

public class CSVR_AccessoriesMuzzle : MonoBehaviour {
    void OnEnable()
    {
        if(CSVR_UIPlay.instance!=null)
        CSVR_UIPlay.instance.btnKnifeAttack.gameObject.SetActive (true);
    }
    void OnDisable()
    {
        if(CSVR_UIPlay.instance!=null)
        CSVR_UIPlay.instance.btnKnifeAttack.gameObject.SetActive (false);
    }
}
