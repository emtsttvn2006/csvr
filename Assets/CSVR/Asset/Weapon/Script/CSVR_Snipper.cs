﻿//This script is completely useless without "UFPS: Ultimate FPS". Get it at https://www.assetstore.unity3d.com/en/#!/content/2943.
//The developer of this script. And of the package containing these scripts, is in no way afiliated with VisionPunk. The developers of the UFPS system.

using System.Collections;
using UnityEngine;

public class CSVR_Snipper : MonoBehaviour
{
    public GameObject camView;

    public TypeWeapon WeaponType;

    //public GameObject objSung, objDan;
    public static CSVR_Snipper instance;
    protected vp_FPPlayerEventHandler Player = null;
    public int maxZoomIndex = 0;
    public int currentZoomIndex = 0;
    public float[] zoomFOV;

    //public Texture SnipeCross = null;

    [HideInInspector]
    public Transform hideWhenZooming;
    [HideInInspector]
    public bool HoldZoomAuto = false;
    [HideInInspector]
    public bool HoldZoom = false;
    vp_FPController m_Controler = null;
    vp_FPController Controler
    {
        get
        {
            if (m_Controler == null)
            {
                m_Controler = transform.root.GetComponent<vp_FPController>();
            }
            return m_Controler;
        }
    }
    protected vp_Weapon m_Weapon = null;
    protected virtual void OnEnable()
    {
		if (CSVR_UIPlay.instance == null)
			return;
        instance = this;

		camView = transform.root.FindChild ("FPSCamera/WeaponCamera").gameObject;

        Player = GameObject.FindObjectOfType(typeof(vp_FPPlayerEventHandler)) as vp_FPPlayerEventHandler;
        if (Player != null)

            Player.Register(this);
       // maxZoomIndex = zoomFOV.Length;
        currentZoomIndex = 0;
		CSVR_UIPlay.instance.changeCameraFOV(CSVR_GameSetting.normalFOV);
        string weaponObjectName = GetComponent<vp_FPWeapon>().WeaponPrefab.name + "(Clone)";
        hideWhenZooming = transform.FindChild(weaponObjectName);
        
    }
    protected virtual void OnDisable()
    {
        instance = null;
        if (Player != null)

            Player.Unregister(this);

        //maxZoomIndex = 0;
        currentZoomIndex = 0;
        CSVR_GameSetting.lookSensitivitySpeedZoom = 1f;
        if (CSVR_UIPlay.instance != null)
        {
            CSVR_UIPlay.instance.imgSniperCrosshair.SetActive(false);
            CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(false);
            CSVR_UIPlay.instance.btnReload.gameObject.SetActive(true);
            
        }
        if (camView!=null)
        {
            camView.SetActive(true);
        }
		if (CSVR_UIPlay.instance != null)
        {
			CSVR_UIPlay.instance.changeCameraFOV(CSVR_GameSetting.normalFOV);
        }
    }

    public virtual void OnStart_Zoom()
    {
        CSVR_GameSetting.DebugLog ("toc do zoom  "+CSVR_GameSetting.lookSensitivitySpeedZoom);
        CSVR_GameSetting.DebugLog ("toc do zoom  "+WeaponType);
		switch (WeaponType)
        {

            case TypeWeapon.AWP:
                CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(true);
                CSVR_UIPlay.instance.imgSniperCrosshair.SetActive(true);
                CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(true);
                //CSVR_UIPlay.instance.btnReload.gameObject.SetActive(false);
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(false);
                CSVR_GameSetting.DebugLog ("maxZoomIndex  "+maxZoomIndex);
                if (currentZoomIndex < maxZoomIndex)
                {
                CSVR_GameSetting.DebugLog ("toc do zoom  "+CSVR_GameSetting.lookSensitivitySpeedZoom);
                    float lookSpeedZoom = (zoomFOV[currentZoomIndex] / CSVR_GameSetting.normalFOV);
                    CSVR_GameSetting.lookSensitivitySpeedZoom = lookSpeedZoom;
                CSVR_GameSetting.DebugLog ("toc do zoom  22222"+CSVR_GameSetting.lookSensitivitySpeedZoom);
                    CSVR_UIPlay.instance.changeCameraFOV(zoomFOV[currentZoomIndex]);
                    currentZoomIndex++;

                }
                else
                {
                    currentZoomIndex = 1;
                    CSVR_UIPlay.instance.changeCameraFOV(zoomFOV[0]);
                }
                camView.SetActive(false);
                if (!Player.Crouch.Active)
                {
                    Controler.MotorAcceleration = 0.12f;
                }
                break;
		case TypeWeapon.SCAR20:
                HoldZoomAuto = true;
                CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(true);
                CSVR_UIPlay.instance.imgSniperCrosshair.SetActive(true);
                CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(true);
                //CSVR_UIPlay.instance.btnReload.gameObject.SetActive(false);
                //Chuyen sang che do zoom o day
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(false);
                if (currentZoomIndex < maxZoomIndex)
                {
                    float lookSpeedZoom = (zoomFOV[currentZoomIndex] / CSVR_GameSetting.normalFOV);
                    CSVR_GameSetting.lookSensitivitySpeedZoom = lookSpeedZoom;
                    CSVR_UIPlay.instance.changeCameraFOV(zoomFOV[currentZoomIndex]);
                    currentZoomIndex++;
                }
                else
                {
                    currentZoomIndex = 1;
                    float lookSpeedZoom = (zoomFOV[0] / CSVR_GameSetting.normalFOV);
                    CSVR_GameSetting.lookSensitivitySpeedZoom = lookSpeedZoom;
                    CSVR_UIPlay.instance.changeCameraFOV(zoomFOV[0]);
                }
                camView.SetActive(false);
                if (!Player.Crouch.Active)
                {
                    Controler.MotorAcceleration = 0.12f;
                }
                break;
		case TypeWeapon.AUG:
				
                CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(true);
                if (currentZoomIndex < maxZoomIndex)
                {
                    float lookSpeedZoom = (zoomFOV[currentZoomIndex] / CSVR_GameSetting.normalFOV);
                    CSVR_GameSetting.lookSensitivitySpeedZoom = lookSpeedZoom;
                    CSVR_UIPlay.instance.changeCameraFOV(zoomFOV[currentZoomIndex]);
                    currentZoomIndex++;
                }
                else
                {
                    currentZoomIndex = 1;
                    float lookSpeedZoom = (zoomFOV[0] / CSVR_GameSetting.normalFOV);
                    CSVR_GameSetting.lookSensitivitySpeedZoom = lookSpeedZoom;
                    CSVR_UIPlay.instance.changeCameraFOV(zoomFOV[0]);
                }
                if (!Player.Crouch.Active)
                {
                    Controler.MotorAcceleration = 0.12f;
                }
                //Debug.Log("Zoom 44          ");
                Player.Zoom.TryStart();
                break;

        }        //Chuyen sang che do zoom o day
        
    }

    public virtual void OnStop_Zoom()
    {
		switch (CSVR_FPWeaponShooter.instance._weaponSniper)
        {
            case TypeWeaponSniper.AWP:
                CSVR_UIPlay.instance.btnZoom.gameObject.SetActive(false);
				
                break;
            case TypeWeaponSniper.AUG:
				CSVR_UIPlay.instance.crosshairParent.gameObject.SetActive(true);
                Player.Zoom.TryStop();
                break;
        }
        CSVR_GameSetting.lookSensitivitySpeedZoom = 1f;
        
        CSVR_UIPlay.instance.imgSniperCrosshair.SetActive(false);
        CSVR_UIPlay.instance.btnStopZoom.gameObject.SetActive(false);
        //CSVR_UIPlay.instance.btnReload.gameObject.SetActive(true);
        if (!Player.Crouch.Active)
        {
            Controler.MotorAcceleration = 0.22f;
        }
        camView.SetActive(true);
        currentZoomIndex = 0;
		CSVR_UIPlay.instance.changeCameraFOV(CSVR_GameSetting.normalFOV);
        HoldZoomAuto = false;
    }

    public string JsonUtility { get; set; }
}
