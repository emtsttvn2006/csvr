﻿using UnityEngine;
using System.Collections;
using PigeonCoopToolkit.Effects.Trails;

public class DieuCay_Anim : MonoBehaviour {

	public ParticleSystem SmokeBreath;
	public MeshRenderer HoaLua;
	public MeshRenderer ThuocLao;
	public SmokePlume Smoke;
	public AudioSource SoundFX;

	void Start(){
		EndSmoke ();
		TatHoaLua ();
		TatThuocLao ();
		TatKhoiThuoc ();
	}

	void BeginSmoke(){
		if(SmokeBreath){
			SmokeBreath.enableEmission = true;
		}
	}

	void EndSmoke(){
		if(SmokeBreath){
			SmokeBreath.enableEmission = false;
		}
	}

	void BatHoaLua(){
		if(HoaLua){
			HoaLua.enabled = true;
		}
	}

	void TatHoaLua(){
		if(HoaLua){
			HoaLua.enabled = false;
		}
	}

	void HienThuocLao(){
		if(ThuocLao){
			ThuocLao.enabled = true;
		}
	}

	void TatThuocLao(){
		if(ThuocLao){
			ThuocLao.enabled = false;
		}
	}

	void BatKhoiThuoc(){
		if(Smoke){
			Smoke.Emit = true;
		}
	}

	void TatKhoiThuoc(){
		if(Smoke){
			Smoke.Emit = false;
		}
	}

	void RareAnimAudio(AudioClip sfx){
		if(SoundFX){
			SoundFX.clip = sfx;
			SoundFX.Play();
		}
	}

}
