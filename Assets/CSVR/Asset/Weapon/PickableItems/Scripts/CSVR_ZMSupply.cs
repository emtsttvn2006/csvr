﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Internal;
using Horus.Json;
using UnityEngine.UI;

public class CSVR_ZMSupply : MonoBehaviour {

	public float BulletCapacityMultiplier = 10f;


	vp_PlayerInventory m_Inventory;
	vp_PlayerInventory PickerInventory;

	void OnTriggerEnter(Collider other)
	{
//		print ("co player cham vao");
		PickerInventory = other.GetComponent<vp_PlayerInventory> ();
		if (PickerInventory != null)
			SupplyRestore (PickerInventory);
	}

	void SupplyRestore(vp_PlayerInventory pickerInventory)
	{
		m_Inventory = pickerInventory;
		foreach(vp_UnitBankInstance unitBank in pickerInventory.UnitBankInstances)
		{
			pickerInventory.SetUnitCount(pickerInventory.GetInternalUnitBank(unitBank.UnitType), (int)(unitBank.PlayfabGunCapacity * BulletCapacityMultiplier));
		}
	}


}
