﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SoundC4 : MonoBehaviour {
	public AudioSource _audio;
	static float speed=1;
	int CountDown=0;
	[SerializeField]
	public ParticleSystem pr;
	private ParticleSystem.EmissionModule EmissionTurbina1;
	// Use this for initialization
	void Start () {
		_audio=this.GetComponent<AudioSource>();
		InvokeRepeating("PlaySound",0,speed);
		EmissionTurbina1=pr.emission;
	}
	void PlaySound()
	{
		CountDown++;
		_audio.Play();
		if(CountDown>=15)
		{
			speed=0.3f;
			EmissionTurbina1.rate=new ParticleSystem.MinMaxCurve(5);
		}
	}

}
