﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CSVR_Intro : MonoBehaviour {
    
    void Awake()
    {


		if (PlayerPrefs.HasKey ("CSVR_GameSetting.Quality")) {
			int quality = (int)PlayerPrefs.GetFloat ("CSVR_GameSetting.Quality");
			QualitySettings.SetQualityLevel(quality, true);
		}

		AsyncOperation async;
		async = Application.LoadLevelAsync(1);
		async.allowSceneActivation = false;
		StartCoroutine(LoadLevelProgress(async));
	}


    IEnumerator LoadLevelProgress(AsyncOperation async)
    {
		#if !UNITY_EDITOR_WIN && !UNITY_STANDALONE_WIN && !VIRTUAL_ROOM
		Handheld.PlayFullScreenMovie("Final2.mp4", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFill);
		#endif
        while (!async.isDone)
        {    
            yield return null;
        }

        // This is where I'm actually changing the scene
        async.allowSceneActivation = true;
    }
}
