﻿using UnityEngine;
using System.Collections;

public class CSVR_UIInviteFriend : MonoBehaviour {
	public static CSVR_UIInviteFriend instance = null;

	public GameObject friend;
	public Transform friendContainer;

	private void Awake () {
		instance = this;
	}

	private void OnEnable(){
		UpdateFriendList();
	}

	private void OnDestroy() {
		instance = null;
	}

	private void UpdateFriendList()
	{
		if (PhotonNetwork.Friends == null )
		{
			if (CSVR_GameSetting.friendsListName.Count > 0) {
				PhotonNetwork.FindFriends (CSVR_GameSetting.friendsListName.ToArray());		
				Invoke ("InitFriend", 2f);
			}
			return;
		}
		InitFriend ();

	}

	private void InitFriend(){
		FriendInfo[] friends = PhotonNetwork.Friends.ToArray();

		if (friends.Length > 0)
		{
			for (int i = 0; i < friends.Length; i++)
			{
				if (friends[i].Name != "Null")
				{
					GameObject f = Instantiate(friend) as GameObject;
					CSVR_UIInviteFriendChild info = f.GetComponent<CSVR_UIInviteFriendChild>();
					info.GetInfo(friends[i]);
					f.transform.SetParent(friendContainer, false);
					f.transform.localScale = Vector3.one;
				}
			}
		}
	
	}

	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
	public void UI_OnDisable(){
		this.gameObject.SetActive (false);
	}
}
