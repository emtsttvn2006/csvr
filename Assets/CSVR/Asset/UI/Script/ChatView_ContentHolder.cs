﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using MarchingBytes;

public class ChatView_ContentHolder : MonoBehaviour {
    
    public LoopVerticalScrollRect scroll;    
    public Text message;
    
    public void Open()
    {
        this.gameObject.SetActive(true);
        if (!scroll.prefabPool) scroll.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
    }

    public void Close()
    {
        this.gameObject.SetActive(false);
    }

    public void Reload(int Scroll_Length)
    {
        scroll.totalCount = Scroll_Length;
        scroll.RefillCells();
    }    
}
