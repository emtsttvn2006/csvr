﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Horus.ClientModels;
using Horus.Json;
using UnityEngine.UI;

public class CSVR_UIItemShop : MonoBehaviour {
	public StatusItem ItemStatus;
	public string ItemClass = null;
	public Text NameText = null;
	public Image IconImage = null;
	public Text PriceText = null;
	public Image PriceImage = null;
	public Button DetailButton = null;
	public CSVR_UIItemDetailShop ItemDetail;
	public Sprite[] coins;

	public void GetInfo(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
		
		Dictionary<string,string> status = HorusSimpleJson.DeserializeObject<Dictionary<string,string>> (item.CustomData);

		ItemStatus = (StatusItem) System.Enum.Parse(typeof(StatusItem), status["Status"]);

		if (ItemStatus == StatusItem.New) {
			this.gameObject.SetActive (true);
		} else if (ItemStatus == StatusItem.Disable) {
			DetailButton.interactable = false;
		}
		ItemClass = item.ItemClass;
		//NameText
		NameText.text = item.DisplayName;
        
		//IconImage
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite> ("Sprites/UIWeapon/" + item.ItemId);
		if (item07 != null) {
			//PriceText
			if (item07.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
				PriceText.text = item07.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
				PriceImage.sprite = coins [0];
			} else {
				PriceText.text = item07.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
				PriceText.color = new Color (1, 0.73f, 0, 1);
				PriceImage.sprite = coins [1];
			}
		} else {
			if (item.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
				PriceText.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
				PriceImage.sprite = coins [0];
			} else {
				PriceText.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
				PriceText.color = new Color (1, 0.73f, 0, 1);
				PriceImage.sprite = coins [1];
			}
		}
		//DetailButton
		switch (item.ItemClass) {
		case GameConstants.VNITEMCLASS_RifleWeapon:
		case GameConstants.VNITEMCLASS_SniperWeapon:
		case GameConstants.VNITEMCLASS_ShotGunWeapon:
		case GameConstants.VNITEMCLASS_SMGWeapon:
		case GameConstants.VNITEMCLASS_MachineWeapon:
		case GameConstants.VNITEMCLASS_PistolWeapon:
			DetailButton.onClick.AddListener(() =>{
				ItemDetail.GetInfoGun(item07,item30,item);
				CharacterView.instance.GetIDGunInShop(item.ItemId);
			});
			break;
		case GameConstants.VNITEMCLASS_MeleeWeapon:
			DetailButton.onClick.AddListener(() =>{
				ItemDetail.GetInfoMelee(item07,item30,item);
				CharacterView.instance.GetIDGunInShop(item.ItemId);
			});
			break;
		case GameConstants.VNITEMCLASS_HelmetWeapon:
		case GameConstants.VNITEMCLASS_ArmorWeapon:
			DetailButton.onClick.AddListener(() => {
			     ItemDetail.GetInfoArmor(item07,item30,item);
				//CharacterView.instance.GetIDGunInShop(item.ItemId);
			});
			break;
		case GameConstants.VNITEMCLASS_GrenadeWeapon:
		case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
			DetailButton.onClick.AddListener(() =>{ 
				ItemDetail.GetInfoGrenade(item07,item30,item);
				CharacterView.instance.GetIDGunInShop(item.ItemId);
			});
			break;
		}
	    
	}

	public void GetInfoAccessories(CatalogItem item)
	{

		ItemClass = item.ItemClass;
		//NameText
		NameText.text = item.DisplayName;

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/" + item.ItemId);

		if (item.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			PriceText.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
			PriceImage.sprite = coins [0];
		} else {
			PriceText.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
			PriceText.color = new Color (1, 0.73f, 0, 1);
			PriceImage.sprite = coins [1];
		}
		DetailButton.onClick.AddListener(() =>{
			ItemDetail.GetInfoAccessories(null,null,item);

		});
	}

	public void GetInfoCharacter(CatalogItem item)
	{
		ItemClass = item.ItemClass;
		//NameText
		NameText.text = item.DisplayName;

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/" + item.ItemId);

		if (item.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			PriceText.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
			PriceImage.sprite = coins [0];
		} else {
			PriceText.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
			PriceText.color = new Color (1, 0.73f, 0, 1);
			PriceImage.sprite = coins [1];
		}
		DetailButton.onClick.AddListener(() =>{
			ItemDetail.GetInfoCharacter(null,null,item);

		});
	}
}
