﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus.ClientModels;

public class CSVR_UIShop : SingletonBaseScreen<CSVR_UIShop> {
	
	[SerializeField] private Image avatar_Icon;
	[SerializeField] private Text player_Name;
	[SerializeField] private Text player_Level;
	[SerializeField] private Image level_Icon;
	[SerializeField] private Transform ChatView_Parent;
	public GameObject InventoryItemUIPrefab = null;
	[Space(5)]
	public Transform PanelListAll = null;
	public Transform PanelListAC = null;
	public Transform PanelListCT = null;
	private List<CSVR_UIItemShop> cacheItemsInfo = new List<CSVR_UIItemShop>();
	private List<CSVR_UIItemShop> cacheAccsInfo = new List<CSVR_UIItemShop>();


	void Start(){
		UpdateItemListItems ();
		UpdateItemListCharacter ();
		UpdateItemListAccessories ();

		avatar_Icon.sprite = AccountManager.instance.avatarPicture;
		player_Name.text = AccountManager.instance.displayName;
		player_Level.text = "Cấp độ: "+ CSVR_GameSetting.accountLevel.ToString();
		level_Icon.sprite = UISpriteManager.instance.GetLevelSprite(CSVR_GameSetting.accountLevel);
	}

    public override void Open()
    {
        base.Open();

        TopBar.instance.SetBackButton(new UnityAction(() => {            
            MainView.instance.Open();
            CharacterView.instance.Open();
            Close();
        }));

        TopBar.instance.SetShopOrInVButton(new UnityAction(() => {            
            CSVR_UIInventory.instance.Open();
            CSVR_UIInventory.instance.UI_OnInventoryOn();            
            AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
            Close();
        }));

        TopBar.instance.Setting.interactable = true;
        TopBar.instance.ShopOrInVText.text = "Thùng đồ";
        Invoke("ChatViewSetParent", 0.5f);
    }

    public override void OnEnable()
	{
		TopBar.instance.SetBackButton(new UnityAction(() =>{
            MainView.instance.Open();
            CharacterView.instance.Open();

            Close();
        }));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			CSVR_UIShop.instance.Close();
			CSVR_UIInventory.instance.Open();
			CSVR_UIInventory.instance.UI_OnInventoryOn();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));
		TopBar.instance.Setting.interactable =  true;
		TopBar.instance.ShopOrInVText.text = "Thùng đồ";
		Invoke ("ChatViewSetParent", 0.5f);
	}
	private void ChatViewSetParent(){
		HomeScreen.instance.ChatView_Item.SetParent (ChatView_Parent, false);
		RectTransform rectTransform = HomeScreen.instance.ChatView_Item.GetComponent<RectTransform>();
		rectTransform.offsetMin = Vector3.zero;
		rectTransform.offsetMax = Vector3.zero;
	}
	private void UpdateItemListItems()
	{
		CatalogItem[] items = CSVR_GameSetting.shopItems.ToArray ();
		if (items.Length > 0)
		{
			for (int i = 0; i < items.Length; i++)
			{
				//if (items[i].ItemClass != GameConstants.VNITEMCLASS_ArmorWeapon)
				{
					GameObject f = Instantiate(InventoryItemUIPrefab) as GameObject;
					CSVR_UIItemShop info = f.GetComponent<CSVR_UIItemShop>();

					info.GetInfo(
						ContainsLoop( CSVR_GameSetting.shopItemsPeriod,items[i].ItemId + GameConstants.VNITEMPREFIX_7Day,items[i].ItemClass),
						ContainsLoop( CSVR_GameSetting.shopItemsPeriod,items[i].ItemId + GameConstants.VNITEMPREFIX_30Day,items[i].ItemClass),
						items[i]);
					
					cacheItemsInfo.Add(info);
					f.transform.SetParent(PanelListAll, false);
				}
			}
		}
	}
	private void UpdateItemListCharacter()
	{
		CatalogItem[] cts = CSVR_GameSetting.shopCharacter.ToArray ();
		if (cts.Length > 0)
		{
			for (int i = 0; i < cts.Length; i++)
			{

				GameObject f = Instantiate(InventoryItemUIPrefab) as GameObject;
				CSVR_UIItemShop info = f.GetComponent<CSVR_UIItemShop>();
				info.GetInfoCharacter (cts [i]);
				f.transform.SetParent(PanelListCT, false);
				//cacheAccsInfo.Add(info);
			}
		}
	}
	private void UpdateItemListAccessories()
	{
		CatalogItem[] acs = CSVR_GameSetting.shopAccessories.ToArray ();
		if (acs.Length > 0)
		{
			for (int i = 0; i < acs.Length; i++)
			{

				GameObject f = Instantiate(InventoryItemUIPrefab) as GameObject;
				CSVR_UIItemShop info = f.GetComponent<CSVR_UIItemShop>();
				info.GetInfoAccessories (acs [i]);
				f.transform.SetParent(PanelListAC, false);
				cacheAccsInfo.Add(info);
			}
		}
	}

	private CatalogItem ContainsLoop(List<CatalogItem> list, string id,string classItem)
	{
		for (int i = 0; i < list.Count; i++)
		{
			if (list[i].ItemClass == classItem && list[i].ItemId == id)
			{
				return list[i];
			}
		}
		return null;
	}

	public void SwichStatusItem(string itemStatus){
		StatusItem ItemStatus = (StatusItem) System.Enum.Parse(typeof(StatusItem), itemStatus);
		if (cacheItemsInfo.Count > 0)
		{
			for (int i = 0,lenght = cacheItemsInfo.Count; i < lenght; i++)
			{
				if(cacheItemsInfo[i].ItemStatus == ItemStatus){
					cacheItemsInfo[i].gameObject.SetActive(true);
				}else{
					cacheItemsInfo[i].gameObject.SetActive(false);
				}

			}
		}

	}

	public void SwichClassAcc(string itemClass){
		if (cacheAccsInfo.Count > 0)
		{
			if(itemClass == "All"){
				for (int i = 0,lenght = cacheAccsInfo.Count; i < lenght; i++)
				{
					cacheAccsInfo[i].gameObject.SetActive(true);
				}
			}else{
				for (int i = 0,lenght = cacheAccsInfo.Count; i < lenght; i++)
				{
					if(cacheAccsInfo[i].ItemClass.Contains(itemClass)){
						cacheAccsInfo[i].gameObject.SetActive(true);
					}else{
						cacheAccsInfo[i].gameObject.SetActive(false);
					}
				}
			}
		}
	}

	public void SwichClassItem(string itemClass){
		if (cacheItemsInfo.Count > 0)
		{
			if(itemClass == "All"){
				for (int i = 0,lenght = cacheItemsInfo.Count; i < lenght; i++)
				{
					cacheItemsInfo[i].gameObject.SetActive(true);
				}
			}else{
				for (int i = 0,lenght = cacheItemsInfo.Count; i < lenght; i++)
				{
					if (itemClass.Contains ("Helmet.Armor")) {//danh rieng cho set items
						if (cacheItemsInfo [i].ItemClass.Contains (GameConstants.VNITEMCLASS_HelmetWeapon)
							|| cacheItemsInfo [i].ItemClass.Contains (GameConstants.VNITEMCLASS_ArmorWeapon)) {
							cacheItemsInfo [i].gameObject.SetActive (true);
						} else {
							cacheItemsInfo [i].gameObject.SetActive (false);
						}
					} else {
						if (cacheItemsInfo [i].ItemClass.Contains (itemClass)) {
							cacheItemsInfo [i].gameObject.SetActive (true);
						} else {
							cacheItemsInfo [i].gameObject.SetActive (false);
						}
					}
				}
			}
		}
	}
	public void UI_ShopChangePolice_Click()
	{
		CharacterView.instance.ShowModePolice ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}

	public void UI_ShopChangeTerrorist_Click()
	{
		CharacterView.instance.ShowModeTerrorist ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}
		

	public void UI_New_Click()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		Application.OpenURL ("https://www.facebook.com/groups/supportCSM/");
	}
	public void UI_Event_Click()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		Application.OpenURL ("https://www.facebook.com/groups/supportCSM/");
	}
	public void UI_Buddy_Click()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		CSVR_UIBuddy.instance.Open();        

		TopBar.instance.SetBackButton(new UnityAction(() => {
			this.Open();
			CSVR_UIBuddy.instance.Close();
			AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
		}));
	}
	public void UI_Tinnhan_Click()
	{
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
		//Noti_Tinnhan.gameObject.SetActive(false);                
	}    
}
