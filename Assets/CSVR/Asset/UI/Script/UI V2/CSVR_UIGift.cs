﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
/*
public class CSVR_UIGift : MonoBehaviour {

	private class GetUserGrantedItemResult{
		public List<ItemInstance> GrantedResult { get; set;}
	}

	public Button[] buttons;
	int day = 0;
	void Start(){

		GetUserDataRequest request = new GetUserDataRequest ();
		request.Keys = new List<string>{"CheckInTracker"};
		PlayFabClientAPI.GetUserReadOnlyData (request, ResultCallback, ErrorCallback);

	}
	void ResultCallback(GetUserDataResult result){

//		if (result.Data.ContainsKey ("CheckInTracker")) {
//
//			Dictionary<string,long> days = PlayFab.Json.JsonConvert.DeserializeObject<Dictionary<string,long>> (result.Data ["CheckInTracker"].Value);
//			day = (int)days ["LoginStreak"];
//			var timeDiff = CSVR_GameSetting.LastLogin - new System.DateTime (1970, 1, 1);
//
//			for (int i= 0; i < 7; i++) {
//				if (i == day && timeDiff.TotalMilliseconds > days ["NextEligibleGrant"]) {
//					buttons [i].interactable = true;
//					buttons [i].onClick.AddListener (() => CheckIn ());
//				} else if (i < day) {
//					buttons [i].transform.GetChild (0).GetComponent<Text> ().text = "Đã Nhận";
//				}
//			}
//		} else {
//			buttons [0].interactable = true;
//			buttons [0].onClick.AddListener (() => CheckIn ());
//		}

	}
	void ErrorCallback(PlayFabError error){
		Debug.Log (error.ErrorMessage);
	}

	public void CheckIn()
	{

		//Debug.Log("Checking-in with Server...");
//		RunCloudScriptRequest request = new RunCloudScriptRequest() { 
//			ActionId = "CheckInNewBie", 
//		};
//		
//		PlayFabClientAPI.RunCloudScript(request, OnCheckInCallback, OnApiCallError);


		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
		request.FunctionName = "CheckInNewBie";
		request.RevisionSelection = CloudScriptRevisionOption.Live;
		request.GeneratePlayStreamEvent = true;
		PlayFabClientAPI.ExecuteCloudScript(request,OnCheckInCallback, OnApiCallError);
	}
	
	void OnCheckInCallback(ExecuteCloudScriptResult result)
	{
		//Debug.Log(PlayFab.SimpleJson.SerializeObject(result.FunctionResult));
		//Debug.Log("CheckIn Results:");
		GetUserGrantedItemResult grantedItems = PlayFab.Json.JsonConvert.DeserializeObject<GetUserGrantedItemResult>(result.FunctionResult.ToString());
		
		if(grantedItems != null)
		{
//			Debug.Log(string.Format("You were granted {0} items:", grantedItems.Count));
//			
//			string output = string.Empty;
//			foreach(var item in grantedItems)
//			{
//				output = string.Format("\t {0}: {1}\n", item.ItemId, item.Annotation);
//				Debug.Log(output);
//			}

			ItemInstance newItem = grantedItems.GrantedResult[1];
//			Debug.Log("newItem "+newItem.ItemId);
//			PlayFabManager.instance.LoadInventoryItem(newItem);
//			CSVR_UIPopupConfirmShop.instance.ShowPopUpGrandItem(grantedItems.GrantedResult[1]);

			buttons[day].interactable = false;
			buttons[day].transform.GetChild(0).GetComponent<Text>().text = "Đã Nhận";
		}
        //else
        //{
        //    Debug.Log("CheckIn Successful! No items granted. \n" + result.Logs);
        //}
	}
	
	void OnApiCallError(PlayFabError err)
	{
		//string http = string.Format("HTTP:{0}", err.HttpCode);
		//string message = string.Format("ERROR:{0} -- {1}", err.Error, err.ErrorMessage);
		string details = string.Empty;
		
		if(err.ErrorDetails != null)
		{
			foreach(var detail in err.ErrorDetails)
			{
				details += string.Format("{0} \n", detail.ToString());
			}
		}
		
		//Debug.LogError(string.Format("{0}\n {1}\n {2}\n", http, message, details));
	}
    
}
*/
