﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using Horus.ClientModels;
using UnityEngine.UI;

public class CSVR_UIInventory : SingletonBaseScreen<CSVR_UIInventory>
{    
    public int Group = 1;
    public GameObject InventoryItemUIPrefab = null;

    public Transform PanelListAll = null;
	public Transform PanelListAC = null;
	public Transform PanelListCT = null;

	public List<CSVR_UIItemInv> cacheItemsInfo = new List<CSVR_UIItemInv>();
	private List<CSVR_UIItemInv> cacheAcsInfo = new List<CSVR_UIItemInv>();
	private List<CSVR_UIItemInv> cacheCharacterInfo = new List<CSVR_UIItemInv>();

	//backpack
	[SerializeField] private Transform listGroupParent; 
	[SerializeField] private Transform listBagParent; 
	[SerializeField] private GameObject listGroupGameObject; 
	[SerializeField] private GameObject listBagGameObject; 
    //public List<Toggle> listGroup = new List<Toggle>();
	public List<CSVR_UIItemBagInv> listGroup;
	public Sprite[] iconWeaponDefault;
	//end
    

    //Right Panel
    public Animator rightInfoPlayer_Pnl;
    public Animator rightPanel;
    public Animator rightDetailPanel;

    public Text UpgradeText;
	[SerializeField] private GameObject[] rightPanelObj; 
    public GameObject[] notification;    


	public override void Awake()
    {    
		base.Awake ();
		notification [0] = MainView.instance.notification_Icon;

		for (int i = 0, lenght = CSVR_GameSetting.bagCount; i < lenght; i++)
        {			
			UI_InstanceBackpack_Run (i);
        }


    }

	private void UI_InstanceBackpack_Run(int index){
		int tempIndex = index + 1;
		GameObject _listGroup;
		GameObject _listGBag;

		if(index != 0){
			_listGroup = Instantiate (listGroupGameObject);
			_listGroup.transform.SetParent (listGroupParent, false);
			_listGroup.name = "Group " + tempIndex;
			_listGroup.gameObject.SetActive(false);
			CSVR_UIItemBagInv _group = _listGroup.GetComponent<CSVR_UIItemBagInv> ();
			_group.GroupID = tempIndex;
			listGroup.Add (_group);

			_listGBag = Instantiate (listBagGameObject);
			_listGBag.transform.SetParent (listBagParent, false);
			_listGBag.name = "Bag " + tempIndex;
		}else{
			_listGroup = listGroupGameObject;
			_listGBag = listBagGameObject;
		}


		Toggle _tog = _listGBag.transform.GetChild (0).GetComponent<Toggle> ();
		_tog.transform.GetChild (1).GetComponent<Text> ().text = tempIndex.ToString ();
		if (index != 0) {
			_tog.isOn = false;
		}

		_tog.onValueChanged.RemoveAllListeners ();
		_tog.onValueChanged.AddListener((value) => {
			_listGroup.gameObject.SetActive(value);
			if (value)
			{
				ToggleClicked(tempIndex);
				listGroup[tempIndex - 1].CheckVIewGun();
			}
		});
	}
	void Start(){
		UpdateItemList();
	}
    public override void OnEnable()
	{
		TopBar.instance.SetBackButton(new UnityAction(() =>{			
			MainView.instance.Open();
            CharacterView.instance.Open();

            CloseChildrenToo();
        }));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{			
			CSVR_UIShop.instance.Open();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);

            CloseChildrenToo();
        }));

		TopBar.instance.ShopOrInVText.text = "Cửa hàng";
	}

    void ToggleClicked(int toggleNo)
    {       
        CSVR_GameSetting.bagStartIndex = (byte)(toggleNo - 1);
		Group = CSVR_GameSetting.bagStartIndex + 1;
    }

    // Load Game
    public void UpdateItemList()
    {
        //ItemInstance[] items = CSVR_GameSetting.inventoryItems.ToArray ();
        //Debug.Log("UpdateItemList:" + AccountManager.instance.inventory.Count);
        ItemInstance[] items = AccountManager.instance.inventory.ToArray();
        CleanCacheList();
        if (items.Length > 0)
        {
            for (int i = 0, lenght = items.Length; i < lenght; i++)
            {
                if (items[i].CatalogVersion == GameConstants.VNITEMCATALOG_Item 
					|| items[i].CatalogVersion == GameConstants.VNITEMCATALOG_Upgrade 
					|| items[i].CatalogVersion == GameConstants.VNITEMCATALOG_ItemPeriod
					|| items[i].CatalogVersion == GameConstants.VNITEMCATALOG_Accessories
					|| items[i].CatalogVersion == GameConstants.VNITEMCATALOG_Character)
                {
                    InstanceItem(items[i]);
                }

            }
			//Debug.Log("1. AccountManager.instance.keyCards.Count: "+AccountManager.instance.keyCards.Count);
        }
    }

    bool ContainsLoop(List<ItemInstance> list, ItemInstance value)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i]._id == value._id)
            {
                return true;
            }
        }
        return false;
    }

    // Load Game and Purchase Item
    //	public void UpdateItemListAfterPurchaseItem()
    //	{
    //		ItemInstance[] items = AccountManager.instance.inventory.ToArray();
    //		List<ItemInstance> itemsNew = AccountManager.instance.itemsNew;
    //
    //		if (items.Length > 0)
    //		{
    //			for (int i = 0,lenght = items.Length; i < lenght; i++)
    //			{
    //				if (items[i].CatalogVersion == "Item" || items[i].CatalogVersion == "Item03Day" || items[i].CatalogVersion == "Item07Day" || items[i].CatalogVersion == "Item30Day")
    //				{
    //					if(ContainsLoop(itemsNew,items[i])){
    //						InstanceItemV3(items[i]);
    //						ShowNotificationPurchase(items[i].ItemClass,true);
    //					}
    //				}
    //			}
    //		}
    //	}

    //	public void UpdateItemListV2()
    //	{
    //		ItemInstance[] items = AccountManager.instance.inventory.ToArray();
    //		List<ItemInstance> itemsNew = AccountManager.instance.itemsNew;
    //
    //		CleanCacheList();
    //		if (items.Length > 0)
    //		{
    //			for (int i = 0,lenght = items.Length; i < lenght; i++)
    //			{
    //				if (items[i].CatalogVersion == "Item" || items[i].CatalogVersion == "Item03Day" || items[i].CatalogVersion == "Item07Day" || items[i].CatalogVersion == "Item30Day")
    //				{
    //					if(ContainsLoop(itemsNew,items[i])){
    //						InstanceItemV3(items[i]);
    //						ShowNotificationPurchase(items[i].ItemClass,true);
    //
    //					}else{
    //						InstanceItem(items[i]);
    //					}
    //					
    //				}
    //			}
    //		}
    //	}

	public void ShowNotificationPurchase(ItemInstance item, bool b)
    {
		if (item != null)
        {
            for (int i = 4; i < 18; i++)
            {
				if (notification[i].gameObject.name == item.ItemClass)
                {
                    notification[i].gameObject.SetActive(b);

					switch (item.CatalogVersion) {
					case GameConstants.VNITEMCATALOG_Item:
					case GameConstants.VNITEMCATALOG_Upgrade:
					case GameConstants.VNITEMCATALOG_ItemPeriod:
						notification [1].gameObject.SetActive (b);
						break;
					case GameConstants.VNITEMCATALOG_Accessories:
						notification [3].gameObject.SetActive (b);
						break;
					}

					if (notification [0] != null) {
						if (AccountManager.instance.itemsNew.Count == 0 && !b) {
							notification [0].gameObject.SetActive (false);
						} else{
							notification [0].gameObject.SetActive (true);
						}
					}
                    break;
                }
            }
        }
    }

    //Instance item
    public void InstanceItem(ItemInstance item)
    {
        GameObject f = Instantiate(InventoryItemUIPrefab) as GameObject;
        CSVR_UIItemInv info = f.GetComponent<CSVR_UIItemInv>();
        info.GetInfo(item);
        
		switch (info.ItemClass) {
		case GameConstants.VNITEMCLASS_MuzzleAccessories:
		case GameConstants.VNITEMCLASS_GripAccessories:
		case GameConstants.VNITEMCLASS_MagazineAccessories:
		case GameConstants.VNITEMCLASS_ScopeAccessories:
		case GameConstants.VNITEMCLASS_SkinAccessories:
		case GameConstants.VNITEMCLASS_AccAccessories:
			{
				info.UI_StateEquipButtonClick (false);
				f.transform.SetParent (PanelListAC, false);
				cacheAcsInfo.Add(info);
				break;
			}
		case GameConstants.VNITEMCLASS_RifleWeapon:
		case GameConstants.VNITEMCLASS_SniperWeapon:
		case GameConstants.VNITEMCLASS_ShotGunWeapon:
		case GameConstants.VNITEMCLASS_SMGWeapon:
		case GameConstants.VNITEMCLASS_MachineWeapon:
		case GameConstants.VNITEMCLASS_PistolWeapon:
		case GameConstants.VNITEMCLASS_MeleeWeapon:
		case GameConstants.VNITEMCLASS_GrenadeWeapon:
		case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
		case GameConstants.VNITEMCLASS_HelmetWeapon:	
		case GameConstants.VNITEMCLASS_ArmorWeapon:	
			{
				info.UI_StateEquipButtonClick (true);
				info.GetInfoEquipItems(item);
				f.transform.SetParent (PanelListAll, false);
				cacheItemsInfo.Add(info);
				break;
			}
		case GameConstants.VNITEMCLASS_DefaultCharacter:	
			{
				info.UI_StateEquipButtonClick (true);
				info.GetInfoEquipCharacter(item);

				f.transform.SetParent (PanelListCT, false);
				cacheCharacterInfo.Add(info);
				break;
			}
		}

		f.transform.SetSiblingIndex(0);
    }

    //Instance item purchase
    public void InstanceItemPurchase(ItemInstance item)
    {
        GameObject f = Instantiate(InventoryItemUIPrefab) as GameObject;
        CSVR_UIItemInv info = f.GetComponent<CSVR_UIItemInv>();
		info.GetInfo(item);
       
        info.GetInfoPurchase(item);
       

		switch (info.ItemClass) {
		case GameConstants.VNITEMCLASS_MuzzleAccessories:
		case GameConstants.VNITEMCLASS_GripAccessories:
		case GameConstants.VNITEMCLASS_MagazineAccessories:
		case GameConstants.VNITEMCLASS_ScopeAccessories:
		case GameConstants.VNITEMCLASS_SkinAccessories:
		case GameConstants.VNITEMCLASS_AccAccessories:
			{
				info.UI_StateEquipButtonClick (false);
				f.transform.SetParent (PanelListAC, false);
				cacheAcsInfo.Add(info);
				break;
			}
		case GameConstants.VNITEMCLASS_RifleWeapon:
		case GameConstants.VNITEMCLASS_SniperWeapon:
		case GameConstants.VNITEMCLASS_ShotGunWeapon:
		case GameConstants.VNITEMCLASS_SMGWeapon:
		case GameConstants.VNITEMCLASS_MachineWeapon:
		case GameConstants.VNITEMCLASS_PistolWeapon:
		case GameConstants.VNITEMCLASS_MeleeWeapon:
		case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
		case GameConstants.VNITEMCLASS_GrenadeWeapon:
		case GameConstants.VNITEMCLASS_HelmetWeapon:	
		case GameConstants.VNITEMCLASS_ArmorWeapon:	
			{
				info.GetInfoEquipItems(item);
				info.UI_StateEquipButtonClick (true);
				f.transform.SetParent (PanelListAll, false);
				cacheItemsInfo.Add(info);
				break;
			}
		case GameConstants.VNITEMCLASS_DefaultCharacter:	
			{
				info.GetInfoEquipCharacter(item);
				info.UI_StateEquipButtonClick (true);
				f.transform.SetParent (PanelListCT, false);
				cacheCharacterInfo.Add(info);
				break;
			}
		}
			
		f.transform.SetSiblingIndex(0);
    }
    //Instance item craft
    public void InstanceItemCraft(ItemInstance currentItem, ItemInstance nextItem)
    {
        if (currentItem._id != nextItem._id)
        {
			
            GameObject f = Instantiate(InventoryItemUIPrefab) as GameObject;
            CSVR_UIItemInv info = f.GetComponent<CSVR_UIItemInv>();
            info.GetInfo(nextItem);

            info.GetInfoUpgrade(nextItem);
            
			switch (info.ItemClass) {
			case GameConstants.VNITEMCLASS_DefaultCharacter:	
				{
					info.GetInfoEquipCharacter(nextItem);
					DestroyItemCraftOrSell (currentItem, 3, true);
					cacheCharacterInfo.Add (info);

					info.UI_StateEquipButtonClick (true);
					f.transform.SetParent (PanelListCT, false);
					break;
				}
			case GameConstants.VNITEMCLASS_MuzzleAccessories:
			case GameConstants.VNITEMCLASS_GripAccessories:
			case GameConstants.VNITEMCLASS_MagazineAccessories:
			case GameConstants.VNITEMCLASS_ScopeAccessories:
			case GameConstants.VNITEMCLASS_SkinAccessories:
			case GameConstants.VNITEMCLASS_AccAccessories:
				{
					DestroyItemCraftOrSell (currentItem, 2, true);
					cacheAcsInfo.Add (info);

					info.UI_StateEquipButtonClick (false);
					f.transform.SetParent (PanelListAC, false);
					break;
				}
			case GameConstants.VNITEMCLASS_RifleWeapon:
			case GameConstants.VNITEMCLASS_SniperWeapon:
			case GameConstants.VNITEMCLASS_ShotGunWeapon:
			case GameConstants.VNITEMCLASS_SMGWeapon:
			case GameConstants.VNITEMCLASS_MachineWeapon:
			case GameConstants.VNITEMCLASS_PistolWeapon:
			case GameConstants.VNITEMCLASS_MeleeWeapon:
			case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
			case GameConstants.VNITEMCLASS_GrenadeWeapon:
			case GameConstants.VNITEMCLASS_HelmetWeapon:	
			case GameConstants.VNITEMCLASS_ArmorWeapon:	
				{
					info.GetInfoEquipItems(nextItem);
					DestroyItemCraftOrSell (currentItem, 1, true);
					cacheItemsInfo.Add (info);

					info.UI_StateEquipButtonClick (true);
					f.transform.SetParent (PanelListAll, false);
					break;
				}

			}
			f.transform.SetSiblingIndex(0);

            
        }
    }

	public void DestroyItemCraftOrSell(ItemInstance currentItem,int isItem,bool craft){
		List<CSVR_UIItemInv> listItemsInfo = new List<CSVR_UIItemInv>();

		if(isItem == 1){
			listItemsInfo = cacheItemsInfo;
		}else if(isItem == 2){
			listItemsInfo = cacheAcsInfo;
		}else if(isItem == 3){
			listItemsInfo = cacheCharacterInfo;
		}

		if (listItemsInfo.Count > 0)
		{
			for (int i = 0; i < listItemsInfo.Count; i++)
			{
				if (listItemsInfo[i].ItemInstanceId == currentItem._id)
				{
					//Debug.Log ("destroy game object item");
					listItemsInfo[i].UnEquipSellSuccess();
					Destroy(listItemsInfo[i].gameObject);
					listItemsInfo.Remove(listItemsInfo[i]);
				}
			}
		}
		if (!craft) {
			UI_OnInventoryOn ();
		}
	}

    void CleanCacheList()
    {
        if (cacheItemsInfo.Count > 0)
        {
            for (int i = 0, lenght = cacheItemsInfo.Count; i < lenght; i++)
            {
                Destroy(cacheItemsInfo[i].gameObject);
            }
			cacheItemsInfo.Clear();
        }
		if (cacheAcsInfo.Count > 0)
		{
			for (int i = 0, lenght = cacheAcsInfo.Count; i < lenght; i++)
			{
				Destroy(cacheAcsInfo[i].gameObject);
			}
			cacheAcsInfo.Clear();
		}
    }

    // sell item success
    public void SellSuccess(string itemInstanceId)
    {
        if (cacheItemsInfo.Count > 0)
        {
            for (int i = 0; i < cacheItemsInfo.Count; i++)
            {
                if (cacheItemsInfo[i].ItemInstanceId == itemInstanceId)
                {
                    cacheItemsInfo[i].UnEquipSellSuccess();
                    Destroy(cacheItemsInfo[i].gameObject);
                    cacheItemsInfo.Remove(cacheItemsInfo[i]);
                }
            }
        }
    }

    //equip item success
    public void EquipItemSuccess(string itemClass, int group)
    {
        if (cacheItemsInfo.Count > 0)
        {
            if (itemClass == GameConstants.VNITEMCLASS_RifleWeapon 
				|| itemClass == GameConstants.VNITEMCLASS_SniperWeapon 
				|| itemClass == GameConstants.VNITEMCLASS_ShotGunWeapon 
				|| itemClass == GameConstants.VNITEMCLASS_SMGWeapon 
				|| itemClass == GameConstants.VNITEMCLASS_MachineWeapon)
            {
                for (int i = 0; i < cacheItemsInfo.Count; i++)
                {
                    if ((cacheItemsInfo[i].ItemClass == GameConstants.VNITEMCLASS_RifleWeapon 
						|| cacheItemsInfo[i].ItemClass == GameConstants.VNITEMCLASS_SniperWeapon 
						|| cacheItemsInfo[i].ItemClass == GameConstants.VNITEMCLASS_ShotGunWeapon 
						|| cacheItemsInfo[i].ItemClass == GameConstants.VNITEMCLASS_SMGWeapon 
						|| cacheItemsInfo[i].ItemClass == GameConstants.VNITEMCLASS_MachineWeapon) 
						&& cacheItemsInfo[i].BaloText.text == group.ToString())
                    {
                        cacheItemsInfo[i].UnEquipMoreItem();
                    }
                }
            }
            else
            {
                for (int i = 0; i < cacheItemsInfo.Count; i++)
                {
                    if (cacheItemsInfo[i].ItemClass == itemClass && cacheItemsInfo[i].BaloText.text == group.ToString())
                    {
                        cacheItemsInfo[i].UnEquipMoreItem();
                    }
                }
            }
        }
    }
	//equip item success
	public void EquipCharacterSuccess(string itemClass, int group)
	{
		if (cacheCharacterInfo.Count > 0)
		{
			if (itemClass == GameConstants.VNITEMCLASS_DefaultCharacter )
			{
				for (int i = 0; i < cacheCharacterInfo.Count; i++)
				{
					if ((cacheCharacterInfo[i].ItemClass == GameConstants.VNITEMCLASS_DefaultCharacter) && cacheItemsInfo[i].BaloText.text == group.ToString())
					{
						cacheCharacterInfo[i].UnEquipMoreItem();
					}
				}
			}
		}
	}
    //switch tab gun
    public void SwichClassItem(string itemClass)
    {
        if (cacheItemsInfo.Count > 0)
        {
			if (itemClass.Contains("All"))
            {
                for (int i = 0, lenght = cacheItemsInfo.Count; i < lenght; i++)
                {
                    cacheItemsInfo[i].gameObject.SetActive(true);
                }
            }
            else
            {
                for (int i = 0, lenght = cacheItemsInfo.Count; i < lenght; i++)
                {
					if (itemClass.Contains ("Helmet.Armor")) {
						if (cacheItemsInfo [i].ItemClass.Contains (GameConstants.VNITEMCLASS_HelmetWeapon)
							|| cacheItemsInfo [i].ItemClass.Contains (GameConstants.VNITEMCLASS_ArmorWeapon)) {
							cacheItemsInfo [i].gameObject.SetActive (true);
						} else {
							cacheItemsInfo [i].gameObject.SetActive (false);
						}
					} else {
						if (cacheItemsInfo [i].ItemClass.Contains (itemClass)) {
							cacheItemsInfo [i].gameObject.SetActive (true);
						} else {
							cacheItemsInfo [i].gameObject.SetActive (false);
						}
					}

                }
            }
        }
    }
	//switch tab accessories
	public void SwichClassAcs(string itemClass)
	{
		if (cacheAcsInfo.Count > 0)
		{
			if (itemClass.Contains("All"))
			{
				for (int i = 0, lenght = cacheAcsInfo.Count; i < lenght; i++)
				{
					cacheAcsInfo[i].gameObject.SetActive(true);
				}
			}
			else
			{
				for (int i = 0, lenght = cacheAcsInfo.Count; i < lenght; i++)
				{
					if (cacheAcsInfo[i].ItemClass.Contains(itemClass))
					{
						cacheAcsInfo[i].gameObject.SetActive(true);
					}
					else
					{
						cacheAcsInfo[i].gameObject.SetActive(false);
					}
				}
			}
		}
	}


    public void UpgradeFaile()
    {
        StartCoroutine(WaitAndPrint(null));
        UpgradeText.gameObject.SetActive(true);
        UpgradeText.text = "Nâng cấp thất bại!";
    }

    public void UpgradeSuccess(string level)
    {
        Debug.Log(level);
        PopupEffectScreenUp.instance.Open();

        StartCoroutine(WaitAndPrint(level));
        UpgradeText.gameObject.SetActive(true);
        UpgradeText.text = "Nâng cấp thành công!";
    }

    IEnumerator WaitAndPrint(string level)
    {
        if (!string.IsNullOrEmpty(level))
        {
            yield return new WaitForSeconds(1f);
            PopupEffectScreenUp.instance.Play(level);
        }

        yield return new WaitForSeconds(1.5f);
        UpgradeText.gameObject.SetActive(false);
        PopupEffectScreenUp.instance.Close();
    }    

    public void UI_OnInventoryOn()
    {
		rightPanelObj [0].gameObject.SetActive (true);
		rightPanelObj [1].gameObject.SetActive (false);
		rightPanelObj [2].gameObject.SetActive (false);
		rightPanelObj [3].gameObject.SetActive (false);
//        CloseAnAnimator(rightInfoPlayer_Pnl);
//        CloseAnAnimator(rightDetailPanel);
//        OpenAnAnimator(rightPanel);
    }

    public void UI_OnDetailOn()
    {
		rightPanelObj [0].gameObject.SetActive (false);
		rightPanelObj [1].gameObject.SetActive (true);
		rightPanelObj [2].gameObject.SetActive (false);
		rightPanelObj [3].gameObject.SetActive (false);
//        CloseAnAnimator(rightInfoPlayer_Pnl);
//        CloseAnAnimator(rightPanel);
//        OpenAnAnimator(rightDetailPanel);
    }

    public void UI_OnProfilePlayerOn()
    {        
		rightPanelObj [0].gameObject.SetActive (false);
		rightPanelObj [1].gameObject.SetActive (false);
		rightPanelObj [2].gameObject.SetActive (true);
		rightPanelObj [3].gameObject.SetActive (false);
//        CloseAnAnimator(rightPanel);
//        CloseAnAnimator(rightDetailPanel);
//        OpenAnAnimator(rightInfoPlayer_Pnl);
    }
	public void UI_OnSanLungTwoOn()
	{        
		rightPanelObj [0].gameObject.SetActive (false);
		rightPanelObj [1].gameObject.SetActive (false);
		rightPanelObj [2].gameObject.SetActive (false);
		rightPanelObj [3].gameObject.SetActive (true);

	}
	public void UI_OnJoinRoomMenu_Click()
	{
		if (CSVR_GameSetting.modeName != "") {

			if ( CSVR_GameSetting.mapName != "") {
				HomeScreen.instance.SetLoadingMapCallBack (UI_LoadingMap_CallBack);
				//RoomIsFinding ();

				CSVR_MPConnection.instance.OnQuickJoinedRoom(true);
				CSVR_MPConnection.instance.JoinRoom ();

				CSVR_GameSetting.TuTaoPhong = false;
				CSVR_GameSetting.maxPlayer = 11;
				CSVR_GameSetting.maxSkill = 50;
				CSVR_GameSetting.timeMatch = 500;
			}
		}
	}
	private void UI_LoadingMap_CallBack(){
		CSVR_UIInventory.instance.Close ();
		TopBar.instance.Close ();
	}

	public void UI_InvToShop_Click()
	{
		CSVR_UIInventory.instance.CloseChildrenToo();
		CSVR_UIShop.instance.Open();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);

	}
	public void UI_InvChangePolice_Click()
	{
		CharacterView.instance.ShowModePolice ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);

	}
	public void UI_InvChangeTerrorist_Click()
	{
		CharacterView.instance.ShowModeTerrorist ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);

	}    
}
