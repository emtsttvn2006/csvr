﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;

public class CSVR_UISpinCard : SingletonBaseScreen<CSVR_UISpinCard> {
	[SerializeField] private Text[] cardPrices;
	[SerializeField] private Image[] itemRewardsIcon;
	[SerializeField] private Text[] itemRewardsName;

	private  List<CatalogItem> itemsBundleCard;
	private  List<CatalogItem> itemsKeyCard;
	private int spinCount = 0;

	void Awake(){
		StartCoroutine (UI_OnShowBundleCard_Run (5));
		//GetCatalogItemsBundleCard ();
		//GetCatalogItemsKeyCard ();
		SetBundlePrices ("miễn phí",-1);
	}

	byte ErrorGetCatalogItemsKeyCardCount = CSVR_GameSetting.errorCountCallBack;
	private void GetCatalogItemsKeyCard()
	{
		HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = "key"
			}, 
			(resultItemPeriod) => {
				ErrorGetCatalogItemsKeyCardCount = CSVR_GameSetting.errorCountCallBack;
				itemsKeyCard = resultItemPeriod.Catalog;
				itemsKeyCard.Sort ();
			},
			(error) => {
				if(ErrorGetCatalogItemsKeyCardCount > 0){
					ErrorGetCatalogItemsKeyCardCount--;
					Invoke ("GetCatalogItemsKeyCard", 0.5f);
				}
			}
		);
	}


	byte ErrorGetCatalogItemsBundleCardCount = CSVR_GameSetting.errorCountCallBack;
	private void GetCatalogItemsBundleCard()
	{
		HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = "BundleCard"
			}, 
			(resultItemPeriod) => {
				ErrorGetCatalogItemsBundleCardCount = CSVR_GameSetting.errorCountCallBack;
				itemsBundleCard = resultItemPeriod.Catalog;
				itemsBundleCard.Sort ();
			},
			(error) => {
				if(ErrorGetCatalogItemsBundleCardCount > 0){
					ErrorGetCatalogItemsBundleCardCount--;
					Invoke ("GetCatalogItemsBundleCard", 0.5f);
				}
			}
		);
	}

	private void SetBundlePrices(string prices,int cardIndex){
		for (int i = 0; i < cardPrices.Length; i++) {
			if (cardPrices [i].transform.parent.GetComponent<Button> ().interactable) {
				if (i == cardIndex) {
					cardPrices [i].transform.parent.GetComponent<Button> ().interactable = false;
					cardPrices [i].text = "Đã mở";
				} else {
					cardPrices [i].text = prices;
				}
			}
		}
	}
	IEnumerator UI_OnShowBundleCard_Run(int count){
		for (int i = 0; i < count; i++) {
			float wai = (float)(i / 10 + 0.35);
			yield return new WaitForSeconds (wai);
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
			cardPrices [i].transform.parent.gameObject.SetActive(true);
		}
	}
	public void UI_OnOpenBundleCard_Click(int cardIndex) {
		
		if (itemsKeyCard == null ||itemsBundleCard == null || spinCount > itemsBundleCard.Count)
			return;
		if (AccountManager.instance.keyCards.Contains (itemsKeyCard [spinCount].ItemId)) {
			Scr_SpinBundleCard_Run (cardIndex);
		} else {
			string moneyUse = GameConstants.VNPRICE_Coin;


			if (AccountManager.instance.gameDollarAmount - int.Parse (itemsKeyCard [spinCount].VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ()) < 0) {
				ErrorView.instance.ShowPopupError ("Bạn không đủ tiền để quay thưởng.");
				return;
			}
			AccountManager.instance.gameDollarAmount -= int.Parse(itemsKeyCard [spinCount].VirtualCurrencyPrices[GameConstants.VNPRICE_Gold].ToString());

			moneyUse = GameConstants.VNPRICE_Gold;
			PurchaseItemRequest request = new PurchaseItemRequest () {
				Purchases = new List<ItemPurchase> () {
					new ItemPurchase () {
						ItemId = itemsKeyCard [spinCount].ItemId,
						moneyUse = moneyUse,
						RemainingUses = 1,
						UseOnBuy = 0
					}
				}
			};

			HorusClientAPI.PurchaseItem(request, 
				(result) => {
					Scr_SpinBundleCard_Run (cardIndex);
				},
				(error) => {
					//ErrorView.instance.ShowPopupError ("Mua trang bị thất bại");
				}
			);
		}
	}
	private void Scr_SpinBundleCard_Run(int cardIndex){
		CraftItemRequest request = new CraftItemRequest () {
			ItemId = itemsBundleCard [spinCount].ItemId,
			useItem = "1"
		};

		HorusClientAPI.CraftItem (request, 
			(result) => {
				AccountManager.instance.keyCards.Remove(itemsKeyCard [spinCount].ItemId);
				spinCount++;


				Debug.Log("3. AccountManager.instance.keyCards.Count: "+AccountManager.instance.keyCards.Count);
				if(result.ItemCraft.Count > 0){
					RewardItem(cardIndex, result.ItemCraft [0]);
				}else{
					RewardItem(cardIndex);
				}
				try{
					SetBundlePrices (itemsKeyCard [spinCount].VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString (),cardIndex);
				}catch{
					Debug.Log("SetBundlePrices"+cardIndex);
					SetBundlePrices ("",cardIndex);	
				}
//				if(spinCount + 1 < itemsKeyCard.Count)
//					SetBundlePrices (itemsKeyCard [spinCount].VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString (),cardIndex);	
			},
			(error) => {

			}
		);
	}
	private void RewardItem(int i,ItemInstance item){
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.spin_Reward);
		itemRewardsIcon [i].transform.gameObject.SetActive (true);
		itemRewardsName [i].transform.gameObject.SetActive (true);
		itemRewardsIcon [i].transform.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		itemRewardsIcon [i].SetNativeSize ();

		if (item.ItemId == GameConstants.VNITEMID_Coin) {
			itemRewardsIcon [i].transform.localScale = Vector3.one;
			itemRewardsName[i].text = ((int)item.RemainingUses - AccountManager.instance.gameCoinAmount) + "(vàng)";
			AccountManager.instance.gameCoinAmount = (int)item.RemainingUses;
		}else if (item.ItemId == GameConstants.VNITEMID_Gold) {
			itemRewardsIcon [i].transform.localScale = Vector3.one;
			itemRewardsName[i].text = ((int)item.RemainingUses - AccountManager.instance.gameDollarAmount) + "(kim cương)";
			AccountManager.instance.gameDollarAmount = (int)item.RemainingUses;
		} else {
			itemRewardsIcon [i].transform.localScale = new Vector3(0.4f,0.4f,0.4f);

			if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_3Day)) {
				itemRewardsName[i].text = item.DisplayName + "(03Ngày)";
			} 
			else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_7Day)) {
				itemRewardsName[i].text = item.DisplayName + "(7Ngày)";
			}
			else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_30Day)) {
				itemRewardsName[i].text = item.DisplayName + "(30Ngày)";
			} 
			else {
				itemRewardsName[i].text = item.DisplayName + "(Vĩnh Viễn)";
			}


			AccountManager.instance.itemsNew.Add (item);
			AccountManager.instance.inventory.Add (item);

			//CSVR_UIInventory.instance.InstanceItemPurchase (item);
			//CSVR_UIInventory.instance.ShowNotificationPurchase(item,true);
		}
	}
	private void RewardItem(int i){


		itemRewardsName [i].transform.gameObject.SetActive (true);
		itemRewardsName [i].text = "Chúc bạn may mắn lần sau";

	}
	public void UI_DestroyThis_Click(){
		Destroy (this.gameObject);
	}
}
