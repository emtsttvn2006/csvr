﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UIInventoryInfoPlayer : MonoBehaviour {


	[SerializeField] private Image avatar_Icon;
	[SerializeField] private Text player_Name;
	[SerializeField] private Text player_Level;
	[SerializeField] private Image level_Icon;

	[SerializeField] private Text playerName;
	[SerializeField] private Text playerLevel;
	[SerializeField] private Text playerID;
	[SerializeField] private Text playerExp;

	[SerializeField] private Text  playerCurrentLevelText;
	[SerializeField] private Image playerCurrentLevelIcon;
	[SerializeField] private Text  playerNextLevelText;
	[SerializeField] private Image playerNextLevel;
	[SerializeField] private Image playerLevelFill;

	[SerializeField] private Text playerKill;
	[SerializeField] private Text playerDeath;
	[SerializeField] private Text playerWin;
	[SerializeField] private Text playerLot;
	[SerializeField] private Text playerHeadShot;

	void Start(){
		avatar_Icon.sprite = AccountManager.instance.avatarPicture;
		player_Name.text = AccountManager.instance.displayName;
		player_Level.text = CSVR_GameSetting.accountLevel.ToString();
		level_Icon.sprite = UISpriteManager.instance.GetLevelSprite(CSVR_GameSetting.accountLevel);
	}

	void OnEnable(){
		
		playerName.text = AccountManager.instance.displayName;
		playerLevel.text = CSVR_GameSetting.accountLevel.ToString();
		//playerID.text = "<b>Mã giới thiệu:</b> " + ID;

		int nextLevelExp = CSVR_GameSetting.experienceForEveryLevel[CSVR_GameSetting.accountLevel+1];
		float exxp = (float)System.Math.Round((float)CSVR_GameSetting.accountExp / nextLevelExp, 3);
        if (exxp <= 0)
        {
            playerExp.text = "0 %";
        }
        else
        {
            playerExp.text = (exxp * 100) + " %";
        }
		playerCurrentLevelText.text = CSVR_GameSetting.experienceForEveryLevelName[CSVR_GameSetting.accountLevel];
		playerCurrentLevelIcon.sprite = Resources.Load<Sprite>("Sprites/UILevel/"+CSVR_GameSetting.accountLevel);

		playerNextLevelText.text = CSVR_GameSetting.experienceForEveryLevelName[CSVR_GameSetting.accountLevel+1];
		playerNextLevel.sprite = Resources.Load<Sprite>("Sprites/UILevel/"+(CSVR_GameSetting.accountLevel+1));

		playerLevelFill.fillAmount = exxp;

		playerKill.text = CSVR_GameSetting.accountKill.ToString();
		playerDeath.text = CSVR_GameSetting.accountDead.ToString();
		playerWin.text = CSVR_GameSetting.accountWin.ToString();
		playerLot.text = CSVR_GameSetting.accountLost.ToString();
		playerHeadShot.text = CSVR_GameSetting.accountHeadShot.ToString();
	}
}
