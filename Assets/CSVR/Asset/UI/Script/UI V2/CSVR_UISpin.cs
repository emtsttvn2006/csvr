﻿using UnityEngine;
using System.Collections;
using Horus;
using Horus.ClientModels;
using UnityEngine.UI;
using UnityEngine.Events;

public class CSVR_UISpin : SingletonBaseScreen<CSVR_UISpin>
{
	[SerializeField] private Toggle Tog_BundleOne;
	//[SerializeField] private Toggle Tog_BundleTwo;
	[SerializeField] private Toggle Tog_BundleThree;
	//[SerializeField] private Toggle Tog_BundleFour;
	[SerializeField] private  CSVR_UISpinChild ChildBundle;	
	[SerializeField] private  GetCatalogItemsResult Result;
    
    public  void Start()
    {
        HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = GameConstants.VNITEMCATALOG_BundleSpin
			}, 
			(result) => {
                this.Result = result;
				Button_Bundle_One_Click();
               // ButtonBundleOne.onClick.Invoke();
            },
			(error) => {
				ErrorView.instance.ShowPopupError ("Quay số không thành công");
			}
		);
		Tog_BundleOne.onValueChanged.AddListener ((value) => {
			if (value) {
				Button_Bundle_One_Click ();
			}
		});
//		Tog_BundleTwo.onValueChanged.AddListener ((value) => {
//			if (value) {
//				Button_Bundle_Two_Click ();
//			}
//		});
		Tog_BundleThree.onValueChanged.AddListener ((value) => {
			if (value) {
				Button_Bundle_Three_Click ();
			}
		});
//		Tog_BundleFour.onValueChanged.AddListener ((value) => {
//			if (value) {
//				Button_Bundle_Four_Click ();
//			}
//		});
	}
	public override void OnEnable()
	{
		base.OnEnable ();

		TopBar.instance.SetBackButton(new UnityAction(() =>{
			this.Close();
			MainView.instance.Open();
			CharacterView.instance.Open();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			this.Close();
			CharacterView.instance.Open();
			CSVR_UIShop.instance.Open();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));
	}
	private void Button_Bundle_One_Click()
    {
        ChildBundle.Show(this.Result.Catalog[0],this.Result.Catalog[1]);
    }

//	private void Button_Bundle_Two_Click()
//    {
//        ChildBundle.Show(this.Result.Catalog[1]);
//    }

	private void Button_Bundle_Three_Click()
    {
        ChildBundle.Show(this.Result.Catalog[2],this.Result.Catalog[3]);
    }

//	private void Button_Bundle_Four_Click()
//    {
//        ChildBundle.Show(this.Result.Catalog[3]);
//    }
}
