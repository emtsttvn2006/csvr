﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Json;

public class CSVR_UIItemAcc : MonoBehaviour {
	public string itemId;
	public string itemClass;
	[SerializeField] private Text item_Name;
	[SerializeField] private Image item_Icon;
	private CSVR_AccessoriInfo infoGun;

	Toggle acs;
	void Awake(){

		acs = this.gameObject.GetComponent<Toggle> ();

	}
	void OnDisable(){
		acs.isOn = false;
	}
	public void GetAccInfo(CatalogItem cataLog){
		itemId = cataLog.ItemId;
		infoGun = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(cataLog.CustomData);
		if(infoGun != null)
			itemClass = infoGun.accessory_type +".plugin";
		else
			itemClass = cataLog.ItemClass;
		
//		itemClass = cataLog.ItemClass;
		//itemClass = cataLog.CustomData

		item_Name.text = cataLog.DisplayName;
		item_Icon.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/" + cataLog.ItemId);
		item_Icon.SetNativeSize ();
		item_Icon.transform.localScale = new Vector3 (0.4f, 0.4f, 0.4f);

		string moneyUse = "vàng";
		if (cataLog.VirtualCurrencyPrices.ContainsKey ("coin")) {
			moneyUse = cataLog.VirtualCurrencyPrices["coin"].ToString() + " bạc";

		} else {
			moneyUse = cataLog.VirtualCurrencyPrices["gold"].ToString() +  " vàng";
		}
			

		acs.onValueChanged.RemoveAllListeners ();
		acs.onValueChanged.AddListener((value) =>{
			if(value)
			{
				GetAccModel(cataLog.ItemClass);
				GetAccInfoInModel(infoGun);
				CSVR_UIAccessories.instance.SetButtonEvent(new UnityAction(() =>{
					OnPurchaseAccessories(cataLog);
				}),moneyUse,"Mua phụ kiện","");   

			}
		});
	}

	public void GetAccInfo(ItemInstance cataLog){
		//Debug.Log ("<b>0. cataLog.pluginTo </b>" +  cataLog.pluginTo);
		itemId = cataLog.ItemId;

		infoGun = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(cataLog.CustomData);
		if(infoGun != null)
			itemClass = infoGun.accessory_type +".plugin";
		else
			itemClass = cataLog.ItemClass;

		//Debug.Log ("<b>0. itemClass </b>" +  itemClass);
		item_Name.text = cataLog.DisplayName;
		item_Icon.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/" + cataLog.ItemId);
		item_Icon.SetNativeSize ();
		item_Icon.transform.localScale = new Vector3 (0.4f, 0.4f, 0.4f);


		acs.onValueChanged.RemoveAllListeners ();
		acs.onValueChanged.AddListener ((value) => {
			if (value) {
				string _idAc = "";
				switch (itemClass) {
				case GameConstants.VNITEMCLASS_MuzzleAccessories:
					try{
						_idAc = CSVR_UIAccessories.instance.objWeaponId.accessories.muzzle ["objId"];
					}catch{}
					break;
				case GameConstants.VNITEMCLASS_GripAccessories:
					try{
						_idAc = CSVR_UIAccessories.instance.objWeaponId.accessories.grip ["objId"];
					}catch{}
					break;
				case GameConstants.VNITEMCLASS_MagazineAccessories:
					try{
						_idAc = CSVR_UIAccessories.instance.objWeaponId.accessories.magazine ["objId"];
					}catch{}
					break;
				case GameConstants.VNITEMCLASS_ScopeAccessories:
					try{
						_idAc = CSVR_UIAccessories.instance.objWeaponId.accessories.scope ["objId"];
					}catch{}
					break;
				case GameConstants.VNITEMCLASS_SkinAccessories:
					try{
						_idAc = CSVR_UIAccessories.instance.objWeaponId.accessories.skin ["objId"];
					}catch{}
					break;
				case GameConstants.VNITEMCLASS_AccAccessories:
					try{
						_idAc = CSVR_UIAccessories.instance.objWeaponId.accessories.acc ["objId"];
					}catch{}
					break;
				}
				GetAccModel (cataLog.ItemClass);
				GetAccInfoInModel(infoGun);
				//Debug.Log (_idAc +"/"+ cataLog._id);
				if (_idAc != cataLog._id) {
					if (!string.IsNullOrEmpty(cataLog.pluginTo) && cataLog.pluginTo != CSVR_UIAccessories.instance.objWeaponId._id) {
						CSVR_UIAccessories.instance.SetButtonEvent (new UnityAction (() => {
							OnUnEquipPlugin (cataLog);
						}), "Gỡ xuống", "Trang bị phụ kiện", "Chú ý: Phụ kiện đang được trang bị ở súng khác...");
					}else{
						CSVR_UIAccessories.instance.SetButtonEvent (new UnityAction (() => {
							OnEquipPlugin (cataLog);
						}), "Trang bị", "Trang bị phụ kiện", "");
					}
				} else{
					CSVR_UIAccessories.instance.SetButtonEvent (new UnityAction (() => {
						OnUnEquipPlugin (cataLog);
					}), "Gỡ xuống", "Gỡ phụ kiện", "");

				}
			}
		});
	
	}

	private void GetAccModel(string classDefault){
		GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + itemId) as GameObject;
		GameObject _obj = Instantiate( foundAccessories);
		if (_obj == null)
			return;

		switch (itemClass) {
		case GameConstants.VNITEMCLASS_MuzzleAccessories:
			_obj.transform.SetParent(CSVR_UIAccessories.instance.accessoriesWeapon[3]);

			if(classDefault == GameConstants.VNITEMCLASS_MuzzleAccessories)
				CSVR_UIAccessories.instance.accessoriesWeapon[3].GetChild(0).gameObject.SetActive(false);
			else
				CSVR_UIAccessories.instance.accessoriesWeapon[3].GetChild(0).gameObject.SetActive(true);
			
			try{
				if(CSVR_UIAccessories.instance.accessoriesWeapon[3].childCount > 2)
					Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[3].GetChild(1).gameObject);
			}catch{
				
			}
			break;
		case GameConstants.VNITEMCLASS_GripAccessories:
			_obj.transform.SetParent(CSVR_UIAccessories.instance.accessoriesWeapon[2]);
			CSVR_UIAccessories.instance.accessoriesWeapon[2].GetChild(0).gameObject.SetActive(false);
			try{
				if(CSVR_UIAccessories.instance.accessoriesWeapon[2].childCount > 2)
					Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[2].GetChild(1).gameObject);
			}catch{

			}
			break;
		case GameConstants.VNITEMCLASS_MagazineAccessories:
			_obj.transform.SetParent(CSVR_UIAccessories.instance.accessoriesWeapon[1]);
			CSVR_UIAccessories.instance.accessoriesWeapon[1].GetChild(0).gameObject.SetActive(false);
			try{
				if(CSVR_UIAccessories.instance.accessoriesWeapon[1].childCount > 2)
					Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[1].GetChild(1).gameObject);
			}catch{

			}
			break;
		case GameConstants.VNITEMCLASS_ScopeAccessories:
			_obj.transform.SetParent(CSVR_UIAccessories.instance.accessoriesWeapon[0]);
			CSVR_UIAccessories.instance.accessoriesWeapon[0].GetChild(0).gameObject.SetActive(false);
			try{
				if(CSVR_UIAccessories.instance.accessoriesWeapon[0].childCount > 2)
					Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[0].GetChild(1).gameObject);
			}catch{

			}
			break;
		}
		_obj.transform.localPosition = Vector3.zero;
		_obj.transform.localRotation = Quaternion.identity;
		_obj.transform.localScale = Vector3.one;
		_obj.transform.gameObject.SetActive(true);
	}

	private void GetAccInfoInModel(CSVR_AccessoriInfo infoGun){
		CSVR_AccessoriInfo current_InfoGun;
		CSVR_AccessoriInfo current_InfoWeapon;
		switch (itemClass) {
		case GameConstants.VNITEMCLASS_MuzzleAccessories:
			
			current_InfoGun = CSVR_UIAccessories.instance.muzzle_AccessoryInfo;
			//Debug.Log ("infoGun.G1 " + infoGun.G1.value);
			//Debug.Log ("current_InfoGun.G1 " + current_InfoGun.G1.value);
			break;
		case GameConstants.VNITEMCLASS_GripAccessories:
			current_InfoGun = CSVR_UIAccessories.instance.grip_AccessoryInfo;
			break;
		case GameConstants.VNITEMCLASS_MagazineAccessories:
			current_InfoGun = CSVR_UIAccessories.instance.magazine_AccessoryInfo;
			break;
		case GameConstants.VNITEMCLASS_ScopeAccessories:
			current_InfoGun = CSVR_UIAccessories.instance.scope_AccessoryInfo;
			break;
		default:
			current_InfoGun = null;
			break;
		}
		current_InfoWeapon = CSVR_UIAccessories.instance.objWeapon_AccessoryInfo;

		//dame
		if (infoGun.G1 != null) {
			//Debug.Log ("current_InfoGun.G1 " + current_InfoGun.G1);
			if (current_InfoGun == null || current_InfoGun.G1 == null) {
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].color = (infoGun.G1.action.Contains ("+")) ? Color.green : Color.red;
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].text = infoGun.G1.action + " " + infoGun.G1.value;
			} else {
				int g1Value = -int.Parse ((current_InfoGun.G1.action + current_InfoGun.G1.value)) + int.Parse ((infoGun.G1.action + infoGun.G1.value));
				if (g1Value >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].text = "+ " + g1Value;
				} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].text = "- " + System.Math.Abs (g1Value);
				}
			}
		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [0].text = string.Empty;
		}

		//accuracy
		if (infoGun.A1 != null && infoGun.A2 != null) {
			float current_InfoGunA1 = 0,current_InfoGunA2 = 0;
			if (current_InfoGun != null && current_InfoGun.A1 != null && current_InfoGun.A2 != null) {
				current_InfoGunA1 = float.Parse (current_InfoGun.A1.action+current_InfoGun.A1.value);
				current_InfoGunA2 = float.Parse (current_InfoGun.A2.action+current_InfoGun.A2.value);
			}
			float m_minNew = float.Parse(current_InfoWeapon.A1.value) + float.Parse (infoGun.A1.action+infoGun.A1.value) - current_InfoGunA1;
			float m_maxNew = float.Parse(current_InfoWeapon.A2.value) + float.Parse (infoGun.A2.action+infoGun.A2.value) - current_InfoGunA2;
			float m_ACNew = (10 * (10 - (5 * m_minNew + m_maxNew) / 6));


			float m_ACDefault = int.Parse(CSVR_UIAccessories.instance.info_Bar_Value [1].text) ;
			int  m_ACDetal = (int)(m_ACNew - m_ACDefault);
			if (m_ACDetal >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [1].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [1].text = "+ " +m_ACDetal;
			} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [1].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [1].text = "- " +System.Math.Abs (m_ACDetal);
			}
		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [1].text = string.Empty;
		}



		//ap
		if (infoGun.G6 != null) {
			if (current_InfoGun == null || current_InfoGun.G6 == null) {
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].color = (infoGun.G6.action.Contains ("+")) ? Color.green : Color.red;
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].text = infoGun.G6.action + " " + infoGun.G6.value;
			} else {
				int g6Value = -int.Parse ((current_InfoGun.G6.action + current_InfoGun.G6.value)) + int.Parse ((infoGun.G6.action + infoGun.G6.value));
				if (g6Value >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].text = "+ " + g6Value;
				} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].text = "- " + System.Math.Abs (g6Value);
				}
			}

		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [7].text = string.Empty;
		}

		//recoil
		if (infoGun.R1 != null && infoGun.R2X != null) {

			float m_R1New = Mathf.Abs (float.Parse (infoGun.R1.value));
			float m_R2XNew = Mathf.Abs (float.Parse (infoGun.R2X.value));
			int m_RCNew = (int)((1 - m_R1New * m_R2XNew / 5) * 100);


			if (current_InfoGun == null || (current_InfoGun.R1.value == null && current_InfoGun.R1.value == null)) {
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [2].text = m_RCNew.ToString ();

			} else {
				float m_R1Current = Mathf.Abs (float.Parse (current_InfoGun.R1.value));
				float m_R2XCurrent = Mathf.Abs (float.Parse (current_InfoGun.R2X.value));
				int m_RCCurrent = (int)((1 - m_R1Current * m_R2XCurrent / 5) * 100);

				int m_RCDetel = -m_RCCurrent + m_RCNew;

				if (m_RCDetel >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [2].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [2].text = "+ " + m_RCDetel;
				} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [2].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [2].text = "- " + System.Math.Abs (m_RCDetel);
				}
			}
		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [2].text = string.Empty;
		}

		//reload
		if (infoGun.G3 != null) {
			if (current_InfoGun == null || current_InfoGun.G3 == null) {
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].color = (infoGun.G3.action.Contains ("+")) ? Color.green : Color.red;
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].text = infoGun.G3.action + " " + infoGun.G3.value;
			} else {
				int g3Value = -int.Parse ((current_InfoGun.G3.action + current_InfoGun.G3.value)) + int.Parse ((infoGun.G3.action + infoGun.G3.value));
				if (g3Value >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].text = "+ " + g3Value;
				} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].text = "- " + System.Math.Abs (g3Value);
				}
			}

		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [3].text = string.Empty;
		}

		//rate of fire
		if (infoGun.G2 != null) {
			float m_ROFNew = (int)(60 / float.Parse (infoGun.G2.value));
			if (current_InfoGun == null || current_InfoGun.G2 == null) {
				
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].color = (infoGun.G2.action.Contains ("+")) ? Color.green : Color.red;
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].text = infoGun.G2.action + " " + m_ROFNew;
			} else {
				float m_ROFTextValue = +m_ROFNew - (int)(60 / float.Parse (current_InfoGun.G2.value));
				if (m_ROFTextValue >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].text = "+ " + m_ROFTextValue;
				} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].text = "- " + System.Math.Abs (m_ROFTextValue);
				}
			}
//			float m_ROFText = (int)(60 / float.Parse (infoGun.G2.value));
//			CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].color = (infoGun.G2.action.Contains ("+")) ? Color.green : Color.red;
//			CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].text = infoGun.G2.action + " " +  m_ROFText;
		}else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [4].text = string.Empty;
		}



		//capacity
		if (infoGun.G4 != null) {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [5].color = Color.yellow;
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [5].text =  infoGun.G4.value;
		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [5].text = string.Empty;
		}


		//weight
		if (infoGun.G5 != null) {
			if (current_InfoGun == null || current_InfoGun.G5 == null) {
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].color = (infoGun.G5.action.Contains ("+")) ? Color.green : Color.red;
				CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].text = infoGun.G5.action + " " + infoGun.G5.value;
			} else {
				float g5Value = -float.Parse ((current_InfoGun.G5.action + current_InfoGun.G5.value)) + float.Parse ((infoGun.G5.action + infoGun.G5.value));
				if (g5Value >= 0) {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].color = Color.green;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].text = "+ " + g5Value;
				} else {
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].color = Color.red;
					CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].text = "- " + System.Math.Abs (g5Value);
				}
			}
		} else {
			CSVR_UIAccessories.instance.ac_Info_Bar_Value [6].text = string.Empty;
		}
	}


	void OnPurchaseAccessories(CatalogItem item) {

		string moneyUse = "coin";
		if (item.VirtualCurrencyPrices.ContainsKey ("coin")) {
			moneyUse = "coin";
			AccountManager.instance.gameCoinAmount -= int.Parse(item.VirtualCurrencyPrices["coin"].ToString());

		} else {
			moneyUse = "gold";
			AccountManager.instance.gameDollarAmount -= int.Parse(item.VirtualCurrencyPrices["gold"].ToString());
		}

		PurchaseItemRequest request = new PurchaseItemRequest () {
			Purchases = new List<ItemPurchase> () {
				new ItemPurchase () {
					ItemId = item.ItemId,
					moneyUse = moneyUse,
					RemainingUses = 1,
					UseOnBuy = 0
				}
			}
		};

		HorusClientAPI.PurchaseItem(request, 
			(result) => {
				for (int i = 0; i < result.ItemPurchase.Count; i++) {
					AccountManager.instance.itemsNew.Add (result.ItemPurchase [i]);
					AccountManager.instance.inventory.Add (result.ItemPurchase [i]);

					CSVR_UIInventory.instance.InstanceItemPurchase (result.ItemPurchase [i]);
					CSVR_UIInventory.instance.ShowNotificationPurchase(result.ItemPurchase[i],true);

					CSVR_UIAccessories.instance.ResetAccList();
					CSVR_UIAccessories.instance.ResetToggleList();

//					CSVR_UIAccessories.instance.SetButtonEvent (new UnityAction (() => {
//						OnEquipPlugin (result.ItemPurchase [i]);
//					}), "Trang bị", "Trang bị phụ kiện", "");

				}
				TopBar.instance.UpdateVirtualCurrency();
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Mua trang bị thất bại");
			}
		);
	}

	void OnEquipPlugin(ItemInstance item) {
		//Debug.Log ("1."+ JsonWrapper.SerializeObject (CSVR_UIAccessories.instance.objWeaponId, Horus.Internal.HorusUtil.ApiSerializerStrategy));
		PluginToItemRequest request = new PluginToItemRequest () {
			objId = CSVR_UIAccessories.instance.objWeaponId._id,
			pluginObjId = item._id
		};

		HorusClientAPI.PluginTo(request,
			(result) => {
				if(result.Status.Contains("PluginSuccess")){


					UpdateInventory(result.ItemPlugin);
					UpdateCacheItemsInfo(result.ItemPlugin);

					item.pluginTo = CSVR_UIAccessories.instance.objWeaponId._id;

					CSVR_UIAccessories.instance.ResetInfoView();
					CSVR_UIAccessories.instance.objWeaponId = result.ItemPlugin;
					CSVR_UIAccessories.instance.UpdateAccInfo(result.ItemPlugin);
					if (!string.IsNullOrEmpty ( result.ItemPlugin.EquipGroup)) {
						CharacterView.instance.GetDefaultItem();
					}

					CSVR_UIAccessories.instance.SetButtonEvent (new UnityAction (() => {
						GetAccModel (item.ItemClass);
						OnUnEquipPlugin (item);
					}), "Gỡ xuống", "Gỡ phụ kiện", "");
				}
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Trang bị phụ kiện thất bại");
			}
		);
	}
	void OnUnEquipPlugin(ItemInstance item) {
		//Debug.Log ("1."+ JsonWrapper.SerializeObject (item, Horus.Internal.HorusUtil.ApiSerializerStrategy));
		PluginToItemRequest request = new PluginToItemRequest () {
			objId = "0",
			pluginObjId = item._id
		};

		HorusClientAPI.PluginTo(request,
			(result) => {
				if(result.Status.Contains("UnPlugSuccess")){
					item.pluginTo = string.Empty;
					UpdateInventory(result.ItemPlugin);
					UpdateCacheItemsInfo(result.ItemPlugin);

					CSVR_UIAccessories.instance.ResetInfoView();
					if(CSVR_UIAccessories.instance.objWeaponId._id == result.ItemPlugin._id){
						CSVR_UIAccessories.instance.objWeaponId = result.ItemPlugin;
					}
					if (!string.IsNullOrEmpty ( result.ItemPlugin.EquipGroup)) {
						CharacterView.instance.GetDefaultItem();
					}
					CSVR_UIAccessories.instance.SetButtonEvent (new UnityAction (() => {
						GetAccModel (item.ItemClass);

						OnEquipPlugin (item);
					}), "Trang bị", "Trang bị phụ kiện", "");
					CSVR_UIAccessories.instance.UpdateAccInfo(result.ItemPlugin);
					CharacterView.instance.UpdateIDGunDefault(result.ItemPlugin);
					if(CSVR_UIAccessories.instance.objWeaponId._id == result.ItemPlugin._id){
						//Debug.Log ("<b>1. itemClass </b>" +  itemClass);
						switch (itemClass) {
						case GameConstants.VNITEMCLASS_MuzzleAccessories:
							
							CSVR_UIAccessories.instance.accessoriesWeapon[3].GetChild(0).gameObject.SetActive(true);
							try{
								if(CSVR_UIAccessories.instance.accessoriesWeapon[3].childCount > 1)
									Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[3].GetChild(1).gameObject);
							}catch{

							}
							break;
						case GameConstants.VNITEMCLASS_GripAccessories:

							CSVR_UIAccessories.instance.accessoriesWeapon[2].GetChild(0).gameObject.SetActive(true);
							try{
								if(CSVR_UIAccessories.instance.accessoriesWeapon[2].childCount > 1)
									Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[2].GetChild(1).gameObject);
							}catch{

							}
							break;
						case GameConstants.VNITEMCLASS_MagazineAccessories:

							CSVR_UIAccessories.instance.accessoriesWeapon[1].GetChild(0).gameObject.SetActive(true);
							try{
								if(CSVR_UIAccessories.instance.accessoriesWeapon[1].childCount > 1)
									Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[1].GetChild(1).gameObject);
							}catch{

							}
							break;
						case GameConstants.VNITEMCLASS_ScopeAccessories:

							CSVR_UIAccessories.instance.accessoriesWeapon[0].GetChild(0).gameObject.SetActive(true);
							try{
								if(CSVR_UIAccessories.instance.accessoriesWeapon[0].childCount > 1)
									Destroy(CSVR_UIAccessories.instance.accessoriesWeapon[0].GetChild(1).gameObject);
							}catch{

							}
							break;
						}
					}
				}
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Trang bị phụ kiện thất bại");
			}
		);
	}

	private void UpdateInventory(ItemInstance item){
		List<ItemInstance> listItem = AccountManager.instance.inventory;
		int count = listItem.Count;
		for (int i = 0; i < count; i++) {
			if (listItem [i]._id == item._id) {
				listItem [i] = item;
				//Debug.Log ("AccountManager.instance.inventory" + JsonWrapper.SerializeObject (AccountManager.instance.inventory [i], Horus.Internal.HorusUtil.ApiSerializerStrategy));
				break;
			}
		}
	}

	private void UpdateCacheItemsInfo(ItemInstance item){
		List<CSVR_UIItemInv> listItem = CSVR_UIInventory.instance.cacheItemsInfo;
		int count = listItem.Count;
		for (int i = 0; i < count; i++) {
			if (listItem [i].ItemInstanceId == item._id) {
				listItem [i].infoTemp = item;
				listItem [i].UpdateSaoPhuKien ();
				//Debug.Log ("AccountManager.instance.inventory" + JsonWrapper.SerializeObject (listItem [i].infoTemp, Horus.Internal.HorusUtil.ApiSerializerStrategy));
				break;
			}
		}
	}
}
