using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using DG.Tweening;

public class CSVR_UIItemInv : MonoBehaviour {

	public string ItemInstanceId;
	public string ItemClass;
	public Text NameText = null;
	public Image IconImage = null;
	public Text DayText = null;
	public Text RequireText = null;
	public Image BaloImage = null;
	public Text BaloText = null;
	public Button DetailButton = null;
	public Button EquipButton = null;
	public Button UnEquipButton = null;
	public GameObject AcsGameOjbect = null; 
	public GameObject LevelGameOjbect = null; 

	public CSVR_UIItemDetailInv ItemDetail;	

	int level = 0;
	int typeWeapon = 0;
	public ItemInstance infoTemp;
	CatalogItem item;

	//public List<CSVR_UIItemBagInv> listBag = new List<CSVR_UIItemBagInv>();


	public void GetInfo(ItemInstance info)
	{
		infoTemp = info;
		ItemInstanceId = info._id;
		ItemClass = info.ItemClass;
		this.gameObject.name = info.DisplayName;
		//NameText
		NameText.text = info.DisplayName;
		//IconImage
		//Debug.Log ("info" + info.ItemIdModel);
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [info.ItemId];

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+info.ItemIdModel);

		//DayText
		if (info.CatalogVersion == GameConstants.VNITEMCATALOG_Item || info.CatalogVersion == GameConstants.VNITEMCATALOG_Accessories || info.CatalogVersion == GameConstants.VNITEMCATALOG_Upgrade ) {
			DayText.text = "Vĩnh Viễn";
		} else if (info.CatalogVersion == GameConstants.VNITEMCATALOG_ItemPeriod) {
			System.TimeSpan day = (System.TimeSpan)(System.DateTime.Parse(info.Expiration) - System.DateTime.Parse(CSVR_GameSetting.LastLogin));
			if (day.Days != 0) {
				DayText.text = day.Days + " Ngày";
			} else {
				DayText.text = day.Hours + " Giờ";
			}
		}
		UpdateSaoPhuKien ();
		if(info.ItemId.Contains("_up")){
			level = int.Parse (info.ItemId.Substring (info.ItemId.Length - 1));
			for(int s = 0; s < 5; s++){
				if(s  < level)
					LevelGameOjbect.transform.GetChild(s).gameObject.SetActive(true);
			}
		}


		DetailButton.onClick.RemoveAllListeners ();
		switch (info.ItemClass) {
			case GameConstants.VNITEMCLASS_DefaultCharacter:
			{
//				if (level == 0) {
//					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopAccessories,info.ItemIdModel);
//				}else{
//					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
//				}
//				GetInfoRequire (item);
				EquipButton.onClick.AddListener(() =>{
					UI_EquipCharacterButton_Click();
					CharacterView.instance.SwitchCharacter(info.ItemIdModel);
					CharacterView.instance.GetIDGunInventory(); 
				});
				UnEquipButton.onClick.AddListener(() =>{
					UI_UnEquipCharacterButton_Click();
					CharacterView.instance.SwitchCharacter("11111_999_Swat");
					CharacterView.instance.GetIDGunInventory(); 
				});
				DetailButton.onClick.AddListener(() =>{
					ItemDetail.GetInfoCharacter(item,infoTemp);
				});
				break;
			}
			case GameConstants.VNITEMCLASS_MuzzleAccessories:
			case GameConstants.VNITEMCLASS_GripAccessories:
			case GameConstants.VNITEMCLASS_MagazineAccessories:
			case GameConstants.VNITEMCLASS_ScopeAccessories:
			case GameConstants.VNITEMCLASS_SkinAccessories:
			case GameConstants.VNITEMCLASS_AccAccessories:
			{
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopAccessories,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}

				DetailButton.onClick.AddListener(() =>{
					ItemDetail.GetInfoAccessories(item,infoTemp);
				});
				break;

			}
			case GameConstants.VNITEMCLASS_RifleWeapon:
			case GameConstants.VNITEMCLASS_SniperWeapon:
			case GameConstants.VNITEMCLASS_ShotGunWeapon:
			case GameConstants.VNITEMCLASS_SMGWeapon:
			case GameConstants.VNITEMCLASS_MachineWeapon:
			{
				typeWeapon = 1;
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopMainGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});

				DetailButton.onClick.AddListener(() => {
					ItemDetail.GetInfoGun(item,infoTemp); 
					CharacterView.instance.GetIDGunInventory(infoTemp); 
	            });
				break;
			}
			case GameConstants.VNITEMCLASS_PistolWeapon:
			{
				typeWeapon = 2;
				if (level == 0) {
						item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopPistolGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});

				DetailButton.onClick.AddListener(() =>{ 
					ItemDetail.GetInfoGun(item,infoTemp);
	                CharacterView.instance.GetIDGunName(info.ItemIdModel);
	            });
				break;
			}
			case GameConstants.VNITEMCLASS_MeleeWeapon:
			{
				typeWeapon = 3;
				if (level == 0) {
						item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopMeleGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});
				DetailButton.onClick.AddListener(() =>{ 
					ItemDetail.GetInfoMelee(item,infoTemp);
	                CharacterView.instance.GetIDGunName(info.ItemIdModel);
	            });
				break;
			}
			case GameConstants.VNITEMCLASS_GrenadeWeapon:
			{
				typeWeapon = 4;
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopGrenadeGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});
				DetailButton.onClick.AddListener(() =>{ 
					ItemDetail.GetInfoGrenade(item,infoTemp);
	                CharacterView.instance.GetIDGunName(info.ItemIdModel);
	            });
				break;
			}
		case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
			{
				typeWeapon = 5;
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopGrenadeGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});

				DetailButton.onClick.AddListener(() =>{ 
					ItemDetail.GetInfoGrenade(item,infoTemp);
					CharacterView.instance.GetIDGunName(info.ItemIdModel);
				});
				break;
			}
		case GameConstants.VNITEMCLASS_TwoGrenadeWeapon:
			{
				typeWeapon = 6;
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopGrenadeGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});

				DetailButton.onClick.AddListener(() =>{ 
					ItemDetail.GetInfoGrenade(item,infoTemp);
					CharacterView.instance.GetIDGunName(info.ItemIdModel);
				});
				break;
			}
			case GameConstants.VNITEMCLASS_HelmetWeapon:	
			{
				typeWeapon = 7;
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopArmorGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});
				DetailButton.onClick.AddListener(() =>{
					ItemDetail.GetInfoArmor(item,infoTemp);
				});
				break;
			}
			case GameConstants.VNITEMCLASS_ArmorWeapon:
			{
				typeWeapon = 8;
				if (level == 0) {
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopArmorGun,info.ItemIdModel);
				}else{
					item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,info.ItemId);
				}
				GetInfoRequire (item);
				EquipButton.onClick.AddListener (() => {
					UI_EquipButton_Click ();
				});
				UnEquipButton.onClick.AddListener (() => {
					UI_UnEquipButton_Click ();
				});
				DetailButton.onClick.AddListener(() =>{
					ItemDetail.GetInfoArmor(item,infoTemp);
	            });
				break;
			}
		}


	}
	public void UpdateSaoPhuKien(){
		if (infoTemp.accessories != null) {
			AcsGameOjbect.gameObject.SetActive (true);
		} else {
			AcsGameOjbect.gameObject.SetActive (false);
			return;
		}
		//magazine
		try{
			string _idAcModel = infoTemp.accessories.muzzle["pluginModelId"];
			if (_idAcModel == null)
				return;
			AcsGameOjbect.transform.GetChild(1).gameObject.SetActive(true);
		}catch{
			AcsGameOjbect.transform.GetChild(1).gameObject.SetActive(false);
		}
		//scope
		try{
			string _idAcModel = infoTemp.accessories.scope["pluginModelId"];
			if (_idAcModel == null)
				return;
			AcsGameOjbect.transform.GetChild(2).gameObject.SetActive(true);
		}catch{
			AcsGameOjbect.transform.GetChild(2).gameObject.SetActive(false);
		}

		//magazine
		try{
			string _idAcModel = infoTemp.accessories.magazine["pluginModelId"];
			if (_idAcModel == null)
				return;
			AcsGameOjbect.transform.GetChild(3).gameObject.SetActive(true);
		}catch{
			AcsGameOjbect.transform.GetChild(3).gameObject.SetActive(false);
		}

		//grip
		try{
			string _idAcModel = infoTemp.accessories.grip["pluginModelId"];
			if (_idAcModel == null)
				return;
			AcsGameOjbect.transform.GetChild(4).gameObject.SetActive(true);
		}catch{
			AcsGameOjbect.transform.GetChild(4).gameObject.SetActive(false);
		}

	}
    void OpenDetailPanel()
    {
        //switch pnl
        CSVR_UIInventory.instance.CloseAnAnimator(CSVR_UIInventory.instance.rightPanel);
        CSVR_UIInventory.instance.OpenAnAnimator(CSVR_UIInventory.instance.rightDetailPanel);
    }

	void ContainsLoop(List<ItemInstance> list, ItemInstance value)
	{
		for (int i = 0; i < list.Count; i++) {
			if (list [i]._id == value._id) {
				list.Remove (list [i]);
				break;
			}
		}

	}

	public void GetInfoPurchase(ItemInstance item){
		DetailButton.onClick.AddListener(() =>{

			ContainsLoop(AccountManager.instance.itemsNew,item);

			CSVR_UIInventory.instance.ShowNotificationPurchase(item,false);
		});
	}

	public void GetInfoEquipItems(ItemInstance item){
		int typeWeapon = 0;
		int group = 0;
        
		BaloImage.gameObject.SetActive (false);
		EquipButton.gameObject.SetActive (true);
		UnEquipButton.gameObject.SetActive (false);

		if (!string.IsNullOrEmpty (item.EquipGroup)) {
			for (int i = 1; i < 3; i++) {
				string[] equip = item.EquipGroup.Split ("." [0]);

				if (equip [0] == i.ToString ()) {
					typeWeapon = int.Parse (equip [1]);

					if (typeWeapon != 0) {
						CSVR_UIInventory.instance.listGroup [i - 1].Equip (infoTemp,typeWeapon, Resources.Load<Sprite> ("Sprites/UIWeapon/" + item.ItemIdModel));
						BaloText.text = i.ToString ();
						group = i;
					}

					BaloImage.gameObject.SetActive (true);
					EquipButton.gameObject.SetActive (false);
					UnEquipButton.gameObject.SetActive (true);
					break;
				}
			}
		}
//		Yo.Log ("2. vao day ",item._id);
//		Yo.Log ("2. vao day ",RequireText.text);
		if (!string.IsNullOrEmpty (RequireText.text)) {
			BaloImage.gameObject.SetActive (false);
			EquipButton.gameObject.SetActive (false);
			UnEquipButton.gameObject.SetActive (false);
		}
	}
	public void GetInfoEquipCharacter(ItemInstance item){

		BaloImage.gameObject.SetActive (false);
		EquipButton.gameObject.SetActive (true);
		UnEquipButton.gameObject.SetActive (false);

		if (!string.IsNullOrEmpty (item.EquipGroup) && item.EquipGroup == GameConstants.VNITEMCHARACTER_Equip) {

			EquipButton.gameObject.SetActive (false);
			UnEquipButton.gameObject.SetActive (true);
		}
	}
	public void UnEquipMoreItem(){
		BaloImage.gameObject.SetActive (false);
		EquipButton.gameObject.SetActive (true);
		UnEquipButton.gameObject.SetActive (false);

		//go item local
		AccountManager.instance.inventory.Find(p => p._id == ItemInstanceId).EquipGroup =  "0";
	}

	public void UI_StateEquipButtonClick(bool status){
		//if(item != null && item.required != null)
		EquipButton.gameObject.SetActive (status);
	}
	private void UI_EquipButton_Click(){

		CSVR_UIInventory.instance.EquipItemSuccess (infoTemp.ItemClass, CSVR_UIInventory.instance.Group);
		CSVR_UIInventory.instance.listGroup[CSVR_UIInventory.instance.Group - 1].Equip(infoTemp,typeWeapon, Resources.Load<Sprite>("Sprites/UIWeapon/"+infoTemp.ItemIdModel),true);
		OnEquipItem(infoTemp._id);
		BaloText.text = CSVR_UIInventory.instance.Group.ToString();


		infoTemp.EquipGroup =  CSVR_UIInventory.instance.Group+"."+typeWeapon;
		BaloImage.gameObject.SetActive(true);
		EquipButton.gameObject.SetActive (false);
		UnEquipButton.gameObject.SetActive (true);

		AccountManager.instance.inventory.Find(p => p._id == infoTemp._id).EquipGroup =  CSVR_UIInventory.instance.Group+"."+typeWeapon;
	}

	private void UI_UnEquipButton_Click(){

		int group = int.Parse (BaloText.text);
		CSVR_UIInventory.instance.listGroup[group-1].UnEquip(typeWeapon);
		OnUnEquipItem(infoTemp._id);
		infoTemp.EquipGroup = "0";
		AccountManager.instance.inventory.Find(p => p._id == infoTemp._id).EquipGroup =  "0";

		BaloImage.gameObject.SetActive(false);
		EquipButton.gameObject.SetActive (true);
		UnEquipButton.gameObject.SetActive (false);
	}

	//danh cho nhan vat
	private void UI_EquipCharacterButton_Click(){
		OnEquipItem(infoTemp._id,GameConstants.VNITEMCHARACTER_Equip);
		infoTemp.EquipGroup = GameConstants.VNITEMCHARACTER_Equip;
		CSVR_UIInventory.instance.EquipCharacterSuccess (infoTemp.ItemClass, CSVR_UIInventory.instance.Group);

		AccountManager.instance.inventory.Find(p => p._id == infoTemp._id).EquipGroup =  GameConstants.VNITEMCHARACTER_Equip;

		EquipButton.gameObject.SetActive (false);
		UnEquipButton.gameObject.SetActive (true);
	}

	private void UI_UnEquipCharacterButton_Click(){

		OnUnEquipItem(infoTemp._id);
		infoTemp.EquipGroup = "0";
		AccountManager.instance.inventory.Find(p => p._id == infoTemp._id).EquipGroup =  "0";

		EquipButton.gameObject.SetActive (true);
		UnEquipButton.gameObject.SetActive (false);
	}
	//end
	public void UnEquipSellSuccess(){
		try{
			int group = int.Parse (BaloText.text);
			CSVR_UIInventory.instance.listGroup[group-1].UnEquip(typeWeapon);
		}catch{

		}
	}

	public void GetInfoUpgrade(ItemInstance item){

		CatalogItem info;
		info = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,item.ItemId);

		switch (item.ItemClass) {
		case GameConstants.VNITEMCLASS_RifleWeapon:
		case GameConstants.VNITEMCLASS_SniperWeapon:
		case GameConstants.VNITEMCLASS_ShotGunWeapon:
		case GameConstants.VNITEMCLASS_SMGWeapon:
		case GameConstants.VNITEMCLASS_MachineWeapon:
		case GameConstants.VNITEMCLASS_PistolWeapon:
			ItemDetail.GetInfoGun(info,item);
				break;
		case GameConstants.VNITEMCLASS_MeleeWeapon:
			ItemDetail.GetInfoMelee(info,item);
				break;
		case GameConstants.VNITEMCLASS_GrenadeWeapon:
		case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
			ItemDetail.GetInfoGrenade(info,item);
				break;
		case GameConstants.VNITEMCLASS_HelmetWeapon:	
		case GameConstants.VNITEMCLASS_ArmorWeapon:
			ItemDetail.GetInfoArmor(info,item);
				break;
		case GameConstants.VNITEMCLASS_MuzzleAccessories:
		case GameConstants.VNITEMCLASS_GripAccessories:
		case GameConstants.VNITEMCLASS_MagazineAccessories:
		case GameConstants.VNITEMCLASS_ScopeAccessories:
		case GameConstants.VNITEMCLASS_SkinAccessories:
		case GameConstants.VNITEMCLASS_AccAccessories:
			ItemDetail.GetInfoAccessories(info,item);
			break;
		}
	}
	private void GetInfoRequire(CatalogItem item){
		if (item == null) {
			return;
		}
		if (item.required != null) {

			string require = "Yêu cầu ";
			int f = 0;
			if (item.required.level != null && item.required.level > CSVR_GameSetting.accountLevel) {
				require +=  "cấp " + item.required.level;
				f++;
			}
			if (item.required.role != null) {
				for (int i = 0; i < item.required.role.Length; i++) {
					int k = 0;
					for (int j = 0; j < AccountManager.instance.roles.Length; j++) {
						if(AccountManager.instance.roles[j].Contains(item.required.role[i])){
							k++;
						}
					}
					if (k == 0) {
						require += " " + item.required.role [i];
						f++;
					}
				}
			}
			require += " để trang bị";
			//require
			if (f == 0) {
				RequireText.text = string.Empty;
			} else {
				DOTween.To(()=> RequireText.transform.localPosition, x=> RequireText.transform.localPosition = x, new Vector3(-400, 0, 0), 15).SetRelative().SetLoops(-1, LoopType.Restart);
				RequireText.text = require;
			}
		}
	}

#region Equip And UnEquip with HorusSDK
	private void OnEquipItem(string itemID,string equipGroup = null) 
	{	
		
		EquipItemRequest request = new EquipItemRequest (){
			_id = itemID,
			EquipGroup = (!string.IsNullOrEmpty(equipGroup))? equipGroup : CSVR_UIInventory.instance.Group + "." + typeWeapon
		};

		HorusClientAPI.EquipItem(request, 
			(result) => {
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Trang bị thất bại");
			}
		);
	}

	private void OnUnEquipItem(string itemID) 
	{	
		EquipItemRequest request = new EquipItemRequest (){
			_id = itemID,
			EquipGroup = "0"
		};
				
		HorusClientAPI.EquipItem(request, 
			(result) => {
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Gỡ trang bị thất bại");
			}
		);
	}
#endregion
}

