﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Json;


public class CSVR_UIItemDetailInv : MonoBehaviour {
	public Text NameText = null;
	public Image IconImage = null;
	public GameObject Acs = null;
	public GameObject Levels = null;

	public Text DamageText = null;
	public Text CustomText = null;
	public Text AccuracyText = null;
	public Text CustomText2 = null;
	public Text APText = null;
	public Text RecoilText = null;
	public Text ReloadText = null;
	public Text ROFText = null;	
	public Text CapacityText = null;
	public Text WeightText = null;
	public Text LevelUpText = null;
	public Text RateUpText = null;

	public Button UpgradeButton = null;
	public Button SellButton = null;
	public Button AccessoriesButton = null;

	//public Animator right_Pnl;
	//public Animator rightDetail_Pnl;
	public GameObject info_grp;
	public Sprite[] coins;

	private int findPhuKien = 0;

	public void GetInfoGun(CatalogItem info,ItemInstance item)
	{
		AccessoriesButton.gameObject.SetActive (true);
		for (int i = 0; i < 8; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(true);
		}

		NameText.text = item.DisplayName;
		CustomText.text = "Chính xác";
		CustomText2.text = "Độ giật";

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		CSVR_GunInfo infoGun = HorusSimpleJson.DeserializeObject<CSVR_GunInfo>(info.CustomData);
		//dame
		DamageText.text = infoGun.G1.ToString();

		//accuracy
		float m_min = infoGun.A1;
		float m_max = infoGun.A2;
		int m_AC = (int)(10*(10-(5*m_min+m_max)/6));
		AccuracyText.text = m_AC.ToString();

		//ap
		APText.text = infoGun.G6.ToString();

		//recoil
		float m_R1 = Mathf.Abs(infoGun.R1);
		float m_R2X = Mathf.Abs(infoGun.R2X);
		int m_RC = (int)((1-m_R1*m_R2X/5)*100);
		RecoilText.text = m_RC.ToString();

		//reload
		ReloadText.text = infoGun.G3+ " s";

		//rate of fire
		float m_ROFText = (int)(60/float.Parse(infoGun.G2.ToString()));
		ROFText.text = m_ROFText+" RPM";

		//capacity
		CapacityText.text = infoGun.G4.ToString();

		//weight
		WeightText.text = infoGun.G5 +"Kg";

        //switch pnl
        CSVR_UIInventory.instance.UI_OnDetailOn();
		UpdateSaoPhuKien (item);
		SetUpButtonEvent (info, item);
	}

	public void GetInfoMelee(CatalogItem info,ItemInstance item)
	{
		AccessoriesButton.gameObject.SetActive (true);
		for (int i = 0; i < 8; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}
		info_grp.transform.GetChild(0).gameObject.SetActive(true);
		info_grp.transform.GetChild(1).gameObject.SetActive(true);
		info_grp.transform.GetChild(2).gameObject.SetActive(true);

		NameText.text = item.DisplayName;
		CustomText.text = "Khoảng cách";
		CustomText2.text = "Xuyên giáp";
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		CSVR_MeleInfo infoMelee = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo>(info.CustomData);
		//Damages
		DamageText.text = infoMelee.Damge.ToString();
		//Ranges
		AccuracyText.text = infoMelee.Ranges.ToString();

		//A.p
		RecoilText.text = infoMelee.AP.ToString();

        //switch pnl
        CSVR_UIInventory.instance.UI_OnDetailOn();
		UpdateSaoPhuKien (item);
        SetUpButtonEvent (info, item);
	}

	public void GetInfoGrenade(CatalogItem info,ItemInstance item)
	{
		AccessoriesButton.gameObject.SetActive (true);
		for (int i = 0; i < 8; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}
		info_grp.transform.GetChild(0).gameObject.SetActive(true);
		info_grp.transform.GetChild(1).gameObject.SetActive(true);
		info_grp.transform.GetChild(2).gameObject.SetActive(true);

		NameText.text = item.DisplayName;
		CustomText.text = "Khoảng cách";
		CustomText2.text = "Xuyên giáp";
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		CSVR_GrenadeInfo infoGrenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo>(info.CustomData);
		//Damages
		DamageText.text = infoGrenadeInfo.Damge.ToString();
		//Ranges

		AccuracyText.text = infoGrenadeInfo.Ranges.ToString();
		
		//A.p
		RecoilText.text = infoGrenadeInfo.AP.ToString();

        //switch pnl
        CSVR_UIInventory.instance.UI_OnDetailOn();
		UpdateSaoPhuKien (item);
        SetUpButtonEvent (info, item);
		
	}
	public void GetInfoArmor(CatalogItem info,ItemInstance item)
	{
		AccessoriesButton.gameObject.SetActive (true);
		for (int i = 0; i < 8; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}
		info_grp.transform.GetChild(1).gameObject.SetActive(true);
		info_grp.transform.GetChild(2).gameObject.SetActive(true);

		NameText.text = item.DisplayName;
		CustomText.text = "Độ bền";
		CustomText2.text = "Chống đỡ";

		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		CSVR_ArmorInfo infoArmorInfo = HorusSimpleJson.DeserializeObject<CSVR_ArmorInfo>(info.CustomData);
		//Reliability
		AccuracyText.text = infoArmorInfo.Reliability.ToString();	
		
		//Resilience
		RecoilText.text = infoArmorInfo.Resilience.ToString();

        //switch pnl
        CSVR_UIInventory.instance.UI_OnDetailOn();
		UpdateSaoPhuKien (item);
        SetUpButtonEvent (info, item);

	}


	public void GetInfoAccessories(CatalogItem info,ItemInstance item)
	{
		AccessoriesButton.gameObject.SetActive (false);
		for (int i = 0; i < 8; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}

		NameText.text = item.DisplayName;

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);

		CSVR_AccessoriInfo infoGun = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(info.CustomData);
		//dame
		if (infoGun.G1 != null) {
			
			info_grp.transform.GetChild(0).gameObject.SetActive(true);
			DamageText.text = infoGun.G1.action + " " + infoGun.G1.value;
		}
		//
		//accuracy
		if ( infoGun.A1 != null && infoGun.A2 != null) {
			CustomText.text = "Chính xác";
			info_grp.transform.GetChild(1).gameObject.SetActive(true);
			float m_min = float.Parse(infoGun.A1.value);
			float m_max =  float.Parse(infoGun.A2.value);
			int m_AC = (int)(100 - (10 * (10 - (5 * m_min + m_max) / 6)));
			if (m_AC > 0) {
				AccuracyText.text = "+ " + m_AC;
			} else {
				AccuracyText.text = "- " + System.Math.Abs(m_AC);
			}
		}
		//ap
		if (infoGun.G6 != null) {
			info_grp.transform.GetChild(7).gameObject.SetActive(true);
			APText.text = infoGun.G6.action + " " + infoGun.G6.value;
		}

		//recoil
		if (infoGun.R1 != null && infoGun.R2X != null) {
			CustomText2.text = "Độ giật";
			info_grp.transform.GetChild(2).gameObject.SetActive(true);
			float m_R1 = Mathf.Abs ( float.Parse(infoGun.R1.value));
			float m_R2X = Mathf.Abs ( float.Parse(infoGun.R2X.value));
			int m_RC = (int)(100 - ((1 - m_R1 * m_R2X / 5) * 100));
			if (m_RC > 0) {
				RecoilText.text = "+ " + m_RC;
			} else {
				RecoilText.text = "- " + System.Math.Abs(m_RC);
			}
		}
		//reload
		if (infoGun.G3 != null) {
			info_grp.transform.GetChild(3).gameObject.SetActive(true);
			ReloadText.text = infoGun.G3.action + " " + infoGun.G3.value + " s";
		}

		//rate of fire
		if (infoGun.G2 != null) {
			info_grp.transform.GetChild(4).gameObject.SetActive(true);
			float m_ROFText = (int)(60 / float.Parse (infoGun.G2.value));
			ROFText.text = infoGun.G2.action + " " +  m_ROFText + " RPM";
		}
		//capacity
		if (infoGun.G4 != null) {
			info_grp.transform.GetChild(5).gameObject.SetActive(true);
			CapacityText.text = infoGun.G4.value;
		}
		//weight
		if (infoGun.G5 != null) {
			info_grp.transform.GetChild (6).gameObject.SetActive (true);
			WeightText.text = infoGun.G5.action + " " +  infoGun.G5.value + "Kg";
		}
		//switch pnl
		CSVR_UIInventory.instance.UI_OnDetailOn();
		UpdateSaoPhuKien (item);
		SetUpButtonEvent (info, item);
	}
	public void GetInfoCharacter(CatalogItem info,ItemInstance item)
	{
		AccessoriesButton.gameObject.SetActive (false);
		for (int i = 0; i < 8; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}

		NameText.text = item.DisplayName;

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);

		//switch pnl
		CSVR_UIInventory.instance.UI_OnDetailOn();
		UpdateSaoPhuKien (item);
		SetUpButtonEvent (info, item);
	}

	private void SetUpButtonEvent(CatalogItem info,ItemInstance item){
		int level = 0;

		for (int s = 4; s >= 0; s--) {
			Levels.transform.GetChild (s).gameObject.SetActive(false);
		}

		if(item.ItemId.Contains(GameConstants.VNITEMPREFIX_Upgrade)){
			level = int.Parse (item.ItemId.Substring (item.ItemId.Length - 1));
			for(int s = 0; s < 5; s++){
				if(s < level)
					Levels.transform.GetChild(s).gameObject.SetActive(true);
				else
					Levels.transform.GetChild(s).gameObject.SetActive(false);
			}
		}
			
		//event 
		Button b = SellButton; 
		AddListenerSellItem (b, item,info);

		//event 
		Button ac = AccessoriesButton; 
		AddListenerEquipAC (ac, item,info);

		if (level == 5 || item.CatalogVersion == GameConstants.VNITEMCATALOG_ItemPeriod) {
			
			UpgradeButton.gameObject.SetActive(false);
			//LevelUpText
	
			LevelUpText.gameObject.SetActive (false);
			RateUpText.gameObject.SetActive (false);


		} else {
			LevelUpText.gameObject.SetActive(true);
			RateUpText.gameObject.SetActive(true);
			UpgradeButton.gameObject.SetActive(true);

			if (item.EquipGroup == "0" || string.IsNullOrEmpty(item.EquipGroup)) {
				if (findPhuKien > 0) {
					LevelUpText.text = "Nâng cấp item";
					RateUpText.text = "Vui lòng gỡ phụ kiện để nâng cấp";
					UpgradeButton.gameObject.SetActive(false);

				} else {
					
					string itemIdLevelUpgrade = item.ItemIdModel+GameConstants.VNITEMPREFIX_Upgrade +(level + 1);

					CatalogItem itemNext = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,itemIdLevelUpgrade);
					//Debug.Log (itemNext.DisplayName);
					if (itemNext != null && itemNext.Recipe != null) {
						//Debug.Log (itemNext.Recipe);
						Button u = UpgradeButton; 

						LevelUpText.text = "Nâng cấp item cấp " + (level + 1);
						RateUpText.text = "Tỉ lệ nâng cấp thành công: " + itemNext.Recipe.success_rate + "%";

						if (itemNext.Recipe.required.ContainsKey (GameConstants.VNITEMID_Coin)) {
							UpgradeButton.transform.GetChild (0).GetComponent<Image> ().sprite = coins [0];
							UpgradeButton.transform.GetChild (1).GetComponent<Text> ().text = itemNext.Recipe.required [GameConstants.VNITEMID_Coin].ToString ();
						} else if (itemNext.Recipe.required.ContainsKey (GameConstants.VNITEMID_Gold)) {
							UpgradeButton.transform.GetChild (0).GetComponent<Image> ().sprite = coins [1];
							UpgradeButton.transform.GetChild (1).GetComponent<Text> ().text = itemNext.Recipe.required [GameConstants.VNITEMID_Gold].ToString ();
						}

						AddListenerUpgradeItem (u, item, itemNext);
					}else {
						LevelUpText.text = "Nâng cấp item";
						RateUpText.text = "Item đã đạt giới hạn nâng cấp";
						UpgradeButton.gameObject.SetActive(false);
					}
				}

			} else {
				LevelUpText.text = "Nâng cấp item";
				RateUpText.text = "Vui lòng gỡ trang bị để nâng cấp";
				UpgradeButton.gameObject.SetActive(false);
			}
		}


	}

	private void AddListenerSellItem(Button b,ItemInstance info, CatalogItem item)
	{
		//Debug.Log (b);Debug.Log (info);Debug.Log (item);
		b.onClick.RemoveAllListeners();
		b.onClick.AddListener(()=> SellItem(info,item));
		
	}

	private void SellItem(ItemInstance info, CatalogItem item)
	{
		CSVR_UIPopupConfirmShop.instance.ShowPopUpSell(info,item);
	}

	private void AddListenerUpgradeItem(Button u,ItemInstance info, CatalogItem item)
	{
	//	Debug.Log (u);Debug.Log (info);Debug.Log (item);
		u.onClick.RemoveAllListeners();
		u.onClick.AddListener(()=> UpgradeItem(info,item));
		
	}

	private void UpgradeItem(ItemInstance info, CatalogItem item)
	{
		if (item.Recipe.required.ContainsKey(GameConstants.VNITEMID_Coin)) {
			if (AccountManager.instance.gameCoinAmount >= int.Parse(item.Recipe.required[GameConstants.VNITEMID_Coin].ToString())){
				AccountManager.instance.gameCoinAmount -= int.Parse (item.Recipe.required [GameConstants.VNITEMID_Coin].ToString ());
				LockUIUpgrade();
				OnUpgradeWeapon (info,item);
			}else{
				ErrorView.instance.ShowPopupError ("Không đủ bạc!..");
			}
		} else if (item.Recipe.required.ContainsKey(GameConstants.VNITEMID_Gold)) {
			if (AccountManager.instance.gameDollarAmount >= int.Parse(item.Recipe.required[GameConstants.VNITEMID_Gold].ToString())){
				AccountManager.instance.gameDollarAmount -= int.Parse (item.Recipe.required [GameConstants.VNITEMID_Gold].ToString ());
				LockUIUpgrade();
				OnUpgradeWeapon (info,item);
			}else{
				ErrorView.instance.ShowPopupError ("Không đủ vàng!..");
			}
		}
	}

	string tempLevel = "up1";
	public void OnUpgradeWeapon(ItemInstance currentItem, CatalogItem nextItem) {

		tempLevel = nextItem.ItemId.Substring (currentItem.ItemIdModel.Length + 1);
		CraftItemRequest request = new CraftItemRequest (){
			ItemId = nextItem.ItemId
		};

		HorusClientAPI.CraftItem(request, 
			(result) => {
				Debug.Log(Horus.Json.JsonWrapper.SerializeObject (result, Horus.Internal.HorusUtil.ApiSerializerStrategy));
				AccountManager.instance.inventory.Remove (currentItem);
				AccountManager.instance.inventory.Add (result.ItemCraft[0]);
				CSVR_UIInventory.instance.InstanceItemCraft(currentItem,result.ItemCraft[0]);
				TopBar.instance.UpdateVirtualCurrency ();
				UnLockUIUpgrade();

				if(result.Status.Contains("success")){
					//music
					AudioManager.instance.audio.PlayOneShot (AudioManager.instance.UpgrageSS_Clip);
					CSVR_UIInventory.instance.UpgradeSuccess (tempLevel);

				}else{
					AudioManager.instance.audio.PlayOneShot (AudioManager.instance.UpgrageFL_Clip);
					CSVR_UIInventory.instance.UpgradeFaile ();
				}
			},
			(error) => {
				UnLockUIUpgrade();
				ErrorView.instance.ShowPopupError ("Nâng cấp thất bại");
			}
		);
	}

	public GameObject imageLock;

	private void LockUIUpgrade(){
		imageLock.gameObject.SetActive (true);
	}

	private void UnLockUIUpgrade(){
		imageLock.gameObject.SetActive (false);
	}


	private void AddListenerEquipAC(Button b,ItemInstance info, CatalogItem item)
	{
		b.onClick.RemoveAllListeners();
		b.onClick.AddListener(()=> ShowACPanel(info,item));

	}

	private void ShowACPanel(ItemInstance info, CatalogItem item)
	{
		CSVR_UIInventory.instance.Close ();
		CharacterView.instance.Close ();
		//Debug.Log ("ShowACPanel....");
		CSVR_UIAccessories.instance.GetAcInfo(info,item);
	}


	public void UpdateSaoPhuKien(ItemInstance infoTemp){
		findPhuKien = 0;
		if (infoTemp.accessories != null) {
			Acs.gameObject.SetActive (true);

		} else {
			Acs.gameObject.SetActive (false);
			return;
		}
		//magazine
		try{
			string _idAcModel = infoTemp.accessories.muzzle["pluginModelId"];
			if (_idAcModel == null)
				return;
			findPhuKien++;
			Acs.transform.GetChild(1).gameObject.SetActive(true);
		}catch{
			Acs.transform.GetChild(1).gameObject.SetActive(false);
		}
		//scope
		try{
			string _idAcModel = infoTemp.accessories.scope["pluginModelId"];
			if (_idAcModel == null)
				return;
			findPhuKien++;
			Acs.transform.GetChild(2).gameObject.SetActive(true);
		}catch{
			Acs.transform.GetChild(2).gameObject.SetActive(false);
		}
		//magazine
		try{
			string _idAcModel = infoTemp.accessories.magazine["pluginModelId"];
			if (_idAcModel == null)
				return;
			findPhuKien++;
			Acs.transform.GetChild(3).gameObject.SetActive(true);
		}catch{
			Acs.transform.GetChild(3).gameObject.SetActive(false);
		}

		//grip
		try{
			string _idAcModel = infoTemp.accessories.grip["pluginModelId"];
			if (_idAcModel == null)
				return;
			findPhuKien++;
			Acs.transform.GetChild(4).gameObject.SetActive(true);
		}catch{
			Acs.transform.GetChild(4).gameObject.SetActive(false);
		}

	}
}
