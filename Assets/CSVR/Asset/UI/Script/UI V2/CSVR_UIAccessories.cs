﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Json;
using CnControls;

public class CSVR_UIAccessories : SingletonBaseScreen<CSVR_UIAccessories> {

	public ItemInstance objWeaponId;
	private CatalogItem objWeaponShop;
	public CSVR_AccessoriInfo objWeapon_AccessoryInfo;


	[SerializeField] private Text item_Name;
	[SerializeField] private Text[] info_Bar_Label;
	public Text[] info_Bar_Value;
	public Text[] ac_Info_Bar_Value;

	[SerializeField] private Transform accessories_Item_Parent;
	[SerializeField] private GameObject accessories_Item;
	[SerializeField] private string toggle_Active;
	[SerializeField] private bool isOnResetToggleList = true;


	[SerializeField] private Toggle toggle_Muzzle;
	public CSVR_AccessoriInfo muzzle_AccessoryInfo;
	private List<CatalogItem> accessories_Muzzle = new List<CatalogItem>();
	private List<ItemInstance> invAccessories_Muzzle = new List<ItemInstance>();

	[SerializeField] private Toggle toggle_Grip;
	public CSVR_AccessoriInfo grip_AccessoryInfo;
	private List<CatalogItem> accessories_Grip = new List<CatalogItem>();
	private List<ItemInstance> invAccessories_Grip = new List<ItemInstance>();

	[SerializeField] private Toggle toggle_Magazine;
	public CSVR_AccessoriInfo magazine_AccessoryInfo;
	private List<CatalogItem> accessories_Magazine = new List<CatalogItem>();
	private List<ItemInstance> invAccessories_Magazine = new List<ItemInstance>();

	[SerializeField] private Toggle toggle_Scope;
	public CSVR_AccessoriInfo scope_AccessoryInfo;
	private List<CatalogItem> accessories_Scope = new List<CatalogItem>();
	private List<ItemInstance> invAccessories_Scope = new List<ItemInstance>();

	[SerializeField] private Toggle toggle_Skin;
	public CSVR_AccessoriInfo skin_AccessoryInfo;
	private List<CatalogItem> accessories_Skin = new List<CatalogItem>();
	private List<ItemInstance> invAccessories_Skin = new List<ItemInstance>();

	[SerializeField] private Toggle toggle_Acc;
	public CSVR_AccessoriInfo acc_AccessoryInfo;
	private List<CatalogItem> accessories_Acc = new List<CatalogItem>();
	private List<ItemInstance> invAccessories_Acc = new List<ItemInstance>();

	private List<CSVR_UIItemAcc> cacheAccInfo = new List<CSVR_UIItemAcc>();
	//public Image pIcon;
	public Transform cubeOne;
	public Transform cubeTwo;
	public Transform[] accessoriesWeapon = new Transform[6];

	public Text tips;
	public Text AccTxt;
	public Button AccBtn;
	public Text toolTip;

	private Transform weapon;


	public override void OnEnable()
	{
		TopBar.instance.SetBackButton(new UnityAction(() =>{
			CSVR_UIAccessories.instance.Close();
			CharacterView.instance.Open ();
			CSVR_UIInventory.instance.Open();
			CSVR_UIInventory.instance.UI_OnInventoryOn();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			this.Close();
			CSVR_UIShop.instance.Open();
			CharacterView.instance.Open ();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));
		TopBar.instance.Setting.interactable =  false;
		TopBar.instance.ShopOrInVText.text =  "Cửa hàng";

	}
	public void OnDisable()
	{
		toggle_Muzzle.isOn = false;
		toggle_Grip.isOn = false;
		toggle_Magazine.isOn = false;
		toggle_Scope.isOn = false;
		toggle_Skin.isOn = false;
		toggle_Acc.isOn = false;
		isOnResetToggleList = true;
		ResetInfoView ();
		AccBtn.transform.parent.gameObject.SetActive (false);
		if(weapon != null)
			weapon.gameObject.SetActive (false);
		
		for (int i = 0; i < accessories_Item_Parent.childCount; i++) {
			Destroy (accessories_Item_Parent.GetChild(i).gameObject);
		}
		toggle_Magazine.onValueChanged.RemoveAllListeners ();
		toggle_Grip.onValueChanged.RemoveAllListeners ();
		toggle_Muzzle.onValueChanged.RemoveAllListeners ();
		toggle_Scope.onValueChanged.RemoveAllListeners ();
		toggle_Skin.onValueChanged.RemoveAllListeners ();
		toggle_Acc.onValueChanged.RemoveAllListeners ();


		if (accessoriesWeapon [0] != null){
			accessoriesWeapon[0].GetChild (0).gameObject.SetActive (true);
			if (accessoriesWeapon [0].childCount > 1) {
				Destroy (accessoriesWeapon [0].GetChild (1).gameObject);
			}
		}
		if (accessoriesWeapon [1] != null) {
			accessoriesWeapon [1].GetChild (0).gameObject.SetActive (true);
			if (accessoriesWeapon [1].childCount > 1) {
				Destroy (accessoriesWeapon [1].GetChild (1).gameObject);
			}
		}
		if (accessoriesWeapon [2] != null) {
			accessoriesWeapon [2].GetChild (0).gameObject.SetActive (true);
			if (accessoriesWeapon [2].childCount > 1) {
				Destroy (accessoriesWeapon [2].GetChild (1).gameObject);
			}
		}
		if (accessoriesWeapon [3] != null) {
			accessoriesWeapon [3].GetChild (0).gameObject.SetActive (true);
			if (accessoriesWeapon [3].childCount > 1) {
				Destroy (accessoriesWeapon [3].GetChild (1).gameObject);
			}
		}
	}

	public void GetAcInfo(ItemInstance info, CatalogItem item = null){
		//Debug.Log ("CSVR_UIAccessories.instance.GetAcInfo(info): "+Horus.Json.JsonWrapper.SerializeObject (info, Horus.Internal.HorusUtil.ApiSerializerStrategy));
		toggle_Active = string.Empty;
		objWeaponId = info;
		ResetAccList (item);
		ResetToggleList ();
		SetupAccessoriesTransform (info);

		this.gameObject.SetActive (true);
		item_Name.text = item.DisplayName;
		UpdateAccInfo (info, item);

	}

	public void UpdateAccInfo(ItemInstance info, CatalogItem item = null){

		switch (info.ItemClass) {
		case GameConstants.VNITEMCLASS_MeleeWeapon:
			GetInfoMelee(item, info);
			break;
		case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
		case GameConstants.VNITEMCLASS_GrenadeWeapon:
			GetInfoGrenade(item, info);
			break;
		case GameConstants.VNITEMCLASS_HelmetWeapon:
		case GameConstants.VNITEMCLASS_ArmorWeapon:
			GetInfoArmor(item, info);
			break;
		default:
			GetInfoGun(item, info);
			break;
		}

	}

	private void GetInfoGun(CatalogItem info,ItemInstance item)
	{
		for (int i = 0; i < 8; i++) {
			info_Bar_Label[i].gameObject.SetActive(true);
			info_Bar_Value [i].text = string.Empty;
			info_Bar_Value [i].color = Color.white;
		}

		info_Bar_Label[1].text = "Chính xác";
		info_Bar_Label[2].text  = "Độ giật";

		CSVR_GunInfo infoGun;
		try{
			infoGun = HorusSimpleJson.DeserializeObject<CSVR_GunInfo>(item.CustomData);
		}catch{
			infoGun = HorusSimpleJson.DeserializeObject<CSVR_GunInfo>(info.CustomData);
		}
		objWeapon_AccessoryInfo = new CSVR_AccessoriInfo ();
		objWeapon_AccessoryInfo.A1 = new ACInfo ();
		objWeapon_AccessoryInfo.A1.value = infoGun.A1.ToString();
		objWeapon_AccessoryInfo.A2 = new ACInfo ();
		objWeapon_AccessoryInfo.A2.value = infoGun.A2.ToString();
		objWeapon_AccessoryInfo.R1 = new ACInfo ();
		objWeapon_AccessoryInfo.R1.value = infoGun.A1.ToString();
		objWeapon_AccessoryInfo.R2X = new ACInfo ();
		objWeapon_AccessoryInfo.R2X.value = infoGun.A2.ToString();


		//dame
		info_Bar_Value[0].text = infoGun.G1.ToString();

		//accuracy
		float m_min = infoGun.A1;
		float m_max = infoGun.A2;
		int m_AC = (int)(10*(10-(5*m_min+m_max)/6));
		info_Bar_Value[1].text = m_AC.ToString();

		//ap
		info_Bar_Value[7].text = infoGun.G6.ToString();

		//recoil
		float m_R1 = Mathf.Abs(infoGun.R1);
		float m_R2X = Mathf.Abs(infoGun.R2X);
		int m_RC = (int)((1-m_R1*m_R2X/5)*100);
		info_Bar_Value[2].text = m_RC.ToString();

		//reload
		info_Bar_Value[3].text = infoGun.G3+ " s";

		//rate of fire
		float m_ROFText = (int)(60/float.Parse(infoGun.G2.ToString()));
		info_Bar_Value[4].text = m_ROFText+" RPM";

		//capacity
		info_Bar_Value[5].text = infoGun.G4.ToString();

		//weight
		info_Bar_Value[6].text = infoGun.G5 +"Kg";
		CheckInfoChange (item);
	}
	private void GetInfoMelee(CatalogItem info,ItemInstance item)
	{

		for (int i = 0; i < 8; i++) {
			info_Bar_Label[i].gameObject.SetActive(false);
			info_Bar_Value [i].text = string.Empty;
		}

		info_Bar_Label[1].text = "Khoảng cách";
		info_Bar_Label[2].text = "Xuyên giáp";

		CSVR_MeleInfo infoMelee;
		try{
			infoMelee = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo>(item.CustomData);
		}catch{
			infoMelee = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo>(info.CustomData);
		}

		//Damages
		info_Bar_Label[0].gameObject.SetActive(true);
		info_Bar_Value[0].text = infoMelee.Damge.ToString();

		//Ranges
		info_Bar_Label[1].gameObject.SetActive(true);
		info_Bar_Value[1].text = infoMelee.Ranges.ToString();

		//A.p
		info_Bar_Label[2].gameObject.SetActive(true);
		info_Bar_Value[2].text = infoMelee.AP.ToString();
	}
	private void GetInfoGrenade(CatalogItem info,ItemInstance item)
	{

		for (int i = 0; i < 8; i++) {
			info_Bar_Label[i].gameObject.SetActive(false);
		}

		info_Bar_Label[1].text = "Khoảng cách";
		info_Bar_Label[2].text = "Xuyên giáp";

		CSVR_GrenadeInfo infoGrenadeInfo;
		try{
			infoGrenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo>(info.CustomData);
		}catch{
			infoGrenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo>(item.CustomData);
		}

		//Damages
		info_Bar_Label[0].gameObject.SetActive(true);
		info_Bar_Value[0].text = infoGrenadeInfo.Damge.ToString();
		//Ranges
		info_Bar_Label[1].gameObject.SetActive(true);
		info_Bar_Value[1].text = infoGrenadeInfo.Ranges.ToString();
		//A.p
		info_Bar_Label[2].gameObject.SetActive(true);
		info_Bar_Value[2].text = infoGrenadeInfo.AP.ToString();

	}
	private void GetInfoArmor(CatalogItem info,ItemInstance item)
	{

		for (int i = 0; i < 8; i++) {
			info_Bar_Label[i].gameObject.SetActive(false);
		}

		info_Bar_Label[1].text = "Độ bền";
		info_Bar_Label[2].text = "Chống đỡ";

		CSVR_ArmorInfo infoArmorInfo;
		try{
			infoArmorInfo = HorusSimpleJson.DeserializeObject<CSVR_ArmorInfo>(info.CustomData);
		}catch{
			infoArmorInfo = HorusSimpleJson.DeserializeObject<CSVR_ArmorInfo>(item.CustomData);
		}

		//Damages
		info_Bar_Label[0].gameObject.SetActive(true);
		info_Bar_Value[0].text = infoArmorInfo.Reliability.ToString();
		//Ranges
		info_Bar_Label[1].gameObject.SetActive(true);
		info_Bar_Value[1].text = infoArmorInfo.Resilience.ToString();

	}
	public void ResetAccList(CatalogItem item = null){
		if (item != null) {
			objWeaponShop = item;
		} else {
			item = objWeaponShop;
		}
		//Debug.Log (JsonWrapper.SerializeObject (new_AccessoriInfo, Horus.Internal.HorusUtil.ApiSerializerStrategy));
		Debug.Log (toggle_Active);
		switch (toggle_Active) {

		case GameConstants.VNITEMCLASS_MuzzleAccessories:
			accessories_Muzzle.Clear();
			invAccessories_Muzzle.Clear ();
			break;
		case GameConstants.VNITEMCLASS_GripAccessories:
			accessories_Grip.Clear();
			invAccessories_Grip.Clear();
			break;
		case GameConstants.VNITEMCLASS_MagazineAccessories:
			accessories_Magazine.Clear();
			invAccessories_Magazine.Clear();
			break;
		case GameConstants.VNITEMCLASS_ScopeAccessories:
			accessories_Scope.Clear();
			invAccessories_Scope.Clear();
			break;
		case GameConstants.VNITEMCLASS_SkinAccessories:
			accessories_Skin.Clear();
			invAccessories_Skin.Clear();
			break;
		case GameConstants.VNITEMCLASS_AccAccessories:
			accessories_Acc.Clear();
			invAccessories_Acc.Clear();
			break;
		default:
			accessories_Muzzle.Clear();
			invAccessories_Muzzle.Clear ();
			accessories_Grip.Clear();
			invAccessories_Grip.Clear();
			accessories_Magazine.Clear();
			invAccessories_Magazine.Clear();
			accessories_Scope.Clear();
			invAccessories_Scope.Clear();
			accessories_Skin.Clear();
			invAccessories_Skin.Clear();
			accessories_Acc.Clear();
			invAccessories_Acc.Clear();
			break;
		}
			




		if (item.accessories != null && item.accessories.itemIdList.Length > 0) {
			string[] accList = item.accessories.itemIdList;

			for (int i = 0; i < accList.Length; i++) {
				for (int j = 0; j < AccountManager.instance.inventory.Count; j++) {
					if (AccountManager.instance.inventory [j].ItemId == accList [i] && (AccountManager.instance.inventory [j].ItemClass == toggle_Active ||  string.IsNullOrEmpty(toggle_Active))) {

						switch (AccountManager.instance.inventory[j].ItemClass) {
						case GameConstants.VNITEMCLASS_MuzzleAccessories:
							invAccessories_Muzzle.Add (AccountManager.instance.inventory[j]);
							break;
						case GameConstants.VNITEMCLASS_GripAccessories:
							invAccessories_Grip.Add (AccountManager.instance.inventory[j]);
							break;
						case GameConstants.VNITEMCLASS_MagazineAccessories:
							invAccessories_Magazine.Add (AccountManager.instance.inventory[j]);
							break;
						case GameConstants.VNITEMCLASS_ScopeAccessories:
							invAccessories_Scope.Add (AccountManager.instance.inventory[j]);
							break;
						case GameConstants.VNITEMCLASS_SkinAccessories:
							invAccessories_Skin.Add (AccountManager.instance.inventory[j]);
							break;
						case GameConstants.VNITEMCLASS_AccAccessories:
							invAccessories_Acc.Add (AccountManager.instance.inventory[j]);
							break;
						}
					}
				}
			}



			for (int i = 0; i < accList.Length; i++) {
			
				CatalogItem cataLog = CSVR_GameSetting.FindItemInListShopItems (CSVR_GameSetting.shopAccessories, accList [i]);
				if (cataLog == null)
					return;
				
				if (cataLog.ItemClass == toggle_Active || string.IsNullOrEmpty (toggle_Active)) {
					switch (cataLog.ItemClass) {
					case GameConstants.VNITEMCLASS_MuzzleAccessories:
						accessories_Muzzle.Add (cataLog);
						break;
					case GameConstants.VNITEMCLASS_GripAccessories:
						accessories_Grip.Add (cataLog);
						break;
					case GameConstants.VNITEMCLASS_MagazineAccessories:
						accessories_Magazine.Add (cataLog);
						break;
					case GameConstants.VNITEMCLASS_ScopeAccessories:
						accessories_Scope.Add (cataLog);
						break;
					case GameConstants.VNITEMCLASS_SkinAccessories:
						accessories_Skin.Add (cataLog);
						break;
					case GameConstants.VNITEMCLASS_AccAccessories:
						accessories_Acc.Add (cataLog);
						break;
					}
				}
			}


		}
	}
	public void ResetToggleList(){
		AccBtn.transform.parent.gameObject.SetActive (false);

		if (objWeaponId.accessories == null)
			return;
		
		toggle_Muzzle.interactable = accessories_Muzzle.Count > 0 ? true : false;
		if (toggle_Muzzle.interactable){
			if (isOnResetToggleList || toggle_Active == GameConstants.VNITEMCLASS_MuzzleAccessories) {
				toggle_Muzzle.isOn = isOnResetToggleList;
				isOnResetToggleList = false;
				toggle_Active = "muzzle.plugin";
				SetupAccessories(invAccessories_Muzzle,accessories_Muzzle);
			}
			toggle_Muzzle.onValueChanged.RemoveAllListeners ();
			toggle_Muzzle.onValueChanged.AddListener ((value) => {
				if(value){
					toggle_Active = "muzzle.plugin";
					SetupAccessories(invAccessories_Muzzle,accessories_Muzzle);
				}else{
					AccBtn.transform.parent.gameObject.SetActive (false);

					AccessoriesMuzzle(objWeaponId);
				}
			});
		}

		toggle_Grip.interactable = accessories_Grip.Count > 0 ? true : false;
		if (toggle_Grip.interactable){
			if (isOnResetToggleList || toggle_Active == GameConstants.VNITEMCLASS_GripAccessories) {
				toggle_Grip.isOn = isOnResetToggleList;
				isOnResetToggleList = false;
				toggle_Active = "grip.plugin";
				SetupAccessories(invAccessories_Grip,accessories_Grip);
			}
			toggle_Grip.onValueChanged.RemoveAllListeners ();
			toggle_Grip.onValueChanged.AddListener ((value) => {
				if(value){
					toggle_Active = "grip.plugin";
					SetupAccessories(invAccessories_Grip,accessories_Grip);
				}else{
					AccBtn.transform.parent.gameObject.SetActive (false);
					AccessoriesMuzzle(objWeaponId);
					//AccessoriesGrip(objWeaponId);
				}
			});
		}

		toggle_Magazine.interactable = accessories_Magazine.Count > 0 ? true : false;
		if (toggle_Magazine.interactable){
			if (isOnResetToggleList || toggle_Active == GameConstants.VNITEMCLASS_MagazineAccessories) {
				toggle_Magazine.isOn = isOnResetToggleList;
				isOnResetToggleList = false;
				toggle_Active = "magazine.plugin";
				SetupAccessories(invAccessories_Magazine,accessories_Magazine);
			}
			toggle_Magazine.onValueChanged.RemoveAllListeners ();
			toggle_Magazine.onValueChanged.AddListener ((value) => {
				if(value){
					toggle_Active = "magazine.plugin";
					SetupAccessories(invAccessories_Magazine,accessories_Magazine);
				}else{
					AccBtn.transform.parent.gameObject.SetActive (false);

					AccessoriesMagazine(objWeaponId);
				}
			});
		}

		toggle_Scope.interactable = accessories_Scope.Count > 0 ? true : false;
		if (toggle_Scope.interactable){
			if (isOnResetToggleList || toggle_Active == GameConstants.VNITEMCLASS_ScopeAccessories) {
				toggle_Scope.isOn = isOnResetToggleList;
				isOnResetToggleList = false;
				toggle_Active = "scope.plugin";
				SetupAccessories(invAccessories_Scope,accessories_Scope);
			}
			toggle_Scope.onValueChanged.RemoveAllListeners ();
			toggle_Scope.onValueChanged.AddListener ((value) => {
				if(value){
					toggle_Active = "scope.plugin";
					SetupAccessories(invAccessories_Scope,accessories_Scope);
				}else{
					AccBtn.transform.parent.gameObject.SetActive (false);

					AccessoriesScope(objWeaponId);
				}
			});
		}

		toggle_Skin.interactable = accessories_Skin.Count > 0 ? true : false;
		if (toggle_Scope.interactable || toggle_Active == GameConstants.VNITEMCLASS_SkinAccessories){
			if (isOnResetToggleList) {
				toggle_Scope.isOn = isOnResetToggleList;
				isOnResetToggleList = false;
				toggle_Active = "skin.plugin";
				SetupAccessories(invAccessories_Scope,accessories_Scope);
			}
			toggle_Skin.onValueChanged.RemoveAllListeners ();
			toggle_Skin.onValueChanged.AddListener ((value) => {
				if(value){
					toggle_Active = "skin.plugin";
					SetupAccessories(invAccessories_Skin,accessories_Skin);
				}else{
					AccBtn.transform.parent.gameObject.SetActive (false);

				}
			});
		}

		toggle_Acc.interactable = accessories_Acc.Count > 0 ? true : false;
		if (toggle_Acc.interactable){
			if (isOnResetToggleList || toggle_Active == GameConstants.VNITEMCLASS_AccAccessories) {
				toggle_Acc.isOn = isOnResetToggleList;
				isOnResetToggleList = false;
				toggle_Active = "acc.plugin";
				SetupAccessories(invAccessories_Acc,accessories_Acc);
			}
			//toggle_Acc.onValueChanged.RemoveAllListeners ();
			toggle_Acc.onValueChanged.AddListener ((value) => {
				if(value){
					toggle_Active = "acc.plugin";
					SetupAccessories(invAccessories_Acc,accessories_Acc);
				}else{
					AccBtn.transform.parent.gameObject.SetActive (false);

				}
			});
		}
		//Yo.JsonLog ("2. ", objWeaponId.accessories);
	}
	private void SetLayerRecursively(GameObject go, int layerNumber)
	{
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
			trans.gameObject.layer = layerNumber;
		}
	}
	private void SetupAccessoriesTransform( ItemInstance idWeapon){
		weapon = cubeTwo.FindChild (idWeapon.ItemIdModel);
		if (weapon == null)
			return;
		weapon.gameObject.SetActive (true);

		vp_WeaponAccessories acsTransform;
		if (weapon.transform.childCount == 0) {
			GameObject foundThirdWeapon = Resources.Load ("Weapon/Remote/Third/" + idWeapon.ItemIdModel) as GameObject;
			if (foundThirdWeapon == null)
				return;
			
			GameObject newWeapon = Instantiate (foundThirdWeapon);
			SetLayerRecursively(newWeapon,31);
			newWeapon.transform.SetParent (weapon.transform);
			newWeapon.transform.localScale = Vector3.one;
			newWeapon.transform.localPosition = Vector3.zero;
			newWeapon.transform.localRotation = Quaternion.identity;
			acsTransform = newWeapon.GetComponent<vp_WeaponAccessories> ();
			newWeapon.gameObject.SetActive (true);
		} else {
			acsTransform = weapon.transform.GetChild(0).GetComponent<vp_WeaponAccessories> ();
		}
			
		if (acsTransform == null || idWeapon.accessories == null)
			return;
		
		if (acsTransform.accessoriesScopeTransform != null) {
			accessoriesWeapon [0] = acsTransform.accessoriesScopeTransform;
			AccessoriesScope (idWeapon);
		}
			
		if (acsTransform.accessoriesMagazineTransform != null) {
			accessoriesWeapon [1] = acsTransform.accessoriesMagazineTransform;
			AccessoriesMagazine (idWeapon);
		}


		if (acsTransform.accessoriesGripTransform != null) {
			accessoriesWeapon [2] = acsTransform.accessoriesGripTransform;
			AccessoriesGrip (idWeapon);
		}


		if (acsTransform.accessoriesMuzzleTransform != null) {
			accessoriesWeapon [3] = acsTransform.accessoriesMuzzleTransform;
			AccessoriesMuzzle (idWeapon);
		}
		//Yo.JsonLog ("1. ", idWeapon.accessories);
	}
	public void ResetInfoView(){
		for (int i = 0; i < 8; i++) {
			ac_Info_Bar_Value [i].text = string.Empty;
		}


	}

	private void SetupAccessories(List<ItemInstance> listAccessoriesIns,List<CatalogItem> listAccessoriesCat  ){
		List<ItemInstance> invAccessories_Active = listAccessoriesIns;
		List<CatalogItem> accessories_Active = listAccessoriesCat;
		cacheAccInfo.Clear();

		int invCount = invAccessories_Active.Count;
		int catCount = accessories_Active.Count;
		int totalCount = invCount + catCount;

		ResetInfoView ();

		for (int i = 0; i < accessories_Item_Parent.childCount; i++) {
			Destroy (accessories_Item_Parent.GetChild(i).gameObject);
		}


		for (int i = 0; i < totalCount; i++) {
			if (i < invCount)
				InstanceItemAcc (invAccessories_Active [i]);
			else
				InstanceItemAcc (accessories_Active [i - invCount]);
		}

	}
	public void InstanceItemAcc(CatalogItem item)
	{
		CSVR_UIItemAcc _acs = cacheAccInfo.Find(x => x.itemId.Contains(item.ItemId));
		if (_acs == null) {
			GameObject acc = Instantiate (accessories_Item);
			acc.transform.SetParent (accessories_Item_Parent);
			acc.transform.localScale = Vector3.one;
			CSVR_UIItemAcc info = acc.GetComponent<CSVR_UIItemAcc> ();
			info.GetAccInfo (item);
			cacheAccInfo.Add (info);
		}
	}
	public void InstanceItemAcc(ItemInstance item)
	{
		GameObject acc = Instantiate(accessories_Item);
		acc.transform.SetParent(accessories_Item_Parent);
		acc.transform.localScale = Vector3.one;
		CSVR_UIItemAcc info = acc.GetComponent<CSVR_UIItemAcc>();
		info.GetAccInfo(item);
		cacheAccInfo.Add (info);
	}
	int acc = 1;
	private void Update()
	{

		if(CnInputManager.GetAxis("Fire Vertical") > 5 ||CnInputManager.GetAxis("Fire Vertical") < -5) 
		{
			cubeOne.transform.Rotate(new Vector3(CnInputManager.GetAxis("Fire Vertical"),0, 0) * Time.deltaTime * 5 );
		}
		if(CnInputManager.GetAxis("Fire Horizontal") > 5 ||CnInputManager.GetAxis("Fire Horizontal") < -5) {
			if (CnInputManager.GetAxis ("Fire Horizontal") > 0) {
				acc = -1;
			} else {
				acc = 1;
			}
			cubeTwo.transform.Rotate(new Vector3(0,-CnInputManager.GetAxis("Fire Horizontal"), 0) * Time.deltaTime * 5 );
		}
		else if(CnInputManager.GetAxis("Fire Horizontal") == 0) {
			
			cubeTwo.transform.Rotate(new Vector3(0,acc, 0) * Time.deltaTime * 10 );
		}
		if (Input.touchCount == 2 )
		{
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			float deltaMagnitudeDiff = -0.2f*(prevTouchDeltaMag - touchDeltaMag);

			cubeOne.transform.localScale += new Vector3(deltaMagnitudeDiff,deltaMagnitudeDiff, deltaMagnitudeDiff); ;

			cubeOne.transform.localScale = new Vector3(Mathf.Clamp(cubeOne.transform.localScale.x, 5, 15), Mathf.Clamp(cubeOne.transform.localScale.y, 5, 15), Mathf.Clamp(cubeOne.transform.localScale.z, 5, 15));
		}
	}

	public void SetButtonEvent(UnityEngine.Events.UnityAction action,string prices,string tip,string tt)
	{
		AccBtn.transform.parent.gameObject.SetActive (true);
		AccBtn.onClick.RemoveAllListeners();
		AccBtn.onClick.AddListener(action);
		tips.text = tip;
		AccTxt.text = prices;
		toolTip.text = tt;
	}

	public string FindItemInstanceByID(string _id,List<ItemInstance> listItem){
		for (int i = 0; i < listItem.Count; i++) {
			if (listItem [i]._id == _id)
				return listItem[i].CustomData;
		}
		return null;
	}

	private void AccessoriesScope( ItemInstance idWeapon){
		try{
			string _idAcModel = idWeapon.accessories.scope["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			_obj.transform.SetParent(accessoriesWeapon [0]);
			accessoriesWeapon[0].GetChild(0).gameObject.SetActive(false);
			if(accessoriesWeapon[0].childCount > 2){
				Destroy(accessoriesWeapon[0].GetChild(1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive(true);

		}catch{
			accessoriesWeapon[0].GetChild(0).gameObject.SetActive(true);
			if(accessoriesWeapon[0].childCount > 1){
				Destroy(accessoriesWeapon[0].GetChild(1).gameObject);
			}
		}
	}
	private void AccessoriesMagazine( ItemInstance idWeapon){
		try{
			string _idAcModel = idWeapon.accessories.magazine["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			_obj.transform.SetParent(accessoriesWeapon [1]);
			accessoriesWeapon[1].GetChild(0).gameObject.SetActive(false);
			if(accessoriesWeapon[1].childCount > 2){
				Destroy(accessoriesWeapon[1].GetChild(1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive(true);

		}catch{
			accessoriesWeapon[1].GetChild(0).gameObject.SetActive(true);
			if(accessoriesWeapon[1].childCount > 1){
				Destroy(accessoriesWeapon[1].GetChild(1).gameObject);
			}
		}
	}
	private void AccessoriesGrip( ItemInstance idWeapon){
		try {
			string _idAcModel = idWeapon.accessories.grip["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			_obj.transform.SetParent (accessoriesWeapon [2]);
			accessoriesWeapon [2].GetChild (0).gameObject.SetActive (false);
			if (accessoriesWeapon [2].childCount > 2) {
				Destroy (accessoriesWeapon [2].GetChild (1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive (true);

		} catch {
			accessoriesWeapon [2].GetChild (0).gameObject.SetActive (true);
			if (accessoriesWeapon [2].childCount > 1) {
				Destroy (accessoriesWeapon [2].GetChild (1).gameObject);
			}
		}
	}
	private void AccessoriesMuzzle( ItemInstance idWeapon){
		try {
			string _idAcModel = idWeapon.accessories.muzzle["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			_obj.transform.SetParent (accessoriesWeapon [3]);
			accessoriesWeapon [3].GetChild (0).gameObject.SetActive (false);
			if (accessoriesWeapon [3].childCount > 2) {
				Destroy (accessoriesWeapon [3].GetChild (1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive (true);

		} catch {
			accessoriesWeapon [3].GetChild (0).gameObject.SetActive (true);
			if (accessoriesWeapon [3].childCount > 1) {
				Destroy (accessoriesWeapon [3].GetChild (1).gameObject);
			}
		}
	}

	private void CheckInfoChange( ItemInstance idWeapon){
		CSVR_AccessoriInfo new_AccessoriInfo;
		try{
			string _idAcScope = idWeapon.accessories.scope["objId"];
			if(_idAcScope != null){
				new_AccessoriInfo = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(FindItemInstanceByID(_idAcScope,invAccessories_Scope));
				//Debug.Log (JsonWrapper.SerializeObject (new_AccessoriInfo, Horus.Internal.HorusUtil.ApiSerializerStrategy));
				//Debug.Log (JsonWrapper.SerializeObject (scope_AccessoryInfo, Horus.Internal.HorusUtil.ApiSerializerStrategy));
				if(new_AccessoriInfo != scope_AccessoryInfo)
					scope_AccessoryInfo = new_AccessoriInfo;
			}else{
				scope_AccessoryInfo = null;
			}
		}catch{
			scope_AccessoryInfo = null;
		}

		try{
			string _idAcMuzzle = idWeapon.accessories.muzzle["objId"];
			if(_idAcMuzzle != null){
				new_AccessoriInfo = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(FindItemInstanceByID(_idAcMuzzle,invAccessories_Muzzle));
				if(new_AccessoriInfo != muzzle_AccessoryInfo)
					muzzle_AccessoryInfo = new_AccessoriInfo;
			}else{
				muzzle_AccessoryInfo = null;
			}
		}catch{
			muzzle_AccessoryInfo = null;
		}

		try{
			string _idAcGrip = idWeapon.accessories.grip["objId"];
			if(_idAcGrip != null){
				new_AccessoriInfo = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(FindItemInstanceByID(_idAcGrip,invAccessories_Grip));
				if(new_AccessoriInfo != grip_AccessoryInfo)
					grip_AccessoryInfo = new_AccessoriInfo;
			}else{
				grip_AccessoryInfo = null;
			}
		}catch{
			grip_AccessoryInfo = null;
		}

		try{
			string _idAcMagazine = idWeapon.accessories.magazine["objId"];
			if(_idAcMagazine != null){
				new_AccessoriInfo = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(FindItemInstanceByID(_idAcMagazine,invAccessories_Magazine));
				if(new_AccessoriInfo != magazine_AccessoryInfo)
					magazine_AccessoryInfo = new_AccessoriInfo;
			}else{
				magazine_AccessoryInfo = null;
			}
		}catch{
			magazine_AccessoryInfo = null;
		}
		//Debug.Log (JsonWrapper.SerializeObject (scope_AccessoryInfo, Horus.Internal.HorusUtil.ApiSerializerStrategy));
//		if (scope_AccessoryInfo != null || muzzle_AccessoryInfo != null || grip_AccessoryInfo != null || toggle_Magazine != null) {
//			if (scope_AccessoryInfo.G1 != null) {
//				info_Bar_Value [0].color = Color.green;
//			}
//			if (scope_AccessoryInfo.A1 != null || scope_AccessoryInfo.A2 != null) {
//				info_Bar_Value [1].color = Color.green;
//			}
//			if (scope_AccessoryInfo.G6 != null ) {
//				info_Bar_Value [7].color = Color.green;
//			}
//			if (scope_AccessoryInfo.R1 != null || scope_AccessoryInfo.R2X != null) {
//				info_Bar_Value [2].color = Color.green;
//			}
//			if (scope_AccessoryInfo.G3 != null ) {
//				info_Bar_Value [3].color = Color.green;
//			}
//			if (scope_AccessoryInfo.G2 != null ) {
//				info_Bar_Value [4].color = Color.green;
//			}
//			if (scope_AccessoryInfo.G4 != null ) {
//				info_Bar_Value [5].color = Color.green;
//			}
//			if (scope_AccessoryInfo.G5 != null ) {
//				info_Bar_Value [6].color = Color.green;
//			}
//		}
	}

}
