﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;

public class CSVR_UIItemBagInv : MonoBehaviour {

	public int GroupID;
	public Image MainGun;
	public Image Pistol;
	public Image Melee;
	public Image Grenade;
	public Image OneGrenade;
	public Image TwoGrenade;
	public Image Helmet;
	public Image Armor;
	public Image Pendant;

	Transform mainGunTransform,
	pistolTransform,
	meleeTransform,
	grenadeTransform,
	oneGrenadeTransform,
	twoGrenadeTransform,
	helmetTransform,
	armorTransform;

	private bool isEquip = false;

	void OnDisable(){
		isEquip = false;
	}

	public void Equip(ItemInstance item, int typeWeapon,Sprite iconWeapon,bool equipLive = false){
		if(mainGunTransform == null)
			mainGunTransform = MainGun.GetComponent<Transform> ();
		if(pistolTransform == null)
			pistolTransform = Pistol.GetComponent<Transform> ();
		if(meleeTransform == null)
			meleeTransform = Melee.GetComponent<Transform> ();
		if(grenadeTransform == null)
			grenadeTransform = Grenade.GetComponent<Transform> ();
		if(oneGrenadeTransform == null)
			oneGrenadeTransform = OneGrenade.GetComponent<Transform> ();
		if(twoGrenadeTransform == null)
			twoGrenadeTransform = TwoGrenade.GetComponent<Transform> ();
		if(helmetTransform == null)
			helmetTransform = Helmet.GetComponent<Transform> ();
		if(armorTransform == null)
			armorTransform = Armor.GetComponent<Transform> ();

		switch (typeWeapon) {
		case 1://sung chinh
			CharacterView.instance.itemsGroup [GroupID - 1, 0] = item;
			MainGun.sprite = iconWeapon;
			MainGun.SetNativeSize();
			mainGunTransform.localScale = new Vector3(0.3f,0.3f,0.3f);
			break;
		case 2://sung luc
			CharacterView.instance.itemsGroup [GroupID - 1, 1] = item;
			Pistol.sprite = iconWeapon;
			Pistol.SetNativeSize();
			pistolTransform.localScale = new Vector3(0.3f,0.3f,0.3f);
			break;
		case 3://dao
			CharacterView.instance.itemsGroup [GroupID - 1, 2] = item;
			Melee.sprite = iconWeapon;
			Melee.SetNativeSize();
			meleeTransform.localScale = new Vector3(0.3f,0.3f,0.3f);
			break;
		case 4://bom no
			Grenade.sprite = iconWeapon;
			Grenade.SetNativeSize();
			grenadeTransform.localScale = new Vector3(0.3f,0.3f,0.3f);
			break;
		case 5://bom khoi
			OneGrenade.sprite = iconWeapon;
			OneGrenade.SetNativeSize();
			oneGrenadeTransform.localScale = new Vector3(0.3f,0.3f,0.3f);
			break;
		case 6://bom mu
			TwoGrenade.sprite = iconWeapon;
			TwoGrenade.SetNativeSize();
			twoGrenadeTransform.localScale = new Vector3(0.3f,0.3f,0.3f);
			break;
		case 7://giap dau
			Helmet.sprite = iconWeapon;
			Helmet.SetNativeSize();
			helmetTransform.localScale = new Vector3(0.25f,0.25f,0.25f);
			break;
		case 8://giap than
			Armor.sprite = iconWeapon;
			Armor.SetNativeSize();
			armorTransform.localScale = new Vector3(0.25f,0.25f,0.25f);
			break;
		}
		if(equipLive){
			isEquip = false;
			CheckVIewGun();
		}
	}

	public void UnEquip(int typeWeapon){
		switch (typeWeapon) {
		case 1://sung chinh
			CharacterView.instance.itemsGroup [GroupID - 1, 0] = null;
			MainGun.sprite = CSVR_UIInventory.instance.iconWeaponDefault[0];
			MainGun.SetNativeSize();
			mainGunTransform.localScale = Vector3.one;
			isEquip = false;
			CheckVIewGun();
			break;
		case 2://sung luc
			CharacterView.instance.itemsGroup [GroupID - 1, 1] = null;
			Pistol.sprite = CSVR_UIInventory.instance.iconWeaponDefault[1];
			Pistol.SetNativeSize();
			pistolTransform.localScale = Vector3.one;
			isEquip = false;
			CheckVIewGun();
			break;
		case 3://dao
			CharacterView.instance.itemsGroup [GroupID - 1, 2] = null;
			Melee.sprite = CSVR_UIInventory.instance.iconWeaponDefault[2];
			Melee.SetNativeSize();
			meleeTransform.localScale = Vector3.one;
			isEquip = false;
			CheckVIewGun();
			break;
		case 4://bom no
			Grenade.sprite = CSVR_UIInventory.instance.iconWeaponDefault[3];
			Grenade.SetNativeSize();
			grenadeTransform.localScale = Vector3.one;
			break;
		case 5://bom khoi
			OneGrenade.sprite = CSVR_UIInventory.instance.iconWeaponDefault[3];
			OneGrenade.SetNativeSize();
			oneGrenadeTransform.localScale = Vector3.one;
			break;
		case 6://bom mu
			TwoGrenade.sprite = CSVR_UIInventory.instance.iconWeaponDefault[3];
			TwoGrenade.SetNativeSize();
			twoGrenadeTransform.localScale = Vector3.one;
			break;
		case 7://giap dau
			Helmet.sprite = CSVR_UIInventory.instance.iconWeaponDefault[4];
			Helmet.SetNativeSize();
			helmetTransform.localScale = Vector3.one;
			break;
		case 8://giap than
			Armor.sprite = CSVR_UIInventory.instance.iconWeaponDefault[5];
			Armor.SetNativeSize();
			armorTransform.localScale = Vector3.one;
			break;
		}
	}

	public void CheckVIewGun(){

		if(CSVR_UIInventory.instance.Group == GroupID && !isEquip){
			for (int i =0; i < 3; i++) {
				if (CharacterView.instance.itemsGroup[GroupID-1,i] != null)
	            {
					CharacterView.instance.GetIDGunDefaultInventory(CharacterView.instance.itemsGroup[GroupID-1,i]);
					isEquip = true;
					break;
				}
			}
			if (!isEquip) {
                CharacterView.instance.GetIDGunDefaultName (GameConstants.VNITEMDEFAULT_Knife);
				isEquip = true;
			}
		}
	}
}
