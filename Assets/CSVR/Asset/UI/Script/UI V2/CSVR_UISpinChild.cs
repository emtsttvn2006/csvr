﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.Json;
using Horus.ClientModels;

public class CSVR_UISpinChild : MonoBehaviour
{
	[SerializeField] private Toggle[] backgroundIconReward;
	[SerializeField] private Image[] itemIconReward;
	[SerializeField] private Image[] itemIconNew;
	[SerializeField] private Image[] itemIconEffect;
	[SerializeField] private Text[] itemDayReward;

	[SerializeField] private Text[] spinPrice;	
	[SerializeField] private Animator anim;

	private float timeStart;
	private string itemID;
	private CatalogItem bundlesItem;
	private CatalogItem bundlesItem10;
	ItemInstance newItem;


	public void Show(CatalogItem CatalogItem,CatalogItem CatalogItem10)
    {
        this.bundlesItem = CatalogItem;
		this.bundlesItem10 = CatalogItem10;

		if (bundlesItem.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			spinPrice[0].text = spinPrice[1].text = bundlesItem.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString() + " vàng";
		} else {
			spinPrice[0].text = spinPrice[1].text  = bundlesItem.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString() + " kim cương";
		}

		if (bundlesItem10.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			spinPrice[1].text = spinPrice[1].text = bundlesItem10.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString() + "vàng";
		} else {
			spinPrice[1].text = spinPrice[1].text  = bundlesItem10.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString() + " kim cương";
		}

		SpinResult infoItem = HorusSimpleJson.DeserializeObject<SpinResult>(bundlesItem.CustomData);

		for (int i = 0; i < 12; i++) {
			itemIconNew [i].gameObject.SetActive (false);
			itemIconEffect[i].gameObject.SetActive (false);

			string idModel = infoItem.items ["slot_" + (i+1)].ItemId;

			if (idModel.Contains (GameConstants.VNITEMPREFIX_3Day)) {
				idModel = idModel.Replace (GameConstants.VNITEMPREFIX_3Day, GameConstants.VNITEMPREFIX_Empty);
				itemDayReward [i].text = "3 ngày";
			} 
			else if (idModel.Contains (GameConstants.VNITEMPREFIX_7Day)) {
				idModel = idModel.Replace (GameConstants.VNITEMPREFIX_7Day, GameConstants.VNITEMPREFIX_Empty);
				itemDayReward [i].text = "7 ngày";
			}
			else if (idModel.Contains (GameConstants.VNITEMPREFIX_30Day)) {
				idModel = idModel.Replace (GameConstants.VNITEMPREFIX_30Day,  GameConstants.VNITEMPREFIX_Empty);
				itemDayReward [i].text = "30 ngày";
			}else {
				itemDayReward [i].text = "Vĩnh viễn";
				itemIconNew [i].gameObject.SetActive (true);
				itemIconEffect[i].gameObject.SetActive (true);
			}

			itemIconReward[i].sprite = Resources.Load<Sprite>("Sprites/UIWeapon/" + idModel);
			itemIconReward [i].SetNativeSize ();
			if (idModel.Contains (GameConstants.VNITEMID_Coin) || idModel.Contains (GameConstants.VNITEMID_Gold)) {
				itemDayReward [i].text = infoItem.items ["slot_" + (i + 1)].number.min + " - " + infoItem.items ["slot_" + (i + 1)].number.max;
				itemIconReward [i].transform.localScale = Vector3.one;
			} else {
				itemIconReward [i].transform.localScale = new Vector3(0.35f,0.35f,0.35f);
			}

			backgroundIconReward [i].name = idModel;
		}
	}
		
	public void SpinOneOne()
    {		
		if (bundlesItem.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			if (AccountManager.instance.gameCoinAmount - int.Parse(bundlesItem.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString()) < 0) {
				ErrorView.instance.ShowPopupError ("Bạn không đủ bạc để quay số ");
				return;
			}
		} else {
			if (AccountManager.instance.gameDollarAmount - int.Parse(bundlesItem.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString()) < 0) {
				ErrorView.instance.ShowPopupError ("Bạn không đủ vàng để quay số ");
				return;
			}
		}

		itemID = "";
		StartCoroutine ("SpinOne");
		OnFirstPurchaseGunItemV3 (bundlesItem);
	}
	public void SpinOneTen()
	{		
		if (bundlesItem10.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			if (AccountManager.instance.gameCoinAmount - int.Parse(bundlesItem10.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString()) < 0) {
				ErrorView.instance.ShowPopupError ("Bạn không đủ bạc để quay số ");
				return;
			}
		} else {
			if (AccountManager.instance.gameDollarAmount - int.Parse(bundlesItem10.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString()) < 0) {
				ErrorView.instance.ShowPopupError ("Bạn không đủ vàng để quay số ");
				return;
			}
		}

		itemID = "";
		StartCoroutine ("SpinOne");
		OnFirstPurchaseGunItemV3 (bundlesItem10);
	}
	IEnumerator SpinOne(){
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 12; j++) {
				backgroundIconReward [j].isOn = true;
				AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
				yield return new WaitForSeconds (0.01f);
			}
		}
			
		for(int j = 0; j < 12; j++){
			backgroundIconReward [j].isOn = true;
			if (backgroundIconReward [j].name == itemID) {
				if (newItem != null) {
					anim.SetTrigger ("Effect");
					AudioManager.instance.audio.PlayOneShot (AudioManager.instance.spin_Reward);
					yield return new WaitForSeconds (1f);
					CSVR_UIPopupConfirmShop.instance.ShowPopUpGrandItem (newItem);
				}
				break;
			}
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
			yield return new WaitForSeconds (0.5f);
		}	
		TopBar.instance.UpdateVirtualCurrency();
	}

	private void OnFirstPurchaseGunItemV3(CatalogItem item) {

		string moneyUse = GameConstants.VNPRICE_Coin;
		if (item.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			moneyUse = GameConstants.VNPRICE_Coin;
			AccountManager.instance.gameCoinAmount -= int.Parse(item.VirtualCurrencyPrices[GameConstants.VNPRICE_Coin].ToString());

		} else {
			moneyUse = GameConstants.VNPRICE_Gold;
			AccountManager.instance.gameDollarAmount -= int.Parse(item.VirtualCurrencyPrices[GameConstants.VNPRICE_Gold].ToString());
		}
		TopBar.instance.UpdateVirtualCurrency();
		PurchaseItemRequest request = new PurchaseItemRequest () {
			Purchases = new List<ItemPurchase> () {
				new ItemPurchase () {
					ItemId = item.ItemId,					
					moneyUse = moneyUse,
					RemainingUses = 1,
					UseOnBuy = 1
				}
			}
		};

		HorusClientAPI.PurchaseItem(request, 
			(result) => {
				//Yo.JsonLog("json",result);
				if(result.ItemPurchase.Count > 1){
					CSVR_UIRewardItem.instance.Open();
					CSVR_UIRewardItem.instance.ResetListItemReward();
					for (int i = 0; i < result.ItemPurchase.Count; i++) {
						if(result.ItemPurchase [i].CatalogVersion != "BundleGiftCode" && (int)result.ItemPurchase [i].RemainingUses != 0){
							CSVR_UIRewardItem.instance.RewardItem(i,result.ItemPurchase [i]);
						}
					}
					CSVR_UIRewardItem.instance.UI_OpenCSVR_UIRewardItem_Click();
					newItem = result.ItemPurchase [result.ItemPurchase.Count - 1];
				}else{
					for (int i = 0; i < result.ItemPurchase.Count; i++) {
						if(result.ItemPurchase [i].CatalogVersion != "bundle" && result.ItemPurchase [i].CatalogVersion != GameConstants.VNITEMCATALOG_BundleSpin && (int)result.ItemPurchase [i].RemainingUses != 0){
							Debug.Log (result.ItemPurchase [i].ItemId);

							if(result.ItemPurchase [i].ItemId == GameConstants.VNITEMID_Coin){
								AccountManager.instance.gameCoinAmount = (int)result.ItemPurchase [i].RemainingUses;
								itemID = result.ItemPurchase [i].ItemIdModel;
								newItem = null;
							} else if(result.ItemPurchase [i].ItemId == GameConstants.VNITEMID_Gold){
								AccountManager.instance.gameDollarAmount = (int)result.ItemPurchase [i].RemainingUses;
								itemID = result.ItemPurchase [i].ItemIdModel;
								newItem = null;
							} else{

								AccountManager.instance.itemsNew.Add (result.ItemPurchase [i]);
								AccountManager.instance.inventory.Add (result.ItemPurchase [i]);

								CSVR_UIInventory.instance.InstanceItemPurchase (result.ItemPurchase [i]);
								CSVR_UIInventory.instance.ShowNotificationPurchase(result.ItemPurchase[i],true);

								itemID = result.ItemPurchase [i].ItemIdModel;
								newItem = result.ItemPurchase [i];
							}
						}
					}
				}
					
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Quay số không thành công");
			}
		);
	}

}
