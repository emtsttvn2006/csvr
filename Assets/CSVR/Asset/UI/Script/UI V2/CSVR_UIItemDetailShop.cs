﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Horus.Json;
using Horus.ClientModels;

public class CSVR_UIItemDetailShop : MonoBehaviour {
	public Text NameText = null;
	public Text DamageText = null;
	public Image IconImage = null;
	public Text CustomText = null;
	public Text CustomText2 = null;
	public Text AccuracyText = null;
	public Text APText = null;
	public Text RecoilText = null;
	public Text ReloadText = null;
	public Text ROFText = null;
	public Text CapacityText = null;
	public Text WeightText = null;
	public Text RequireText = null;

	//Event Group
	public Button By7Button = null;
	public Image IconBy7Button = null;
	public Text PricesBy7Button = null;
	public Button By30Button = null;
	public Image IconBy30Button = null;
	public Text PricesBy30Button = null;
	public Button ByButton = null;
	public Image IconByButton = null;
	public Text PricesByButton = null;
	[Space(5)]
	public GameObject right_Pnl;
	public GameObject rightDetail_Pnl;
	public GameObject info_grp;
	[Space(5)]
	public Sprite[] coins;

	public void GetInfoGun(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
		//Debug.Log (item.CustomData);//chua chuyen sang tring
		for (int i = 0; i < 9; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(true);
		}
		
		NameText.text = item.DisplayName;
		CustomText.text = "Chính xác";
		CustomText2.text = "Độ giật";
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		CSVR_GunInfo infoGun = HorusSimpleJson.DeserializeObject<CSVR_GunInfo>(item.CustomData);
		//dame
		DamageText.text = infoGun.G1.ToString();
		
		//accuracy
		float m_min = infoGun.A1;
		float m_max = infoGun.A2;
		int m_AC = (int)(10*(10-(5*m_min+m_max)/6));
		AccuracyText.text = m_AC.ToString();

		//ap
		APText.text = infoGun.G6.ToString();
		
		//recoil
		float m_R1 = Mathf.Abs(infoGun.R1);
		float m_R2X = Mathf.Abs(infoGun.R2X);
		int m_RC = (int)((1-m_R1*m_R2X/5)*100);
		RecoilText.text = m_RC.ToString();
		
		//reload
		ReloadText.text = infoGun.G3+ " s";
		
		//rate of fire
		float m_ROFText = (int)(60/float.Parse(infoGun.G2.ToString()));
		ROFText.text = m_ROFText+" RPM";
		
		//capacity
		CapacityText.text = infoGun.G4.ToString();
		
		//weight
		WeightText.text = infoGun.G5 +"Kg";

		//require
		if (item.required != null) {
			info_grp.transform.GetChild (8).gameObject.SetActive (true);
			string require = string.Empty;
			if (item.required.level != null) {
				require += "Cấp " + item.required.level;
			}
			if (item.required.role != null) {
				require +=" "+ item.required.role [0];
			}
			RequireText.text = require;
		} else {
			info_grp.transform.GetChild(8).gameObject.SetActive(false);
		}

		SetUpButtonEvent (item07, item30, item);
	}

	public void GetInfoMelee(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
//        switch (item.ItemClass)
//        {
//            case "Melee.Weapon":
//                ControlWeapon.instance.SetStateAnim("Knife");
//                break;
//        }
		for (int i = 0; i < 9; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}
		info_grp.transform.GetChild(0).gameObject.SetActive(true);
		info_grp.transform.GetChild(1).gameObject.SetActive(true);
		info_grp.transform.GetChild(2).gameObject.SetActive(true);
		NameText.text = item.DisplayName;
		CustomText.text = "Khoảng cách";
		CustomText2.text = "Xuyên giáp";
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		CSVR_MeleInfo infoMelee = HorusSimpleJson.DeserializeObject<CSVR_MeleInfo>(item.CustomData);

		//Damages
		DamageText.text = infoMelee.Damge.ToString();
		//Ranges
		AccuracyText.text = infoMelee.Ranges.ToString();
		
		//A.p
		RecoilText.text = infoMelee.AP.ToString();

		if (item.required != null) {
			info_grp.transform.GetChild(8).gameObject.SetActive(true);
			string require = string.Empty;
			if (item.required.level != null) {
				require += "Cấp " + item.required.level;
			}
			if (item.required.role != null) {
				require +=" "+ item.required.role[0];
			}
			//require
			RequireText.text = require;
		}else {
			info_grp.transform.GetChild(8).gameObject.SetActive(false);
		}
		
		SetUpButtonEvent (item07, item30, item);
	}
	public void GetInfoGrenade(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
		for (int i = 0; i < 9; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}
		info_grp.transform.GetChild(0).gameObject.SetActive(true);
		info_grp.transform.GetChild(1).gameObject.SetActive(true);
		info_grp.transform.GetChild(2).gameObject.SetActive(true);
		NameText.text = item.DisplayName;
		CustomText.text = "Khoảng cách";
		CustomText2.text = "Xuyên giáp";
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		CSVR_GrenadeInfo infoGrenadeInfo = HorusSimpleJson.DeserializeObject<CSVR_GrenadeInfo>(item.CustomData);
		//Damages
		DamageText.text = infoGrenadeInfo.Damge.ToString();
		//Ranges
		
		AccuracyText.text = infoGrenadeInfo.Ranges.ToString();
		
		//A.p
		RecoilText.text = infoGrenadeInfo.AP.ToString();
		if (item.required != null) {
			info_grp.transform.GetChild(8).gameObject.SetActive(true);
			string require = string.Empty;
			if (item.required.level != null) {
				require += "Cấp " + item.required.level;
			}
			if (item.required.role != null) {
				require +=" "+ item.required.role[0];
			}
			//require
			RequireText.text = require;
		}

		SetUpButtonEvent (item07, item30, item);
	}
	public void GetInfoArmor(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
		for (int i = 0; i < 9; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}
		info_grp.transform.GetChild(1).gameObject.SetActive(true);
		info_grp.transform.GetChild(2).gameObject.SetActive(true);
		NameText.text = item.DisplayName;
		CustomText.text = "Độ bền";
		CustomText2.text = "Chống đỡ";
		//IconImage.sprite = CSVR_AssetManager.itemIconDic [item.ItemId];
		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		CSVR_ArmorInfo infoArmorInfo = HorusSimpleJson.DeserializeObject<CSVR_ArmorInfo>(item.CustomData);
		//Reliability
		AccuracyText.text = infoArmorInfo.Reliability.ToString();

		//Resilience
		RecoilText.text = infoArmorInfo.Resilience.ToString();
		if (item.required != null) {
			info_grp.transform.GetChild(8).gameObject.SetActive(true);
			string require = string.Empty;
			if (item.required.level != null) {
				require += "Cấp " + item.required.level;
			}
			if (item.required.role != null) {
				require +=" "+ item.required.role[0];
			}
			//require
			RequireText.text = require;
		}

		SetUpButtonEvent (item07, item30, item);
	}


	public void GetInfoAccessories(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
		//Debug.Log (item.CustomData);//chua chuyen sang tring
		for (int i = 0; i < 9; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}

		NameText.text = item.DisplayName;
		CustomText.text = "Chính xác";
		CustomText2.text = "Độ giật";

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);

		CSVR_AccessoriInfo infoGun = HorusSimpleJson.DeserializeObject<CSVR_AccessoriInfo>(item.CustomData);
		//dame
		if (infoGun.G1 != null) {
			info_grp.transform.GetChild(0).gameObject.SetActive(true);
			DamageText.text = infoGun.G1.action + " " + infoGun.G1.value;
		}
//
		//accuracy
		if ( infoGun.A1 != null && infoGun.A2 != null) {
			info_grp.transform.GetChild(1).gameObject.SetActive(true);
			float m_min = float.Parse(infoGun.A1.value);
			float m_max =  float.Parse(infoGun.A2.value);
			int m_AC = (int)(100 - (10 * (10 - (5 * m_min + m_max) / 6)));
			if (m_AC > 0) {
				AccuracyText.text = "+ " + m_AC;
			} else {
				AccuracyText.text = "- " + System.Math.Abs(m_AC);
			}
		}
		//ap
		if (infoGun.G6 != null) {
			info_grp.transform.GetChild(7).gameObject.SetActive(true);
			APText.text = infoGun.G6.action + " " + infoGun.G6.value;
		}

		//recoil
		if (infoGun.R1 != null && infoGun.R2X != null) {
			info_grp.transform.GetChild(2).gameObject.SetActive(true);
			float m_R1 = Mathf.Abs ( float.Parse(infoGun.R1.value));
			float m_R2X = Mathf.Abs ( float.Parse(infoGun.R2X.value));
			int m_RC = (int)(100 - ((1 - m_R1 * m_R2X / 5) * 100));
			if (m_RC > 0) {
				RecoilText.text = "+ " + m_RC;
			} else {
				RecoilText.text = "- " + System.Math.Abs(m_RC);
			}
		}
		//reload
		if (infoGun.G3 != null) {
			info_grp.transform.GetChild(3).gameObject.SetActive(true);
			ReloadText.text = infoGun.G3.action + " " + infoGun.G3.value + " s";
		}

		//rate of fire
		if (infoGun.G2 != null) {
			info_grp.transform.GetChild(4).gameObject.SetActive(true);
			float m_ROFText = (int)(60 / float.Parse (infoGun.G2.value));
			ROFText.text = infoGun.G2.action + " " +  m_ROFText + " RPM";
		}
		//capacity
		if (infoGun.G4 != null) {
			info_grp.transform.GetChild(5).gameObject.SetActive(true);
			CapacityText.text = infoGun.G4.value;
		}
		//weight
		if (infoGun.G5 != null) {
			info_grp.transform.GetChild (6).gameObject.SetActive (true);
			WeightText.text = infoGun.G5.action + " " +  infoGun.G5.value + "Kg";
		}

		//require
		if (item.required != null) {
			info_grp.transform.GetChild(8).gameObject.SetActive(true);
			string require = string.Empty;
			if (item.required.level != null) {
				require += "Cấp " + item.required.level;
			}
			if (item.required.role != null) {
				require +=" "+ item.required.role[0];
			}
			RequireText.text = require;
		}

		SetUpButtonEvent (item07, item30, item);
	}


	public void GetInfoCharacter(CatalogItem item07,CatalogItem item30,CatalogItem item)
	{
		//Debug.Log (item.CustomData);//chua chuyen sang tring
		for (int i = 0; i < 9; i++) {
			info_grp.transform.GetChild(i).gameObject.SetActive(false);
		}

		NameText.text = item.DisplayName;

		IconImage.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		//require
		if (item.required != null) {
			info_grp.transform.GetChild(8).gameObject.SetActive(true);
			string require = string.Empty;
			if (item.required.level != null) {
				require += "Cấp " + item.required.level;
			}
			if (item.required.role != null) {
				require +=" "+ item.required.role[0];
			}
			RequireText.text = require;
		}

		SetUpButtonEvent (item07, item30, item);
	}

	Color GDColor = new Color(1,0.73f,0,1);
	private void SetUpButtonEvent(CatalogItem item07,CatalogItem item30,CatalogItem item){
		//switch pnl
		right_Pnl.gameObject.SetActive (false);
		rightDetail_Pnl.gameObject.SetActive (true);

		if (item07 != null) {
			IconBy7Button.gameObject.SetActive (true);
			if (item07.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
				PricesBy7Button.text = item07.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
				PricesBy7Button.color = Color.white;
				IconBy7Button.sprite = coins [0];
			} else {
				PricesBy7Button.text = item07.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
				PricesBy7Button.color = GDColor;
				IconBy7Button.sprite = coins [1];
			}
			Button b07 = By7Button; 
			AddListenerBuyItem (b07, item07);
		} else {
			IconBy7Button.gameObject.SetActive (false);
			PricesBy7Button.text = "sắp bán";
		}

		if (item30 != null) {
			IconBy30Button.gameObject.SetActive (true);
			if (item30.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
				PricesBy30Button.text = item30.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
				PricesBy30Button.color = Color.white;
				IconBy30Button.sprite = coins [0];
			} else {
				PricesBy30Button.text = item30.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
				PricesBy30Button.color = GDColor;
				IconBy30Button.sprite = coins [1];
			}
			Button b30 = By30Button; 
			AddListenerBuyItem (b30, item30);
		} else {
			IconBy30Button.gameObject.SetActive (false);
			PricesBy30Button.text = "sắp bán";
		}
		
		if (item.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			PricesByButton.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin].ToString ();
			PricesByButton.color = Color.white;
			IconByButton.sprite = coins[0];
		} else {
			PricesByButton.text = item.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold].ToString ();
			PricesByButton.color = GDColor;
			IconByButton.sprite = coins[1];
		}
		Button b = ByButton; 
		AddListenerBuyItem (b, item);
	}

	private void AddListenerBuyItem(Button b,CatalogItem info)
	{
		b.onClick.RemoveAllListeners();
		b.onClick.AddListener(()=> BuyItem(info));
		
	}
	private void BuyItem(CatalogItem info)
	{
		//Debug.Log ("CatalogItem " + info.DisplayName);
		if (info.VirtualCurrencyPrices.ContainsKey (GameConstants.VNPRICE_Coin)) {
			if (AccountManager.instance.gameCoinAmount >= info.VirtualCurrencyPrices [GameConstants.VNPRICE_Coin]){
				CSVR_UIPopupConfirmShop.instance.ShowPopUpPurchase (info);
			}else{
				ErrorView.instance.ShowPopupError ("Không đủ bạc!..");
			}
		} else {
			if (AccountManager.instance.gameDollarAmount >= info.VirtualCurrencyPrices [GameConstants.VNPRICE_Gold]){
				CSVR_UIPopupConfirmShop.instance.ShowPopUpPurchase (info);
			}else{
				ErrorView.instance.ShowPopupError ("Không đủ vàng!..");
			}
		}

	}

}
