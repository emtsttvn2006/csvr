﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_painHUD : MonoBehaviour {

	public float Duration = 2f;
	public Sprite ArrowImage;
	public Color ArrowColor = new Color(0.8f,0,0,1f);
	
	RectTransform arrowRect;
	float currentEndTime = 0f;
	Image arrowImage;
	Color arrowColor;
	Transform lastSource = null;



	private vp_FPPlayerEventHandler m_Player = null;

	void Awake(){
		m_Player = transform.GetComponent<vp_FPPlayerEventHandler>();
	}

	void Start(){
		GameObject a = CSVR_UIPlayManage.init.ArrowHUD;
		arrowRect = a.GetComponent<RectTransform>();
		arrowImage = a.GetComponentInChildren<Image> ();
		arrowImage.sprite = ArrowImage;
		arrowColor = ArrowColor;
		arrowColor.a = 0;
		arrowImage.color = arrowColor;
	}

	protected virtual void OnEnable(){
		if (m_Player != null)
			m_Player.Register(this);
	}

	protected virtual void OnDisable(){
		if (m_Player != null)
			m_Player.Unregister(this);
	}

	
	protected virtual void OnMessage_HUDDamageFlash(vp_DamageInfo dmg)
	{

		if (dmg == null || dmg.Damage == 0.0f)
		{
			return;
		}

		if(Time.time > currentEndTime)
		{
			currentEndTime = Time.time + Duration;
			StartCoroutine(DrawSprite(dmg));
			lastSource = dmg.Source;
		}
		if(dmg.Source != lastSource)
		{
			currentEndTime = Time.time + Duration;
			StopAllCoroutines();
			StartCoroutine(DrawSprite(dmg));
			lastSource = dmg.Source;
		}
		else
			currentEndTime = Time.time + Duration;
	}


	IEnumerator DrawSprite(vp_DamageInfo damageInfo)
	{

		while(Time.time < currentEndTime)
		{
			yield return new WaitForSeconds(0.1f);
			if (damageInfo.Source == null)
			{
				arrowColor.a = 0f;
				arrowImage.color = arrowColor;
				StopAllCoroutines ();
			}
			
			try
			{
				arrowRect.rotation = Quaternion.Euler(new Vector3(0,0,-(vp_3DUtility.LookAtAngleHorizontal(
					transform.position,
					transform.forward,
					damageInfo.Source.position)
						+ ((damageInfo.Source != transform) ? 0 : 90))));	// if damaging self, point straight down

				arrowColor.a = (currentEndTime - Time.time)/Duration;
				arrowImage.color = arrowColor;
			}
			catch
			{
				
			}
			yield return null;
	
		}
	}

}
