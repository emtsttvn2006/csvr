﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus.ClientModels;
using Horus.Json;


public class CSVR_UIMultiplayerLobby : SingletonBaseScreen<CSVR_UIMultiplayerLobby>
{

	[SerializeField]
	GameObject findingMap;

	[SerializeField]
	GameObject modeParent;
	[SerializeField]
	GameObject mode_Item;

	[SerializeField]
	GameObject parent_ModeContent;
	[SerializeField]
	GameObject parent_ContentMapItem;
	[SerializeField]
	GameObject map_Item;

	[SerializeField]
	Sprite[] modeImage;

	public LobbyResultV2 lobbyResult;
	public List<ItemModeV2> ListModeAvailable = null;
    //string json = "{\"ModeAvaiable\":{\"CSVR_RandomMode\":\"Ngẫu nhiên\",\"CSVR_TeamDeathmatch\":\"Đấu Đội\",\"CSVR_Deathmatch\":\"Đấu Đơn\",\"CSVR_DeadOrAlive\":\"Sinh Tử\",\"CSVR_SearchAndDestroy\":\"Đặt Bomb\",\"CSVR_Zoombie\":\"Bắn Zombie\",\"CSVR_Flag\":\"Đoạt Cờ\",\"CSVR_BaseCapture\":\"Cứ Điểm\"},\"MapAvaiable\":{\"Map_Random\":\"Ngẫu Nhiên\",\"Map_Ngatu\":\"Ngã Tư Tử Thần\",\"Map_Thuydien\":\"Thủy Điện\",\"Map_Tauhang\":\"Tàu Chở Hàng\",\"Map_Thinghiem\":\"Viện Nghiên Cứu\",\"Map_Italy\":\"Italy\",\"Map_Dust\":\"Sa Mạc\",\"Map_Train\":\"Nhà Ga\",\"Map_Dust2\":\"Sa Mạc 2\"},\"Lobby\":[{\"ModeName\":\"CSVR_RandomMode\",\"Interactable\":true,\"Maps\":[\"Map_Random\"]},{\"ModeName\":\"CSVR_TeamDeathmatch\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Deathmatch\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_DeadOrAlive\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_SearchAndDestroy\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Zoombie\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Flag\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_BaseCapture\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]}]}";
    //string json = "{\"ModeAvaiable\":{\"CSVR_RandomMode\":\"Ngẫu nhiên\",\"CSVR_TeamDeathmatch\":\"Đấu Đội\",\"CSVR_Deathmatch\":\"Đấu Đơn\",\"CSVR_DeadOrAlive\":\"Sinh Tử\",\"CSVR_SearchAndDestroy\":\"Đặt Bomb\",\"CSVR_Zoombie\":\"Bắn Zombie\",\"CSVR_Flag\":\"Đoạt Cờ\",\"CSVR_BaseCapture\":\"Cứ Điểm\",\"CSVR_ZombieSurvival\":\"Zombie Sinh tồn\"},\"MapAvaiable\":{\"Map_Random\":\"Ngẫu Nhiên\",\"Map_Ngatu\":\"Ngã Tư Tử Thần\",\"Map_Thuydien\":\"Thủy Điện\",\"Map_Tauhang\":\"Tàu Chở Hàng\",\"Map_Thinghiem\":\"Viện Nghiên Cứu\",\"Map_Italy\":\"Italy\",\"Map_Dust\":\"Sa Mạc\",\"Map_Train\":\"Nhà Ga\",\"Map_Dust2\":\"Sa Mạc 2\",\"Map_NV_1_Station\":\"Nhà Ga\"},\"Lobby\":[{\"ModeName\":\"CSVR_RandomMode\",\"Interactable\":true,\"Maps\":[\"Map_Random\"]},{\"ModeName\":\"CSVR_TeamDeathmatch\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\"]},{\"ModeName\":\"CSVR_Deathmatch\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_DeadOrAlive\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_SearchAndDestroy\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Zoombie\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Flag\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_BaseCapture\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_ZombieSurvival\",\"Interactable\":true,\"Maps\":[\"Map_NV_1_Station\"]}]}";

    public Toggle ToggleTutmode = null;
    public Toggle ToggleTutmodecontent = null;
    public Button ButtonXuatkich;

    public override void OnEnable()
	{
		TopBar.instance.SetBackButton(new UnityAction(() =>{
			CSVR_UIMultiplayerLobby.instance.Close();

			CharacterView.instance.Open();
			MainView.instance.Open();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			CSVR_UIMultiplayerLobby.instance.Close();
			CharacterView.instance.Open();
			CSVR_UIShop.instance.Open();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));
		TopBar.instance.ShopOrInVText.text = "Cửa hàng";
	}

	void Start()
	{
		lobbyResult = HomeScreen.instance.lobbyResult;

		ListModeAvailable = new List<ItemModeV2> ();

		//show hide mode map avaialbe
		//string json = HorusManager.instance.GetTitleValue("ModeMapAvailable");
		//Debug.Log (json);

		bool modeContentOn = true;

		for (int i = 0,lenght = lobbyResult.Lobby.Count; i<lenght; i++) {
			//lay ten mode
			string modeName = lobbyResult.Lobby [i].ModeName;            
            //CSVR_TeamDeathmatch
            //khoi tao mode
            GameObject mode = Instantiate (mode_Item);
			mode.transform.SetParent (modeParent.transform);
			mode.transform.localScale = Vector3.one;
			mode.name = modeName;

			//anh map
			try
			{
				mode.transform.GetChild(1).GetComponent<Image>().sprite = HomeScreen.instance.modeMapSprites[HomeScreen.instance.modeMapSpritesIndex[modeName]];
			}
			catch
			{
			}
			//ten map
			mode.transform.GetChild(2).GetComponent<Text>().text = lobbyResult.ModeAvaiable[modeName];

			RectTransform rectTransformMode = mode.GetComponent<RectTransform>();

			rectTransformMode.anchorMin =  new Vector2(0, 0);
			rectTransformMode.anchorMax = new Vector2(1, 1);
			rectTransformMode.offsetMin  = Vector3.zero;
			rectTransformMode.offsetMax  = Vector3.zero;

			Toggle itemToggle = mode.transform.GetComponent<Toggle>();            

            itemToggle.interactable = lobbyResult.Lobby[i].Interactable;
	
			if(itemToggle.interactable){
				ListModeAvailable.Add (lobbyResult.Lobby [i]);

				//khoi tao mode content de chua map
				GameObject mode_Content = Instantiate (parent_ContentMapItem);
				mode_Content.transform.SetParent (parent_ModeContent.transform);
				mode_Content.transform.localScale = Vector3.one;
				mode_Content.name = modeName;
			
				RectTransform rectTransform = mode_Content.GetComponent<RectTransform>();
				rectTransform.anchorMin =  new Vector2(0, 0);
				rectTransform.anchorMax = new Vector2(1, 1);
				rectTransform.offsetMin  = Vector3.zero;
				rectTransform.offsetMax  = Vector3.zero;

				if (modeContentOn) {
					itemToggle.isOn = true;
					mode_Content.gameObject.SetActive (true);
					modeContentOn = false;
				}

				//add su kien cho mode
				AddListenerMode (itemToggle, modeName);

				itemToggle.onValueChanged.AddListener((value) =>{
					mode_Content.gameObject.SetActive(value);
				});

				//mode duoc bat
				itemToggle.image.sprite = modeImage[0];
				ToggleGroup toggleGroup = mode_Content.transform.GetChild(0).transform.GetComponent<ToggleGroup>();
				bool isOn = true;
				int j = 0;
				foreach (string m in lobbyResult.Lobby[i].Maps) {
					//khoi tao map
					GameObject map = Instantiate (map_Item);
					map.name = m;
					map.transform.SetParent (toggleGroup.transform);
					map.transform.localScale = Vector3.one;

					try
					{
						//anh map
						map.transform.GetChild(0).GetComponent<Image>().sprite = HomeScreen.instance.modeMapSprites[HomeScreen.instance.modeMapSpritesIndex[m]];
						//ten map
						map.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = lobbyResult.MapAvaiable[m];
					}
					catch
					{
					}


					Toggle mapToggle = map.transform.GetComponent<Toggle>();
					mapToggle.group = toggleGroup;

					//map active
					if (isOn) {
						mapToggle.isOn = true;
						isOn = false;
					}
					//add su kien cho toggle
					AddListenerMap (map.transform.GetComponent<Toggle> (), m);
				}

			}else{
				//mode da tat
				itemToggle.image.sprite = modeImage[1];
			}
		}
			
	}    

    public void RoomIsFinding()
	{
		findingMap.gameObject.SetActive(true);
	}

	public void RoomEndFinding()
	{
		findingMap.gameObject.SetActive(false);
	}

	public void UI_RoomIsFindingClose_Click()
	{
		CSVR_MPConnection.instance.LeaveRoom();

		findingMap.gameObject.SetActive(false);
	}

	public void OnRoomPropertiesChangedQuick(RoomInfo roomInfo)
	{
		if (PhotonNetwork.player.isMasterClient)
		{
			OnConfirmClick();
		}
	}

	public void OnConfirmClick()
	{
		CSVR_MPConnection.instance.SetReady();

	}

	public void MapCommingSoon(){
		ErrorView.instance.ShowPopupError ("Bản đồ sắp ra mắt! \nChúc bạn chơi game vui vẻ");
	}

	//tao room
	public void UI_OnCreateRoom_Click()
	{
		HomeScreen.instance.SetMatchRoomScreenCallBack (UI_MatchRoomScreen_CallBack);
		CSVR_UICreateRoomView.instance.Open ();
	}
	public void UI_MatchRoomScreen_CallBack()
	{
		CSVR_UIMultiplayerLobby.instance.Close ();
		CSVR_UICreateRoomView.instance.Close ();
		ErrorView.instance.Loading (false);
		TopBar.instance.SetBackButton(new UnityAction(() =>{
			CSVR_UIMultiplayerLobby.instance.Open();
			CSVR_UIMatchRoomScreen.instance.Close();
			CSVR_MPConnection.instance.LeaveRoom();
		}));   
	}

	public void UI_OnJoinRoomMenu_Click()
	{
		if (CSVR_GameSetting.modeName != "") {
			
			if ( CSVR_GameSetting.mapName != "") {
				HomeScreen.instance.SetLoadingMapCallBack (UI_LoadingMap_CallBack);
				RoomIsFinding ();

				CSVR_MPConnection.instance.OnQuickJoinedRoom(true);
				CSVR_MPConnection.instance.JoinRoom ();

				CSVR_GameSetting.TuTaoPhong = false;
				CSVR_GameSetting.maxPlayer = 11;
				CSVR_GameSetting.maxSkill = 50;
				CSVR_GameSetting.timeMatch = 300;
			}
		}
	}

	private void UI_LoadingMap_CallBack(){
		CSVR_UIMultiplayerLobby.instance.Close ();
		TopBar.instance.Close ();
	}

	private void AddListenerMode(Toggle u,string modeName)
	{
		u.onValueChanged.RemoveAllListeners();
		u.onValueChanged.AddListener((value) =>{
			if(value == true){
				if(modeName == GameConstants.VNMODE_Random){
					CSVR_GameSetting.modeName = GetModeRandom();
				}else{
					CSVR_GameSetting.modeName = modeName;
				}
			}
		});

	}
	private string GetModeRandom()
	{
		string modeRandom = ListModeAvailable [Random.Range (1, ListModeAvailable.Count)].ModeName;
		return modeRandom;
	}

	private void AddListenerMap(Toggle u,string mapName)
	{
		u.onValueChanged.RemoveAllListeners();
		u.onValueChanged.AddListener((value) =>{
			if(value == true){
				if(mapName == GameConstants.VNMAP_Random){
					CSVR_GameSetting.mapName = GetMapRandomInMode();
				}else{
					CSVR_GameSetting.mapName = mapName;
				}
			}
		});

	}
	private string GetMapRandomInMode()
	{
		string[] maps = lobbyResult.Lobby.Find(x => x.ModeName.Contains(CSVR_GameSetting.modeName)).Maps;
		string mapRandom = maps [Random.Range (1, maps.Length)];
		return mapRandom;
	}

}
