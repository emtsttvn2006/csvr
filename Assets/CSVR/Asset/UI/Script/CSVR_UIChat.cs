﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using MarchingBytes;
using Horus;
using Horus.ClientModels;
using LitJson;

public class CSVR_UIChat : SingletonBaseScreen<CSVR_UIChat>
{
    public float LineText_In_UI = 32.5f;
    public int Max_Letters_In_String = 33;

    public GameObject ContentHolderGlobal;
    public GameObject ContentHolderBanbe;
    public GameObject ContentHolderClan;
    public GameObject ContentHolderPrivate;

    public LoopVerticalScrollRect scrollUserGlobal;
    public LoopVerticalScrollRect scrollContentGlobal;
    public LoopVerticalScrollRect scrollUserBanbe;
    public LoopVerticalScrollRect scrollContentBanbe;
    public LoopVerticalScrollRect scrollUserClan;
    public LoopVerticalScrollRect scrollContentClan;    
    public LoopVerticalScrollRect scrollUserPrivate;
    public LoopVerticalScrollRect scrollContentPrivate;    

    public Toggle ToggleGlobal;
    public Toggle ToggleBanbe;
    public Toggle ToggleClan;
    public Toggle TogglePrivate;
    private List<Toggle> MenuToggles = new List<Toggle>();

    public Image NotiGlobal;
    public Image NotiBanbe;
    public Image NotiClan;
    public Image NotiPrivate;

    public string NameChosenBanbe = string.Empty;
    public string NameChosenPrivate = string.Empty;

    public Toggle AutoScrolling;

    public Dropdown DropdownSapxep;
    //room chat
    public InputField messageInput;    

    [HideInInspector]
    public List<RelationshipInfo> listRelationshipInfo = new List<RelationshipInfo>();

    [HideInInspector]
    public List<RelationshipInfo> listBanBe = new List<RelationshipInfo>();
    [HideInInspector]
    public List<RelationshipInfo> listWaiting = new List<RelationshipInfo>();
    [HideInInspector]
    public List<RelationshipInfo> listChan = new List<RelationshipInfo>();

    private const int maxTimePollPhotonFriend = 10;
    private int localTimePollPhotonFriend = 0;

    public string ChosenFriend = string.Empty;

    void Start()
    {
        Init();        
    }

    public void Init()
    {
        MenuToggles.Add(ToggleGlobal);
        MenuToggles.Add(ToggleBanbe);
        MenuToggles.Add(ToggleClan);
        MenuToggles.Add(TogglePrivate);
    }

    public void ResetToggle()
    {
        if (ToggleGlobal.isOn)
        {
            NotiGlobal.gameObject.SetActive(false);
        }
        ContentHolderGlobal.gameObject.SetActive(ToggleGlobal.isOn);

        if (ToggleBanbe.isOn)
        {
            NotiBanbe.gameObject.SetActive(false);
            int IndexChosenBanbe = ChatCache.instance.BanbeCaches.GetUserIndex(NameChosenBanbe);            
            UpdateBanbeChatContent();
        }
        ContentHolderBanbe.gameObject.SetActive(ToggleBanbe.isOn);

        if (ToggleClan.isOn)
        {
            NotiClan.gameObject.SetActive(false);

            if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "") {
                ClanSearchRequest ClanSearchRequest = new ClanSearchRequest();
                ClanSearchRequest.roleId = HorusManager.instance.Clan.clanName;

                CSVR_UIWaitingIcon.instance.Open();

                HorusClientAPI.ClanSearchProcess(ClanSearchRequest,
                    (result) =>
                    {
                        CSVR_UIWaitingIcon.instance.Close();

                        if (result.Clan.Count > 0)
                        {
                            ClanResult ClanResult = result.GetClan(HorusManager.instance.Clan.clanName);
                            ClanResult.UpdateAllMemberArray();                            
                            ChatCache.instance.UpdateClanUserInfo(ClanResult);
                        }
                        else
                        {
                            ErrorView.instance.ShowPopupError("Không tìm thấy Clan này (0)");
                        }
                    },
                    (error) =>
                    {
                        CSVR_UIWaitingIcon.instance.Close();

                        ErrorView.instance.ShowPopupError("Không tìm thấy Clan này");
                    }, null);
            }
        }
        ContentHolderClan.gameObject.SetActive(ToggleClan.isOn);

        if (TogglePrivate.isOn)
        {
            NotiPrivate.gameObject.SetActive(false);            
            UpdatePrivateChatContent();
        }
        ContentHolderPrivate.gameObject.SetActive(TogglePrivate.isOn);
    }    

    public override void Open()
    {
        base.Open();

        if (CSVR_PlayerMessenger.instance.IsConnected() == false)
        {
            if (DebugError) Debug.Log("Đang cố gắng đăng nhập lại Chat");
            CSVR_PlayerMessenger.instance.Connect();
        }

        if (!scrollUserGlobal.prefabPool) scrollUserGlobal.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollContentGlobal.prefabPool) scrollContentGlobal.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollUserBanbe.prefabPool) scrollUserBanbe.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollContentBanbe.prefabPool) scrollContentBanbe.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollUserClan.prefabPool) scrollUserClan.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollContentClan.prefabPool) scrollContentClan.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollUserPrivate.prefabPool) scrollUserPrivate.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        if (!scrollContentPrivate.prefabPool) scrollContentPrivate.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();

        CSVR_PlayerMessenger.instance.isChatPanelCalled = true;

        InitBanbe();
    }

    public void InitBanbe()
    {
        CSVR_UIWaitingIcon.instance.Open();
        HorusClientAPI.GetRelationShip(new GetRelationShipRequest(),
                (result) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    this.listRelationshipInfo = result.RelationshipList;

                    UpdateDanhSachBanBe();
                },
                (error) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    if (DebugError) Debug.Log("Loading Danh sach Ban be error");                    
                }, null);
    }

    public void UpdateDanhSachBanBe()
    {
        listBanBe.Clear();
        listWaiting.Clear();
        listChan.Clear();
        foreach (RelationshipInfo ri in this.listRelationshipInfo)
        {
            if (ri.Status == 2)
            {
                listBanBe.Add(ri);
            }
            else if (ri.Status == 1)
            {
                listWaiting.Add(ri);
            }
            else if (ri.Status == -1)
            {
                listChan.Add(ri);
            }
        }

        ClearEmptyDisPlayName(ref listBanBe);
        ClearEmptyDisPlayName(ref listWaiting);
        ClearEmptyDisPlayName(ref listChan);

        for (int i = 0; i < listBanBe.Count; i++)
        {
            if (listBanBe[i].Partner != null) {
                BanbeUser BanbeUser = new BanbeUser(listBanBe[i].PartnerId, "", "", listBanBe[i].Partner.AccountLevel.ToString(), new List<string>(), true);
                ChatCache.instance.BanbeCaches.AddUser(BanbeUser);
            } 
        }

        UpdateBanbeUser();
    }

    public void ClearEmptyDisPlayName(ref List<RelationshipInfo> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].Partner == null || list[i].Partner.DisplayName == "")
            {
                list.RemoveAt(i);
                i--;
            }
        }
    }

    public void InitPrivateChat(string Name)
    {
        ToggleGlobal.isOn = true;
        messageInput.text = "@-" + Name + "-@";
    }

    void PollPhotonFriend_Repeating()
    {
        localTimePollPhotonFriend++;

        if (localTimePollPhotonFriend <= maxTimePollPhotonFriend) return;

        if (!ToggleBanbe.isOn) return;

        PollPhotonFriend();
        localTimePollPhotonFriend = 0;
    }

    void PollPhotonFriend()
    {
        if (DebugError) Debug.Log("Pool Friend Data from Photon");
        if (this.listBanBe.Count > 0)
        {
            string[] BanBeArrayName = new string[this.listBanBe.Count];
            for (int i = 0; i < listBanBe.Count; i++)
            {
                BanBeArrayName[i] = (listBanBe[i].CreateId == HorusManager.instance.playerID) ? listBanBe[i].PartnerId : listBanBe[i].CreateId;
            }
            PhotonNetwork.FindFriends(BanBeArrayName);
        }
    }

    public void UIChat_OnMessage(string sender, string message, string channel)
    {
        if (DebugError) Debug.Log("raw message : " + sender + " " + message + " " + channel);
        if (message == string.Empty || message.ToLower().Contains("read error"))
        {
            return;
        }
        else
        {
            MainView.instance.Noti_Tinnhan.gameObject.SetActive(true);

            if (channel == "#global")
            {
				HomeScreen.instance.TextChat1Line.text = sender + " : " + message;
            }

            ChatCache.instance.AddContent(sender, message, channel);
        }
    }

    public void ButtonChatGlobal_ValueChanged()
    {
        ResetToggle();        
    }

    public void ButtonChatBanbe_ValueChanged()
    {
        /*   
        HorusClientAPI.GetRelationShip(new GetRelationShipRequest(),
            (result) => {
                this.listBanBe = result.RelationshipList;
                for (int i = 0; i < this.listBanBe.Count; i++)
                {
                    if (this.listBanBe[i].Partner == null || this.listBanBe[i].Status != 2)
                    {
                        listBanBe.RemoveAt(i);
                        i--;
                    }
                }
                PollPhotonFriend();
                Invoke("InitChat", 1.0f);
            },
            (error) => { }, null);*/
        ResetToggle();        
    }

    public void ButtonChatClan_ValueChanged()
    {
        ResetToggle();        
    }

    public void ButtonChatPrivate_ValueChanged()
    {
        ResetToggle();        
    }

    private void InitChat()
    {
        if (ToggleGlobal.isOn)
        {
            
        }

        if (ToggleBanbe.isOn)
        {
            
        }
    }

    public bool GetPhotonNetworkIsOnline(int idx)
    {
        if (idx < PhotonNetwork.Friends.Count)
            return PhotonNetwork.Friends[idx].IsOnline;
        else
        {
            PollPhotonFriend();
            return false;
        }
    }

    public bool GetPhotonNetworkIsInRoom(int idx)
    {
        if (idx < PhotonNetwork.Friends.Count)
            return PhotonNetwork.Friends[idx].IsInRoom;
        else
        {
            PollPhotonFriend();
            return false;
        }
    }

    //room chat input
    public void SendChatMessage()
    {
        if (messageInput.text != string.Empty)
        {
            string message = messageInput.text;            
            messageInput.text = "";

            if (ToggleGlobal.isOn)
            {
                if (!message.Contains("@-"))
                {                    
                    CSVR_PlayerMessenger.instance.SendMessage("#global", message);
                    ChatCache.instance.AddContent(CSVR_PlayerMessenger.instance.GlobalUser.Name, JsonMapper.ToJson(CSVR_PlayerMessenger.instance.GlobalUser) + message, "#global");
                    MoveAScrollContent(scrollContentGlobal.transform.GetChild(0).GetComponent<RectTransform>(), JsonMapper.ToJson(CSVR_PlayerMessenger.instance.GlobalUser) + message);
                }
                else
                {
                    int content_begin = message.LastIndexOf("-@");
                    if (content_begin <= 0)
                    {
                        if (DebugError) Debug.Log("Lỗi cú pháp tin nhắn cho User: Không có nội dung");
                    }
                    else
                    {
                        string content = message.Substring(content_begin + 2, message.Length - (content_begin + 2));

                        bool loop = true;
                        while (loop)
                        {
                            int begin = message.IndexOf("@-");
                            if (begin != -1)
                            {
                                int end = message.IndexOf("-@");
                                if (end != -1)
                                {
                                    string receiver = message.Substring(begin + 2, end - (begin + 2));
                                    message = message.Remove(begin, 2 + receiver.Length + 2);                                    
                                    CSVR_PlayerMessenger.instance.SendMessage(receiver, content);

                                    int index = ChatCache.instance.GlobalCaches.GetUserIndex(receiver);
                                    GlobalUser GlobalUser = ChatCache.instance.GlobalCaches.GlobalUsers[index];

                                    int BanbeIndex = ChatCache.instance.BanbeCaches.GetUserIndex(GlobalUser.Name);
                                    if (BanbeIndex == -1)
                                    {
                                        ChatCache.instance.PrivateCaches.AddUser(GlobalUser);
                                        ChatCache.instance.PrivateCaches.AddContent(GlobalUser.Name, GlobalUser.Name + " : " + message);                                        

                                        UpdatePrivateUser();
                                        UpdatePrivateChatContent();

                                        MoveAScrollContent(scrollContentPrivate.transform.GetChild(0).GetComponent<RectTransform>(), GlobalUser.Name + " : " + message);
                                    } else
                                    {
                                        ChatCache.instance.BanbeCaches.AddUser(GlobalUser);
                                        ChatCache.instance.BanbeCaches.AddContent(GlobalUser.Name, GlobalUser.Name + " : " + message);                                        

                                        UpdateBanbeUser();
                                        UpdateBanbeChatContent();

                                        MoveAScrollContent(scrollContentBanbe.transform.GetChild(0).GetComponent<RectTransform>(), GlobalUser.Name + " : " + message);
                                    }                                        
                                    loop = message.Contains("@-");
                                }
                                else if (end <= begin || end <= 0)
                                {
                                    if (DebugError) Debug.Log("Cú pháp tin nhắn cho User lỗi: Không xác định được User");
                                    loop = false;
                                }
                            }
                        }
                    }
                }
            }
            else if (ToggleBanbe.isOn)
            {
                int IndexChosenBanbe = ChatCache.instance.BanbeCaches.GetUserIndex(NameChosenBanbe);
                if (IndexChosenBanbe != -1)
                {                     
                    BanbeUser BanbeUser = ChatCache.instance.BanbeCaches.BanbeUsers[IndexChosenBanbe];
                    GlobalUser GlobalUser = new GlobalUser(BanbeUser);
                    GlobalUser.Name = "u" + GlobalUser.Name;
                    CSVR_PlayerMessenger.instance.SendMessage(GlobalUser.Name.Remove(0,1), CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) + " : " + message);
                    ChatCache.instance.AddContent(GlobalUser.Name, JsonMapper.ToJson(GlobalUser) + CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) + " : " + message, "NOCHANNEL");
                    UpdateBanbeChatContent();

                    MoveAScrollContent(scrollContentBanbe.transform.GetChild(0).GetComponent<RectTransform>(), JsonMapper.ToJson(GlobalUser) + CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) + " : " + message);
                }
            }
            else if (ToggleClan.isOn)
            {
                if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "")
                {
                    CSVR_PlayerMessenger.instance.SendMessage("#" + HorusManager.instance.Clan.clanName, message);
                    ChatCache.instance.AddContent(CSVR_PlayerMessenger.instance.GlobalUser.Name, JsonMapper.ToJson(CSVR_PlayerMessenger.instance.GlobalUser) + message, "#" + HorusManager.instance.Clan);
                    MoveAScrollContent(scrollContentClan.transform.GetChild(0).GetComponent<RectTransform>(), JsonMapper.ToJson(CSVR_PlayerMessenger.instance.GlobalUser) + message);
                } 
            }
            else if (TogglePrivate.isOn)
            {
                int IndexChosenPrivate = ChatCache.instance.PrivateCaches.GetUserIndex(NameChosenPrivate);
                if (IndexChosenPrivate != -1)
                {
                    BanbeUser BanbeUser = ChatCache.instance.PrivateCaches.BanbeUsers[IndexChosenPrivate];
                    GlobalUser GlobalUser = new GlobalUser(BanbeUser);
                    GlobalUser.Name = "u" + GlobalUser.Name;                    
                    CSVR_PlayerMessenger.instance.SendMessage(GlobalUser.Name.Remove(0,1), CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) + " : " + message);
                    ChatCache.instance.AddContent(GlobalUser.Name, JsonMapper.ToJson(GlobalUser) + CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) + " : " + message, "NOCHANNEL");
                    UpdatePrivateChatContent();

                    MoveAScrollContent(scrollContentPrivate.transform.GetChild(0).GetComponent<RectTransform>(), JsonMapper.ToJson(GlobalUser) + CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) + " : " + message);
                }
            }
        }
    }

    public void MoveAScrollContent(RectTransform scroll, string message)
    {
        if (AutoScrolling.isOn)
            scroll.localPosition += new Vector3(0, (message.Length / Max_Letters_In_String + 1) * LineText_In_UI, 0);
    }

    int IndexToggle(Toggle Toggle)
    {
        int index = -1;
        for (int i = 0; i < this.MenuToggles.Count; i++)
        {
            if (this.MenuToggles[i] == Toggle)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public void SetToggleOn(Toggle Toggle)
    {
        Toggle.isOn = true;
    }    

    public void UpdateGlobalUser()
    {        
        scrollUserGlobal.totalCount = ChatCache.instance.GlobalCaches.GlobalUsers.Count;
        scrollUserGlobal.Rebuild(CanvasUpdate.Layout);        
    }

    public void UpdateGlobalChatContent()
    {                    
        scrollContentGlobal.totalCount = ChatCache.instance.GlobalCaches.Contents.Count;
        scrollContentGlobal.Rebuild(CanvasUpdate.Layout);
        if (!ToggleGlobal.isOn) NotiGlobal.gameObject.SetActive(!ChatCache.instance.GlobalCaches.isRead);
    }

    public void UpdateBanbeUser()
    {
        scrollUserBanbe.totalCount = ChatCache.instance.BanbeCaches.BanbeUsers.Count;
        scrollUserBanbe.Rebuild(CanvasUpdate.Layout);
    }

    public void UpdateBanbeChatContent()
    {
        if (this.NameChosenBanbe != string.Empty)
        {
            int IndexChosenBanbe = ChatCache.instance.BanbeCaches.GetUserIndex(NameChosenBanbe);
            scrollContentBanbe.totalCount = ChatCache.instance.BanbeCaches.BanbeUsers[IndexChosenBanbe].contents.Count;
            scrollContentBanbe.Rebuild(CanvasUpdate.Layout);
        } else
        {
            if (ChatCache.instance.BanbeCaches.BanbeUsers.Count > 0)
            {
                NameChosenBanbe = ChatCache.instance.BanbeCaches.BanbeUsers[0].Name;
                UpdateBanbeChatContent();
            }
            else
            {
                scrollContentBanbe.totalCount = 0;
                scrollContentBanbe.Rebuild(CanvasUpdate.Layout);
            }
        }

        if (!ToggleBanbe.isOn) NotiBanbe.gameObject.SetActive(!ChatCache.instance.BanbeCaches.isRead());
    }

    public void UpdateClanUser()
    {
        scrollUserClan.totalCount = ChatCache.instance.ClanCaches.GlobalUsers.Count;
        scrollUserClan.Rebuild(CanvasUpdate.Layout);
    }

    public void UpdateClanChatContent()
    {
        scrollContentClan.totalCount = ChatCache.instance.ClanCaches.Contents.Count;
        scrollContentClan.Rebuild(CanvasUpdate.Layout);
        if (!ToggleClan.isOn) NotiClan.gameObject.SetActive(true);
    }

    public void UpdatePrivateUser()
    {           
        scrollUserPrivate.totalCount = ChatCache.instance.PrivateCaches.BanbeUsers.Count;
        scrollUserPrivate.Rebuild(CanvasUpdate.Layout);        
    }

    public void UpdatePrivateChatContent()
    {
        if (this.NameChosenPrivate != string.Empty)
        {
            int IndexChosenPrivate = ChatCache.instance.PrivateCaches.GetUserIndex(NameChosenPrivate);
            scrollContentPrivate.totalCount = ChatCache.instance.PrivateCaches.BanbeUsers[IndexChosenPrivate].contents.Count;
            scrollContentPrivate.Rebuild(CanvasUpdate.Layout);
        } else
        {
            if (ChatCache.instance.PrivateCaches.BanbeUsers.Count > 0)
            {
                NameChosenPrivate = ChatCache.instance.PrivateCaches.BanbeUsers[0].Name;
                UpdatePrivateChatContent();
            }
            else
            {
                scrollContentPrivate.totalCount = 0;
                scrollContentPrivate.Rebuild(CanvasUpdate.Layout);
            }
        }
        
        if (!TogglePrivate.isOn) NotiPrivate.gameObject.SetActive(!ChatCache.instance.PrivateCaches.isRead());
    }    

    public void AddUserToSend(string Name)
    {
        messageInput.text = messageInput.text.Insert(0, "@-" + Name + "-@");
    }

    public int GetIndexUserNameInInputText(string UserName)
    {        
        int begin = messageInput.text.IndexOf(UserName);
        if (begin == -1)
        {
            return begin;
        } else
        {
            if (begin > 1 
                && messageInput.text[begin - 1] == '-' 
                && messageInput.text[begin - 2] == '@'
                && messageInput.text[begin + UserName.Length] == '-'
                && messageInput.text[begin + UserName.Length + 1] == '@')
            {
                return begin;
            } else
            {
                return -1;
            }
        }
    }

    /// <summary>
    /// Value = 0 Sắp xếp theo tên
    /// Value = 1 Sắp xếp theo level
    /// </summary>
    public void Dropdown_Sapxep_Click()
    {        
        switch (DropdownSapxep.value)
        {
            case 0:
                if (ToggleGlobal.isOn)
                {
                    ChatCache.instance.GlobalCaches.GlobalUsers.Sort(SX_Ten_Comparer);
                }
                else if(ToggleBanbe.isOn)
                {
                    ChatCache.instance.BanbeCaches.BanbeUsers.Sort(SX_Ten_Comparer);
                }
                else if (ToggleClan.isOn)
                {
                    ChatCache.instance.ClanCaches.GlobalUsers.Sort(SX_Ten_Comparer);
                }
                else if (TogglePrivate.isOn)
                {
                    ChatCache.instance.PrivateCaches.BanbeUsers.Sort(SX_Ten_Comparer);
                }
                break;
            case 1:
                if (ToggleGlobal.isOn)
                {
                    ChatCache.instance.GlobalCaches.GlobalUsers.Sort(SX_Level_Comparer);
                }
                else if (ToggleBanbe.isOn)
                {
                    ChatCache.instance.BanbeCaches.BanbeUsers.Sort(SX_Level_Comparer);
                }
                else if (ToggleClan.isOn)
                {
                    ChatCache.instance.ClanCaches.GlobalUsers.Sort(SX_Level_Comparer);
                }
                else if (TogglePrivate.isOn)
                {
                    ChatCache.instance.PrivateCaches.BanbeUsers.Sort(SX_Level_Comparer);
                }
                break;
        }
    }

    int SX_Ten_Comparer(GlobalUser GlobalUser1, GlobalUser GlobalUser2)
    {
        return GlobalUser1.Name.CompareTo(GlobalUser2.Name);
    }

    int SX_Ten_Comparer(BanbeUser BanbeUser1, BanbeUser BanbeUser2)
    {
        return BanbeUser1.Name.CompareTo(BanbeUser2.Name);
    }

    int SX_Level_Comparer(GlobalUser GlobalUser1, GlobalUser GlobalUser2)
    {
        return GlobalUser1.Level.CompareTo(GlobalUser2.Level);
    }

    int SX_Level_Comparer(BanbeUser BanbeUser1, BanbeUser BanbeUser2)
    {
        return BanbeUser1.Level.CompareTo(BanbeUser2.Level);
    }

    public void UI_OnEnable()
    {
        this.gameObject.SetActive(true);
    }

    public void UI_OnDisable()
    {
        this.gameObject.SetActive(false);
    }    
}
