﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using MarchingBytes;
using Horus.ClientModels;
using Horus;

public class CSVR_UIClanList_TaoClan : SingletonBaseScreen<CSVR_UIClanList_TaoClan>
{
    public Image IMGAvatarClan;
    public InputField INPClanName;
    public InputField INPClanDescription;
    public GameObject ContentHolderAvatar;

    public LoopVerticalScrollRect scrollRequirement;
    public LoopHorizontalScrollRect scrollAvatar;

    [HideInInspector] public List<string> listRequirement;    

    public override void Open()
    {
        base.Open();
        this.transform.SetSiblingIndex(CSVR_UICLanList.instance.transform.GetSiblingIndex() + 1);        

        if (this.listRequirement == null || this.listRequirement.Count == 0)
        {
            Init();
        }

        Update_scrollRequire();
    }

    public override void Close()
    {
        base.Close();
        CSVR_UICLanList.instance.Open();
    }

    public void Init()
    {
        this.listRequirement = new List<string>()
        {
            "Cần đạt cấp độ 5 trở lên.",
            "Cần phải có 200 kim cương.",
            "Không là thành viên của Clan khác.",
            "Đã rời khỏi Clan khác 3 ngày trở lên."            
        };
    }

    public void Button_Back_Click()
    {
        Close();
    }

    public void Button_TaoClan_Click()
    {
        if (this.INPClanName.text != "" && this.INPClanDescription.text != "")
        {
            ClanCreateRequest ClanCreateRequest = new ClanCreateRequest();
            ClanCreateRequest.roleId = this.INPClanName.text;

            HorusClientAPI.ClanCreateProcess(ClanCreateRequest,
                (result) =>
                {
                    if (result.Clan.roleId != null && result.Clan.roleId != string.Empty && result.Clan.roleId != "")
                    {   
                        ClanUpdateRequest ClanUpdateRequest = new ClanUpdateRequest();
                        ClanUpdateRequest.description = this.INPClanDescription.text;
                        ClanUpdateRequest.logo = this.IMGAvatarClan.sprite.name;

                        HorusClientAPI.ClanUpdateProcess(ClanUpdateRequest,
                            (result_update) =>
                            {
                                CSVR_UICLanList.instance.MyClan = result.Clan;
                                HorusManager.instance.Clan = new ClanInfo(CSVR_UICLanList.instance.MyClan.roleId);
                                CSVR_UICLanList.instance.MyClan.UpdateAllMemberArray();

                                CSVR_UICLanList.instance.UpdateButtonXem_Tao();
                                CSVR_UICLanList.instance.UpdateClanListInUI();

                                CSVR_UIClanList_XemMyClan.instance.Open();
                                CSVR_UIClanList_XemMyClan.instance.Show(CSVR_UICLanList.instance.MyClan);
                                Close();
                            },
                            (error_update) =>
                            {
                                ErrorView.instance.ShowPopupError("LỖI KHÔNG CẬP NHẬT ĐƯỢC CLAN (0)!!!");
                            }, null);                        
                    }
                },
                (error) =>
                {
                    ErrorView.instance.ShowPopupError("LỖI KHÔNG TẠO ĐƯỢC CLAN!!! HÃY XEM LẠI ĐIỀU KIỆN ĐỂ KIẾN TẠO CLAN!!");
                }, null);
        } else
        {
            if (this.INPClanName.text == "") ErrorView.instance.ShowPopupError("CHƯA CÓ TÊN CLAN!!");
            if (this.INPClanDescription.text == "") ErrorView.instance.ShowPopupError("CHƯA CÓ KHẨU HIỆU CLAN!!");
        }
    }

    public void Button_ThayLogo_Click()
    {
        ContentHolderAvatar.gameObject.SetActive(true);
        Update_scrollAvatar();
    }

    public void Update_scrollAvatar()
    {
        scrollAvatar.totalCount = UISpriteManager.instance.LogoClan.Length;
        scrollAvatar.RefillCells();
    }

    public void Update_scrollRequire()
    {
        scrollRequirement.totalCount = listRequirement.Count;
        scrollRequirement.RefillCells();
    }
}