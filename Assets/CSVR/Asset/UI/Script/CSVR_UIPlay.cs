﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CSVR_UIPlay : MonoBehaviour
{
	public static CSVR_UIPlay instance;
	//bật tắt UI khi chết các mode sinh tử
	public GameObject[] UIPlayObject;
	public Button[] camViewButton;
	public CSVR_UICamView camView;
    public Button btnKnifeAttack;
	public Canvas canvas;
	public Image iconWeapon,loadbar;
	public Text  txtMaxAmmoCount,txtPlayer;
	public Button btnNextWeapon, btnPrevWeapon,btnGetC4, btnGrenade, btnCrouch, btnZoom, btnResult, btnStopZoom, btnReload,btnReloadGun;
	public GameObject btnJump,  imgSniperCrosshair,ScoreBoard,objLoadbar,_playerJoined,_control1;
	public Sprite Crouch, idle;
	
	[Space(5)]
	public Transform crosshairParent; 
	[Space(5)]
	public Camera camera2D; 
	[Space(5)]
	public EventSystem eventSystem;

	//danh cho mode bomb
	public LongPressEventTrigger longPressEventTrigger;
	
	void Awake(){
		instance = this;
	}
	void Start(){
		//tat game object de nguoi choi khong the di chuyen
		UIPlayObject[0].gameObject.SetActive (false);
	}
	public void updateCamera(Quaternion rot, Vector3 pos) {		
		//		Debug.Log ("updateCamera");
		switch (CSVR_GameSetting.viewMode) {
		case CSVR_ViewMode.VIEW_2D_MODE: 
			
			camera2D.transform.rotation = rot;
			camera2D.transform.position = pos;				
			break;
		case CSVR_ViewMode.VIEW_VR_MODE: 
			//			cameraSBS.rotation = rot;
			//			cameraSBS.position = pos;		
			break;	
		default:
			
			break;					
		}
	}
	public void UIPlayState(bool enable)
	{
		if (enable)
		{
			_control1.SetActive(true);        
		}
		else
		{
			_control1.SetActive(false);
		}
	}
	public void PlayerJoined(string _name)
	{
		Text obj = GameObject.Instantiate(txtPlayer).GetComponent<Text>();
		obj.text = _name+" Tham Chiến.";
		obj.transform.SetParent(_playerJoined.transform);
		obj.transform.localScale = Vector3.one;
	}
	public void PlayerDisconect(string _name)
	{
		if (vp_MPMaster.Instance.IsEndGame) return;
		Text obj = GameObject.Instantiate(txtPlayer).GetComponent<Text>();
		obj.text = _name + " Rời trận";
		obj.color = Color.red;
		obj.transform.SetParent(_playerJoined.transform);
		obj.transform.localScale = Vector3.one;
	}
	public void changeCameraFOV(float newFOV) {
		switch (CSVR_GameSetting.viewMode) {
		case CSVR_ViewMode.VIEW_2D_MODE: 
			//Debug.Log("switchCamera 2D");
			camera2D.GetComponent<Camera>().fieldOfView = newFOV;
			break;
			
		case CSVR_ViewMode.VIEW_VR_MODE: 
			//Debug.Log("switchCamera VR");
			//cameraSBS.GetComponent<CSVR_ALPSController>().setFieldOfView(newFOV);
			break;					
			
		default:
			
			break;					
		}
	}
	public void SwitchViewMode(CSVR_ViewMode newViewMode) {
		CSVR_GameSetting.viewMode = newViewMode;
		switch (newViewMode) {
		case CSVR_ViewMode.VIEW_VR_MODE: 
			//SwitchControlMode(CSVR_ControlMode.CONTROL_HORUS); //force gamepad mode
			
			camera2D.gameObject.SetActive(false);
			//cameraSBS.gameObject.SetActive(true);
			
			break;					
			
			
		case CSVR_ViewMode.VIEW_2D_MODE: 
		default:
			//UIPlay.gameObject.SetActive(true);
			//crosshairParent.gameObject.SetActive(true);
			camera2D.gameObject.SetActive(true);            
			break;
		}
		
		
	}
	public void UI_OnEnable(){
		UIPlayObject[0].gameObject.SetActive (true);
		UIPlayObject[1].gameObject.SetActive (true);
		UIPlayObject[2].gameObject.SetActive (false);
	}
	public void UI_OnDisable(){
		UIPlayObject[0].gameObject.SetActive (false);
		UIPlayObject[1].gameObject.SetActive (false);
		UIPlayObject[2].gameObject.SetActive (true);
	}
	//    private void OnApplicationPause(bool pause)
	//    {
	//
	//        if (pause && PhotonNetwork.isMasterClient)
	//        {
	//           PlayFabManager.instance.gameModeInstance.transform.FindChild(CSVR_GameSetting.modeGame).gameObject.SetActive(false);
	//           HomeScreen.states = States.Lobby;
	//           Application.LoadLevel("GameOver");
	//        }
	//    }
}
