﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIChangeCharacter : MonoBehaviour {

	public Button policeButton;

	public Button terroristButton;

	void Start(){

		policeButton.onClick.AddListener(() => { 
			policeButton.gameObject.SetActive(false);
			terroristButton.gameObject.SetActive(true);
		});
//
		terroristButton.onClick.AddListener(() => {
			policeButton.gameObject.SetActive(true);
			terroristButton.gameObject.SetActive(false);
		});
	}
}
