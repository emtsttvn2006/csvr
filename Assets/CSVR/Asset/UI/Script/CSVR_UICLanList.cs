﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;
using MarchingBytes;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using LitJson;
using UnityEngine.Events;

public class CSVR_UICLanList : SingletonBaseScreen<CSVR_UICLanList>
{
    private List<string> ClanListBy_List = new List<string>() { "roleId", "level", "memberCount", "updated_at", "created_at" };
    private List<string> ClanOrder_List = new List<string>() { "asc", "desc" };

    public Dropdown DropdownSapxepTruong1;
    public Dropdown DropdownSapxepTruong2;
    public InputField INPTimkiem;

    public Text TextAnnounce;

    public Button ButtonTaoClan;
    public Button ButtonXem;
    public Button ButtonDaichien;

    private bool IsButtonPreviousHighlighted = false;
    private bool IsButtonNextHighlighted = false;
    public Button ButtonPrevious;
    public Button ButtonNext;

    [HideInInspector]
    public string currentClanListBy = "roleId";
    [HideInInspector]
    public string currentClanOrder = "asc";
    [HideInInspector]
    public string currentClanPage = "0";

    [HideInInspector]
    public ClanListResult ClanListResult = new ClanListResult();
    [HideInInspector]
    public ClanListResult ListClanInviting = new ClanListResult();
    [HideInInspector]
    public ClanListResult ListClanSearch = new ClanListResult();
    [HideInInspector]
    public ClanResult MyClan = new ClanResult();

    public LoopVerticalScrollRect scrollClanList;
    public LoopVerticalScrollRect scrollClanInvitingList;

    public override void Open()
    {
        base.Open();

        CharacterView.instance.Close();
        MainView.instance.Close();

        if (scrollClanList.prefabPool == null) scrollClanList.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();

        TopBar.instance.SetBackButton(new UnityAction(() =>
        {
            MainView.instance.Open();
            CharacterView.instance.Open();

            Close();
        }));

        TopBar.instance.SetShopOrInVButton(new UnityAction(() =>
        {
            CharacterView.instance.Open();
            CSVR_UIShop.instance.Open();

            TopBar.instance.SetBackButton(new UnityAction(() =>
            {
                CharacterView.instance.Open();
                MainView.instance.Open();

                CSVR_UIShop.instance.Close();
            }));

            Close();
            AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
        }));

        TopBar.instance.Setting.interactable = true;
        TopBar.instance.ShopOrInVText.text = "Cửa hàng";

        UpdateMyClan();

        UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);        
    }

    public override void Close()
    {
        base.Close();
        CSVR_UIClanItem_MyClanItem.instance.Close();
        TopBar.instance.Open();
        MainView.instance.Open();
        CharacterView.instance.Open();
    }

    public void ResetButtonPreviousNext()
    {
        ButtonPrevious.interactable = true;
        IsButtonPreviousHighlighted = false;

        ButtonNext.interactable = true;
        IsButtonNextHighlighted = false;
    }

    public void UpdateClanList()
    {
        UpdateClanList(this.currentClanListBy, this.currentClanOrder, this.currentClanPage);
    }

    public void UpdateClanList(string listBy, string order, string page)
    {
        ClanListRequest ClanListRequest = new ClanListRequest();
        ClanListRequest.listBy = listBy;
        ClanListRequest.order = order;
        ClanListRequest.page = page;

        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.ClanListProcess(ClanListRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (result.Clan.Count > 0)
                {
                    TextAnnounce.gameObject.SetActive(false);
                    this.ClanListResult.Clan = result.Clan;
                    this.ClanListResult.UpdateAllClanArray();
                    GetClanInviting();
                    UpdateClanListInUI();

                    if (IsButtonPreviousHighlighted)
                    {
                        currentClanPage = (int.Parse(currentClanPage) - 1).ToString();
                    }
                    else if (IsButtonNextHighlighted)
                    {
                        currentClanPage = (int.Parse(currentClanPage) + 1).ToString();
                    }

                }
                else
                {
                    ErrorView.instance.ShowPopupError("KHÔNG TÌM THẤY TRANG!!");
                    if (IsButtonPreviousHighlighted) ButtonPrevious.interactable = false;
                    else if (IsButtonNextHighlighted) ButtonNext.interactable = false;
                }
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                ErrorView.instance.ShowPopupError("KHÔNG TÌM THẤY TRANG!!");
                if (IsButtonPreviousHighlighted) ButtonPrevious.interactable = false;
                else if (IsButtonNextHighlighted) ButtonNext.interactable = false;
            }, null);
    }

    public void GetClanInviting()
    {
        ListClanInviting.Clan.Clear();
        for (int i = 0; i < this.ClanListResult.Clan.Count; i++)
        {
            if (this.ClanListResult.Clan[i].Invite.FindIndex((Invitee) => Invitee == HorusManager.instance.playerUserName) != -1)
            {
                ListClanInviting.Clan.Add(this.ClanListResult.Clan[i]);
            }
        }
    }

    public void UpdateClanListInUI()
    {
        scrollClanList.gameObject.SetActive(true);
        scrollClanInvitingList.gameObject.SetActive(false);

        if (this.INPTimkiem.text == "")
        {
            scrollClanList.totalCount = this.ClanListResult.Clan.Count;
            scrollClanList.RefillCells();
        }
        else
        {
            scrollClanList.totalCount = this.ListClanSearch.Clan.Count;
            scrollClanList.RefillCells();
        }
    }

    public void UpdateClanInvitingInUI()
    {
        scrollClanList.gameObject.SetActive(false);
        scrollClanInvitingList.gameObject.SetActive(true);

        scrollClanInvitingList.totalCount = this.ListClanInviting.Clan.Count;
        scrollClanInvitingList.RefillCells();
    }

    public void UpdateButtonXem_Tao()
    {
        if ((this.MyClan != null && this.MyClan.roleId != null && this.MyClan.roleId != ""))
        {
            ButtonXem.gameObject.SetActive(true);
        }
        else
        {
            ButtonXem.gameObject.SetActive(false);
        }

        ButtonTaoClan.gameObject.SetActive(!ButtonXem.gameObject.activeSelf);
    }

    public void Button_Refresh_Click()
    {
        if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "")
        {
            UpdateMyClan();
        }

        UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
    }

    public void Dropdown_Sapxep1_ValueChanged()
    {
        INPTimkiem.text = "";

        if (DropdownSapxepTruong1.value < ClanListBy_List.Count)
        {
            currentClanListBy = ClanListBy_List[DropdownSapxepTruong1.value];
            UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
        }
        else
        {
            switch (DropdownSapxepTruong1.captionText.text)
            {
                case "Được mời":
                    UpdateClanInvitingInUI();
                    break;
            }
        }
    }

    public void Dropdown_Sapxep2_ValueChanged()
    {
        INPTimkiem.text = "";

        currentClanOrder = ClanOrder_List[DropdownSapxepTruong2.value];
        UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
    }

    public void Button_XoaTimKiem_Click()
    {
        INPTimkiem.text = "";
        UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
    }

    public void Button_Search_Click()
    {
        if (INPTimkiem.text != "")
        {
            ClanSearchRequest ClanSearchRequest = new ClanSearchRequest();
            ClanSearchRequest.roleId = INPTimkiem.text;

            CSVR_UIWaitingIcon.instance.Open();

            HorusClientAPI.ClanSearchProcess(ClanSearchRequest,
                (result) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();
                    this.ListClanInviting = new ClanListResult();
                    this.ListClanSearch.Clan = result.Clan;
                    UpdateClanListInUI();
                },
                (error) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();
                    ErrorView.instance.ShowPopupError("Không tìm thấy Clan này");
                }, null);
        }
        else
        {
            UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
        }
    }

    public void INPTimkiem_ValueChanged()
    {
        if (INPTimkiem.text == "")
        {
            UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
        }
    }

    public void Button_Previous_Click()
    {
        ButtonPrevious.interactable = false;
        IsButtonPreviousHighlighted = true;

        IsButtonNextHighlighted = false;
        ButtonNext.interactable = true;

        if (int.Parse(currentClanPage) > 0)
        {
            UpdateClanList(currentClanListBy, currentClanOrder, (int.Parse(currentClanPage) - 1).ToString());
        }
    }

    public void Button_Next_Click()
    {
        IsButtonNextHighlighted = true;
        ButtonNext.interactable = false;

        IsButtonPreviousHighlighted = false;
        ButtonPrevious.interactable = true;
        UpdateClanList(currentClanListBy, currentClanOrder, (int.Parse(currentClanPage) + 1).ToString());
    }

    public void UpdateMyClan()
    {
        GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();
        GetUserInfoRequest.UserIds.Add(HorusManager.instance.playerUserName);

        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.GetUserInfo(GetUserInfoRequest,
            (result_userinfo) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (result_userinfo.users.Count > 0)
                {
                    if (result_userinfo.users[0].clan != null)
                    {
                        ClanInfo ClanInfo = JsonMapper.ToObject<ClanInfo>(result_userinfo.users[0].clan.ToString());
                        if (ClanInfo != null && ClanInfo.clanName != "")
                        {
                            ClanSearchRequest ClanSearchRequest = new ClanSearchRequest();
                            ClanSearchRequest.roleId = ClanInfo.clanName;

                            HorusClientAPI.ClanSearchProcess(ClanSearchRequest,
                                (result_clansearch) =>
                                {
                                    if (result_clansearch.Clan.Count <= 0)
                                    {
                                        ErrorView.instance.ShowPopupError("Danh sách trống!!");
                                    }
                                    else
                                    {
                                        this.MyClan = result_clansearch.GetClan(ClanInfo.clanName);
                                        this.MyClan.UpdateAllMemberArray();
                                        HorusManager.instance.Clan = ClanInfo;
                                        UpdateButtonXem_Tao();
                                        UpdateClanListInUI();
                                        CSVR_UIClanItem_MyClanItem.instance.OpenChildrenToo();
                                    }
                                },
                                (error) =>
                                {

                                }, null);

                            if (this.MyClan != null && this.MyClan.roleId != "")
                            {
                                ButtonXem.gameObject.SetActive(true);
                                ButtonTaoClan.gameObject.SetActive(false);
                            }
                            else
                            {
                                ButtonXem.gameObject.SetActive(false);
                                ButtonTaoClan.gameObject.SetActive(true);
                            }
                        }
                        else
                        {
                            if (ClanInfo == null)
                                if (DebugError)
                                    Debug.Log("Sai thông tin!!");

                            if (ClanInfo.clanName == "")
                                if (DebugError)
                                    Debug.Log("Tên Clan Trống!!");
                        }
                    }
                    else
                    {
                        if (DebugError)
                            //Debug.Log("Thông tin User không có biến \"clan\"");
                            ButtonXem.gameObject.SetActive(false);
                        ButtonTaoClan.gameObject.SetActive(true);
                    }
                }
                else
                {
                    ErrorView.instance.ShowPopupError("Không tìm thấy thông tin User");
                    ButtonXem.gameObject.SetActive(false);
                    ButtonTaoClan.gameObject.SetActive(false);
                }
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                ErrorView.instance.ShowPopupError("Cập nhật thông tin User Lỗi (0)");
                ButtonXem.gameObject.SetActive(false);
                ButtonTaoClan.gameObject.SetActive(false);
            }, null);
    }

    public void SmallScale_ScrollView()
    {
        this.scrollClanList.GetComponent<RectTransform>().sizeDelta = new Vector2(0, -61f);
    }

    public void BigScale_ScrollView()
    {
        this.scrollClanList.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
    }

    public void Button_BXHClan_Click()
    {
        UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
    }

    public void Button_TaoClan_Click()
    {
        if (HorusManager.instance.Clan == null || HorusManager.instance.Clan.clanName == "")
        {
            CSVR_UIClanList_TaoClan.instance.Open();

            TopBar.instance.SetBackButton(new UnityAction(() =>
            {
                CSVR_UIClanList_TaoClan.instance.Close();
                CSVR_UICLanList.instance.Open();
            }));

        }
    }

    public void Button_XemMyClan_Click()
    {
        CSVR_UIClanList_XemMyClan.instance.Open();
        CSVR_UIClanList_XemMyClan.instance.Show(this.MyClan);
        this.Close();

        TopBar.instance.SetBackButton(new UnityAction(() =>
        {
            CSVR_UIClanList_XemMyClan.instance.Close();
            this.Open();
        }));
    }

    public void Button_ClanWar_Click()
    {
        ErrorView.instance.ShowPopupError("TÍNH NĂNG NÀY ĐANG PHÁT TRIỂN!!");
    }

    public void AddAJoining(string Name)
    {
        ClanMemberAddRequest ClanMemberAddRequest = new ClanMemberAddRequest();
        //ClanMemberAddRequest.username = Name;

        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.ClanMemberAddProcess(ClanMemberAddRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                this.MyClan = result.Clan;
                this.MyClan.UpdateAllMemberArray();
                HorusManager.instance.Clan = new ClanInfo(this.MyClan.roleId);
                UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
                CSVR_UIClanList_XemMyClan.instance.Show(this.MyClan);
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (DebugError) Debug.Log("Đồng ý vào Clan lỗi (0)");
                ErrorView.instance.ShowPopupError("ĐỒNG Ý VÀO CLAN LỖI" + error.ErrorMessage);
            }
            , null);
    }

    public void RemoveAMember(string Name)
    {
        ClanMemberRemoveRequest ClanMemberRemoveRequest = new ClanMemberRemoveRequest();
        ClanMemberRemoveRequest.Add(Name);

        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.ClanMemberRemoveProcess(ClanMemberRemoveRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                this.MyClan = result.Clan;
                this.MyClan.UpdateAllMemberArray();
                HorusManager.instance.Clan = new ClanInfo(this.MyClan.roleId);
                UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
                CSVR_UIClanList_XemMyClan.instance.Show(this.MyClan);
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (DebugError) Debug.Log("Cho ra đảo lỗi (0)");
                ErrorView.instance.ShowPopupError("CHO RA ĐẢO LỖI" + error.ErrorMessage);
            }
            , null);
    }

    public void JoiningAClan(string roleID)
    {
        ClanJoiningRequest ClanJoiningRequest = new ClanJoiningRequest();
        ClanJoiningRequest.clan = roleID;

        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.ClanJoiningProcess(ClanJoiningRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                ErrorView.instance.ShowPopupError("ĐÃ GỬI ĐƠN XIN VÀO " + result.Clan.roleId);

                this.Open();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (DebugError) Debug.Log("Xin vào Clan lỗi!! Có thể bạn đã nộp đơn!! Hãy thử nói chuyện với chủ clan!!");
                ErrorView.instance.ShowPopupError("Xin vào Clan lỗi!! Có thể bạn đã nộp đơn!! Hãy thử nói chuyện với chủ clan!!" + error.ErrorMessage);
            }
            , null);
    }

    public void OutAClan()
    {
        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.ClanOutProcess(new ClanOutRequest(),
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                this.MyClan = null;
                HorusManager.instance.Clan = new ClanInfo("");

                UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
                UpdateButtonXem_Tao();

                CSVR_UIClanList_XemMyClan.instance.Close();
                CSVR_UIClanItem_MyClanItem.instance.CloseChildrenToo();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (DebugError) Debug.Log("Không Thể Thoát Clan");
                ErrorView.instance.ShowPopupError("KHÔNG THỂ THOÁT CLAN:");
            }
            , null);
    }

    public void InActiveAClan()
    {
        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.ClanInActiveProcess(new ClanInActiveRequest(),
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                CSVR_UIClanList_XemMyClan.instance.Close();
                this.MyClan = null;
                HorusManager.instance.Clan = new ClanInfo("");
                UpdateClanList(currentClanListBy, currentClanOrder, currentClanPage);
                UpdateButtonXem_Tao();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (DebugError) Debug.Log("Không Thể Thoát Clan");
                ErrorView.instance.ShowPopupError("KHÔNG THỂ THOÁT CLAN:");
            }
            , null);
    }

    public void UpdateAClan(string logo, string description)
    {
        CSVR_UIWaitingIcon.instance.Open();
        ClanUpdateRequest ClanUpdateRequest = new ClanUpdateRequest();
        ClanUpdateRequest.logo = logo;
        ClanUpdateRequest.description = description;

        HorusClientAPI.ClanUpdateProcess(ClanUpdateRequest,
            (result) =>
            {
                result.Clan.UpdateAllMemberArray();
                MyClan = result.Clan;
                UpdateClanListInUI();

                CSVR_UIWaitingIcon.instance.Close();

                CSVR_UIClanList_XemMyClan.instance.Open();
                CSVR_UIClanList_XemMyClan.instance.Show(result.Clan);
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();

                if (DebugError) Debug.Log("Không Thể Cập Nhật Clan");
                ErrorView.instance.ShowPopupError("KHÔNG THỂ CẬP NHẬT CLAN:");
            }
            , null);
    }
}
