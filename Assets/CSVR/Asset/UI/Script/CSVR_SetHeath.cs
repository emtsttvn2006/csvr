﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_SetHeath : MonoBehaviour {
    public Text txtHeath;
	// Use this for initialization
    public void SetHeath(string strHeath)
    {
        txtHeath.text = strHeath;
    }
    void Start()
    {
        StartCoroutine("DestroyObject");
    }
    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(0.55f);
        Destroy(gameObject);
    }
    void Update()
    {
        transform.LookAt(Camera.main.transform);
        transform.rotation = Camera.main.transform.rotation;
    }
}
