﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIClanList_TaoClan_AvataItem_Callback : MonoBehaviour 
{
    public Image Image;

    void ScrollCellIndex(int idx)
    {        
        this.Image.sprite = UISpriteManager.instance.GetClanLogoSprite(idx.ToString());        
    }

    public void Button_Click()
    {
        CSVR_UIClanList_TaoClan.instance.IMGAvatarClan.sprite = this.Image.sprite;
        CSVR_UIClanList_TaoClan.instance.ContentHolderAvatar.gameObject.SetActive(false);
    }
}
