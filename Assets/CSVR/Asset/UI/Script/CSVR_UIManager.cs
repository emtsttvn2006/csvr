//using UnityEngine;
//using System.Collections;
//using  UnityEngine.UI;
//using PlayFab;
//using PlayFab.ClientModels;
//using PlayFab.Internal;
//
//using System.Text.RegularExpressions;
//
//using Newtonsoft.Json;
//
//public enum CSVR_UI {
//	ALL,
//	NONE,
//	LOGIN,
//	REGISTRATION,
//	UPDATENAME,
//	HOME_V3,
//	HOME,
//	CHAT,
//	INFO,
//	SETTING,
//	FRIEND,
//	MULTIPLAYER,
//	CREATEROOM,
//	SINGLEPLAYER,
//	EVENT,
//	SHOP,
//	INVENTORY,
//	TOTALGUN,
//	MAINGUN,
//	PISTOLGUN,
//	MELEGUN,
//	ARMORGUN,
//	GRENADEGUN,
//	MESSAGE,
//	LOADINGHOME,
//	ENDGAME,
//	LOADINGMAP,
//	PLAYING,
//	LEVELUP,
//	PAUSE_UI
//}
//
//public class CSVR_UIManager : MonoBehaviour {
//	
//	CSVR_ApplicationState previousAppState = CSVR_ApplicationState.NULL;
//	string previousStateInfo = "";
//	
//	//Login
//	public GameObject UI_LoginScreen;
//	
//	public GameObject playfabUsername_Login;
//	public GameObject playfabPassword_Login;
//	public GameObject playfabRemember_Login;
//	//	public GameObject playfabLoginStatus;
//	
//	// User registration
//	public GameObject UI_PlayfabRegistrationScreen;
//	
//	public GameObject playfabUsername_Registration;
//	public GameObject playfabPassword_Registration;
//	public GameObject playfabConfirmPassword_Registration;
//	public GameObject playfabEmail_Registration;
//	public GameObject playfabDK_Registration;
//	//	public GameObject playfabRegistrationStatus;
//	//Update Name
//	public GameObject UI_UpdateName;
//	public InputField UI_InputName;
//	//Home Truong 18.3.2016
//	public GameObject UI_Home_V3;
//	//Home
//	public GameObject UI_Home;
//	//Home
//	public GameObject UI_Chat;
//	//	public GameObject UI_HomeParticle;
//	//Home
//	//public GameObject UI_Info;
//	//Setting
//	public GameObject UI_Setting;
//	
//	//Friend
//	public GameObject UI_Friend;
//	//	public InputField UI_InputNameFriend;
//	//Pause setting
//	//	public GameObject UI_PauseButton;
//	//	public GameObject UI_PauseScreen;
//	//	public GameObject key_setting_UI;
//	
//	//Multiplayer
//	public GameObject UI_MultiplayerLobbyScreen;
//	public GameObject UI_MultiplayerCreateRoomScreen;
//	public GameObject UI_MultiplayerRoomScreen;
//	//	public Endgame.ListView  UI_RoomListView;
//	//	public Endgame.ListView  UI_MultiplayerMapList;
//	//	public Endgame.ListView  UI_MultiplayerCharacterList;
//	//	
//	//Singleplayer
//	//	public GameObject UI_SingleplayerScreen;
//	//	public Endgame.ListView  UI_SingleplayerMapList;
//	//	public Endgame.ListView  UI_SingleplayerCharacterList;
//	
//	//Shop
//	//public GameObject UI_ShopScreen;
//	//	public ListView UI_ShopListView;
//	//	public Text UI_CurrencyTxt;
//	
//	//Inventory GUn.
//	public GameObject UI_TotalGunScreen;
//	//public GameObject UI_InventoryScreen;
//	//	public GameObject UI_InventoryMainGun;
//	//	public GameObject UI_InventoryPistolGun;
//	//	public GameObject UI_InventoryMeleGun;
//	//	public GameObject UI_InventoryArmorGun;
//	//	public GameObject UI_InventoryGrenadeGun;
//	//public GameObject UI_InventoryChestAndMore;
//	//	public ListView UI_ShopListView;
//	//	public Text UI_CurrencyTxt;
//	
//	//	public ListView UI_InventoryListView;
//	//	public ListView UI_EquippedItemListView;
//	
//	public GameObject UI_LevelUP;
//	//Message	
//	
//	public GameObject UI_MessageScreen;
//	public GameObject UI_LoadingHome;
//	public GameObject UI_LoadingMap;
//	public GameObject UI_EndGame;
//	//Popup	
//	
//	public GameObject UI_PopupScreen;	
//	
//	public GameObject UI_PopupConfirm;
//	
//	//internal
//	private uint roomListVersion = 0;
//	
//	void Start() {
//		Object.DontDestroyOnLoad(transform.root.gameObject);
//		
//		//		MusicMa/nager.Instance.Source.clip = MusicManager.Instance.menu;
//		CSVR_MusicManager.instance.musicSource.Stop();
//		CSVR_MusicManager.instance.musicSource.loop = true;
//		CSVR_MusicManager.instance.musicSource.clip = CSVR_MusicManager.instance.menuClip;
//		CSVR_MusicManager.instance.musicSource.Play();
//		switchUI (CSVR_UI.ALL);
//		CSVR_ApplicationManager.instance.infos = FindObjectsOfType(typeof(CSVR_UIPlayerInfo)) as CSVR_UIPlayerInfo[];
//	}
//	
//	
//	//	//Change UI depending on the application state
//	void Update() {
//				if (Time.frameCount % 30 == 0)  
//				{  
//					System.GC.Collect();  
//				}  
//		//Force update UI if the stateInfo has changed
//		if (previousStateInfo != CSVR_ApplicationManager.StateInfo) {
//			previousStateInfo = CSVR_ApplicationManager.StateInfo;
//			previousAppState = CSVR_ApplicationState.NULL;
//		}
//		
//		
//		if (previousAppState != CSVR_ApplicationManager.ApplicationState) {
//			previousAppState = CSVR_ApplicationManager.ApplicationState;
//			switch (CSVR_ApplicationManager.ApplicationState) {
//				
//			case CSVR_ApplicationState.MULTIPLAYER_CREATING_ROOM:
//				//UI_Message_SetText("Creating room");
//			//	Debug.Log ("RoomIsCreate 2");
//				switchUI (CSVR_UI.CREATEROOM);
//				break;
//			case CSVR_ApplicationState.MULTIPLAYER_JOINING_ROOM:
//				UI_Message_SetText("Joining room");
//				break;
//			case CSVR_ApplicationState.MULTIPLAYER_STARTING:
//				UI_Message_SetText ("Connecting to server");
//				break;
//				
//			case CSVR_ApplicationState.INITIALIZING:
//				UI_Message_SetText("Initializing...");
//				break;
//			case CSVR_ApplicationState.MULTIPLAYER_LOBBY:
//				switchUI (CSVR_UI.MULTIPLAYER);
//				//				ShowMultiplayerLobby();
//				break;
//			case CSVR_ApplicationState.MULTIPLAYER_PLAYING:
//				switchUI (CSVR_UI.PLAYING);
//				break;
//			case CSVR_ApplicationState.MULTIPLAYER_PAUSE:
//				switchUI (CSVR_UI.PAUSE_UI);
//				break;
//			case CSVR_ApplicationState.SINGLEPLAYER_LOADING_MAP:
//				UI_Message_SetText("Loading map...");
//				break;
//			case CSVR_ApplicationState.SINGLEPLAYER_PAUSE:
//				switchUI (CSVR_UI.PAUSE_UI);
//				break;
//			case CSVR_ApplicationState.SINGLEPLAYER_PLAYING:
//				switchUI (CSVR_UI.PLAYING);
//				break;
//			case CSVR_ApplicationState.LOGIN:
//				switchUI (CSVR_UI.LOGIN);
//				//				playfabLoginStatus.GetComponent<Text>().text = CSVR_ApplicationManager.StateInfo;
//				break;
//			case CSVR_ApplicationState.REGISTRATION:
//				switchUI (CSVR_UI.REGISTRATION);
//				//				playfabRegistrationStatus.GetComponent<Text>().text = CSVR_ApplicationManager.StateInfo;
//				break;
//			case CSVR_ApplicationState.LOGIN_FACEBOOK_LOGGING:
//				switchUI (CSVR_UI.NONE);
//				break;
//				
//			case CSVR_ApplicationState.SHOP_LOADING_ITEM:
//				UI_Loading_Home();
//				//				UI_Message_SetText("Loading");
//				
//				break;
//			case CSVR_ApplicationState.SHOP_BUYING_ITEM:
//				UI_PopupScreen.gameObject.SetActive(true);
//				
//				break;
//			case CSVR_ApplicationState.SHOP_END_BUY:
//				UI_PopupScreen.gameObject.SetActive(false);
//				
//				break;
//			case CSVR_ApplicationState.EQUIP_ITEM:
//				UI_Message_SetText("Equip item");
//				
//				break;
//			case CSVR_ApplicationState.UNEQUIP_ITEM:
//				UI_Message_SetText("Un Equip item");
//				
//				break;
//				
//			case CSVR_ApplicationState.SHOP_READY:
//				switchUI (CSVR_UI.SHOP);
//				
//				break;
//			case CSVR_ApplicationState.INVENTORY_TOTAL:
//				switchUI (CSVR_UI.TOTALGUN);
//				
//				break;
//			case CSVR_ApplicationState.INVENTORY_MAINGUN:
//				switchUI (CSVR_UI.MAINGUN);
//				
//				break;
//			case CSVR_ApplicationState.INVENTORY_PISTOLGUN:
//				switchUI (CSVR_UI.PISTOLGUN);
//				
//				break;
//			case CSVR_ApplicationState.INVENTORY_ARMORGUN:
//				switchUI (CSVR_UI.ARMORGUN);
//				
//				break;
//			case CSVR_ApplicationState.INVENTORY_MELEGUN:
//				switchUI (CSVR_UI.MELEGUN);
//				
//				break;
//			case CSVR_ApplicationState.INVENTORY_GRENADEGUN:
//				
//				switchUI (CSVR_UI.GRENADEGUN);
//				
//				break;
//			case CSVR_ApplicationState.UPDATENAME:
//				
//				switchUI (CSVR_UI.UPDATENAME);
//				
//				break;
//			case CSVR_ApplicationState.MAP_LOADING:
//				
//				switchUI (CSVR_UI.LOADINGMAP);
//				
//				break;
//			case CSVR_ApplicationState.ENDGAME:
//				
//				switchUI (CSVR_UI.ENDGAME);
//				break;
//			case CSVR_ApplicationState.LEVELUP:
//				
//				switchUI (CSVR_UI.LEVELUP);
//				
//				break;
//			case CSVR_ApplicationState.HOME:
//			default:
//				//Truong 18.03.2016
//				switchUI (CSVR_UI.HOME_V3);
//				break;
//				
//			}
//			
//		}
//		
//		//		UpdateMultiplayerRoomList ();
//	}
//	
//	public void switchUI(CSVR_UI uiType){ 
//		
//		if (uiType == CSVR_UI.ALL) {
//			UI_LoginScreen.SetActive(true);
//			UI_PlayfabRegistrationScreen.SetActive(true);
//			UI_UpdateName.SetActive(true);
//			UI_Home_V3.SetActive (true);
//			UI_Home.SetActive (true);
//			UI_Chat.SetActive (true);
//			//UI_Info.SetActive(true);
//			UI_Setting.SetActive (true);
//			UI_Friend.SetActive(true);
//			UI_MultiplayerLobbyScreen.SetActive (true);
//			UI_MultiplayerCreateRoomScreen.SetActive(true);
//			UI_MultiplayerRoomScreen.SetActive (true);
//			//			UI_SingleplayerScreen.SetActive (true);
//			//UI_ShopScreen.SetActive (true);
//			UI_TotalGunScreen.SetActive (true);
//			//UI_InventoryScreen.SetActive (true);
//			//			UI_InventoryMainGun.SetActive (true);
//			//			UI_InventoryPistolGun.SetActive (true);
//			//			UI_InventoryMeleGun.SetActive (true);
//			//			UI_InventoryArmorGun.SetActive (true);
//			//			UI_InventoryGrenadeGun.SetActive (true);
//			//UI_InventoryChestAndMore.SetActive (true);
//			//			UI_PauseScreen.SetActive (true);
//			//			UI_PauseButton.SetActive (true);
//			UI_MessageScreen.SetActive (true);
//			UI_LoadingHome.SetActive(true);
//			UI_LoadingMap.SetActive(true);
//			UI_EndGame.SetActive(true);
//			UI_LevelUP.SetActive(true);
//			UI_PopupScreen.SetActive (true);
//		} else {
//			UI_LoginScreen.SetActive(false);
//			UI_PlayfabRegistrationScreen.SetActive(false);
//			UI_UpdateName.SetActive(false);
//			UI_Home.SetActive (false);
//			UI_Home_V3.SetActive (false);
//			UI_Chat.SetActive (false);
//			//			UI_HomeParticle.SetActive (false);
//			//UI_Info.SetActive(false);
//			UI_Setting.SetActive (false);
//			
//			UI_Friend.SetActive(false);
//			UI_MultiplayerLobbyScreen.SetActive (false);
//			UI_MultiplayerCreateRoomScreen.SetActive(false);
//			UI_MultiplayerRoomScreen.SetActive (false);
//
//			UI_TotalGunScreen.SetActive (false);
//
//			UI_MessageScreen.SetActive (false);
//			UI_LoadingHome.SetActive(false);
//			UI_EndGame.SetActive(false);
//			UI_LevelUP.SetActive(false);
//			UI_LoadingMap.SetActive(false);
//			UI_PopupScreen.SetActive (false);
//		}
//		
//		switch (uiType) {
//		case CSVR_UI.LOGIN:
//			UI_LoginScreen.SetActive(true);
//			break;
//		case CSVR_UI.REGISTRATION:
//			UI_PlayfabRegistrationScreen.SetActive(true);
//			break;
//		case CSVR_UI.EVENT:
//			break;
//		case CSVR_UI.HOME_V3:
//			UI_Home_V3.SetActive (true);
//			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_eff_Start);
//			CSVR_MusicManager.instance.musicSource.Stop();
//			CSVR_MusicManager.instance.musicSource.loop = true;
//			CSVR_MusicManager.instance.musicSource.clip = CSVR_MusicManager.instance.menuClip;
//			CSVR_MusicManager.instance.musicSource.Play();
//			break;
//		case CSVR_UI.HOME:
//			UI_Home.SetActive (true);
//			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_eff_Start);
//			break;
//		case CSVR_UI.CHAT:
//			UI_Chat.SetActive (true);
//			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_ChatClick);
//			break;
////		case CSVR_UI.INFO:
////			UI_Info.SetActive (true);
////			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_CharacterInfo);
////			break;
//		case CSVR_UI.SETTING:
//			UI_Setting.SetActive (true);
//			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_Menu);
//			break;
//		case CSVR_UI.FRIEND:	
//			UI_Friend.SetActive (true);
//			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_Menu);
//			break;
//		case CSVR_UI.MESSAGE:
//			UI_MessageScreen.SetActive (true);
//			break;
//		case CSVR_UI.LOADINGHOME:
//			UI_LoadingHome.SetActive (true);
//			break;
//		case CSVR_UI.ENDGAME:
//			UI_EndGame.SetActive (true);
//			break;
//		case CSVR_UI.LEVELUP:
//			UI_LevelUP.SetActive (true);
//			break;
//		case CSVR_UI.LOADINGMAP:
//			UI_LoadingMap.SetActive (true);
//			break;
//		case CSVR_UI.MULTIPLAYER:
//			UI_MultiplayerLobbyScreen.SetActive (true);
//			CSVR_MusicManager.instance.musicSource.Stop();
//			CSVR_MusicManager.instance.musicSource.loop = true;
//			CSVR_MusicManager.instance.musicSource.clip = CSVR_MusicManager.instance.modeClip;
//			CSVR_MusicManager.instance.musicSource.Play();
//			break;
//		case CSVR_UI.CREATEROOM:
//			//Debug.Log ("RoomIsCreate 3");
//			UI_MultiplayerRoomScreen.SetActive (true);
//			break;
//		case CSVR_UI.SINGLEPLAYER:
//			//			UI_SingleplayerScreen.SetActive (true);
//			break;
//		case CSVR_UI.PLAYING:
//			//			UI_PauseButton.SetActive (true);
//			break;
//		case CSVR_UI.PAUSE_UI:
//			//			UI_PauseScreen.SetActive (true);
//			break;
//			//		case CSVR_UI.SHOP:
//			//			UI_ShopScreen.SetActive(true);
//			//			UI_UpdateShopItemList();
//			//			UI_UpdateInventoryListView();
//			break;
//		case CSVR_UI.TOTALGUN:
//			UI_TotalGunScreen.SetActive(true);
//			break;
//			//		case CSVR_UI.INVENTORY:
//			//			UI_InventoryScreen.SetActive(true);
//			//			break;
//			//		case CSVR_UI.MAINGUN:
//			//			UI_InventoryMainGun.SetActive(true);
//			//			break;
//			//		case CSVR_UI.PISTOLGUN:
//			//			UI_InventoryPistolGun.SetActive(true);
//			//			break;
//			//		case CSVR_UI.MELEGUN:
//			//			UI_InventoryMeleGun.SetActive(true);
//			//			break;
//			//		case CSVR_UI.ARMORGUN:
//			//			UI_InventoryArmorGun.SetActive(true);
//			//			break;
//			//		case CSVR_UI.GRENADEGUN:
//			//			UI_InventoryGrenadeGun.SetActive(true);
//			//			break;
//		case CSVR_UI.UPDATENAME:
//			UI_UpdateName.SetActive(true);
//			break;
//		case CSVR_UI.ALL:
//		case CSVR_UI.NONE:
//		default:
//			break;
//		}
//		
//	}
//	
//	
//	
//	
//	public void UI_LoginPlayfab() {
//		if(playfabRemember_Login.GetComponent<Toggle>().isOn){
//			PlayerPrefs.SetInt("CSVR_GameSetting.isAutoLogin",1);
//		}else{
//			PlayerPrefs.SetInt("CSVR_GameSetting.isAutoLogin",0);
//		}
//		
//		CSVR_ApplicationManager.instance.LoginPlayfab (playfabUsername_Login.GetComponent<InputField> ().text.Trim (), 
//		                                               playfabPassword_Login.GetComponent<InputField> ().text.Trim ());
//	}
//	
//	public void UI_LoginFacebook() {
//		if(playfabRemember_Login.GetComponent<Toggle>().isOn){
//			PlayerPrefs.SetInt("CSVR_GameSetting.isAutoLogin",1);
//		}else{
//			PlayerPrefs.SetInt("CSVR_GameSetting.isAutoLogin",0);
//		}
//		CSVR_ApplicationManager.instance.StartFacebookLogin(); 
//	}
//	public void UI_LoginGoogle() {
//		if(playfabRemember_Login.GetComponent<Toggle>().isOn){
//			PlayerPrefs.SetInt("CSVR_GameSetting.isAutoLogin",1);
//		}else{
//			PlayerPrefs.SetInt("CSVR_GameSetting.isAutoLogin",0);
//		}
//		CSVR_ApplicationManager.instance.StartGooglePlusLogin(); 
//	}
//	
//	public void UI_PlayfabUpdateName()
//	{
//		if(UI_InputName.text != "")
//			CSVR_ApplicationManager.instance.PlayfabUpdateName(UI_InputName.text);
//	}
//	
//	public void UI_PlayfabRegistration() {
//		
//		if(validateEmail(playfabEmail_Registration.GetComponent<InputField>().text.Trim() ) && 
//		   validatePassword(playfabPassword_Registration.GetComponent<InputField>().text.Trim(), 
//		                 playfabConfirmPassword_Registration.GetComponent<InputField>().text.Trim() ) &&
//		   playfabDK_Registration.GetComponent<Toggle>().isOn)
//			
//		{
//			CSVR_ApplicationManager.instance.PlayfabRegistration(playfabUsername_Registration.GetComponent<InputField>().text.Trim(),
//			                                                     playfabPassword_Registration.GetComponent<InputField>().text.Trim(),
//			                                                     playfabEmail_Registration.GetComponent<InputField>().text.Trim());
//		}
//		else{
//			CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//			CSVR_UIMessageText.instance.SetAlphaText("Dang ki khong hop le");
//		}
//	}
//	
//	// meet all requirments of RFCs 5321 & 5322
//	const string emailPattern = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
//	private bool validateEmail(string email){
//		return Regex.IsMatch(email.Trim(), emailPattern);
//		
//	}
//	
//	private bool validatePassword(string password1, string password2){
//		return ((password1 == password2)
//		        &&(password1.Length>0)); 
//	}
//	
//	//Playing game
//	
//	//	public void UI_OnPauseGame() {
//	//		CSVR_ApplicationManager.instance.PauseGame ();
//	//		UI_PauseScreen.SetActive (true);
//	//		UpdateSettingUI ();
//	//	}
//	//	
//	//	public void UI_OnResumeGame() {
//	//		CSVR_ApplicationManager.instance.ResumeGame ();
//	//		switchUI (CSVR_UI.PLAYING);
//	//	}
//	//	
//	//	void UpdateSettingUI() {
//	//		Debug.Log("UpdateSettingUI.UpdateSettingUI");
//	//		if (UI_PauseScreen.GetActive ()) {
//	//			//			Debug.Log("UpdateSettingUI.CSVR_GameSetting.controlMode");
//	//			switch (CSVR_GameSetting.controlMode) {
//	//			case CSVR_ControlMode.CONTROL_HORUS:
//	////				UI_PauseScreen.transform.FindChild("Dropdown_ControlMode").GetComponent<Dropdown>().value = 2;
//	//				break;
//	//			case CSVR_ControlMode.CONTROL_GAMEPAD:
//	////				UI_PauseScreen.transform.FindChild("Dropdown_ControlMode").GetComponent<Dropdown>().value = 1;
//	//				break;
//	//				
//	//			case CSVR_ControlMode.CONTROL_TOUCH:
//	//			default:
//	////				UI_PauseScreen.transform.FindChild("Dropdown_ControlMode").GetComponent<Dropdown>().value = 0;
//	//				break;
//	//			}
//	//			
//	//			switch (CSVR_GameSetting.viewMode) {
//	//			case CSVR_ViewMode.VIEW_VR_MODE:
//	////				UI_PauseScreen.transform.FindChild("Dropdown_ViewMode").GetComponent<Dropdown>().value = 1;
//	//				break;
//	//				
//	//			case CSVR_ViewMode.VIEW_2D_MODE:
//	//			default:
//	////				UI_PauseScreen.transform.FindChild("Dropdown_ViewMode").GetComponent<Dropdown>().value = 0;
//	//				break;
//	//			}
//	//		}
//	//	}
//	//	
//	//	
//	//	public void UI_OnSwitchViewMode() {
//	//		switch (UI_PauseScreen.transform.FindChild ("Dropdown_ViewMode").GetComponent<Dropdown> ().captionText.text) {
//	//			
//	//		case "VR":
//	//			CSVR_ApplicationManager.instance.SwitchViewMode(CSVR_ViewMode.VIEW_VR_MODE);
//	//			break;
//	//			
//	//		case "2D":
//	//		default:
//	//			CSVR_ApplicationManager.instance.SwitchViewMode(CSVR_ViewMode.VIEW_2D_MODE);
//	//			
//	//			break;
//	//			
//	//			
//	//		}
//	//		UpdateSettingUI();
//	//	}
//	
//	//	public void UI_OnSwitchControlMode() {
//	//		Debug.Log ("UI_OnSwitchControlMode");
//	//		switch (UI_PauseScreen.transform.FindChild ("Dropdown_ControlMode").GetComponent<Dropdown> ().captionText.text) {
//	//		case "Horus Controller":
//	//			CSVR_ApplicationManager.instance.SwitchControlMode(CSVR_ControlMode.CONTROL_HORUS);
//	//			break;
//	//			
//	//		case "Gamepad":
//	//			CSVR_ApplicationManager.instance.SwitchControlMode(CSVR_ControlMode.CONTROL_GAMEPAD);
//	//			break;
//	//			
//	//		case "Touch":
//	//		default:
//	//			CSVR_ApplicationManager.instance.SwitchControlMode(CSVR_ControlMode.CONTROL_TOUCH);
//	//			
//	//			break;
//	//			
//	//			
//	//		}
//	//	}
//	
//	
//	public void UI_KeySettingDialog_On() {
//		
//		//		UI_PauseScreen.SetActive (false);
//		//		key_setting_UI.SetActive (true);
//	}
//	
//	public void UI_KeySettingDialog_Off() {
//		
//		//		key_setting_UI.SetActive (false);
//		//		UI_PauseScreen.SetActive (true);
//	}
//	
//	
//	/// <summary>
//	/// Show a message to the screen
//	/// </summary>
//	public void UI_Message_SetText(string newMessage) {
//		switchUI (CSVR_UI.MESSAGE);
//		UI_MessageScreen.GetComponentInChildren<Text>().text =  newMessage; 
//	}
//	public void UI_Loading_Home() {
//		switchUI (CSVR_UI.LOADINGHOME);
//		//		UI_MessageScreen.GetComponentInChildren<Text>().text =  newMessage; 
//	}
//	/// <summary>
//	/// Show a message to the screen
//	/// </summary>
//	public void UI_Popup_SetText(string newMessage) {
//		UI_PopupScreen.gameObject.SetActive(true);
//		//		switchUI (CSVR_UI.MESSAGE);
//		//		UI_MessageScreen.GetComponentInChildren<Text>().text =  newMessage; 
//	}	
//	
//	
//	
//	
//	//Single game
//	//	
//	//	public void UI_OnPlaySingleMenuClicked() {
//	//		//		Debug.Log ("UI_OnPlaySingleMenuClicked");
//	//		switchUI (CSVR_UI.SINGLEPLAYER);
//	//		
//	//		if (UI_SingleplayerMapList.isActiveAndEnabled) {
//	//			
//	//			UI_SingleplayerMapList.SuspendLayout ();
//	//			{
//	//				UI_SingleplayerMapList.Items.Clear ();
//	//				foreach (string mapName in CSVR_AssetManager.instance.mapNames) {
//	//					//					Debug.Log ("Map " + mapName);
//	//					Endgame.ListViewItem listViewItem = new Endgame.ListViewItem (mapName);
//	//					listViewItem.SubItems.Add (mapName);
//	//					UI_SingleplayerMapList.Items.Add (listViewItem);		
//	//					
//	//				}
//	//			}
//	//			UI_SingleplayerMapList.ResumeLayout ();
//	//		}
//	//		
//	//		
//	//		if (UI_SingleplayerCharacterList.isActiveAndEnabled) {
//	//			
//	//			UI_SingleplayerCharacterList.SuspendLayout ();
//	//			{
//	//				UI_SingleplayerCharacterList.Items.Clear ();
//	//				foreach (string playerTypeName in CSVR_AssetManager.instance.playerTypeNames) {
//	//					//					Debug.Log ("playerTypeName  " + playerTypeName);
//	//					Endgame.ListViewItem listViewItem = new Endgame.ListViewItem (playerTypeName);
//	//					listViewItem.SubItems.Add (playerTypeName);
//	//					UI_SingleplayerCharacterList.Items.Add (listViewItem);		
//	//					
//	//				}
//	//			}
//	//			UI_SingleplayerCharacterList.ResumeLayout ();
//	//		}
//	//		
//	//	}
//	//	
//	//	
//	////	
//	//	
//	//	public void UI_OnCreateSingleGame() {
//	//		if (UI_SingleplayerMapList.SelectedItems.Count > 0 && UI_SingleplayerCharacterList.SelectedItems.Count > 0) {
//	//			//Debug.Log(UI_SingleplayerMapList.SelectedItems[0].Text + " selected");
//	//			CSVR_GameSetting.gameMap = UI_SingleplayerMapList.SelectedItems[0].Text;
//	//			CSVR_GameSetting.playerType = UI_SingleplayerCharacterList.SelectedItems[0].Text;
//	//			CSVR_ApplicationManager.instance.CreateSingleGame ();
//	//		}
//	//	}
//	
//	//Multiplayer
//	public void UI_OnPlayOnlineBasicGlobalMenuClicked() {
//		if (CSVR_GameSetting.networkMode == CSVR_NetworkMode.OFFLINE) {
//			return;
//		} 
//		
//		//		CSVR_ApplicationManager.instance.PlayOnlineBasicGlobal ();
//		
//		
//	}
//	public void UI_OnPlayOnlineBasicPriviteMenuClicked() {
//		if (CSVR_GameSetting.networkMode == CSVR_NetworkMode.OFFLINE) {
//			return;
//		} 
//		
//		//		CSVR_ApplicationManager.instance.PlayOnlineBasicPrivate ();
//		
//		
//	}
//	
//	public void UI_OnPlayNowMenuClicked() {
//		CSVR_ApplicationManager.instance.PlayNow ();
//		
//	}
//	//Friend	
//	//	public void UI_OnAddFriendClicked() {
//	//		if(UI_InputNameFriend.text !="")
//	//			CSVR_ApplicationManager.instance.AddFriend (UI_InputNameFriend.text);
//	//		
//	//	}
//	//Shop 
//	
//	//	public void UI_OnShopMenuClicked() {
//	//		CSVR_ApplicationManager.instance.StartShop ();
//	//
//	//	}
//	//	
//	//	public void UI_UpdateShopItemList(){
//	//		
//	//		
//	//		UI_ShopListView.SuspendLayout();
//	//		{
//	//			UI_ShopListView.Items.Clear();
//	//			
//	//			
//	//			for (int x = 0; x < CSVR_GameSetting.shopItems.Count; x++) {
//	//				Debug.Log("Shop item " + CSVR_GameSetting.shopItems[x].ItemId);
//	//				
//	//				Endgame.ListViewItem item = new Endgame.ListViewItem(CSVR_GameSetting.shopItems[x].ItemId);
//	//				//	item.SubItems.Add(CSVR_GameSetting.shopItems[x].ItemId);
//	//				
//	//				item.ImageKey = CSVR_GameSetting.shopItems[x].ItemId;
//	//				/*	ItemData itemData = new ItemData();
//	//				itemData.ImageKey = CSVR_GameSetting.shopItems[x].ItemId;
//	//				itemData.SliderValue = 0;
//	//				item.Tag = itemData;
//	//*/
//	//				UI_ShopListView.Items.Add(item);
//	//				
//	//			}
//	//		}
//	//		
//	//		UI_ShopListView.ResumeLayout();
//	//		Time.timeScale = 1;
//	//	}
//	//	
//	//	public void UI_OnPurchase() {
//	//		
//	//		if (UI_ShopListView.SelectedItems.Count >0 ) {
//	//			for (int i = 0 ; i< UI_ShopListView.SelectedItems.Count; i++) {
//	//				Debug.Log(UI_ShopListView.SelectedItems[i].Text);
//	//				CSVR_ApplicationManager.instance.OnPurchaseItem(UI_ShopListView.SelectedItems[i].Text);
//	//				
//	//			}
//	//		}
//	//	}
//	//	
//	//	public void UI_UpdateInventoryListView() {
//	//		//Update UI if it is enabled
//	//		if (UI_InventoryListView.isActiveAndEnabled) {
//	//			//			UI_InventoryListView.SmallImageList = CSVR_AssetManager.itemIconList; //Add item icons, after the listview has been active
//	//			
//	//			
//	//			UI_CurrencyTxt.text = CSVR_GameSetting.virtualCurrency ["GC"].ToString () + " coins";
//	//			
//	//			UI_InventoryListView.SuspendLayout ();
//	//			{
//	//				UI_InventoryListView.Items.Clear ();
//	//				UI_EquippedItemListView.Items.Clear ();
//	//				if (UI_InventoryListView.Columns.Count <1) {
//	//					ColumnHeader NameColumn = new ColumnHeader ();
//	//					NameColumn.Text = "Dang co";
//	//					UI_InventoryListView.Columns.Add (NameColumn);
//	//				}
//	//				
//	//				
//	//				for (int i = 0; i<CSVR_GameSetting.inventoryItems.Count; i++) {
//	//					Debug.Log ("Inventory item " + CSVR_GameSetting.inventoryItems [i].ItemId +" ItemInstanceId: "+ CSVR_GameSetting.inventoryItems [i].ItemInstanceId);
//	//					//					Debug.Log ("CustomData: "+ CSVR_GameSetting.inventoryItems [i].CustomData);
//	//					Endgame.ListViewItem item = new Endgame.ListViewItem (CSVR_GameSetting.inventoryItems [i].ItemId);				
//	//					
//	//					item.ImageKey = CSVR_GameSetting.inventoryItems [i].ItemId;	
//	//					
//	//					if(CSVR_GameSetting.inventoryItems [i].CustomData != null)
//	//					{
//	//						if(CSVR_GameSetting.inventoryItems [i].CustomData["Equipped"] == "True")
//	//						{
//	//							UI_EquippedItemListView.Items.Add (item);
//	//
//	//						}
//	//						else
//	//						{		
//	//							UI_InventoryListView.Items.Add (item);
//	//						}
//	//					}else
//	//						UI_InventoryListView.Items.Add (item);
//	//					
//	//					//Only list items which are not equipped
//	//					//					if (!CSVR_GameSetting.equippedItems.Contains(CSVR_GameSetting.inventoryItems [i].ItemId) ) {
//	//					//						
//	//					//						Endgame.ListViewItem item = new Endgame.ListViewItem (CSVR_GameSetting.inventoryItems [i].ItemId);				
//	//					//						
//	//					//						item.ImageKey = CSVR_GameSetting.inventoryItems [i].ItemId;				
//	//					//						
//	//					//						UI_InventoryListView.Items.Add (item);
//	//					//					}
//	//					
//	//				}
//	//			}
//	//			UI_InventoryListView.Columns [0].Width = 200;
//	//			UI_InventoryListView.ResumeLayout ();
//	//			
//	//			
//	//			Time.timeScale = 1;
//	//		}
//	//	}
//	//	
//	//	public void UI_OnEquip() {
//	//		
//	//		if (UI_InventoryListView.SelectedItems.Count >0 ) {
//	//			for (int i = 0 ; i< UI_InventoryListView.SelectedItems.Count; i++) {
//	//				Debug.Log(UI_InventoryListView.SelectedItems[i].Text);
//	//				CSVR_ApplicationManager.instance.OnEquipItem(UI_InventoryListView.SelectedItems[i].Text);
//	//				
//	//			}
//	//		}
//	//		
//	//	}
//	
//	//	public void UI_OnUnEquip() {
//	//		
//	//		if (UI_EquippedItemListView.SelectedItems.Count >0 ) {
//	//			for (int i = 0 ; i< UI_EquippedItemListView.SelectedItems.Count; i++) {
//	//				Debug.Log(UI_EquippedItemListView.SelectedItems[i].Text);
//	//				CSVR_ApplicationManager.instance.OnUnEquipItem(UI_EquippedItemListView.SelectedItems[i].Text);
//	//				
//	//			}
//	//		}
//	//		
//	//	}
//	//Login
//	public void UI_Logout() {
//		vp_MPConnection.StayConnected = false;
//		PhotonNetwork.Disconnect();
//		CSVR_UIChatManager.instance.LeaveChat();
//		Application.LoadLevel("CSVR_CleaningScene");
//		//StartCoroutine(DelayCall(CSVR_UI.LOGIN));
//		//		CSVR_UIHome_v2.instance.mainGunInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.pistolInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.armorInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.meleInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.grenadeInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.ClearListWeapon();
//		//
//		//		for(int i = 1 ; i <= 3; i++)
//		//		{
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Pistol.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"MainGun.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Mele.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Grenade.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Armor.Weapon");
//		//
//		//		}
//		//Application.LoadLevel ("CSVR_CleaningScene");
//		//CSVR_ApplicationManager.instance.ExitGame();
//	}
//	public void UI_ReLogin() {
//		StartCoroutine(DelayCall(CSVR_UI.LOGIN));
//		//		CSVR_UIHome_v2.instance.mainGunInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.pistolInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.armorInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.meleInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.grenadeInvContent.transform.ClearLogOut();
//		//		CSVR_UIHome_v2.instance.ClearListWeapon();
//		//
//		//		for(int i = 1 ; i <= 3; i++)
//		//		{
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Pistol.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"MainGun.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Mele.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Grenade.Weapon");
//		//			CSVR_UIHome_v2.instance.DisableItemToGroup(i,"Armor.Weapon");
//		//
//		//		}
//		//Application.LoadLevel ("CSVR_CleaningScene");
//		//CSVR_ApplicationManager.instance.ExitGame();
//	}
//	//Registration
//	public void UI_SwithRegistration() {
//		StartCoroutine(DelayCall(CSVR_UI.REGISTRATION));
//		//		switchUI (CSVR_UI.REGISTRATION);
//	}
//	//	//Registration
//	//	public void UI_SwithRegistration() {
//	//		StartCoroutine(DelayCall(CSVR_UI.REGISTRATION));
//	//		//		switchUI (CSVR_UI.REGISTRATION);
//	//	}
//	//Home
//	public void UI_OnHome() {
//		//CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.xuatkichClip);
////		CSVR_MusicManager.instance.musicSource.Stop();
////		CSVR_MusicManager.instance.musicSource.loop = true;
////		CSVR_MusicManager.instance.musicSource.clip = CSVR_MusicManager.instance.menuClip;
////		CSVR_MusicManager.instance.musicSource.Play();
//		StartCoroutine(DelayCall(CSVR_UI.HOME));
//		//		switchUI (CSVR_UI.HOME);
//		
//	}
//	public void UI_OnHome_V3() {
//		//CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.xuatkichClip);
//		//		CSVR_MusicManager.instance.musicSource.Stop();
//		//		CSVR_MusicManager.instance.musicSource.loop = true;
//		//		CSVR_MusicManager.instance.musicSource.clip = CSVR_MusicManager.instance.menuClip;
//		//		CSVR_MusicManager.instance.musicSource.Play();
//		StartCoroutine(DelayCall(CSVR_UI.HOME_V3));
//		//		switchUI (CSVR_UI.HOME);
//		
//	}
//	public void UI_OnInfo() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.INFO));
//		//		switchUI (CSVR_UI.MAINGUN);
//		
//	}
//	//	//Setting
//	//	public void UI_OnSetting() {
//	//		StartCoroutine(DelayCall(CSVR_UI.SETTING));
//	//		//		switchUI (CSVR_UI.HOME);
//	//		
//	//	}
//	//Total GUn
//	public void UI_OnTotalGun() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.TOTALGUN));
//		//		switchUI (CSVR_UI.HOME);
//		
//	}
//	//shop
//	public void UI_OnEndGame() {
//		CSVR_UILevelUp.instance.isLevelUp = false;
//		switchUI (CSVR_UI.ENDGAME);
//	}
//	//shop
//	public void UI_OnShop() {
//		switchUI (CSVR_UI.SHOP);
//	}
//	//Inventory Gun
//	public void UI_OnInventory() {
//		switchUI (CSVR_UI.INVENTORY);
//	}
//	public void UI_OnInventoryMainGun() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.MAINGUN));
//		//		switchUI (CSVR_UI.MAINGUN);
//		
//	}
//	public void UI_OnInventoryPistolGun() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.PISTOLGUN));
//		//		switchUI (CSVR_UI.PISTOLGUN);
//		
//	}
//	public void UI_OnInventoryMeleGun() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.MELEGUN));
//		//		switchUI (CSVR_UI.MELEGUN);
//		
//	}
//	public void UI_OnInventoryArmorGun() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.ARMORGUN));
//		//		switchUI (CSVR_UI.ARMORGUN);
//		
//	}
//	public void UI_OnInventoryGrenadeGun() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.GRENADEGUN));
//		//		switchUI (CSVR_UI.GRENADEGUN);
//		
//	}
//	public void UI_OnShowMultiplayerLobby() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.xuatkichClip);
////		CSVR_MusicManager.instance.musicSource.Stop();
////		CSVR_MusicManager.instance.musicSource.loop = true;
////		CSVR_MusicManager.instance.musicSource.clip = CSVR_MusicManager.instance.modeClip;
////		CSVR_MusicManager.instance.musicSource.Play();
//
//		//		CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.inClip);
//		StartCoroutine(DelayCall(CSVR_UI.MULTIPLAYER));
//		//		switchUI (CSVR_UI.MULTIPLAYER);
//		
//	}
//	public void UI_OnBackMultiplayerLobby() {
//		//		CSVR_MusicManager.instance.Source.Stop();
//		//		CSVR_MusicManager.instance.Source.loop = true;
//		//		CSVR_MusicManager.instance.Source.clip = CSVR_MusicManager.instance.modeClip;
//		//		CSVR_MusicManager.instance.Source.Play();
//		//		CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.xuatkichClip);
//		//		CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.inClip);
//		CSVR_ApplicationManager.instance.BackToLobby();
//		CSVR_UIChatManager.instance.roomName = "";
//		//StartCoroutine(DelayCall(CSVR_UI.MULTIPLAYER));
//		//		switchUI (CSVR_UI.MULTIPLAYER);
//		
//	}
//	public void UI_OnBackMultiplayerLobbyV2() {
//		CSVR_ApplicationManager.instance.BackToLobby();
//	}
//	public void UI_OnFriend() {
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.selectModeClip);
//		StartCoroutine(DelayCall(CSVR_UI.FRIEND));
//	}
//
//	IEnumerator DelayCall (CSVR_UI state) {
//		yield return new WaitForSeconds(0.5f);
//		switchUI (state);
//	}
//}
