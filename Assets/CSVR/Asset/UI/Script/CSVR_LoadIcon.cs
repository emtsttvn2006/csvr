﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_LoadIcon : MonoBehaviour {
    Image img;
    public Sprite DefaultSprite;
	// Use this for initialization
	void Awake () {
	    img=GetComponent<Image>();
	}
    public void SetIcon(Sprite id)
    {
        img.sprite = id;
    }
}
