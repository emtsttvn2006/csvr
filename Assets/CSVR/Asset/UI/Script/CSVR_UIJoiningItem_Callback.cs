﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;
using Horus;

public class CSVR_UIJoiningItem_Callback : MonoBehaviour {

    public Image Avata;
    public Text TextName;
    public Image ImgLV;
    public Text TextLV;
    public Text TextStatus;

    public Button ButtonXoa;
    public Button ButtonChapNhan;

    [HideInInspector] UserInfo UserInfo;    
    [HideInInspector] string2 string2;    

    void Start()
    {
        this.TextName.text = "--";
        this.TextLV.text = "--";
        this.UserInfo = new UserInfo() ;
    }

    void ScrollCellIndex(int idx)
    {
        this.string2 = CSVR_UIClanList_XemMyClan.instance.listString_Joining[idx];

        int index = CSVR_UIClanList_XemMyClan.instance.GetIndex(string2.Value,
                CSVR_UIClanList_XemMyClan.instance.listUser);

        if (index != -1) this.UserInfo = CSVR_UIClanList_XemMyClan.instance.listUser[index];            

        if (UserInfo != null)
        {
            if (this.UserInfo.DisplayName != null && this.UserInfo.DisplayName != "")
            {
                this.TextName.text = this.UserInfo.DisplayName;
            }
            else
            {
                this.TextName.text = "Không xác định!!";
            }
            this.TextLV.text = this.UserInfo.AccountLevel.ToString();
            //this.ImgLV.sprite =
            this.TextStatus.text = string2.Key;
            switch (string2.Key)
            {
                case "XIN VÀO": ButtonChapNhan.gameObject.SetActive(true); break;
                case "ĐANG MỜI": ButtonChapNhan.gameObject.SetActive(false); break;
            }
        }
    }

    public void Button_Xoa_Click()
    {
        ClanMemberRemoveRequest ClanMemberRemoveRequest = new ClanMemberRemoveRequest();
        ClanMemberRemoveRequest.Add(string2.Value);

        HorusClientAPI.ClanMemberRemoveProcess(ClanMemberRemoveRequest,
            (result) =>
            {
                result.Clan.UpdateAllMemberArray();
                CSVR_UICLanList.instance.MyClan = result.Clan;                
                HorusManager.instance.Clan = new ClanInfo(result.Clan.roleId);
                CSVR_UICLanList.instance.UpdateClanList();
                CSVR_UIClanList_XemMyClan.instance.Show(CSVR_UICLanList.instance.MyClan);
                CSVR_UIJoining.instance.Open();
            },
            (error) =>
            {
                if (CSVR_UICLanList.instance.DebugError) Debug.Log("Cho ra đảo lỗi (0)");
                ErrorView.instance.ShowPopupError("CHO RA ĐẢO LỖI" + error.ErrorMessage);
            }
            , null);
    }

    public void Button_ChapNhan_Click()
    {
        ClanMemberAddRequest ClanMemberAddRequest = new ClanMemberAddRequest();
        ClanMemberAddRequest.usernames.Add(string2.Value);

        HorusClientAPI.ClanMemberAddProcess(ClanMemberAddRequest,
            (result) =>
            {
                if (result.error != null && result.error != "")
                {
                    ErrorView.instance.ShowPopupError("ĐỒNG Ý VÀO CLAN LỖI" + result.error);
                }
                else
                {
                    CSVR_UICLanList.instance.MyClan = result.Clan;
                    HorusManager.instance.Clan = new ClanInfo(result.Clan.roleId);
                    CSVR_UICLanList.instance.UpdateClanList();
                    CSVR_UIClanList_XemMyClan.instance.Show(CSVR_UICLanList.instance.MyClan);
                    CSVR_UIJoining.instance.Open();
                }
            },
            (error) =>
            {                
                ErrorView.instance.ShowPopupError("ĐỒNG Ý VÀO CLAN LỖI" + error.Error.ToString() + " " + error.ErrorMessage);
                
                ClanMemberRemoveRequest ClanMemberRemoveRequest = new ClanMemberRemoveRequest();
                ClanMemberRemoveRequest.Add(this.string2.Value);

                HorusClientAPI.ClanMemberRemoveProcess(ClanMemberRemoveRequest,
                    (result_remove) =>
                    {
                        CSVR_UICLanList.instance.MyClan = result_remove.Clan;
                        HorusManager.instance.Clan = new ClanInfo(result_remove.Clan.roleId);
                        CSVR_UICLanList.instance.UpdateClanList();
                        CSVR_UIClanList_XemMyClan.instance.Show(result_remove.Clan);
                        CSVR_UIJoining.instance.Open();
                    },
                    (error_remove) =>
                    {
                        if (CSVR_UICLanList.instance.DebugError) Debug.Log("Cho ra đảo lỗi (0)");
                        ErrorView.instance.ShowPopupError("CHO RA ĐẢO LỖI" + error.ErrorMessage);
                    }
                    , null);                
            }
            , null);
    }
}
