﻿using UnityEngine;
using System.Collections;
using ProgressBar;

public class CSVR_UILoadingPVE : SingletonBaseScreen<CSVR_UILoadingPVE> {
	[SerializeField] private ProgressBarBehaviour progressBar = null;
	void Start (){
		progressBar.IncrementValue (100);
	}
}
