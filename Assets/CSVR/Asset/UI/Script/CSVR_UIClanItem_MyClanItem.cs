﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;

public class CSVR_UIClanItem_MyClanItem : SingletonBaseScreen<CSVR_UIClanItem_MyClanItem> {

    public Image ImgBackGround;
    public Text TextOrder;
    public Image ImgAvatar;
    public Text TextClanName;
    public Text TextChuClan;
    public Text TextClanLV;
    public Text TextClanDiemHoatDong;
    public Text TextClanMember;

    public bool IsOn = false;

    [HideInInspector]
    public ClanResult ClanResult;

    public override void OpenChildrenToo()
    {        
        base.OpenChildrenToo();
        this.ClanResult = CSVR_UICLanList.instance.MyClan;
        if (this.ClanResult != null && this.ClanResult.roleId != "")
        {            
            Show();            
        } else
        {
            CloseChildrenToo();
        }
    }

    public override void CloseChildrenToo()
    {
        base.CloseChildrenToo();
        CSVR_UICLanList.instance.BigScale_ScrollView();
    }

    public void StableSibling()
    {
        this.transform.SetSiblingIndex(CSVR_UICLanList.instance.transform.GetSiblingIndex() + 1);
    }

    public void Hide()
    {
        this.ImgBackGround.gameObject.SetActive(false);
        CancelInvoke("StableSibling");
        CSVR_UICLanList.instance.BigScale_ScrollView();
    }

    public void Show()
    {
        this.ImgBackGround.gameObject.SetActive(true);
        this.ImgBackGround.color = (CSVR_UICLanList.instance.MyClan.roleId == this.ClanResult.roleId) ? Color.green : Color.white;
        this.TextOrder.text = "--";
        this.TextClanName.text = this.ClanResult.roleId;
        this.TextChuClan.text = this.ClanResult.chief;
        this.TextClanLV.text = this.ClanResult.level.ToString();
        this.TextClanDiemHoatDong.text = "chưa có!!";
        this.TextClanMember.text = this.ClanResult.memberCount.ToString();

        InvokeRepeating("StableSibling", 0.0f, 0.6f);
        CSVR_UICLanList.instance.SmallScale_ScrollView();
    }    

    public void Button_MyClanItem_Click()
    {
        CSVR_UICLanList.instance.ButtonXem.onClick.Invoke();
    }
}
