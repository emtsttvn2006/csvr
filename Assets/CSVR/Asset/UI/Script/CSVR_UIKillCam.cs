﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CnControls;
using UnityEngine.UI;

public class CSVR_UIKillCam : MonoBehaviour {
	public static CSVR_UIKillCam instance;
	/// <summary>
	/// Target to follow
	/// </summary>
	public Transform target = null;
	/// <summary>
	/// Enemys Tag
	/// </summary>
	public string TagTargets = "";
	/// <summary>
	/// Distance from camera to target
	/// </summary>
	public float distance = 10.0f;
	/// <summary>
	/// list with al enemys with same tag
	/// </summary>
	public List<Transform> OtherList = new List<Transform>();
	/// <summary>
	/// target name to follow
	/// </summary>
	public string Follow = "";
	/// <summary>
	/// in case targets is null see this
	/// </summary>
	public Transform Provide;
	/// <summary>
	/// Maxime Distance to target
	/// </summary>
	public float distanceMax = 15f;
	/// <summary>
	/// Min Distance to target
	/// </summary>
	public float distanceMin = 0.5f;
	/// <summary>
	/// X vector speed
	/// </summary>
	public float xSpeed = 120f;
	/// <summary>
	/// maxime y vector Limit
	/// </summary>
	public float yMaxLimit = 80f;
	/// <summary>
	/// minime Y vector limit
	/// </summary>
	public float yMinLimit = -20f;
	/// <summary>
	/// Y vector speed
	/// </summary>
	public float ySpeed = 120f;
	
	public bool Can_See_Other = true;
	
	public int m_wait = 1;


	
	private int CurrentOther = 0;
	private bool isSearch = false;
	private bool isWait = false;
	private float x;
	private float y;

	private Button nextTarget;
	private Button previousTarget;
	private CSVR_UICamView camView;

	private CSVR_ServerConnector _cacheServerConnectorInstance;
	private CSVR_PlayerDamageHandler _cachePlayerDamageHandler;
	private vp_PlayerEventHandler _cachePlayerEventHandler;

	void Awake(){
		instance = this;
	}
	void Start(){
		camView = CSVR_UIPlay.instance.camView;
		previousTarget = CSVR_UIPlay.instance.camViewButton [0];
		nextTarget = CSVR_UIPlay.instance.camViewButton [1];
		previousTarget.onClick.AddListener (() => UpdateTargetPrevious ());
		nextTarget.onClick.AddListener (() => UpdateTargetNext ());
	}
	void OnEnable()
	{
		this.x = 30f;
		this.y = 30f;

		//InvokeRepeating ("UpdateOtherList", 1f, 1f);
		if (m_wait > 0)
		{
			isWait = true;
			StartCoroutine(Wait());
		}
		UpdateOtherList();
		UpdateTargetInfo ();
	}
	
	void OnDisable()
	{
		CancelInvoke ("UpdateOtherList");
		CancelInvoke("RespawnTime");
		CancelInvoke ("UpdateTargetInfo");
	}
	
	/// <summary>
	/// update list of targets
	/// </summary>
	void UpdateOtherList()
	{

		if (TagTargets == "") {
			if(Provide.GetComponent<CSVR_ServerConnector>().idTeams == 1){
				TagTargets = "PolicePlayer";
			}else{
				TagTargets = "TerrorisPlayer";
			}
		}
		OtherList.Clear();
		GameObject[] others = GameObject.FindGameObjectsWithTag(TagTargets);
		foreach (GameObject ot in others)
		{
			CSVR_FPPlayerDamageHandler fplayerDame = ot.GetComponent<CSVR_FPPlayerDamageHandler>();
			if(fplayerDame == null)
				OtherList.Add(ot.transform);	
		}
	}
	
	/// <summary>
	/// resfresh all
	/// </summary>
	void Refresh()
	{
		if (GameObject.Find(Follow) != null)
		{
			target = GameObject.Find(Follow).transform;
		}
		else
		{
			if (!Can_See_Other)
			{
				target = Provide;
			}
			else
			{
				OtherList.Clear();
				GameObject[] others = GameObject.FindGameObjectsWithTag(TagTargets);
				foreach (GameObject ot in others)
				{
					CSVR_FPPlayerDamageHandler lala = ot.GetComponent<CSVR_FPPlayerDamageHandler> ();
					if(lala == null)
						OtherList.Add(ot.transform);
				}
			}
		}
		
	}
	
	void LateUpdate () {
		if (!isWait)
		{
			CamMovements();
			//UpdateTarget();
		}
	}

	void CamMovements()
	{
		if (this.target != null)
		{
			this.x += ((CnInputManager.GetAxis("Touch Horizontal") * this.xSpeed) * this.distance) * 0.02f;
			this.y -= (CnInputManager.GetAxis("Touch Vertical") * this.ySpeed) * 0.02f;
			this.y = ClampAngle(this.y, this.yMinLimit, this.yMaxLimit);
			Quaternion quaternion = Quaternion.Euler(this.y, this.x, 0f);

			//zoom in zoom out
			if (Input.touchCount == 2)
			{
				// Store both touches.
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne = Input.GetTouch(1);
				
				// Find the position in the previous frame of each touch.
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
				
				// Find the magnitude of the vector (the distance) between the touches in each frame.
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
				
				// Find the difference in the distances between each frame.
				float deltaMagnitudeDiff = -0.05f*( prevTouchDeltaMag - touchDeltaMag);
				
				this.distance = Mathf.Clamp(this.distance - (deltaMagnitudeDiff * 5f), distanceMin, distanceMax);
			
			}


			Vector3 vector = new Vector3(0f, 0f, -distance);
			Vector3 vector2 = target.position;
			vector2.y = target.position.y + 1f;
			Vector3 vector3 = (quaternion * vector) + vector2;
			transform.rotation = quaternion;

			transform.position = vector3;

		}
		if (isWait)
		{
			transform.LookAt(target);
		}
	}
	void UpdateTargetPrevious()
	{
		CancelInvoke ("UpdateTargetInfo");
		isSearch = true;
		if (CurrentOther <= OtherList.Count  && CurrentOther >= 0)
		{
			CurrentOther --;
		}
		if ( CurrentOther <= OtherList.Count)
		{
			CurrentOther = 0;
		}
		
		
		if (!isSearch)
		{
			if (GameObject.Find(Follow) != null)
			{
				target = GameObject.Find(Follow).transform;
			}
			else
			{
				target = Provide;
			}
		}
		else
		{
			if (OtherList.Count > 0 && Can_See_Other)
			{
				target = OtherList[CurrentOther];
			}
			else
			{
				target = Provide;
			}
		}
		InvokeRepeating ("UpdateTargetInfo", 1f, 1f);
	}
	void UpdateTargetNext()
	{
		CancelInvoke ("UpdateTargetInfo");
		isSearch = true;
		if (CurrentOther <= OtherList.Count  && CurrentOther >= 0)
		{
			CurrentOther ++;
		}
		if ( CurrentOther >= OtherList.Count)
		{
			CurrentOther = 0;
		}

		
		if (!isSearch)
		{
			if (GameObject.Find(Follow) != null)
			{
				target = GameObject.Find(Follow).transform;
			}
			else
			{
				target = Provide;
			}
		}
		else
		{
			if (OtherList.Count > 0 && Can_See_Other)
			{
				target = OtherList[CurrentOther];
			}
			else
			{
				target = Provide;
			}
		}
		InvokeRepeating ("UpdateTargetInfo", 0, 1f);
	}


	void UpdateTargetInfo(){

		if (target != null) {
			_cacheServerConnectorInstance = target.GetComponent<CSVR_ServerConnector> ();
			_cachePlayerDamageHandler = target.GetComponent<CSVR_PlayerDamageHandler> ();
			_cachePlayerEventHandler = target.GetComponent<vp_PlayerEventHandler> ();

			if (_cacheServerConnectorInstance != null && _cachePlayerDamageHandler != null && _cachePlayerEventHandler != null) {
				camView.SetPlayerInfo ((int)_cachePlayerDamageHandler.CurrentHealth,
					((int)_cachePlayerDamageHandler.CurrentAmor).ToString (),
					_cacheServerConnectorInstance.PlayfabName,
					_cacheServerConnectorInstance.Level, 
					_cachePlayerEventHandler.CurrentWeaponName.Get ());
			}
		}
	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360f)
		{
			angle += 360f;
		}
		if (angle > 360f)
		{
			angle -= 360f;
		}
		return Mathf.Clamp(angle, min, max);
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(m_wait);
		isWait = false;
		
	}
	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
	public void UI_OnDisable(){
		this.gameObject.SetActive (false);
	}
}