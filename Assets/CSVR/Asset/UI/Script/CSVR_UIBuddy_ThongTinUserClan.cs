﻿using UnityEngine;
using System.Collections;
using Horus.ClientModels;
using UnityEngine.UI;
using Horus;
using LitJson;

public class CSVR_UIBuddy_ThongTinUserClan : SingletonBaseScreen<CSVR_UIBuddy_ThongTinUserClan>
{
    public Image ImgAvata;
    public Text TextName;
    public Image ImgQuanHam;
    public Image ImgLevel;
    public Text TextLV;

    public Text TextID;
    public Text TextClan;
    public Text TextRank;
    public Text TextKill;
    public Text TextHeadshot;
    public Text TextKillDeath;

    public Button ButtonKetban;
    public Button ButtonKick;

    [HideInInspector]
    UserInfo UserInfo;

    public void Show(string Name)
    {
        ButtonKick.gameObject.SetActive(false);
        ButtonKetban.gameObject.SetActive(false);

        GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();
        GetUserInfoRequest.Add(Name);

        CSVR_UIWaitingIcon.instance.Open();

        HorusClientAPI.GetUserInfo(GetUserInfoRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                if (result.users != null && result.users.Count > 0)
                {
                    this.UserInfo = result.users[0];

                    if (this.UserInfo.UserName != "")
                    {
                        this.TextName.text = this.UserInfo.DisplayName;
                        this.TextLV.text = "Cấp " + this.UserInfo.AccountLevel.ToString();
                        this.ImgLevel.sprite = UISpriteManager.instance.GetLevelSprite(this.UserInfo.AccountLevel);
                        this.TextID.text = this.UserInfo._id.ToString();

                        ClanInfo ClanInfo = null;

                        if (this.UserInfo.clan != null)
                        {
                            ClanInfo = JsonMapper.ToObject<ClanInfo>(this.UserInfo.clan.ToString());
                        }

                        if (ClanInfo != null && ClanInfo.clanName != "")
                        {
                            this.TextClan.text = ClanInfo.clanName;
                        }
                        else
                        {
                            this.TextClan.text = "--";
                        }
                        
                        this.TextRank.text = "--";
                        this.TextKill.text = this.UserInfo.AccountKill.ToString();
                        this.TextHeadshot.text = this.UserInfo.AccountHeadShot.ToString();

                        if (this.UserInfo.AccountDead > 0)
                        {
                            this.TextKillDeath.text = (this.UserInfo.AccountKill / this.UserInfo.AccountDead).ToString();
                        }
                        else
                        {
                            this.TextKillDeath.text = "100 %";
                        }

                        if (HorusManager.instance.Clan != null
                        && this.UserInfo.clan != null
                        && HorusManager.instance.Clan.clanName == ClanInfo.clanName
                        && CSVR_UICLanList.instance.MyClan != null
                        && CSVR_UICLanList.instance.MyClan.roleId != ClanInfo.clanName
                        && (CSVR_UICLanList.instance.MyClan.chief == HorusManager.instance.Clan.clanName || CSVR_UICLanList.instance.MyClan.GetViceIndex(HorusManager.instance.playerUserName) != -1))
                        {
                            ButtonKick.gameObject.SetActive(true);
                        }
                    } else
                    {
                        if (DebugError) Debug.Log("Tên trống!!");
                    }
                } else
                {
                    if (DebugError) Debug.Log("Không hiểu chuyển gì đang xảy ra luôn @@!!");
                }
            }, (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                if (DebugError) Debug.Log("Cập nhật thông tin User Lỗi");
                ErrorView.instance.ShowPopupError("Cập nhật thông tin User Lỗi");
            }, null);
    }
}
