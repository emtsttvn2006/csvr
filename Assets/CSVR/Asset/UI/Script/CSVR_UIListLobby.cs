﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.Json;
using Horus.ClientModels;


public class LobbyItem
{
	public string LobbyId { get; set;}
	public string LobbyName { get; set;}
	public string LobbyIp { get; set;}
	//public bool Interactable { get; set;}
	public int PlayerMax { get; set;}
	public int Players { get; set;}
}

public class Lobbys
{
	public List<LobbyItem> ListLobby{ get; set;}
}
public class CSVR_UIListLobby : SingletonBaseScreen<CSVR_UIListLobby> {



	[SerializeField] private Transform 	Lobby_Item_Parent;
	[SerializeField] private GameObject Lobby_Item;

	[SerializeField] private List<CSVR_UILobbyItem> ListLobbyCache;
	private Dictionary<string,RP_ServerProfile> _roomProfile;
	private string keyServer = "s1";

	void Start(){
		
		_roomProfile = HomeScreen.instance.roomProfile;
		HienThi ();

	}
	public override void OnEnable()
	{
		base.OnEnable ();

		GetRoomStatic ();
	}
	void HienThi(){

		int _listListLobbyCount = _roomProfile[keyServer].lobbyList.Count;
		int _listLobbyCacheCount = ListLobbyCache.Count;
		int _forCount = (_listListLobbyCount > _listLobbyCacheCount) ? _listListLobbyCount : _listLobbyCacheCount;

		int i=0;
		foreach (KeyValuePair<string, RP_LobbyProfile> keyValuePair in _roomProfile[keyServer].lobbyList)
		{
			LobbyItem item = new LobbyItem ();
			item.LobbyId = keyValuePair.Key;
			item.LobbyName = keyValuePair.Value.name;
			item.LobbyIp = keyValuePair.Value.ip;
			item.PlayerMax = keyValuePair.Value.max;
			if (i < _listLobbyCacheCount) {
				ListLobbyCache [i].GetInfo (item);
			} else {
				InstanceItem (item);
			}
			i++;
		}
	}

	public void InstanceItem(LobbyItem item)
	{
		GameObject f = Instantiate(Lobby_Item) as GameObject;
		CSVR_UILobbyItem info = f.GetComponent<CSVR_UILobbyItem>();
		info.GetInfo (item);
		f.transform.SetParent (Lobby_Item_Parent, false);
		ListLobbyCache.Add(info);

	}

	public void UI_OnCloseThis_Click(){
		this.Close ();
	}

	byte ErrorGetRoomStaticCount = CSVR_GameSetting.errorCountCallBack;
	private void GetRoomStatic()
	{
		HorusClientAPI.RoomStatic(
			new RoomStaticRequest(){
				serverId = keyServer
			}, 
			(result) => {
				foreach (KeyValuePair<string, int> pair in result.roomStatic[keyServer]){
					for(int i=0;i<ListLobbyCache.Count;i++){
						if(pair.Key == ListLobbyCache[i].Lobby_Id){
							ListLobbyCache[i].Player_Count = pair.Value;
						}
					}
				}
			},
			(error) => {
				if(ErrorGetRoomStaticCount > 0){
					ErrorGetRoomStaticCount--;
					Invoke ("GetRoomStatic", 0.5f);
				}
			}
		);
	}
}

