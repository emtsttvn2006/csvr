﻿using UnityEngine;
using System.Collections;
using Horus.ClientModels;
using UnityEngine.UI;
using UnityEngine.Events;

public class CSVR_UIBuddyWaitingItem_Callback : MonoBehaviour
{
    Color Color_Online = Color.green;
    Color Color_Offline = Color.white;

    public Image Avatar;
    public Text TextName;
    public Image ImgLevel;
    public Text TextLevel;
    public Image ImgOnline;
    public Text TextStatus;
    public Button ButtonChoiCung;
    public Button ButtonList;

    public string RoomPlaying = "";

    [HideInInspector]
    public RelationshipInfo RelationshipInfo { get; set; }

    void ScrollCellIndex(int idx)
    {
        if (CSVR_UIBuddy.instance.INPTimkiem.text == "")
        {
            this.RelationshipInfo = CSVR_UIBuddy.instance.listWaiting[idx];
        }
        else
        {
            this.RelationshipInfo = CSVR_UIBuddy.instance.listTemporary[idx];
        }

        TextName.text = this.RelationshipInfo.Partner.DisplayName;
        ImgLevel.sprite = UISpriteManager.instance.GetLevelSprite(this.RelationshipInfo.Partner.AccountLevel);
        TextLevel.text = this.RelationshipInfo.Partner.AccountLevel.ToString();

        if (this.RelationshipInfo.Online)
        {
            ImgOnline.color = Color.green;
            TextStatus.text = "Đang trong trận!!";

            int index = CSVR_UIBuddy.instance.listBanbePhoton.FindIndex((Banbe) => Banbe.Name == this.RelationshipInfo.Partner._id);
            if (index != -1)
            {
                RoomPlaying = CSVR_UIBuddy.instance.listBanbePhoton[index].Room;
                if (RoomPlaying != "")
                {
                    ButtonChoiCung.gameObject.SetActive(true);
                    ButtonChoiCung.onClick.AddListener(new UnityAction(() => {
                        CSVR_UIWaitingIcon.instance.Open();
                        CSVR_UIWaitingIcon.instance.ShowButtonOk(true);
                        CSVR_UIWaitingIcon.instance.SetAction(new UnityAction(() =>
                        {
                            CSVR_UIWaitingIcon.instance.Close();
                            CSVR_GameSetting.mapName = RoomPlaying;
                            CSVR_MPConnection.instance.LeaveRoom();
                        }));

                        CSVR_MPConnection.instance.OnQuickJoinedRoom(true);
                        CSVR_MPConnection.instance.JoinRoom();

                        CSVR_GameSetting.TuTaoPhong = false;
                        CSVR_GameSetting.maxPlayer = 11;
                        CSVR_GameSetting.maxSkill = 50;
                        CSVR_GameSetting.timeMatch = 30;
                    }));
                }
            }
        }
        else
        {
            ImgOnline.color = Color.yellow;
            TextStatus.text = "Không liên lạc được!!";
            ButtonChoiCung.gameObject.SetActive(false);
        }
    }

    public void CorrectRelationShipInfo()
    {

    }

    public void CSVR_UIBuddyItem_ListClick()
    {
        AddTemplate_XemThongTin();
        AddTemplate_Chat();
    }

    public void AddTemplate_XemThongTin()
    {
        AButton AButton = new AButton();
        AButton.tittle = "THÔNG TIN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIBuddy_ThongTinUserClan.instance.Open();
            CSVR_UIBuddy_ThongTinUserClan.instance.Show(this.RelationshipInfo.PartnerId);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }

    public void AddTemplate_Chat()
    {
        AButton AButton = new AButton();
        AButton.tittle = "NÓI CHUYỆN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIClanList_XemClan.instance.Close();
            CSVR_UICLanList.instance.Close();
            CSVR_UIChat.instance.Open();
            CSVR_UIChat.instance.InitPrivateChat(this.RelationshipInfo.PartnerId);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }
}
