﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus.ClientModels;
using Horus;

public class Gift_Diem_Danh_Item_Callback : MonoBehaviour
{

    public Text Tittle;

	private QuestInfo questInfo { get; set; }
	[SerializeField]private CSVR_Gift_Item_Moi_Ngay_Callback[] listItemReward;

    void ScrollCellIndex(int idx)
    {
		if(transform.parent.name == "7DayContent")
			this.questInfo = CSVR_UIGift.instance.questList7Day[idx];
		else if(transform.parent.name == "30DayContent")
			this.questInfo = CSVR_UIGift.instance.questList30Day[idx];
		else if(transform.parent.name == "LevelUpContent")
			this.questInfo = CSVR_UIGift.instance.questListLevelUp[idx];
		
		HUD_ShowInfo_Run ();
    }

	private void HUD_ShowInfo_Run(){
		int i = 0;
		listItemReward [0].gameObject.SetActive (false);
		listItemReward [1].gameObject.SetActive (false);
		listItemReward [2].gameObject.SetActive (false);
		Tittle.text = questInfo.displayName;
		//Debug.Log(Horus.Json.JsonWrapper.SerializeObject (questInfo.reward, Horus.Internal.HorusUtil.ApiSerializerStrategy));
		if (questInfo.reward == null || questInfo.reward.items == null)
			return;
		
		foreach (KeyValuePair<string, ItemSlot> pair in questInfo.reward.items)
		{

			string idModel = pair.Value.ItemId;
			listItemReward [i].gameObject.SetActive (true);
			if (idModel.Contains (GameConstants.VNITEMPREFIX_3Day)) {
				idModel = idModel.Replace (GameConstants.VNITEMPREFIX_3Day, GameConstants.VNITEMPREFIX_Empty);
				listItemReward [i].TextItem.text = "3 ngày";
			} 
			else if (idModel.Contains (GameConstants.VNITEMPREFIX_7Day)) {
				idModel = idModel.Replace (GameConstants.VNITEMPREFIX_7Day, GameConstants.VNITEMPREFIX_Empty);
				listItemReward [i].TextItem.text = "7 ngày";
			}
			else if (idModel.Contains (GameConstants.VNITEMPREFIX_30Day)) {
				idModel = idModel.Replace (GameConstants.VNITEMPREFIX_30Day,  GameConstants.VNITEMPREFIX_Empty);
				listItemReward [i].TextItem.text = "30 ngày";
			} 
			else {
				listItemReward [i].TextItem.text = "Vĩnh viễn";
			}

			listItemReward[i].ImageItem.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+idModel);
			listItemReward [i].ImageItem.SetNativeSize ();
			if (idModel.Contains (GameConstants.VNITEMID_Coin) || idModel.Contains (GameConstants.VNITEMID_Gold)) {
				listItemReward [i].TextItem.text = (pair.Value.number.min == pair.Value.number.max) ? pair.Value.number.min.ToString() :( pair.Value.number.min+"-"+pair.Value.number.max);
				listItemReward [i].ImageItem.SetNativeSize ();
			}

			i++;
		}
	}

	public void HUD_RewardItem_Click(){
		HorusClientAPI.GetQuestProcess(new GetQuestProcessRequest(){
			questId = questInfo.questId
		}, 
			(result) => {
				if(result.quest.questProcess.Contains(GameConstants.QUEST_FINISH)){
					//if(result.quest.itemsOwn.Count > 1){
						CSVR_UIRewardItem.instance.Open();
						CSVR_UIRewardItem.instance.ResetListItemReward();
					//}
					for (int i = 0; i < result.quest.itemsOwn.Count; i++) {

						if(result.quest.itemsOwn [i].CatalogVersion != "BundleGiftCode" && (int)result.quest.itemsOwn [i].RemainingUses != 0){
							CSVR_UIRewardItem.instance.RewardItem(i,result.quest.itemsOwn [i]);
						}
					}
					CSVR_UIRewardItem.instance.UI_OpenCSVR_UIRewardItem_Click();
					TopBar.instance.UpdateVirtualCurrency();
				}else if(result.quest.questProcess.Contains(GameConstants.QUEST_DONE)){
					ErrorView.instance.ShowPopupError("Bạn đã nhận thưởng.");
				}else if(result.quest.questProcess.Contains(GameConstants.QUEST_DOING)){
					ErrorView.instance.ShowPopupError("Đang trong quá trình làm.");
				}else {
					ErrorView.instance.ShowPopupError("Chưa thế nhận thưởng.");
				}

			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Lỗi nhận thưởng");
			}
		);
	}
}
