﻿using UnityEngine;
using UnityEngine.UI;

public class CSVR_UIRoomLobbyScreen_RoomItemCallback : MonoBehaviour 
{
    public Text roomName;
    public Text mapName;
    public Text modeName;

    public Text roomStatus;
    public Text playerHost;
    public Text playerCount;    

    [HideInInspector]
    public RoomInfo _roomInfo = null;
    //Phải giữ nguyên tên hàm là ScrollCellIndex do gọi trong code
    void ScrollCellIndex (int idx) 
    {

        this._roomInfo = CSVR_UIRoomLobbyScreen.instance.roomInfoList[idx];

		if (idx == 0) {
			CSVR_UIRoomLobbyScreen.instance.ShowChosen(this._roomInfo);
		}
			
		
		roomName.text = _roomInfo.customProperties[GameConstants.Name].ToString();
		mapName.text = HomeScreen.instance.lobbyResult.ModeAvaiable [_roomInfo.customProperties[GameConstants.Mode].ToString()];
		modeName.text = HomeScreen.instance.lobbyResult.MapAvaiable [_roomInfo.customProperties[GameConstants.Map].ToString()];
		roomStatus.text = "-";
		playerHost.text = _roomInfo.customProperties[GameConstants.Host].ToString();
   
		if(_roomInfo.maxPlayers % 2 == 1)
			playerCount.text = (_roomInfo.playerCount - 1) + "/" + (_roomInfo.maxPlayers - 1);
		else
			playerCount.text = _roomInfo.playerCount + "/" + _roomInfo.maxPlayers;
	}

	public void UI_OnJoinRoom_Click(){
		HomeScreen.instance.SetLoadingMapCallBack (CSVR_UIRoomLobbyScreen.instance.UI_LoadingMap_CallBack);
		CSVR_MPConnection.instance.JoinRoom(_roomInfo.name);
	}

    public void Click()
    {
        CSVR_UIRoomLobbyScreen.instance.ShowChosen(this._roomInfo);
    }

    #region Rainbow 
    /*
    // http://stackoverflow.com/questions/2288498/how-do-i-get-a-rainbow-color-gradient-in-c
    public static Color Rainbow(float progress)
    {
        progress = Mathf.Clamp01(progress);
        float r = 0.0f;
        float g = 0.0f;
        float b = 0.0f;
        int i = (int)(progress * 6);
        float f = progress * 6.0f - i;
        float q = 1 - f;

        switch (i % 6)
        {
            case 0:
                r = 1;
                g = f;
                b = 0;
                break;
            case 1:
                r = q;
                g = 1;
                b = 0;
                break;
            case 2:
                r = 0;
                g = 1;
                b = f;
                break;
            case 3:
                r = 0;
                g = q;
                b = 1;
                break;
            case 4:
                r = f;
                g = 0;
                b = 1;
                break;
            case 5:
                r = 1;
                g = 0;
                b = q;
                break;
        }
        return new Color(r, g, b);
    }*/
    #endregion
}
