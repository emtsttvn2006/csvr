﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UISanLungOne : SingletonBaseScreen<CSVR_UISanLungOne> {

	[SerializeField] private Sprite mapIcon;
	[SerializeField] private Button[] maps;

	[SerializeField] private int t = -1;
	public override void OnEnable()
	{
		TopBar.instance.SetBackButton(new UnityAction(() =>{
			this.Close();

			CharacterView.instance.Open();
			MainView.instance.Open();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			this.Close();
			CharacterView.instance.Open();
			CSVR_UIShop.instance.Open();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));
		TopBar.instance.ShopOrInVText.text = "Cửa hàng";
	}
	public void UI_SelectMap_Click(int index){
		for (int i = 0; i < maps.Length; i++) {
			if (i == index) {
				if (t != -1) {
					maps [t].GetComponent<Animator> ().enabled = false;
					maps [t].GetComponent<Image> ().sprite = mapIcon;
					maps [t].transform.localScale = Vector3.one;
					maps [t].transform.GetChild(0).localScale = Vector3.one;
					t = index;
				} else {
					t = index;
				}
				maps [index].GetComponent<Animator> ().enabled = true;
				maps [index].transform.localScale = new Vector3(2f,2f,2f);
				maps [index].transform.GetChild(0).localScale = new Vector3(0.5f,0.5f,0.5f);

				break;
			}
		}
	}
	public void UI_StartMode_Click(){
		CSVR_GameSetting.modeName = "CSVR_ZombieSurvival";
		CSVR_GameSetting.mapName = "Map_NV_1_Station";
	}
	public void UI_StartMap_Click(){
		this.Close ();
		CharacterView.instance.Open();
		CSVR_UIInventory.instance.Open();
		CSVR_UIInventory.instance.UI_OnSanLungTwoOn();

		TopBar.instance.SetBackButton(new UnityAction(() =>{
			CSVR_UIInventory.instance.Close();
			CharacterView.instance.Close();
			CSVR_UISanLungOne.instance.Open();
		}));   
	}
}
