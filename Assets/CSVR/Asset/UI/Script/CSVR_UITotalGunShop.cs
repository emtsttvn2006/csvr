//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using PlayFab;
//using PlayFab.ClientModels;
//using PlayFab.Internal;
//
//
//public class CSVR_UITotalGunShop : MonoBehaviour {
//	public static CSVR_UITotalGunShop instance;
//	public Text UI_CurrencyGCTxt;
//	public Text UI_CurrencyGDTxt;
//	
//	public GameObject mainGunContainer,pistolContainer,meleContainer,grenadeContainer,armorContainer;
//	public GameObject mainGunPanel;
//	
//	public GameObject dPanel;
//	public Text dName;
//	public Image dIcon;
//	public GameObject dInfo;
//	public GameObject dDay;
//	//	public GameObject dFix;
//	public Button dBuy;
//	public Button dUpgrade;
//	public Button dEquip;
//	public Button dExtend;
//	public Button dAution;
//	public Button dSell;
//	private string itemClass,dayExpired,idTemp,nameTemp;
//	private bool group1,group2,group3,damua;
//	public Sprite[] iconType;
//	CSVR_GunInfo infoGunTemp;
//	CSVR_DayInfo infoDayTemp;
//	float fillAmount;
//	int lvGun = 0; 
//
//	GameObject destroyTHIS;
//	void Awake()
//	{
//		instance = this;
//	}
//	
//	public void HienThi()
//	{
//		mainGunContainer.transform.Clear();
//		for(int i = 0; i < CSVR_GameSetting.shopItems.Count; i++)
//		{
////			ItemInstance item = CSVR_GameSetting.FindItemInListInventoryItems(transform.name);
//			GameObject mainGunClone = Instantiate(mainGunPanel);
//			itemClass = CSVR_GameSetting.shopItems[i].ItemClass;
//			//kiem tra xem co ton tai trong thung do khong
//			if(CSVR_GameSetting.FindItemInListInventoryItems(CSVR_GameSetting.shopItems[i].ItemId) != null)
//			{
//				continue;
//
//			}else{
//				//chua mua
//				lvGun = 0;
//				if(CSVR_GameSetting.shopItems[i].CustomData != null){
//					infoGunTemp = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GunInfo>(CSVR_GameSetting.shopItems[i].CustomData);
//					//					Debug.Log("CSVR_GameSetting.shopItems[i].CustomData"+CSVR_GameSetting.shopItems[i].CustomData);
//					dayExpired = infoGunTemp.D0.ToString()+"Day 0Hours 0Miutes";
//				}
//				idTemp = CSVR_GameSetting.shopItems[i].ItemId;
//				nameTemp = CSVR_GameSetting.shopItems[i].DisplayName;
//				damua = false;
//				
//				fillAmount = 1;
//				
//				mainGunClone.transform.GetChild(3).GetComponent<Image>().sprite = iconType[1];
//				mainGunClone.transform.GetChild(4).GetComponent<Text>().text = CSVR_GameSetting.itemPrices[idTemp].ToString();
//				
//			}
//			mainGunClone.name = mainGunClone.transform.GetChild(1).GetComponent<Text>().text = CSVR_GameSetting.shopItems[i].DisplayName;
//			mainGunClone.transform.GetChild(2).GetComponent<Image>().sprite = CSVR_AssetManager.itemIconDic[idTemp];
//			if(itemClass == "Pistol.Weapon")
//				mainGunClone.transform.SetParent(pistolContainer.transform);
//			else if(itemClass == "MainGun.Weapon")
//				mainGunClone.transform.SetParent(mainGunContainer.transform);
//			else if(itemClass == "Mele.Weapon")
//				mainGunClone.transform.SetParent(meleContainer.transform);
//			else if(itemClass == "Grenade.Weapon")
//				mainGunClone.transform.SetParent(grenadeContainer.transform);
//			else if(itemClass == "Armor.Weapon")
//				mainGunClone.transform.SetParent(armorContainer.transform);
//
//			mainGunClone.transform.localScale = Vector3.one;
////			Debug.Log("itemClass "+itemClass +" displayname "+CSVR_GameSetting.shopItems[i].DisplayName);
//			Button b = mainGunClone.GetComponent<Button>();
//			AddListenerItems(b,mainGunClone,idTemp,nameTemp,itemClass,dayExpired,fillAmount,damua,group1,group2,group3,infoGunTemp,lvGun); // Usi
//			if(i==0){
//				DetailPanel(mainGunClone,idTemp,nameTemp,itemClass,dayExpired,fillAmount,damua,group1,group2,group3,infoGunTemp,lvGun);
//			}
//		}
//	}
//	private void AddListenerItems(Button b,GameObject _obj,string id,string name,string iClass,string day,float fillA,bool damua,bool gr1,bool gr2,bool gr3,CSVR_GunInfo gun,int lv)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> DetailPanel(_obj,id,name,iClass,day,fillA,damua,gr1,gr2,gr3,gun,lv));
//	}
//	
//	private void DetailPanel(GameObject _obj,string id,string n,string itemClass,string day,float fill,bool damua,bool group1,bool group2,bool group3,CSVR_GunInfo gun,int lv)
//	{
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.changeGroupClip);
//		if(dPanel.activeSelf == false)
//			dPanel.SetActive(true);
//		
//		//		dIcon.sprite = Resources.Load<Sprite>("Sprites/"+code);
//		dIcon.sprite = CSVR_AssetManager.itemIconDic[id];
//		dAution.interactable = false;
//		dSell.interactable = false;
//		dDay.transform.GetChild(0).GetComponent<Image>().fillAmount = fill;
//		dDay.transform.GetChild(1).GetComponent<Text>().text = day;
//		dName.text = n;
//		
//		//		dInfo.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G8.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolDameMax"));
//		//		dInfo.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G9.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolAPMax"));
//		//		dInfo.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G2.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolROFMax"));
//		//dInfo.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = gun.G8.ToString();
//		dInfo.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = gun.G5.ToString()+" độ";
//		dInfo.transform.GetChild(2).GetChild(1).GetComponent<Text>().text = gun.G4.ToString()+" s";
//		dInfo.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = gun.G3.ToString()+" kg";
//		dInfo.transform.GetChild(4).GetChild(1).GetComponent<Text>().text = gun.G7.ToString() +" m";
//		for(int s = 0; s < 9; s++){
//			if(s < lv)
//				dPanel.transform.GetChild(0).GetChild(4).GetChild(s).gameObject.SetActive(true);
//			else
//				dPanel.transform.GetChild(0).GetChild(4).GetChild(s).gameObject.SetActive(false);
//		}
//		if(damua){
//			dBuy.gameObject.SetActive(false);
//			dExtend.gameObject.SetActive(false);
//			dUpgrade.gameObject.SetActive(true);
//			dEquip.gameObject.SetActive(true);
//			dAution.gameObject.SetActive(true);
//			dSell.gameObject.SetActive(true);
//			Button u = dUpgrade;
//			if((lv+1) == 9)
//				u.interactable = false;
//			else
//				AddListenerUpgradeItem(u,id,lv);
//			
//			Button e = dEquip;
//			
//			if((CSVR_UIHome_v2.instance.currentGroup == 1 && !group1) || (CSVR_UIHome_v2.instance.currentGroup == 2 && !group2) || (CSVR_UIHome_v2.instance.currentGroup == 3 && !group3)){
//				dEquip.transform.GetChild(0).GetComponent<Text>().text = "Đeo lên";
//				AddListenerEquipItem(e,id,itemClass);
//			}else{
//				dEquip.transform.GetChild(0).GetComponent<Text>().text = "Gỡ xuống";
//				AddListenerUnEquipItem(e,id);
//			}
//		}else{
//			dBuy.gameObject.SetActive(true);
//			dUpgrade.gameObject.SetActive(false);
//			dEquip.gameObject.SetActive(false);
//			dExtend.gameObject.SetActive(false);
//			dAution.gameObject.SetActive(false);
//			dSell.gameObject.SetActive(false);
//			Button b = dBuy;
//			AddListenerBuyItem(_obj,b,id,itemClass,gun);
//		}
//		if(fill > 0){
//			dExtend.GetComponent<Button>().interactable = false;
//		}else{
//			dExtend.GetComponent<Button>().interactable = true;
//			dBuy.gameObject.SetActive(false);
//			dUpgrade.gameObject.SetActive(false);
//			dEquip.gameObject.SetActive(false);
//			dExtend.gameObject.SetActive(true);
//			dAution.gameObject.SetActive(false);
//			dSell.gameObject.SetActive(false);
//			Button ex = dExtend;
//			AddListenerExtendItem(ex,id);
//		}
//	}
//	private void AddListenerEquipItem(Button b,string id,string itemClass)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> EquipMainGunItem(id,itemClass));
//		
//	}
//	private void EquipMainGunItem(string id,string itemClass)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
////		CSVR_ApplicationManager.instance.OnEquipItem(id,itemClass);
//	}
//	
//	private void AddListenerUnEquipItem(Button b,string id)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> UnEquipMainItem(id));
//		
//	}
//	private void UnEquipMainItem(string id)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
////		CSVR_ApplicationManager.instance.OnUnEquipItem(id);
//	}
//	
//	private void AddListenerBuyItem(GameObject _obj,Button b,string id,string classs,CSVR_GunInfo gun)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> BuyMainItem(_obj,id,gun,classs));
//		
//	}
//	public void ListenerBuySuccess()
//	{
//		Destroy(destroyTHIS);
//	}
//	private void BuyMainItem(GameObject gb,string id,CSVR_GunInfo gun,string classs)
//	{
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.buyClip);
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_TOTAL;
//		CSVR_ApplicationManager.instance.OnFirstPurchaseGunItem(id,gun);
//		CSVR_ApplicationManager._itemID = id;
//		CSVR_ApplicationManager._itemCLASS = classs;
//		destroyTHIS = gb;
//	}
//	
//	private void AddListenerExtendItem(Button b,string id)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> ExtendMainItem(id));
//		
//	}
//	private void ExtendMainItem(string id)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		CSVR_ApplicationManager.instance.OnExtendGunItem(id);
//	}
//	private void AddListenerUpgradeItem(Button b,string id,int lv)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> UpgradeMainGunItem(id,lv));
//		
//	}
//	private void UpgradeMainGunItem(string id,int lv)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		CSVR_ApplicationManager.instance.OnUpgradeGunItem(id,lv);
//	}
//	
//	// Update is called once per frame
//	//	void Update () {
//	//		UI_CurrencyGCTxt.text = CSVR_GameSetting.virtualCurrency ["GC"].ToString ();
//	//		UI_CurrencyGDTxt.text = CSVR_GameSetting.virtualCurrency ["GD"].ToString ();
//	//	}
//}
