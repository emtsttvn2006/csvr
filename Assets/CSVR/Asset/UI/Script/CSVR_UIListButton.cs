﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using MarchingBytes;

public class AButton
{
    public string tittle { get; set; }
    public UnityAction acion { get; set; }

    public AButton()
    {
        this.tittle = "";
        this.acion = null;
    }

    public AButton(string tittle, UnityAction action)
    {
        this.tittle = tittle;
        this.acion = action;
    }
}

public class CSVR_UIListButton : SingletonBaseScreen<CSVR_UIListButton> {

    [HideInInspector]
    public Vector2 v2Size = Vector2.zero;

    public RectTransform RectTransform;

    public GameObject ContentHolder;

    public LoopVerticalScrollRect scrollButtons;

    [HideInInspector] public List<AButton> listButtons = new List<AButton>();

    public void Start()
    {
        if (this.scrollButtons.prefabPool == null) this.scrollButtons.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
    }

    public override void Open()
    {
        base.Open();

        this.gameObject.SetActive(true);

        this.v2Size = HomeScreen.instance.v2Size;

        GotoLastInLayer();
    }

    public override void Close()
    {
        base.Close();
        Clear();
    }

    public void SetSpawnPoint(Vector3 SpawnPoint)
    {
        this.RectTransform.localPosition = new Vector3(v2Size.x * (SpawnPoint.x - 0.5f), v2Size.y * (SpawnPoint.y - 0.5f), 0); ;
    }

    public void Clear()
    {
        this.listButtons.Clear();
        UpdateButtonsUI();
    }

    public void Show(List<AButton> list)
    {
        this.listButtons = list;
        UpdateButtonsUI();
    }

    public void UpdateButtonsUI()
    {
        this.scrollButtons.totalCount = this.listButtons.Count;
        this.scrollButtons.RefillCells();
    }
    
    public void AddAButton(AButton AButton)
    {
        this.listButtons.Add(AButton);
        UpdateButtonsUI();
    }
}
