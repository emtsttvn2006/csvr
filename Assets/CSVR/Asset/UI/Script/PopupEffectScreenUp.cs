﻿using UnityEngine;
using System.Collections;

public class PopupEffectScreenUp : SingletonBaseScreen<PopupEffectScreenUp> {

    public Animator star_animator;

    public void Play(string level)
    {
        if (HasParameter(level, star_animator))
        {
            star_animator.SetTrigger(level);
        }
    }
}
