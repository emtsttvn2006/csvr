﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UILevelUp : SingletonBaseScreen<CSVR_UILevelUp>
{
    public Text levelText;
	public Text coinRewardText;

	void Start(){
		ShowLevelUp ();
		Invoke ("DestroyGameObecjt",5f);
	}

	void ShowLevelUp(){
		levelText.text = CSVR_GameSetting.accountLevel.ToString();
		//coinRewardText.text = "+"+((CSVR_GameSetting.accountLevel- 1)* 50 + 1000);
	}

	void DestroyGameObecjt(){
		Destroy (this.gameObject);
	}
}
