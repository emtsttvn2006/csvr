using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CSVR_UIEndGame : SingletonBaseScreen<CSVR_UIEndGame>
{	
	public Text NameTeam1, NameTeam2;
	public Text policeKill, terrirosKill, teamWinText;
	public Text cointText,dollarText;
	public Image map;
	public GameObject UI_BtnRePlayGame;
	[Space(5)]
	[SerializeField]
	CSVR_UIEndGameChild[] policePlayers;
	[Space(5)]
	[SerializeField]
	CSVR_UIEndGameChild[] terroristPlayers;

	int[] teamsCount;
	
	void Start(){
		#if !VIRTUAL_ROOM
		HienThiMoi ();
		#else
		UI_OnLeavRoomClick();
		#endif
		
	}
	
	void ClearPortraits()
	{
		teamsCount = new int[] { 0, 0 };
		
		for (int i = 0; i < 5; i++)
		{
			policePlayers[i].thisGameObject.SetActive(false);
			terroristPlayers[i].thisGameObject.SetActive(false);
		}
	}
	
	void HienThiMoi()
	{
		ClearPortraits ();
		int aiStart = 1;
		PhotonPlayer[] players = PhotonNetwork.playerList;
		for (int i = 0; i < players.Length; i++)
		{
			//try
			{
				string playerNameHUD = players[i].customProperties["PlayFabName"].ToString();
				string playerLevelHUD = players[i].customProperties["Level"].ToString();
				string playerFacebookID = players[i].customProperties["FacebookID"].ToString();
				string playerKDAHUD = "...";
				string playerIDMainGun = CSVR_GameSetting.mainGun;
				try{
					playerKDAHUD = players[i].customProperties["KDA"].ToString();
				}catch{
				}
				try{
					playerIDMainGun = players[i].customProperties["MainGun"].ToString();
				}catch{
				}

				bool isMe = players[i].name == AccountManager.instance.displayName;
				if (isMe) { 
					try
					{
						cointText.text = (int.Parse (players [i].customProperties ["Coin"].ToString ()) - AccountManager.instance.gameCoinAmount).ToString ();
						if (int.Parse (players [i].customProperties ["Coin"].ToString ()) != AccountManager.instance.gameCoinAmount) {
							AccountManager.instance.gameCoinAmount = int.Parse (players [i].customProperties ["Coin"].ToString ());

						}
					}catch{
						cointText.text = "...";
					}
					try
					{
						dollarText.text = (int.Parse (players [i].customProperties ["Gold"].ToString ()) - AccountManager.instance.gameDollarAmount).ToString ();
						if (int.Parse (players [i].customProperties ["Gold"].ToString ()) != AccountManager.instance.gameDollarAmount) {
							AccountManager.instance.gameDollarAmount = int.Parse (players [i].customProperties ["Gold"].ToString ());

						}
					}catch{
						dollarText.text = "...";
					}
						teamWinText.text = players [i].customProperties ["EndGame"].ToString();

					try
					{
						if (int.Parse (players [i].customProperties ["Exp"].ToString ()) != CSVR_GameSetting.accountExp) {
							CSVR_GameSetting.accountExp = int.Parse (players [i].customProperties ["Exp"].ToString ());

						}
					}catch{

					}
					try
					{
						if (int.Parse (players [i].customProperties ["Level"].ToString ()) != CSVR_GameSetting.accountLevel) {
							CSVR_GameSetting.accountLevel = int.Parse (players [i].customProperties ["Level"].ToString ());
							CSVR_UILevelUp.instance.Open ();
						}
					}catch{

					}


					try{
						List<string> keyNames = Horus.Json.JsonWrapper.DeserializeObject<List<string> >(players [i].customProperties ["KeyList"].ToString());
						for(int lala = 0; lala < keyNames.Count;lala++){
							Debug.Log("1. AccountManager.instance.keyCards.Count: "+keyNames [lala]);
							AccountManager.instance.keyCards.Add (keyNames [lala]);
						}
						if (AccountManager.instance.keyCards.Count != 0) {
							CSVR_UISpinCard.instance.Open ();
						}
						Debug.Log("2. AccountManager.instance.keyCards.Count: "+AccountManager.instance.keyCards.Count);
					}catch{
					}
				}
			TeamTypes playerTeam = (TeamTypes)int.Parse(players[i].customProperties["Team"].ToString());
			if (playerTeam == TeamTypes.RoomHostPlayer) continue;// Vì đây là player chỉ để host cái room 
			int myTeamCount = teamsCount[playerTeam.GetHashCode()];
			
			byte[] pngBytes = players[i].customProperties["Avatar"] as byte[];
			Texture2D tex = new Texture2D(128, 128);
			tex.LoadImage(pngBytes);
			Sprite playerAvatarHUD = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2());
			
			if (playerTeam == TeamTypes.Police)
			{
				policePlayers[myTeamCount].thisGameObject.SetActive(true);
				policePlayers[myTeamCount].playerAvatar.sprite = playerAvatarHUD;
				policePlayers[myTeamCount].playerName.text = playerNameHUD;
				policePlayers[myTeamCount].playerLevel.text = "Cấp độ: "+playerLevelHUD;
				policePlayers[myTeamCount].playerKDA.text = playerKDAHUD;
				policePlayers[myTeamCount].playerGun.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+playerIDMainGun);
				try{
					List<string> keyNames = Horus.Json.JsonWrapper.DeserializeObject<List<string> >(players [i].customProperties ["KeyList"].ToString());
						//Debug.Log("3. AccountManager.instance.keyCards.Count: "+keyNames.Count);
						if (keyNames.Count != 0) {
							policePlayers[myTeamCount].playerGun.transform.GetChild(0).gameObject.SetActive(true);
					}
				}catch{
				}
				policePlayers[myTeamCount].idFacebook = playerFacebookID;

				if (isMe){ 
					policePlayers[myTeamCount].isMine.SetActive(true);
					policePlayers[myTeamCount].addFriend.interactable = false;
				}
				else{ 
					policePlayers[myTeamCount].isMine.SetActive(false);
					policePlayers[myTeamCount].addFriend.interactable = true;
				}
			}
			else
			{
				terroristPlayers[myTeamCount].thisGameObject.SetActive(true);
				terroristPlayers[myTeamCount].playerAvatar.sprite = playerAvatarHUD;
				terroristPlayers[myTeamCount].playerName.text = playerNameHUD;
				terroristPlayers[myTeamCount].playerLevel.text = "Cấp độ: "+playerLevelHUD;
				terroristPlayers[myTeamCount].playerKDA.text = playerKDAHUD;
				terroristPlayers[myTeamCount].playerGun.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+playerIDMainGun);
				try{
					List<string> keyNames = Horus.Json.JsonWrapper.DeserializeObject<List<string> >(players [i].customProperties ["KeyList"].ToString());
						//Debug.Log("3. AccountManager.instance.keyCards.Count: "+keyNames.Count);
						if (keyNames.Count != 0) {
							terroristPlayers[myTeamCount].playerGun.transform.GetChild(0).gameObject.SetActive(true);
					}
				}catch{
				}
				terroristPlayers[myTeamCount].idFacebook = playerFacebookID;

				if (isMe){ 
					terroristPlayers[myTeamCount].isMine.SetActive(true);
					terroristPlayers[myTeamCount].addFriend.interactable = false;
				}else {
					terroristPlayers[myTeamCount].isMine.SetActive(false);
					terroristPlayers[myTeamCount].addFriend.interactable = true;
				}
			}
			
			teamsCount[playerTeam.GetHashCode()]++;
			}
//			catch{
//			}

		}

		int k = 0;
		for (int i = aiStart; i < CSVR_GameSetting.AI_InGame.Count+aiStart; i++) {

			CSVR_UIAIInfo aiInfo = CSVR_GameSetting.AI_InGame[k];
			TeamTypes aiTeam = (TeamTypes)(aiInfo.AI_TeamNumber-1);

			byte[] pngBytes = AccountManager.instance.avatarPlayerTexture.EncodeToPNG();;
			Texture2D tex = new Texture2D(128, 128);
			tex.LoadImage(pngBytes);

			string aiNameHUD = aiInfo.AI_Name;
			int aiLevelHUD = aiInfo.AI_Level;
			string aiKDAHUD = aiInfo.AI_Kill+"/"+ aiInfo.AI_Death;
			Sprite aiAvatarHUD = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2());

			int myTeamCount = teamsCount[aiTeam.GetHashCode()];
			if (aiTeam == TeamTypes.Police)
			{
				policePlayers[myTeamCount].thisGameObject.SetActive(true);
				policePlayers[myTeamCount].playerAvatar.sprite = aiAvatarHUD;
				policePlayers[myTeamCount].playerName.text = aiNameHUD;
				policePlayers[myTeamCount].playerLevel.text = "Cấp độ: "+aiLevelHUD;
				policePlayers[myTeamCount].playerKDA.text = aiKDAHUD;
				policePlayers[myTeamCount].playerGun.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+aiInfo.AI_Weapon);
				policePlayers[myTeamCount].idFacebook = "groups/csmvietnam/";
			}
			else
			{
				terroristPlayers[myTeamCount].thisGameObject.SetActive(true);
				terroristPlayers[myTeamCount].playerAvatar.sprite = aiAvatarHUD;
				terroristPlayers[myTeamCount].playerName.text = aiNameHUD;
				terroristPlayers[myTeamCount].playerLevel.text = "Cấp độ: "+aiLevelHUD;
				terroristPlayers[myTeamCount].playerKDA.text = aiKDAHUD;
				terroristPlayers[myTeamCount].playerGun.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+aiInfo.AI_Weapon);
				terroristPlayers[myTeamCount].idFacebook = "groups/csmvietnam/";
			}
			teamsCount[aiTeam.GetHashCode()]++;
			k ++;
		}
		map.sprite = HomeScreen.instance.modeMapSprites[HomeScreen.instance.modeMapSpritesIndex[CSVR_GameSetting.mapName]];
	}

	public void UI_OnLeavRoomClick(){
		Destroy (this.gameObject);
		//Destroy (HorusManager.instance.gameModeInstance.transform.GetChild(1).gameObject);
		if(PhotonNetwork.isMasterClient){
			PhotonNetwork.room.open = false;
			PhotonNetwork.DestroyAll();
		}
		
		PhotonNetwork.LeaveRoom ();
		HorusManager.instance.gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(false);
		//HomeScreen.istance.UpdateScreenState (4);
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		//PlayFabManager.instance.LoadTitleDataEndGame ();
		//Resources.UnloadUnusedAssets ();
		//System.GC.Collect();
	}
	
}
