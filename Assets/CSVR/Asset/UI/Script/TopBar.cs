﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using System.Collections;
using DG.Tweening;

public class TopBar : SingletonBaseScreen<TopBar>
{
    public Button Back;
	public Button Setting;
	public Button ShopOrInV;
	public Text ShopOrInVText;

    public Text gameCoin;
    public Text gameDollar;
	public Text gameNewFeed;

	void Start(){
		UpdateVirtualCurrency ();
		DOTween.To(()=> gameNewFeed.transform.localPosition, x=> gameNewFeed.transform.localPosition = x, new Vector3(-900, 0, 0), 25).SetRelative().SetLoops(-1, LoopType.Restart);
	}

    public void UpdateVirtualCurrency()
    {
        gameCoin.text = AccountManager.instance.gameCoinAmount.ToString();
        gameDollar.text = AccountManager.instance.gameDollarAmount.ToString();     
    }

    public override void SetBackButton(UnityEngine.Events.UnityAction action)
    {
        Back.onClick.RemoveAllListeners();
        Back.onClick.AddListener(action);
    }

	public void SetShopOrInVButton(UnityAction action)
	{
		ShopOrInV.onClick.RemoveAllListeners();
		ShopOrInV.onClick.AddListener(action);
	}

    public void SetShop()
    {
        SetShopOrInVButton(new UnityAction(() => {
            MainView.instance.Close();
            CharacterView.instance.Open();
            CSVR_UIShop.instance.Open();
            AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
        }));

        Setting.interactable = true;
        ShopOrInVText.text = "Cửa hàng";
    }

    public void SetInventory()
    {
        SetShopOrInVButton(new UnityAction(() => {
            MainView.instance.Close();
            CSVR_UIShop.instance.Close();
            CSVR_UIInventory.instance.Open();
            CharacterView.instance.Open();
            AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
        }));

        Setting.interactable = true;
        ShopOrInVText.text = "Thùng đồ";
    }

    public void UI_OnSettingClick()
    {
        CSVR_UISettingView.instance.Open();
    }
	public void UI_OnVip_Click()
	{
		CSVR_UIVip.instance.Open();
	}    
}
