﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;

public class CSVR_GBAPI : MonoBehaviour {
	public Dropdown UI_CardType;
	public InputField UI_PinNumber;
	public InputField UI_SeriNumber;
	public Button UI_ButtonSubmit;


	void Start(){

	//	UI_ChangeRate.text = "1 : " + HorusManager.instance.GetTitleValue ("Key_ChangeCoin");
	//	UI_ButtonSubmit.onClick.AddListener(()=> OnGameBankCard(UI_PinNumber.text,UI_SeriNumber.text,(UI_CardType.value+1).ToString(),HorusManager.instance.playerDisplayName));
	}

	void OnGameBankCard(string pin, string seri,string card_type,string note) {
//		RunCloudScriptRequest request = new RunCloudScriptRequest();
//		request.ActionId = "GameBankCardRequest";
//		request.ParamsEncoded = "{\"pin\":\""+pin+"\",\"seri\":\""+seri+"\",\"card_type\":\""+card_type+"\",\"note\":\""+note+"\"}";
//		PlayFabClientAPI.RunCloudScript(request,OnGameBankCardSuccess, OnGameBankCardFaile);

		var parameter = new {pin = pin,seri = seri,card_type = card_type,note = note};
		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
		request.FunctionName = "GameBankCardRequest";
		request.FunctionParameter = parameter;
		request.RevisionSelection = CloudScriptRevisionOption.Live;
		request.GeneratePlayStreamEvent = true;
		PlayFabClientAPI.ExecuteCloudScript(request,OnGameBankCardSuccess, OnGameBankCardFaile);
	}
	void OnGameBankCardSuccess(ExecuteCloudScriptResult result){
		if (result.Logs[0].Message.Contains ("false")) {
			GameBankResult sellResul = PlayFab.Json.JsonConvert.DeserializeObject<GameBankResult>(result.FunctionResult.ToString());
			ErrorView.instance.ShowPopupError ("Error: "+sellResul.msg);
//			CSVR_UIMessageText.instance.SetAlphaText(sellResul.msg);
		} else {
			AddUserVirtualCurrencyResult sellResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
			CSVR_GameSetting.virtualCurrency ["GD"] = sellResul.Balance;
//			HomeScreen.instance.UpdateVirtualCurrency();
//			CSVR_UIMessageText.instance.SetAlphaText("+"+sellResul.Balance);
		}
	}
	void OnGameBankCardFaile(PlayFabError error){
		ErrorView.instance.ShowPopupError ("Error: "+error.ErrorMessage);
	}

	private class GameBankResult{
		//public int code;
		public string msg="";
		public string info_card="";
		public string transaction_id="";
	}
	public InputField UI_Gold;
	public Text UI_ChangeRate;
	public InputField UI_Coin;
	public void UI_ChangeCoinClick(){
		//if (AccountManager.instance.gameDollarAmount - int.Parse (UI_Gold.text) >= 0)
//			HorusManager.instance.OnChangeCoin ((UI_Gold.text));
		//else
			//ErrorView.instance.ShowPopupError ("Không đủ vàng để đổi!..");
	}
	public void UI_ChangeCoinEndEdit(){
		UI_Coin.text = (int.Parse(UI_Gold.text)*int.Parse(HorusManager.instance.GetTitleValue("Key_ChangeCoin"))).ToString(); 
	}
}

