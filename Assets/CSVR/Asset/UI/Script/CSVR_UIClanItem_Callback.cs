﻿using UnityEngine;
using System.Collections;
using Horus.ClientModels;
using UnityEngine.UI;
using Horus;
using UnityEngine.Events;

public class CSVR_UIClanItem_Callback : MonoBehaviour
{
    public Image ImgBackGround;
    public Text TextOrder;
    public Image ImgAvatar;
    public Text TextClanName;
    public Text TextChuClan;
    public Text TextClanLV;
    public Text TextClanDiemHoatDong;
    public Text TextClanMember;
    public Button ButtonXinVao;
        
    [HideInInspector] public float distance;

    [HideInInspector]
    public ClanResult ClanResult;

    void ScrollCellIndex(int idx)
    {
        if (CSVR_UICLanList.instance.INPTimkiem.text == "")
        {
            this.ClanResult = CSVR_UICLanList.instance.ClanListResult.Clan[idx];
        } else
        {
            this.ClanResult = CSVR_UICLanList.instance.ListClanSearch.Clan[idx];
        }
        
        this.TextOrder.text = idx.ToString();
        this.TextClanName.text = this.ClanResult.roleId;
        this.TextChuClan.text = "CHỦ CLAN: " + this.ClanResult.chief;
        this.TextClanLV.text = this.ClanResult.level.ToString();
        this.TextClanDiemHoatDong.text = "chưa có!!";
        this.TextClanMember.text = this.ClanResult.memberCount.ToString();

        if (CSVR_UICLanList.instance.MyClan != null && CSVR_UICLanList.instance.MyClan.roleId != "")
        {
            ButtonXinVao.gameObject.SetActive(false);
        } else
        {
            ButtonXinVao.gameObject.SetActive(true);
        }

        Reset_InvokeActivateClanListItem();
    }

    public void Reset_InvokeActivateClanListItem()
    {
        CancelInvoke("InvokeActivateClanListItem");
        InvokeRepeating("InvokeActivateClanListItem", 0.0f, 0.8f);
    }

    public void Clear_InvokeActivateClanListItem()
    {
        CancelInvoke("InvokeActivateClanListItem");        
    }

    public void InvokeActivateClanListItem()
    {
        if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName == this.ClanResult.roleId)
        {
            distance = -(CSVR_UICLanList.instance.scrollClanList.content.GetComponent<RectTransform>().localPosition.y + this.GetComponent<RectTransform>().localPosition.y);
            if (!(distance / 61 > 0f && distance / 61 < 3f))
            {
                if (!CSVR_UIClanItem_MyClanItem.instance.IsOn)
                {
                    CSVR_UIClanItem_MyClanItem.instance.IsOn = true;
                    CSVR_UIClanItem_MyClanItem.instance.OpenChildrenToo();
                }
            }
            else
            {
                if (CSVR_UIClanItem_MyClanItem.instance.IsOn)
                {
                    CSVR_UIClanItem_MyClanItem.instance.IsOn = false;
                    CSVR_UIClanItem_MyClanItem.instance.Hide();
                }
            }
        }
    }

    public void CSVR_UIClanItem_Callback_Click()
    {        
        CSVR_UIClanList_XemClan.instance.Open();
        CSVR_UIClanList_XemClan.instance.Show(this.ClanResult);

        TopBar.instance.SetBackButton(new UnityAction(() => {
            CSVR_UIClanList_XemClan.instance.Close();
            CSVR_UICLanList.instance.Open();
        }));
    }

    public void Button_XinVao_Click()
    {
        if (CSVR_UICLanList.instance.MyClan != null && CSVR_UICLanList.instance.MyClan.roleId != "")
        {
            ErrorView.instance.ShowPopupError("BẠN ĐANG THUỘC MỘT CLAN. HÃY THOÁT CLAN HIỆN TẠI VÀ THỰC HIỆN LẠI THAO TÁC GIA NHẬP");
        }
        else
        {
            if (this.ClanResult != null && this.ClanResult.roleId != "")
            {
                CSVR_UICLanList.instance.JoiningAClan(this.ClanResult.roleId);
            }
        }
    }
}

