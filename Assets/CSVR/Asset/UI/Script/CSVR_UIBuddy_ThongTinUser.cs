﻿using UnityEngine;
using System.Collections;
using Horus.ClientModels;
using UnityEngine.UI;
using Horus;

public class CSVR_UIBuddy_ThongTinUser : SingletonBaseScreen<CSVR_UIBuddy_ThongTinUser>
{
    public Image ImgAvata;
    public Text TextName;
    public Image ImgLevel;
    public Text TextLV;

    public Text TextPVPHaSat;
    public Text TextPVPHaSatHoiSinh;
    public Text TextPVPTranDau;
    public Text TextPVPThang;
    public Text TextPVPNgaySo;
    public Text TextPVPThoatGame;

    public Text TextPVEHaSat;
    public Text TextPVEHaSatHoiSinh;
    public Text TextPVEThang;

    [HideInInspector]
    UserInfo UserInfo;

    public void Show(string Name)
    {
        GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();
        GetUserInfoRequest.Add(Name);

        HorusClientAPI.GetUserInfo(GetUserInfoRequest,
            (result) =>
            {
                this.UserInfo = result.users[0];
                this.TextPVPHaSat.text = "--";
                this.TextPVPHaSatHoiSinh.text = "--";
                this.TextPVPTranDau.text = "--";
                this.TextPVPThang.text = "--";
                this.TextPVPNgaySo.text = "--";
                this.TextPVPThoatGame.text = "--";
                this.TextPVEHaSat.text = "--";
                this.TextPVEHaSatHoiSinh.text = "--";
                this.TextPVEThang.text = "--";
            }, (error) =>
            {
                if (DebugError) Debug.Log("Cập nhật thông tin User Lỗi");
                ErrorView.instance.ShowPopupError("Cập nhật thông tin User Lỗi");
            }, null);
    }
}
