﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChatView_BanbeChatContentCallBack : MonoBehaviour
{
    public string Name;
    public Text Message;
    public LayoutElement LayoutElement;

    void ScrollCellIndex(int idx)
    {
        int cacheIndex = ChatCache.instance.BanbeCaches.GetUserIndex(CSVR_UIChat.instance.NameChosenBanbe);
        if (cacheIndex != -1) {
            Message.text = ChatCache.instance.BanbeCaches.BanbeUsers[cacheIndex].contents[idx];
        }
        //SetPreferHeight(Message.text.Length / CSVR_UIChat.instance.Max_Letters_In_String + 1);
    }

    /*
    void SetPreferHeight(int line)
    {
        LayoutElement.preferredHeight = CSVR_UIChat.instance.LineText_In_UI * line;        
    }*/

    public void ChatView_GlobalChatContent_Click()
    {
        
    }
}