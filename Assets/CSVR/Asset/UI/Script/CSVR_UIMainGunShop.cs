//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using PlayFab;
//using PlayFab.ClientModels;
//using PlayFab.Internal;
//
//
//public class CSVR_UIMainGunShop : MonoBehaviour {
//	public static CSVR_UIMainGunShop instance;
//	public Text UI_CurrencyGCTxt;
//	public Text UI_CurrencyGDTxt;
//
//	public GameObject mainGunContainer;
//	public GameObject mainGunPanel;
//
//	public GameObject dPanel;
//	public Text dName;
//	public Image dIcon;
//	public GameObject dInfo;
//	public GameObject dDay;
////	public GameObject dFix;
//	public Button dBuy;
//	public Button dUpgrade;
//	public Button dEquip;
//	public Button dExtend;
//	public Button dAution;
//	public Button dSell;
//	private string itemClass,dayExpired,sellPrices;
//	private bool group1,group2,group3;
//	public Sprite[] iconType;
//	CSVR_GunInfo infoGunTemp;
//	CSVR_DayInfo infoDayTemp;
//	float fillAmount;
//	int lvGun = 0; 
//	void Awake()
//	{
//		instance = this;
//	}
//
//	public void HienThi()
//	{
//		mainGunContainer.transform.Clear();
//		ToggleGroup group = mainGunContainer.transform.GetComponent<ToggleGroup>();
//		for(int i = 0; i < CSVR_GameSetting.shopMainGun.Count; i++)
//		{
//			string idTemp,nameTemp;
//			bool damua;
//			//tao main gun
//			GameObject mainGunClone = Instantiate(mainGunPanel);
//			itemClass = CSVR_GameSetting.shopMainGun[0].ItemClass;
//			//kiem tra xem co ton tai trong thung do khong
////			if(CSVR_GameSetting.FindItemInListInventoryItems(CSVR_GameSetting.shopMainGun[i].ItemId) != null)
////			{
////				//da mua
////				ItemInstance mainGunInv = CSVR_GameSetting.FindItemInListInventoryItems(CSVR_GameSetting.shopMainGun[i].ItemId);
//////				CSVR_UIHome_v2.instance.ImportItemToMainGunInv(mainGunInv,CSVR_UIHome_v2.instance.mainGunInvContent,CSVR_UIHome_v2.instance.moreMainPanel);
////				itemClass = mainGunInv.ItemClass;
////				//kiem tra truong custom data ton tai khong
////				if(mainGunInv.CustomData != null){
////					if(mainGunInv.CustomData.ContainsKey("DayManager"))
////					{
////						infoDayTemp = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_DayInfo>(mainGunInv.CustomData["DayManager"].ToString());
////						if(!CSVR_GameSetting.IsExpired(infoDayTemp.D2))
////						{
////							System.TimeSpan maxDay  =  infoDayTemp.D2 - infoDayTemp.D1;
////							System.TimeSpan interval  =  infoDayTemp.D2 - CSVR_GameSetting.LastLogin;
////							
////							fillAmount = float.Parse(interval.TotalHours.ToString()) / float.Parse(maxDay.TotalHours.ToString());
////
////							dayExpired = interval.Days +"Day "+interval.Hours+"Hours "+interval.Minutes+"Minute";
////							mainGunClone.transform.GetChild(4).GetComponent<Text>().fontSize = 25;
////							mainGunClone.transform.GetChild(4).GetComponent<Text>().text = dayExpired;
////						}
////						else
////						{
////							fillAmount = 0;
////							dayExpired = "Hết hạn";
////						}
////					}
////					if(mainGunInv.CustomData.ContainsKey("GunManager"))
////					{
////						infoGunTemp = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GunInfo>(mainGunInv.CustomData["GunManager"].ToString());
////						Debug.Log("infoGunTemp "+ infoGunTemp.D0 +" G1 "+ infoGunTemp.G1 +" "+infoGunTemp.G9);
////						//dame	
////					}
////					if(mainGunInv.CustomData.ContainsKey("Level"))
////					{
//////						int startLenght = int.Parse(mainGunInv.CustomData["Level"].ToString());
//////						lvGun = startLenght;
//////						for(int s = 0; s < 9; s++){
//////							if(s < startLenght)
//////								mainGunClone.transform.GetChild(5).GetChild(s).gameObject.SetActive(true);
//////							else
//////								mainGunClone.transform.GetChild(5).GetChild(s).gameObject.SetActive(false);
//////						}
////					}
////					if(mainGunInv.CustomData.ContainsKey("SellPrices"))
////					{
////						sellPrices = mainGunInv.CustomData["SellPrices"];
////					}
////					if(!mainGunInv.CustomData.ContainsKey("EquippedGroup1"))
////					{
////						group1 = false;
////					}else{
////						group1 = bool.Parse(mainGunInv.CustomData["EquippedGroup1"]);
////					}
////					
////					if(!mainGunInv.CustomData.ContainsKey("EquippedGroup2"))
////					{
////						group2 = false;
////					}else{
////						group2 = bool.Parse(mainGunInv.CustomData["EquippedGroup2"]);
////					}
////					
////					if(!mainGunInv.CustomData.ContainsKey("EquippedGroup3"))
////					{
////						group3 = false;
////					}else{
////						group3 = bool.Parse(mainGunInv.CustomData["EquippedGroup3"]);
////					}
////				}
////
////				idTemp = mainGunInv.ItemId;
////				nameTemp = mainGunInv.DisplayName;
////				damua = true;
//////				mainGunClone.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Image>().fillAmount = fillAmount;
////				mainGunClone.transform.GetChild(3).GetComponent<Image>().sprite = iconType[2];
////				mainGunClone.transform.GetChild(4).GetComponent<Text>().text = dayExpired;
////			}else
//			{
//				//chua mua
//				sellPrices = "0";
//				lvGun = 0;
//				if(CSVR_GameSetting.shopMainGun[i].CustomData != null){
//					infoGunTemp = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GunInfo>(CSVR_GameSetting.shopMainGun[i].CustomData);
////					Debug.Log("CSVR_GameSetting.shopMainGun[i].CustomData"+CSVR_GameSetting.shopMainGun[i].CustomData);
//					dayExpired = infoGunTemp.D0.ToString()+"Day 0Hours 0Miutes";
//				}
//				idTemp = CSVR_GameSetting.shopMainGun[i].ItemId;
//				nameTemp = CSVR_GameSetting.shopMainGun[i].DisplayName;
//				damua = false;
//
//				fillAmount = 1;
//
//				mainGunClone.transform.GetChild(3).GetComponent<Image>().sprite = iconType[1];
//				mainGunClone.transform.GetChild(4).GetComponent<Text>().text = CSVR_GameSetting.itemPrices[idTemp].ToString();
//
//			}
//			mainGunClone.name = mainGunClone.transform.GetChild(1).GetComponent<Text>().text = CSVR_GameSetting.shopMainGun[i].DisplayName;
//			mainGunClone.transform.GetChild(2).GetComponent<Image>().sprite = CSVR_AssetManager.itemIconDic[idTemp];
//			mainGunClone.transform.SetParent(mainGunContainer.transform,false);
//			mainGunClone.transform.localScale = Vector3.one;
//			
//			Toggle b = mainGunClone.GetComponent<Toggle>();
//			b.group = group;
//			AddListenerItems(b,idTemp,nameTemp,itemClass,dayExpired,fillAmount,damua,group1,group2,group3,infoGunTemp,lvGun,sellPrices); // Usi
//			if(i==0){
//				b.isOn = true;
//				DetailPanel(idTemp,nameTemp,itemClass,dayExpired,fillAmount,damua,group1,group2,group3,infoGunTemp,lvGun,sellPrices);
//			}
//		}
//	}
//	private void AddListenerItems(Toggle b,string id,string name,string iClass,string day,float fillA,bool damua,bool gr1,bool gr2,bool gr3,CSVR_GunInfo gun,int lv,string pri)
//	{
//		b.onValueChanged.RemoveAllListeners();
//		b.onValueChanged.AddListener((value)=> DetailPanel(id,name,iClass,day,fillA,damua,gr1,gr2,gr3,gun,lv,pri));
//	}
//
//	private void DetailPanel(string id,string n,string itemClass,string day,float fill,bool damua,bool group1,bool group2,bool group3,CSVR_GunInfo gun,int lv,string pri)
//	{
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.changeGroupClip);
//		if(dPanel.activeSelf == false)
//			dPanel.SetActive(true);
//		
////		dIcon.sprite = Resources.Load<Sprite>("Sprites/"+code);
//		dIcon.sprite = CSVR_AssetManager.itemIconDic[id];
//		dAution.interactable = false;
////		dSell.interactable = false;
////		dDay.transform.GetChild(0).GetComponent<Image>().fillAmount = fill;
//		dDay.transform.GetChild(0).GetComponent<Text>().text = day;
//		dName.text = n;
//
////		dInfo.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G8.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolDameMax"));
////		dInfo.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G9.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolAPMax"));
////		dInfo.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G2.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolROFMax"));
//		dInfo.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = gun.G1.ToString();
//		dInfo.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = gun.G2.ToString()+" RPM";
//		dInfo.transform.GetChild(2).GetChild(1).GetComponent<Text>().text = gun.G3.ToString()+" s";
//		dInfo.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = gun.G4.ToString();
//		dInfo.transform.GetChild(4).GetChild(1).GetComponent<Text>().text = gun.G5.ToString()+" kg";
////		for(int s = 0; s < 9; s++){
////			if(s < lv)
////				dPanel.transform.GetChild(0).GetChild(4).GetChild(s).gameObject.SetActive(true);
////			else
////				dPanel.transform.GetChild(0).GetChild(4).GetChild(s).gameObject.SetActive(false);
////		}
//		if(damua){
////			dBuy.gameObject.SetActive(false);
////			dExtend.gameObject.SetActive(false);
////			dUpgrade.gameObject.SetActive(true);
////			dEquip.gameObject.SetActive(true);
////			dAution.gameObject.SetActive(true);
////			dSell.gameObject.SetActive(true);
////			Button s = dSell;
////			AddListenerSellItems(s,id,pri,n,dIcon.sprite);
////			AddListenerSellItem(s,id,gun,itemClass,n,day,dIcon.sprite);
////			Button u = dUpgrade;
////			if((lv+1) == 9)
////				u.interactable = false;
////			else
////				AddListenerUpgradeItem(u,id,lv);
////
////			Button e = dEquip;
//			
////			if((CSVR_UIHome.instance.currentGroup == 1 && !group1) || (CSVR_UIHome.instance.currentGroup == 2 && !group2) || (CSVR_UIHome.instance.currentGroup == 3 && !group3)){
////				dEquip.transform.GetChild(0).GetComponent<Text>().text = "Đeo lên";
////				AddListenerEquipItem(e,id,itemClass);
////			}else{
////				dEquip.transform.GetChild(0).GetComponent<Text>().text = "Gỡ xuống";
////				AddListenerUnEquipItem(e,id);
////			}
//		}else{
//			dBuy.gameObject.SetActive(true);
////			dUpgrade.gameObject.SetActive(false);
////			dEquip.gameObject.SetActive(false);
////			dExtend.gameObject.SetActive(false);
////			dAution.gameObject.SetActive(false);
//			dSell.gameObject.SetActive(false);
//			Button b = dBuy;
//			AddListenerBuyItem(b,id,gun,itemClass,n,day,dIcon.sprite);
//		}
////		if(fill > 0){
////			dExtend.GetComponent<Button>().interactable = false;
////		}else{
////			dExtend.GetComponent<Button>().interactable = true;
////			dBuy.gameObject.SetActive(false);
////			dUpgrade.gameObject.SetActive(false);
////			dEquip.gameObject.SetActive(false);
////			dExtend.gameObject.SetActive(true);
////			dAution.gameObject.SetActive(false);
////			dSell.gameObject.SetActive(false);
////			Button ex = dExtend;
////			AddListenerExtendItem(ex,id);
////		}
//	}
////	private void AddListenerEquipItem(Button b,string id,string itemClass)
////	{
////		b.onClick.RemoveAllListeners();
////		b.onClick.AddListener(()=> EquipMainGunItem(id,itemClass));
////		
////	}
////	private void EquipMainGunItem(string id,string itemClass)
////	{
////		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_MAINGUN;
////		CSVR_ApplicationManager.instance.OnEquipItem(id,itemClass);
////	}
////	
////	private void AddListenerUnEquipItem(Button b,string id)
////	{
////		b.onClick.RemoveAllListeners();
////		b.onClick.AddListener(()=> UnEquipMainItem(id));
////		
////	}
////	private void UnEquipMainItem(string id)
////	{
////		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_MAINGUN;
////		CSVR_ApplicationManager.instance.OnUnEquipItem(id);
////	}
//	
//	private void AddListenerBuyItem(Button b,string id,CSVR_GunInfo gun,string classs,string name,string day,Sprite icon)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> BuyMainItem(id,gun,classs,name,day,icon));
//		
//	}
//	private void BuyMainItem(string id,CSVR_GunInfo gun,string classs,string name,string day,Sprite icon)
//	{
//		//CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.buyClip);
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_MAINGUN;
////		CSVR_UIPopupConfirmShop.instance.ShowPopUpPurchase(id,gun,classs,name,day,icon);
////		CSVR_ApplicationManager.instance.OnFirstPurchaseGunItem(id,gun);
////		CSVR_ApplicationManager._itemID = id;
////		CSVR_ApplicationManager._itemCLASS = classs;
//	}
////	private void AddListenerSellItems(Button s,string id,string prices,string name,Sprite icon)
////	{
////		s.onClick.RemoveAllListeners();
////		s.onClick.AddListener(()=> SellWeapon(id,prices,name,icon));
////	}
////	private void SellWeapon(string id,string prices,string name,Sprite icon)
////	{
////		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
////
////		CSVR_UIPopupConfirmShop.instance.ShowPopUpSell(id,prices,name,icon);
////	}
////	private void AddListenerExtendItem(Button b,string id)
////	{
////		b.onClick.RemoveAllListeners();
////		b.onClick.AddListener(()=> ExtendMainItem(id));
////		
////	}
////	private void ExtendMainItem(string id)
////	{
////		Debug.Log("ID "+ id);
////		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_MAINGUN;
////		CSVR_ApplicationManager.instance.OnExtendGunItem(id);
////	}
////	private void AddListenerUpgradeItem(Button b,string id,int lv)
////	{
////		b.onClick.RemoveAllListeners();
////		b.onClick.AddListener(()=> UpgradeMainGunItem(id,lv));
////		
////	}
////	private void UpgradeMainGunItem(string id,int lv)
////	{
////		Debug.Log("ID "+ id);
////		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_MAINGUN;
////		CSVR_ApplicationManager.instance.OnUpgradeGunItem(id,lv);
////	}
//
//	void Update () {
//		if(CSVR_GameSetting.virtualCurrency != null)
//		if(CSVR_GameSetting.virtualCurrency.ContainsKey("GC") && CSVR_GameSetting.virtualCurrency.ContainsKey("GD")){
//			UI_CurrencyGCTxt.text = CSVR_GameSetting.virtualCurrency ["GC"].ToString ();
//			UI_CurrencyGDTxt.text = CSVR_GameSetting.virtualCurrency ["GD"].ToString ();
//		}
//	}
//}
