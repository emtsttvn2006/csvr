﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIMessageText : MonoBehaviour {
	public static CSVR_UIMessageText instance;
	public Image displayBackgroundImg;
	public Text displayText;
	public float displayTime ;
	public float fadeTime;
	public float moveSpeed;
	private void Awake(){
		instance = this;

	}

	private IEnumerator fadeAlphaImg;

	public void SetAlphaText (string message) {
		displayText.text = message;
		if (fadeAlphaImg != null) {
			StopCoroutine (fadeAlphaImg);
		}
		fadeAlphaImg = FadeAlphaMessage ();
//		StartCoroutine (fadeAlphaImg);
	}
	
	IEnumerator FadeAlphaMessage () {
		Vector3 resetPositon = Vector3.zero;
		Color resetColor = displayBackgroundImg.color;
		resetColor.a = 1;
		displayBackgroundImg.color = resetColor;
		displayText.color = resetColor;
		displayBackgroundImg.transform.localPosition = resetPositon;

		yield return new WaitForSeconds (displayTime);
		
		while (displayBackgroundImg.color.a > 0) {
			Color displayColor = displayBackgroundImg.color;
			displayColor.a -= Time.deltaTime / fadeTime;
			displayBackgroundImg.color = displayColor;
			displayText.color = displayColor;
			displayBackgroundImg.transform.localPosition +=Vector3.up *moveSpeed ;
			yield return null;
		}
		yield return null;
	}
}
