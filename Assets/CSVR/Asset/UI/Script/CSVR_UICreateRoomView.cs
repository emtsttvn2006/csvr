﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class CSVR_UICreateRoomView : SingletonBaseScreen<CSVR_UICreateRoomView> {

	[SerializeField] private Dropdown SelectPlayMode, SelectModeGame, SelectMapGame, SelectPlayer, SelectmaxSkill, SelectTimeMatch;
	private LobbyResultV2 lobbyResult;
	private List<ItemModeV2> ListModeAvailable = null;

	private void Start(){
		lobbyResult = HomeScreen.instance.lobbyResult;
		ListModeAvailable = new List<ItemModeV2> ();
		SelectModeGame.options.Clear ();
		for (int i = 0, lenght = lobbyResult.Lobby.Count; i < lenght; i++) {
			if (lobbyResult.Lobby [i].Interactable) {
				ListModeAvailable.Add (lobbyResult.Lobby [i]);
				SelectModeGame.options.Add (new Dropdown.OptionData() {text = lobbyResult.ModeAvaiable[lobbyResult.Lobby [i].ModeName]});
			}
		}
		//chon
		SelectPlayMode.onValueChanged.AddListener(delegate { PlayMode(); });
		//chọn mode khi tạo phòng
		SelectModeGame.onValueChanged.AddListener(delegate { SetModeInCreate(); });
		//chọn map khi tạo phòng
		SelectMapGame.onValueChanged.AddListener(delegate { SetMapInCreate(); });

		SelectPlayer.onValueChanged.AddListener(delegate { SetPlayer(); });
		SelectmaxSkill.onValueChanged.AddListener(delegate { SetMaxSkill(); });
		SelectTimeMatch.onValueChanged.AddListener(delegate { SetTimeMatch(); });

	}

	public override void OnEnable()
	{
		PlayMode ();
		SetPlayer ();
		SetMaxSkill ();
		SetTimeMatch ();
	}

	void PlayMode()
	{
		CSVR_GameSetting.selectPlayModeValue = SelectPlayMode.value;
		if (SelectPlayMode.value == 0) {
			SelectmaxSkill.value = 0;
			SelectTimeMatch.value = 0;
			SelectPlayer.value = 0;
			SelectmaxSkill.interactable = false;
			SelectTimeMatch.interactable = false;
			SelectPlayer.interactable = false;
		} else {
			SelectmaxSkill.interactable = true;
			SelectTimeMatch.interactable = true;
			SelectPlayer.interactable = true;
		}

	}
	void SetPlayer()
	{
		CSVR_GameSetting.selectPlayerValue = SelectPlayer.value;
		CSVR_GameSetting.maxPlayer = int.Parse(SelectPlayer.options[SelectPlayer.value].text)+1;
	}

	void SetMaxSkill()
	{
		CSVR_GameSetting.selectMaxSkillValue = SelectmaxSkill.value;
		CSVR_GameSetting.maxSkill = int.Parse(SelectmaxSkill.options[SelectmaxSkill.value].text);
	}

	void SetTimeMatch()
	{
		CSVR_GameSetting.selectTimeMatchValue = SelectTimeMatch.value;
		CSVR_GameSetting.timeMatch = int.Parse(SelectTimeMatch.options[SelectTimeMatch.value].text) * 60;
	}

	private void SetModeInCreate(){
		SelectMapGame.options.Clear ();
		foreach (KeyValuePair<string, string> pair in lobbyResult.ModeAvaiable)
		{
			if (pair.Value == SelectModeGame.captionText.text) {
				string modeNameTemp = pair.Key;

				//lay ten mode code
				if(pair.Key == GameConstants.VNMODE_Random){
					CSVR_GameSetting.modeName = GetModeRandom();
				}else{
					CSVR_GameSetting.modeName = pair.Key;
				}

				//tim map trong mode do
				foreach (ItemModeV2 mo in lobbyResult.Lobby) {
					if (mo.ModeName == modeNameTemp) {
						//add map vao list map
						for (int j = 0; j < mo.Maps.Length; j++) {
							SelectMapGame.options.Add (new Dropdown.OptionData() {
								text = lobbyResult.MapAvaiable[mo.Maps[j]],
								image = HomeScreen.instance.modeMapSprites[HomeScreen.instance.modeMapSpritesIndex[ mo.Maps[j] ] ]
							});
						}
						SelectMapGame.value = -1;
					}
				}
			}
		}
		//Debug.Log ("modeName" +CSVR_GameSetting.modeName+ " mapName " +CSVR_GameSetting.mapName);
	}
	private string GetModeRandom()
	{
		string modeRandom = ListModeAvailable [Random.Range (1, ListModeAvailable.Count)].ModeName;
		//Debug.Log (modeRandom);
		return modeRandom;
	}

	private string GetMapRandomInMode()
	{
		string[] maps = lobbyResult.Lobby.Find(x => x.ModeName.Contains(CSVR_GameSetting.modeName)).Maps;
		string mapRandom = maps [Random.Range (1, maps.Length)];
		return mapRandom;
	}

	private void SetMapInCreate(){

		foreach (KeyValuePair<string, string> pair in lobbyResult.MapAvaiable)
		{
			if (pair.Value == SelectMapGame.captionText.text) {
				//lay ten mode code
				if(pair.Key == GameConstants.VNMAP_Random){
					CSVR_GameSetting.mapName = GetMapRandomInMode();
				}else{
					CSVR_GameSetting.mapName = pair.Key;
				}
			}
		}
	}
	public void HUD_OnCloseThis_Click()
	{
		this.Close ();
	}

	public void HUD_OnCreateRoomFinal_Click()
	{

		CSVR_GameSetting.TuTaoPhong = true;
		CSVR_GameSetting.IsQuickJoinedRoom = false;
		ErrorView.instance.Loading (true);
		//ErrorView.instance.Loading (true);

		if (CSVR_GameSetting.UseRoomVIP) {
			CSVR_RoomHostService.TryingRoomVIP = true;

			//PhotonNetwork.JoinRandomRoom(); //Tim room o default lobby, chinh la lobby chua roomVIP

			//Room Properties Matchmaking
			ExitGames.Client.Photon.Hashtable expectedProperties = new ExitGames.Client.Photon.Hashtable();
			expectedProperties.Add( GameConstants.Vip, "true" );
			PhotonNetwork.JoinRandomRoom( expectedProperties,2);
		} else {
			CSVR_MPConnection.instance.CreateNewRoom((byte)CSVR_GameSetting.maxPlayer,CSVR_GameSetting.modeName,CSVR_GameSetting.mapName,CSVR_GameSetting.accountLevel,CSVR_GameSetting.hostName);
		}
	}
}
