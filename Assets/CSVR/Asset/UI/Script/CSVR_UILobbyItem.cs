﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UILobbyItem : MonoBehaviour {

	public string Lobby_Id;
	public string Lobby_Ip;
	public float Player_Count;
	[SerializeField] private Text 	Lobby_Name;
	[SerializeField] private Text 	Lobby_Name_Status;
	[SerializeField] private Image 	Lobby_Count_Img;
	[SerializeField] private Button Lobby_Join;

	[SerializeField] private float 	Player_Max;
	CSVR_MPConnection mp_Connection;

	void Start(){
		mp_Connection = HorusManager.instance.gameModeInstance.GetComponentInChildren<CSVR_MPConnection> ();

	}


	public void GetInfo(LobbyItem item){
		Lobby_Id = item.LobbyId;
		Lobby_Ip = item.LobbyIp;
		Lobby_Name.text = item.LobbyName;
		Player_Max = item.PlayerMax;
		Player_Count = item.Players;

		if (Player_Count == Player_Max){
			Lobby_Name_Status.text = "Đầy";
			Lobby_Join.interactable = false;
		}else{
			Lobby_Name_Status.text = "Vào";
			Lobby_Join.interactable = true;
		}

		Lobby_Count_Img.fillAmount = (float)(Player_Count / Player_Max);
	}

	public void UI_OnJoinLobbyItem_Click(){
		mp_Connection.SelectedPhotonServer (Lobby_Ip);
		mp_Connection.Connect ();

		CSVR_UIListLobby.instance.Close ();
		MainView.instance.Close();
		CharacterView.instance.Close();
		CSVR_UIRoomLobbyScreen.instance.Open();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
	}

}
