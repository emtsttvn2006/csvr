﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChatView_GlobalChatContentCallBack : MonoBehaviour
{
    public string Name = string.Empty;
    public Text Message;
    public LayoutElement LayoutElement;

    void ScrollCellIndex(int idx)
    {
        Message.text = ChatCache.instance.GlobalCaches.Contents[idx];                
        //SetPreferHeight(Message.text.Length / CSVR_UIChat.instance.Max_Letters_In_String + 1);
    }

    /*
    void SetPreferHeight(int line)
    {
        LayoutElement.preferredHeight = CSVR_UIChat.instance.LineText_In_UI * line;        
    }*/

    public void ChatView_GlobalChatContent_Click()
    {
        if (this.Name != string.Empty)
            CSVR_UIChat.instance.messageInput.text = "@" + Name + " ";
    }
}