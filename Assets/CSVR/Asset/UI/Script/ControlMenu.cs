﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlMenu : MonoBehaviour {
	public GameObject _formRegister,_formLogin,_formNew;
	public Sprite _hoverRegister,_hoverLogin,bgRegister,bgLogin,_hoverInfo,bgInfo;
	public Button btnRegister,btnLogin,btnInfo;
	// Use this for initialization
	void Start () {
		btnLogin.image.overrideSprite = bgLogin;
		btnRegister.image.overrideSprite = bgRegister;
		btnInfo.image.overrideSprite = bgInfo;
		ShowFormInfo ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void ShowFormLogin()
	{
		btnRegister.image.overrideSprite = bgRegister;
		btnInfo.image.overrideSprite = bgInfo;
		btnLogin.image.overrideSprite = _hoverLogin;
		_formRegister.SetActive (false);
		_formLogin.SetActive (true);
		_formNew.SetActive (false);
	}
	public void ShowFormRegister()
	{
		btnLogin.image.overrideSprite = bgLogin;
		btnInfo.image.overrideSprite = bgInfo;
		btnRegister.image.overrideSprite = _hoverRegister;
		_formRegister.SetActive (true);
		_formLogin.SetActive (false);
		_formNew.SetActive (false);
	}
	public void ShowFormInfo()
	{
		btnLogin.image.overrideSprite = bgLogin;
		btnRegister.image.overrideSprite = bgRegister;
		btnInfo.image.overrideSprite = _hoverInfo;
		_formNew.SetActive (true);
		_formRegister.SetActive (false);
		_formLogin.SetActive (false);
	}
}
