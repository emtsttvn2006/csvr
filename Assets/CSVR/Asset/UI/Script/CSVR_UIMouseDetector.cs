﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIMouseDetector : MonoBehaviour
{
    public Vector3 mousePosition = Vector3.zero;

    void Awake()
    {
        CSVR_UIMouseClick.instance.Open();
        CSVR_UIMouseClick.instance.SetOnOff = false;
        CSVR_UIMouseClick.instance.ImageOnOff();
    }

    void FixedUpdate() { 
        if (Input.GetMouseButton(0))
        {
            if (!CSVR_UIMouseClick.instance.SetOnOff)
            {
                CSVR_UIMouseClick.instance.SetAlpha("AlphaIn");
                CSVR_UIMouseClick.instance.SetOnOff = true;
                CSVR_UIMouseClick.instance.DelayOn();
            }

            mousePosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            CSVR_UIMouseClick.instance.v3MoveTo = mousePosition;
            CSVR_UIMouseClick.instance.DelayMoveTo();
        }
        else
        {
            if (CSVR_UIMouseClick.instance.SetOnOff)
            {
                CSVR_UIMouseClick.instance.SetAlpha("AlphaOut");
                CSVR_UIMouseClick.instance.DelayOff();
            }

        }
    }
}
