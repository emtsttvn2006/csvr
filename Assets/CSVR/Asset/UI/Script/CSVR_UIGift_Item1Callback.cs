﻿using UnityEngine;
using System.Collections;
using Horus;
using Horus.ClientModels;
using UnityEngine.UI;
using MarchingBytes;

public class CSVR_UIGift_Item1Callback : MonoBehaviour
{
    public Text TextTitle;
    public Button ButtonCollect;
    public Text TextCollect;
    public LoopHorizontalScrollRect scroll;

    [HideInInspector]
    public BanbeUser BanbeUser { get; set; }

    void Start()
    {
        if (!scroll.prefabPool) scroll.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
    }

    void ScrollCellIndex(int idx)
    {
        
    }

    public void CSVR_UIGift_Item1_Click()
    {
        
    }
}