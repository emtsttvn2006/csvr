﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;
using MarchingBytes;

public class PopupBanbe : SingletonBaseScreen<PopupBanbe> {
    
    public string _id;

    public Button ButtonNguoiChoi;
    public Button ButtonBanbe;
    public Button ButtonDongY;
    public Button ButtonXoaBan;
    public Button ButtonHuyKetBan;
    public Button ButtonChoiCung;
    public Button ButtonBack;

    public Text TextName_Chosen;
    public Text TextLevel_Chosen;
    public Text TextRankCall_Chosen;
    public Text TextClan_Chosen;    

    public Dropdown StyleSapxepDropdown;
    public InputField IPFTimkiem;

    public LoopVerticalScrollRect banbeScroll;

    [HideInInspector] public List<RelationshipInfo> listBanBe = new List<RelationshipInfo>();
    [HideInInspector] public List<FriendInfo> listBanbePhoton = new List<FriendInfo>();
    [HideInInspector] public PopupBanbeItemCallback PopupBanbeItem_Chosen = null;    
    
    bool WaitForEvent = false;    
    
    // Use this for initialization
    void Start() {
        if (!banbeScroll.prefabPool) banbeScroll.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();        
    }    

    void reset()
    {
        ButtonNguoiChoi.interactable = false;
        ButtonBanbe.interactable = false;
        ButtonDongY.gameObject.SetActive(false);
        ButtonXoaBan.gameObject.SetActive(false);
        ButtonHuyKetBan.gameObject.SetActive(false);
        ButtonChoiCung.gameObject.SetActive(false);

        PopupBanbeItem_Chosen = null;
        TextName_Chosen.text = string.Empty;
        TextLevel_Chosen.text = string.Empty;
        TextRankCall_Chosen.text = string.Empty;
        TextClan_Chosen.text = string.Empty;

        banbeScroll.ClearCells();
    }

    public void ButtonNguoiChoi_Click()
    {
        reset();
        ButtonBanbe.interactable = true;
    }

    public void UpdateStatusBanbe_Click(int status)
    {        
        UpdateRelationshipRequest UpdateRelationshipRequest = new UpdateRelationshipRequest();

        if (PopupBanbeItem_Chosen != null)
        {
            string compareID = (PopupBanbeItem_Chosen.RelationshipInfo.CreateId == HorusManager.instance.playerID) ? PopupBanbeItem_Chosen.RelationshipInfo.PartnerId : PopupBanbeItem_Chosen.RelationshipInfo.CreateId;
            UpdateRelationshipRequest.Status = status;
            UpdateRelationshipRequest.PartnerId = compareID;

            HorusClientAPI.UpdateRelationship(UpdateRelationshipRequest,
                (result) =>
                {   
                    if (status == 2)
                    {
                        UpdateDanhSachBanBe();
                        PopupBanbeItem_Chosen.RelationshipInfo.Status = 2;
                        ShowRelationShip(PopupBanbeItem_Chosen);
                    } else if (status == 0)
                    {
                        ButtonBanbe_Click();
                    }
                },
                (error) => 
                {
                    Debug.Log("error");
                }, null);
        }        
    }

    public void ButtonBanbe_Click()
    {
        reset();
        ButtonNguoiChoi.interactable = true;
        UpdateDanhSachBanBe();        
    }

    public void ButtonRefresh_Click()
    {
        if (ButtonNguoiChoi.IsInteractable()) UpdateDanhSachBanBe();
        else if(ButtonBanbe.IsInteractable()) UpdateDanhSachNguoichoi();
    }

    public void UpdateDanhSachNguoichoi()
    {
    }

    public void UpdateDanhSachBanBe()
    {
        HorusClientAPI.GetRelationShip(new GetRelationShipRequest(),
            (result) => {
                this.listBanBe = result.RelationshipList;                                
                for (int i = 0; i < this.listBanBe.Count; i++)
                {
                    if (this.listBanBe[i].Partner == null || this.listBanBe[i].Status == 0)
                    {
                        listBanBe.RemoveAt(i);
                        i--;
                    }
                }
                string[] BanBeArrayName = new string[listBanBe.Count];
                for (int i = 0; i < listBanBe.Count; i++)
                {
                    BanBeArrayName[i] = (listBanBe[i].CreateId == HorusManager.instance.playerID) ? listBanBe[i].PartnerId : listBanBe[i].CreateId;
                }
                
                if (BanBeArrayName.Length > 0) PhotonNetwork.FindFriends(BanBeArrayName); 
            },
            (error) => { }, null);

        #region xử lý cũ local
        //Xoa cu~ tren UI
        /*
        relationshipInfos_popup = new List<RelationshipInfo>();
        foreach (Toggle tg in ItemBanbeHolder.GetComponentsInChildren<Toggle>(true))
        {
            int index = GetIndexByUserName(tg.GetComponent<PopupBanbeItem>().RelationshipInfo.Partner.UserName, this.relationshipInfos_result);
            if (!tg.GetComponent<PopupBanbeItem>() || tg.GetComponent<PopupBanbeItem>().RelationshipInfo == null || tg.GetComponent<PopupBanbeItem>().RelationshipInfo.Partner == null || index == -1)
            {
                Destroy(tg.gameObject);
            }
            else
            {
                relationshipInfos_popup.Add(tg.GetComponent<PopupBanbeItem>().RelationshipInfo);
                tg.GetComponent<PopupBanbeItem>().Show(relationshipInfos_result[index]);
            }
        }

        for (int i = 0; i < relationshipInfos_result.Count; i++)
        {
            if (GetIndexByUserName(relationshipInfos_result[i].Partner.UserName, relationshipInfos_popup) == -1)
            {
                GameObject go = (GameObject)Instantiate(ItemBanbePrefab, Vector3.zero, Quaternion.identity);
                go.transform.SetParent(ItemBanbeHolder.transform);
                go.gameObject.SetActive(true);
                ItemBanbeHolder.RegisterToggle(go.GetComponent<Toggle>());
                go.GetComponent<PopupBanbeItem>().Show(relationshipInfos_result[i]);                
                go.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                relationshipInfos_popup.Add(go.GetComponent<PopupBanbeItem>().RelationshipInfo);

            }
        }

        switch (StyleSapxepDropdown.value)
        {
            case 0: relationshipInfos_popup.Sort(NameComparator); break;
            case 1: relationshipInfos_popup.Sort(LevelComparator); break;
        }

        for (int i = 0; i < ItemBanbeHolder.GetComponentsInChildren<Toggle>().Length; i++)
        {
            ItemBanbeHolder.transform.GetChild(i).GetComponent<PopupBanbeItem>().Show(relationshipInfos_popup[i]);
        }
        */
        #endregion
    }

    public void OnUpdatedFriendList()
    {
        //		Debug.Log("co chay khong");
        if (!PhotonNetwork.connected || PhotonNetwork.Friends == null || !PhotonNetwork.insideLobby)
        {            
            return;
        }
        listBanbePhoton = PhotonNetwork.Friends;

        banbeScroll.totalCount = listBanBe.Count;
        banbeScroll.RefillCells();
    }    

    int NameComparator(RelationshipInfo info1, RelationshipInfo info2)
    {
        return info1.Partner.UserName.CompareTo(info2.Partner.UserName);
    }

    int LevelComparator(RelationshipInfo info1, RelationshipInfo info2)
    {
        return info1.Partner.AccountLevel.CompareTo(info2.Partner.AccountLevel);
    }

    int GetIndexByUserName(string name, List<RelationshipInfo> infos)
    {
        int index = -1;
        for (int i = 0; i < infos.Count; i++)
        {
            if (infos[i].Partner != null && infos[i].Partner.UserName == name)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    int GetIndexById(string ID, List<RelationshipInfo> infos)
    {
        int index = -1;
        for (int i = 0; i < infos.Count; i++)
        {
            if (infos[i].Partner != null && infos[i].Partner._id == ID)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public void ResetIPFTimkiem()
    {
        /*IPFTimkiem.text = string.Empty;
        for (int i = 0; i < ItemBanbeHolder.transform.childCount; i++)
        {
            ItemBanbeHolder.transform.GetChild(i).gameObject.SetActive(true);
        }*/
    }

    public void ButtonTimkiem_Click()
    {/*
        if (IPFTimkiem.text != string.Empty)
        {
            foreach (Transform tr in ItemBanbeHolder.transform)
            {
                if (!tr.GetComponent<PopupBanbeItem>().RelationshipInfo.Partner.UserName.Contains(IPFTimkiem.text))
                {
                    tr.gameObject.SetActive(false);
                }
            }
        }*/
    }

    public void Button_Back_Click()
    {
        this.gameObject.SetActive(false);
    }

	public void ShowRelationShip(PopupBanbeItemCallback item )
    {
        PopupBanbeItem_Chosen = item;

        TextName_Chosen.text = (PopupBanbeItem_Chosen.RelationshipInfo.Partner.DisplayName == string.Empty || item.RelationshipInfo.Partner.DisplayName == null) ? item.RelationshipInfo.Partner.UserName : item.RelationshipInfo.Partner.DisplayName;
        TextLevel_Chosen.text = PopupBanbeItem_Chosen.RelationshipInfo.Partner.AccountLevel.ToString();
        TextRankCall_Chosen.text = string.Empty;
        //TextClan_Chosen.text = PopupBanbeItem_Chosen.RelationshipInfo.Partner.ClanName;

        if (PopupBanbeItem_Chosen.RelationshipInfo.Status == 2)
        {
            ButtonDongY.gameObject.SetActive(false);
            ButtonXoaBan.gameObject.SetActive(true);            
            ButtonHuyKetBan.gameObject.SetActive(false);
            if (PopupBanbeItem_Chosen.RoomPlaying.Length > 0)
            {
                ButtonChoiCung.gameObject.SetActive(true);
            } else
            {
                ButtonChoiCung.gameObject.SetActive(false);
            }

        } else if (PopupBanbeItem_Chosen.RelationshipInfo.Status == 1)
        {            
            ButtonDongY.gameObject.SetActive(!(PopupBanbeItem_Chosen.RelationshipInfo.CreateId == HorusManager.instance.playerID));
            ButtonXoaBan.gameObject.SetActive(false);
            ButtonHuyKetBan.gameObject.SetActive(true);
        }
    }    

    public void ButtonChoiCung_Click()
    {
        if (PopupBanbeItem_Chosen != null)
        {
            CSVR_MPConnection.instance.OnQuickJoinedRoom(true);
            CSVR_MPConnection.instance.JoinRoom(PopupBanbeItem_Chosen.RoomPlaying);
        }
    }
}