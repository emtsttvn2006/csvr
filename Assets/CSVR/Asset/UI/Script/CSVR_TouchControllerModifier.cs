﻿using UnityEngine;
using System.Collections;

public class CSVR_TouchControllerModifier : MonoBehaviour
{
    public static CSVR_TouchControllerModifier instance;
    public Texture2D crouchButtonImage;
    public Texture2D standButtonImage;

    public Texture2D[] baloButtonImage;

    public string GrenadeName;

    // Use this for initialization
    void Start()
    {
        instance = this;
    }
}
