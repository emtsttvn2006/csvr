﻿using UnityEngine;
using System.Collections;
using Horus.ClientModels;

public class CSVR_Quest : Singleton<CSVR_Quest> {
    /*    
    public void HandleAQuestResult(QuestProcessResult result)
    {
        if (result.questProcess == "finish")
        {
            foreach (QuestProcessResult.ItemOwn item in result.itemsOwn)
            {
                switch (item.ItemId)
                {
                    case "01_01_coin" : UpdateCoin(item.RemainingUses);
                        break;
                } 
            }
        }
    }*/

    public void UpdateCoin(int value)
    {
        AccountManager.instance.gameCoinAmount = value;
        //UpdateValue(ref AccountManager.instance.gameCoinAmount, value);
    }

    public void UpdateValue(ref int i, int value)
    {
        i = value;
    }

    public void UpdateValue(ref int i, string value)
    {
        i = int.Parse(value);
    }

    public void UpdateValue(ref string s, string value)
    {
        s = value;
    }

    public void UpdateValue(ref string s, int value)
    {
        s = value.ToString();
    }
}
