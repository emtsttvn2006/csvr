﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIClanList_XemMyClan_ThongBaoItem_Callback : MonoBehaviour
{
    public Text TextContent;
    public Text TextDate;

    [HideInInspector]
    public XemMyClan_TinNhan XemMyClan_TinNhan;

    void ScrollCellIndex(int idx)
    {
        this.XemMyClan_TinNhan = CSVR_UIClanList_XemMyClan.instance.listTinnhan[idx];
        this.TextContent.text = this.XemMyClan_TinNhan.content;
        this.TextDate.text = this.XemMyClan_TinNhan.date;
    }
}
