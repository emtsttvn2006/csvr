﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIClanList_TaoClan_MuaAvata_AvataFreeItem_Callback : MonoBehaviour
{
    public Image ImgSprite;

    void ScrollCellIndex(int idx)
    {
        this.ImgSprite.sprite = CSVR_UIClanList_TaoClan_MuaAvata.instance.listMienPhi[idx].Sprite;
    }

    void Button_Click()
    {
        CSVR_UIClanList_TaoClan.instance.IMGAvatarClan = ImgSprite;
        CSVR_UIClanList_TaoClan_MuaAvata.instance.Close();
    }
}
