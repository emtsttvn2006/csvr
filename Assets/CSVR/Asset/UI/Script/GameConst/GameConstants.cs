﻿using UnityEngine;
using System.Collections;

public class GameConstants  {
	
	//room property
	public const string Mode = "C0";
	public const string Map = "C1";
	public const string SkillLevel = "C2";
	public const string Name = "C3";
	public const string Host = "C4";

	public const string Vip = "C9";

	public const string VNITEMDEFAULT_Knife = "77_999_Knife";
	//class item weapon
	public const string VNITEMCLASS_Empty = "";

	public const string VNITEMCLASS_RifleWeapon = "Rifle.Weapon";
	public const string VNITEMCLASS_SniperWeapon = "Sniper.Weapon";
	public const string VNITEMCLASS_ShotGunWeapon = "ShotGun.Weapon";
	public const string VNITEMCLASS_SMGWeapon = "SMG.Weapon";
	public const string VNITEMCLASS_MachineWeapon = "Machine.Weapon";

	public const string VNITEMCLASS_PistolWeapon = "Pistol.Weapon";
	public const string VNITEMCLASS_MeleeWeapon = "Melee.Weapon";
	public const string VNITEMCLASS_GrenadeWeapon = "Grenade.Weapon";
	public const string VNITEMCLASS_OneGrenadeWeapon = "One.Grenade.Weapon";
	public const string VNITEMCLASS_TwoGrenadeWeapon = "Two.Grenade.Weapon";
	public const string VNITEMCLASS_HelmetWeapon = "Helmet.Weapon";
	public const string VNITEMCLASS_ArmorWeapon = "Armor.Weapon";

	public const string VNITEMCLASS_MuzzleAccessories = "muzzle.plugin";
	public const string VNITEMCLASS_GripAccessories = "grip.plugin";
	public const string VNITEMCLASS_MagazineAccessories = "magazine.plugin";
	public const string VNITEMCLASS_ScopeAccessories = "scope.plugin";
	public const string VNITEMCLASS_SkinAccessories = "skin.plugin";
	public const string VNITEMCLASS_AccAccessories = "acc.plugin";

	public const string VNITEMCHARACTER_Equip = "11.11";
	public const string VNITEMCLASS_DefaultCharacter = "default.character";

	public const string VNITEMCATALOG_Item = "Item";
	public const string VNITEMCATALOG_Upgrade = "Upgrade";
	public const string VNITEMCATALOG_ItemPeriod = "ItemPeriod";
	public const string VNITEMCATALOG_Accessories = "accessories";
	public const string VNITEMCATALOG_Character = "Character";
	public const string VNITEMCATALOG_BundleSpin = "BundleSpin";
	//danh cho lat bai
	public const string VNITEMCATALOG_BundleCard = "BundleCard";
	public const string VNITEMCATALOG_Key = "key";
	//end

	//prefix item
	public const string VNITEMPREFIX_Empty = "";
	public const string VNITEMPREFIX_3Day = "_3day";
	public const string VNITEMPREFIX_7Day = "_7day";
	public const string VNITEMPREFIX_30Day = "_30day";
	public const string VNITEMPREFIX_Upgrade = "_up";

	public const string VNITEMID_Coin = "01_01_coin";
	public const string VNITEMID_Gold = "01_02_gold";

	public const string VNPRICE_Coin = "coin";
	public const string VNPRICE_Gold = "gold";

	public const string VNTAG_Police = "PolicePlayer";
	public const string VNTAG_Terrorist = "TerrorisPlayer";

	public const string VNMODE_Random = "CSVR_RandomMode";
	public const string VNMAP_Random = "Map_Random";


	public const string VNMODESAD_BombC4 = "00_000_C4";
	public const string VNMODESAD_BombSiteA = "BombSiteA";
	public const string VNMODESAD_BombSiteB = "BombSiteB";


	public const string VNROLE_Exp = "exp_percent";
	public const string VNROLE_Coin = "coin_percent";
	public const string VNROLE_Gold = "gold_percent";

	//nhan luong hang ngay
	public const string VNQUESTID_Reward = "daily-salary-level";

	public const string VNQUESTSERIAL_7day = "7days";
	public const string VNQUESTSERIAL_30day = "daily31days";
	public const string VNQUESTSERIAL_Mission = "dailyMission";
	public const string VNQUESTSERIAL_Main = "mainQuest";
	public const string VNQUESTSERIAL_Level = "levelupgift";

	//trang thai quest
	public const string QUEST_FINISH = "finish";//vua lam xong
	public const string QUEST_DONE = "done-before";//xong lau roi
	public const string QUEST_DOING = "doing";//dang lam
	public const string QUEST_PENDING = "pending";//cho lam,ex: cho sang ngay moi

	public const string QUEST_KILL = "kill";
	public const string QUEST_DEATH = "death";
}
