﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using MarchingBytes;
using Horus.ClientModels;
using UnityEngine.Events;
using Horus;

public class CSVR_UIClanList_XemClan : SingletonBaseScreen<CSVR_UIClanList_XemClan>
{    
    public Image IMGAvatarClan;
    public Text TextClanName;
    public Text TextClanDescription;
    public Text TextChuClan;
    public Image ImgClanLV;
    public Text TextClanMember;
    public Text TextClanDate;

    public Button ButtonXinVao;

    public LoopVerticalScrollRect scrollMembers;

    [HideInInspector]
    public ClanResult ClanResult;

    [HideInInspector]
    public List<UserInfo> listUserInfo = new List<UserInfo>();

    [HideInInspector]
    public List<string2> listString_Members = new List<string2>();

    public override void Open()
    {
        base.Open();
        Init();
    }

    void Start()
    {
        if (scrollMembers.prefabPool == null) scrollMembers.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
    }

    void Init()
    {
        IMGAvatarClan.sprite = UISpriteManager.instance.GetClanLogoSprite("_default");
        TextClanName.text = "--";
        TextClanDescription.text = "--"; 
        TextChuClan.text = "--";         
        TextClanMember.text = "--"; 
        TextClanDate.text = "--";
        ImgClanLV.sprite = UISpriteManager.instance.GetLevelSprite(0);

        this.listString_Members.Clear();
        this.listUserInfo.Clear();
        UpdateUI();
    }

    public void Show(ClanResult ClanResult)
    {
        if (ClanResult != null)
        {
            this.ClanResult = ClanResult;
            this.ClanResult.UpdateAllMemberArray();

            if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName == this.ClanResult.roleId && CSVR_UICLanList.instance.MyClan != null && CSVR_UICLanList.instance.MyClan.roleId == this.ClanResult.roleId)
            {
                ButtonXinVao.interactable = false;
            } else
            {
                ButtonXinVao.interactable = true;
            }

            GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();

            TextClanName.text = this.ClanResult.roleId;
            if (this.ClanResult.logo != null && this.ClanResult.logo != "")
            {
                IMGAvatarClan.sprite = UISpriteManager.instance.GetClanLogoSprite(this.ClanResult.logo);
            } else
            {
                IMGAvatarClan.sprite = UISpriteManager.instance.GetClanLogoSprite("0");
            }
            ImgClanLV.sprite = UISpriteManager.instance.GetLevelSprite(this.ClanResult.level);
            TextClanDescription.text = (this.ClanResult.description == "") ? "Điếc không sợ súng!!!" : this.ClanResult.description;
            TextChuClan.text = this.ClanResult.chief;            
            TextClanMember.text = this.ClanResult.memberCount.ToString();
            TextClanDate.text = this.ClanResult.created_at;

            this.listString_Members.Clear();
            this.listUserInfo.Clear();
            UpdateUI();

            GetUserInfoRequest.Add(this.ClanResult.chief);
            this.listString_Members.Add(new string2("TRƯỞNG CLAN", this.ClanResult.chief));

            for (int i = 0; i < this.ClanResult.Vice.Count; i++)
            {
                if (GetIndex(this.ClanResult.Vice[i], this.listString_Members) == -1)
                {
                    GetUserInfoRequest.Add(this.ClanResult.Vice[i]);
                    this.listString_Members.Add(new string2("PHÓ CLAN", this.ClanResult.Vice[i]));
                }
            }

            for (int i = 0; i < this.ClanResult.Members.Count; i++)
            {
                if (GetIndex(this.ClanResult.Members[i], this.listString_Members) == -1)
                {
                    this.listString_Members.Add(new string2("THÀNH VIÊN", this.ClanResult.Members[i]));
                    GetUserInfoRequest.Add(this.ClanResult.Members[i]);
                }
            }

            CSVR_UIWaitingIcon.instance.Open();

            HorusClientAPI.GetUserInfo(GetUserInfoRequest,
                (result) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    this.listUserInfo = result.users;
                    UpdateUI();
                },
                (error) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();
                }, null);
        } else
        {
            ErrorView.instance.ShowPopupError("Không tìm thấy thông tin. Hãy quay lại sau!!");
        }
    }

    public void UpdateUI()
    {
        scrollMembers.totalCount = this.listString_Members.Count;
        scrollMembers.RefillCells();
    }


    public int GetIndex(string Name, List<UserInfo> list)
    {
        return list.FindIndex((UserInfo) => UserInfo.UserName == Name);
    }

    public int GetIndex(string Name, List<string2> list)
    {
        return list.FindIndex((string2) => string2.Value == Name);
    }

    public void Button_XinVao_Click()
    {
        if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "")
        {
            ErrorView.instance.ShowPopupError("HÃY THOÁT CLAN HIỆN TẠI ĐỂ THỰC HIỆN THAO TÁC NÀY!!!");
        }
        else
        {
            CSVR_UICLanList.instance.JoiningAClan(this.ClanResult.roleId);
        }
    }
    
}
