﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Horus;
using Horus.ClientModels;

public class CSVR_UIEndGameChild : MonoBehaviour {
	public GameObject thisGameObject;
	public GameObject isMine;
	public Image playerAvatar;
	public Text playerName;
	public Text playerLevel;
	public Image playerLevelIcon;
	public Text playerKDA;
	public Image playerGun;
	public Text gunName;
	public Button addFriend;
	public string idFacebook;
	public string horusID;

	public void UI_OnAddFriendClick(){
//		ErrorView.instance.Loading(true);
//
//		UpdateRelationshipRequest request = new UpdateRelationshipRequest();
//		request.Status = 1;
//		request.PartnerId = horusID;
//		HorusClientAPI.UpdateRelationship(request,
//			(result) =>
//			{   
//				addFriend.interactable = false;
//				ErrorView.instance.ShowPopupError("Kết bạn thành công!..");
//			},
//			(error) => 
//			{
//				ErrorView.instance.ShowPopupError("Kết bạn không thành công!..");
//			}, null);

	}

	public void UI_OnProfileFacebookClick(){
		Application.OpenURL ("https://www.facebook.com/" + idFacebook);
	}
}
