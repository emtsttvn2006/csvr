﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using System.Collections;
using Horus;
using Horus.ClientModels;
using Horus.Internal;
using Horus.Json;

public class MainView : SingletonBaseScreen<MainView> {    
	
	[SerializeField] private Image avatar_Icon;
	[SerializeField] private Text player_Name;
	[SerializeField] private Text player_Level;
	[SerializeField] private Image level_Icon;
	[SerializeField] private Transform  ChatView_Parent;

    public GameObject notification_Icon;
    public GameObject Noti_Tinnhan;

    public Button ButtonQuickPlay;
    public Button ButtonInventory;

    void Start(){
		UpdateInfoPlayer();
		if (CSVR_PlayerMessenger.instance.IsConnected() == false )
		{
			if (DebugError) Debug.Log("Đang cố gắng đăng nhập lại Chat");
			CSVR_PlayerMessenger.instance.Connect();
		}
    }

	public void UpdateInfoPlayer(){
		avatar_Icon.sprite = AccountManager.instance.avatarPicture;
		player_Name.text = AccountManager.instance.displayName;
		player_Level.text = "Cấp độ: "+ CSVR_GameSetting.accountLevel.ToString();
		level_Icon.sprite = UISpriteManager.instance.GetLevelSprite(CSVR_GameSetting.accountLevel);
	}    

    public override void OnEnable()
	{
		base.OnEnable ();

		TopBar.instance.SetBackButton(new UnityAction(() =>{
			Open();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			MainView.instance.Close();
			CSVR_UIShop.instance.Open();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));

		TopBar.instance.Setting.interactable =  true;
		TopBar.instance.ShopOrInVText.text = "Cửa hàng";    
		Invoke ("ChatViewSetParent", 0.5f);
	}

    public override void Open()
    {
        base.Open();

        /*
        if (TutorialManager.instance != null)
        {
            if (TutorialManager.instance.TutorialState == TutorialState.NOTHING)
            {
                CSVR_UITutorial.instance.Open();
                CSVR_UITutorial.instance.ShowAction();
            } else if (TutorialManager.instance.TutorialState == TutorialState.DOING)
            {
                CSVR_UITutorial.instance.Open();
                CSVR_UITutorial.instance.ShowContinue();
            }
        } */
    }

    private void ChatViewSetParent(){
		HomeScreen.instance.ChatView_Item.SetParent (ChatView_Parent, false);
		RectTransform rectTransform = HomeScreen.instance.ChatView_Item.GetComponent<RectTransform>();
		rectTransform.offsetMin = Vector3.zero;
		rectTransform.offsetMax = Vector3.zero;
	}
	public void UI_MaiViewToInventory_Click()
	{
		MainView.instance.Close();
		CSVR_UIInventory.instance.Open();
		CSVR_UIInventory.instance.UI_OnInventoryOn();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
	}

	public void UI_MaiViewToQuickPlay_Click()
	{
		MainView.instance.Close();
		CharacterView.instance.Close();
		CSVR_UIMultiplayerLobby.instance.Open();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
	}
	public void UI_MaiViewToSanLung_Click()
	{
		MainView.instance.Close();
		CharacterView.instance.Close();
		CSVR_UISanLungOne.instance.Open();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
	}
	public void UI_MaiViewToRoomLobby_Click()
	{
		MainView.instance.Close();
		CharacterView.instance.Close();
		CSVR_UIRoomLobbyScreen.instance.Open();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
	}		
	public void UI_MaiViewOpenLeaderBoard_Click()
	{
		CSVR_UILeaderBoard.instance.Open();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}	
	public void UI_MaiViewToProfilePlayer_Click()
    {
		MainView.instance.Close();
		CSVR_UIInventory.instance.Open();
        CSVR_UIInventory.instance.UI_OnProfilePlayerOn();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
    }

	public void UI_MaiViewChangePolice_Click()
	{
		CharacterView.instance.ShowModePolice ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}

	public void UI_MaiViewChangeTerrorist_Click()
	{
		CharacterView.instance.ShowModeTerrorist ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}

	public void UI_MaiViewOpenGiftCode_Click()
	{
		ReferralCodeView.instance.Open ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}
	public void UI_MaiViewOpenQuest_Click()
	{
		CSVR_UIQuest.instance.Open ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
	}
	public void UI_MaiViewOpenSpin_Click()
	{
		MainView.instance.Close();
		CharacterView.instance.Close();
		CSVR_UISpin.instance.Open ();
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);

	}
	public void UI_New_Click()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		Application.OpenURL ("https://www.facebook.com/groups/supportCSM/");
	}
	public void UI_Event_Click()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		Application.OpenURL ("https://www.facebook.com/groups/supportCSM/");
	}
	public void UI_Buddy_Click()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		CSVR_UIBuddy.instance.Open();        

	}
    public void UI_Tinnhan_Click()
    {
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);
        Noti_Tinnhan.gameObject.SetActive(false);                
    }
		
    public void UI_Phanthuong_Click()
    {
     	CSVR_UIGift.instance.Open();
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
    }

	public void UI_MaiViewOpenListLobby_Click(){
		CSVR_UIListLobby.instance.Open();
	}

    public void UI_ClanList_Click()
    {
        CSVR_UICLanList.instance.Open();
        this.Close();
        CharacterView.instance.Close();

        TopBar.instance.SetBackButton(new UnityAction(() => {
            this.Open();
            CharacterView.instance.Open();
            CSVR_UICLanList.instance.Close();
            CSVR_UIClanItem_MyClanItem.instance.CloseChildrenToo();
        }));
    }
}
