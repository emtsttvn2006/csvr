﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class AnAvatar
{
    public Sprite Sprite { get; set; }
    public int Price { get; set; }

    public AnAvatar()
    {
        this.Sprite = null;
        this.Price = -1;
    }

    public AnAvatar(Sprite Sprite, int Price)
    {
        this.Sprite = Sprite;
        this.Price = Price;
    }
}

public class CSVR_UIClanList_TaoClan_MuaAvata : SingletonBaseScreen<CSVR_UIClanList_TaoClan_MuaAvata> {

    public Toggle ToggleMienPhi;
    public Toggle ToggleTraphi;

    public LoopHorizontalScrollRect scrollRect;

    public List<AnAvatar> listMienPhi;
    public List<AnAvatar> listTraPhi;

    public override void Open()
    {
        base.Open();
        if (this.listMienPhi == null || this.listMienPhi.Count == 0 || this.listTraPhi == null || this.listTraPhi.Count == 0)
        {
            Init();
        }
    }
    
    void Init()
    {
        this.listMienPhi = new List<AnAvatar>()
        {
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("1"), 0),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("2"), 0),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("3"), 0),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("7"), 0),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("8"), 0),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("9"), 0)
        };

        this.listMienPhi = new List<AnAvatar>()
        {            
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("4"), 20),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("5"), 20),
            new AnAvatar(UISpriteManager.instance.GetClanLogoSprite("6"), 20)
        };
    }
}
