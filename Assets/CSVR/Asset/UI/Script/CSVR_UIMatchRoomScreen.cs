using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UIMatchRoomScreen : SingletonBaseScreen<CSVR_UIMatchRoomScreen>
{
	/// <summary>
	/// Portrait border of the police team
	/// </summary>
	[SerializeField]
	Image[] policeTeamPortraitBorders;
	
	/// <summary>
	/// Text of the police team name
	/// </summary>
	[SerializeField]
	Text[] policeTeamNames;
	
	
	/// <summary>
	/// Portrait border of the terrorist team
	/// </summary>
	[SerializeField]
	Image[] terroristTeamPortraitBorders;

	/// <summary>
	/// Text of the terrorist team name
	/// </summary>
	[SerializeField]
	Text[] terroristTeamNames;

	
	TeamTypes selectedTeam;
	
	
	/// <summary>
	/// Holds the player count of the teams
	/// </summary>
	int[] teamsCount;
	
	/// <summary>
	/// Check if the player is ready
	/// </summary>
	bool ready=false;
	
	/// <summary>
	/// Total of ready players
	/// </summary>
	int playersReady;
	
	/// <summary>
	/// Check if the time is started
	/// </summary>
	bool timerStarted;
	
	/// <summary>
	/// Value of the time counter
	/// </summary>
	float timerCounter;
	[SerializeField]
	GameObject inviteFriends;

	public Image mapImage;
	public Text modeName,mapName,maxPlayer,maxKill,maxTime;
	public Button changeInfoButton,startGameButton;


	ProjectDelegates.SimpleCallback OnMatchRoomScreenCallBack;
	void Start()
	{
		teamsCount = new int[] { 0, 0 };
		this.UpdateTeamPortraits();

		OnRoomInfoChanged (PhotonNetwork.isMasterClient);
	}

	public override void OnEnable()
	{
		OnMatchRoomScreenCallBack = HomeScreen.instance.OnMatchRoomScreenCallBack;
		OnMatchRoomScreenCallBack ();  

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{

		}));
		TopBar.instance.ShopOrInVText.text = "Thùng đồ";
	}

	public void ChangeTeam(bool syncNetwork)
	{
		if (ready) return;

		int newID = selectedTeam.GetHashCode();
		newID++;
		if (newID > TeamTypes.Terrorist.GetHashCode())
		{
			newID = 0;
		}

		TeamTypes newTeam = (TeamTypes)newID;
		
		if (newTeam != selectedTeam)
		{
			selectedTeam = newTeam;
			CSVR_MPConnection.instance.ChangeTeam(selectedTeam,syncNetwork);
		}
	}

	
	void UpdateTeamPortraits()
	{
		//int maxPlayersInTeam = MultiplayerRoomsManager.instance.GetMaxPlayers() / 2;
		int maxPlayersInTeam = 12;
		for (int i = 0; i < policeTeamPortraitBorders.Length; i++)
		{
			if (i >= maxPlayersInTeam)
			{
				policeTeamPortraitBorders[i].gameObject.SetActive(false);
				if (policeTeamNames != null) policeTeamNames[i].gameObject.SetActive(false);

				terroristTeamPortraitBorders[i].gameObject.SetActive(false);
				if (terroristTeamNames != null) terroristTeamNames[i].gameObject.SetActive(false);
			}
		}
	}
	
	void ClearPortraits()
	{
        teamsCount = new int[] { 0, 0 };
		for (int i = 0; i < policeTeamPortraitBorders.Length; i++)
		{
			policeTeamPortraitBorders[i].color = Color.white;
			policeTeamPortraitBorders[i].transform.GetChild(1).gameObject.SetActive(false);
			if (policeTeamNames != null) policeTeamNames[i].text = "";

			terroristTeamPortraitBorders[i].color = Color.white;
			terroristTeamPortraitBorders[i].transform.GetChild(1).gameObject.SetActive(false);
			if (terroristTeamNames != null) terroristTeamNames[i].text = "";
		}
	}

	public void OnRoomPropertiesChanged(RoomInfo roomInfo)
	{

		this.Open ();
		this.ClearPortraits();

		PhotonPlayer[] players = PhotonNetwork.playerList;

		if (PhotonNetwork.isMasterClient) {
			startGameButton.gameObject.SetActive (true);
		} else {
			startGameButton.gameObject.SetActive (false);
		}
			
		for (int i = 0; i < players.Length; i++)
		{
			TeamTypes playerTeam = (TeamTypes)int.Parse(players[i].customProperties["Team"].ToString());
			if (playerTeam == TeamTypes.RoomHostPlayer)
				continue;

			string playerNameID = players[i].name;
			string playerNameHUD = players [i].customProperties ["PlayFabName"].ToString ();


			bool isMe = players[i].name == AccountManager.instance.displayName;
			bool isHost = players[i].isMasterClient;
			
			if (isMe){ 
				this.CheckMyPlayerProperties(playerNameID,playerTeam);
			}
			
			int myTeamCount = teamsCount[playerTeam.GetHashCode()];
			
			if (playerTeam == TeamTypes.Police)
			{
				policeTeamNames[myTeamCount].text = playerNameHUD;
                if (isHost){
                    policeTeamPortraitBorders[myTeamCount].transform.GetChild(1).gameObject.SetActive(true);
                }else{
                    policeTeamPortraitBorders[myTeamCount].transform.GetChild(1).gameObject.SetActive(false);
                }
			}
			else if (playerTeam == TeamTypes.Terrorist)
			{
				terroristTeamNames[myTeamCount].text = playerNameHUD;
                if (isHost){
                    terroristTeamPortraitBorders[myTeamCount].transform.GetChild(1).gameObject.SetActive(true);
                }else{
                    terroristTeamPortraitBorders[myTeamCount].transform.GetChild(1).gameObject.SetActive(false);
                }
			}
			
			teamsCount[playerTeam.GetHashCode()]++;
		}

	}

	public void ChangeInfo()
	{
		//tat vi chua co tinh nang doi thong tin phong trong tao phong
		/*
		if (ready) return;
		CSVR_GameSetting.selectPlayModeValue = SelectPlayModeInRoom.value;

		CSVR_GameSetting.maxPlayer = int.Parse(SelectPlayerInRoom.options[SelectPlayerInRoom.value].text);

		CSVR_GameSetting.selectModeGameValue = SelectModeGameInRoom.value;
		SelectModeGameInRoomQuick.value = CSVR_GameSetting.selectModeGameValue;
		//CSVR_GameSetting.maxPlayer = int.Parse(SelectPlayerInRoom.options[SelectPlayerInRoom.value].text);

		CSVR_GameSetting.selectMapGameValue = SelectMapGameInRoom.value;
		SelectMapGameInRoomQuick.value = CSVR_GameSetting.selectMapGameValue;
		//CSVR_GameSetting.maxPlayer = int.Parse(SelectPlayerInRoom.options[SelectPlayerInRoom.value].text);

		CSVR_GameSetting.selectPlayerValue = SelectPlayerInRoom.value;
		SelectPlayerInRoomQuick.value = CSVR_GameSetting.selectPlayerValue;
		CSVR_GameSetting.maxPlayer = int.Parse(SelectPlayerInRoom.options[SelectPlayerInRoom.value].text);

		CSVR_GameSetting.selectMaxSkillValue = SelectmaxSkillInRoom.value;
		SelectmaxSkillInRoomQuick.value = CSVR_GameSetting.selectMaxSkillValue;
		CSVR_GameSetting.maxSkill = int.Parse(SelectmaxSkillInRoom.options[SelectmaxSkillInRoom.value].text);

		CSVR_GameSetting.selectTimeMatchValue = SelectTimeMatchInRoom.value;
		SelectTimeMatchInRoomQuick.value = CSVR_GameSetting.selectTimeMatchValue;
		CSVR_GameSetting.timeMatch = 60*int.Parse(SelectTimeMatchInRoom.options[SelectTimeMatchInRoom.value].text);

		
		CSVR_MPConnection.instance.OnInfoChanged();
		*/
	}
	public void OnRoomInfoChanged(bool master)
	{
		if (master) {
			startGameButton.gameObject.SetActive(true);
		} else {
			startGameButton.gameObject.SetActive(false);
		}
		mapImage.sprite = HomeScreen.instance.modeMapSprites[HomeScreen.instance.modeMapSpritesIndex[CSVR_GameSetting.mapName]];
		modeName.text = HomeScreen.instance.lobbyResult.ModeAvaiable[CSVR_GameSetting.modeName];
		mapName.text = HomeScreen.instance.lobbyResult.MapAvaiable[CSVR_GameSetting.mapName];
		maxPlayer.text = CSVR_GameSetting.maxPlayer.ToString();
		maxKill.text = CSVR_GameSetting.maxSkill.ToString();
		maxTime.text = CSVR_GameSetting.timeMatch.ToString ();
	}

	/*
	public void OnRoomInfoChanged(bool master,int valuePlayMode,int valueModeGame,int valueMapGame,int valuePlayer,int valueMaxSkill,int valueTimeMatch,int valueTimeRepawn)
	{
//		string debug = string.Format("valuePlayMode: {0}. valueMapGame: {1}. valuePlayer: {2}. valueMaxSkill: {3}. valueTimeMatch: {4}. valueTimeRepawn: {5}", valuePlayMode, valueMapGame, valuePlayer, valueMaxSkill, valueTimeMatch, valueTimeRepawn);
//		Debug.Log (debug);

		if (master) {
//			SelectPlayModeInRoom.interactable = true;
//			SelectModeGameInRoom.interactable = true;
//			SelectMapGameInRoom.interactable = true;
//			SelectPlayerInRoom.interactable = true;
//			SelectmaxSkillInRoom.interactable = true;
//			SelectTimeMatchInRoom.interactable = true;

//			changeInfoButton.interactable = true;
			startGameButton.gameObject.SetActive(true);
		} else {
//			SelectPlayModeInRoom.interactable = false;
//			SelectModeGameInRoom.interactable = false;
//			SelectMapGameInRoom.interactable = false;
//			SelectPlayerInRoom.interactable = false;
//			SelectmaxSkillInRoom.interactable = false;
//			SelectTimeMatchInRoom.interactable = false;

//			changeInfoButton.interactable = false;
			startGameButton.gameObject.SetActive(false);
		}
			
		CSVR_GameSetting.selectModeGameValue 	= SelectModeGameInRoomQuick.value 	= valueModeGame;
		//CSVR_GameSetting.modeGame 	= SelectModeGameInRoom.options[SelectModeGameInRoom.value].text;

		CSVR_GameSetting.selectMapGameValue 	= SelectMapGameInRoomQuick.value 	= valueMapGame;
		//CSVR_GameSetting.roomName 	= SelectMapGameInRoom.options[SelectMapGameInRoom.value].text;

		CSVR_GameSetting.selectPlayerValue 		= SelectPlayerInRoomQuick.value 	= valuePlayer;
		CSVR_GameSetting.maxPlayer 	= int.Parse(SelectPlayerInRoom.options[SelectPlayerInRoom.value].text);

		CSVR_GameSetting.selectMaxSkillValue 	= SelectmaxSkillInRoomQuick.value 	= valueMaxSkill;
		CSVR_GameSetting.maxSkill 	= int.Parse(SelectmaxSkillInRoom.options[SelectmaxSkillInRoom.value].text);

		CSVR_GameSetting.selectTimeMatchValue 	= SelectTimeMatchInRoomQuick.value 	= valueTimeMatch;
		CSVR_GameSetting.timeMatch 	=  60*int.Parse(SelectTimeMatchInRoom.options[SelectTimeMatchInRoom.value].text);
	}
	*/
	void CheckMyPlayerProperties(string playerName, TeamTypes playerTeam)
	{
		
		if (selectedTeam != playerTeam)
		{
			this.ChangeTeam(false);
		}

	}

	public void UI_OnExitClick()
	{
		inviteFriends.gameObject.SetActive(false);
	}
	public void UI_OnInviteFriendOn()
	{
		inviteFriends.gameObject.SetActive(true);
	}
	public void UI_OnInviteFriendOff()
	{
		inviteFriends.gameObject.SetActive(false);
	}

	public void UI_OnReadyClick(){
		HomeScreen.instance.SetLoadingMapCallBack (UI_LoadingMapCallBack);
		CSVR_MPConnection.instance.SetReady();
	}
	private void UI_LoadingMapCallBack(){
		this.Close ();
		TopBar.instance.Close ();
	}
}