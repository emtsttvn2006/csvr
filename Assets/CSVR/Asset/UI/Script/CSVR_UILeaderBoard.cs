﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;

public class CSVR_UILeaderBoard : SingletonBaseScreen <CSVR_UILeaderBoard>
{
	public GameObject playerPanel;
	public Sprite[] playerPanelBackground;

	public GameObject topLevelContainer;
	public GameObject topKillContainer;
	public GameObject topClanContainer;
	private GameObject topTempContainer;

	void Start(){
		ErrorView.instance.Loading (true);
		GetTopStatisticName (CSVR_GameSetting.accountLevelKey);
		GetTopStatisticName (CSVR_GameSetting.accountKillKey);
	}

	void GetTopStatisticName(string statisticName)
	{
		GetLeaderBoardRequest request = new GetLeaderBoardRequest ();
		request.order = statisticName;
		//request.by = "asc";
		request.by = "desc";
		request.count = 10;
		HorusClientAPI.GetLeaderboard (request, GetTopStatisticNameSuccess, GetTopError, statisticName);
	}

	void GetTopStatisticNameSuccess( GetLeaderBoardResult result)
	{
		Debug.Log (result.UserList);
		int lenght = result.UserList.Count;

		switch (result.CustomData.ToString ()) {
		case "AccountLevel":
			topTempContainer = topLevelContainer;
			for (int i = 0; i < lenght; i++) 
			{
				GameObject obj = Instantiate(playerPanel) as GameObject;
				if(i <= 2){ 
					obj.transform.GetComponent<Image>().sprite = playerPanelBackground[i];
				}
				obj.transform.GetChild(0).GetComponent<Text>().text = (i+1).ToString();
				obj.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = result.UserList[i].status;
				try{
					obj.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = result.UserList[i].AccountLevel.ToString();

				}catch{
					obj.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "0";
				}
				obj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = result.UserList[i].DisplayName.ToString();


				obj.gameObject.transform.SetParent(topTempContainer.transform, false);
			}
			break;
		case "AccountKill":
			topTempContainer = topKillContainer;
			for (int i = 0; i < lenght; i++) 
			{
				GameObject obj = Instantiate(playerPanel) as GameObject;
				if(i <= 2){ 
					obj.transform.GetComponent<Image>().sprite = playerPanelBackground[i];
				}

				obj.transform.GetChild(0).GetComponent<Text>().text = (i+1).ToString();
				obj.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = result.UserList[i].status;
				try{
					obj.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = result.UserList[i].AccountKill.ToString();
				}catch{
					obj.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "0";
				}
				obj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Text>().text = result.UserList[i].DisplayName.ToString();


				obj.gameObject.transform.SetParent(topTempContainer.transform, false);
			}
			break;
		}


		ErrorView.instance.Loading (false);
	}

	void GetTopError(HorusError error)
	{

	}

	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
}