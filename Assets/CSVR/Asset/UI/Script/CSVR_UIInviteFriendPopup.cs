﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UIInviteFriendPopup : MonoBehaviour {
	public static CSVR_UIInviteFriendPopup instance = null;
	public Text mess;
	private string roomNameJoin;
	private void Awake () {
		instance = this;
	}
//    private void Start()
//    {
//        CSVR_UIMultiplayerLobby.instance.Tester();
//    }
	public void UI_OnShowPopup(string message,string room){
		Debug.Log ("UI_OnClickInviteFriend UI_OnShowPopup" + message+" room "+room);
		mess.text = message;
		roomNameJoin = room;
	}

	public void UI_OnJoinRoomClick(){
		if (!string.IsNullOrEmpty(roomNameJoin))
		{
			PhotonNetwork.JoinRoom(roomNameJoin);
		}
	}

	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
	public void UI_OnDisable(){
		this.gameObject.SetActive (false);
	}
}
