﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonView))]

public class CSVR_UIChatInGame : Photon.MonoBehaviour {
	[SerializeField]
	private Toggle team;

	[SerializeField]
	private GameObject grid;

	[SerializeField]
	private GameObject item;

	private string name;
	private int teamIndex;

	// Use this for initialization
	void Start () {
		name = PhotonNetwork.player.customProperties ["PlayFabName"].ToString ();
		teamIndex = int.Parse(PhotonNetwork.player.customProperties ["Team"].ToString ());
	}

	[PunRPC]
	public void Recive_Chat(string mess,bool messSender,int teamSender){
		if (grid.transform.childCount > 3) {
			Destroy(grid.transform.GetChild(0).gameObject);
		}

		if (messSender) {//chat all
			GameObject obj = Instantiate (item);
			Text txt = obj.transform.GetChild(0).GetComponent<Text>();
			txt.text = mess;
			obj.transform.SetParent(grid.transform,false);
			obj.transform.localScale = Vector3.one;
			Destroy(obj,10f);
		} else {
			if(teamSender == teamIndex){//chat team
				GameObject obj = Instantiate (item);
				Text txt = obj.transform.GetChild(0).GetComponent<Text>();
				txt.text = mess;
				obj.transform.SetParent( grid.transform,false);
				obj.transform.localScale = Vector3.one;
				txt.color = Color.blue;
				Destroy(obj,10f);
			}
		}
	}

	public void ChatSendMessage(string mess){
		photonView.RPC("Recive_Chat", PhotonTargets.All,"["+name+"]: "+ mess, team.isOn, teamIndex);
	}
}
