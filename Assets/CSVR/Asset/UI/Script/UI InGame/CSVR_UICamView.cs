﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UICamView : MonoBehaviour {
	public Text HP;
	public Text AC;
	public Text Name;
	public Text Level;
	public Image Weapon;
	private string _cacheWeapon;
	private Sprite _cacheSprite;
	public void SetPlayerInfo(int hp, string ac,string name,string level,string weapon){
		if (_cacheWeapon != weapon) {
			_cacheWeapon = weapon;
			_cacheSprite = Resources.Load<Sprite> ("Sprites/UIWeapon/" + _cacheWeapon + "_icon");
		}
		if (hp < 0) {
			HP.text = "Đang ngủ";
		} else {
			HP.text = "Máu "+ hp;
		}

		AC.text = "Giáp "+ac;
		Name.text = name;
		Level.text = "Cấp độ "+level;
		if (_cacheSprite != null) {
			Weapon.enabled = true;
		} else {
			Weapon.enabled = false;
		}
		Weapon.sprite = _cacheSprite;
	}
	public void ChangeWeaponInfo(Sprite weapon){
		Weapon.sprite = weapon;
	}
}
