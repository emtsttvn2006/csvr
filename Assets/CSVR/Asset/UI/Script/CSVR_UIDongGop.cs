﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;
using MarchingBytes;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class CSVR_UIDongGop : SingletonBaseScreen<CSVR_UIDongGop> {

    public Toggle ToggleKimCuong;
    public Toggle ToggleVang;
    public InputField INPNoidung;
    public InputField INPSoluong;

    public void ResetToggle()
    {
        INPSoluong.text = "";

        if (ToggleKimCuong.isOn)
        {
            INPNoidung.text = "HÃY GÓP THẬT NHIỀU KIM CƯƠNG!! CLAN SẼ CÓ NHỮNG LỢI ÍCH: ....!!";
        } else if (ToggleVang.isOn)
        {
            INPNoidung.text = "HÃY GÓP THẬT NHIỀU VÀNG!! CLAN SẼ CÓ NHỮNG LỢI ÍCH: ....!!";
        }
    }

    public void Toggle_KimCuong_ValueChanged()
    {
        ResetToggle();
    }

    public void Toggle_Vang_ValueChanged()
    {
        ResetToggle();
    }

    public void Button_DongGop_Click()
    {
        ErrorView.instance.ShowPopupError("TÍNH NĂNG NÀY ĐANG PHÁT TRIỂN!!");
        /*
        ClanDonateRequest ClanDonateRequest = new ClanDonateRequest();
        
        if (ToggleKimCuong.isOn)
        {
            ClanDonateRequest.itemId = "01_02_gold";
        } else if (ToggleVang.isOn)
        {
            ClanDonateRequest.itemId = "01_01_coin";
        }

        ClanDonateRequest.number = int.Parse(INPSoluong.text);

        HorusClientAPI.ClanDonateProcess(ClanDonateRequest,
            (result) =>
            {
                ErrorView.instance.ShowPopupError("Tốt!! Tiếp tục đóng góp thêm nhé!!");
                Close();
            },
            (error) =>
            {
                ErrorView.instance.ShowPopupError("Đóng góp Clan Lỗi");
            }, null);*/
    }
}
