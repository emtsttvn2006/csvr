﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIMouseClick : SingletonBaseScreen<CSVR_UIMouseClick> {
    
    public bool SetOnOff = false;
    public Vector3 v3MoveTo;
    public RectTransform ImageHolder;
    public Image Image;
    public Animator Animator;
    public RectTransform rectTransform;
    [HideInInspector] public Vector2 v2Size = Vector2.zero;

    public override void Awake()
    {
        base.Awake();
        this.gameObject.SetActive(true);

        this.v2Size = HomeScreen.instance.v2Size;

        rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
        rectTransform.sizeDelta = Vector2.zero;
    }

    public override void Open()
    {
        base.Open();

        this.gameObject.SetActive(true);

        this.v2Size = HomeScreen.instance.v2Size;

        rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
        rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
        rectTransform.sizeDelta = Vector2.zero;
    }    

    public void DelayMoveTo()
    {
        Invoke("MoveTo", 0.03f);
    }

    public void MoveTo()
    {        
        ImageHolder.localPosition = new Vector3(v2Size.x * (v3MoveTo.x - 0.5f), v2Size.y * (v3MoveTo.y - 0.5f), 0);
    }

    public void SetAlpha(string Set)
    {
        Animator.SetTrigger(Set);
    }

    public void DelayOn()
    {
        SetOnOff = true;
        Invoke("ImageOnOff", 0.1f);
    }

    public void DelayOff()
    {
        SetOnOff = false;
        Invoke("ImageOnOff", Random.Range(0.7f , 1.5f));
    }    

    public void ImageOnOff()
    {
        Image.enabled = SetOnOff;        
    }
}
