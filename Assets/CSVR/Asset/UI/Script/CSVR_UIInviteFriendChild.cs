﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UIInviteFriendChild : MonoBehaviour {

	public Image avatarPlayer;
	public Text namePlayer;
	public Text levelPlayer;
	public Image levelIconPlayer;
	//public Text statusPlayer;
	public Button invitePlayer;
	string friendName;

	public void GetInfo(FriendInfo info)
	{

		string nameFriend ="";
		for(int i =0 ; i< CSVR_GameSetting.friends.Count;i++)
		{
			if(info.Name == CSVR_GameSetting.friends[i].FriendPlayFabId){
				nameFriend = CSVR_GameSetting.friends[i].TitleDisplayName;
				
				break;
			}
			else
				nameFriend  = "";
		}
		namePlayer.text = nameFriend ;
		
		if(CSVR_GameSetting.friendInfos.ContainsKey(info.Name)){
			levelPlayer.text = "Cấp độ: "+CSVR_GameSetting.friendInfos[info.Name].level;
		}

//		if (statusPlayer != null) {
//			statusPlayer.text = (info.IsOnline) ? "Online" : "Offline"; 
//			statusPlayer.color = (info.IsOnline) ? new Color(0, 0.9f, 0, 0.9f) : new Color(0.9f, 0, 0, 0.9f);
//			
//		}

		//invitePlayer.interactable = ((info.IsInRoom) ? true : false);
		friendName = info.Name;
		
	}

	public void UI_OnClickInviteFriend(){
		if (!string.IsNullOrEmpty(friendName))
		{
			Debug.Log ("UI_OnClickInviteFriend " + friendName);

			//CSVR_UIChatManager.instance.InviteToJoinRoom(friendName);

			//Su dung he thong chat moi
			//Phai them chu u vao dau tien
			CSVR_IRCPlayerMessenger.instance.SendMessage (friendName, "roominvitation,"+PhotonNetwork.room.name);
		}
	}
}
