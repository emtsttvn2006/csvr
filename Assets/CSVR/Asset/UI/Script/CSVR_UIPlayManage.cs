﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class CSVR_UIPlayManage : MonoBehaviour
{
    public static CSVR_UIPlayManage init = null;
	public GameObject InitBadge,DeadInfo,InfoKiller,HeadShot,HeadShotRewardDay,KnifeShot, HeadShotFull, HeadWallShot, HeadWallShotFull, Star1, Star2, Star3, Star4, Star5, Star6, WallShot, GrenadeShot,ListPlayerDead,ListKillMark,ArrowHUD;
    public List<GameObject> IconBadge;
    public List<GameObject> IconBadge5;
    public int CountGold, CountBadge,Count10Badge;
    public Button icon, BackpackIcon;
    public Sprite[] listIconBackpack;
    public Text nameWeapon;
	public GameObject[] mission;    
    public Text txtNameWeapon, txtNamePlayer, txtLevel;
	public Image imgPlayerAvatar,imgPlayerLevel;
    public Image  imgIconWeapon;
	public GameObject[] victims;

    // Use this for initialization
    void Start()
    {
		Resources.UnloadUnusedAssets ();
        init = this;
        IconBadge = new List<GameObject>();
        IconBadge5 = new List<GameObject>();
        CountGold = 0;
        CountBadge = 0;
        
    }

	Sprite PlessWaitCheckAvatar(string PlayfabName)
	{
		PhotonPlayer[] players = PhotonNetwork.playerList;
		
		for (int i = 0; i < players.Length; i++)
		{
			if (string.Compare(players[i].name, PlayfabName) == 0)
			{
				if (players[i].customProperties["Level"].ToString()!=null){
					byte[] pngBytes = players[i].customProperties["Avatar"] as byte[];
					Texture2D tex = new Texture2D(128, 128);
					tex.LoadImage(pngBytes);

					Sprite avatar = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2());
					return avatar;
					break;
				}
			}
		}
		return null;
	}
    public void ShowInfoKiller(string nameWeapon, string playerName, string level, Sprite iconWeapon, int idBadge)
    {
        txtNameWeapon.text = nameWeapon;
        txtNamePlayer.text = playerName;
		txtLevel.text = "Cấp độ: "+level;
		imgPlayerLevel.sprite = Resources.Load<Sprite>("Sprites/UILevel/"+level);
		imgPlayerAvatar.sprite = PlessWaitCheckAvatar (playerName);
        imgIconWeapon.sprite = iconWeapon;
		if(vp_MPMaster.Instance.Victims.Count > 5){
			vp_MPMaster.Instance.Victims.RemoveRange(0, vp_MPMaster.Instance.Victims.Count - 5);
		}


		int lenght = vp_MPMaster.Instance.Victims.Count;
		for (int i = 4; i >= 0; i--) {
			if(lenght - i >= 1)
			{
				victims[i].transform.gameObject.SetActive(true);
				victims[i].transform.GetChild(0).GetComponent<Text>().text = vp_MPMaster.Instance.Victims[i].Name;
				victims[i].transform.GetChild(1).GetComponent<Text>().text = "Cấp: "+ vp_MPMaster.Instance.Victims[i].Level;
				victims[i].transform.GetChild(2).GetComponent<Text>().text = vp_MPMaster.Instance.Victims[i].Heath;
			}
			else
			{
				victims[i].transform.gameObject.SetActive(false);
			}

		}
		vp_MPMaster.Instance.Victims.Clear ();
    }
    public void InitPlayerKill(string str1, string str2, Sprite sp1, int Hitpart, int idTeam, int victimTeams)
    {        
        GameObject obj = GameObject.Instantiate(DeadInfo, transform.position, transform.rotation) as GameObject;
        CSVR_ShowPlaySkill PlayKillInit = obj.GetComponent<CSVR_ShowPlaySkill>();
        PlayKillInit.strPlayer1 = str1;
        PlayKillInit.strPlayer2 = str2;
        if (idTeam == 1)
        {
            PlayKillInit.txtPlayer1.color = new Color(0, 0.652f, 1, 1);
        }
        else if (idTeam == 2)
        {
            PlayKillInit.txtPlayer1.color = new Color(1, 0, 0, 1);
        }
        if (victimTeams == 1)
        {
            PlayKillInit.txtPlayer2.color = new Color(0, 0.652f, 1, 1);
        }
        else if (victimTeams == 2)
        {
            PlayKillInit.txtPlayer2.color = new Color(1, 0, 0, 1);
        }
        PlayKillInit.spr1 = sp1;
        if (Hitpart == 0)
        {
            PlayKillInit.img2.gameObject.SetActive(true);
        }
        PlayKillInit.setInfo();
        obj.transform.SetParent(ListPlayerDead.transform, false);
        obj.transform.localScale = Vector3.one;
    }
    
    public void InitWeaponIcon()
    {
        if (!PhotonNetwork.player.isLocal) return;
        icon.gameObject.SetActive(true);
    }
    public void InitBackpackIcon()
    {
        if (!PhotonNetwork.player.isLocal) return;
        BackpackIcon.gameObject.SetActive(true);
    }
    public void InitGrenade()
    {
        if (!PhotonNetwork.player.isLocal) return;
        GameObject obj1 = GameObject.Instantiate(GrenadeShot) as GameObject;
        obj1.transform.SetParent(InitBadge.transform, false);
        obj1.transform.localPosition = Vector3.zero;
        obj1.transform.localScale = Vector3.one;
    }
    public void InitKnifeShot()
    {
        if (!PhotonNetwork.player.isLocal) return;
        GameObject obj1 = GameObject.Instantiate(KnifeShot) as GameObject;
        obj1.transform.SetParent(InitBadge.transform, false);
        obj1.transform.localPosition = Vector3.zero;
        obj1.transform.localScale = Vector3.one;
    }
    public void InitHeadShot()
    {
        if (!PhotonNetwork.player.isLocal) return;
        GameObject obj = GameObject.Instantiate(HeadShot) as GameObject;
        obj.transform.SetParent(InitBadge.transform,false);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;

		GameObject objReward = GameObject.Instantiate(HeadShotRewardDay) as GameObject;
		objReward.transform.SetParent(InitBadge.transform,false);
		objReward.transform.localPosition = Vector3.zero;
		objReward.transform.localScale = Vector3.one;
    }
    public void InitHeadShotFull()
    {
        if (!PhotonNetwork.player.isLocal) return;
        GameObject obj = GameObject.Instantiate(HeadShotFull) as GameObject;
        obj.transform.SetParent(InitBadge.transform,false);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;


		GameObject objReward = GameObject.Instantiate(HeadShotRewardDay) as GameObject;
		objReward.transform.SetParent(InitBadge.transform,false);
		objReward.transform.localPosition = Vector3.zero;
		objReward.transform.localScale = Vector3.one;

    }
    public void InitHeadWallShot()
    {
        if (!PhotonNetwork.player.isLocal) return;
        GameObject obj = GameObject.Instantiate(HeadWallShot) as GameObject;
        obj.transform.SetParent(InitBadge.transform,false);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;

    }
    public void InitHeadWallShotFull()
    {
        if (!PhotonNetwork.player.isLocal) return;
        GameObject obj = GameObject.Instantiate(HeadWallShotFull) as GameObject;
        obj.transform.SetParent(InitBadge.transform,false);
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;
    }
    public void InitStar(int id)
    {
        if (!PhotonNetwork.player.isLocal) return;
        switch (id)
        {
            case 1:
                GameObject obj1 = GameObject.Instantiate(Star1) as GameObject;
                obj1.transform.SetParent(InitBadge.transform,false);
                obj1.transform.localPosition = Vector3.zero;
                obj1.transform.localScale = Vector3.one;
                //obj1.transform.position = new Vector3(0,-320,0);
                break;
            case 2:
                GameObject obj2 = GameObject.Instantiate(Star2) as GameObject;
                obj2.transform.SetParent(InitBadge.transform,false);
                obj2.transform.localPosition = Vector3.zero;
                obj2.transform.localScale = Vector3.one;
                break;
            case 3:
                GameObject obj3 = GameObject.Instantiate(Star3) as GameObject;
                obj3.transform.SetParent(InitBadge.transform, false);
                obj3.transform.localPosition = Vector3.zero;
                obj3.transform.localScale = Vector3.one;
                break;
            case 4:
                GameObject obj4 = GameObject.Instantiate(Star4) as GameObject;
                obj4.transform.SetParent(InitBadge.transform, false);
                obj4.transform.localPosition = Vector3.zero;
                obj4.transform.localScale = Vector3.one;
                break;
            case 5:
                GameObject obj5 = GameObject.Instantiate(Star5) as GameObject;
                obj5.transform.SetParent(InitBadge.transform, false);
                obj5.transform.localPosition = Vector3.zero;
                obj5.transform.localScale = Vector3.one;
                break;
            case 6:
                GameObject obj6 = GameObject.Instantiate(Star6) as GameObject;
                obj6.transform.SetParent(InitBadge.transform, false);
                obj6.transform.localPosition = Vector3.zero;
                obj6.transform.localScale = Vector3.one;
                break;
            default:
                break;
        }

    }
    public void MissionSuccess()
    {
		mission [0].gameObject.SetActive (true);
    }
    public void MissionFaile()
    {
		mission [1].gameObject.SetActive (true);
    }
	public void DisableMission()
	{
		mission [0].gameObject.SetActive (false);
		mission [1].gameObject.SetActive (false);
	}
}
