﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CSVR_EffectIcon : MonoBehaviour {
    public GameObject HHSum,HH5,HH10;
    public AudioClip SoundInit;
    public AudioClip[] sound;
    int _random;
    public AudioSource audioSource, audioSource1;
	// Use this for initialization
	void Start () {
        _random = Random.Range(0, 5);
        if (CSVR_UIPlayManage.init.CountGold < 5)
        {
            CSVR_UIPlayManage.init.CountGold++;
        }
        audioSource = transform.GetComponent<AudioSource>();
        audioSource1 = gameObject.AddComponent<AudioSource>();
        audioSource.clip = SoundInit;
        audioSource.Play();
        audioSource1.clip = sound[_random];
        audioSource1.Play();
        StartCoroutine("InitSao");
	}
    IEnumerator InitSao()
    {
        yield return new WaitForSeconds(1.04f);
        if (CSVR_UIPlayManage.init.CountGold < 5 && CSVR_UIPlayManage.init.CountBadge != 2)
        {
            GameObject obj = GameObject.Instantiate(HHSum, transform.position, transform.rotation) as GameObject;
            obj.transform.SetParent(CSVR_UIPlayManage.init.ListKillMark.transform);
            obj.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
            CSVR_UIPlayManage.init.IconBadge.Add(obj);
            //if (CSVR_UIPlayManage.init.CountGold > 0)
            //{
            //    CSVR_UIPlayManage.init.ListBadge[CSVR_UIPlayManage.init.CountGold - 1] = obj;
            //}
        }
        else if (CSVR_UIPlayManage.init.CountGold == 5 && CSVR_UIPlayManage.init.CountBadge < 2)
        {
            CSVR_UIPlayManage.init.CountBadge++;
            if (CSVR_UIPlayManage.init.CountBadge < 2)
            {
                GameObject obj = GameObject.Instantiate(HH5, transform.position, transform.rotation) as GameObject;
                obj.transform.SetParent(CSVR_UIPlayManage.init.ListKillMark.transform);
                obj.transform.localScale = Vector3.one;
                for (int i = 0; i<CSVR_UIPlayManage.init.IconBadge.Count; i++)
                {
                    if (CSVR_UIPlayManage.init.IconBadge[i] != null)
                    {
                        Destroy(CSVR_UIPlayManage.init.IconBadge[i]);
                    }
                }
                CSVR_UIPlayManage.init.IconBadge.Clear();
                CSVR_UIPlayManage.init.IconBadge5.Add(obj);
                CSVR_UIPlayManage.init.CountGold = 0;
            }
            else
            {
                CSVR_UIPlayManage.init.Count10Badge++;
                GameObject obj = GameObject.Instantiate(HH10) as GameObject;
                obj.transform.SetParent(CSVR_UIPlayManage.init.ListKillMark.transform);
                obj.transform.localScale = Vector3.one;
                for (int i = 0; i < CSVR_UIPlayManage.init.IconBadge.Count; i++)
                {
                    if (CSVR_UIPlayManage.init.IconBadge[i] != null)
                    {
                        Destroy(CSVR_UIPlayManage.init.IconBadge[i]);
                    }
                }
                for (int j = 0; j < CSVR_UIPlayManage.init.IconBadge5.Count; j++)
                {
                    if (CSVR_UIPlayManage.init.IconBadge5[j] != null)
                    {
                        Destroy(CSVR_UIPlayManage.init.IconBadge5[j]);
                    }
                }
                CSVR_UIPlayManage.init.IconBadge.Clear();
                CSVR_UIPlayManage.init.IconBadge5.Clear();
                CSVR_UIPlayManage.init.CountGold = 0;
                CSVR_UIPlayManage.init.CountBadge = 0;
            }

        }
        Destroy(gameObject);
    }
}
