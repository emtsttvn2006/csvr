﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class TopBar_BackButton : MonoBehaviour, IPointerClickHandler
{
    public List<Transform> Layers;

    int tap;

    public void Awake()
    {
        Layers = new List<Transform>();
        Transform Canvas = GameObject.Find("Canvas").transform;
        
        int LayerID = 0;        
        bool boo = Canvas.FindChild("Layer" + LayerID.ToString());

        while (boo)
        {
            Layers.Add(Canvas.FindChild("Layer" + LayerID.ToString()));
            LayerID++;
            boo = Canvas.FindChild("Layer" + LayerID.ToString());
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        tap = eventData.clickCount;

        if (tap == 2)
        {
            foreach (Transform tr in Layers)
            {
                foreach(Transform child in tr)
                {
                    if (child.name != "TopBar")
                        DestroyImmediate(child.gameObject);
                }
            }

            TopBar.instance.Open();
            MainView.instance.Open();
            CharacterView.instance.Open();
        }

    }
}