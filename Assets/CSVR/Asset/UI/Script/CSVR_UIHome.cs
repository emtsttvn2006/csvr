using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using PlayFab.ClientModels;

public class CSVR_UIHome : MonoBehaviour {
	public static CSVR_UIHome instance;

	public Text UI_CurrencyGCTxt;
	public Text UI_CurrencyGDTxt;
	public Sprite[] listPingIcon;
	public Image UI_PingImg;
	public Text UI_PingTxt;

	public Text UI_NewTxt;
	void Awake()
	{
		instance = this;
	}

	void Update () {
		
		if(CSVR_GameSetting.virtualCurrency != null)
		if(CSVR_GameSetting.virtualCurrency.ContainsKey("GC") && CSVR_GameSetting.virtualCurrency.ContainsKey("GD")){
			UI_CurrencyGCTxt.text = CSVR_GameSetting.virtualCurrency ["GC"].ToString ();
			UI_CurrencyGDTxt.text = CSVR_GameSetting.virtualCurrency ["GD"].ToString ();
			if (PhotonNetwork.GetPing() < 130)
			{
				UI_PingTxt.color = Color.green;
				UI_PingImg.sprite = listPingIcon[0];
			}
			else if (PhotonNetwork.GetPing() > 230)
			{
				UI_PingTxt.color = Color.red;
				UI_PingImg.sprite = listPingIcon[2];
			}
			else
			{
				UI_PingTxt.color = Color.yellow;
				UI_PingImg.sprite = listPingIcon[1];
			}
			UI_PingTxt.text = PhotonNetwork.GetPing() + "ms";
		}
	}
}
