﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIListButton_Button_Callback : MonoBehaviour
{
    public Text TextTittle;
    public Button Button;

    void ScrollCellIndex(int idx)
    {
        TextTittle.text = CSVR_UIListButton.instance.listButtons[idx].tittle;
        this.Button.onClick.RemoveAllListeners();
        this.Button.onClick.AddListener(CSVR_UIListButton.instance.listButtons[idx].acion);
    }    
}
