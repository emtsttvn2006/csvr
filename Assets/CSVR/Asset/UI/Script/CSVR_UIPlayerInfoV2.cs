﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIPlayerInfoV2 : MonoBehaviour {

	public Text levelValueText = null;
	public Text nameValueText = null;
	public Image expValueBarFill = null;
	public Text startDayValueText = null;
	public Text totalTimeValueText = null;
	public Text killValueText = null;
	public Text deadValueText = null;
	public Text headshotValueText = null;
	public Text revengeValueText = null;
	
	public void HienThi()
	{
		int lastLevelExp = CSVR_GameSetting.experienceForEveryLevel[CSVR_GameSetting.accountLevel];
		int nextLevelExp = CSVR_GameSetting.experienceForEveryLevel[CSVR_GameSetting.accountLevel+1];
		int actualExp = CSVR_GameSetting.accountExp;
		
		nameValueText.text = AccountManager.instance.displayName;
		levelValueText.text = "Level: " + (CSVR_GameSetting.accountLevel).ToString();
		
		float fillAmount = (float)(actualExp - lastLevelExp) / (float)(nextLevelExp - lastLevelExp);
		
		expValueBarFill.fillAmount = fillAmount;
		startDayValueText.text = CSVR_GameSetting.accountStartDay;
		killValueText.text = CSVR_GameSetting.accountKill.ToString();
		deadValueText.text = CSVR_GameSetting.accountDead.ToString();
		headshotValueText.text = CSVR_GameSetting.accountHeadShot.ToString();
		revengeValueText.text = CSVR_GameSetting.accountRevenge.ToString();
	}
	public void UI_OnInfoOn()
	{
		this.gameObject.SetActive(true);
	}
	public void UI_OnInfoOff()
	{
		this.gameObject.SetActive(false);
	}
}
