﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;
using MarchingBytes;

public class CSVR_UIGift : SingletonBaseScreen<CSVR_UIGift>
{
	[SerializeField] private InputField inputGiftCode;
	[SerializeField] private Toggle Tog_QuaDiemDanh;
	[SerializeField] private List<Button> Btn_LuongMoiNgay;
	[SerializeField] private List<CSVR_Gift_Item_Moi_Ngay_Callback> itemReward;

	[SerializeField] private LoopVerticalScrollRect Qua7Day;
	[SerializeField] private LoopVerticalScrollRect Qua30Day;
	[SerializeField] private LoopVerticalScrollRect QuaLevelUp;

	public  List<QuestInfo> questList7Day;
	public  List<QuestInfo> questList30Day;
	public  List<QuestInfo> questListLevelUp;

	void Start(){
		questList7Day = new List<QuestInfo> ();
		questList30Day = new List<QuestInfo> ();
		questListLevelUp = new List<QuestInfo> ();
		if (!Qua7Day.prefabPool) Qua7Day.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
		if (!Qua30Day.prefabPool) Qua30Day.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
		if (!QuaLevelUp.prefabPool) Qua30Day.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();

		Tog_QuaDiemDanh.onValueChanged.AddListener ((value) => {
			if (value) {
				Tog_QuaDiemDanh.GetComponent<LayoutElement> ().preferredHeight = 275;
			} else {
				Tog_QuaDiemDanh.GetComponent<LayoutElement> ().preferredHeight = 75;
			}
		});
		SCR_Qua7Ngay_Run ();
		SCR_Qua30Ngay_Run ();
		SCR_QuaLevelUp_Run ();
	}

	public void HUD_RedeemReferralCodeClear_Click()
	{	
		this.inputGiftCode.text = string.Empty;

	}
	public void HUD_RedeemReferralCode_Click()
	{	
		HorusClientAPI.RedeemCoupon(new GetRedeemCouponRequest(){
			code = this.inputGiftCode.text ,
			useItem = "1"
		}, 
			(result) => {
				this.inputGiftCode.text = string.Empty;
				if(result.Inventory.Count > 1){
					CSVR_UIRewardItem.instance.Open();
					CSVR_UIRewardItem.instance.ResetListItemReward();
				}
				for (int i = 0; i < result.Inventory.Count; i++) {

					if(result.Inventory [i].CatalogVersion != "BundleGiftCode" && (int)result.Inventory [i].RemainingUses != 0){
						CSVR_UIRewardItem.instance.RewardItem(i,result.Inventory [i]);
					}
				}
				TopBar.instance.UpdateVirtualCurrency();
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Mã code đã được sử dụng, hoặc không đúng định dạng. Xin vui lòng nhập lại");
			}
		);

	}


	public void SCR_Qua7Ngay_Run()
	{	
		HorusClientAPI.GetQuestSerial(new GetQuestSerialRequest(){
			serial = GameConstants.VNQUESTSERIAL_7day 
		}, 
			(result) => {
				questList7Day = result.questList;
				questList7Day.Sort();
				HUD_UpdateQua7Ngay_Run();
			},
			(error) => {
				Yo.JsonLog("error",error);
			}
		);
	}
	public void HUD_UpdateQua7Ngay_Run()
	{
		Qua7Day.totalCount = questList7Day.Count;
		Qua7Day.Rebuild(CanvasUpdate.Layout);
	}


	public void SCR_Qua30Ngay_Run()
	{	
		HorusClientAPI.GetQuestSerial(new GetQuestSerialRequest(){
			serial = GameConstants.VNQUESTSERIAL_30day 
		}, 
			(result) => {
				questList30Day = result.questList;
				questList30Day.Sort();
				HUD_UpdateQua30Ngay_Run();
			},
			(error) => {
				Yo.JsonLog("error",error);
			}
		);
	}
	public void HUD_UpdateQua30Ngay_Run()
	{
		Qua30Day.totalCount = questList30Day.Count;
		Qua30Day.Rebuild(CanvasUpdate.Layout);
	}

	public void SCR_QuaLevelUp_Run()
	{	
		HorusClientAPI.GetQuestSerial(new GetQuestSerialRequest(){
			serial = GameConstants.VNQUESTSERIAL_Level 
		}, 
			(result) => {
				questListLevelUp = result.questList;
				questListLevelUp.Sort();
				HUD_UpdateQuaLevelUp_Run();
			},
			(error) => {
				Yo.JsonLog("error",error);
			}
		);
	}
	public void HUD_UpdateQuaLevelUp_Run()
	{
		QuaLevelUp.totalCount = questListLevelUp.Count;
		QuaLevelUp.Rebuild(CanvasUpdate.Layout);
	}

	public void HUD_TienLuongMoiNgay_Click()
	{	
		HorusClientAPI.GetQuestProcess(new GetQuestProcessRequest(){
			questId = GameConstants.VNQUESTID_Reward 
		}, 
			(result) => {
				if(result.quest.questProcess.Contains(GameConstants.QUEST_FINISH)){
					for (int i = 0; i < result.quest.itemsOwn.Count; i++) {

						if((int)result.quest.itemsOwn [i].RemainingUses != 0){
							RewardItem(i,result.quest.itemsOwn [i]);
						}
					}
					TopBar.instance.UpdateVirtualCurrency();
				}else{
					ErrorView.instance.ShowPopupError("Bạn đã nhận thưởng.");
				}
			},
			(error) => {
				Yo.JsonLog("error",error);
				//Debug.Log(Horus.Json.JsonWrapper.SerializeObject (error, Horus.Internal.HorusUtil.ApiSerializerStrategy));
			}
		);
	}

	public void RewardItem(int i,ItemInstance item){
		itemReward [i].gameObject.SetActive (true);

		itemReward[i].ImageItem.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		itemReward[i].ImageItem.SetNativeSize ();

		if (item.ItemId == GameConstants.VNITEMID_Coin) {
			itemReward[i].ImageItem.transform.localScale = Vector3.one;
			itemReward[i].TextItem.text = ((int)item.RemainingUses - AccountManager.instance.gameCoinAmount) + "(vàng)";
			AccountManager.instance.gameCoinAmount = (int)item.RemainingUses;
		}else if (item.ItemId == GameConstants.VNITEMID_Gold) {
			itemReward[i].ImageItem.transform.localScale = Vector3.one;
			itemReward[i].TextItem.text = ((int)item.RemainingUses - AccountManager.instance.gameDollarAmount) + "(kim cương)";
			AccountManager.instance.gameDollarAmount = (int)item.RemainingUses;
		} else {
			itemReward[i].ImageItem.transform.localScale = new Vector3(0.3f,0.3f,0.3f);

			if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_3Day)) {
				itemReward[i].TextItem.text = item.DisplayName + "(03Ngày)";
			} 
			else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_7Day)) {
				itemReward[i].TextItem.text = item.DisplayName + "(7Ngày)";
			}
			else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_30Day)) {
				itemReward[i].TextItem.text = item.DisplayName + "(30Ngày)";
			} 
			else {
				itemReward[i].TextItem.text = item.DisplayName + "(Vĩnh Viễn)";
			}


			AccountManager.instance.itemsNew.Add (item);
			AccountManager.instance.inventory.Add (item);

			CSVR_UIInventory.instance.InstanceItemPurchase (item);
			CSVR_UIInventory.instance.ShowNotificationPurchase(item,true);
		}
	}
}