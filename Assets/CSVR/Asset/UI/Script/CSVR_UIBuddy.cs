﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using MarchingBytes;
using Horus;
using Horus.ClientModels;
using LitJson;

public class CSVR_UIBuddy : SingletonBaseScreen<CSVR_UIBuddy>
{
    public GameObject ContentHolderBanbe;
    public GameObject ContentHolderWaiting;    
    public GameObject ContentHolderChan;
    public GameObject ContentHolderMoiban;
    public GameObject ContentHolderTop;

    public Toggle ToggleBanbe;
    public Toggle ToggleWaiting;
    public Toggle ToggleChan;
    public Toggle ToggleMoiban;

    public GameObject ContentHolderConfirmUnBlock;

    public InputField INPTimkiem;

    [HideInInspector] public List<RelationshipInfo> listRelationshipInfo = new List<RelationshipInfo>();
    [HideInInspector] public List<RelationshipInfo> listBanBe = new List<RelationshipInfo>();
    [HideInInspector] public List<RelationshipInfo> listWaiting = new List<RelationshipInfo>();
    [HideInInspector] public List<RelationshipInfo> listChan = new List<RelationshipInfo>();
    [HideInInspector] public List<RelationshipInfo> listTemporary = new List<RelationshipInfo>();
    [HideInInspector] public List<FriendInfo> listBanbePhoton = new List<FriendInfo>();    
    
    public LoopVerticalScrollRect scrollBanbe;
    public LoopVerticalScrollRect scrollWaiting;
    public LoopVerticalScrollRect scrollChan;
        
    private int time = 0;

    public override void OnEnable()
    {
        base.OnEnable();

        TopBar.instance.SetBackButton(new UnityAction(() => {
            this.Close();
            AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
        }));

        TopBar.instance.SetShopOrInVButton(new UnityAction(() => {
            this.Close();
            MainView.instance.Close();
            CharacterView.instance.Open();
            CSVR_UIShop.instance.Open();

            AudioManager.instance.audio.PlayOneShot(AudioManager.instance.InvOpen_Clip);
        }));
    }

    public override void Open()
    {
        base.Open();
        InvokeRepeating("TimeCollapse", 0, 1.0f);
        Init();
    }

    void TimeCollapse()
    {
        if (time > 0) time--;
    }    

    public void Init()
    {
        if (scrollBanbe.prefabPool == null) scrollBanbe.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();

        CallApiUpdate();        
    }

    public void UpdateFromPhoton()
    {               
        if (!PhotonNetwork.connected || !PhotonNetwork.insideLobby)
        {            
            CSVR_MPConnection.instance.Connect();            
            ErrorView.instance.ShowPopupError("Đang cố gắng kết nối lại vào đại sảnh!! Hãy quay lại sau vài giây!!");
            return;
        } 

        CSVR_UIWaitingIcon.instance.Open();

        List<string> listToFind = new List<string>();
        for (int i = 0; i < listBanBe.Count; i++)
        {
            listToFind.Add(listBanBe[i].PartnerId);
        }

        bool CanFind = PhotonNetwork.FindFriends(listToFind.ToArray());

        if (CanFind)
        {
            CSVR_UIWaitingIcon.instance.Close();

            if (PhotonNetwork.Friends != null)
            {
                listBanbePhoton = PhotonNetwork.Friends;

                ///WrapFromPhoton
                foreach (RelationshipInfo ri in listBanBe)
                {
                    int index = listBanbePhoton.FindIndex((BanBe) => BanBe.Name == ri.PartnerId);
                    if (index != -1)
                    {
                        listBanBe[index].Online = true;
                    }
                    else
                    {
                        listBanBe[index].Online = false;
                    }
                }                
            } else
            {
                CSVR_UIWaitingIcon.instance.Close();

                ErrorView.instance.ShowPopupError("Không cập nhật được thông tin Online!!Hãy thử lại sau!!");                
            }            
        } else
        {
            CSVR_UIWaitingIcon.instance.Close();

            ErrorView.instance.ShowPopupError("Không cập nhật được thông tin!!Hãy thử lại!!");
        }

        UpdateDanhSachBanBe();
    }
	
    public void ResetToggle()
    {
        ContentHolderTop.gameObject.SetActive(!ToggleMoiban.isOn);
        ContentHolderBanbe.gameObject.SetActive(ToggleBanbe.isOn);
        ContentHolderWaiting.gameObject.SetActive(ToggleWaiting.isOn);
        ContentHolderChan.gameObject.SetActive(ToggleChan.isOn);
        ContentHolderMoiban.gameObject.SetActive(ToggleMoiban.isOn);
    }

    public void CallApiUpdate()
    {
        if (time == 0)
        {
            time = 5;

            CSVR_UIWaitingIcon.instance.Open();

            HorusClientAPI.GetRelationShip(new GetRelationShipRequest(),
                (result) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    this.listRelationshipInfo = result.RelationshipList;

                    UpdateFromPhoton();
                },
                (error) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    if (DebugError) Debug.Log("Loading Danh sach Ban be error");                    
                    scrollBanbe.gameObject.SetActive(false);
                }, null);
        } else
        {
            ErrorView.instance.ShowPopupError("Hãy đợi 5s!!");
        }
    }

    public void ButtonBanbe_ValueChanged()
    {
        ResetToggle();
        if (time == 0 || listRelationshipInfo == null) CallApiUpdate();
    }

    public void ButtonWaiting_ValueChanged()
    {
        ResetToggle();        
        if (time == 0 || listRelationshipInfo == null) CallApiUpdate();
    }

    public void ButtonChan_ValueChanged()
    {
        ResetToggle();
        if (time == 0 || listRelationshipInfo == null) CallApiUpdate();
    }

    public void ButtonMoiban_ValueChanged()
    {
        ResetToggle();
    }

    public void UpdateDanhSachBanBe()
    {
        time = 5;

        listBanBe.Clear();
        listWaiting.Clear();
        listChan.Clear();
        foreach (RelationshipInfo ri in this.listRelationshipInfo)
        {
            if (ri.Status == 2)
            {
                listBanBe.Add(ri);
            } else if (ri.Status == 1)
            {
                listWaiting.Add(ri);
            } else if (ri.Status == -1)
            {
                listChan.Add(ri);
            }
        }

        ClearEmptyDisPlayName(ref listBanBe);
        ClearEmptyDisPlayName(ref listWaiting);
        ClearEmptyDisPlayName(ref listChan);

        if (ToggleBanbe.isOn)
        {
            if (this.listBanBe.Count != 0)
            {
                scrollBanbe.gameObject.SetActive(true);
                scrollBanbe.totalCount = this.listBanBe.Count;
                scrollBanbe.RefillCells();
            }
            else
            {                
                scrollBanbe.gameObject.SetActive(false);                
            }
        } else if (ToggleWaiting.isOn) {
            if (this.listWaiting.Count != 0)
            {
                scrollWaiting.gameObject.SetActive(true);
                scrollWaiting.totalCount = this.listWaiting.Count;
                scrollWaiting.RefillCells();
            }
            else
            {                
                scrollWaiting.gameObject.SetActive(false);                
            }
        } else if (ToggleChan.isOn)
        {
            if (this.listChan.Count != 0)
            {
                scrollChan.gameObject.SetActive(true);
                scrollChan.totalCount = this.listChan.Count;
                scrollChan.RefillCells();
            }
            else
            {                
                scrollChan.gameObject.SetActive(false);             
            }
        }
    }

    public void ClearEmptyDisPlayName(ref List<RelationshipInfo> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].Partner == null || list[i].Partner.DisplayName == "")
            {
                list.RemoveAt(i);
                i--;
            }
        }
    }

    public void Button_XoaINPTimkiem_Click()
    {
        INPTimkiem.text = string.Empty;

        if (ToggleBanbe.isOn)
        {
            listBanBe = listTemporary;
            listTemporary.Clear();
            scrollBanbe.totalCount = this.listBanBe.Count;
            scrollBanbe.RefillCells();
        }
        else if (ToggleWaiting.isOn)
        {
            listWaiting = listTemporary;
            listTemporary.Clear();
            scrollWaiting.totalCount = this.listWaiting.Count;
            scrollWaiting.RefillCells();
        }
        else if (ToggleChan.isOn)
        {
            listChan = listTemporary;
            listTemporary.Clear();
            scrollChan.totalCount = this.listChan.Count;
            scrollChan.RefillCells();
        }
    }
    
    public void Button_Timkiem_Click()
    {
        if (ToggleBanbe.isOn) {
            for (int i = 0; i < listBanBe.Count; i++)
            {
                if (listBanBe[i].Partner.DisplayName.Contains(INPTimkiem.text))
                {
                    listTemporary.Add(listBanBe[i]);
                }
            }

            scrollBanbe.totalCount = this.listBanBe.Count;
            scrollBanbe.RefillCells();
        } else if (ToggleWaiting.isOn)
        {
            for (int i = 0; i < listWaiting.Count; i++)
            {
                if (listWaiting[i].Partner.DisplayName.Contains(INPTimkiem.text))
                {
                    listTemporary.Add(listWaiting[i]);
                }                
            }

            scrollWaiting.totalCount = this.listWaiting.Count;
            scrollWaiting.RefillCells();
        } else if (ToggleChan.isOn)
        {
            for (int i = 0; i < listChan.Count; i++)
            {
                if (listChan[i].Partner.DisplayName.Contains(INPTimkiem.text))
                {
                    listTemporary.Add(listChan[i]);
                }
            }

            scrollChan.totalCount = this.listChan.Count;
            scrollChan.RefillCells();
        }
    }

    public void InviteAFriend(string Name)
    {
        CSVR_UIWaitingIcon.instance.Open();

        UpdateRelationshipRequest UpdateRelationshipRequest = new UpdateRelationshipRequest();
        UpdateRelationshipRequest.PartnerId = Name;
        UpdateRelationshipRequest.Status = 1;

        HorusClientAPI.UpdateRelationship(UpdateRelationshipRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                CallApiUpdate();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                ErrorView.instance.ShowPopupError("Từ chối bạn bè lỗi!!");
            }, null);
    }

    public void Unfriend(string Name)
    {
        CSVR_UIWaitingIcon.instance.Open();

        UpdateRelationshipRequest UpdateRelationshipRequest = new UpdateRelationshipRequest();
        UpdateRelationshipRequest.PartnerId = Name;
        UpdateRelationshipRequest.Status = 0;

        HorusClientAPI.UpdateRelationship(UpdateRelationshipRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                CallApiUpdate();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                ErrorView.instance.ShowPopupError("Từ chối bạn bè lỗi!!");
            }, null);
    }

    public void AgreeFriend(string Name)
    {
        CSVR_UIWaitingIcon.instance.Open();

        UpdateRelationshipRequest UpdateRelationshipRequest = new UpdateRelationshipRequest();
        UpdateRelationshipRequest.PartnerId = Name;
        UpdateRelationshipRequest.Status = 2;

        HorusClientAPI.UpdateRelationship(UpdateRelationshipRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                CallApiUpdate();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                ErrorView.instance.ShowPopupError("Kết bạn lỗi!!");
            }, null);
    }

    public void BlockAFriend(string Name)
    {
        CSVR_UIWaitingIcon.instance.Open();

        UpdateRelationshipRequest UpdateRelationshipRequest = new UpdateRelationshipRequest();
        UpdateRelationshipRequest.PartnerId = Name;
        UpdateRelationshipRequest.Status = -1;

        HorusClientAPI.UpdateRelationship(UpdateRelationshipRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                CallApiUpdate();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                ErrorView.instance.ShowPopupError("Chặn bạn lỗi!!");
            }, null);
    }

    public void UnBlockAFriend(string Name)
    {
        CSVR_UIWaitingIcon.instance.Open();

        UpdateRelationshipRequest UpdateRelationshipRequest = new UpdateRelationshipRequest();
        UpdateRelationshipRequest.PartnerId = Name;
        UpdateRelationshipRequest.Status = 0;

        HorusClientAPI.UpdateRelationship(UpdateRelationshipRequest,
            (result) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                CallApiUpdate();
            },
            (error) =>
            {
                CSVR_UIWaitingIcon.instance.Close();
                ErrorView.instance.ShowPopupError("Bỏ chặn bạn lỗi!!");
            }, null);
    }
}