﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIClanList_TaoClan_MuaAvata_AvataFeeItem_Callback : MonoBehaviour
{
    public Image ImgSprite;
    public Text TextPrice;

    public GameObject ContentConfirm;

    void ScrollCellIndex(int idx)
    {
        this.ImgSprite.sprite = CSVR_UIClanList_TaoClan_MuaAvata.instance.listTraPhi[idx].Sprite;
        this.TextPrice.text = CSVR_UIClanList_TaoClan_MuaAvata.instance.listTraPhi[idx].Price.ToString();
    }

    void Button_Click()
    {
        ContentConfirm.gameObject.SetActive(true);
    }

    void Button_Confirm_Click()
    {
        CSVR_UIClanList_TaoClan.instance.IMGAvatarClan = ImgSprite;
        CSVR_UIClanList_TaoClan_MuaAvata.instance.Close();
    }

    void Button_Close_Confirm_Click()
    {
        ContentConfirm.gameObject.SetActive(false);
    }
}
