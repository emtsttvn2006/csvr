﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_Setting : MonoBehaviour
{
    public Toggle gyroEnabled;
    public Slider gyroSpeed;
    public Slider lookSensitivitySpeed;
    public GameObject KeySetting;
    public Slider music;
    public Slider soundFX;

    void Start()
    {
        gyroEnabled.isOn = CSVR_GameSetting.gyroEnabled;
        gyroSpeed.value = CSVR_GameSetting.gyroScale;
        lookSensitivitySpeed.value = CSVR_GameSetting.lookSensitivitySpeed;

        music.value = CSVR_GameSetting.music;

        soundFX.value = CSVR_GameSetting.soundEfx;
        
    }
    public void  ShowKeySetting(int id)
    {
        switch (id)
        {
            case 1:
                KeySetting.SetActive(true);
                break;
            case 2:
                KeySetting.SetActive(false);
                break;
            default:
                break;
        }
    }
    //Setting Save Controll
    public void UI_OnSettingSaveControll()
    {
        CSVR_GameSetting.gyroEnabled = gyroEnabled.isOn;
        CSVR_GameSetting.gyroScale = gyroSpeed.value;
        CSVR_GameSetting.lookSensitivitySpeed = lookSensitivitySpeed.value;
        CSVR_GameSetting.SaveSetting();
        this.gameObject.SetActive(false);
    }

    //Setting Save Music
    public void UI_OnSettingSaveMusic()
    {
        CSVR_GameSetting.music = music.value;
        CSVR_GameSetting.soundEfx = soundFX.value;
        CSVR_GameSetting.SaveSetting();
        this.gameObject.SetActive(false);
    }
    //Setting On
    public void UI_OnSettingHomeBackOn()
    {
        this.transform.gameObject.SetActive(true);
//        CSVR_GUITabController guiTab = this.transform.GetComponent<CSVR_GUITabController>();
//        guiTab.Run(guiTab.tabContentsPairs[3]);
    }
    //Setting On
    public void UI_OnSettingExitOn()
    {
        Application.Quit();
    }

    //Setting On
    public void UI_OnSettingOn()
    {
        this.transform.gameObject.SetActive(true);
    }

    //Setting Off
    public void UI_OnSettingOff()
    {
        this.transform.gameObject.SetActive(false);
    }

    public void UpdateSoundEfx(float value)
    {
        CSVR_MusicManager.instance.efxSource.volume = value;
    }
    public void UpdateMusic(float value)
    {
        CSVR_MusicManager.instance.musicSource.volume = value;
    }
	public void ExitGame(){
		PhotonNetwork.LeaveRoom();
		PhotonNetwork.LoadLevel("GameOver");
	}
}
