﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using MarchingBytes;
using Horus.ClientModels;
using UnityEngine.Events;
using Horus;

[System.Serializable]
public class stringlist
{
    public List<string> listString { get; set; }

    public stringlist()
    {
        this.listString = new List<string>();
    }

    public stringlist(List<string> listString)
    {
        this.listString = listString;
    }

    public void Add(string str)
    {
        this.listString.Add(str);
    }
}

[System.Serializable]
public class string2
{
    public string Key { get; set; }
    public string Value { get; set; }

    public string2()
    {
        this.Key = "";
        this.Value = "";
    }        

    public string2(string str1, string str2)
    {
        this.Key = str1;
        this.Value = str2;
    }
}

public class XemMyClan_TinNhan
{
    public string content { get; set; }
    public string date { get; set; }

    public XemMyClan_TinNhan()
    {
        this.content = "";
        this.date = "";
    }
}

public class CSVR_UIClanList_XemMyClan : SingletonBaseScreen<CSVR_UIClanList_XemMyClan> {

    public GameObject ContentHolderConfirmThoatClan;
    public Text TextContentHolderConfirm;
    public Button ButtonContentHolderConfirm;

    public Image IMGAvatarClan;
    public GameObject ContentHolderAvatar;
    public LoopHorizontalScrollRect scrollAvatar;
    public Text TextClanName;

    public Text TextChuClan;
    public Text TextClanLV;
    public Text TextDiemHoatDong;
    public Text TextClanMember;
    public Text TextClanDate;

    public Text TextClanDescription;
    public InputField INPClanDescription;
    public InputField INPClanThongBao;

    public Button ButtonCapNhat;
    public Button ButtonThongBao;    
    public Button ButtonThanhVien;
    public Button ButtonUngVien;    
    public Button ButtonQuyenGop;
    public Button ButtonThoatClan;
    public Button ButtonChangeLogo;
    public Image ImageChangeLogoShow;
    public Button ButtonChangeDescription;    

    [HideInInspector]
    public ClanResult ClanResult;

    [HideInInspector]
    public List<string2> listString_Members = new List<string2>();

    [HideInInspector]
    public List<UserInfo> listUser = new List<UserInfo>();

    [HideInInspector]
    public List<string2> listString_Joining = new List<string2>();

    [HideInInspector]
    public List<XemMyClan_TinNhan> listTinnhan = new List<XemMyClan_TinNhan>();

    public string currentLogo = "";
    public string currentDescription = "";

    public override void Open()
    {
        base.Open();
        this.transform.SetSiblingIndex(CSVR_UICLanList.instance.transform.GetSiblingIndex() + 1);

        Init();
    }

    public override void Close()
    {
        base.Close();
        CSVR_UICLanList.instance.Open();
    }

    void Start()
    {
                
    }

    void Init()
    {
        ContentHolderConfirmThoatClan.gameObject.SetActive(false);
        ButtonContentHolderConfirm.onClick.RemoveAllListeners();

        ButtonUngVien.gameObject.SetActive(false);

        this.ClanResult = new ClanResult();
    }

    public void Show(ClanResult ClanResult)
    {
        if (ClanResult != null)
        {
            this.ClanResult = ClanResult;
            this.ClanResult.UpdateAllMemberArray();

            if (this.ClanResult.logo != null && this.ClanResult.logo != "")
            {
                this.IMGAvatarClan.sprite = UISpriteManager.instance.GetClanLogoSprite(this.ClanResult.logo);
            } else
            {
                this.IMGAvatarClan.sprite = UISpriteManager.instance.GetClanLogoSprite("0");
            }

            this.currentLogo = this.IMGAvatarClan.sprite.name;

            this.TextClanName.text = this.ClanResult.roleId;
            this.TextClanLV.text = this.ClanResult.level.ToString();
            this.TextClanDate.text = this.ClanResult.created_at;
            this.TextClanMember.text = this.ClanResult.memberCount.ToString();
            this.TextChuClan.text = this.ClanResult.chief;
            this.TextDiemHoatDong.text = "--";            
            this.INPClanDescription.text = this.ClanResult.description;
            this.currentDescription = this.ClanResult.description;

            GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();

            this.listUser.Clear();

            this.listString_Members.Clear();
            this.listString_Members.Add(new string2("TRƯỞNG CLAN", this.ClanResult.chief));

            this.listString_Joining.Clear();

            GetUserInfoRequest.Add(this.ClanResult.chief);

            for (int i = 0; i < this.ClanResult.Vice.Count; i++)
            {
                if (GetIndex(this.ClanResult.Vice[i], this.listString_Members) == -1)
                {
                    this.listString_Members.Add(new string2("PHÓ CLAN", this.ClanResult.Vice[i]));
                    GetUserInfoRequest.Add(this.ClanResult.Vice[i]);
                }
            }

            for (int i = 0; i < this.ClanResult.Members.Count; i++)
            {
                if (GetIndex(this.ClanResult.Members[i], this.listString_Members) == -1)
                {
                    this.listString_Members.Add(new string2("THÀNH VIÊN", this.ClanResult.Members[i]));
                    GetUserInfoRequest.Add(this.ClanResult.Members[i]);
                }
            }

            if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName == CSVR_UICLanList.instance.MyClan.roleId)
            {
                if (HorusManager.instance.playerUserName == CSVR_UICLanList.instance.MyClan.chief ||
                    CSVR_UICLanList.instance.MyClan.Vice.FindIndex((Name) => Name == HorusManager.instance.playerUserName) != -1)
                {
                    for (int i = 0; i < this.ClanResult.Joining.Count; i++)
                    {
                        if (GetIndex(this.ClanResult.Joining[i], this.listString_Members) == -1 &&
                            GetIndex(this.ClanResult.Joining[i], this.listString_Joining) == -1)
                        {
                            this.listString_Joining.Add(new string2("XIN VÀO", this.ClanResult.Joining[i]));
                            GetUserInfoRequest.Add(this.ClanResult.Joining[i]);
                        }
                    }

                    for (int i = 0; i < this.ClanResult.Invite.Count; i++)
                    {
                        if (GetIndex(this.ClanResult.Invite[i], this.listString_Members) == -1 &&
                            GetIndex(this.ClanResult.Invite[i], this.listString_Joining) == -1)
                        {
                            this.listString_Joining.Add(new string2("ĐANG MỜI", this.ClanResult.Invite[i]));
                            GetUserInfoRequest.Add(this.ClanResult.Invite[i]);
                        }
                    }
                }
            }

            CSVR_UIWaitingIcon.instance.Open();

            HorusClientAPI.GetUserInfo(GetUserInfoRequest,
                (result) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    this.listUser = result.users;
                    ButtonUngVien.gameObject.SetActive(true);
                },
                (error) => 
                {
                    CSVR_UIWaitingIcon.instance.Close();
                }, null);

            InitChangeLogoNChangeDescription(HorusManager.instance.playerUserName == this.ClanResult.chief ||
                (this.ClanResult.GetViceIndex(HorusManager.instance.playerUserName) != -1));
            
        } else
        {
            ErrorView.instance.ShowPopupError("Bạn CHƯA ở trong Clan nào cả!!!");
            CSVR_UICLanList.instance.UpdateButtonXem_Tao();
            Close();
        }
    }

    public void InitChangeLogoNChangeDescription(bool ChiefVice = false)
    {
        ButtonCapNhat.gameObject.SetActive(false);
        INPClanDescription.interactable = false;

        if (ChiefVice)
        {
            ButtonThongBao.gameObject.SetActive(true);

            ButtonChangeLogo.interactable = true;
            ButtonChangeLogo.onClick.RemoveAllListeners();
            ButtonChangeLogo.onClick.AddListener(new UnityAction(() =>
            {
                Button_ThayLogo_Click();
            }));

            ImageChangeLogoShow.gameObject.SetActive(true);
            ButtonChangeDescription.gameObject.SetActive(true);

            ButtonChangeDescription.onClick.RemoveAllListeners();
            ButtonChangeDescription.onClick.AddListener(new UnityAction(() =>
            {
                Button_ThayDescription_Click();
            }));
        } else
        {
            ButtonThongBao.gameObject.SetActive(false);
            ButtonChangeLogo.interactable = false;
            ImageChangeLogoShow.gameObject.SetActive(false);
            ButtonChangeDescription.gameObject.SetActive(false);            
        }
    }

    public void Button_ThayLogo_Click()
    {
        ContentHolderAvatar.gameObject.SetActive(true);
        Update_scrollAvatar();
    }

    public void Button_ThayDescription_Click()
    {
        INPClanDescription.interactable = true;
    }

    public void INPClanDescription_EndEdit()
    {
        if (INPClanDescription.text != "")
        {
            if (INPClanDescription.text != currentDescription)
            {
                Update_ButtonCapNhat();
            }
        } else
        {
            ErrorView.instance.ShowPopupError("Khẩu hiệu không thể rỗng!!");
            INPClanDescription.interactable = false;
        }
    }

    public void Update_ButtonCapNhat()
    {
        if (this.IMGAvatarClan.sprite.name != currentLogo || (this.INPClanDescription.text != "" && this.INPClanDescription.text != this.currentDescription)){
            ButtonCapNhat.gameObject.SetActive(true);
            ButtonCapNhat.onClick.RemoveAllListeners();
            ButtonCapNhat.onClick.AddListener(new UnityAction(() =>
            {
                Button_CapNhat_Click();
            }));
        } else
        {
            ButtonCapNhat.gameObject.SetActive(false);
        }
    }

    public void Button_CapNhat_Click()
    {
        CSVR_UICLanList.instance.UpdateAClan(this.IMGAvatarClan.sprite.name, this.INPClanDescription.text);
    }

    public void Update_scrollAvatar()
    {
        scrollAvatar.totalCount = UISpriteManager.instance.LogoClan.Length;
        scrollAvatar.RefillCells();
    }    

    public void Button_ThongBao_Click()
    {
        ErrorView.instance.ShowPopupError("CHỨC NĂNG NÀY ĐANG PHÁT TRIỂN!!");
    }

    public void Button_ThanhVien_Click()
    {
        CSVR_UIMember.instance.Open();
    }

    public void Button_UngVien_Click()
    {
        CSVR_UIJoining.instance.Open();
    }

    public void Button_QuyenGop_Click()
    {        
        CSVR_UIDongGop.instance.Open();
    }

    public UserInfo GetUserInfo(int index)
    {
        if (index != -1) return this.listUser[index];
        else return null;
    }

    public int GetIndex(string Name, List<UserInfo> list)
    {
        return list.FindIndex((UserInfo) => UserInfo.UserName == Name);        
    }

    public int GetIndex(string Name, List<string2> list)
    {
        return list.FindIndex((string2) => string2.Value == Name);        
    }

    public void Button_XinVao_Click()
    {
        CSVR_UICLanList.instance.JoiningAClan(this.ClanResult.roleId);
    }

    public void Button_ThoatClan_Click()
    {
        if (HorusManager.instance.Clan != null && this.ClanResult.roleId == HorusManager.instance.Clan.clanName && this.ClanResult.chief == HorusManager.instance.playerUserName)
        {
            if (CSVR_UICLanList.instance.MyClan.Members.Count == 1)
            {
                ContentHolderConfirmThoatClan.gameObject.SetActive(true);
                TextContentHolderConfirm.text = "BẠN LÀ TRƯỞNG CLAN. NẾU BẠN THOÁT, CLAN NÀY SẼ BỊ XÓA VĨNH VIỄN. \nBẠN CÓ CÂN NHẮC HÀNH ĐỘNG NÀY KHÔNG? \nHÃY BỔ NHIỆM NGƯỜI KHÁC ĐỂ GIỮ CLAN NÀY!!";
                ButtonContentHolderConfirm.onClick.RemoveAllListeners();
                ButtonContentHolderConfirm.onClick.AddListener(new UnityAction(() => 
                {
                    CSVR_UICLanList.instance.InActiveAClan();
                    Close();
                }));
            } else if (CSVR_UICLanList.instance.MyClan.Members.Count > 1)
            {
                ContentHolderConfirmThoatClan.gameObject.SetActive(true);
                TextContentHolderConfirm.text = "HÃY BỔ NHIỆM TRƯỞNG CLAN CHO PHÓ CLAN VÀ THỰC HIỆN LẠI THAO TÁC NÀY!!";
                ButtonContentHolderConfirm.onClick.RemoveAllListeners();
                ButtonContentHolderConfirm.onClick.AddListener(new UnityAction(() => 
                {                    
                    CSVR_UIMember.instance.Open();
                    Close();
                }));
            }
        } else if (this.ClanResult.roleId == HorusManager.instance.Clan.clanName && this.ClanResult.chief != HorusManager.instance.playerUserName)
        {
            CSVR_UICLanList.instance.OutAClan();
        }
    }

    public void Button_Close_Click()
    {
        CSVR_UICLanList.instance.UpdateClanList(CSVR_UICLanList.instance.currentClanListBy, CSVR_UICLanList.instance.currentClanOrder, CSVR_UICLanList.instance.currentClanPage);
        CSVR_UICLanList.instance.ResetButtonPreviousNext();
    }

    public void Button_Confirm_Close_Click()
    {
        ButtonContentHolderConfirm.onClick.RemoveAllListeners();
        ContentHolderConfirmThoatClan.gameObject.SetActive(false);
    }    
}
