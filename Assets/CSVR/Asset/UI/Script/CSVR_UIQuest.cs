﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;
using MarchingBytes;

public class CSVR_UIQuest : SingletonBaseScreen<CSVR_UIQuest> {

	[SerializeField] private LoopVerticalScrollRect NhiemVuHangNgay;
	[SerializeField] private LoopVerticalScrollRect NhiemVuChinhTuyen;

	public  List<QuestInfo> questNhiemVuHangNgay;
	public  List<QuestInfo> questNhiemVuChinhTuyen;
	public Dictionary<string,Dictionary<string,int>> dailyGame;
	public string dailyGameKey;

	void Start(){
		questNhiemVuHangNgay = new List<QuestInfo> ();
		questNhiemVuChinhTuyen = new List<QuestInfo> ();
		dailyGame = AccountManager.instance.dailyGame;
		foreach (KeyValuePair<string,Dictionary<string,int>> p in dailyGame) {
			dailyGameKey = p.Key;
		}
		if (!NhiemVuHangNgay.prefabPool) NhiemVuHangNgay.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
		if (!NhiemVuChinhTuyen.prefabPool) NhiemVuChinhTuyen.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();

		SCR_NhiemVuHangNgay_Run ();
		SCR_NhiemVuChinhTuyen_Run ();
	}

	public void SCR_NhiemVuHangNgay_Run()
	{	
		HorusClientAPI.GetQuestSerial(new GetQuestSerialRequest(){
			serial = GameConstants.VNQUESTSERIAL_Mission 
		}, 
			(result) => {
				questNhiemVuHangNgay = result.questList;
				questNhiemVuHangNgay.Sort();
				HUD_UpdateNhiemVuHangNgay_Run();
			},
			(error) => {
				Yo.JsonLog("error",error);
			}
		);
	}
	private void HUD_UpdateNhiemVuHangNgay_Run()
	{
		NhiemVuHangNgay.totalCount = questNhiemVuHangNgay.Count;
		NhiemVuHangNgay.Rebuild(CanvasUpdate.Layout);
	}

	public void SCR_NhiemVuChinhTuyen_Run()
	{	
		HorusClientAPI.GetQuestSerial(new GetQuestSerialRequest(){
			serial = GameConstants.VNQUESTSERIAL_Main
		}, 
			(result) => {
				questNhiemVuChinhTuyen = result.questList;
				questNhiemVuChinhTuyen.Sort();
				HUD_UpdateNhiemVuChinhTuyen_Run();
			},
			(error) => {
				Yo.JsonLog("error",error);
			}
		);
	}
	private void HUD_UpdateNhiemVuChinhTuyen_Run()
	{
		NhiemVuChinhTuyen.totalCount = questNhiemVuChinhTuyen.Count;
		NhiemVuChinhTuyen.Rebuild(CanvasUpdate.Layout);
	}
}
