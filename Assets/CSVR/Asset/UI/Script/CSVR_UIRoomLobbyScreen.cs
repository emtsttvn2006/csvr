﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus.Json;
using MarchingBytes;

public class CSVR_UIRoomLobbyScreen : SingletonBaseScreen<CSVR_UIRoomLobbyScreen>
{    
	[SerializeField]
	GameObject findingMap;

	public List<RoomInfo> roomInfoList = new List<RoomInfo>();

	[SerializeField] private LoopVerticalScrollRect roomScroll;


	[SerializeField] private Text Chosen_TextMode;
	[SerializeField] private Text Chosen_TextMap;
	[SerializeField] private Text Chosen_TextName;
	[SerializeField] private Image Chosen_ImageMap;
	[SerializeField] private Text Chosen_TextPlayerCount;
	[SerializeField] private Text Chosen_TextKillTarget;
	[SerializeField] private Text Chosen_TextTime;
	[SerializeField] private Transform  ChatView_Parent;
	private string nameToJoin;

	public override void OnEnable()
	{
		OnReceivedRoomListUpdate ();

		TopBar.instance.SetBackButton(new UnityAction(() =>{
			CSVR_UIRoomLobbyScreen.instance.Close();
			MainView.instance.Open();
			CharacterView.instance.Open();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{
			CSVR_UIRoomLobbyScreen.instance.Close();
			CharacterView.instance.Open();
			CSVR_UIInventory.instance.Open();
			CSVR_UIInventory.instance.UI_OnInventoryOn();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
		}));
		TopBar.instance.ShopOrInVText.text = "Thùng đồ";

		Invoke ("ChatViewSetParent", 0.5f);
	}
	private void ChatViewSetParent(){
		HomeScreen.instance.ChatView_Item.SetParent (ChatView_Parent, false);
		RectTransform rectTransform = HomeScreen.instance.ChatView_Item.GetComponent<RectTransform>();
		rectTransform.offsetMin = Vector3.zero;
		rectTransform.offsetMax = Vector3.zero;
	}
	void reset()
	{
		Chosen_TextMode.text = string.Empty;
		Chosen_TextMap.text = string.Empty;
		Chosen_TextPlayerCount.text = string.Empty;
		Chosen_TextTime.text = string.Empty;
	}

	public void OnReceivedRoomListUpdate()
	{	
		roomInfoList.Sort(NameRoomComparetor);
		roomInfoList = new List<RoomInfo>();

		foreach (RoomInfo ri in PhotonNetwork.GetRoomList())
		{
			if (ri.customProperties.Count > 0) {
				if (ri.customProperties.ContainsKey (GameConstants.Vip)) {
					continue;
				} else {

					roomInfoList.Add (ri);
				}
			}
		}        
			
		//Xu ly moi dung` scrollrect
		roomScroll.totalCount = roomInfoList.Count;
		roomScroll.RefillCells();

		#region xu ly local cu~
		//Xu ly local cu~
		/*
        for (int i = 0; i < grid.childCount; i++)
        {
            CSVR_UIRoomLobbyScreenChild child = grid.GetChild(i).GetComponent<CSVR_UIRoomLobbyScreenChild>();
            RoomInfo roomInfo = roomInfoList.Find(x => x.name.Equals(child._roomInfo.name));
            if (roomInfo != null)
            {
                child.Show(roomInfo);
            }
            else
            {
                roomInfoList_InUI.Remove(roomInfoList_InUI.Find(x => x.name.Equals(child._roomInfo.name)));
                Destroy(child.gameObject);
            }
        }

        for (int i = 0; i < roomInfoList.Count; i++)
        {
            if (roomInfoList_InUI.Find(x => x.name.Equals(roomInfoList[i].name)) == null)
            {
                GameObject room = (GameObject) Instantiate(roomPanel);
                room.gameObject.SetActive(true);
                room.transform.SetParent(grid);
                room.transform.localScale = Vector3.one;

                room.GetComponent<CSVR_UIRoomLobbyScreenChild>().Show(roomInfoList[i]);
                roomInfoList_InUI.Add(roomInfoList[i]);
            }
            
        }*/
		#endregion
	}

	int NameRoomComparetor(RoomInfo room1, RoomInfo room2)
	{
		return room1.name.CompareTo(room2.name);
	}

	int PlayerCountComparetor(RoomInfo room1, RoomInfo room2)
	{
		return room1.playerCount.CompareTo(room2.playerCount);
	}

	public void ShowChosen(RoomInfo roomInfo)
	{
		Chosen_TextName.text = "Tên phòng "+ roomInfo.customProperties [GameConstants.Name].ToString ();
		Chosen_TextMode.text = HomeScreen.instance.lobbyResult.ModeAvaiable[roomInfo.customProperties[GameConstants.Mode].ToString()];
		Chosen_TextMap.text = HomeScreen.instance.lobbyResult.MapAvaiable[roomInfo.customProperties[GameConstants.Map].ToString()];
		Chosen_ImageMap.sprite = HomeScreen.instance.modeMapSprites[HomeScreen.instance.modeMapSpritesIndex[roomInfo.customProperties[GameConstants.Map].ToString()]];
		if(roomInfo.maxPlayers % 2 == 1)
			Chosen_TextPlayerCount.text = (roomInfo.playerCount - 1) + "/" + (roomInfo.maxPlayers - 1);
		else
			Chosen_TextPlayerCount.text = roomInfo.playerCount + "/" + roomInfo.maxPlayers;
		
		nameToJoin = roomInfo.name;

	}



	//tao room
	public void UI_OnCreateRoom_Click()
	{
		HomeScreen.instance.SetMatchRoomScreenCallBack (UI_MatchRoomScreen_CallBack);
		CSVR_UICreateRoomView.instance.Open ();
	}
	public void UI_MatchRoomScreen_CallBack()
	{
		CSVR_UIRoomLobbyScreen.instance.Close ();
		CSVR_UICreateRoomView.instance.Close ();
		ErrorView.instance.Loading (false);
		TopBar.instance.SetBackButton(new UnityAction(() =>{
			CSVR_UIRoomLobbyScreen.instance.Open();
			CSVR_UIMatchRoomScreen.instance.Close();
			CSVR_MPConnection.instance.LeaveRoom();
		}));   
	}
		
	public void UI_RoomIsFindingClose_Click()
	{
		CSVR_MPConnection.instance.LeaveRoom();
		findingMap.gameObject.SetActive(false);
	}
	public void UI_OnJoinRoomMenu_Click(){
		if (CSVR_GameSetting.modeName != "") {

			if ( CSVR_GameSetting.mapName != "") {
				HomeScreen.instance.SetLoadingMapCallBack (UI_LoadingMap_CallBack);
				findingMap.gameObject.SetActive(true);

				CSVR_MPConnection.instance.OnQuickJoinedRoom(true);
				CSVR_MPConnection.instance.JoinRoom ();

				CSVR_GameSetting.TuTaoPhong = false;
				CSVR_GameSetting.maxPlayer = 11;
				CSVR_GameSetting.maxSkill = 50;
				CSVR_GameSetting.timeMatch = 300;
			}
		}
	}

//	public void UI_OnJoinRoom_Click(){
//		HomeScreen.instance.SetLoadingMapCallBack (UI_LoadingMap_CallBack);
//		CSVR_MPConnection.instance.JoinRoom(nameToJoin);
//	}
	public void UI_LoadingMap_CallBack(){
		this.Close ();
		TopBar.instance.Close ();
	}
}
