﻿using UnityEngine;
using System.Collections;
using Horus;
using Horus.ClientModels;
using UnityEngine.UI;

public class PopupBanbeItemCallback : MonoBehaviour {

    private Color ColorOffline = Color.black;
    private Color ColorOnline = Color.green;
    private Color ColorPending = Color.white;

    public Text TextName;
    public Text TextLevel;
    public Image ImgRank;
    public Image ImgOnline;
    public Text TextStatus;

    [HideInInspector] public RelationshipInfo RelationshipInfo { get; set; }
    [HideInInspector] public string RoomPlaying = "";

    void ScrollCellIndex(int idx)
    {
        this.RelationshipInfo = PopupBanbe.instance.listBanBe[idx];
        TextName.text = (RelationshipInfo.Partner.DisplayName == string.Empty || RelationshipInfo.Partner.DisplayName == null) ? RelationshipInfo.Partner.UserName : RelationshipInfo.Partner.DisplayName;
        TextLevel.text = "LV " + RelationshipInfo.Partner.AccountLevel.ToString();
        ImgRank.sprite = UISpriteManager.instance.GetLevelSprite(RelationshipInfo.Partner.AccountLevel);
        TextStatus.text = RelationshipInfo.Status == 1 ? "Đang chờ" : string.Empty;

        string compareID = (PopupBanbe.instance.listBanBe[idx].CreateId == HorusManager.instance.playerID) ? PopupBanbe.instance.listBanBe[idx].PartnerId : PopupBanbe.instance.listBanBe[idx].CreateId;
        FriendInfo fi = PopupBanbe.instance.listBanbePhoton.Find(x => x.Name.Equals(compareID));
        if (fi == null)
        {
            SetImageOnline(-1);
        } else
        {
            if (fi.IsOnline) SetImageOnline(1); 
            else SetImageOnline(0);
            if (fi.Room != null && fi.Room != string.Empty)
            {
                this.RoomPlaying = fi.Room;            
            }
        }
    }

    public void PopupBanbeItem_Click()
    {
        PopupBanbe.instance.ShowRelationShip(this);
    }

    void SetImageOnline(int Online = -1)
    {
        switch (Online)
        {
            case -1: ImgOnline.color = ColorPending; break;
            case 1: ImgOnline.color = ColorOnline; break;
            case 0: ImgOnline.color = ColorOffline; break;
        }
    }
}
