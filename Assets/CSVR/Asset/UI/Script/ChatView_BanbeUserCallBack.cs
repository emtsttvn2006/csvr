﻿using UnityEngine;
using System.Collections;
using Horus;
using Horus.ClientModels;
using UnityEngine.UI;

public class ChatView_BanbeUserCallBack : MonoBehaviour {
    
    private Color ColorOffline = Color.yellow;
    private Color ColorOnline = Color.white;

    public Text TextName;
    public Text TextLevel;
    public Image ImgLevel;
    public Image ImgAvatar;    
    public Text TextStatus;
    public Image ImgNotification;    

    [HideInInspector]
    public BanbeUser BanbeUser { get; set; }    

    void Start()
    {
                
    }

    void ScrollCellIndex(int idx)
    {        
        this.BanbeUser = ChatCache.instance.BanbeCaches.BanbeUsers[idx];
        UpdateReadMessage(idx);
        TextName.text = BanbeUser.Name;
        TextLevel.text = "Cấp độ " + BanbeUser.Level;
        ImgAvatar.sprite = AccountManager.instance.avatarPicture;
        ImgLevel.sprite = UISpriteManager.instance.GetLevelSprite(int.Parse(BanbeUser.Level));
        TextStatus.text = string.Empty;        
    }

    

    public void UpdateReadMessage(int idx)
    {
        this.ImgNotification.gameObject.SetActive(!ChatCache.instance.BanbeCaches.BanbeUsers[idx].isRead);
    }

    public void ChatView_BanbeUser_Click()
    {
        /*if (this.BanbeUser.Name != CSVR_PlayerMessenger.instance.GlobalUser.Name.Remove(0, 1))
        {                        
            CSVR_UIChat.instance.NameChosenBanbe = this.BanbeUser.Name;
            CSVR_UIChat.instance.UpdateBanbeChatContent();

            int IndexChosenBanbe = ChatCache.instance.BanbeCaches.GetUserIndex(this.BanbeUser.Name);
            ChatCache.instance.BanbeCaches.BanbeUsers[IndexChosenBanbe].isRead = true;
            UpdateReadMessage(IndexChosenBanbe);
            
        }*/
    }    
}