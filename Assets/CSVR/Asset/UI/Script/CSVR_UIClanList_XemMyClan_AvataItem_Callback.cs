﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UIClanList_XemMyClan_AvataItem_Callback : MonoBehaviour
{
    public Image Image;

    void ScrollCellIndex(int idx)
    {
        this.Image.sprite = UISpriteManager.instance.GetClanLogoSprite(idx.ToString());
    }

    public void Button_Click()
    {
        CSVR_UIClanList_XemMyClan.instance.IMGAvatarClan.sprite = this.Image.sprite;
        CSVR_UIClanList_XemMyClan.instance.Update_ButtonCapNhat();
        CSVR_UIClanList_XemMyClan.instance.ContentHolderAvatar.gameObject.SetActive(false);
    }
}
