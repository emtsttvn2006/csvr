﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UILoadingMap : SingletonBaseScreen<CSVR_UILoadingMap> {

	[SerializeField]
	Text[] ModeMap;

	[SerializeField]
	Image[] policeTeamAvatars;

	[SerializeField]
	Image[] policeTeamPortraits;

	[SerializeField]
	Text[] policeTeamNames;

	[SerializeField]
	Text[] policeTeamLevels;


	[SerializeField]
	Image[] terroristTeamAvatars;

	[SerializeField]
	Image[] terroristTeamPortraits;

	[SerializeField]
	Text[] terroristTeamNames;
	
	[SerializeField]
	Text[] terroristTeamLevels;
	
	int[] teamsCount;
	[SerializeField]
	Sprite[] teamAvatar;

	ProjectDelegates.SimpleCallback OnLoadingMapCallBack = HomeScreen.instance.OnLoadingMapCallBack;

	void ClearPortraits()
	{
		StopCoroutine ("InstanceAI");
		teamsCount = new int[] { 0, 0 };

		for (int i = 0; i < policeTeamPortraits.Length; i++)
		{
			policeTeamPortraits[i].sprite = teamAvatar[0];
			if (policeTeamNames != null) policeTeamNames[i].text = "";
			if (terroristTeamLevels != null) policeTeamLevels[i].text = "";

			terroristTeamPortraits[i].sprite = teamAvatar[0];
			if (terroristTeamNames != null) terroristTeamNames[i].text = "";
			if (terroristTeamLevels != null) terroristTeamLevels[i].text = "";
		}
	}

	public override void OnEnable(){
		OnLoadingMapCallBack ();
	}
	int aiStart = 1;
	public void HienThi()
	{
		this.gameObject.SetActive (true);
		ClearPortraits();

		PhotonPlayer[] players = PhotonNetwork.playerList;
		for (int i = 0; i < players.Length; i++)
		{
			if (players[i].customProperties["Team"] == null) {
				continue;
			}
			TeamTypes playerTeam = (TeamTypes)int.Parse(players[i].customProperties["Team"].ToString());
			if(playerTeam == TeamTypes.RoomHostPlayer){
				continue;
			}else{
//				aiStart = i+1;
				byte[] pngBytes = players[i].customProperties["Avatar"] as byte[];
				Texture2D tex = new Texture2D(128, 128);
				tex.LoadImage(pngBytes);

				string playerNameHUD = players[i].customProperties["PlayFabName"].ToString();
				string playerLevelHUD = players[i].customProperties["Level"].ToString();
				Sprite playerAvatarHUD = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2());

				int myTeamCount = teamsCount[playerTeam.GetHashCode()];

				if (playerTeam == TeamTypes.Police)
				{
					policeTeamAvatars[myTeamCount].sprite = playerAvatarHUD;
					policeTeamPortraits[myTeamCount].sprite =  UISpriteManager.instance.GetLevelSprite(int.Parse(playerLevelHUD));
					policeTeamNames[myTeamCount].text = playerNameHUD;
					policeTeamLevels[myTeamCount].text = "Cấp độ: "+playerLevelHUD;
				}
				else
				{
					terroristTeamAvatars[myTeamCount].sprite = playerAvatarHUD;
					terroristTeamPortraits[myTeamCount].sprite = UISpriteManager.instance.GetLevelSprite(int.Parse(playerLevelHUD));
					terroristTeamNames[myTeamCount].text = playerNameHUD;
					terroristTeamLevels[myTeamCount].text = "Cấp độ: "+ playerLevelHUD;
				}
				teamsCount[playerTeam.GetHashCode()]++;
			}
		}
		StartCoroutine ("InstanceAI");
		try{
			ModeMap [0].text = HomeScreen.instance.lobbyResult.ModeAvaiable [CSVR_GameSetting.modeName];
			ModeMap [1].text = HomeScreen.instance.lobbyResult.MapAvaiable [CSVR_GameSetting.mapName];
		}catch{

		}
	}
	IEnumerator InstanceAI(){
		int k = 0;
		for (int i = aiStart; i < CSVR_GameSetting.AI_InGame.Count+aiStart; i++) {
			yield return new WaitForSeconds (0.2f);
			CSVR_UIAIInfo aiInfo = CSVR_GameSetting.AI_InGame[k];
			TeamTypes aiTeam = (TeamTypes)(aiInfo.AI_TeamNumber-1);

			byte[] pngBytes = AccountManager.instance.avatarPlayerTexture.EncodeToPNG();;
			Texture2D tex = new Texture2D(128, 128);
			tex.LoadImage(pngBytes);

			string aiNameHUD = aiInfo.AI_Name;
			int aiLevelHUD = aiInfo.AI_Level;
			Sprite aiAvatarHUD = Sprite.Create(tex, new Rect(0, 0, 128, 128), new Vector2());

			int myTeamCount = teamsCount[aiTeam.GetHashCode()];
			if (aiTeam == TeamTypes.Police)
			{
				policeTeamAvatars[myTeamCount].sprite = aiAvatarHUD;
				policeTeamPortraits[myTeamCount].sprite =  UISpriteManager.instance.GetLevelSprite(aiLevelHUD);
				policeTeamNames[myTeamCount].text = aiNameHUD;
				policeTeamLevels[myTeamCount].text = "Cấp độ: "+aiLevelHUD;
			}
			else
			{
				terroristTeamAvatars[myTeamCount].sprite = aiAvatarHUD;
				terroristTeamPortraits[myTeamCount].sprite = UISpriteManager.instance.GetLevelSprite(aiLevelHUD);
				terroristTeamNames[myTeamCount].text = aiNameHUD;
				terroristTeamLevels[myTeamCount].text = "Cấp độ: "+ aiLevelHUD;
			}
			teamsCount[aiTeam.GetHashCode()]++;
			k ++;
		}
	}
	public void UI_LoadingEndEventAnimation(){
		Invoke("CheckConnectRoom",30f);
	}

	void CheckConnectRoom(){
		if (instance == null) {
			//Debug.Log(" Đã vào trận ");
		} else {
			//Debug.Log(" Bị treo ");
			PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);
			if(PhotonNetwork.isMasterClient){
				PhotonNetwork.room.open = false;
				PhotonNetwork.DestroyAll();
			}
			PhotonNetwork.LeaveRoom();
			HorusManager.instance.gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(false);
			//HomeScreen.instance.states = States.Lobby;
			Application.LoadLevel("GameOver");
		}
	}

}
