﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class CSVR_UIMouse : MonoBehaviour, IPointerClickHandler, IDragHandler , IPointerEnterHandler , IPointerExitHandler
{
    public void OnPointerClick(PointerEventData eventData) // 3
    {
        print("I was clicked");
    }

    public void OnDrag(PointerEventData eventData)
    {
        print("I'm being dragged!");        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //target = Color.green;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //target = Color.red;
    }
}
