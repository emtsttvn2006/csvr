//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using PlayFab;
//using PlayFab.ClientModels;
//using PlayFab.Internal;
//
//
//
//public class CSVR_UIArmorShop : MonoBehaviour {
//	public static CSVR_UIArmorShop instance;
//	
//	public GameObject armorGunContainer;
//	public GameObject armorPanel;
//	
//	public GameObject dPanel;
//	public Text dName;
//	public Image dIcon;
//	public GameObject dInfo;
//	public GameObject dDay;
//	public Button dBuy;
//	private string itemClass,dayExpired,sellPrices;
//	private bool group1,group2,group3;
//	public Sprite[] iconType;
//	CSVR_ArmorInfo infoArmorTemp;
//	CSVR_DayInfo infoDayTemp;
//	float fillAmount;
//	int lvGun = 0; 
//	
//	void Awake()
//	{
//		instance = this;
//	}
//	
//	public void HienThi()
//	{
//		armorGunContainer.transform.Clear();
//		ToggleGroup group = armorGunContainer.transform.GetComponent<ToggleGroup>();
//		for(int i = 0; i < CSVR_GameSetting.shopArmorGun.Count; i++)
//		{
//			string idTemp,nameTemp,idIns;
//			bool damua;
//			GameObject pistolClone = Instantiate(armorPanel);
//			itemClass = CSVR_GameSetting.shopArmorGun[0].ItemClass;
//			{
//				//chua mua
//				idIns = "";
//				sellPrices = "0";
//				lvGun = 0;
//				if(CSVR_GameSetting.shopArmorGun[i].CustomData != null){
//					infoArmorTemp = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_ArmorInfo>(CSVR_GameSetting.shopArmorGun[i].CustomData);
//					dayExpired = infoArmorTemp.D0.ToString()+" Day";
//				}
//				idTemp = CSVR_GameSetting.shopArmorGun[i].ItemId;
//				nameTemp = CSVR_GameSetting.shopArmorGun[i].DisplayName;
//				damua = false;
//				
//				fillAmount = 1;
//				//				dayExpired = "";
//				pistolClone.transform.GetChild(3).GetComponent<Image>().sprite = iconType[1];
//				pistolClone.transform.GetChild(4).GetComponent<Text>().text = CSVR_GameSetting.itemPrices[idTemp].ToString();
//			}
//
//			pistolClone.name = pistolClone.transform.GetChild(1).GetComponent<Text>().text = CSVR_GameSetting.shopArmorGun[i].DisplayName;
//			//Debug.Log("idTemp "+idTemp);
//			pistolClone.transform.GetChild(2).GetComponent<Image>().sprite = CSVR_AssetManager.itemIconDic[idTemp];
//			pistolClone.transform.SetParent(armorGunContainer.transform,false);
//			pistolClone.transform.localScale = Vector3.one;
//			
//			Toggle b = pistolClone.GetComponent<Toggle>();
//			b.group = group;
//			AddListenerItems(b,idTemp,nameTemp,itemClass,dayExpired,fillAmount,damua,group1,group2,group3,infoArmorTemp,lvGun,sellPrices,idIns); // Usi
//			if(i==0){
//				b.isOn = true;
//				DetailPanel(idTemp,nameTemp,itemClass,dayExpired,fillAmount,damua,group1,group2,group3,infoArmorTemp,lvGun,sellPrices,idIns);
//			}
//		}
//	}
//	private void AddListenerItems(Toggle b,string id,string name,string iClass,string day,float fillA,bool damua,bool gr1,bool gr2,bool gr3,CSVR_ArmorInfo gun,int lv,string pri,string insId)
//	{
//		b.onValueChanged.RemoveAllListeners();
//		b.onValueChanged.AddListener((value) => DetailPanel(id,name,iClass,day,fillA,damua,gr1,gr2,gr3,gun,lv,pri,insId));
//	}
//	
//	private void DetailPanel(string id,string n,string itemClass,string day,float fill,bool damua,bool group1,bool group2,bool group3,CSVR_ArmorInfo gun,int level,string pri,string insId)
//	{
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.changeGroupClip);
//		
//		if(dPanel.activeSelf == false)
//			dPanel.SetActive(true);
//		
//		dIcon.sprite = CSVR_AssetManager.itemIconDic[id];
//		
//		dDay.transform.GetChild(0).GetComponent<Text>().text = day;
//		dName.text = n;
//		dInfo.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = gun.Reliability.ToString();
//		dInfo.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = gun.Resilience.ToString();
////		dInfo.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Image>().fillAmount = float.Parse(gun.G2.ToString())/float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_PistolROFMax"));
////		dInfo.transform.GetChild(2).GetChild(1).GetComponent<Text>().text = gun.G4.ToString()+" s";
////		dInfo.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = gun.G3.ToString()+" kg";
////		dInfo.transform.GetChild(4).GetChild(1).GetComponent<Text>().text = gun.G7.ToString()+" m";
////		
//		
//		if(damua ){
//			
//		}else{
//			dBuy.gameObject.SetActive(true);
//			
//			
//			Button b = dBuy;
//			
//			AddListenerBuyItem(b,id,gun,itemClass,n,day,dIcon.sprite);
//		}
//		
//	}
//	
//	
//	private void AddListenerBuyItem(Button b,string id,CSVR_ArmorInfo gun,string classs,string name,string day,Sprite icon)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> BuyPistolItem(id,gun,classs,name,day,icon));
//		
//	}
//	private void BuyPistolItem(string id,CSVR_ArmorInfo gun,string classs,string name,string day,Sprite icon)
//	{
//		//Debug.Log("id :"+id +"Test item class "+classs);
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.INVENTORY_PISTOLGUN;
//		//CSVR_UIPopupConfirmShop.instance.ShowPopUpPurchaseArmor(id,gun,classs,name,day,icon);
//	}
//	
//	
//}
