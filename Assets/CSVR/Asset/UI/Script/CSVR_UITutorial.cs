﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CSVR_UITutorial : SingletonBaseScreen<CSVR_UITutorial> {
/*
    public Image ImgBackGround;
    public RectTransform ButtonRect;
    public RectTransform ButtonRectShow;
    public Button Button;
    public GameObject ContentHolder_AskingAtFirstTime;
    public Animator AnimatorButtonHand;

    public CSVR_UITutorial_Follow CSVR_UITutorial_Follow;

    public bool Next = false;

    public Text TextNoiDung;
    public GameObject ContentReward1;
    public GameObject ContentReward2;

    public Button ButtonCo;
    public Button ButtonKhong;
    public Button ButtonTieptuc;
    public Button ButtonDesau;
    public Button ButtonNhan;

    [HideInInspector]
    public Vector2 v2Size = Vector2.zero;

    public Vector3 Vector3 = Vector3.zero;

    public override void Open()
    {
        base.Open();

        GotoLastInLayer();

        this.v2Size = HomeScreen.instance.v2Size;
        TutorialManager.instance.Pause = false;

        if (!TutorialManager.instance.Pause)
        {
            ShowAction();
        } else
        {
            ShowContinue();
        }
    }
    
    public void Reset_CSVR_UITutorial_Follow()
    {
        this.CSVR_UITutorial_Follow.isFollow = false;
        SetIdle();
    }

    public void SetUpAction_CSVR_UITutorial_Follow(UnityAction UnityAction)
    {
        UnityAction.Invoke();
    }

    public override void Awake()
    {
        base.Awake();                
        RepeatFollow();
    }

    public void Button_Co_Click()
    {
        TutorialManager.instance.Pause = false;
        TutorialManager.instance.currentTut = 0;
        TutorialManager.instance.NextTutorial();
        CloseContentAsk();
    }

    public void CloseContentAsk()
    {
        this.ContentHolder_AskingAtFirstTime.gameObject.SetActive(false);
        ImgBackGround.color = new Color(0f, 0f, 0f, 0.01f);
    }

    public void OpenContentAsk()
    {
        this.ContentHolder_AskingAtFirstTime.gameObject.SetActive(true);
    }

    public void Button_LamSau_Click()
    {
        TutorialManager.instance.Pause = true;
        Close();
    }

    public void Button_BoQua_Click()
    {
        TutorialManager.instance.Finish("TutorialState");
        Close();
    }

    public void Button_Nhan_Click()
    {
        ErrorView.instance.ShowPopupError("Được nhận thưởng: ");
        Close();
    }

    #region Set Direction
    public void SetIdle()
    {
        this.AnimatorButtonHand.SetBool("Idle", true);
        this.AnimatorButtonHand.SetBool("Top", false);
        this.AnimatorButtonHand.SetBool("Bottom", false);
        this.AnimatorButtonHand.SetBool("Left", false);
        this.AnimatorButtonHand.SetBool("Right", false);
    }

    public void SetTop()
    {
        SetIdle();
        this.Parameter = "Top";
        Invoke("SetBoolDelay", 0.7f);
    }

    public void SetBottom()
    {
        SetIdle();
        this.Parameter = "Bottom";
        Invoke("SetBoolDelay", 0.7f);
    }

    public void SetLeft()
    {
        SetIdle();
        this.Parameter = "Left";
        Invoke("SetBoolDelay", 0.7f);
    }

    public void SetRight()
    {
        SetIdle();
        this.Parameter = "Right";
        Invoke("SetBoolDelay", 0.7f);
    }

    public string Parameter = "";

    public void SetBoolDelay()
    {
        this.AnimatorButtonHand.SetBool(Parameter, true);
        this.AnimatorButtonHand.SetBool("Idle", false);
    }

    #endregion    

    public void RepeatFollow()
    {
        InvokeRepeating("Follow", 0.0f, 0.4f);
    }

    public void Follow()
    {
        if (this.ButtonRect != null && this.CSVR_UITutorial_Follow.RectFollow != null && this.CSVR_UITutorial_Follow.isFollow)
        {
            if (!CSVR_UITutorial_Follow.gameObject.activeSelf)
                CSVR_UITutorial_Follow.gameObject.SetActive(true);

            this.ButtonRect.position = this.CSVR_UITutorial_Follow.RectFollow.position + this.CSVR_UITutorial_Follow.v3OffSet;
        } else if (!this.CSVR_UITutorial_Follow.isFollow)
        {
            if (CSVR_UITutorial_Follow.gameObject.activeSelf)
                CSVR_UITutorial_Follow.gameObject.SetActive(false);
        }
    }

    public void SetAction(UnityAction UnityAction)
    {
        this.Button.onClick.RemoveAllListeners();
        this.Button.onClick.AddListener(UnityAction);        
    }    
    
    public void Update()
    {
        if (this.Next == true)
        {
            this.Next = false;
            TutorialManager.instance.currentTut = 0;
            TutorialManager.instance.NextTutorial();
        }
    }

    public void ShowAction()
    {
        ImgBackGround.color = new Color(0f, 0f, 0f, 0.5f);
        ContentHolder_AskingAtFirstTime.gameObject.SetActive(true);
        this.CSVR_UITutorial_Follow.isFollow = false;

        TextNoiDung.text = "BẠN CÓ MUỐN LÀM HƯỚNG DẪN ĐỂ NHẬN THƯỞNG?";

        ButtonCo.gameObject.SetActive(true);
        ButtonKhong.gameObject.SetActive(true);
        ButtonDesau.gameObject.SetActive(true);
        ButtonTieptuc.gameObject.SetActive(false);
        ButtonNhan.gameObject.SetActive(false);
    }

    public void ShowContinue()
    {
        ContentHolder_AskingAtFirstTime.gameObject.SetActive(true);
        this.CSVR_UITutorial_Follow.isFollow = false;
        ImgBackGround.color = new Color(0f, 0f, 0f, 0.5f);

        TextNoiDung.text = "TIẾP TỤC HOÀN THÀNH HƯỚNG DẪN ĐỂ NHẬN THƯỞNG NHÉ!!";

        ButtonCo.gameObject.SetActive(false);
        ButtonKhong.gameObject.SetActive(true);
        ButtonDesau.gameObject.SetActive(true);
        ButtonTieptuc.gameObject.SetActive(true);
        ButtonNhan.gameObject.SetActive(false);
    }

    public void ShowReward()
    {
        ImgBackGround.color = new Color(0, 0, 0, 0.5f);
        ContentHolder_AskingAtFirstTime.gameObject.SetActive(true);
        this.CSVR_UITutorial_Follow.isFollow = false;

        TextNoiDung.text = "ĐÃ HOÀN THÀNH HƯỚNG DẪN!! BẠN ĐƯỢC THƯỞNG : ";

        ButtonCo.gameObject.SetActive(false);
        ButtonKhong.gameObject.SetActive(false);
        ButtonDesau.gameObject.SetActive(false);
        ButtonTieptuc.gameObject.SetActive(false);
        ButtonNhan.gameObject.SetActive(true);
    }

    public void SetUpTutorial_ButtonNhan()
    {
        SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial_Follow.SetFollow(ButtonNhan.GetComponent<RectTransform>());
            CSVR_UITutorial_Follow.isFollow = true;
            SetTop();
        }));
    }

    public void SetUpTutorial_TieptucChoDenKhiDayTui()
    {
        this.TextNoiDung.text = "Tìm cách trang bị đầy túi và bắt đầu 1 ván mới nhé!!";
        ButtonCo.gameObject.SetActive(false);
        ButtonKhong.gameObject.SetActive(false);
        ButtonDesau.gameObject.SetActive(false);
        ButtonTieptuc.gameObject.SetActive(false);
        ButtonNhan.gameObject.SetActive(true);

        SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial_Follow.SetFollow(ButtonTieptuc.GetComponent<RectTransform>());
            CSVR_UITutorial_Follow.isFollow = true;
            SetTop();
        }));
    }
    */
}
