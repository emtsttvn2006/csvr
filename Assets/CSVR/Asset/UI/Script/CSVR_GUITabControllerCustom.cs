﻿//using UnityEngine;
//using UnityEngine.UI;
//using System;
//using System.Collections.Generic;
//
//public class CSVR_GUITabControllerCustom : MonoBehaviour {
//	
//	[SerializableAttribute]
//	public struct TabContentsPair {
//		public Button tab;
//		public CanvasRenderer contentBody;
//		public Button tabSelect;
//		
//		public void SetTabInteractable(bool b) {
//			tab.interactable = b;
//			tabSelect.interactable = !b;
//			if (tabSelect != null) {
//				tabSelect.gameObject.SetActive(!b);
//				tab.gameObject.SetActive(b);
//			}
//		}
//	}
//	public LayoutElement itemLayout;
//	public List<TabContentsPair> tabContentsPairs;
//	
//	// Use this for initialization
//	void Start() {
//		if (tabContentsPairs != null) {
//			tabContentsPairs.ForEach(pair => {
//				// initialize tab state
//				pair.SetTabInteractable(!pair.contentBody.gameObject.activeSelf);
//				// add click listener
//				pair.tab.onClick.AddListener(() => {
//					// switch active contents
//					tabContentsPairs.ForEach(p => p.contentBody.gameObject.SetActive(false));
//					pair.contentBody.gameObject.SetActive(true);
//
//					// switch interactable
//					tabContentsPairs.ForEach(p => p.SetTabInteractable(true));
//					pair.SetTabInteractable(false);
//					itemLayout.minHeight = 850;
//				});
//				// add click listener
//				pair.tabSelect.onClick.AddListener(() => {
//					// switch active contents
//					tabContentsPairs.ForEach(p => p.contentBody.gameObject.SetActive(false));
//					// switch interactable
//					tabContentsPairs.ForEach(p => p.SetTabInteractable(true));
//					itemLayout.minHeight = 310;
//				});
//			});
//		}
//	}
//}