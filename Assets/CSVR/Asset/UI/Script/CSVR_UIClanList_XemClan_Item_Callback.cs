﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;
using Horus;
using UnityEngine.Events;

public class CSVR_UIClanList_XemClan_Item_Callback : MonoBehaviour {

    public Text TextChucdanh;
    public Text TextTen;
    public Text TextLV;
    public Image ImgLV;
    public Text TextVip;
    public Text TextHoatdong;
    
    [HideInInspector]
    UserInfo UserInfo;

    [HideInInspector]
    string2 string2;

    void Start()
    {
        TextChucdanh.text = "--";
        TextTen.text = "--";
        TextLV.text = "--";
        //ImgLV.text = "--";
        TextVip.text = "--";
        TextHoatdong.text = "--";
}

    void ScrollCellIndex(int idx)
    {
        this.string2 = CSVR_UIClanList_XemClan.instance.listString_Members[idx];

        int index = CSVR_UIClanList_XemClan.instance.GetIndex(string2.Value,
                CSVR_UIClanList_XemClan.instance.listUserInfo);

        if (index != -1) this.UserInfo = CSVR_UIClanList_XemClan.instance.listUserInfo[index];

        if (UserInfo != null)
        {
            this.TextChucdanh.text = this.string2.Key;
            if (this.UserInfo.DisplayName != null && this.UserInfo.DisplayName != "")
            {
                this.TextTen.text = this.UserInfo.DisplayName;
            } else
            {
                this.TextTen.text = "Không xác định!!";
            }
            this.TextLV.text = this.UserInfo.AccountLevel.ToString();
            ImgLV.sprite = UISpriteManager.instance.GetLevelSprite(this.UserInfo.AccountLevel);
        }
    }

    public void ButtonList_Click()
    {
        if (string2 != null && string2.Value != null && string2.Value != "")
        {
            AddTemplate_XemThongTin();
            AddTemplate_Chat();
        }
    }

    public void AddTemplate_XemThongTin()
    {
        AButton AButton = new AButton();
        AButton.tittle = "THÔNG TIN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIBuddy_ThongTinUserClan.instance.Open();
            CSVR_UIBuddy_ThongTinUserClan.instance.Show(string2.Value);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }

    public void AddTemplate_Chat()
    {
        AButton AButton = new AButton();
        AButton.tittle = "NÓI CHUYỆN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIClanList_XemClan.instance.Close();
            CSVR_UICLanList.instance.Close();
            CSVR_UIChat.instance.Open();
            CSVR_UIChat.instance.InitPrivateChat(string2.Value);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }    
}
