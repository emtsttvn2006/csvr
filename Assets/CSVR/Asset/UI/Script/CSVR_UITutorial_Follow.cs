﻿using UnityEngine;
using UnityEngine.UI;

public class CSVR_UITutorial_Follow : MonoBehaviour {

    public bool isFollow = false;
    public RectTransform RectFollow = null;
    public RectTransform RectTransform = null;
    public Vector3 v3OffSet = Vector3.zero;

    void Awake()
    {
        try
        {
            this.RectTransform = this.GetComponent<RectTransform>();
        } catch
        {

        }

        if (this.RectTransform == null)
        {
            Destroy();
            if (CSVR_UITutorial.instance.DebugError)
                Debug.Log("Không có Rect");
        }

        RepeatFollow();
    }

    public void SetParent(Transform tr)
    {
        this.transform.parent = transform;
    }

    public void SetFollow(RectTransform rect)
    {
        this.RectFollow = rect;
        this.v3OffSet = Vector3.zero;
    }

    public void SetOffSet(Vector3 Vector3)
    {
        this.v3OffSet = Vector3;
    }

    public void RepeatFollow()
    {
        InvokeRepeating("Follow", 0.0f, 0.4f);
    }

    public void Follow()
    {
        if (this.RectTransform != null && RectFollow != null && this.isFollow)
        {
            this.RectTransform.position = RectFollow.position + v3OffSet;
        } 
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }    
}
