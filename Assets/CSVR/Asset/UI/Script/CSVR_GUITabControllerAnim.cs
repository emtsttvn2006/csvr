﻿//using UnityEngine;
//using UnityEngine.UI;
//using System;
//using System.Collections.Generic;
//
//public class CSVR_GUITabControllerAnim : MonoBehaviour {
//	public GameObject inventoryGroup;
//	public int groupCount = 2;
//	[SerializableAttribute]
//	public struct TabInventoryPair {
//		public Toggle MainGun;
//		public Toggle Pistol;
//		public Toggle Mele;
//		public Toggle Grenade;
//		public Toggle Armor;
//	}
//	public Button _mainGun;
//	public Button _pistol;
//	public Button _mele;
//	public Button _grenade;
//	public Button _armor;
//
//	public List<TabInventoryPair> tabInventoryPairs;
//	void Start()
//	{
//		tabInventoryPairs = new List<TabInventoryPair>();
//		for(int i = 0; i < groupCount; i++){
//			TabInventoryPair tab = new TabInventoryPair();
//			tab.MainGun = inventoryGroup.transform.GetChild(i).GetChild(0).GetComponent<Toggle>();
//			tab.Pistol = inventoryGroup.transform.GetChild(i).GetChild(1).GetComponent<Toggle>();
//			tab.Mele = inventoryGroup.transform.GetChild(i).GetChild(2).GetComponent<Toggle>();
//			tab.Grenade = inventoryGroup.transform.GetChild(i).GetChild(3).GetComponent<Toggle>();
//			tab.Armor = inventoryGroup.transform.GetChild(i).GetChild(4).GetComponent<Toggle>();
//			tabInventoryPairs.Add(tab);
//		}
//		AddListener();
//	}
//	void AddListener()
//	{
//		if (tabInventoryPairs != null) {
//			tabInventoryPairs.ForEach(pair => {
//				pair.MainGun.onValueChanged.AddListener((value) =>{
//					tabInventoryPairs.ForEach(p => p.MainGun.isOn = value);
////					if(value == true){
////						CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.touchClip);
////						Debug.Log("goi may lan");
////					}
//				});
//				pair.Pistol.onValueChanged.AddListener((value) => tabInventoryPairs.ForEach(p => p.Pistol.isOn = value));
//				pair.Mele.onValueChanged.AddListener((value) => tabInventoryPairs.ForEach(p => p.Mele.isOn = value));
//				pair.Grenade.onValueChanged.AddListener((value) => tabInventoryPairs.ForEach(p => p.Grenade.isOn = value));
//				pair.Armor.onValueChanged.AddListener((value) => tabInventoryPairs.ForEach(p => p.Armor.isOn = value));
//			});
//			_mainGun.onClick.AddListener(() => {
//				tabInventoryPairs.ForEach(p => p.MainGun.isOn = true);
////				CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.touchClip);
//
//			});
//			_pistol.onClick.AddListener(() => {
//				tabInventoryPairs.ForEach(p => p.Pistol.isOn = true);
////				CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.touchClip);
//
//			});
//			_mele.onClick.AddListener(() => {
//				tabInventoryPairs.ForEach(p => p.Mele.isOn = true);
////				CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.touchClip);
//
//			});
//			_grenade.onClick.AddListener(() => {
//				tabInventoryPairs.ForEach(p => p.Grenade.isOn = true);
////				CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.touchClip);
//
//			});
//			_armor.onClick.AddListener(() => {
//				tabInventoryPairs.ForEach(p => p.Armor.isOn = true);
////				CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.touchClip);
//
//			});
//		}
////		_mainGun.onClick.AddListener(() => tabInventoryPairs.ForEach(p => p.MainGun.isOn = true));
////		_pistol.onClick.AddListener(() => tabInventoryPairs.ForEach(p => p.Pistol.isOn = true));
////		_mele.onClick.AddListener(() => tabInventoryPairs.ForEach(p => p.Mele.isOn = true));
////		_grenade.onClick.AddListener(() => tabInventoryPairs.ForEach(p => p.Grenade.isOn = true));
////		_armor.onClick.AddListener(() => tabInventoryPairs.ForEach(p => p.Armor.isOn = true));
//
//	}
//
//}
