﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISpriteManager : MonoBehaviour {

	public static UISpriteManager instance;

    public Sprite[] newUI;
    public Sprite[] UILevel1;
    public Sprite[] UILevel2;
	public Sprite[] UIVip;
	public Sprite[] UIIconMission;
    public Sprite[] LogoClan;

    private void Awake(){
        if (instance == null)
        {
            instance = this;
        }
    }

    public void UI_OnEnable()
    {
        this.gameObject.SetActive(true);
    }

    public Sprite GetLevelSprite(int Level)
    {
        if (Level >= 0)
        {
            if (Level <= 50) return GetSprite(UILevel1, Level.ToString());
            else return GetSprite(UILevel2, Level.ToString());
        } else
        {
            return GetSprite(UILevel1, "0");
        }
    }

    Sprite GetSprite(Sprite[] sprites, string name)
    {
        Sprite sprite = null;
        for (int i = 0; i < sprites.Length; i++)
        {
            if (sprites[i].name == name)
            {
                sprite = sprites[i];
                break;
            }
        }
        return sprite;
    }

	public Sprite GetVipSprite(string name)
	{
		return GetSprite(UIVip, name);
	}

	public Sprite GetMissionSprite(string name)
	{
		return GetSprite(UIIconMission, name);
	}

    public Sprite GetClanLogoSprite(string name)
    {
        return GetSprite(LogoClan, name);
    }    
}
