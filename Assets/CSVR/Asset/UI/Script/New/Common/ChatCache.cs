﻿using UnityEngine;
using System.Collections;
using Horus.Internal;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus.Json;
using Horus.ClientModels;
using Horus;

#region Định nghĩa mấy cái class con

[System.Serializable]
public class GlobalUser
{
    public string Name { get; set; }
    public string NickName { get; set; }
    public string Avatar { get; set; }
    public string Level { get; set; }
    public string Chucdanh { get; set; }

    public GlobalUser()
    {
        this.Name = string.Empty;
        this.NickName = string.Empty;
        this.Avatar = string.Empty;
        this.Level = string.Empty;
        this.Chucdanh = string.Empty;   
    }

    public GlobalUser(string Name, string NickName, string Avatar, string Level)
    {
        this.Name = Name;
        this.NickName = NickName;
        this.Avatar = Avatar;
        this.Level = Level;        
    }

    public GlobalUser(BanbeUser BanbeUser)
    {
        this.Name = BanbeUser.Name;
        this.NickName = BanbeUser.NickName;
        this.Avatar = BanbeUser.Avatar;
        this.Level = BanbeUser.Level;
    }

    public void Update(GlobalUser GlobalUser)
    {
        this.Name = GlobalUser.Name;
        this.NickName = GlobalUser.NickName;
        this.Avatar = GlobalUser.Avatar;
        this.Level = GlobalUser.Level;
    }

    public void Update(UserInfo UserInfo)
    {
        this.Name = UserInfo.UserName;
        this.Level = UserInfo.AccountLevel.ToString();        
    }
}

[System.Serializable]
public class BanbeUser
{
    public string Name { get; set; }
    public string NickName { get; set; }
    public string Avatar { get; set; }
    public string Level { get; set; }
    public List<string> contents { get; set; }
    public bool isRead { get; set; }

    public BanbeUser()
    {
        this.Name = string.Empty;
        this.NickName = string.Empty;
        this.Avatar = string.Empty;
        this.Level = string.Empty;
        this.contents = new List<string>();
        this.isRead = false;
    }

    public BanbeUser(string Name, string NickName, string Avatar, string Level, List<string> contents, bool isRead)
    {
        this.Name = Name;
        this.NickName = NickName;
        this.Avatar = Avatar;
        this.Level = Level;
        this.contents = contents;
        this.isRead = isRead;
    }

    public void AddContent(string content)
    {
        this.contents.Add(content);
        this.isRead = false;
        if (this.contents.Count > ChatCache.instance.Max_Caches_Lines) contents.RemoveAt(0);
    }

    public void Update(BanbeUser BanbeUser)
    {
        if (BanbeUser.Name != "DNU") this.Name = BanbeUser.Name;
        if (BanbeUser.NickName != "DNU") this.NickName = BanbeUser.NickName;
        if (BanbeUser.Avatar != "DNU") this.Name = BanbeUser.Avatar;
        if (BanbeUser.Level != "DNU") this.Name = BanbeUser.Level;
        this.contents = BanbeUser.contents;
    }

    public void Update(GlobalUser GlobalUser)
    {
        if (GlobalUser.Name != "DNU") this.Name = GlobalUser.Name;
        if (GlobalUser.NickName != "DNU") this.NickName= GlobalUser.NickName;
        if (GlobalUser.Avatar != "DNU") this.Avatar = GlobalUser.Avatar;
        if (GlobalUser.Level != "DNU") this.Level = GlobalUser.Level;
    }
}

/// <summary>
/// AChatCache chứa nội dung để show ra chat
/// </summary>
[System.Serializable]
public class AGlobalCache
{    
    public List<GlobalUser> GlobalUsers { get; set; }    
    public List<string> Contents { get; set; }
    public bool isRead { get; set; }

    public AGlobalCache()
    {
        this.GlobalUsers = new List<GlobalUser>();
        this.Contents = new List<string>();
        this.isRead = false;
    }

    public int GetUserIndex(string Name)
    {
        int index = -1;
        for (int i = 0; i < GlobalUsers.Count; i++)
        {
            if (GlobalUsers[i].Name == Name)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public void AddUser(GlobalUser GlobalUser)
    {
        int index = GetUserIndex(GlobalUser.Name);
        if (index == -1)
        {
            this.GlobalUsers.Add(GlobalUser);
        }
        else
        {
            this.GlobalUsers[index].Update(GlobalUser);
        }
    }    

    public void AddContent(string content)
    {
        this.Contents.Add(content);
        this.isRead = false;
        if (this.Contents.Count > ChatCache.instance.Max_Caches_Lines) Contents.RemoveAt(0);
    }
}

[System.Serializable]
public class ABanbeCache
{
    [SerializeField]
    public List<BanbeUser> BanbeUsers { get; set; }    

    public ABanbeCache()
    {
        this.BanbeUsers = new List<BanbeUser>();        
    }

    public int GetUserIndex(string Name)
    {
        int index = -1;
        for (int i = 0; i < BanbeUsers.Count; i++)
        {
            if (BanbeUsers[i].Name == Name)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public bool isRead()
    {
        bool value = true;
        for (int i = 0; i < BanbeUsers.Count; i++)
        {
            if (!BanbeUsers[i].isRead)
            {
                value = false;
                break;
            }
        }
        return value;
    }

    public void AddUser(BanbeUser BanbeUser)
    {
        int index = GetUserIndex(BanbeUser.Name);
        if (index == -1)
        {
            if (HorusManager.instance.playerUserName != BanbeUser.Name)
            {
                this.BanbeUsers.Add(BanbeUser);                
            }
        }
        else
        {
            this.BanbeUsers[index].Update(BanbeUser);
        }
    }

    public void AddUser(GlobalUser GlobalUser)
    {
        int index = GetUserIndex(GlobalUser.Name);
        if (index == -1)
        {
            if (CSVR_IRCPlayerMessenger.instance.GlobalUser.Name.Remove(0, 1) != GlobalUser.Name)
            {
                this.BanbeUsers.Add(new BanbeUser(GlobalUser.Name, GlobalUser.NickName, GlobalUser.Avatar, GlobalUser.Level, new List<string>(), true));
            }
        } else
        {
            this.BanbeUsers[index].Update(GlobalUser);
        }
    }

    public void AddContent(string Name, string content)
    {
        int index = GetUserIndex(Name);
        this.BanbeUsers[index].AddContent(content);
    }
}
#endregion

public class ChatCache : Singleton<ChatCache>
{
    public int Max_Caches_Lines = 500;

    public AGlobalCache GlobalCaches = new AGlobalCache();
    public ABanbeCache BanbeCaches = new ABanbeCache();
    public AGlobalCache ClanCaches = new AGlobalCache();
    public ABanbeCache PrivateCaches = new ABanbeCache();

    #region UI Chat - Global
    /// <summary>
    /// Liên quan đến chat global cho hết vô đây
    /// </summary>        

    public void AddContent(string sender, string content, string channel)
    {
        int begin = -1;
        int end = -1;
        
        if (content.Contains("Vào!"))
        {
            begin = content.IndexOf("{");
            end = content.IndexOf("}");

            if (begin != -1 && end != -1)
            {
                string jsonstring = content.Substring(begin, end - begin + 1);
                GlobalUser GlobalUser = JsonWrapper.DeserializeObject<GlobalUser>(jsonstring);
                content = content.Remove(begin, end - begin + 1);

                if (GlobalUser != null)
                {
                    if (GlobalUser.Name[0] == 'u')
                    {
                        GlobalUser.Name = GlobalUser.Name.Remove(0, 1);
                        if ((sender == "#global" || channel == "#global") && !GlobalUser.Name.ToLower().Contains("roomhost"))
                        {
                            GlobalCaches.AddUser(GlobalUser);
                            CSVR_UIChat.instance.UpdateGlobalUser();
                        }
                    }
                }
            }
            return;
        }
        
        begin = content.IndexOf("{");
        end = content.IndexOf("}");        

        if (begin != -1 && end != -1)
        {
            string jsonstring = content.Substring(begin, end - begin + 1);
            GlobalUser GlobalUser = JsonWrapper.DeserializeObject<GlobalUser>(jsonstring);
            content = content.Remove(begin, end - begin + 1);

            if (GlobalUser != null)
            {
                if (GlobalUser.Name[0] == 'u')
                {
                    GlobalUser.Name = GlobalUser.Name.Remove(0, 1);
                    if (sender == "#global" || channel == "#global")
                    {
                        GlobalCaches.AddUser(GlobalUser);
                        CSVR_UIChat.instance.UpdateGlobalUser();
                        GlobalCaches.AddContent(GlobalUser.Name + " : " + content);
						HomeScreen.instance.TextChat1Line.text = GlobalUser.Name + " : " + content;
                        CSVR_UIChat.instance.UpdateGlobalChatContent();

                        CSVR_UIChat.instance.MoveAScrollContent(CSVR_UIChat.instance.scrollContentGlobal.transform.GetChild(0).GetComponent<RectTransform>(), GlobalUser.Name + " : " + content);
                    }
                    else if (sender == "u" + GlobalUser.Name && channel == "NOCHANNEL")
                    {
                        if (BanbeCaches.GetUserIndex(GlobalUser.Name) == -1)
                        {
                            PrivateCaches.AddUser(GlobalUser);
                            CSVR_UIChat.instance.UpdatePrivateUser();
                            PrivateCaches.AddContent(GlobalUser.Name, content);
                            CSVR_UIChat.instance.UpdatePrivateChatContent();

                            CSVR_UIChat.instance.MoveAScrollContent(CSVR_UIChat.instance.scrollContentPrivate.transform.GetChild(0).GetComponent<RectTransform>(), content);
                        }
                        else
                        {
                            BanbeCaches.AddUser(GlobalUser);
                            CSVR_UIChat.instance.UpdateBanbeUser();
                            BanbeCaches.AddContent(GlobalUser.Name, content);
							HomeScreen.instance.TextChat1Line.text = GlobalUser.Name + " : " + content;
                            CSVR_UIChat.instance.UpdateBanbeChatContent();

                            CSVR_UIChat.instance.MoveAScrollContent(CSVR_UIChat.instance.scrollContentBanbe.transform.GetChild(0).GetComponent<RectTransform>(), content);
                        }
                    }
                    else if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "" && channel == "#" + HorusManager.instance.Clan.clanName)
                    {
                        ClanCaches.AddUser(GlobalUser);
                        CSVR_UIChat.instance.UpdateClanUser();
                        ClanCaches.AddContent(GlobalUser.Name + " : " + content);
                        CSVR_UIChat.instance.UpdateClanChatContent();
                        CSVR_UIChat.instance.MoveAScrollContent(CSVR_UIChat.instance.scrollContentClan.transform.GetChild(0).GetComponent<RectTransform>(), content);
                    }
                    else if (content.Contains("#private"))
                    {
                        content.Remove(content.IndexOf("#private"), 8);
                    }
                }
                else
                {
                    if (DebugError) Debug.Log("Tên User không có u đầu");
                }
            }
            else
            {
                if (DebugError) Debug.Log("Thông tin User không đúng mẫu " + jsonstring);
            }
        }
        else
        {
            if (DebugError) Debug.Log("Nhận được tin Chat không có thông tin User. " + sender + ":" + content);
        }
    }

    public void UpdateClanUserInfo(ClanResult ClanResult)
    {
        ClanCaches.GlobalUsers.Clear();
        List<string> totalmember = new List<string>();

        if (ClanResult.chief != HorusManager.instance.playerUserName)
        {
            GlobalUser GlobalUser = new GlobalUser();
            GlobalUser.Name = ClanResult.chief;
            GlobalUser.Chucdanh = "TRƯỞNG CLAN";
            ClanCaches.AddUser(GlobalUser);
            totalmember.Add(GlobalUser.Name);
        }

        for (int i = 0; i < ClanResult.Vice.Count; i++)
        {
            if (ClanResult.Vice[i] != HorusManager.instance.playerUserName)
            {
                GlobalUser GlobalUser = new GlobalUser();
                GlobalUser.Name = ClanResult.Vice[i];
                GlobalUser.Chucdanh = "PHÓ CLAN";
                ClanCaches.AddUser(GlobalUser);
                totalmember.Add(GlobalUser.Name);
            }
        }

        for (int i = 0; i < ClanResult.Members.Count; i++)
        {
            if (ClanResult.Members[i] != HorusManager.instance.playerUserName)
            {
                GlobalUser GlobalUser = new GlobalUser();
                GlobalUser.Name = ClanResult.Members[i];
                GlobalUser.Chucdanh = "THÀNH VIÊN";
                ClanCaches.AddUser(GlobalUser);
                totalmember.Add(GlobalUser.Name);
            }
        }

        for (int i = 0; i < ClanResult.Joining.Count; i++)
        {
            if (ClanResult.Joining[i] != HorusManager.instance.playerUserName)
            {
                GlobalUser GlobalUser = new GlobalUser();
                GlobalUser.Name = ClanResult.Members[i];
                GlobalUser.Chucdanh = "ĐANG XIN";
                ClanCaches.AddUser(GlobalUser);
                totalmember.Add(GlobalUser.Name);
            }
        }

        for (int i = 0; i < ClanResult.Invite.Count; i++)
        {
            if (ClanResult.Invite[i] != HorusManager.instance.playerUserName)
            {
                GlobalUser GlobalUser = new GlobalUser();
                GlobalUser.Name = ClanResult.Members[i];
                GlobalUser.Chucdanh = "ĐƯỢC MỜI";
                ClanCaches.AddUser(GlobalUser);
                totalmember.Add(GlobalUser.Name);
            }
        }

        UpdateClanMemberInfo(totalmember);        
    }    

    private void UpdateClanMemberInfo(List<string> list)
    {
        if (HorusManager.instance.Clan != null && HorusManager.instance.Clan.clanName != "" && list.Count > 0)
        {
            GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();
            GetUserInfoRequest.UserIds = list;

            CSVR_UIWaitingIcon.instance.Open();

            HorusClientAPI.GetUserInfo(GetUserInfoRequest,
                (result) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    for (int i = 0; i < result.users.Count; i++)
                    {
                        int index = ClanCaches.GlobalUsers.FindIndex(x => x.Name == result.users[i].UserName);
                        ClanCaches.GlobalUsers[index].Update(result.users[i]);
                    }

                    CSVR_UIChat.instance.UpdateClanUser();
                },
                (error) =>
                {
                    CSVR_UIWaitingIcon.instance.Close();

                    ErrorView.instance.ShowPopupError("Không tìm thấy Thông tin Member này");
                }, null);
        }
    }
    #endregion

}
