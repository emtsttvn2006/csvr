﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Horus.Json;
using Horus.Internal;

[System.Serializable]
public class SingletonClass
{
    [SerializeField] public string Name { get; set; }
    [SerializeField] public string AssetPath { get; set; }
    [SerializeField] public string Layer { get; set; }
}

[System.Serializable]
public class SingletonClassList
{
    [SerializeField] public List<SingletonClass> SingletonClasses { get; set; }
}

public class SingletonResources : SingletonMonoBehaviour<SingletonResources>
{
    public SingletonClassList list;
    public TextAsset TextAsset;    

    void Start()
    {
        Init();
    }

    void Init()
    {
        TextAsset = (TextAsset)Resources.Load("SingletonList");
        list = JsonWrapper.DeserializeObject<SingletonClassList>(TextAsset.text);                
    }    

    int GetIndexClass(string ClassName)
    {
        if (list == null) Init();
        int index = -1;
        for (int i = 0; i < list.SingletonClasses.Count; i++)
        {
            if (list.SingletonClasses[i].Name == ClassName)
            {
                index = i;
                break;
            }
        }
        if (index == -1) Debug.Log("Kiểm tra lại tên Class " + ClassName);
        return index;
    }

    public string GetAssetPath(string ClassName)
    {
        int index = GetIndexClass(ClassName);
        return (list.SingletonClasses[index].AssetPath);
    }

    public string GetLayer(string ClassName)
    {
        int index = GetIndexClass(ClassName);
        return (list.SingletonClasses[index].Layer);
    }

    void OnApplicationQuit()
    {
        Debug.Log("Application ending after " + Time.time + " seconds");
        //Resources.UnloadAsset(TextAsset);
        Resources.UnloadUnusedAssets();
    }
}
