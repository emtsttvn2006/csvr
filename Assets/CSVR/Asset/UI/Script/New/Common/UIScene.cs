﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScene : MonoBehaviour {
	public UIScene instance;

    public Sprite[] newUI;
    public Sprite[] UILevel1;
    public Sprite[] UILevel2;

	private void Awake(){
		instance = this;
	}

    public Sprite GetLevelSprite(int Level)
    {
        if (Level <= 50) return GetSprite(UILevel1, Level.ToString());
        else return GetSprite(UILevel2, Level.ToString());
    }

    Sprite GetSprite(Sprite[] sprites, string name)
    {
        Sprite sprite = null;
        for (int i = 0; i < sprites.Length; i++)
        {
            if (sprites[i].name == name)
            {
                sprite = sprites[i];
                break;
            }
        }
        return sprite;
    }

}
