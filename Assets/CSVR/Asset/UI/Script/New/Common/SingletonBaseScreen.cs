﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public class SingletonBaseScreen<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    private static object _lock = new object();

    private static GameObject singleton;    

    public static T instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                    "' already destroyed on application quit." +
                    " Won't create again - returning null.");
                return null;
            }

            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogError("[Singleton] Something went really wrong " +
                            " - there should never be more than 1 singleton!" +
                            " Reopening the scene might fix it.");
                        return _instance;
                    }

                    if (_instance == null)
                    {
                        singleton = (GameObject)Instantiate((GameObject)Resources.Load(SingletonResources.instance.GetAssetPath(typeof(T).Name)));
                        _instance = singleton.GetComponent<T>();
                        singleton.name = typeof(T).Name;
						#if !VIRTUAL_ROOM
                        Transform Parent = GameObject.Find("Canvas").transform.FindChild(SingletonResources.instance.GetLayer(typeof(T).Name));
                        if (Parent == null)
                        {
                            Debug.Log("Set lại layer !!!");
                        } else
                        {
                            singleton.transform.SetParent(Parent);
                            singleton.GetComponent<RectTransform>().localScale = Vector3.one;
                            RectTransform rectTransform = singleton.GetComponent<RectTransform>();
//                            rectTransform.anchorMin = new Vector2(0, 0);
//                            rectTransform.anchorMax = new Vector2(1, 1);
                            rectTransform.offsetMin = Vector3.zero;
                            rectTransform.offsetMax = Vector3.zero;

                        }
						#endif


                        //DontDestroyOnLoad(singleton);

                        //Debug.Log("[Singleton] An instance of " + typeof(T) +" is needed in the scene, so '" + singleton.name +"' was created with DontDestroyOnLoad.");
                    }
                    else
                    {
                        //Debug.Log("[Singleton] Using instance already created: " + _instance.gameObject.name);
                    }
                }

                return _instance;
            }
        }
    }

    public Button ButtonClose;    

    public void SetButton(Button Button, UnityAction UnityAction)
    {
        Button.onClick.RemoveAllListeners();
        Button.onClick.AddListener(UnityAction);
    }

    public virtual void SetBackButton(UnityAction UnityAction)
    {
        if (this.ButtonClose != null)
        {
            SetButton(this.ButtonClose, UnityAction);
        }
        else
        {
            if (DebugError) Debug.Log("Chưa set nút button");
        }
    }

    public bool DebugError = false;

    private static bool applicationIsQuitting = false;

    public virtual void Awake()
    {
        //Open();
		this.gameObject.SetActive(false);
    }    

    public virtual void OnEnable()
    {
       // Open();
    }
        
    public virtual void Disable()
    {
        this.transform.localScale = Vector3.zero;
    }

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public void OnDestroy()
	{
		try {
			Resources.UnloadUnusedAssets ();
		} catch (UnityException e) {
			if (DebugError) Debug.Log ("Lỗi Destroy Object" + e.ToString ());
		}
		_instance = null;
		//chi dung khi dont destroyonload
        //applicationIsQuitting = true;
    }	

    #region RectTransform
    public void SetParent(GameObject go, Transform transform)
    {
        go.transform.SetParent(transform);
        go.GetComponent<RectTransform>().localScale = Vector3.one;
        RectTransform rectTransform = go.GetComponent<RectTransform>();
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(1, 1);
        rectTransform.offsetMin = Vector3.zero;
        rectTransform.offsetMax = Vector3.zero;
    }
    #endregion 

    #region Animator
    /// <summary>
    /// Mặc định các Base Screen đều phải có Animator theo dạng (Entry -> Idle -> Open -> Close)
    /// </summary>
    public virtual void Open()
    {        
		this.gameObject.SetActive (true);
        Animator animator = GetComponent<Animator>();
//        if (HasParameter("ScaleZero", animator))
//        {
//            animator.SetTrigger("ResetIdle");
//        }

        if (animator != null)
        {
            if (Check_IS_Animator_BaseScreen(animator))
            {
				if (animator.GetBool ("Open") == false) {
					animator.SetBool ("Idle", false);
					animator.SetBool ("Open", true);
				}
            }
        }
    }

    public List<SingletonBaseScreen<T>> UINotAtSameList;

    public virtual void InitLayerRequirement()
    {
        
    }

    public virtual void Close()
    {		
        Animator animator = GetComponent<Animator>();
        if (animator != null)
        {
            if (Check_IS_Animator_BaseScreen(animator))
            {
				if (animator.GetBool ("Open") == true) {
					animator.SetBool ("Idle", false);
					animator.SetBool ("Open", false);
				}
            }
        }
		this.gameObject.SetActive (false);
    }


	public virtual void CloseAndOpen()
	{
		Close ();
		Invoke ("Open", 0.5f);
	}
    public virtual void Idle()
    {
        Animator animator = GetComponent<Animator>();
        if (animator != null)
        {
            if (Check_IS_Animator_BaseScreen(animator))
            {
                animator.SetBool("Idle", true);                
            }
        }        
    }

    public virtual void OpenChildrenToo()
    {
        Open();
        foreach (Animator animator in GetComponentsInChildren<Animator>())
        {            
            if (Check_IS_Animator_BaseScreen(animator))
            {
                animator.SetBool("Idle", false);
                animator.SetBool("Open", true);
            }            
        }
    }

    public virtual void CloseChildrenToo()
    {
        Close();
        foreach (Animator animator in GetComponentsInChildren<Animator>())
        {            
            if (animator != null)
            {
                if (Check_IS_Animator_BaseScreen(animator))
                {
                    animator.SetBool("Idle", false);
                    animator.SetBool("Open", false);
                }
            }
        }
    }

    public virtual void OpenAnAnimator(Animator animator)
    {
        if (Check_IS_Animator_BaseScreen(animator))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Open", true);
        }
    }

    public virtual void CloseAnAnimator(Animator animator)
    {
        if (Check_IS_Animator_BaseScreen(animator))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Open", false);
        }
    }

    public virtual void DisableAnAnimator(Animator animator)
    {
        animator.transform.localScale = Vector3.zero;
    }

    public virtual void ScaleZero()
    {
        Animator animator = GetComponent<Animator>();
        if (HasParameter("ScaleZero", animator))
        {
            animator.SetTrigger("ScaleZero");
        }
    }

    public bool Check_IS_Animator_BaseScreen(Animator animator)
    {
        if (!HasParameter("Idle", animator) || !HasParameter("Open", animator)) { 
            if (DebugError) Debug.Log("Không đúng chuẩn BaseScreen Animator. Phải có đầy đủ Idle, Open");
            return false;
        } else return true;
    }    

    public bool HasParameter(string paramName, Animator animator)
    {
        bool result = false;
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.name == paramName)
            {
                result = true;
                break;
            }
        }
        if (!result)
            if (DebugError)
                Debug.Log(animator.name + " không có parameter " + paramName);
        return result;
    }

    public void GotoBeforeLayer(string LayerName)
    {
        this.transform.SetSiblingIndex(this.transform.parent.FindChild(LayerName).GetSiblingIndex() - 1);
    }

    public void GotoIndexInLayer(int index)
    {
        this.transform.SetSiblingIndex(index);
    }

    public void GotoLastInLayer()
    {
        this.transform.SetSiblingIndex(this.transform.parent.childCount - 1);
    }
    #endregion
}