﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using UnityEditor;

[RequireComponent(typeof(Image))]
[ExecuteInEditMode]
[DisallowMultipleComponent]
public class HorusImage : MonoBehaviour {

    public bool Editing = false;
    public Sprite[] sprites;
    public string sprite_child;    

    void Awake()
    {        
        UpdateImage();
    }

    void Start()
    {
        this.enabled = false;
    }
    
    public void LoadImage(string name)
    {
        for (int i = 0; i < sprites.Length; i++)
        {
            if (sprites[i].name == name)
            {
                GetComponent<Image>().sprite = sprites[i];
                break;
            }
        }
        
    }

    public void UpdateImage()
    {
        if (GetComponent<Image>() && GetComponent<Image>().sprite) sprite_child = GetComponent<Image>().sprite.name;
    }

    void Update()
    {
        //if (Editing) { Debug.Log("cs");}
    }
}