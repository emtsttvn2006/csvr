﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;

public class UpdateName : MonoBehaviour {
	public InputField name;

	public void UI_OnUpdateNameClick(){
		//music
		if (name.text.Length >= 6) {
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);

			UpdateUserInfoRequest request = new UpdateUserInfoRequest();
			request.UserInfo = new Dictionary<string, string>();
			request.UserInfo.Add("DisplayName", name.text);

			HorusClientAPI.UpdateUserInfo(request,
				(result) => {
					AccountManager.instance.displayName = name.text;
					OnUpdateNameCompleted();
				},
				(error) => {

				}
			);
		}
	}
	void OnUpdateNameCompleted()
	{
		HomeScreen.instance.UpdateNameAfterUpdate ();
		Destroy (this.gameObject);
	}
}
