﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;

public class ReferralCode : MonoBehaviour {

	public static ReferralCode instance = null;
	private void Awake(){instance = this;}
	private void OnDestroy(){instance = null;}

	// UI 
	public Button redeem;
	public InputField inputReferralCode;
	public InputField inputGiftCode;
	//public Text myReferralCode;
	//public Text referredBy;
	public Transform choiseRewardGroup;
	public Transform redeemGroup;
	public Transform giftCodeGroup;
	
	// INSPECTOR TWEAKABLES
	public string badgeName = "referralBadge";
	
	// PLAYFAB DATA
	List<ItemInstance> inventory = new List<ItemInstance>();
	
	private bool wasReferralBageFound; 
	
	// Use this for initialization
	void Start () 
	{
		LockUI();

		GetInventory();	


	}
	
	public void OnRedeemClicked()
	{
		RedeemReferralCode();
	}


	
	public void ShowGiftCodeGroup()
	{

		this.giftCodeGroup.gameObject.SetActive (true);
		this.redeemGroup.gameObject.SetActive (false);
		this.choiseRewardGroup.gameObject.SetActive (false);

	}

	public void ShowRedeemGroup()
	{
		this.giftCodeGroup.gameObject.SetActive(false);
		this.redeemGroup.gameObject.SetActive(true);
		this.choiseRewardGroup.gameObject.SetActive(false);
	}

	public void ShowChoiseRewardGroup()
	{
		if (!wasReferralBageFound) {
			this.giftCodeGroup.gameObject.SetActive(false);
			this.redeemGroup.gameObject.SetActive(false);
			this.choiseRewardGroup.gameObject.SetActive(true);
		} else {
			this.gameObject.SetActive (false);
		}
	}

	void UnlockUI()
	{
		SearchForReferralBadge();
		if(this.wasReferralBageFound == true)
		{
			ShowGiftCodeGroup();
		}
		else
		{
			ShowChoiseRewardGroup();
		}
		this.redeem.interactable = true;

		ErrorView.instance.Loading (false);
	}
	
	void LockUI()
	{

		this.redeem.interactable = false;

		ErrorView.instance.Loading (true);
	}
	
	void GetInventory()
	{	
		//Debug.Log("Getting the player inventory...");
		GetUserInventoryRequest request = new GetUserInventoryRequest();
		PlayFabClientAPI.GetUserInventory(request, OnGetInventoryCallback, OnApiCallError);
	}
	
	void OnGetInventoryCallback(GetUserInventoryResult result) 
	{
		//Debug.Log(string.Format("Inventory retrieved. You have {0} items.", result.Inventory.Count));
		this.inventory = result.Inventory;
		
		int gdBalance;
		result.VirtualCurrency.TryGetValue("GD", out gdBalance);
		//Debug.Log(string.Format("Tài khoản có {0} vàng.", gdBalance));
		UnlockUI();
	}
	
	void SearchForReferralBadge()
	{
		foreach(var itemInstance in this.inventory)
		{
			if(itemInstance.ItemId == this.badgeName)
			{
				this.wasReferralBageFound = true;

				//this.referredBy.text = "Mã đã giới thiệu: \n" +itemInstance.Annotation;
			}
		}
	}
	
	void RedeemReferralCode()
	{	
		//Debug.Log("REDEEMING...");
		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest() { 
			FunctionName = "RedeemReferral", 
			FunctionParameter = new { 
				referralCode = this.inputReferralCode.text 
			}
		};
		PlayFabClientAPI.ExecuteCloudScript(request, OnRedeemReferralCodeCallback, OnApiCallError);
	}
	
	void OnRedeemReferralCodeCallback(ExecuteCloudScriptResult result) 
	{
		// output any errors that happend within cloud script
		if(result.Error != null)
		{
			ErrorView.instance.ShowPopupError("Mã giới thiệu sai, xin vui lòng nhập mã khác.");
			return;
		}
		ErrorView.instance.ShowPopupError("Bạn nhận được 100 vàng.");
		AccountManager.instance.gameDollarAmount += 100;
		HomeScreen.instance.UpdateVirtualCurrency();
		this.wasReferralBageFound = true;
		//this.referredBy.text = "Mã đã giới thiệu: \n" +this.inputReferralCode.text;
		UnlockUI ();
		/*
		List<ItemInstance> grantedItems = (List<ItemInstance>)result.FunctionResult;

		if(grantedItems != null)
		{
			Debug.Log("SUCCESS!...\nYou Just Recieved:");
			string output = string.Empty;
			foreach(var itemInstance in grantedItems)
			{			
				output += string.Format("\t {0} \n", itemInstance.DisplayName); 
			}
			
			this.inventory.AddRange(grantedItems);
			SearchForReferralBadge();
			ShowReferredGroup();
			Debug.Log(output);
			foreach(var statement in result.Logs)
			{
				Debug.Log(statement.Message);
			}
		}
		else
		{
			Debug.LogError("An error occured when attemtpting to deserialize the granted items.");
		}
		*/
	}
	
	void OnApiCallError(PlayFabError err)
	{
		string http = string.Format("HTTP:{0}", err.HttpCode);
		string message = string.Format("ERROR:{0} -- {1}", err.Error, err.ErrorMessage);
		string details = string.Empty;
		
		if(err.ErrorDetails != null)
		{
			foreach(var detail in err.ErrorDetails)
			{
				details += string.Format("{0} \n", detail.ToString());
			}
		}
		
		Debug.LogError(string.Format("{0}\n {1}\n {2}\n", http, message, details));
	}

	#region RedeemCoupon



	public void OnGiftCode(){
		PlayFabClientAPI.RedeemCoupon(new RedeemCouponRequest() {CouponCode = this.inputGiftCode.text}, OnGiftCodeResult, OnGiftCodeError);
	}
	void OnGiftCodeResult(RedeemCouponResult result){
		//AccountManager.instance.inventory.Add (result.GrantedItems[0]);
		try{
			ItemInstance newItem = result.GrantedItems[0];
//			CSVR_UIPopupConfirmShop.instance.ShowPopUpGrandItem(newItem);
			PlayFabManager.instance.LoadInventoryItem (newItem);
			this.gameObject.SetActive (false);
		}catch{
			ErrorView.instance.ShowPopupError ("Code đã hết hạn.");
		}
		//Debug.Log("RedeemCouponResult: " + result);

	}

	void OnGiftCodeError(PlayFabError error)
	{
		string errorMessage = string.Empty;
		switch (error.Error) {
		case PlayFabErrorCode.InvalidParams:
			errorMessage = "Lỗi: Code đã được sử dụng. Vui lòng nhập code khác.";
			break;
		case PlayFabErrorCode.CouponCodeNotFound:
			errorMessage = "Lỗi: Code không tồn tại. Vui lòng nhập code khác.";
			break;
		}

		ErrorView.instance.ShowPopupError (errorMessage);
		//loginScreen
		//errorView.ShowPopupError ("Error: " + error.ErrorMessage);
		//Debug.Log("Login error: " + error.Error);
	}
	#endregion

	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
	public void UI_OnDisable(){
		this.gameObject.SetActive (false);
	}
}
