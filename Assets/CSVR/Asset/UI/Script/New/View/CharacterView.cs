﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;

public class CharacterView : SingletonBaseScreen<CharacterView> {
	public string idWeapon = "";

	public GameObject character_Manager;
    public GameObject police, Terrorist;
	public ControlWeapon controlWeaponCharacter;

	public ItemInstance weaponDefault;
	public ItemInstance[,] itemsGroup = new ItemInstance[CSVR_GameSetting.bagCount, 3];
    
	void Start(){
		itemsGroup = new ItemInstance[CSVR_GameSetting.bagCount, 3];
		GetDefaultItem ();
	}

	public override void Open()
	{
		base.Open();

		if(weaponDefault != null)
			GetIDGunDefaultInventory (weaponDefault);
	}

	public void GetDefaultItem()
	{

		itemsGroup = new ItemInstance[CSVR_GameSetting.bagCount, 3];
		for (int i = 0; i < AccountManager.instance.inventory.Count; i++) {
			//kiem tra key lat bai
			if (AccountManager.instance.inventory [i].CatalogVersion == GameConstants.VNITEMCATALOG_Key) {

				AccountManager.instance.keyCards.Add (AccountManager.instance.inventory [i].ItemId);
			}
			if (AccountManager.instance.inventory [i].EquipGroup == GameConstants.VNITEMCHARACTER_Equip) {
				SwitchCharacter(AccountManager.instance.inventory [i].ItemIdModel);
			}
			if (!string.IsNullOrEmpty (AccountManager.instance.inventory[i].EquipGroup)) {
				
				string[] equip = AccountManager.instance.inventory[i].EquipGroup.Split ("." [0]);
				if (equip.Length >= 2) {
					//Yo.JsonLog ("equip", equip);
					if (int.Parse (equip [1]) < 3) {
						itemsGroup [int.Parse (equip [0]) - 1, int.Parse (equip [1]) - 1] = AccountManager.instance.inventory [i];
					}
				}
			}
		}

		GetIDGunInventory ();

	}


	public void SwitchCharacter(string characterId){
		
		for (int i = 0; i < character_Manager.transform.childCount; i++) {
			if (character_Manager.transform.GetChild (i).name == characterId) {
				bool characterActive = true;
				CSVR_GameSetting.currentPlayerTypeName = characterId;
				character_Manager.transform.GetChild (i).gameObject.SetActive (true);
				controlWeaponCharacter = character_Manager.transform.GetChild (i).GetComponent<ControlWeapon> ();

				if (police.gameObject.activeSelf)
					characterActive = true;
				else
					characterActive = false;
				
				police = controlWeaponCharacter.transform.FindChild ("Police").transform.gameObject;
				police.gameObject.SetActive (characterActive);

				Terrorist = controlWeaponCharacter.transform.FindChild ("Terrorist").transform.gameObject;
				Terrorist.gameObject.SetActive (!characterActive);
				
			} else {
				character_Manager.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
	}


	public void GetIDGunDefaultName(string id){
		
		idWeapon = id;
		controlWeaponCharacter.SetWeapon (id);
		controlWeaponCharacter.SetStateAnim (id);
	}

	public void GetIDGunName(string id){
		controlWeaponCharacter.SetWeapon(id);
		controlWeaponCharacter.SetStateAnim(id);
	}


	public void UpdateIDGunDefault(ItemInstance item){
		if (weaponDefault != null && item._id == weaponDefault._id) {
			weaponDefault = item;
			controlWeaponCharacter.SetWeapon (item);
		}
	}
	public void GetIDGunDefaultInventory(ItemInstance item){
		
		weaponDefault = item;
		controlWeaponCharacter.SetWeapon (item);
	}
	public void GetIDGunInventory(ItemInstance item){

		controlWeaponCharacter.SetWeapon (item);
	}
	public void GetIDGunInventory(){
		bool isEquip = false;
		for (int i = 0; i < CSVR_GameSetting.bagCount; i++) {
			for (int j = 0; j < 3; j++) {
				if (i == CSVR_GameSetting.bagStartIndex && itemsGroup[i,j] != null)
				{
					GetIDGunDefaultInventory(itemsGroup[i,j]);
					isEquip = true;
					break;
				}
			}
		}
		if (!isEquip) {
			GetIDGunDefaultName (GameConstants.VNITEMDEFAULT_Knife);
			isEquip = true;
		}
	}
	public void GetIDGunInShop(string id){
		controlWeaponCharacter.SetWeapon(id);
		controlWeaponCharacter.SetStateAnim(id);
	}

    public void ShowModePolice()
    {
        police.SetActive(true);
        Terrorist.SetActive(false);
    }

    public void ShowModeTerrorist()
    {
        police.SetActive(false);
        Terrorist.SetActive(true);
    }
}
