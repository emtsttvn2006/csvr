﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CodeTest : MonoBehaviour {
	public static CodeTest instance = null;
	void Awake(){
		instance = this;

	}
	public InputField code;

	public bool isValidate = false;

	public void CheckCode(){
		if (isValidate) {
			gameObject.SetActive (false);
		} else {
			ErrorView.instance.Loading(false);
			gameObject.SetActive (true);
		}
	}
	public void UI_OnUpdateNameClick(){

		if(code.text == "HiHiHaHaHoHo"){
			OnUpdateCodeCompleted();
		}else{
			ErrorView.instance.Loading(true);
			//PlayFabManager.instance.RedeemCoupon (code.text, OnUpdateCodeCompleted);
		}
	}
	void OnUpdateCodeCompleted()
	{
		this.gameObject.SetActive(false);
		ErrorView.instance.Loading(false);
		AccountManager.instance.OnUpdateCodeCompleted ();
	}
}
