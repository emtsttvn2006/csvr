﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Horus.ClientModels;

public class CSVR_UIRewardItem : SingletonBaseScreen<CSVR_UIRewardItem> {
	[SerializeField] private GameObject[] itemReward;
	[SerializeField] private Image[] itemRewardIcon;
	[SerializeField] private Text[] itemRewardName;
	[SerializeField] private GameObject btn_Close;


	public void ResetListItemReward(){
		for (int i = 0; i < itemReward.Length; i++) {
			itemReward [i].gameObject.SetActive (false);
		}

	}

	public void RewardItem(int i,ItemInstance item){
		StartCoroutine(WaitAndPrint(i));

		itemRewardIcon[i].sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemIdModel);
		itemRewardIcon [i].SetNativeSize ();

		if (item.ItemId == GameConstants.VNITEMID_Coin) {
			itemRewardIcon [i].transform.localScale = Vector3.one;
			itemRewardName[i].text = ((int)item.RemainingUses - AccountManager.instance.gameCoinAmount) + "(vàng)";
			AccountManager.instance.gameCoinAmount = (int)item.RemainingUses;
		}else if (item.ItemId == GameConstants.VNITEMID_Gold) {
			itemRewardIcon [i].transform.localScale = Vector3.one;
			itemRewardName[i].text = ((int)item.RemainingUses - AccountManager.instance.gameDollarAmount) + "(kim cương)";
			AccountManager.instance.gameDollarAmount = (int)item.RemainingUses;
		} else {
			itemRewardIcon [i].transform.localScale = new Vector3(0.4f,0.4f,0.4f);

			if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_3Day)) {
				itemRewardName[i].text = item.DisplayName + "(03Ngày)";
			} 
			else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_7Day)) {
				itemRewardName[i].text = item.DisplayName + "(7Ngày)";
			}
			else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_30Day)) {
				itemRewardName[i].text = item.DisplayName + "(30Ngày)";
			} 
			else {
				itemRewardName[i].text = item.DisplayName + "(Vĩnh Viễn)";
			}

			AccountManager.instance.itemsNew.Add (item);
			AccountManager.instance.inventory.Add (item);

			CSVR_UIInventory.instance.InstanceItemPurchase (item);
			CSVR_UIInventory.instance.ShowNotificationPurchase(item,true);
		}
	}
	IEnumerator WaitAndPrint(int index) {
		float wai = (float)((index * 0.5) + 0.5);
		yield return new WaitForSeconds(wai);
		itemReward [index].gameObject.SetActive (true);
	}

	public void UI_OpenCSVR_UIRewardItem_Click(){
		btn_Close.SetActive (true);
	}
	public void UI_CloseCSVR_UIRewardItem_Click(){
		this.Close ();
		btn_Close.SetActive (false);
	}
}
