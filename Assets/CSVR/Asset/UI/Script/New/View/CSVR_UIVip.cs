﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;

public class CSVR_UIVip : SingletonBaseScreen<CSVR_UIVip> {

	[SerializeField] private Image CurrVip_icon;
	[SerializeField] private Image CurrVip_level;
	[SerializeField] private Image Process_Img;
	[SerializeField] private Text Process_Txt;
	[SerializeField] private Text RequireVip;
	[SerializeField] private Image NextVip_icon;
	[SerializeField] private Image NextVip_level;
	[Space(5)]
	[SerializeField] private Image CurrentVip_icon;
	[SerializeField] private Image CurrentVip_level;
	[SerializeField] private Text CurrentVip_exp_percent;
	[SerializeField] private Text CurrentVip_coin_percent;
	[SerializeField] private Text CurrentVip_gold_percent;

	private List<RoleInfo> role;
	private ItemSpecific gameMoneyAmount;
	private string[] roleName;

	void Awake(){
		role = new List<RoleInfo> ();
		gameMoneyAmount = AccountManager.instance.gameMoneyAmount;
		roleName = AccountManager.instance.roles;
		GetRoleVip ();
	}

	byte ErrorGetGetRoleVipCount = CSVR_GameSetting.errorCountCallBack;
	private void GetRoleVip()
	{
		HorusClientAPI.GetRoleList(
			new RoleListRequest(){
				type = "vip",
				listBy = "roleId",
				order = "asc",
				page = "0"
			}, 
			(result) => {
				//Debug.Log(Horus.Json.JsonWrapper.SerializeObject (result, Horus.Internal.HorusUtil.ApiSerializerStrategy));
				role = result.role;
				HUD_ShowNextVip_Run();
				HUD_ShowCurrentVip_Run();
			},
			(error) => {
				//Debug.Log(Horus.Json.JsonWrapper.SerializeObject (error, Horus.Internal.HorusUtil.ApiSerializerStrategy));
				if(ErrorGetGetRoleVipCount > 0){
					ErrorGetGetRoleVipCount--;
					Invoke ("GetRoleVip", 0.5f);
				}
			}
		);
	}
	private void HUD_ShowNextVip_Run()
	{
		int level = roleName.Length - 1;
		int nextLevel = roleName.Length;
		float currentMoneyConvert = gameMoneyAmount.money / 1000;
		float nextMoneyConvert = float.Parse( role [nextLevel].required.charge["VND"].ToString()) / 1000;

		if (level >= 1) {
			if (level < 6) {
				CurrVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("1_5");
			} else if (level >= 6 && level < 11) {
				CurrVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("2_10");
			} else if (level >= 11 && level < 16) {
				CurrVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("3_15");
			} else {
				CurrVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("4_20");
			}
			CurrVip_level.gameObject.SetActive (true);
			CurrVip_level.sprite = UISpriteManager.instance.GetVipSprite (level.ToString ());
		}

		Process_Img.fillAmount = currentMoneyConvert /nextMoneyConvert;
		Process_Txt.text = currentMoneyConvert + "/" + nextMoneyConvert;
		RequireVip.text = "Bạn cần nạp thêm "+(nextMoneyConvert-currentMoneyConvert)+" kim cương để lên Vip"+nextLevel;

		if (nextLevel > 1) {
			if (nextLevel < 6) {
				NextVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("1_5");
			} else if (nextLevel >= 6 && nextLevel < 11) {
				NextVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("2_10");
			} else if (nextLevel >= 11 && nextLevel < 16) {
				NextVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("3_15");
			} else {
				NextVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("4_20");
			}
			NextVip_level.gameObject.SetActive (true);
			NextVip_level.sprite = UISpriteManager.instance.GetVipSprite (nextLevel.ToString ());
		}

	}
	private void HUD_ShowCurrentVip_Run()
	{
		int lenght = roleName.Length - 1;
		int exp_percent_value = 0;
		int coin_percent_value = 0;
		int gold_percent_value = 0;

		if (lenght < 1)
			return;
		
		if (lenght < 6) {
			CurrentVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("1_5");
		}else if (lenght >= 6 && lenght < 11 ) {
			CurrentVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("2_10");
		}else if (lenght >= 11 && lenght < 16 ) {
			CurrentVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("3_15");
		}else{
			CurrentVip_icon.sprite = UISpriteManager.instance.GetVipSprite ("4_20");
		}
		CurrentVip_level.gameObject.SetActive (true);
		CurrentVip_level.sprite = UISpriteManager.instance.GetVipSprite (lenght.ToString ());

		for (int i = 0; i < lenght; i++) {
			exp_percent_value += role [i].benefit.exp_percent;
			coin_percent_value += role [i].benefit.coin_percent;
			gold_percent_value += role [i].benefit.gold_percent;
		}
		CurrentVip_exp_percent.text = "Kinh nghiệm +" + exp_percent_value + "%";
		CurrentVip_coin_percent.text = "Bạc +" + coin_percent_value + "%";
		CurrentVip_gold_percent.text = "Vàng +" + gold_percent_value + "%";

	}
	public void UI_OnCloseThis_Click(){
		this.Close();
	}
}
