﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;

public class ReferralCodeView : SingletonBaseScreen<ReferralCodeView> {

	// UI 
	public Button redeem;
	public InputField inputReferralCode;
	public InputField inputGiftCode;
	//public Text myReferralCode;
	//public Text referredBy;
	public Transform choiseRewardGroup;
	public Transform redeemGroup;
	public Transform giftCodeGroup;
	

	
	private bool wasReferralBageFound; 
	
	// Use this for initialization
//	void Start () 
//	{
//		LockUI();
//
//	}
//	
	public void OnRedeemClicked()
	{
		RedeemReferralCode();
	}


	
	public void ShowGiftCodeGroup()
	{

		this.giftCodeGroup.gameObject.SetActive (true);
		this.redeemGroup.gameObject.SetActive (false);
		this.choiseRewardGroup.gameObject.SetActive (false);

	}

	public void ShowRedeemGroup()
	{
		this.giftCodeGroup.gameObject.SetActive(false);
		this.redeemGroup.gameObject.SetActive(true);
		this.choiseRewardGroup.gameObject.SetActive(false);
	}

	public void ShowChoiseRewardGroup()
	{
		if (!wasReferralBageFound) {
			this.giftCodeGroup.gameObject.SetActive(false);
			this.redeemGroup.gameObject.SetActive(false);
			this.choiseRewardGroup.gameObject.SetActive(true);
		} else {
			this.gameObject.SetActive (false);
		}
	}

	void UnlockUI()
	{
		//SearchForReferralBadge();
		if(this.wasReferralBageFound == true)
		{
			ShowGiftCodeGroup();
		}
		else
		{
			ShowChoiseRewardGroup();
		}
		this.redeem.interactable = true;

		ErrorView.instance.Loading (false);
	}
	
	void LockUI()
	{

		this.redeem.interactable = false;

		ErrorView.instance.Loading (true);
	}

	void RedeemReferralCode()
	{	
		HorusClientAPI.RedeemCoupon(new GetRedeemCouponRequest(){
			code = this.inputGiftCode.text ,
			useItem = "1"
			}, 
			(result) => {
				this.inputGiftCode.text = string.Empty;
				if(result.Inventory.Count > 1){
					CSVR_UIRewardItem.instance.Open();
					CSVR_UIRewardItem.instance.ResetListItemReward();
				}
				for (int i = 0; i < result.Inventory.Count; i++) {

					if(result.Inventory [i].CatalogVersion != "BundleGiftCode" && (int)result.Inventory [i].RemainingUses != 0){
						CSVR_UIRewardItem.instance.RewardItem(i,result.Inventory [i]);
					}
				}
				CSVR_UIRewardItem.instance.UI_OpenCSVR_UIRewardItem_Click();
				TopBar.instance.UpdateVirtualCurrency();
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Mã code đã được sử dụng, hoặc không đúng định dạng. Xin vui lòng nhập lại");
			}
		);

	}

	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
	public void UI_OnDisable(){
		this.gameObject.SetActive (false);
	}
}
