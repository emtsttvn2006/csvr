﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorView : MonoBehaviour {
	public static ErrorView instance = null;

	public GameObject loading;
	public GameObject error;
	public Text errorMessage;
	
	public Text coutdownText;
	public GameObject okBtn;

	void Awake(){
		instance = this;
	}
	void OnDestroy()
	{
		if (instance == this)
		{
			instance = null;
		}
	}
	public void Loading(bool isOn = false){
		if (isOn) {
			loading.SetActive (true);
			this.gameObject.SetActive(true);
			Invoke ("LoadingCallBack", 8f);
		} else {
			loading.SetActive (false);
			this.gameObject.SetActive(false);
		}

	}
	private void LoadingCallBack(){
		Loading (false);
	}

	public void ShowPopupErrorAccountBand(string mess){
		this.gameObject.SetActive(true);
		okBtn.SetActive(false);

		error.SetActive (true);
		errorMessage.text = mess;

	}

	public void ShowPopupError(string mess){
		this.gameObject.SetActive(true);
		okBtn.SetActive(true);

		error.SetActive (true);
		errorMessage.text = mess;
		if(loading != null)
			loading.SetActive (false);
	}

	public void ClosePopupError(){
		this.gameObject.SetActive(false);
		okBtn.SetActive(false);

		error.SetActive (false);
		loading.SetActive (false);
	}
	public void CoutDownExitGame(int time,string content){
		this.gameObject.SetActive(true);
		coutdownText.gameObject.SetActive (true);

		PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);
		PhotonNetwork.LeaveRoom();
		HorusManager.instance.gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(false);
		HomeScreen.states = States.Lobby;
		Application.LoadLevel("GameOver");

		this.gameObject.SetActive(false);
		coutdownText.gameObject.SetActive (false);

		//StartCoroutine (CountDown (time, content));
	}
	IEnumerator CountDown(int time,string content){

		for(int i = time; i >= 0; i--){
			yield return new WaitForSeconds(1f);
			coutdownText.text = content + i + " giây ...";
		}

		PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);
		PhotonNetwork.LeaveRoom();
		HorusManager.instance.gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(false);
		//HomeScreen.states = States.Lobby;
		Application.LoadLevel("GameOver");

		this.gameObject.SetActive(false);
		coutdownText.gameObject.SetActive (false);
	}

	public void CoutDownQuitGame(int time,string content){
		this.gameObject.SetActive(true);
		coutdownText.gameObject.SetActive (true);
		Application.Quit();

		//StartCoroutine (CountDownQG (time, content));
	}
	IEnumerator CountDownQG(int time,string content){
		
		for(int i = time; i >= 0; i--){
			coutdownText.text = content + i + " giây ...";
			yield return new WaitForSeconds(1f);
		}
		
		Application.Quit();
	}

	public void CoutDownChangeMaster(int time,string content){
		this.gameObject.SetActive(true);
		coutdownText.gameObject.SetActive (true);
		
		StartCoroutine (CountDownCM (time, content));
	}
	IEnumerator CountDownCM(int time,string content){
		for(int i = time; i >= 0; i--){
			coutdownText.text = content + i + " giây ...";
			yield return new WaitForSeconds(1f);
		}
		coutdownText.gameObject.SetActive (false);
		this.gameObject.SetActive(false);

	}
}
