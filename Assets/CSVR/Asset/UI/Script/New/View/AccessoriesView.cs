﻿using UnityEngine;
using System.Collections;

public class AccessoriesView : SingletonBaseScreen<AccessoriesView> {
	public string idAc = "";
	[SerializeField] private GameObject[] ListAccessories;

	public void SetACActive(string nameWeapon)
	{
		this.gameObject.SetActive (true);
		idAc = nameWeapon;

		for (int i = 0; i < ListAccessories.Length; i++) {
			if (ListAccessories [i].name == nameWeapon) {
				ListAccessories [i].SetActive (true);
			} else {
				ListAccessories [i].SetActive (false);
			}
		}
	
	}
}
