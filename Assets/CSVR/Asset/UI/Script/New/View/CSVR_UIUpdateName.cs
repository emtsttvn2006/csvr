﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;

public class CSVR_UIUpdateName : SingletonBaseScreen<CSVR_UIUpdateName> {
	public InputField name;

	public void UI_OnUpdateNameClick(){
		//music
		if (name.text.Length >= 3 && name.text.Length < 16) {
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.button_Clip);

			UpdateUserInfoRequest request = new UpdateUserInfoRequest();
			request.UserInfo = new Dictionary<string, string>();
			request.UserInfo.Add("DisplayName", name.text);

			HorusClientAPI.UpdateUserInfo(request,
				(result) => {
					AccountManager.instance.displayName = name.text;
					CSVR_GameSetting.hostName = name.text;
					OnUpdateNameCompleted();
				},
				(error) => {
					//{"HttpCode":200,"HttpStatus":"Client failed to parse response from server","Error":"Unknown","ErrorMessage":null,"CustomData":null}
					if(error.HttpStatus == "Client failed to parse response from server"){
						AccountManager.instance.displayName = name.text;
						CSVR_GameSetting.hostName = name.text;
						OnUpdateNameCompleted();
					}else{
						//Debug.Log(Horus.Json.JsonWrapper.SerializeObject (error, Horus.Internal.HorusUtil.ApiSerializerStrategy));
						ErrorView.instance.ShowPopupError("Tên đăng nhập đã tồn tại, xin vui lòng chọn tên khác");
					}

				}
			);
		}
	}

	void OnUpdateNameCompleted()
	{
		MainView.instance.UpdateInfoPlayer ();
		Destroy (this.gameObject);
	}
}
