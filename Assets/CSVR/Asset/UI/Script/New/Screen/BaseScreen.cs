﻿using UnityEngine;
using System.Collections;

public class BaseScreen : MonoBehaviour 
{
    /// <summary>
    /// Default delay time to call LoadLevel.
    /// </summary>
    protected float defaultDelayTime = 0.5f;

    /// <summary>
    /// Calls LoadLevel to loading scene.
    /// </summary>
    
    protected void CallChangeScene()
    {
        Application.LoadLevel(Scenes.Loading.ToString());
    }    

    public virtual void EnableWithAnimation()
    {
        if (!this.gameObject.activeSelf) this.gameObject.SetActive(true);
        if (!GetComponent<Animator>()) return;
        Animator animator = GetComponent<Animator>();
        if (HasParam(animator, "Open"))
        {
            animator.SetBool("Open", true);
        }
    }

    bool HasParam(Animator _Anim, string _ParamName)
    {
        foreach (AnimatorControllerParameter param in _Anim.parameters)
        {
            if (param.name == _ParamName) return true;
        }
        return false;
    }
}