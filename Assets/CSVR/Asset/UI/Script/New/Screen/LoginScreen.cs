﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ProgressBar;
using Horus;
using Horus.ClientModels;

public class LoginScreen : BaseScreen {
	//Login View
	public InputField accInputLogin;
	public InputField passInputLogin;
	public Toggle autoLogin;

	//Register View
	public InputField accInputRegister;
	public InputField pass1InputRegister;
	public InputField pass2InputRegister;
	public InputField emailInputRegister;
	public InputField phoneNumberInputRegister;
	public Toggle dkRegister;

//	public Dropdown qualitySetting;

	public GameObject LoginView,loadingHome,CodeTestObj;
	public ErrorView errorView;
	public ProgressBarBehaviour progressBar;
	void Awake(){
		CodeTestObj.gameObject.SetActive (true);
		CodeTestObj.gameObject.SetActive (false);
	}
	void Start(){
		
		HorusClientAPI.GetStatusImei(
			new GetStatusImeiRequest(){
				imei = SystemInfo.deviceUniqueIdentifier
			}, 
			(result) => {

				if (result.exist == 0) {
					//UnityEngine.Debug.Log ("horusManager.Initialize(OnPlayfabLoginCompleted");
					try {
						CSVR_GameSetting.isAutoLogin = bool.Parse (PlayerPrefs.GetString ("CSVR_GameSetting.isAutoLogin"));
					} catch {
						CSVR_GameSetting.isAutoLogin = false;
					}
					#if !VIRTUAL_ROOM
					if (CSVR_GameSetting.isAutoLogin == true) {
						loadingHome.gameObject.SetActive (true);
						switch (PlayerPrefs.GetInt ("LoginType")) {
						case 1:
							AccountManager.instance.LoginWithCSVR (OnLoginCompleted);
							break;
						case 2:
							AccountManager.instance.LoginWithFacebook (OnLoginCompleted);
							break;
						case 3:
							AccountManager.instance.LoginWithGoogle (OnLoginCompleted);
							break;
						default:
							LoginView.gameObject.SetActive (true);
							break;
						}
					} else {
						LoginView.gameObject.SetActive (true);
					}
					#else
					AccountManager.instance.LoginWithCSVR (OnLoginCompleted);
					#endif
				} else {
					//loadingHome.gameObject.SetActive (true);
					ErrorView.instance.ShowPopupErrorAccountBand ("Tài khoản đã bị khóa");
				}
			},
			(error) => {
				//Debug.Log (error);
				//ErrorView.instance.ShowPopupErrorAccountBand ("Tài khoản đã bị khóa");
			}
		);







	}

	public void OnRegisterHorusClick()
	{
		if (accInputRegister.text != string.Empty 
			&& pass1InputRegister.text != string.Empty 
			&&( pass2InputRegister.text == pass1InputRegister.text) 
			&& emailInputRegister.text != string.Empty
			&& dkRegister.isOn == true
		) {
			errorView.Loading(true);
			//PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","true");
			AccountManager.instance.RegisterWithCSVR (OnLoginCompleted);
		}
	}

	public void OnRegisterPlayfabClick()
	{
		if (accInputRegister.text != string.Empty 
		    && pass1InputRegister.text != string.Empty 
		    && pass2InputRegister.text != string.Empty 
		    && emailInputRegister.text != string.Empty
		    && dkRegister.isOn == true
		    ) {
                errorView.Loading(true);
			//PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","true");
			AccountManager.instance.RegisterWithCSVR (OnLoginCompleted);
		}
	}

	public void OnConnectPlayfabClick()
	{
		if (accInputLogin.text != "" && passInputLogin.text != "") {
			errorView.Loading(true);
            
			if(autoLogin.isOn){
				PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","true");
			}else{
				PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","false");
			}

			PlayerPrefs.SetInt("LoginType", 1);
			AccountManager.instance.LoginWithCSVR (OnLoginCompleted);
		}
	}

	public void OnConnectFacebookClick()
	{
        
		if(autoLogin.isOn){
			PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","true");

		}else{
			PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","false");
		}
		PlayerPrefs.SetInt("LoginType", 2);
		AccountManager.instance.LoginWithFacebook(OnLoginCompleted);
	}
	public void OnConnectGoogleClick()
	{
		if(autoLogin.isOn){
			PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","true");

		}else{
			PlayerPrefs.SetString("CSVR_GameSetting.isAutoLogin","false");
		}
		PlayerPrefs.SetInt("LoginType", 3);
		AccountManager.instance.LoginWithGoogle(OnLoginCompleted);
	}
	void OnLoginCompleted()
	{
		errorView.Loading(false);
		CodeTestObj.gameObject.SetActive (false);
		loadingHome.gameObject.SetActive (true);
		progressBar.IncrementValue (80f);
	}
//	public void OnLoadInfoPlayerComplete(){
//
//		if(PlayFabManager.instance.GetTitleValue("Key_GameUpdate") != Application.version){
//			Application.LoadLevel ("Update");
//			
//		}else{
//			//Debug.Log ("OnLoadInfoPlayerComplete");
//			System.GC.Collect();
//			Resources.UnloadUnusedAssets();
//			Application.LoadLevel ("Home");
//		}
//	}
//	public void ChangeQuality(int i) {
//		PlayerPrefs.SetFloat("CSVR_GameSetting.Quality",i);
//		QualitySettings.SetQualityLevel(i, true);
//
//	}
}
