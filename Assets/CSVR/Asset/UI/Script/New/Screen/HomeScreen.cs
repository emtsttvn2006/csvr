﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;
using Horus.Json;


public class HomeScreen : SingletonBaseScreen<HomeScreen> {
	public Sprite[] modeMapSprites;
	public Dictionary<string, int> modeMapSpritesIndex = new Dictionary<string, int>();

	public static States states = States.Home;
	public static bool isLevelUp;

    public GameObject EasyPool;
	public AudioSource bgMusic;
	public AudioSource fxMusic;    

	//chat
	public Transform ChatView_Item;
	public Text TextChat1Line;
	//end

	public LobbyResultV2 lobbyResult;
	public Dictionary<string,RP_ServerProfile> roomProfile;

    public Vector2 v2Size = Vector2.zero;    

	string json = "{\"ModeAvaiable\":{\"CSVR_RandomMode\":\"Ngẫu nhiên\",\"CSVR_TeamDeathmatch\":\"Đấu Đội\",\"CSVR_AITeamDeathmatch\":\"Đấu Máy\",\"CSVR_Deathmatch\":\"Đấu Đơn\",\"CSVR_DeadOrAlive\":\"Sinh Tử\",\"CSVR_SearchAndDestroy\":\"Đặt Bomb\",\"CSVR_Zoombie\":\"Bắn Zombie\",\"CSVR_Flag\":\"Đoạt Cờ\",\"CSVR_BaseCapture\":\"Cứ Điểm\"},\"MapAvaiable\":{\"Map_Random\":\"Ngẫu Nhiên\",\"Map_Ngatu\":\"Ngã Tư Tử Thần\",\"Map_Thuydien\":\"Thủy Điện\",\"Map_Tauhang\":\"Tàu Chở Hàng\",\"Map_Thinghiem\":\"Viện Nghiên Cứu\",\"Map_Italy\":\"Italy\",\"Map_Dust\":\"Sa Mạc\",\"Map_Train\":\"Nhà Ga\",\"Map_Dust2\":\"Sa Mạc 2\"},\"Lobby\":[{\"ModeName\":\"CSVR_RandomMode\",\"Interactable\":true,\"Maps\":[\"Map_Random\"]},{\"ModeName\":\"CSVR_TeamDeathmatch\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\"]},{\"ModeName\":\"CSVR_AITeamDeathmatch\",\"Interactable\":true,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\"]},{\"ModeName\":\"CSVR_Deathmatch\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_DeadOrAlive\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_SearchAndDestroy\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Zoombie\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_Flag\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]},{\"ModeName\":\"CSVR_BaseCapture\",\"Interactable\":false,\"Maps\":[\"Map_Random\",\"Map_Ngatu\",\"Map_Thuydien\",\"Map_Tauhang\",\"Map_Thinghiem\",\"Map_Italy\",\"Map_Dust\",\"Map_Train\",\"Map_Dust2\"]}]}";

	void Awake(){

		if (PlayerPrefs.HasKey ("CSVR_GameSetting.music")) {
			bgMusic.volume = PlayerPrefs.GetFloat ("CSVR_GameSetting.music");
		} else {
			bgMusic.volume = 1;
		}
		if (PlayerPrefs.HasKey ("CSVR_GameSetting.soundEfx")) {
			fxMusic.volume = PlayerPrefs.GetFloat("CSVR_GameSetting.soundEfx");
		} else {
			fxMusic.volume = 1;
		}

        v2Size = GetComponent<CanvasScaler>().referenceResolution;
    }
	void Start(){

		for (int i = 0; i < modeMapSprites.Length; i++) {
			modeMapSpritesIndex.Add(modeMapSprites[i].name, i);
		}

		#if MODE_TEST
		lobbyResult = HorusSimpleJson.DeserializeObject<LobbyResultV2>(json);
		#else
		lobbyResult = HorusSimpleJson.DeserializeObject<LobbyResultV2>(HorusManager.instance.GetTitleValue("ModeMapAvailable"));
		#endif      
		GetRoomProfile ();
	}    
    

	byte ErrorGetRoomProfileCount = CSVR_GameSetting.errorCountCallBack;
	private void GetRoomProfile()
	{
		HorusClientAPI.RoomProfile(
			new RoomProfileRequest(){
			}, 
			(result) => {
				ErrorGetRoomProfileCount = CSVR_GameSetting.errorCountCallBack;
				roomProfile = result.roomStatic;
			},
			(error) => {
				if(ErrorGetRoomProfileCount > 0){
					ErrorGetRoomProfileCount--;
					Invoke ("GetRoomProfile", 0.5f);
				}
			}
		);
	}

    void OnLevelWasLoaded(int level) {
		if (Application.loadedLevelName == "Home")
        { 
            switch (states) {
                case States.UpdateName:                    
				    CharacterView.instance.Open ();
				    MainView.instance.Open ();
					TopBar.instance.Open ();
				    CSVR_UIUpdateName.instance.Open ();
                    break;       
                                    
                case States.Home:			
			        CharacterView.instance.Open ();
			        MainView.instance.Open ();
					TopBar.instance.Open ();
                    CSVR_UIChat.instance.ScaleZero();
				    break;
			
			    case States.Lobby:                   
					TopBar.instance.Open ();
				    CSVR_UIMultiplayerLobby.instance.Open ();
				    break;
			
    			case States.EndGame:	
					TopBar.instance.Open ();
	    			CSVR_UIMultiplayerLobby.instance.Open ();
		    		CSVR_UIEndGame.instance.Open ();
				    break;
				case States.EndGamePVE:	
					TopBar.instance.Open ();
					CSVR_UIEndGamePVE.instance.Open ();
					CharacterView.instance.Open ();
					break;		
			}
        }
	}

	public void UI_OnChatClick()
	{
		AudioManager.instance.audio.PlayOneShot(AudioManager.instance.button_Clip);
		CSVR_UIChat.instance.Open();        

	}

	public ProjectDelegates.SimpleCallback OnLoadingMapCallBack;
	public void SetLoadingMapCallBack(ProjectDelegates.SimpleCallback callBack){
		OnLoadingMapCallBack = callBack;
	}

	public ProjectDelegates.SimpleCallback OnMatchRoomScreenCallBack;
	public void SetMatchRoomScreenCallBack(ProjectDelegates.SimpleCallback callBack){
		OnMatchRoomScreenCallBack = callBack;
	}
}
