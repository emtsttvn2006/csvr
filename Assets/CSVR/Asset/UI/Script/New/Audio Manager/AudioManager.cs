﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

	public AudioClip spin_Reward;
	public AudioClip alert_Clip;
	public AudioClip button_Clip;
	public AudioClip buySS_Clip;
	public AudioClip buySSEff_Clip;
	public AudioClip Equip_Clip;
	public AudioClip Play_Clip;
	public AudioClip Sell_Clip;
	public AudioClip ChangeCoin_Clip;
	public AudioClip InvClick_Clip;
	public AudioClip InvOpen_Clip;
	public AudioClip Upgrage_Clip;
	public AudioClip UpgrageFL_Clip;
	public AudioClip UpgrageSS_Clip;

	public AudioSource audio;
    void Awake()
    {
		instance = this;
		audio.GetComponent<AudioSource> ();
    }
}