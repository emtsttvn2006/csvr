﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class FacebookManager : MonoBehaviour
{
    /// <summary>
    /// Callback to be called when Facebook login process completes.
    /// </summary>
    ProjectDelegates.FacebookLoginCallback OnFacebookLoginCompletedCallback;

	public void Initialize(ProjectDelegates.FacebookLoginCallback OnLoginCompletedCallback)
	{
		this.OnFacebookLoginCompletedCallback = OnLoginCompletedCallback;
		if(FB.IsInitialized == true)
		{
			FBOnInitComplete();
		}
		else
		{
			FB.Init(FBOnInitComplete);
		}
	}
	
	// callback after FB.Init();
	public void FBOnInitComplete()
	{
		if(FB.IsLoggedIn == false)
		{
			CallFBLogin();
		}
		else
		{
			FB.API("me/picture?type=square&height=128&width=128", HttpMethod.GET, OnProfileInfoLoaded);
		}
	}
	private void CallFBLogin()
	{
		FB.LogInWithReadPermissions(new List<string>(){"public_profile","email","user_friends"}, OnLoginCompleted);
	}

    /// <summary>
    /// Callback used when Facebook login is complete.
    /// </summary>
    /// <param name="result">Login result</param>
	void OnLoginCompleted(ILoginResult result)
    {
        if (result.Error != null)
        {
            //Debug.Log("error facebook " + result.Error);
        }
        else if (!FB.IsLoggedIn)
        {
            //Debug.Log("login cancelled, by user or error");
        }
        else
        {
            //Debug.Log("login successful");
			FB.API("me/picture?type=square&height=128&width=128", HttpMethod.GET, OnProfileInfoLoaded);
        }
    }

    /// <summary>
    /// Callback used when user's profile info loads.
    /// </summary>
    /// <param name="result"></param>
	void OnProfileInfoLoaded(IGraphResult result)
    {
		//Debug.Log ("lala ");
		if (result.Texture != null) {
			AccountManager.instance.avatarPicture = Sprite.Create (result.Texture, new Rect (0, 0, 128, 128), new Vector2 ());
			AccountManager.instance.avatarPlayerTexture = result.Texture;
		}
		this.OnFacebookLoginCompletedCallback(AccessToken.CurrentAccessToken.TokenString);
    }
    /// <summary>
    /// Function needed to handle application win/lose focus
    /// </summary>
    /// <param name="isGameShown">Is focus on app?</param>
    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // start the game back up - we're getting focus again
            Time.timeScale = 1;
        }
    }
}