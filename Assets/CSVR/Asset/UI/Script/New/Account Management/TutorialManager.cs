﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

public enum TutorialState
{
    NOTHING,
    DOING,
    FINISH
}

public class TutorialManager : MonoBehaviour {
    /*
    public static TutorialManager instance;

    public TutorialState TutorialState;

    public bool Pause = false;

    public List<string> Phases = new List<string>()
    {
        "TEAMFIGHT",
        "BUYUPGRADE",
        "IAP",
        "EQUIP",
        "DONETUT"
    };

    public List<string> TutKeys = new List<string>()
    {
        "TutorialState",
        "TEAMFIGHT_QuickPlayButton",
        "TEAMFIGHT_TeamFightButton",
        "TEAMFIGHT_DeathmatchButton",
        "TEAMFIGHT_XuatkichButton",
        "BUYUPGRADE_ShopButton",
        "BUYUPGRADE_ScrollShop",
        "BUYUPGRADE_GrenadeButton",
        "BUYUPGRADE_GrenadeItemButton",
        "BUYUPGRADE_GrenadeItemButtonBuy7Day",
        "BUYUPGRADE_GrenadeConfirmButtonOk",
        "BUYUPGRADE_GrenadeBackToInventory",
        "BUYUPGRADE_ChooseSecondBag",
        "BUYUPGRADE_TrangBiGrenade",
        "BUYUPGRADE_TieptucChoDenKhiDayTui",
        "DONETUT_ButtonNhan"
    };

    public List<string> PhasesValues = new List<string>();

    public int currentTut = 0;    

    public List<string> TutValues = new List<string>();

    public List<stringlist> TutRequires = new List<stringlist>();
    
    public List<UnityAction> TutActions = new List<UnityAction>();

    //public List<TutorialAction> StatesDoing;

    [HideInInspector]
    public Vector2 v2Size = Vector2.zero;

    void Awake()
    {
        instance = this;

        TutorialState = GetState("TutorialState");

        UpdatePhases();

        UpdateTut();

        UpdateRequire();

        UpdateAction();
    }

    public void UpdatePhases()
    {
        PhasesValues.Clear();
        for (int i = 0; i < Phases.Count; i++)
        {
            PhasesValues.Add(GetState(Phases[i]).ToString());
        }
    }

    public void UpdateTut()
    {
        TutValues.Clear();
        for (int i = 0; i < TutKeys.Count; i++)
        {
            TutValues.Add(GetState(TutKeys[i]).ToString());
        }
    }

    public void UpdateRequire()
    {
        TutRequires.Clear();
        #region DoAdd
        //TutorialState
        TutRequires.Add(new stringlist(new List<string>()
        {            
        }));
        //TEAMFIGHT_QuickPlayButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            
        }));
        //TEAMFIGHT_TeamFightButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            "TEAMFIGHT_QuickPlayButton"
        }));
        //TEAMFIGHT_DeathmatchButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            "TEAMFIGHT_TeamFightButton"
        }));
        //TEAMFIGHT_XuatkichButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            "TEAMFIGHT_DeathmatchButton"
        }));
        //BUYUPGRADE_ShopButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            
        }));
        //BUYUPGRADE_ScrollShop
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_ShopButton"
        }));
        //BUYUPGRADE_GrenadeButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_ScrollShop"
        }));
        //BUYUPGRADE_GrenadeItemButton
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_GrenadeButton"
        }));
        //BUYUPGRADE_GrenadeItemButtonBuy7Day
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_GrenadeItemButton"
        }));
        // BUYUPGRADE_GrenadeConfirmButtonOk
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_GrenadeItemButtonBuy7Day"
        }));
        //BUYUPGRADE_GrenadeBackToInventory
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_GrenadeConfirmButtonOk"
        }));
        //BUYUPGRADE_ChooseSecondBag
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_GrenadeBackToInventory"
        }));
        //BUYUPGRADE_TrangBiGrenade
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_ChooseSecondBag"
        }));
        //BUYUPGRADE_TieptucChoDenKhiDayTui
        TutRequires.Add(new stringlist(new List<string>()
        {
            "BUYUPGRADE_TrangBiGrenade"
        }));
        //DONETUT_ButtonNhan
        TutRequires.Add(new stringlist(new List<string>()
        {
            "TutorialState"
        }));
        #endregion
        Debug.Log(this.TutRequires.Count.ToString());
    }

    public void UpdateAction()
    {
        TutActions.Clear();
        #region DoAdd
        //TutorialState
        TutActions.Add(new UnityAction(() =>
        {
            this.currentTut = 0;
            NextTutorial();
        }));
        //TEAMFIGHT_QuickPlayButton
        TutActions.Add(new UnityAction(() =>
        {
            MainView.instance.SetUpTutorial_TEAMFIGHT_QuickPlayButton();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                MainView.instance.ButtonQuickPlay.onClick.Invoke();
                SetStateDoing("TutorialState", TutorialState.DOING);
                SetStateDoing("TEAMFIGHT_QuickPlayButton", TutorialState.FINISH);
                this.currentTut = 1;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //TEAMFIGHT_TeamFightButton
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIMultiplayerLobby.instance.SetUpTutorial_TEAMFIGHT_TeamFightButton();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIMultiplayerLobby.instance.ToggleTutmode.isOn = true;
                SetStateDoing("TEAMFIGHT_TeamFightButton", TutorialState.FINISH);
                this.currentTut = 2;
                Invoke("NextTutorial", 0.4f);
            }));
        }));
        //TEAMFIGHT_DeathmatchButton
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIMultiplayerLobby.instance.SetUpTutorial_TEAMFIGHT_DeathmatchButton();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIMultiplayerLobby.instance.ToggleTutmodecontent.isOn = true;
                SetStateDoing("TEAMFIGHT_DeathmatchButton", TutorialState.FINISH);
                this.currentTut = 3;
                Invoke("NextTutorial", 0.4f);
            }));
        }));
        //TEAMFIGHT_XuatkichButton
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIMultiplayerLobby.instance.SetUpTutorial_TEAMFIGHT_XuatkichButton();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {                
                SetStateDoing("TEAMFIGHT_XuatkichButton", TutorialState.FINISH);
                SetPhaseDoing("TEAMFIGHT", TutorialState.FINISH);
                CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = false;                
                CSVR_UIMultiplayerLobby.instance.UI_OnJoinRoomMenu_Click();
            }));
        }));
        //BUYUPGRADE_ShopButton
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIMultiplayerLobby.instance.Close();
            MainView.instance.Open();
            CharacterView.instance.Open();            
            TopBar.instance.SetUpTutorial_ShopButton();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                SetStateDoing("TutorialState", TutorialState.DOING);
                CSVR_UIShop.instance.Open();
                CSVR_UIShop.instance.RightPanel.gameObject.SetActive(true);
                CSVR_UIShop.instance.RightPanelDetail.gameObject.SetActive(false);
                SetStateDoing("BUYUPGRADE_ShopButton", TutorialState.FINISH);
                this.currentTut = 5;
                Invoke("NextTutorial", 0.7f);
            }));
        }));
        //BUYUPGRADE_ScrollShop
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIShop.instance.SetUpTutorial_Scroll_WeaponContent_ToGrenade();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIShop.instance.Scroll_MoveTo(new Vector3(0, 250f, 0));
                SetStateDoing("BUYUPGRADE_ScrollShop", TutorialState.FINISH);
                this.currentTut = 6;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_GrenadeButton
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIShop.instance.SetUpTutorial_ButtonGrenade();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIShop.instance.SwichClassItem("Grenade.Weapon");
                SetStateDoing("BUYUPGRADE_GrenadeButton", TutorialState.FINISH);
                this.currentTut = 7;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_GrenadeItemButton
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIShop.instance.SetUpTutorial_ButtonGrenadeItem();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIShop.instance.ButtonGrenadeItemBuy.onClick.Invoke();
                SetStateDoing("BUYUPGRADE_GrenadeItemButton", TutorialState.FINISH);
                this.currentTut = 8;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_GrenadeItemButtonBuy7Day
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIShop.instance.SetUpTutorial_ButtonGrenadeItem();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIShop.instance.ButtonGrenadeItemBuy7Day.GetComponent<Button>().onClick.Invoke();
                SetStateDoing("BUYUPGRADE_GrenadeItemButtonBuy7Day", TutorialState.FINISH);
                this.currentTut = 9;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_GrenadeConfirmButtonOk
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIPopupConfirmShop.instance.SetUpTutorial_ButtonGrenadeItemConfirmOk();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIPopupConfirmShop.instance.UI_DisableItemId();
                CSVR_UIPopupConfirmShop.instance.Close();                
                SetStateDoing("BUYUPGRADE_GrenadeConfirmButtonOk", TutorialState.FINISH);
                this.currentTut = 10;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_GrenadeBackToInventory
        TutActions.Add(new UnityAction(() =>
        {
            TopBar.instance.SetUpTutorial_InventoryButton();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIShop.instance.Close();
                CSVR_UIInventory.instance.Open();
                SetStateDoing("BUYUPGRADE_GrenadeBackToInventory", TutorialState.FINISH);
                this.currentTut = 11;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_ChooseSecondBag
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIInventory.instance.SetUpTutorial_ChooseSecondBag();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIInventory.instance.SetlistGroupToggleOn(1);
                SetStateDoing("BUYUPGRADE_ChooseSecondBag", TutorialState.FINISH);
                this.currentTut = 12;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_TrangBiGrenade
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UIInventory.instance.SetUpTutorial_TrangBiGrenade();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UIInventory.instance.ButtonGrenadeTrangBi.onClick.Invoke();
                SetStateDoing("BUYUPGRADE_TrangBiGrenade", TutorialState.FINISH);
                this.currentTut = 13;
                Invoke("NextTutorial", 0.5f);
            }));
        }));
        //BUYUPGRADE_TieptucChoDenKhiDayTui
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.ShowContinue();
            CSVR_UITutorial.instance.SetUpTutorial_TieptucChoDenKhiDayTui();
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UITutorial.instance.Close();
                SetPhaseDoing("BUYUPGRADE", TutorialState.FINISH);
                SetStateDoing("BUYUPGRADE_TieptucChoDenKhiDayTui", TutorialState.FINISH);
                ReFillAPhase("TEAMFIGHT");                
            }));
        }));
        //DONETUT_ButtonNhan
        TutActions.Add(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.ShowReward();
            CSVR_UITutorial.instance.SetUpTutorial_ButtonNhan();
            CSVR_UITutorial.instance.SetAction(new UnityAction(() => {
                CSVR_UITutorial.instance.Close();
                SetPhaseDoing("DONETUT", TutorialState.FINISH);
                Finish("TutorialState");
            }));
        }));
        #endregion
        Debug.Log(this.TutActions.Count.ToString());
    }  
    
    public void Finish(string Phase)
    {
        PlayerPrefs.SetString(Phase, TutorialState.FINISH.ToString());
        PlayerPrefs.Save();
    }    

    public TutorialState GetState(string State)
    {        
        if (PlayerPrefs.HasKey(State))
        {
            return (TutorialState) Enum.Parse(typeof(TutorialState), PlayerPrefs.GetString(State));
        }
        else
        {            
            PlayerPrefs.SetString(State, TutorialState.NOTHING.ToString());
            PlayerPrefs.Save();
            return TutorialState.NOTHING;            
        }        
    }    

    public void SetStateDoing(string stateName, TutorialState TutorialState)
    {
        int index = TutKeys.FindIndex((key) => key == stateName);
        if (index != -1)
        {
            TutValues[index] = TutorialState.ToString();
            PlayerPrefs.SetString(stateName, TutorialState.ToString());
            PlayerPrefs.Save();
        } else
        {
            Debug.Log(stateName + " không có!!");
        }
    }

    public void SetPhaseDoing(string phaseName, TutorialState TutorialState)
    {
        int index = Phases.FindIndex((key) => key == phaseName);
        if (index != -1)
        {
            PhasesValues[index] = TutorialState.ToString();
            PlayerPrefs.SetString(phaseName, TutorialState.ToString());
            PlayerPrefs.Save();
        }
        else
        {
            Debug.Log(phaseName + " không có!!");
        }
    }

    public bool CheckRequire(string Name)
    {
        bool ValueReturn = true;

        int index = TutKeys.FindIndex((key) => key == Name);
        if (index != -1)
        {
            for (int i = 0; i < TutRequires[index].listString.Count; i++)
            {
                int index1 = TutKeys.FindIndex((str) => str == TutRequires[index].listString[i]);
                if (TutValues[index1] == TutorialState.NOTHING.ToString())
                {                 
                    ValueReturn = false;
                    break;
                }
            }
        }

        return ValueReturn;
    }

    public void NextTutorial()
    {
        CSVR_UIWaitingIcon.instance.Open();        
        if (this.currentTut < TutKeys.Count - 1)
        {
            if (this.currentTut >= 1)
            {
                this.currentTut++;
                if (TutValues[this.currentTut] == TutorialState.NOTHING.ToString()) {
                    if (this.currentTut == FirstInPhase(TutKeys[this.currentTut]))
                    {
                        if (TutValues[this.currentTut] == TutorialState.NOTHING.ToString())
                        {
                            SetPhaseDoing(GetPhaseOfTutKey(TutKeys[this.currentTut]), TutorialState.DOING);
                            CSVR_UIWaitingIcon.instance.Close();
                            TutActions[this.currentTut].Invoke();
                        }
                    } else
                    {
                        if (GetState(GetPhaseOfTutKey(TutKeys[this.currentTut])) == TutorialState.DOING)
                        {
                            CSVR_UIWaitingIcon.instance.Close();
                            TutActions[this.currentTut].Invoke();                            
                        }
                    }
                }
            } else if (this.currentTut == 0)
            {
                bool Has = false;         
                for (int i = 1; i < TutKeys.Count - 1; i++)
                {
                    if (TutValues[i] == TutorialState.NOTHING.ToString())
                    {
                        if (i == FirstInPhase(GetPhaseOfTutKey(TutKeys[i])))
                        {
                            SetPhaseDoing(GetPhaseOfTutKey(TutKeys[i]), TutorialState.DOING);
                            CSVR_UIWaitingIcon.instance.Close();                            
                            TutActions[i].Invoke();
                            Has = true;
                            break;
                        } else
                        {
                            ReFillAPhase(GetPhaseOfTutKey(TutKeys[i]));
                            this.currentTut = 0;
                            Invoke("NextTutorial", 1.0f);
                            break;
                        }
                    }                    
                }
                if (!Has)
                {
                    this.currentTut = TutKeys.Count - 1;
                    Invoke("NextTutorial", 1.0f);
                }
            }
        } else
        {
            CSVR_UIWaitingIcon.instance.Close();
            TutActions[this.currentTut].Invoke();
        }
    }

    public string GetPhaseOfTutKey(string Key)
    {
        string str = "";

        for (int i = 0; i < Phases.Count; i++) {
            if (Key.Contains(Phases[i]))
            {
                str = Phases[i];
                break;
            }
        }

        return str;
    }

    public int GetIndexTutKey(string Name)
    {
        return TutKeys.FindIndex((key) => key == Name);
    }

    public bool APhaseIsFull(string Phase)
    {
        bool Full = true;

        int index = TutKeys.FindIndex((key) => key == Phase);

        if (index != -1)
        {
            if (TutValues[index] == TutorialState.FINISH.ToString() && !CheckRequire(TutKeys[index]))
            {            
                Full = false;            
            } else if (TutValues[index] == TutorialState.NOTHING.ToString())
            {
                bool Check = true;                     
                for (int i = 0; i < TutRequires[index].listString.Count; i++)
                {
                    int index1 = GetIndexTutKey(TutRequires[index].listString[i]);
                    if (index1 != -1)
                    {
                        if (GetState(TutKeys[index1]) == TutorialState.NOTHING)
                        {
                            Check = false;
                            break;                            
                        }
                    } else
                    {
                        Debug.Log("lỗi");
                    }                    
                }

                if (!Check || (Check && (index != LastInPhase(GetPhaseOfTutKey(TutKeys[index])))))
                    { 
                        Full = false;
                    }
            } 
        } else
        {
            Debug.Log("Lỗi!!");
        }

        if (!Full) ReFillAPhase(Phase);
        return Full;
    }

    public int FirstInPhase(string Phase)
    {
        int index = -1;
        for (int i = 0; i < TutKeys.Count; i++)
        {
            if (TutKeys[i].Contains(Phase))
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public int LastInPhase(string Phase)
    {
        int index = -1;
        for (int i = TutKeys.Count; i > 0; i--)
        {
            if (TutKeys[i].Contains(Phase))
            {
                index = i;
                break;
            }
        }
        return index;
    }

    public void ReFillAPhase(string Phase)
    {
        SetPhaseDoing(Phase, TutorialState.NOTHING);
        for (int i = 0; i < TutKeys.Count; i++)
        {
            if (TutKeys[i].Contains(Phase))
            {
                SetStateDoing(TutKeys[i], TutorialState.NOTHING);
            }
        }
    }            

    #region Setup Tutorial
    public void SetUpTutorial_TEAMFIGHT_TeamFightButton()
    {
        this.ToggleTutmode = modeParent.transform.FindChild("CSVR_TeamDeathmatch").GetComponent<Toggle>();

        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(this.ToggleTutmode.GetComponent<RectTransform>());
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetOffSet(Vector3.zero);
            CSVR_UITutorial.instance.SetTop();
        }));

    }

    public void SetUpTutorial_TEAMFIGHT_DeathmatchButton()
    {
        this.ToggleTutmodecontent = parent_ModeContent.transform.FindChild("CSVR_TeamDeathmatch").transform.FindChild("Grid").transform.FindChild("Map_Ngatu").GetComponent<Toggle>();


        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(this.ToggleTutmodecontent.GetComponent<RectTransform>());
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetOffSet(Vector3.zero);
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    public void SetUpTutorial_TEAMFIGHT_XuatkichButton()
    {
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ButtonXuatkich.GetComponent<RectTransform>());
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetOffSet(new Vector3(-0.2f, 0.1f, 0f));
            CSVR_UITutorial.instance.SetTop();
            this.ToggleTutmodecontent = ButtonXuatkich.GetComponent<Toggle>();
        }));
    }

    public void SetUpTutorial_ButtonGrenadeItemConfirmOk()
    {
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(cOk.GetComponent<RectTransform>());            
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    #region Setup Tutorial
    public void SetUpTutorial_TEAMFIGHT_QuickPlayButton()
    {        
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {         
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ButtonQuickPlay.GetComponent<RectTransform>());            
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    public void SetUpTutorial_TEAMFIGHT_InventoryButton()
    {
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {            
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ButtonInventory.GetComponent<RectTransform>());
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }
    #endregion

    public void SetUpTutorial_ShopButton()
    {
        SetShop();
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ShopOrInV.GetComponent<RectTransform>());
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetOffSet(new Vector3(-0.2f, -0.1f, 0f));
            CSVR_UITutorial.instance.SetBottom();            
        }));
    }

    public void SetUpTutorial_InventoryButton()
    {
        SetInventory();
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ShopOrInV.GetComponent<RectTransform>());
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetOffSet(new Vector3(-0.2f, -0.1f, 0f));
            CSVR_UITutorial.instance.SetBottom();
        }));
    }

    public RectTransform RectSecondBag;    

    public void SetUpTutorial_ChooseSecondBag()
    {        
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(RectSecondBag);            
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    #endregion

    #region Setup Tutorial
    public RectTransform Tut_Scroll_WeaponContent_ToGrenade;

    public RectTransform RightPanel;
    public RectTransform RightPanelDetail;

    public void SetUpTutorial_Scroll_WeaponContent_ToGrenade()
    {
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(Tut_Scroll_WeaponContent_ToGrenade);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    Vector3? v3_Scroll_MoveTo;
    bool isStart_Scroll_MoveTo = false;

    public void Scroll_MoveTo(Vector3 Vector3)
    {
        this.v3_Scroll_MoveTo = Vector3 + this.Tut_Scroll_WeaponContent_ToGrenade.localPosition;
        this.isStart_Scroll_MoveTo = true;
        InvokeRepeating("Do_Scroll_MoveTo", 0.0f, 0.034f);
    }

    public void Stop_Scroll_MoveTo()
    {
        this.v3_Scroll_MoveTo = null;
        this.isStart_Scroll_MoveTo = false;
        CancelInvoke("Do_Scroll_MoveTo");
    }

    public void Do_Scroll_MoveTo()
    {
        if (this.v3_Scroll_MoveTo != null && this.isStart_Scroll_MoveTo)
        {
            if ((this.Tut_Scroll_WeaponContent_ToGrenade.localPosition - ((Vector3)this.v3_Scroll_MoveTo)).magnitude > 1)
            {
                this.Tut_Scroll_WeaponContent_ToGrenade.localPosition = new Vector3(0, Mathf.Lerp(this.Tut_Scroll_WeaponContent_ToGrenade.localPosition.y, ((Vector3)this.v3_Scroll_MoveTo).y, 0.1f), 0f);
            }
            else
            {
                Stop_Scroll_MoveTo();
            }
        } else
        {
            Stop_Scroll_MoveTo();
        }
    }

    public RectTransform ButtonGrenade;

    public void SetUpTutorial_ButtonGrenade()
    {
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ButtonGrenade);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    public RectTransform AllContentGrid;
    public RectTransform ButtonGrenadeItem;
    public Button ButtonGrenadeItemBuy;

    public void SetUpTutorial_ButtonGrenadeItem()
    {   
        for (int i = 0; i < AllContentGrid.transform.childCount; i++)
        {
            if (AllContentGrid.GetChild(i).FindChild("Item_name").GetComponent<Text>().text == "Lựu đạn nổ")
            {
                ButtonGrenadeItem = AllContentGrid.GetChild(i).FindChild("Item_By").GetComponent<RectTransform>();
                ButtonGrenadeItemBuy = AllContentGrid.GetChild(i).GetComponent<CSVR_UIItemShop>().DetailButton;
                break;
            }
        }

        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ButtonGrenadeItem);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetOffSet(new Vector3(-0.05f, 0.02f, 0f));
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    public RectTransform ButtonGrenadeItemBuy7Day;

    public void SetUpTutorial_ButtonGrenadeItemBuy7Day()
    {
        CSVR_UITutorial.instance.SetUpAction_CSVR_UITutorial_Follow(new UnityAction(() =>
        {
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetParent(this.transform);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.SetFollow(ButtonGrenadeItemBuy7Day);
            CSVR_UITutorial.instance.CSVR_UITutorial_Follow.isFollow = true;
            CSVR_UITutorial.instance.SetTop();
        }));
    }

    #endregion
    */
}
