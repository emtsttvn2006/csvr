﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class CSVRManager : MonoBehaviour {
	public LoginScreen loginScreen;

	ProjectDelegates.CSVRLoginCallback OnCSVRLoginCompletedCallback;
	ProjectDelegates.CSVRRegisterCallback OnCSVRRegisterCompletedCallback;
	
	public void Initialize(ProjectDelegates.CSVRLoginCallback OnLoginCompletedCallback)
	{
		this.OnCSVRLoginCompletedCallback = OnLoginCompletedCallback;

		#if !VIRTUAL_ROOM
		if (CSVR_GameSetting.isAutoLogin == true) {
			this.OnCSVRLoginCompletedCallback (PlayerPrefs.GetString ("CSVR_GameSetting.username"), PlayerPrefs.GetString ("CSVR_GameSetting.password"));
		} else {
			this.OnCSVRLoginCompletedCallback (loginScreen.accInputLogin.text, loginScreen.passInputLogin.text);
		}



//		        this.OnCSVRLoginCompletedCallback(loginScreen.accInputLogin.text, loginScreen.passInputLogin.text);
		#else
		
		#if UNITY_EDITOR
			if (CSVR_GameSetting.isAutoLogin == true) {
				this.OnCSVRLoginCompletedCallback (PlayerPrefs.GetString ("CSVR_GameSetting.username"), PlayerPrefs.GetString ("CSVR_GameSetting.password"));
			} else {
				this.OnCSVRLoginCompletedCallback (loginScreen.accInputLogin.text, loginScreen.passInputLogin.text);
			}

		#else
			string[] cmlnArgs = CSVR_RoomHostService.GetCommandLineArgs();
			PlayerPrefs.DeleteAll();
			Screen.SetResolution(640, 400, false);
			if (cmlnArgs.Length > 2) {
				this.OnCSVRLoginCompletedCallback (cmlnArgs[1], cmlnArgs[2]);
			} else {
				this.OnCSVRLoginCompletedCallback ("","");
			}

			//Giet hoan toan process cu
			try {
				if (cmlnArgs.Length>3 && cmlnArgs[3] != null) {
					System.Diagnostics.Process parrentProc = System.Diagnostics.Process.GetProcessById(int.Parse(cmlnArgs[3]));	
					if (parrentProc != null) {
						parrentProc.Kill();
					}
				}}
			catch(System.Exception e) {
			}

		#endif     
		#endif   
	}


	public void InitializeRes(ProjectDelegates.CSVRRegisterCallback OnRegisterCompletedCallback)
	{

		this.OnCSVRRegisterCompletedCallback = OnRegisterCompletedCallback;
		//CreateNewUser
		this.OnCSVRRegisterCompletedCallback(loginScreen.accInputRegister.text,loginScreen.pass1InputRegister.text,loginScreen.emailInputRegister.text,loginScreen.phoneNumberInputRegister.text);
	}


	public static CSVRManager instance;
	bool IsRestarting = false;


	public  GameObject homeUICamera;
	public  Transform roomVIPAudioListener;
	public  GameObject roomVIPCamera;
	public  bool currentGraphicState = false;

	void Awake() {
		instance = this;
		#if VIRTUAL_ROOM
		GameObject roomhoststatus = Instantiate(Resources.Load("CSVR_RoomHostServiceStatus") as GameObject);
		roomhoststatus.transform.SetParent (this.transform);

		homeUICamera = GameObject.Find("Canvas/Main Camera");
		if (homeUICamera != null) {
			homeUICamera.SetActive(false);
			roomVIPAudioListener = homeUICamera.transform.root.FindChild("roomVIPAudioListener");

			if (roomVIPAudioListener == null) {
				roomVIPAudioListener = new GameObject("roomVIPAudioListener").transform;
				roomVIPAudioListener.SetParent(homeUICamera.transform.root);
				roomVIPAudioListener.gameObject.AddComponent<AudioListener>();
				AudioListener.pause = true;
				AudioListener.volume = 0;
			}
		}

		#endif

		if (Debug.isDebugBuild) { 
			GameObject debugLogViewer = Instantiate(Resources.Load("CSVR_DebugLogViewer") as GameObject);
			debugLogViewer.transform.SetParent (this.transform);
		}

	}

	#if VIRTUAL_ROOM

	public virtual void OnLevelWasLoaded(int id)
	{
		
		//Tắt đồ họa
		homeUICamera = GameObject.Find("Canvas/Main Camera");
		if (homeUICamera != null) {
			homeUICamera.SetActive (false);
			roomVIPAudioListener = transform.root.FindChild ("roomVIPAudioListener");

			if (roomVIPAudioListener == null) {
				roomVIPAudioListener = new GameObject ("roomVIPAudioListener").transform;
				roomVIPAudioListener.SetParent (transform.root);
				roomVIPAudioListener.gameObject.AddComponent<AudioListener> ();
				AudioListener.pause = true;
				AudioListener.volume = 0;
			}
		} else {
			roomVIPCamera = Instantiate (Resources.Load ("roomVIPCamera")) as GameObject;
			roomVIPCamera.SetActive (false);
		}
	
	}

	void Update() {
		if (Input.GetKeyUp("r")) {
			AppRestart ();
		}

		if (Input.GetKeyUp("s")) {
			Debug.Log ("OnOffGraphic");
			currentGraphicState = !currentGraphicState;
			OnOffGraphic(currentGraphicState);
		}
	}

	void OnOffGraphic(bool isOn) {
		Debug.Log ("OnOffGraphic ");
		//Tắt đồ họa


		if (homeUICamera != null) {
			Debug.Log ("OnOffGraphic homeUICamera " + isOn.ToString());
			homeUICamera.SetActive(isOn);
		}else if (roomVIPCamera != null) {
			Debug.Log ("OnOffGraphic roomVIPCamera " + isOn.ToString());
			roomVIPCamera.SetActive(isOn);
		}

		if (roomVIPAudioListener != null)  {
			if (isOn) {
				roomVIPAudioListener.gameObject.SetActive(false);
				AudioListener.pause = false;
				AudioListener.volume = 0;
			} else {
				roomVIPAudioListener.gameObject.SetActive(true);
				AudioListener.pause = true;
				AudioListener.volume = 100;
			}
		}

	
	}



	void OnApplicationQuit() {
	#if !UNITY_EDITOR
		if (IsRestarting) { 

			System.Diagnostics.Process process = new System.Diagnostics.Process();
			process.StartInfo.FileName = "roomhost.exe";
			process.StartInfo.Arguments = CSVR_GameSetting.username + " " + CSVR_GameSetting.password  + " " + System.Diagnostics.Process.GetCurrentProcess ().Id.ToString();
			process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
			/*
		//use to create no window when running cmd script
			process.StartInfo.UseShellExecute = true;
			process.StartInfo.CreateNoWindow = true;
			process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			*/
			//process.Start();

			Thread th= new Thread(() =>
				{
					process.Start();
					process.WaitForExit();
				});
			th.Start();

		}
	#endif
		#if VIRTUAL_ROOM
			CSVR_GameSetting.SaveSetting();
		#endif

	}

	public void AppRestart() {
		Debug.Log ("restart app");
		IsRestarting = true;
		Application.Quit ();
	}
	#endif
}
