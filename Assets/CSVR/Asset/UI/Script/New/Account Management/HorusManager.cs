﻿using Horus;
using LitJson;
using Horus.ClientModels;
using ProgressBar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorusManager : MonoBehaviour {

	public static HorusManager instance;

	public Dictionary<string, string> titleData { get; set; }
	public string playerID { get; set; }
	public string playerUserName { get; set; }
	public string userFacebookInfo { get; set; }
	public string userAPIKey { get; set; }
    public ClanInfo Clan { get; set; }

    public GameObject gameModeInstance = null;

	public ProgressBarBehaviour progressBar = null;

	CSVR_MPConnection mpConnectionInstance;

	ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback;

	AsyncOperation async;

	void Awake(){
		instance = this;
        Clan = new ClanInfo();
	}
	public void CreateNewUser(string username, string password,string email,string phoneNumber,ProjectDelegates.PlayFabLoginCallback OnUserCreatedCallback)
	{

		if (username != null) {
			CSVR_GameSetting.username = username;
		}

		if (password != null) {
			CSVR_GameSetting.password = password;
		}
			
		RegisterHorusUserRequest request = new RegisterHorusUserRequest();
		request.UserName = username;
		request.Password = password;
		//request.Email = email;
		//request.PhoneNumber = phoneNumber;


		HorusClientAPI.RegisterHorusUser(request, 
			(result) => {
				LoginWithPlayFab(username,password,OnUserCreatedCallback);
			},
			(error) => {
//				if(error.Error = HorusErrorCode.CreateAccountError){
//					
//				}
				ErrorView.instance.ShowPopupError ("Đăng kí thất bại, vui lòng kiểm tra thông tin");
			}
		);
	}

	public void LoginWithFacebook(string facebookAccessToken, ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback)
	{
		this.OnLoginCompletedCallback = OnLoginCompletedCallback;

		LoginWithFacebookRequest facebookRequest = new LoginWithFacebookRequest();
		facebookRequest.access_token = facebookAccessToken;

		//HorusClientAPI.LoginWithFacebook(facebookRequest, OnLoginCompleted, OnError);
		HorusClientAPI.LoginWithFacebook(facebookRequest, 
			(result) => {
				OnLoginCompleted(result);
				#if !VIRTUAL_ROOM
				//PlayerPrefs.SetString ("CSVR_GameSetting.isAutoLogin", "true");
				PlayerPrefs.SetInt ("LoginType", 2);
				#endif
			},
			(error) => {
				PlayerPrefs.DeleteKey("CSVR_GameSetting.username");
				PlayerPrefs.DeleteKey("CSVR_GameSetting.password");
				PlayerPrefs.DeleteKey("CSVR_GameSetting.isAutoLogin");
				PlayerPrefs.DeleteKey("LoginType");

				ErrorView.instance.ShowPopupError ("Đăng nhập thất bại, vui lòng kiểm tra thông tin");
			}
		);
	}

	public void LoginWithPlayFab(string username, string password, ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback)
	{


		loadInfoPlayerComplete = false;
		this.OnLoginCompletedCallback = OnLoginCompletedCallback;

		CSVR_GameSetting.username = username;
		CSVR_GameSetting.password = password;

		LoginWithHorusRequest request = new LoginWithHorusRequest();
		request.UserName = username;
		request.Password = password;

		HorusClientAPI.LoginWithHorus(request, 
			(result) => {
				OnLoginCompleted(result);
				#if !VIRTUAL_ROOM
				PlayerPrefs.SetString ("CSVR_GameSetting.username", username);
				PlayerPrefs.SetString ("CSVR_GameSetting.password", password);
				//PlayerPrefs.SetString ("CSVR_GameSetting.isAutoLogin", "true");
				PlayerPrefs.SetInt ("LoginType", 1);
				#endif
			},
			(error) => {
				PlayerPrefs.DeleteKey("CSVR_GameSetting.username");
				PlayerPrefs.DeleteKey("CSVR_GameSetting.password");
				PlayerPrefs.DeleteKey("CSVR_GameSetting.isAutoLogin");
				PlayerPrefs.DeleteKey("LoginType");

				ErrorView.instance.ShowPopupError ("Đăng nhập thất bại, vui lòng kiểm tra thông tin");
			}
		);
	}

	void OnLoginCompleted(LoginResult result)
	{
		HorusClientAPI.GetAPIKey (
			new GetAPIKeyRequest (){ }, 
			(resultChild) => {
				userAPIKey = resultChild.APIKey;
			},
			(error) => {

			}
		);

		InvokeRepeating ("KeepCookie", 600, 600);

		try
        {
			userFacebookInfo = "groups/csmvietnam/";
		}catch{
			userFacebookInfo = "groups/csmvietnam/";
		}
		string displayName = "";
		if (result.DisplayName != null) {
			displayName = result.DisplayName;
			CSVR_GameSetting.hostName = displayName;
			HomeScreen.states = States.Home;
		} else {
			HomeScreen.states = States.UpdateName;
		}

		if (result.imei == null) {
			
			UpdateUserInfoRequest requestUpdate = new UpdateUserInfoRequest();
			requestUpdate.UserInfo = new Dictionary<string, string>();
			requestUpdate.UserInfo.Add("imei", SystemInfo.deviceUniqueIdentifier);

			HorusClientAPI.UpdateUserInfo(requestUpdate,
				(resultUpdate) => {

				},
				(error) => {
					
				}
			);
		}
		
		CSVR_GameSetting.accountLevel = result.AccountLevel;
		CSVR_GameSetting.accountExp = result.AccountExp;
		CSVR_GameSetting.accountKill = result.AccountKill;
		CSVR_GameSetting.accountHeadShot = result.AccountHeadShot;
		CSVR_GameSetting.accountDead = result.AccountDead;
		CSVR_GameSetting.accountLost = result.AccountLost;
		CSVR_GameSetting.accountWin = result.AccountLost;
		
		playerID = result._id;
		playerUserName = result.UserName;
		CSVR_GameSetting.LastLogin = result.last_api_login;
	
		//Debug.Log ("result.Inventory.Count"+result.Inventory.Count);
		int currencyGC = 0;
		try{
			currencyGC = (int)result.Inventory.Find(x => x.ItemId.Contains(GameConstants.VNITEMID_Coin)).RemainingUses;
		}catch{

		}
		int currencyGD = 0;
		try{
			currencyGD = (int)result.Inventory.Find(x => x.ItemId.Contains(GameConstants.VNITEMID_Gold)).RemainingUses;
		}catch{

		}

		ItemSpecific currencyMoney = new ItemSpecific ();
		try{
			currencyMoney = result.accounts[0];
		}catch{

		}
		AccountManager.instance.dailyGame = result.dailyGame;
		AccountManager.instance.gameMoneyAmount = currencyMoney;
		AccountManager.instance.roles = result.roles;


        if (result.clan != null && result.clan.ToString() != "")
        {
            Clan = JsonMapper.ToObject<ClanInfo>(result.clan.ToString());
        } else
        {
            Clan = new ClanInfo("");
        }
	
		OnLoginCompletedCallback(result.Inventory, displayName, currencyGC, currencyGD, 10, Clan);

		GetAllCatalogItems();
		LoadTitleData ();
	}

	private void KeepCookie(){
		HorusClientAPI.KeepLogin (
			new KeepLoginRequest (){ }, 
			(result) => {
			},
			(error) => {

			}
		);
	}


	byte ErrorGetAllCatalogItemsCount = CSVR_GameSetting.errorCountCallBack;
	public void GetAllCatalogItems()
	{
		GetCatalogItemsRequest request = new GetCatalogItemsRequest();
		request.CatalogVersion = "Item";
		HorusClientAPI.GetCatalogItems(request, OnCatalogItemsLoaded, OnCatalogItemsLoadedError);
	}
	private void OnCatalogItemsLoaded(GetCatalogItemsResult result)
	{

		CSVR_GameSetting.shopItems = result.Catalog;
		CSVR_GameSetting.shopItems.Sort ();
		CSVR_GameSetting.shopMeleGun = new List<CatalogItem>();
		CSVR_GameSetting.shopPistolGun = new List<CatalogItem>();
		CSVR_GameSetting.shopGrenadeGun = new List<CatalogItem>();
		CSVR_GameSetting.shopArmorGun = new List<CatalogItem>();
		CSVR_GameSetting.shopMainGun = new List<CatalogItem>();
		CSVR_GameSetting.shopBag = new List<CatalogItem>();

		for (int x = 0; x < CSVR_GameSetting.shopItems.Count; x++) {
			if(CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_MeleeWeapon)
				CSVR_GameSetting.shopMeleGun.Add(CSVR_GameSetting.shopItems[x]);
			else if(CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_PistolWeapon)
				CSVR_GameSetting.shopPistolGun.Add(CSVR_GameSetting.shopItems[x]);
			else if(CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_GrenadeWeapon 
				||CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_OneGrenadeWeapon 
				||CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_TwoGrenadeWeapon )
				CSVR_GameSetting.shopGrenadeGun.Add(CSVR_GameSetting.shopItems[x]);
			else if(CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_ArmorWeapon
				||CSVR_GameSetting.shopItems[x].ItemClass == GameConstants.VNITEMCLASS_HelmetWeapon )
				CSVR_GameSetting.shopArmorGun.Add(CSVR_GameSetting.shopItems[x]);
			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Bag.Item")
				CSVR_GameSetting.shopBag.Add(CSVR_GameSetting.shopItems[x]);
			else 
				CSVR_GameSetting.shopMainGun.Add(CSVR_GameSetting.shopItems[x]);

		};
		GetCatalogCharacter ();
		GetCatalogAccessories ();
		GetCatalogItemsUpgrade ();
		GetCatalogItemsPeriod ();
		ConnectPhoton ();

	}
	private void OnCatalogItemsLoadedError(HorusError error)
	{
		if(ErrorGetAllCatalogItemsCount > 0){
			ErrorGetAllCatalogItemsCount--;
			Invoke ("GetAllCatalogItems", 0.5f);
			//GetAllCatalogItems();
		}
	}

	private bool loadInfoPlayerComplete = false;
	public void OnLoadInfoPlayerComplete(){
		loadInfoPlayerComplete = true;
		async.allowSceneActivation = true;
	}

	byte ErrorGetCatalogItemsUpgradeCount = CSVR_GameSetting.errorCountCallBack;
	private void GetCatalogItemsUpgrade()
	{
		HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = "Upgrade"
			}, 
			(resultUpgrade) => {
				ErrorGetCatalogItemsUpgradeCount = CSVR_GameSetting.errorCountCallBack;
				CSVR_GameSetting.upgradeItems = resultUpgrade.Catalog;
			},
			(error) => {
				if(ErrorGetCatalogItemsUpgradeCount > 0){
					ErrorGetCatalogItemsUpgradeCount--;
					//GetCatalogItemsUpgrade();
					Invoke ("GetCatalogItemsUpgrade", 0.5f);
				}

			}
		);
	}

	byte ErrorGetCatalogAccessoriesCount = CSVR_GameSetting.errorCountCallBack;
	private void GetCatalogAccessories()
	{
		HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = "accessories"
			}, 
			(resultAccessories) => {
				ErrorGetCatalogItemsUpgradeCount = CSVR_GameSetting.errorCountCallBack;
				CSVR_GameSetting.shopAccessories = resultAccessories.Catalog;
			},
			(error) => {
				if(ErrorGetCatalogItemsUpgradeCount > 0){
					ErrorGetCatalogItemsUpgradeCount--;
					Invoke ("GetCatalogAccessories", 0.5f);
				}

			}
		);
	}

	byte ErrorGetCatalogChractersCount = CSVR_GameSetting.errorCountCallBack;
	private void GetCatalogCharacter()
	{
		HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = "Character"
			}, 
			(resultAccessories) => {
				ErrorGetCatalogChractersCount = CSVR_GameSetting.errorCountCallBack;
				CSVR_GameSetting.shopCharacter = resultAccessories.Catalog;
			},
			(error) => {
				if(ErrorGetCatalogChractersCount > 0){
					ErrorGetCatalogChractersCount--;
					Invoke ("GetCatalogCharacter", 0.5f);
				}

			}
		);
	}

	byte ErrorGetCatalogItemsItemPeriodCount = CSVR_GameSetting.errorCountCallBack;
	private void GetCatalogItemsPeriod()
	{
		HorusClientAPI.GetCatalogItems(
			new GetCatalogItemsRequest(){
				CatalogVersion = "ItemPeriod"
			}, 
			(resultItemPeriod) => {
				ErrorGetCatalogItemsItemPeriodCount = CSVR_GameSetting.errorCountCallBack;
				CSVR_GameSetting.shopItemsPeriod = resultItemPeriod.Catalog;
			},
			(error) => {
				if(ErrorGetCatalogItemsItemPeriodCount > 0){
					ErrorGetCatalogItemsItemPeriodCount--;
					Invoke ("GetCatalogItemsPeriod", 0.5f);
				}
			}
		);
	}


	byte ErrorGetTitleDataCount = CSVR_GameSetting.errorCountCallBack;
	void LoadTitleData()
	{
		HorusClientAPI.GetTitleData(
			new GetTitleDataRequest(){
			}, 
			(result) => {
				ErrorGetTitleDataCount = CSVR_GameSetting.errorCountCallBack;
				OnTitleDataLoadSuccess(result);
			},
			(error) => {
				if(ErrorGetTitleDataCount > 0){
					ErrorGetTitleDataCount--;
					//LoadTitleData();
					Invoke ("LoadTitleData", 0.5f);
				}
			}
		);
	}

	private void OnTitleDataLoadSuccess(GetTitleDataResult result)
	{
		
		titleData = result.Data;
//		Debug.Log("OnTitleDataLoadSuccess."+ GetTitleValue ("Key_GameUpdate"));

		#if !VIRTUAL_ROOM && !UNITY_IPHONE
		if(GetTitleValue ("Key_GameUpdate") != Application.version){
			async = Application.LoadLevelAsync("Update");
			async.allowSceneActivation = loadInfoPlayerComplete;
		}else{

			async = Application.LoadLevelAsync("Home");
			async.allowSceneActivation = loadInfoPlayerComplete;
		}
		#else
		async = Application.LoadLevelAsync("Home");
		async.allowSceneActivation = false;
		#endif

	}

	public string GetTitleValue(string key)
	{
		string value = "";
		if (titleData != null)
		{
			titleData.TryGetValue(key, out value);
		}
		return value;
	}

	private void OnError(HorusError error)
	{
		//Debug.Log("HorusError."+ error);
		ErrorView.instance.ShowPopupError ("Error: "+error.Error);
	}

	void ConnectPhoton()
	{
		string server = GetTitleValue ("Key_UseServer");


		if (server == "3") {
			ErrorView.instance.ShowPopupError ("Server đang bảo trì, cảm ơn bạn đã ủng hộ game Việt\nXin bạn quay lại sau...");
			return;
		}


	//#if VIRTUAL_ROOM
		mpConnectionInstance = gameModeInstance.GetComponentInChildren<CSVR_MPConnection> ();
		mpConnectionInstance.SelectedPhotonServer ();
		mpConnectionInstance.Connect ();
	//#endif

		progressBar.IncrementValue (20f);
	}

}
