//using PlayFab;
//using PlayFab.ClientModels;
//using ProgressBar;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//public class PlayFabManager : MonoBehaviour
//{
//	/// <summary>
//	/// Static instance, enabling the class to be called anywhere in the project.
//	/// </summary>
//	public static PlayFabManager instance;
//	
//	/// <summary>
//	/// All the game news' reference.
//	/// </summary>
//	public List<TitleNewsItem> gameNews ;
//	
//	/// <summary>
//	/// Callback to be called when PlayFab login process completes.
//	/// </summary>
//	ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback;
//	
//	/// <summary>
//	/// Game ID given when the developer creates a new PlayFab game.
//	/// </summary>
//	//[SerializeField]
//	// string playFabGameID;
//	
//	public GameObject gameModeInstance;
//	
//	CSVR_MPConnection mpConnectionInstance;
//	#if DEV_TEST
//	string playFabGameID = "504";
//	public string AppId = "315a21ee-72f6-44e4-be59-04f0b27abb08";
//	#elif PUB_1
//	string playFabGameID = "5120";
//	public string AppId = "101b1ffb-cdc4-4e55-a57f-a2d49ec73086";
//	#else
//	string playFabGameID = "D03D";
//	public string AppId = "08e3e4f4-a135-4c0b-af12-38c092075852";
//	#endif
//	//public string AppId = "6925b83b-1adb-4448-9941-c6823d53ba5d";
//	//public string AppId = "315a21ee-72f6-44e4-be59-04f0b27abb08";
//	
//	
//	/// <summary>
//	/// Reference of all the title data, stored in PlayFab setting.
//	/// </summary>
//	public Dictionary<string, string> titleData { get; private set; }
//	
//	/// <summary>
//	/// User's unique PlayFab ID.
//	/// </summary>
//	public string playerID;
//	
//	/// <summary>
//	/// User's unique PlayFab username.
//	/// </summary>
//	public string playerUsername { get; private set; }
//	
//	/// <summary>
//	/// Photon authetication token
//	/// </summary>
//	public string playerPhotonToken  {get; set; }
//	
//	
//	/// <summary>
//	/// User's display name (facebook name).
//	/// </summary>
//	public string playerDisplayName { get; private set; }
//	
//	public string userFacebookInfo { get; private set;}
//	/// <summary>
//	/// User's facebook picture URL.
//	/// </summary>
//	string playerPictureURL;
//	
//	/// <summary>
//	/// Callback to be called when getting leaderboards process completes.
//	/// </summary>
//	ProjectDelegates.PlayFabLeaderboardCallback OnLeaderboardLoadedCallback;
//	
//	/// <summary>
//	/// Callback to be called when getting store catalog process completes.
//	/// </summary>
//	//    ProjectDelegates.PlayFabCatalogListCallback OnCatalogLoadedCallback;
//	
//	/// <summary>
//	/// Callback to be called when buying item with virtual currency process completes.
//	/// </summary>
//	ProjectDelegates.PlayFabItemBuyCallback OnBuySuccessCallback;
//	/// <summary>
//	/// Store's catalog items.
//	/// </summary>
//	List<CatalogItem> catalogItems;
//	
//	/// <summary>
//	/// Number to be added to the player's name, if there's another player with the same name.
//	/// </summary>
//	int userNameIndex;
//	public ErrorView errorView = null;
//	public ProgressBarBehaviour progressBar = null;
//	
//	void Awake()
//	{
//		instance = this;
//	}
//	
//	/// <summary>
//	/// Function to be called when the user will create a new account from scratch.
//	/// </summary>
//	/// <param name="username">User's chosen username</param>
//	/// <param name="password">User's chosen password</param>
//	/// <param name="email">User's  chosen email</param>
//	/// <param name="OnUserCreatedCallback">Function to call after process ends.</param>
//	public void CreateNewUser(string username, string password, string email, ProjectDelegates.PlayFabLoginCallback OnUserCreatedCallback)
//	{
//		if (username != null) {
//			CSVR_GameSetting.username = username;
//		}
//		
//		if (password != null) {
//			CSVR_GameSetting.password = password;
//		}
//		
//		this.OnLoginCompletedCallback = OnUserCreatedCallback;
//		
//		PlayFabSettings.TitleId = playFabGameID;
//		
//		RegisterPlayFabUserRequest request = new RegisterPlayFabUserRequest();
//		request.Username = username;
//		request.Password = password;
//		request.Email = email;
//		request.TitleId = playFabGameID;
//		
//		//Debug.Log ("username " + username + "password " + password + "email " + email + "playFabGameID " + playFabGameID);
//		PlayFabClientAPI.RegisterPlayFabUser(request, OnRegistrationCompleted, OnLoginError);
//		
//		
//	}
//	
//	/// <summary>
//	/// Function used to login an existing player with PlayFab.
//	/// </summary>
//	/// <param name="username">User's username</param>
//	/// <param name="password">User's password</param>
//	/// <param name="OnLoginCompletedCallback">Function to call after process ends</param>
//	public void LoginWithPlayFab(string username, string password, ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback)
//	{
//		
//		#if !VIRTUAL_ROOM
//		PlayerPrefs.SetString ("CSVR_GameSetting.username", username);
//		PlayerPrefs.SetString ("CSVR_GameSetting.password", password);
//		#endif
//		//	Debug.Log ("LoginWithCSVR");
//		//	Debug.Log (PlayerPrefs.GetString ("CSVR_GameSetting.username"));
//		//	Debug.Log (PlayerPrefs.GetString ("CSVR_GameSetting.password"));
//		
//		//		Debug.Log ("username " + username + "password" + password);
//		this.OnLoginCompletedCallback = OnLoginCompletedCallback;
//		
//		PlayFabSettings.TitleId = playFabGameID;
//		
//		LoginWithPlayFabRequest request = new LoginWithPlayFabRequest();
//		request.Username = username;
//		request.Password = password;
//		request.TitleId = playFabGameID;
//		
//		CSVR_GameSetting.username = username;
//		CSVR_GameSetting.password = password;
//
//		PlayFabClientAPI.LoginWithPlayFab(request, OnLoginCompleted, OnLoginError);
//	}
//	
//	/// <summary>
//	/// Function to be called to link Facebook user with PlayFab user 
//	/// (creates a new one if not exists, logins existing user if exists).
//	/// </summary>
//	/// <param name="facebookAccessToken">Facebook's access token</param>
//	/// <param name="OnLoginCompletedCallback">Function to call after process ends</param>
//	public void LoginWithFacebook(string facebookAccessToken, ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback)
//	{
//		this.OnLoginCompletedCallback = OnLoginCompletedCallback;
//		
//		PlayFabSettings.TitleId = playFabGameID;
//		
//		LoginWithFacebookRequest facebookRequest = new LoginWithFacebookRequest();
//		facebookRequest.CreateAccount = true;
//		facebookRequest.TitleId = playFabGameID;
//		facebookRequest.AccessToken = facebookAccessToken;
//		
//		PlayFabClientAPI.LoginWithFacebook(facebookRequest, OnLoginCompleted, OnLoginError);
//	}
//	
//	/// <summary>
//	/// Function to be called to link Google user with PlayFab user 
//	/// (creates a new one if not exists, logins existing user if exists).
//	/// </summary>
//	/// <param name="googleAccessToken">Facebook's access token</param>
//	/// <param name="OnLoginCompletedCallback">Function to call after process ends</param>
//	public void LoginWithGoogle(string googleAccessToken, ProjectDelegates.PlayFabLoginCallback OnLoginCompletedCallback)
//	{
//		this.OnLoginCompletedCallback = OnLoginCompletedCallback;
//		
//		
//		PlayFabSettings.TitleId = playFabGameID;
//		
//		LoginWithGoogleAccountRequest googleRequest = new LoginWithGoogleAccountRequest();
//		googleRequest.CreateAccount = true;
//		googleRequest.TitleId = playFabGameID;
//		googleRequest.AccessToken = googleAccessToken;
//		
//		PlayFabClientAPI.LoginWithGoogleAccount(googleRequest, OnLoginCompleted, OnLoginError);
//	}
//	
//	
//	/// <summary>
//	/// Removes all references from playFab user.
//	/// </summary>
//	public void Logout()
//	{
//		playerID = "";
//		playerUsername = "";
//	}
//	
//	/// <summary>
//	/// Callback called when new user creation completes.
//	/// </summary>
//	/// <param name="result">Result of user creation</param>
//	void OnRegistrationCompleted(RegisterPlayFabUserResult result)
//	{
//		CSVR_GameSetting.SaveSetting ();
//		playerID = result.PlayFabId;
//		playerDisplayName = playerID;
//		this.LoadTitleData();
//		
//		OnGrantItemToPlayer ();
//		//	ConnectPhoton ();
//		
//		Dictionary<string, string> playerData = new Dictionary<string, string>();
//		playerData.Add(CSVR_GameSetting.accountExpKey, "0");
//		
//		UpdateUserDataRequest request = new UpdateUserDataRequest();
//		request.Data = playerData;
//		request.Permission = UserDataPermission.Public;
//		
//		PlayFabClientAPI.UpdateUserData(request, OnAddDataSuccess, OnAddDataError);
//		
//	}
//	//	public String getMacAddress(Context context) {
//	//		WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//	//		String macAddress = wimanager.getConnectionInfo().getMacAddress();
//	//		if (macAddress == null) {
//	//			macAddress = "Device don't have mac address or wi-fi is disabled";
//	//		}
//	//		return macAddress;
//	//	}
//	/// <summary>
//	/// Callback called when user login completes.
//	/// </summary>
//	/// <param name="result">Result of user login</param>
//	void OnLoginCompleted(LoginResult result)
//	{
//		//Debug.Log ("OnLoginCompleted");
//		CSVR_GameSetting.SaveSetting ();
//		playerID = result.PlayFabId;
//		playerDisplayName = playerID;
//		
//		
//		//Bat dau ket noi chat
//		CSVR_PlayerMessenger messenger = gameObject.GetComponent<CSVR_IRCPlayerMessenger>();
//
//		if (messenger == null) {
//			messenger =	gameObject.AddComponent<CSVR_IRCPlayerMessenger> ();
//		}
//		messenger.userName = "u"+result.PlayFabId;
//
//		this.LoadTitleData();
//		
//		//	ConnectPhoton ();
//		
//		if (result.NewlyCreated)
//		{
//			// trao item cho nhan vat
//			OnGrantItemToPlayer();
//			
//			Dictionary<string, string> playerData = new Dictionary<string, string>();
//			playerData.Add(CSVR_GameSetting.accountExpKey, "0");
//			
//			UpdateUserDataRequest request = new UpdateUserDataRequest();
//			request.Data = playerData;
//			request.Permission = UserDataPermission.Public;
//			
//			PlayFabClientAPI.UpdateUserData(request, OnAddDataSuccess, OnAddDataError);
//			
//		}
//		
//		GetAccountInfoRequest requestAI = new GetAccountInfoRequest();
//		requestAI.PlayFabId = playerID;
//		PlayFabClientAPI.GetAccountInfo(requestAI, GetAccountInfoSuccess, OnAddDataError);
//	}
//	void ConnectPhoton()
//	{
//		string server = GetTitleValue ("Key_UseServer");
//
//		#if DEV_TEST
//		if(server == "3"){
//			ErrorView.instance.ShowPopupError("Server đang bảo trì, cảm ơn bạn đã ủng hộ game Việt\nXin bạn quay lại sau...");
//			return;
//		}
//			#if CSVR_PHOTON_CLOUD_EU
//			PhotonNetwork.PhotonServerSettings.UseCloud ("315a21ee-72f6-44e4-be59-04f0b27abb08",CloudRegionCode.eu);
//			#elif CSVR_PHOTON_CLOUD_US
//			PhotonNetwork.PhotonServerSettings.UseCloud ("315a21ee-72f6-44e4-be59-04f0b27abb08",CloudRegionCode.us);
//			#elif CSVR_PHOTON_CLOUD_ASIA
//			PhotonNetwork.PhotonServerSettings.UseCloud ("315a21ee-72f6-44e4-be59-04f0b27abb08",CloudRegionCode.asia);
//			#elif CSVR_PHOTON_CLOUD_JP
//			PhotonNetwork.PhotonServerSettings.UseCloud ("315a21ee-72f6-44e4-be59-04f0b27abb08",CloudRegionCode.jp);
//			#elif CSVR_PHOTON_CLOUD_AU
//			PhotonNetwork.PhotonServerSettings.UseCloud ("315a21ee-72f6-44e4-be59-04f0b27abb08",CloudRegionCode.au);			
//			#else
//			//PhotonNetwork.PhotonServerSettings.UseMyServer ("192.168.0.103", 5055, "master");
//			//PhotonNetwork.PhotonServerSettings.UseMyServer ("103.53.168.68", 5055, "315a21ee-72f6-44e4-be59-04f0b27abb08");
//			PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "101b1ffb-cdc4-4e55-a57f-a2d49ec73086");
//			#endif
//		#elif PUB_1
//		if(server == "3"){
//			ErrorView.instance.ShowPopupError("Server đang bảo trì, cảm ơn bạn đã ủng hộ game Việt\nXin bạn quay lại sau...");
//			return;
//		}else if (server == "1") {
//			PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "101b1ffb-cdc4-4e55-a57f-a2d49ec73086");
//		} else if (server == "2") {
//			PhotonNetwork.PhotonServerSettings.UseCloud("101b1ffb-cdc4-4e55-a57f-a2d49ec73086",CloudRegionCode.au);
//		}
//		#else
//		if(server == "3"){
//			ErrorView.instance.ShowPopupError("Server đang bảo trì, cảm ơn bạn đã ủng hộ game Việt\nXin bạn quay lại sau...");
//			return;
//		}else if (server == "1") {
//			PhotonNetwork.PhotonServerSettings.UseMyServer ("192.168.0.100", 5055, "master");
//			//PhotonNetwork.PhotonServerSettings.UseMyServer ("43.239.220.6", 5055, "08e3e4f4-a135-4c0b-af12-38c092075852");
//		} else if (server == "2") {
//			PhotonNetwork.PhotonServerSettings.UseCloud("08e3e4f4-a135-4c0b-af12-38c092075852",CloudRegionCode.asia);
//		}
//		#endif
//		//		PhotonNetwork.PhotonServerSettings.UseCloud("08e3e4f4-a135-4c0b-af12-38c092075852",CloudRegionCode.asia);
//		mpConnectionInstance = gameModeInstance.GetComponentInChildren<CSVR_MPConnection>();
//		mpConnectionInstance.Connect ();
//		try{
//			progressBar.IncrementValue (20f);
//			progressBar = null;
//		}catch{
//			progressBar.GetComponent<ProgressBarBehaviour>().IncrementValue(20f);
//			progressBar = null;
//		}
//	}
//	public void TestCreate() {
//		//		for (int i =0; i< gameModeInstance.transform.childCount; i++) {
//		//			gameModeInstance.transform.GetChild(i).gameObject.SetActive(false);
//		//		}
//		
//		gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(true);
//		ChoiseMode (CSVR_GameSetting.modeName);
//		//		vp_MPMaster.Instance.CurrentLevel = CSVR_GameSetting.roomName;
//		//		vp_MPMaster.Instance.GameLength = CSVR_GameSetting.timeMatch;
//		//		gameModeInstance.transform.FindChild(CSVR_GameSetting.modeGame).GetComponent<vp_TDMMaster>().CurrentLevel = CSVR_GameSetting.roomName;
//		//		gameModeInstance.transform.FindChild(CSVR_GameSetting.modeGame).GetComponent<vp_TDMMaster>().GameLength = CSVR_GameSetting.timeMatch;
//	}
//    //public void ChoiseMasterClient(){
//    //    PhotonPlayer chosenNewMaster = mpConnectionInstance.GetNewMasterCandidateLag ();
//    //    if (chosenNewMaster != null && chosenNewMaster.ID != PhotonNetwork.masterClient.ID) {
//    //        PhotonNetwork.SetMasterClient (chosenNewMaster);
//    //    }
//    //}
//	public void TestPlay() {
//		mpConnectionInstance.SetReady();
//	}
//	
//	public void JoinRoom() {
//		if ( CSVR_GameSetting.mapName != "") {
//			//applicationState = CSVR_ApplicationState.MULTIPLAYER_JOINING_ROOM;
//			mpConnectionInstance.OnQuickJoinedRoom(true);
//			//			vp_MPMaster.Instance.CurrentLevel = CSVR_GameSetting.roomName;
//			//			gameModeInstance.transform.FindChild(CSVR_GameSetting.modeGame).GetComponent<vp_TDMMaster>().CurrentLevel = CSVR_GameSetting.roomName;
//			ChoiseMode (CSVR_GameSetting.modeName);
//			mpConnectionInstance.JoinRoom ();
//			
//		}
//	}
//	public void CreateRoom() {
//		mpConnectionInstance.OnQuickJoinedRoom(false);
//
//		ChoiseMode (CSVR_GameSetting.modeName);
//		if (CSVR_GameSetting.UseRoomVIP) {
//			CSVR_RoomHostService.TryingRoomVIP = true;
//
//			PhotonNetwork.JoinRandomRoom(); //Tim room o default lobby, chinh la lobby chua roomVIP
//		} else {
//			mpConnectionInstance.CreateNewRoom((byte)CSVR_GameSetting.maxPlayer,CSVR_GameSetting.modeName,CSVR_GameSetting.mapName,CSVR_GameSetting.accountLevel);
//		}
//		//cameraUI.SetActive (false);
//	}
//	private void ChoiseMode(string mode){
//		//Debug.Log (CSVR_GameSetting.modeGame);
//		switch (mode)
//		{
//		case "CSVR_Deathmatch":
//			vp_DMMaster dmMaster = gameModeInstance.transform.FindChild(CSVR_GameSetting.modeName).GetComponent<vp_DMMaster>();
//			dmMaster.CurrentLevel = CSVR_GameSetting.mapName;
//			dmMaster.GameLength = CSVR_GameSetting.timeMatch;
//			break;
//		case "CSVR_TeamDeathmatch":
//			vp_TDMMaster tdmMaster = gameModeInstance.transform.FindChild(CSVR_GameSetting.modeName).GetComponent<vp_TDMMaster>();
//			tdmMaster.CurrentLevel = CSVR_GameSetting.mapName;
//			tdmMaster.GameLength = CSVR_GameSetting.timeMatch;
//			break;
//		case "CSVR_SearchAndDestroy":
//			vp_SADMaster sadMaster = gameModeInstance.transform.FindChild(CSVR_GameSetting.modeName).GetComponent<vp_SADMaster>();
//			sadMaster.CurrentLevel = CSVR_GameSetting.mapName;
//			break;
//		case "CSVR_DeadOrAlive":
//			//Debug.Log (CSVR_GameSetting.modeGame);
//			vp_DOAMaster doaMaster = gameModeInstance.transform.FindChild(CSVR_GameSetting.modeName).GetComponent<vp_DOAMaster>();
//			doaMaster.CurrentLevel = CSVR_GameSetting.mapName;
//			break;
//		case "CSVR_BaseCapture":
//			vp_BCMaster bcMaster = gameModeInstance.transform.FindChild(CSVR_GameSetting.modeName).GetComponent<vp_BCMaster>();
//			bcMaster.CurrentLevel = CSVR_GameSetting.mapName;
//			bcMaster.GameLength = CSVR_GameSetting.timeMatch;
//			break;
//		}
//	}
//	public void BackToLobby()
//	{
//		mpConnectionInstance.LeaveRoom();
//	}
//	public void MapLoading()
//	{
//		CSVR_UILoadingMap.instance.HienThi();
//	}
//	public void ChangeInfo()
//	{
//		
//		mpConnectionInstance.OnInfoChanged();
//	}
//	public void ChangeTeam(TeamTypes newTeam, bool syncNetwork)
//	{
//		mpConnectionInstance.ChangeTeam(newTeam,syncNetwork);
//	}
//	void GetAccountInfoSuccess(GetAccountInfoResult result)
//	{
//		try{
//			userFacebookInfo = result.AccountInfo.FacebookInfo.FacebookId;
//		}catch{
//			userFacebookInfo = "groups/csmvietnam/";
//		}
//		
//		if(result.AccountInfo.TitleInfo.DisplayName == playerID){
//			HomeScreen.states = States.UpdateName;
//		}else{
//			HomeScreen.states = States.Home;
//		}
//		
//#if !VIRTUAL_ROOM
//		//CSVR_GameSetting.LastLogin = (System.DateTime)result.AccountInfo.TitleInfo.LastLogin;
//		InvokeRepeating("CheckSessionTicket", 60, 60);
//		OnRewardKillHeadShotInDay (0);
//#endif
//		PlayFabClientAPI.GetUserCombinedInfo(new GetUserCombinedInfoRequest(), OnGetUserCombinedInfoResult, OnGetUserCombinedInfoError);
//		
//	}
//	void CheckSessionTicket(){
//		GetAccountInfoRequest requestAI = new GetAccountInfoRequest();
//		requestAI.PlayFabId = playerID;
//		PlayFabClientAPI.GetAccountInfo(requestAI, CheckSessionTicketSuccess, CheckSessionTicketError);
//	}
//	
//	void CheckSessionTicketSuccess(GetAccountInfoResult result){
//		//		Debug.Log ("Có kết nối mạng");
////		if (CSVR_GameSetting.LastLogin != (System.DateTime)result.AccountInfo.TitleInfo.LastLogin) {
////			CancelInvoke("CheckSessionTicket");
////			errorView.ShowPopupError ("Tài khoản được đăng nhập ở nơi khác.");
////			Invoke("ExitGame",2f);
////		}
//	}
//	void CheckSessionTicketError(PlayFabError error){
//		if (error.Error == PlayFabErrorCode.ServiceUnavailable) {
//			errorView.ShowPopupError ("Bạn đã đứt kết nối đến máy chủ\nVui lòng đăng nhập lại.");
//			Invoke("ExitGame",5f);
//		}
//		
//	}
//	void ExitGame(){
//		PlayerPrefs.SetString ("CSVR_GameSetting.isAutoLogin", "false");
//		vp_Timer time =  FindObjectOfType(typeof(vp_Timer)) as vp_Timer;
//		try {
//			Destroy(time.gameObject);
//		}catch{
//			
//		}
//		GameObject[] gosPolice = GameObject.FindGameObjectsWithTag("PolicePlayer");
//		for (int i = 0; i< gosPolice.Length; i++) {
//			Destroy(gosPolice[i]);
//		}
//		GameObject[] gosTerrorism = GameObject.FindGameObjectsWithTag("TerrorisPlayer");
//		for (int i = 0; i<gosTerrorism.Length; i++) {
//			Destroy(gosTerrorism[i]);
//		}
//		AccountManager acc =  FindObjectOfType(typeof(AccountManager)) as AccountManager;
//		Destroy(acc.gameObject);
//		
//		PhotonNetwork.Disconnect ();
//		
//		SceneLoader.sceneToLoad = Scenes.Login;
//		
//		Application.LoadLevel(Scenes.Loading.ToString());
//	}
//	void OnPlayFabError(PlayFabError error)
//	{
//		//Debug.Log ("Got an error: " + error.ErrorMessage);
//	}
//	/// <summary>
//	/// Callback called when an error occurs.
//	/// </summary>
//	/// <param name="error">Error information</param>
//	void OnLoginError(PlayFabError error)
//	{
//		//Debug.Log ("OnLoginError");
//		CSVR_GameSetting.isAutoLogin = false;
//		//		PlayerPrefs.DeleteKey ("CSVR_GameSetting.username");
//		//		PlayerPrefs.DeleteKey ("CSVR_GameSetting.password");
//		string errorMessage = string.Empty;
//		switch (error.Error) {
//		case PlayFabErrorCode.InvalidParams:
//			errorMessage = "Lỗi: Tài khoản không hợp lệ";
//			break;
//		case PlayFabErrorCode.InvalidTitleId:
//			errorMessage = "Lỗi: Kết nối không thành công";
//			break;
//		case PlayFabErrorCode.AccountNotFound:
//			errorMessage = "Lỗi: Tài khoản không tồn tại. Vui lòng đăng kí.";
//			break;
//		case PlayFabErrorCode.AccountBanned:
//			errorMessage = "Tài khoản đã bị khóa. Do gian lận trong quá trình chơi game.";
//			break;
//		case PlayFabErrorCode.InvalidUsernameOrPassword:
//			errorMessage = "Lỗi: Tên đăng nhập hoặc mật khẩu không chính xác. Vui lòng đăng nhập lại.";
//			break;
//		case PlayFabErrorCode.ServiceUnavailable:
//			errorMessage = "Lỗi: Kết nối đến máy chủ thất bại. Vui lòng kiểm tra lại đường truyền.";
//			break;
//		case PlayFabErrorCode.EmailAddressNotAvailable:
//			errorMessage = "Lỗi: Tài khoản email đã được sử dụng. Xin vui lòng sử dụng tài khoản khác";
//			break;
//		case PlayFabErrorCode.InvalidEmailAddress:
//			errorMessage = "Lỗi: Tài khoản email không hợp lệ. Xin vui lòng nhập lại tài khoản email";
//			break;
//		case PlayFabErrorCode.UsernameNotAvailable:
//			errorMessage = "Lỗi: Tên đăng nhập đã được sử dụng. Xin vui lòng sử dụng tên đăng nhập khác";
//			break;
//		case PlayFabErrorCode.InvalidUsername:
//			errorMessage = "Lỗi: Tên đăng nhập không hợp lệ. Xin vui lòng nhập lại tên đăng nhập.";
//			break;
//		case PlayFabErrorCode.InvalidPassword:
//			errorMessage = "Lỗi: Mật khẩu không hợp lệ. Xin vui lòng nhập lại mật khẩu";
//			break;
//		}
//		
//		errorView.ShowPopupError (errorMessage);
//		//loginScreen
//		//errorView.ShowPopupError ("Error: " + error.ErrorMessage);
//		////Debug.Log("Login error: " + error.Error);
//	}
//	
//	/// <summary>
//	/// Callback called if the data saving is successful.
//	/// </summary>
//	/// <param name="result">Update result.</param>
//	void OnAddDataSuccess(UpdateUserDataResult result)
//	{
//		
//		//Everything related to login completed. Time to go back to the manager.
//		// OnLoginCompletedCallback(0, 0);
//		
//		this.UpdateUserDisplayName();
//	}
//	
//	/// <summary>
//	/// Callback called when an error occurs.
//	/// </summary>
//	/// <param name="error">Error information</param>
//	void OnAddDataError(PlayFabError error)
//	{
//		//Debug.Log("Add data error: " + error.Error + " " + error.ErrorMessage);
//	}
//	
//	/// <summary>
//	/// Callback used when getting user data scceeded.
//	/// </summary>
//	/// <param name="result">User data</param>
//	void OnGetUserDataSuccess(GetUserDataResult result)
//	{
//		
//		this.UpdateUserDisplayName();
//	}
//	
//	/// <summary>
//	/// Updates user display name.
//	/// </summary>
//	void UpdateUserDisplayName()
//	{
//		UpdateUserTitleDisplayNameRequest displayNameRequest = new UpdateUserTitleDisplayNameRequest();
//		displayNameRequest.DisplayName = playerDisplayName;
//		PlayFabClientAPI.UpdateUserTitleDisplayName(displayNameRequest, OnUpdatedDisplayName, OnUpdateNameError);
//	}
//	
//	/// <summary>
//	/// Login update name callback.
//	/// </summary>
//	ProjectDelegates.SimpleCallback OnUpdateNameCompleted;
//	public void UpdateUserDisplayNameInHome(string name, ProjectDelegates.SimpleCallback OnUpdateNameCompletedCallback)
//	{
//		OnUpdateNameCompleted = OnUpdateNameCompletedCallback;
//		UpdateUserTitleDisplayNameRequest displayNameRequest = new UpdateUserTitleDisplayNameRequest();
//		displayNameRequest.DisplayName = name;
//		PlayFabClientAPI.UpdateUserTitleDisplayName(displayNameRequest, OnUpdatedDisplayNameInHome, OnUpdateNameErrorInHome);
//	}
//	/// <summary>
//	/// User display name successfully updated.
//	/// </summary>
//	/// <param name="result">Result.</param>
//	void OnUpdatedDisplayName(UpdateUserTitleDisplayNameResult result)
//	{
//		// Debug.Log("Display name update success. new name " + result.DisplayName);
//		playerDisplayName = result.DisplayName;
//		
//		PlayFabClientAPI.GetAccountInfo(new GetAccountInfoRequest(), GetAccountInfoSuccess, OnAddDataError);
//	}
//	void OnUpdatedDisplayNameInHome(UpdateUserTitleDisplayNameResult result)
//	{
//		//Debug.Log("Display name update success. new name " + result.DisplayName);
//		AccountManager.instance.displayName = result.DisplayName;
//		OnUpdateNameCompleted ();
//	}
//	/// <summary>
//	/// User display name update failed.
//	/// Will try again, with a number by the username's end.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	void OnUpdateNameError(PlayFabError error)
//	{
//		// Debug.LogError("Update display name error : " + error.Error + " " + error.ErrorMessage);
//		
//		playerDisplayName = playerDisplayName.Replace(userNameIndex.ToString(), "");
//		userNameIndex++;
//		playerDisplayName += userNameIndex;
//		this.UpdateUserDisplayName();
//	}
//	void OnUpdateNameErrorInHome(PlayFabError error)
//	{
//		//Debug.LogError("Update display name error : " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Update display name error : " + error.Error);
//	}
//	
//	/// <summary>
//	/// Getting user combined info successfully completed.
//	/// </summary>
//	/// <param name="result">User combined info.</param>
//	private void OnGetUserCombinedInfoResult(GetUserCombinedInfoResult result)
//	{
//		playerID = result.PlayFabId;
//		playerUsername = result.AccountInfo.TitleInfo.DisplayName;
//		//		Debug.Log ("playerUsername "+playerUsername);
//		int currencyGC = result.VirtualCurrency["GC"];
//		int currencyGD = result.VirtualCurrency["GD"];
//		if (!result.Data.ContainsKey(CSVR_GameSetting.accountExpKey))
//		{
//			Dictionary<string, string> playerData = new Dictionary<string, string>();
//			playerData.Add(CSVR_GameSetting.accountExpKey, "0");
//			
//			UpdateUserDataRequest request = new UpdateUserDataRequest();
//			request.Data = playerData;
//			request.Permission = UserDataPermission.Public;
//			
//			PlayFabClientAPI.UpdateUserData(request, OnAddDataSuccess, OnAddDataError);
//		}
//		else
//		{
//			int exp = int.Parse(result.Data[CSVR_GameSetting.accountExpKey].Value);
//			CSVR_GameSetting.accountExp = exp;
//			this.GetUserStatistics();
//			
////			OnLoginCompletedCallback(result.Inventory,playerUsername,currencyGC,currencyGD,exp);
//			
//			GetAllCatalogItems();
//		}
//	}
//	
//	/// <summary>
//	/// Getting user combined info failed.
//	/// When failed, will create all necessary keys again, and continue the login process.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	private void OnGetUserCombinedInfoError(PlayFabError error)
//	{
//		//Debug.Log("Get user combined info error: " + error.Error + " " + error.ErrorMessage);
//		
//		Dictionary<string, string> playerData = new Dictionary<string, string>();
//		playerData.Add(CSVR_GameSetting.accountExpKey, "0");
//		UpdateUserDataRequest request = new UpdateUserDataRequest();
//		request.Data = playerData;
//		request.Permission = UserDataPermission.Public;
//		
//		PlayFabClientAPI.UpdateUserData(request, OnAddDataSuccess, OnAddDataError);
//	}
//	
//	/// <summary>
//	/// Callback called when an error occurs.
//	/// </summary>
//	/// <param name="error">Error information</param>
//	void OnGetUserDataError(PlayFabError error)
//	{
//		//Debug.Log("Get user data error: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	/// <summary>
//	/// Method used to get a defined number of users around the current user, based on a defined stat.
//	/// </summary>
//	/// <param name="property">Property to define the leaderboard.</param>
//	/// <param name="maxResults">Maximum number of players to return.</param>
//	/// <param name="OnLeaberboardLoaded">Callback when the leaderboard is loaded.</param>
//	public void GetLeaderboardAroundMe(string property, int maxResults, ProjectDelegates.PlayFabLeaderboardCallback OnLeaberboardLoaded)
//	{
//		OnLeaderboardLoadedCallback = OnLeaberboardLoaded;
//		
//		GetLeaderboardAroundCurrentUserRequest request = new GetLeaderboardAroundCurrentUserRequest();
//		request.StatisticName = property;
//		request.MaxResultsCount = maxResults;
//		
//		PlayFabClientAPI.GetLeaderboardAroundCurrentUser(request, OnGetLeaderboardAroundMeResult, OnGetLeaderboardError);
//	}
//	
//	/// <summary>
//	/// Method used to get a defined number of users starting from a defined position, based on a defined stat.
//	/// </summary>
//	/// /// <param name="property">Property to define the leaderboard.</param>
//	/// <param name="maxResults">Maximum number of players to return.</param>
//	/// /// <param name="startPosition">Position to start getting the leaderboard.</param>
//	/// <param name="OnLeaberboardLoaded">Callback when the leaderboard is loaded.</param>
//	public void GetLeaderboardFromPosition(string property, int maxResults, int startPosition, ProjectDelegates.PlayFabLeaderboardCallback OnLeaberboardLoaded)
//	{
//		OnLeaderboardLoadedCallback = OnLeaberboardLoaded;
//		
//		GetLeaderboardRequest request = new GetLeaderboardRequest();
//		request.StatisticName = property;
//		request.MaxResultsCount = maxResults;
//		request.StartPosition = startPosition;
//		
//		PlayFabClientAPI.GetLeaderboard(request, OnGetLeaderboardFromPositionResult, OnGetLeaderboardError);
//	}
//	
//	/// <summary>
//	/// Callback to be called when loading the leaderboard around the current user completes.
//	/// </summary>
//	/// <param name="result">Leaderboard returned.</param>
//	void OnGetLeaderboardAroundMeResult(GetLeaderboardAroundCurrentUserResult result)
//	{
//		OnLeaderboardLoadedCallback(result.Leaderboard);
//	}
//	
//	/// <summary>
//	/// Callback to be called when loading the leaderboard starting from a defined position completes.
//	/// </summary>
//	/// <param name="result">Leaderboard returned.</param>
//	void OnGetLeaderboardFromPositionResult(GetLeaderboardResult result)
//	{
//		OnLeaderboardLoadedCallback(result.Leaderboard);
//	}
//	
//	/// <summary>
//	/// Callback to be called when leaderboard fails loading.
//	/// </summary>
//	/// <param name="error">Error info.</param>
//	void OnGetLeaderboardError(PlayFabError error)
//	{
//		//Debug.LogError("Error getting leaderboard. Error: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	
//	
//	/// <summary>
//	/// Tries to buy item with virtual currency.
//	/// </summary>
//	/// <param name="item">Item to buy.</param>
//	/// <param name="OnBuySuccess">Callback in case scceeds.</param>
//	public void BuyItemWithVirtualCurrency(CatalogItem item,ProjectDelegates.PlayFabItemBuyCallback OnBuySuccess)
//	{
//		this.OnBuySuccessCallback = OnBuySuccess;
//		
//		PurchaseItemRequest request = new PurchaseItemRequest();
//		request.ItemId = item.ItemId;
//		request.Price = (int)item.VirtualCurrencyPrices["1"];
//		request.VirtualCurrency = "1";
//		
//		PlayFabClientAPI.PurchaseItem(request, OnPurchaseSuccess, OnPurchaseError);
//	}
//	
//	/// <summary>
//	/// Purchase with virtual currency succeeded.
//	/// </summary>
//	/// <param name="result">Purchase info.</param>
//	void OnPurchaseSuccess(PurchaseItemResult result)
//	{
//		this.OnBuySuccessCallback(result.Items);
//	}
//	
//	/// <summary>
//	/// Purchase with virtual currency failed.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	void OnPurchaseError(PlayFabError error)
//	{
//		//Debug.LogError("Error buying item: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	/// <summary>
//	/// Trying to consume item we just bought.
//	/// </summary>
//	/// <param name="itemInstanceID">Item instance ID.</param>
//	public void ConsumeItem(string itemInstanceID)
//	{
//		ConsumeItemRequest request = new ConsumeItemRequest();
//		request.ItemInstanceId = itemInstanceID;
//		request.ConsumeCount = 1;
//		
//		PlayFabClientAPI.ConsumeItem(request, OnConsumeItemSuccess, OnConsumeItemError);
//	}
//	
//	/// <summary>
//	/// Successfully consumed item.
//	/// </summary>
//	/// <param name="result">Result.</param>
//	void OnConsumeItemSuccess(ConsumeItemResult result)
//	{
//		//      Debug.Log("successfully consumed item");
//	}
//	
//	/// <summary>
//	/// Consuming item failed.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	void OnConsumeItemError(PlayFabError error)
//	{
//		//Debug.LogError("Error consuming item: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	
//	/// <summary>
//	/// Method used to get user statistics from PlayFab.
//	/// </summary>
//	public void GetUserStatistics()
//	{
//		GetUserStatisticsRequest request = new GetUserStatisticsRequest();
//		PlayFabClientAPI.GetUserStatistics(request, OnUserStatisticsLoaded, OnGetUserStatisticsError);
//	}
//	
//	/// <summary>
//	/// Callback used when user statistics load scceeds.
//	/// </summary>
//	/// <param name="result">User statistics info.</param>
//	void OnUserStatisticsLoaded(GetUserStatisticsResult result)
//	{
//		int kills;
//		result.UserStatistics.TryGetValue(CSVR_GameSetting.accountKillKey,out kills);
//		CSVR_GameSetting.accountKill = kills;
//		
//		int wins;
//		result.UserStatistics.TryGetValue(CSVR_GameSetting.accountWinKey, out wins);
//		CSVR_GameSetting.accountWin = wins;
//		
//		int levels;
//		result.UserStatistics.TryGetValue(CSVR_GameSetting.accountLevelKey, out levels);
//		CSVR_GameSetting.accountLevel = levels;
//		
//		int deads;
//		result.UserStatistics.TryGetValue(CSVR_GameSetting.accountDeadKey, out deads);
//		CSVR_GameSetting.accountDead = deads;
//		
//		int losts;
//		result.UserStatistics.TryGetValue(CSVR_GameSetting.accountLostKey, out losts);
//		CSVR_GameSetting.accountLost = losts;
//		
//		int headshots;
//		result.UserStatistics.TryGetValue(CSVR_GameSetting.accountHeadShotKey, out headshots);
//		CSVR_GameSetting.accountHeadShot = headshots;
//		
//		AccountManager.instance.UpdateUserStatistics(kills, wins, levels,deads, losts, headshots);
//	}
//	
//	/// <summary>
//	/// Getting user statistics failed.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	void OnGetUserStatisticsError(PlayFabError error)
//	{
//		// Debug.LogError("Error getting stats: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	/// <summary>
//	/// Method used to ask PlayFab server for ingame news.
//	/// </summary>
//	/// <param name="amount">Amount of news to bring.</param>
//	public void GetGameNews(int amount)
//	{
//		GetTitleNewsRequest request = new GetTitleNewsRequest();
//		request.Count = amount;
//		
//		PlayFabClientAPI.GetTitleNews(request, OnGameNewsLoaded, OnGameNewsLoadFailed);
//	}
//	
//	/// <summary>
//	/// Callback received when ingame news are loaded.
//	/// </summary>
//	/// <param name="result">Result from PlayFab.</param>
//	void OnGameNewsLoaded(GetTitleNewsResult result)
//	{
//		gameNews = result.News;
//		
//	}
//	
//	/// <summary>
//	/// Callback received when ingame news loading returns an error.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	void OnGameNewsLoadFailed(PlayFabError error)
//	{
//		//Debug.LogError("Error getting game news: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	/// <summary>
//	/// Gets logged player's PlayFab ID.
//	/// </summary>
//	/// <returns>PlayFab ID.</returns>
//	public string GetMyPlayerID()
//	{
//		return playerID;
//	}
//	
//	// this should be done with cloud script
//	/// <summary>
//	/// Sends a push notification request to PlayFab.
//	/// </summary>
//	/// <param name="playFabID">Recipient's PlayFab ID.</param>
//	/// <param name="message">Message to send.</param>
//	//    public void SendPush(string playFabID, string message)
//	//    {
//	//        PlayFab.Server.SendPushNotificationRequest request = new PlayFab.Server.SendPushNotificationRequest();
//	//        request.Recipient = playFabID;
//	//        request.Message = message;
//	////        PlayFabServerAPI.SendPushNotification(request, OnSendPushNotificationSuccess, OnSendPushNotificationError);
//	//
//	//    }
//	
//	/// <summary>
//	/// Event called when push notification is successfully sent.
//	/// </summary>
//	/// <param name="result">Result data.</param>
//	//    private void OnSendPushNotificationSuccess(PlayFab.Server.SendPushNotificationResult result)
//	//    {
//	//        Debug.Log("Successfully sent push notification");
//	//    }
//	
//	/// <summary>
//	/// Event called when push notification has failed to send.
//	/// </summary>
//	/// <param name="error">Error details.</param>
//	//    private void OnSendPushNotificationError(PlayFabError error)
//	//    {
//	//        Debug.LogError("Error sending push notification: " + error.Error + " " + error.ErrorMessage);
//	//    }
//	
//	/// <summary>
//	/// Gets All title data from PlayFab.
//	/// </summary>
//	void LoadTitleData()
//	{
//		GetTitleDataRequest request = new GetTitleDataRequest();
//		request.Keys = new List<string>() { "Key_ChangeCoin","Key_UpgradeLevel1", "Key_UpgradeLevel2", "Key_UpgradeLevel3", "Key_UpgradeLevel4","Key_UpgradeLevel5",
//			"Key_UpgradeLevel6","Key_GameUpdate","Key_APKServer","Key_APKName","ModeMapAvaialbe","Key_UseServer","Key_CodeTest"};
//		
//		PlayFabClientAPI.GetTitleData(request, OnTitleDataLoadSuccess, OnTitleDataLoadError);
//	}
//	public void LoadTitleDataEndGame()
//	{
//		GetTitleDataRequest request = new GetTitleDataRequest();
//		request.Keys = new List<string>() { "Key_ChangeCoin","Key_UpgradeLevel1", "Key_UpgradeLevel2", "Key_UpgradeLevel3", "Key_UpgradeLevel4","Key_UpgradeLevel5",
//			"Key_UpgradeLevel6","Key_GameUpdate","Key_APKServer","Key_APKName","ModeMapAvaialbe","Key_UseServer","Key_CodeTest"};
//		
//		PlayFabClientAPI.GetTitleData(request, OnTitleDataLoadSuccessEndGame, OnTitleDataLoadError);
//	}
//	private void OnTitleDataLoadSuccessEndGame(GetTitleDataResult result)
//	{
//		titleData = result.Data;
//		#if !VIRTUAL_ROOM && !UNITY_IPHONE
//		if(GetTitleValue ("Key_GameUpdate") != Application.version){
//			Application.LoadLevel("Update");
//		}
//		#endif
//		
//	}
//	AsyncOperation async;
//	
//	private void OnTitleDataLoadSuccess(GetTitleDataResult result)
//	{
//		
//		titleData = result.Data;
//		
//		System.GC.Collect();
//		Resources.UnloadUnusedAssets();
//		#if !VIRTUAL_ROOM && !UNITY_IPHONE
//		if(GetTitleValue ("Key_GameUpdate") != Application.version){
//			async = Application.LoadLevelAsync("Update");
//			async.allowSceneActivation = false;
//		}else{
//			
//			async = Application.LoadLevelAsync("Home");
//			async.allowSceneActivation = false;
//		}
//		#else
//		async = Application.LoadLevelAsync("Home");
//		async.allowSceneActivation = false;
//		#endif
//		
//	}
//	
//	/// <summary>
//	/// Event called when title data load fails.
//	/// </summary>
//	/// <param name="error">Error data.</param>
//	private void OnTitleDataLoadError(PlayFabError error)
//	{
//		// Debug.LogError("Error getting title data: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	
//	/// <summary>
//	/// Gets a specific title value, based on sent key. Must be called after LoadTitleData() completes.
//	/// </summary>
//	/// <param name="key">Key to find value.</param>
//	/// <returns>Value, if found.</returns>
//	public string GetTitleValue(string key)
//	{
//		string value = "";
//		if (titleData != null)
//		{
//			titleData.TryGetValue(key, out value);
//		}
//		return value;
//	}
//	
//	#region this.LoadShopItem();
//	public void GetAllCatalogItems()
//	{
//		GetCatalogItemsRequest request = new GetCatalogItemsRequest();
//		request.CatalogVersion = "Item";
//		PlayFabClientAPI.GetCatalogItems(request, OnCatalogItemsLoaded, OnCatalogItemsError);
//		
//	}
//	
//	private void OnCatalogItemsLoaded(GetCatalogItemsResult result)
//	{
//		
////		CSVR_GameSetting.shopItems = result.Catalog;
////		CSVR_GameSetting.shopItems.Sort ();
////		//		Debug.Log ("OnGetShopItem " + CSVR_GameSetting.shopItems.Count);
////		CSVR_GameSetting.shopMeleGun = new List<CatalogItem>();
////		CSVR_GameSetting.shopPistolGun = new List<CatalogItem>();
////		CSVR_GameSetting.shopGrenadeGun = new List<CatalogItem>();
////		CSVR_GameSetting.shopArmorGun = new List<CatalogItem>();
////		CSVR_GameSetting.shopMainGun = new List<CatalogItem>();
////		//		CSVR_GameSetting.shopChest = new List<CatalogItem>();
////		CSVR_GameSetting.shopBag = new List<CatalogItem>();
////		
////		//		CSVR_GameSetting.itemCatalogVers = new Dictionary<string, string>() ;
////		//		CSVR_GameSetting.itemPrices = new Dictionary<string, int>();
////		
////		
////		for (int x = 0; x < CSVR_GameSetting.shopItems.Count; x++) {
////			if(CSVR_GameSetting.shopItems[x].ItemClass == "Melee.Weapon")
////				CSVR_GameSetting.shopMeleGun.Add(CSVR_GameSetting.shopItems[x]);
////			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Pistol.Weapon")
////				CSVR_GameSetting.shopPistolGun.Add(CSVR_GameSetting.shopItems[x]);
////			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Grenade.Weapon")
////				CSVR_GameSetting.shopGrenadeGun.Add(CSVR_GameSetting.shopItems[x]);
////			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Armor.Weapon")
////				CSVR_GameSetting.shopArmorGun.Add(CSVR_GameSetting.shopItems[x]);
////			//			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Chest.GiftBox")
////			//				CSVR_GameSetting.shopChest.Add(CSVR_GameSetting.shopItems[x]);
////			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Bag.Item")
////				CSVR_GameSetting.shopBag.Add(CSVR_GameSetting.shopItems[x]);
////			else 
////				CSVR_GameSetting.shopMainGun.Add(CSVR_GameSetting.shopItems[x]);
////			
////			//			CSVR_GameSetting.itemCatalogVers.Add(CSVR_GameSetting.shopItems[x].ItemId, CSVR_GameSetting.shopItems[x].CatalogVersion);
////			//			CSVR_GameSetting.itemPrices.Add( CSVR_GameSetting.shopItems[x].ItemId, (int)CSVR_GameSetting.shopItems[x].VirtualCurrencyPrices["GC"]);
////		};
////		this.LoadShopItem07Day ();
//		
//	}
//	//
//	//	private void OnGetShopItem(GetCatalogItemsResult result){
//	//		
//	//		CSVR_GameSetting.shopItems = result.Catalog;
//	////		Debug.Log ("OnGetShopItem " + CSVR_GameSetting.shopItems.Count);
//	//		CSVR_GameSetting.shopMeleGun = new List<CatalogItem>();
//	//		CSVR_GameSetting.shopPistolGun = new List<CatalogItem>();
//	//		CSVR_GameSetting.shopGrenadeGun = new List<CatalogItem>();
//	//		CSVR_GameSetting.shopArmorGun = new List<CatalogItem>();
//	//		CSVR_GameSetting.shopMainGun = new List<CatalogItem>();
//	//		CSVR_GameSetting.shopChest = new List<CatalogItem>();
//	//		CSVR_GameSetting.shopBag = new List<CatalogItem>();
//	//		
//	//		CSVR_GameSetting.itemCatalogVers = new Dictionary<string, string>() ;
//	//		CSVR_GameSetting.itemPrices = new Dictionary<string, int>();
//	//		
//	//		
//	//		for (int x = 0; x < CSVR_GameSetting.shopItems.Count; x++) {
//	//			if(CSVR_GameSetting.shopItems[x].ItemClass == "Melee.Weapon")
//	//				CSVR_GameSetting.shopMeleGun.Add(CSVR_GameSetting.shopItems[x]);
//	//			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Pistol.Weapon")
//	//				CSVR_GameSetting.shopPistolGun.Add(CSVR_GameSetting.shopItems[x]);
//	//			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Grenade.Weapon")
//	//				CSVR_GameSetting.shopGrenadeGun.Add(CSVR_GameSetting.shopItems[x]);
//	//			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Armor.Weapon")
//	//				CSVR_GameSetting.shopArmorGun.Add(CSVR_GameSetting.shopItems[x]);
//	//			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Chest.GiftBox")
//	//				CSVR_GameSetting.shopChest.Add(CSVR_GameSetting.shopItems[x]);
//	//			else if(CSVR_GameSetting.shopItems[x].ItemClass == "Bag.Item")
//	//				CSVR_GameSetting.shopBag.Add(CSVR_GameSetting.shopItems[x]);
//	//			else 
//	//				CSVR_GameSetting.shopMainGun.Add(CSVR_GameSetting.shopItems[x]);
//	//			
//	//			CSVR_GameSetting.itemCatalogVers.Add(CSVR_GameSetting.shopItems[x].ItemId, CSVR_GameSetting.shopItems[x].CatalogVersion);
//	//			CSVR_GameSetting.itemPrices.Add( CSVR_GameSetting.shopItems[x].ItemId, (int)CSVR_GameSetting.shopItems[x].VirtualCurrencyPrices["GC"]);
//	//		};
//	//		this.LoadShopItem07Day ();
//	//		
//	//		
//	//	}
//	private void LoadShopItem07Day(){
//		
//		GetCatalogItemsRequest request = new GetCatalogItemsRequest();
//		request.CatalogVersion = "Item07Day";
//		PlayFabClientAPI.GetCatalogItems (request,OnGetShopItem07Day,OnCatalogItemsError);
//		
//	}
//	private void OnGetShopItem07Day(GetCatalogItemsResult result){
////		CSVR_GameSetting.shopItems07Day = result.Catalog;
////		CSVR_GameSetting.shopItems07Day.Sort ();
//		//		Debug.Log ("OnGetShopItem07Day " + CSVR_GameSetting.shopItems07Day.Count);
//		this.LoadShopItem30Day ();
//	}
//	private void LoadShopItem30Day(){
//		
//		GetCatalogItemsRequest request = new GetCatalogItemsRequest();
//		request.CatalogVersion = "Item30Day";
//		PlayFabClientAPI.GetCatalogItems (request,OnGetShopItem30Day,OnCatalogItemsError);
//		
//	}
//	private void OnGetShopItem30Day(GetCatalogItemsResult result){
////		CSVR_GameSetting.shopItems30Day = result.Catalog;
////		CSVR_GameSetting.shopItems30Day.Sort ();
//		//Debug.Log ("OnGetShopItem30Day " + CSVR_GameSetting.shopItems30Day.Count);
//		this.LoadUpgradeItem();
//	}
//	private void LoadUpgradeItem(){
//		//		applicationState = CSVR_ApplicationState.SHOP_LOADING_ITEM;
//		GetCatalogItemsRequest request = new GetCatalogItemsRequest();
//		request.CatalogVersion = "UpgradeV2";
//		PlayFabClientAPI.GetCatalogItems (request,OnGetUpgradeItemSuccess,OnCatalogItemsError);
//		
//	}
//	private void OnGetUpgradeItemSuccess(GetCatalogItemsResult result){
//		
////		CSVR_GameSetting.upgradeItems = result.Catalog;
//		
//		//		CSVR_GameSetting.upgradePricesB = new Dictionary<string, int>();
//		//		CSVR_GameSetting.upgradePricesV = new Dictionary<string, int>();
//		//		for (int x = 0; x < CSVR_GameSetting.upgradeItems.Count; x++) {
//		//			if(CSVR_GameSetting.upgradeItems[x].VirtualCurrencyPrices.ContainsKey("GC"))
//		//			{
//		//				CSVR_GameSetting.upgradePricesB.Add( CSVR_GameSetting.upgradeItems[x].ItemId, (int)CSVR_GameSetting.upgradeItems[x].VirtualCurrencyPrices["GC"]);
//		//			}if(CSVR_GameSetting.upgradeItems[x].VirtualCurrencyPrices.ContainsKey("GD"))
//		//				CSVR_GameSetting.upgradePricesV.Add( CSVR_GameSetting.upgradeItems[x].ItemId, (int)CSVR_GameSetting.upgradeItems[x].VirtualCurrencyPrices["GD"]);
//		//		};
//		
//		this.LoadFriendList();
//	}
//	private void OnCatalogItemsError(PlayFabError error)
//	{
//		//Debug.LogError("Error loading store items: " + error.Error + " " + error.ErrorMessage);
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	#endregion
//	
//	#region this.LoadFriendList();
//	private void LoadFriendList()
//	{
//		GetFriendsListRequest request = new GetFriendsListRequest();
//		request.IncludeFacebookFriends = true;
//		PlayFabClientAPI.GetFriendsList (request, OnLoadFriendListSuccess,OnLoadFriendListFail);
//	}
//	private void OnLoadFriendListSuccess(GetFriendsListResult result) {
//		CSVR_GameSetting.friends = result.Friends;
//		for(int i=0;i<CSVR_GameSetting.friends.Count;i++){
//			if(CSVR_GameSetting.friends[i].FriendPlayFabId != ""){
//				OnFriendInfo(CSVR_GameSetting.friends[i].FriendPlayFabId);
//				CSVR_GameSetting.friendsListName.Add(CSVR_GameSetting.friends[i].FriendPlayFabId);
//			}
//		}
//		ConnectPhoton ();
//		
//		//		Debug.Log("aaa");
//		//OnLoadInfoPlayerComplete ();
//	}
//	public void OnLoadInfoPlayerComplete(){
//		async.allowSceneActivation = true;
//		//		if(GetTitleValue ("Key_GameUpdate") != Application.version){
//		//			Application.LoadLevel ("Update");
//		//		}else{
//		//			Application.LoadLevel ("Home");
//		//		}
//	}
//	
//	private void OnLoadFriendListFail(PlayFabError error)
//	{
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	public void RemoveFriend(string displayName)
//	{
//		//		Debug.Log("lala");
//		RemoveFriendRequest request = new RemoveFriendRequest();
//		request.FriendPlayFabId = displayName;
//		PlayFabClientAPI.RemoveFriend(request, OnRemoveFriendSuccess,OnAddFriendFail);
//	}
//	private void OnRemoveFriendSuccess(RemoveFriendResult result) {
//		CSVR_UIFriend.instance.UI_RemoveFriendResult("Hủy kết bạn thành công!");
//	}
//	string friendName;
//	public void AddFriend(string displayName)
//	{
//		
//		//		Debug.Log("lala");
//		friendName = displayName;
//		AddFriendRequest request = new AddFriendRequest();
//		request.FriendTitleDisplayName = displayName;
//		PlayFabClientAPI.AddFriend (request, OnAddFriendSuccess,OnAddFriendFail);
//	}
//	private void OnAddFriendSuccess(AddFriendResult result) {
//		LoadFriendListUpdate();
//	}
//	private void LoadFriendListUpdate()
//	{
//		GetFriendsListRequest request = new GetFriendsListRequest();
//		request.IncludeFacebookFriends = true;
//		PlayFabClientAPI.GetFriendsList (request, OnLoadFriendListUpdateSuccess,OnLoadFriendListFail);
//	}
//	private void OnLoadFriendListUpdateSuccess(GetFriendsListResult result) {
//		CSVR_GameSetting.friends = result.Friends;
//		
//		ErrorView.instance.ShowPopupError("Kết bạn thành công!");
//		if(CSVR_UIFriend.instance != null){
//			for(int i=0;i<CSVR_GameSetting.friends.Count;i++){
//				if (System.String.Compare(CSVR_GameSetting.friends[i].TitleDisplayName, friendName, true) == 0)
//				{
//					CSVR_UIFriend.instance.AddFriend(CSVR_GameSetting.friends[i].FriendPlayFabId);
//				}
//			}
//		}
//		
//		
//	}
//	private void OnAddFriendFail(PlayFabError error)
//	{
//		switch(error.Error ){
//		case PlayFabErrorCode.InvalidParams:
//			ErrorView.instance.ShowPopupError("Tên nhân vật lỗi");
//			break;
//		case PlayFabErrorCode.AccountNotFound:
//			ErrorView.instance.ShowPopupError("Tên nhân vật không tồn tại");
//			break;
//		case PlayFabErrorCode.UsersAlreadyFriends:
//			ErrorView.instance.ShowPopupError("Đã có trong danh sách bạn bè");
//			break;
//		}
//	}
//	
//	private void FindPlayerFail(PlayFabError error)
//	{
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	public void OnFriendInfo(string id) {
//		//		RunCloudScriptRequest request = new RunCloudScriptRequest();
//		//		request.ActionId = "onGetInfoFriendV2";
//		//		//	Debug.Log("playfabId:" + id);
//		//		request.ParamsEncoded = "{\"playfabId\":\""+id+"\"}";
//		//		PlayFabClientAPI.RunCloudScript(request,OnFriendInfoSuccess, OnFriendInfoFaile);
//		
//		var parameter = new {playfabId = id};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onGetInfoFriendV2";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnFriendInfoSuccess, OnFriendInfoFaile);
//	}
//	private void OnFriendInfoFaile(PlayFabError error){
//		errorView.ShowPopupError ("Error: "+error.Error);
//	}
//	Sprite avatarFacebook;
//	private void OnFriendInfoSuccess(ExecuteCloudScriptResult result){
//		//	Debug.Log ("OnFriendInfoSuccess");
//		//GetUserDataResultV2 userResul = PlayFab.Json.JsonConvert.DeserializeObject<GetUserDataResultV2>(result.Results.ToString());
//		GetUserStatisticsResultV2 userResul = PlayFab.Json.JsonConvert.DeserializeObject<GetUserStatisticsResultV2>(result.FunctionResult.ToString());
//		CSVR_FriendInfo info = new CSVR_FriendInfo();
//		
//		//		for(int i=0;i<CSVR_GameSetting.friends.Count;i++){
//		//
//		//			if( !string.IsNullOrEmpty(CSVR_GameSetting.friends[i].FacebookInfo.FacebookId) && userResul.PlayFabId == CSVR_GameSetting.friends[i].FriendPlayFabId){
//		//				//Debug.Log("CSVR_GameSetting.friends[i].FriendPlayFabId"+CSVR_GameSetting.friends[i].FacebookInfo.FacebookId);
//		//				StartCoroutine(DownloadAvatarFacebook(CSVR_GameSetting.friends[i].FacebookInfo.FacebookId));
//		//				info.avatar = avatarFacebook;
//		//				break;
//		//			}
//		//		}
//		if (userResul.UserStatistics.ContainsKey (CSVR_GameSetting.accountLevelKey)) {
//			info.level = userResul.UserStatistics["AccountLevel"].ToString();
//		}
//		
//		if(CSVR_GameSetting.friendInfos.ContainsKey(userResul.PlayFabId))
//			CSVR_GameSetting.friendInfos[userResul.PlayFabId] = info;
//		else
//			CSVR_GameSetting.friendInfos.Add(userResul.PlayFabId,info);
//		
//	}
//	IEnumerator DownloadAvatarFacebook(string id){
//		//WWW url = new WWW("http://graph.facebook.com/" + id+ "/picture?width=100&height=100");
//		WWW url = new WWW("http://graph.facebook.com/" + id+ "/picture");
//		Texture2D textFb2 = new Texture2D(8, 8, TextureFormat.ARGB32, false); //TextureFormat must be DXT5
//		yield return url;
//		url.LoadImageIntoTexture(textFb2);
//		avatarFacebook = Sprite.Create(textFb2, new Rect(0, 0, 8, 8), new Vector2());
//	}
//	#endregion
//	
//	#region Change Coin
//	public void OnChangeCoin(string GD)
//	{
//		//		RunCloudScriptRequest request = new RunCloudScriptRequest ();
//		//		request.ActionId = "onChangeGameCoin";
//		//		request.ParamsEncoded = "{\"GD\":\"" + GD + "\"}";
//		//		PlayFabClientAPI.RunCloudScript (request, OnChangeCoinSuccess, OnPlayFabError_Equip,GD);
//		
//		var parameter = new {GD = GD};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onChangeGameCoin";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnChangeCoinSuccess, OnPlayFabError_Equip,GD);
//		
//	}
//	private void OnChangeCoinSuccess(ExecuteCloudScriptResult result){
//		
//		//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.shop_Add);
//		int GD = int.Parse(result.CustomData.ToString());
//		AddUserVirtualCurrencyResult addResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//		AccountManager.instance.gameCoinAmount = (int)addResul.Balance;
//		AccountManager.instance.gameDollarAmount -= GD;
//	}
//	
//	#endregion
//	
//	#region Equip And UnEquip
//	public void OnEquipItem(string itemID,string itemClass) 
//	{	string _group = "EquippedGroup" + CSVR_UIInventory.instance.Group;
//		var parameter = new {IIId = itemID, Class = itemClass , Group = _group};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onEquipGroup";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		//	request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnEquipSuccess, OnPlayFabError_Equip);
//	}
//	private void OnEquipSuccess(ExecuteCloudScriptResult result){
//		try{
////			AccountManager.instance.inventory = PlayFab.Json.JsonConvert.DeserializeObject<GetUserInventoryResult>(result.FunctionResult.ToString()).Inventory;
//		}catch{
//			errorView.ShowPopupError ("Trang bị lỗi");
//		}
//	}
//	private void OnPlayFabError_Equip(PlayFabError error){errorView.ShowPopupError ("Trang bị thất bại");}
//	
//	
//	public void OnUnEquipItem(string itemID,int group) 
//	{
//		string _group = "EquippedGroup" + group;
//		var parameter = new {IIId = itemID , Group = _group};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onUnEquipGroup";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		//request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnUnEquipSuccess, OnPlayFabError_UnEquip);
//	}
//	private void OnUnEquipSuccess(ExecuteCloudScriptResult result){
////		AccountManager.instance.inventory = PlayFab.Json.JsonConvert.DeserializeObject<GetUserInventoryResult>(result.FunctionResult.ToString()).Inventory;
//	}
//	private void OnPlayFabError_UnEquip(PlayFabError error){errorView.ShowPopupError ("Trang bị thất bại");}
//	
//	#endregion
//	
//	#region Purchase Item
//	
//	public void OnFirstPurchaseGunItemV3(CatalogItem item) {
//		//music
//		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.buySSEff_Clip);;
//		PurchaseItemRequest request = new PurchaseItemRequest();
//		request.CatalogVersion = item.CatalogVersion;
//		if (item.VirtualCurrencyPrices.ContainsKey ("GC")) {
//			request.VirtualCurrency = "GC";
//			request.Price = (int)item.VirtualCurrencyPrices ["GC"];
//		} else {
//			request.VirtualCurrency = "GD";
//			request.Price = (int)item.VirtualCurrencyPrices ["GD"];
//		}
//		request.ItemId = item.ItemId;
//		PlayFabClientAPI.PurchaseItem(request,OnFirstPurchaseGunSuccessV3, OnCatalogItemsError);
//	}
//	private void OnFirstPurchaseGunSuccessV3(PurchaseItemResult result){
//		ItemInstance newItem = result.Items [0];
//		
//		LoadInventoryItem (newItem);
//	}
//	public void LoadInventoryItem(ItemInstance newItem){
//		PlayFabClientAPI.GetUserInventory (new GetUserInventoryRequest(),OnGetUserInventory, OnCatalogItemsError,newItem);
//	}
//	
//	private void OnGetUserInventory(GetUserInventoryResult result){
//		
//		ItemInstance newItem = (ItemInstance)result.CustomData;
//		//CSVR_GameSetting.inventoryItemsNew.Add (newItem);
////		AccountManager.instance.itemsNew.Add (newItem);
//		//AccountManager.instance.inventory = result.Inventory;
//		AccountManager.instance.gameCoinAmount = (int)result.VirtualCurrency["GC"];
//		AccountManager.instance.gameDollarAmount = (int)result.VirtualCurrency["GD"];
//		
//		HomeScreen.instance.UpdateVirtualCurrency();
//		//Debug.Log ("OnGetUserInventory");
//		//CSVR_UIInventory.instance.UpdateItemListV2 ();
//	}
//	
//	#endregion
//	
//	#region Upgrade Item
//	ProjectDelegates.SimpleCallback OnUpgradeGunItemV5Completed;
//	string tempLevel = "up1";
//	public void OnUpgradeGunItemV5(ItemInstance item,CatalogItem info,ProjectDelegates.SimpleCallback OnUpgradeGunItemV5CompletedCallBack) {
//		OnUpgradeGunItemV5Completed = OnUpgradeGunItemV5CompletedCallBack;
//		//		CSVR_UIInventory.instance.UpgradeWatting ();
//		//		Debug.Log ("OnUpgradeGunItemV5");
//		PurchaseItemRequest request = new PurchaseItemRequest();
//		request.CatalogVersion = "UpgradeV2";
//		if (info.VirtualCurrencyPrices.ContainsKey ("GC")) {
//			request.VirtualCurrency = "GC";
//			request.Price = int.Parse (info.VirtualCurrencyPrices ["GC"].ToString ());
//		} else {
//			request.VirtualCurrency = "GD";
//			request.Price = int.Parse (info.VirtualCurrencyPrices ["GD"].ToString ());
//		}
//		request.ItemId = info.ItemId;
//		PlayFabClientAPI.PurchaseItem (request, OnUpgradeGunItemSuccess, OnUpgradeGunItemError, item);
//	}
//	
//	public  void OnUpgradeGunItemSuccess(PurchaseItemResult result){
//		//		Debug.Log (result.Items);
//		ItemInstance item = result.Items[0];
//		if (item.UnitCurrency == "GC") {
//			AccountManager.instance.gameCoinAmount -= (int)item.UnitPrice;
//		} else {
//			AccountManager.instance.gameDollarAmount -= (int)item.UnitPrice;
//		}
//		CSVR_GunInfo gunInfo = CSVR_GameSetting.FindGunInfoInUpgradeItems(item.ItemId);
//		OnUpgradeWeaponInfoInventory(result.CustomData,gunInfo);
//	}
//	
//	public void OnUpgradeWeaponInfoInventory(object itemInstance,CSVR_GunInfo gunSetup )
//	{	
//		ItemInstance item = (ItemInstance)itemInstance;
//		string _level = gunSetup.Level.ToString();
//		string _sellPrices = gunSetup.SellPrices.ToString();
//		tempLevel = "up"+gunSetup.Level;
//		var parameter = new {IIId = item.ItemInstanceId  , Level = _level ,Sell = _sellPrices};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onUpgradeItem";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnUpgradeWeaponInfoInventorySuccess, OnUpgradeWeaponInfoInventoryError,item);
//	}
//	void OnUpgradeWeaponInfoInventorySuccess(ExecuteCloudScriptResult result){
//		//Debug.Log (result.Logs [0].Message);
//		bool upgradeSuccess = bool.Parse (result.Logs [0].Message);
//		try {
//			if (!upgradeSuccess) {
//				//music
//				AudioManager.instance.audio.PlayOneShot (AudioManager.instance.UpgrageFL_Clip);
//				CSVR_UIInventory.instance.UpgradeFaile ();
//				HomeScreen.instance.UpdateVirtualCurrency ();
//			} else {
//				//music
//				AudioManager.instance.audio.PlayOneShot (AudioManager.instance.UpgrageSS_Clip);
//				ItemInstance item = (ItemInstance)result.CustomData;
//				//			Debug.Log(result.FunctionResult.ToString());
//				GetUserInventoryUpgrade inventoryResult = PlayFab.Json.JsonConvert.DeserializeObject<GetUserInventoryUpgrade> (result.FunctionResult.ToString ());
//				//AccountManager.instance.inventory = inventoryResult.Inventory;
//				AccountManager.instance.gameCoinAmount = (int)inventoryResult.VirtualCurrency ["GC"];
//				AccountManager.instance.gameDollarAmount = (int)inventoryResult.VirtualCurrency ["GD"];
//				
//				HomeScreen.instance.UpdateVirtualCurrency ();
////				CSVR_UIInventory.instance.UpdateItemListUp (item);
////				CSVR_UIInventory.instance.UpgradeSuccess (tempLevel);
//			}
//		} catch {
//		}
//		OnUpgradeGunItemV5Completed ();
//	}
//	void OnUpgradeGunItemError(PlayFabError error){
//		OnUpgradeGunItemV5Completed ();
//	}
//	void OnUpgradeWeaponInfoInventoryError(PlayFabError error){
//		OnUpgradeGunItemV5Completed ();
//	}
//	#endregion
//	
//	#region Sell Item
//	public void OnSellWeaPon(ItemInstance item,string prices)
//	{
//		
//		//		RunCloudScriptRequest request = new RunCloudScriptRequest ();
//		//		request.ActionId = "onSellItem";
//		//		request.ParamsEncoded = "{\"IIId\":\"" + item.ItemInstanceId + "\",\"SellPrices\":\"" + prices + "\"}";
//		//		PlayFabClientAPI.RunCloudScript (request, OnSellWeaPonSuccess, OnPlayFabError_Equip, item);
//		
//		
//		var parameter = new {IIId = item.ItemInstanceId  , SellPrices = prices};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onSellItem";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnSellWeaPonSuccess, OnPlayFabError_Equip,item);
//		
//	}
//	void OnSellWeaPonSuccess(ExecuteCloudScriptResult result){
//		ItemInstance item = (ItemInstance)result.CustomData;
//		
//		//AccountManager.instance.inventory.Remove (item);
//		
//		CSVR_UIInventory.instance.SellSuccess(item.ItemInstanceId);
//		AddUserVirtualCurrencyResult sellResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//		AccountManager.instance.gameCoinAmount = sellResul.Balance;
//		
//		HomeScreen.instance.UpdateVirtualCurrency();
//		CSVR_UIPopupConfirmShop.instance.SellSuccess ();
//		//them 25.03.2016
//		
//	}
//	
//	#endregion
//	
//	#region GameControll
//	// ReportLots -> ReportLostsStats
//	public void ReportLots(int amount)
//	{
//		if (amount > 1 )
//			return;
//		
//		CSVR_GameSetting.accountLost += amount;
//		ReportLostsStats(CSVR_GameSetting.accountLost);
//		
//	}
//	public void ReportLostsStats(int amount)
//	{
//		UpdateUserStatisticsRequest request = new UpdateUserStatisticsRequest();
//		request.UserStatistics = new Dictionary<string, int>();
//		request.UserStatistics.Add(CSVR_GameSetting.accountLostKey, amount);
//		
//		PlayFabClientAPI.UpdateUserStatistics(request, OnUpdateStatsCompleted, OnUpdateStatsError);
//		
//	}
//	
//	// ReportWins -> ReportWinsStats
//	public void ReportWins(int amount)
//	{
//		if (amount > 1)
//			return;
//		
//		CSVR_GameSetting.accountWin += amount;
//		ReportWinsStats(CSVR_GameSetting.accountWin);
//		
//	}
//	private void ReportWinsStats(int amount)
//	{
//		UpdateUserStatisticsRequest request = new UpdateUserStatisticsRequest();
//		request.UserStatistics = new Dictionary<string, int>();
//		request.UserStatistics.Add(CSVR_GameSetting.accountWinKey, amount);
//		
//		PlayFabClientAPI.UpdateUserStatistics(request, OnUpdateStatsCompleted, OnUpdateStatsError);
//		
//	}
//	
//	// ReportKills -> ReportKillStats
//	public void ReportKills(int amount)
//	{
//		if (amount == 0)
//			return;
//		
//		CSVR_GameSetting.accountKill += amount;
//		ReportKillStats(CSVR_GameSetting.accountKill);
//	}
//	private void ReportKillStats(int amount)
//	{
//		UpdateUserStatisticsRequest request = new UpdateUserStatisticsRequest();
//		request.UserStatistics = new Dictionary<string, int>();
//		request.UserStatistics.Add(CSVR_GameSetting.accountKillKey, amount);
//		
//		PlayFabClientAPI.UpdateUserStatistics(request, OnUpdateStatsCompleted, OnUpdateStatsError);
//		
//	}
//	
//	// ReportDeads -> ReportDeadsStats
//	public void ReportDeads(int amount)
//	{
//		if (amount == 0)
//			return;
//		
//		CSVR_GameSetting.accountDead += amount;
//		ReportDeadsStats(CSVR_GameSetting.accountDead);	
//	}
//	private void ReportDeadsStats(int amount)
//	{
//		UpdateUserStatisticsRequest request = new UpdateUserStatisticsRequest();
//		request.UserStatistics = new Dictionary<string, int>();
//		request.UserStatistics.Add(CSVR_GameSetting.accountDeadKey, amount);
//		
//		PlayFabClientAPI.UpdateUserStatistics(request, OnUpdateStatsCompleted, OnUpdateStatsError);
//	}
//	
//	// ReportHeadShots -> ReportHeadShotsStats
//	public void ReportHeadShots(int amount)
//	{
//		if (amount == 0)
//			return;
//		
//		CSVR_GameSetting.accountHeadShot += amount;
//		ReportHeadShotsStats(CSVR_GameSetting.accountHeadShot);
//	}
//	private void ReportHeadShotsStats(int amount)
//	{
//		UpdateUserStatisticsRequest request = new UpdateUserStatisticsRequest();
//		request.UserStatistics = new Dictionary<string, int>();
//		request.UserStatistics.Add(CSVR_GameSetting.accountHeadShotKey, amount);
//		
//		PlayFabClientAPI.UpdateUserStatistics(request, OnUpdateStatsCompleted, OnUpdateStatsError);
//	}
//	
//	// OnUpdateStatsCompleted - OnUpdateStatsError
//	void OnUpdateStatsCompleted(UpdateUserStatisticsResult result)
//	{
//		//	Debug.Log("Successfully updated stats");
//	}
//	void OnUpdateStatsError(PlayFabError error)
//	{
//		//	Debug.LogError("Error updating stat: " + error.Error + " " + error.ErrorMessage);
//	}
//	
//	
//	public void OnRewardKill(int count){
//		if (count == 0) {
//			AccountManager.instance.gameCoinReward = 0;
//			return;
//		}
//		
//		var parameter = new {level = CSVR_GameSetting.accountLevel  , count = count};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onMonsterKilledCSVR";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnRewardKillSuccess, OnPlayFabError_Equip);
//	}
//	void OnRewardKillSuccess(ExecuteCloudScriptResult result)
//	{
//		try{
//			AddUserVirtualCurrencyResult sellResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//			AccountManager.instance.gameCoinReward = sellResul.BalanceChange;
//			AccountManager.instance.gameCoinAmount = sellResul.Balance;
//		}catch{
//			AccountManager.instance.gameCoinReward = 0;
//		}
//	}
//	
//	void OnRewardKillError(PlayFabError error)
//	{
//		//	Debug.Log ("error" + error);
//	}
//	
//	public int HeadShotRewardInDayCount = 10;
//	public int HeadShotRewardInGame = 0;
//	
//	public void OnRewardKillHeadShotInDay(int count){
//		
//		var parameter = new {headshot = count};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "CheckInHeadShot";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnRewardKillHeadShotInDaySuccess, OnRewardKillError);
//	}
//	void OnRewardKillHeadShotInDaySuccess(ExecuteCloudScriptResult result)
//	{
//		try{
//			AddUserVirtualCurrencyResult headshotResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//			if(headshotResul != null){
//				AccountManager.instance.gameDollarReward = headshotResul.BalanceChange;
//				AccountManager.instance.gameDollarAmount = headshotResul.Balance;
//				HeadShotRewardInDayCount = int.Parse(result.Logs[0].Message);
//				HeadShotRewardInGame = 0;
//			}else{
//				HeadShotRewardInDayCount = 0;
//				HeadShotRewardInGame = 0;
//			}
//			//Debug.Log ("OnRewardKillHeadShotInDaySuccess: "+HeadShotRewardInDayCount);
//		}catch{
//			HeadShotRewardInDayCount = 0;
//			AccountManager.instance.gameDollarReward = 0;
//		}
//	}
//	
//	public int AceRewardInDayCount = 10;
//	
//	public void OnRewardAceInDay(){
//		
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "CheckInAce";
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnRewardAceInDaySuccess, OnRewardKillError);
//	}
//	void OnRewardAceInDaySuccess(ExecuteCloudScriptResult result)
//	{
//		try{
//			//	Debug.Log ("OnRewardKillHeadShotInDaySuccess: "+result.Logs[0].Message);
//			AceRewardInDayCount = int.Parse(result.Logs[0].Message);
//			AddUserVirtualCurrencyResult aceResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//			AccountManager.instance.gameDollarReward = aceResul.BalanceChange;
//			AccountManager.instance.gameDollarAmount = aceResul.Balance;
//		}catch{
//			AccountManager.instance.gameDollarReward = 0;
//		}
//	}
//	#endregion
//	
//	#region Acc
//	void OnLevelComplete(){
//		//		RunCloudScriptRequest request = new RunCloudScriptRequest();
//		//		request.ActionId = "onLevelCompleteCSVR";
//		//		request.ParamsEncoded = "{\"level\":\""+CSVR_GameSetting.accountLevel+"\"}";
//		//Debug.Log (request.ParamsEncoded);
//		//		PlayFabClientAPI.RunCloudScript(request,OnLevelCompleteSuccess, OnLevelCompleteError);
//		
//		var parameter = new {level = CSVR_GameSetting.accountLevel};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onLevelCompleteCSVR";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnLevelCompleteSuccess, OnLevelCompleteError);
//	}
//	void OnLevelCompleteSuccess(ExecuteCloudScriptResult result)
//	{
//		AddUserVirtualCurrencyResult sellResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//		AccountManager.instance.gameCoinAmount = sellResul.Balance;
//		//HomeScreen.instance.UpdateVirtualCurrency ();
//		//Debug.Log ("OnLevelCompleteSuccess" + sellResul.BalanceChange);
//	}
//	void OnLevelCompleteError(PlayFabError error)
//	{
//		//Debug.Log ("error" + error);
//	}
//	
//	
//	public void GiveExpToAccount(int exp){
//		if (exp > 50) {
//			exp = 0;
//		}
//		this.CheckAccountLevel(exp);
//	}
//	// sữa lại lỗi, chưa reset lại biến accountExp
//	private void CheckAccountLevel(int exp)
//	{
//		if (CSVR_GameSetting.accountLevel == 100) return;
//		
//		int nextExp = CSVR_GameSetting.experienceForEveryLevel[CSVR_GameSetting.accountLevel+1];
//		CSVR_GameSetting.accountExp += exp;
//		if (CSVR_GameSetting.accountExp < 0)
//			CSVR_GameSetting.accountExp = 0;
//		
//		if (CSVR_GameSetting.accountExp > nextExp)
//		{
//			CSVR_GameSetting.accountLevel++;
//			OnLevelComplete();
//			CSVR_GameSetting.accountExp -= nextExp;
//			UpdateAccountExpAndLevel(CSVR_GameSetting.accountExp, CSVR_GameSetting.accountLevel);
//			HomeScreen.isLevelUp = true;
//			
//		}else{
//			UpdateAccountExp(CSVR_GameSetting.accountExp);
//		}
//		
//		//CSVR_UIPlayerInfoV2.instance.HienThi ();
//	}
//	private void UpdateAccountExp(int accountExp)
//	{
//		UpdateUserDataRequest request = new UpdateUserDataRequest();
//		request.Data = new Dictionary<string, string>();
//		request.Data.Add(CSVR_GameSetting.accountExpKey, accountExp.ToString());
//		request.Permission = UserDataPermission.Public;
//		PlayFabClientAPI.UpdateUserData(request, OnUpdateAccountExpAndLevelSuccess, OnAddDataError);
//	}
//	private void UpdateAccountExpAndLevel(int accountExp, int accountLevel)
//	{
//		UpdateUserDataRequest request = new UpdateUserDataRequest();
//		request.Data = new Dictionary<string, string>();
//		request.Data.Add(CSVR_GameSetting.accountExpKey, accountExp.ToString());
//		request.Permission = UserDataPermission.Public;
//		PlayFabClientAPI.UpdateUserData(request, OnUpdateAccountExpAndLevelSuccess, OnAddDataError);
//		
//		
//		UpdateUserStatisticsRequest requestStat = new UpdateUserStatisticsRequest();
//		requestStat.UserStatistics = new Dictionary<string, int>();
//		requestStat.UserStatistics.Add(CSVR_GameSetting.accountLevelKey, accountLevel);
//		PlayFabClientAPI.UpdateUserStatistics(requestStat, OnUpdateStatsCompleted, OnUpdateStatsError);
//	}
//	
//	/// <summary>
//	/// Callback used when the account level and exp update succeeds.
//	/// </summary>
//	/// <param name="result">Result.</param>
//	void OnUpdateAccountExpAndLevelSuccess(UpdateUserDataResult result)
//	{
//		
//	}
//	
//	/// <summary>
//	/// Reports user's headshots statistics.
//	/// </summary>
//	/// <param name="amount"></param>
//	
//	#endregion
//	
//	#region RedeemCoupon
//	
//	
//	ProjectDelegates.SimpleCallback OnRedeemCouponCompleted;
//	public void RedeemCoupon(string code,ProjectDelegates.SimpleCallback OnRedeemCouponCompletedCallBack){
//		OnRedeemCouponCompleted = OnRedeemCouponCompletedCallBack;
//		PlayFabClientAPI.RedeemCoupon(new RedeemCouponRequest() {CouponCode=code, CatalogVersion="AlphaTest"}, OnRedeemCouponResult, OnRedeemCouponError);
//	}
//	void OnRedeemCouponResult(RedeemCouponResult result){
//		//AccountManager.instance.inventory.Add (result.GrantedItems[0]);
//		UpdateUserDataRequest request = new UpdateUserDataRequest ();
//		request.Data = new Dictionary<string, string>();
//		request.Data.Add ("SystemInfo", SystemInfo.deviceUniqueIdentifier); 
//		request.Permission = UserDataPermission.Public;
//		PlayFabClientAPI.UpdateUserData(request, SystemInfoSuccess, OnRedeemCouponError);
//	}
//	void SystemInfoSuccess(UpdateUserDataResult result)
//	{
//		OnRedeemCouponCompleted ();
//		AccountManager.instance.OnUpdateCodeCompleted ();
//	}
//	public void RedeemCouponSystemInfo( ProjectDelegates.SimpleCallback OnRedeemCouponCompletedCallBack){
//		//Debug.Log ("RedeemCouponSystemInfo");
//		OnRedeemCouponCompleted = OnRedeemCouponCompletedCallBack;
//		PlayFabClientAPI.GetUserData(new GetUserDataRequest() {}, RedeemCouponSystemInfoResult, OnRedeemCouponError);
//	}
//	public void RedeemCouponSystemInfoResult(GetUserDataResult result){
//		if (result.Data ["SystemInfo"].Value == SystemInfo.deviceUniqueIdentifier){
//			OnRedeemCouponCompleted ();
//		}else{
//			
//			CodeTest.instance.isValidate = false;
//			CodeTest.instance.CheckCode ();
//			errorView.ShowPopupError("Xin nhập lại code test, do chuyển máy. Code chỉ có hiệu lực trên máy được nhập");
//		}
//		
//	}
//	void OnRedeemCouponError(PlayFabError error)
//	{
//		string errorMessage = string.Empty;
//		switch (error.Error) {
//		case PlayFabErrorCode.InvalidParams:
//			errorMessage = "Lỗi: Code đã được sử dụng. Vui lòng nhập code khác.";
//			break;
//		case PlayFabErrorCode.CouponCodeNotFound:
//			errorMessage = "Lỗi: Code không tồn tại. Vui lòng nhập code khác.";
//			break;
//		}
//		
//		errorView.ShowPopupError (errorMessage);
//		//loginScreen
//		//errorView.ShowPopupError ("Error: " + error.ErrorMessage);
//		//Debug.Log("Login error: " + error.Error);
//	}
//	#endregion
//	
//	#region Report Player Cheat
//	public void Event_OnReportPlayer(string mess){
//		errorView.ShowPopupError ("Phát hiện phần mêm gian lận, Bạn sẽ bị thoát khỏi game");
//		ReportPlayerClientRequest request = new ReportPlayerClientRequest (){ReporteeId = playerID, Comment = mess };
//		PlayFabClientAPI.ReportPlayer (request, OnReportPlayerSuccess, OnReportPlayerError);
//	}
//	void OnReportPlayerSuccess(ReportPlayerClientResult result){
//		Application.Quit ();
//	}
//	void OnReportPlayerError(PlayFabError error){
//		Application.Quit ();
//	}
//	#endregion
//	
//	#region Report Player Quit Game
//	public void OnReportPlayerQuitGame(){
//		var parameter = new {Level = CSVR_GameSetting.accountLevel};
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onPlayerQuitGame";
//		request.FunctionParameter = parameter;
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnReportPlayerQuitGameSuccess, OnReportPlayerQuitGameFaile);
//	}
//	void OnReportPlayerQuitGameSuccess(ExecuteCloudScriptResult result){
//		//	Debug.Log ("OnReportPlayerQuitGameSuccess" + result.FunctionResult.ToString ());
//		AddUserVirtualCurrencyResult sellResul = PlayFab.Json.JsonConvert.DeserializeObject<AddUserVirtualCurrencyResult>(result.FunctionResult.ToString());
//		AccountManager.instance.gameCoinAmount = sellResul.Balance;
//	}
//	void OnReportPlayerQuitGameFaile(PlayFabError error){
//		
//	}
//	#endregion
//	
//	#region Report Player Quit Game
//	public void OnGrantItemToPlayer(){
//		ExecuteCloudScriptRequest request = new ExecuteCloudScriptRequest();
//		request.FunctionName = "onGrantItemsFirstLogin";
//		request.RevisionSelection = CloudScriptRevisionOption.Live;
//		request.GeneratePlayStreamEvent = true;
//		PlayFabClientAPI.ExecuteCloudScript(request,OnGrantItemToPlayerSuccess, OnGrantItemToPlayerFaile);
//	}
//	void OnGrantItemToPlayerSuccess(ExecuteCloudScriptResult result){}
//	void OnGrantItemToPlayerFaile(PlayFabError error){}
//	#endregion
//	
//	//#region Report Player Quit Game
//	//	public PhotonView PhotonViewMPConnection(){
//	//		return mpConnectionInstance.transform.Find(CSVR_GameSetting.modeGame).GetComponent<PhotonView>();
//	//	}
//	//#endregion
//	
//}