﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if !UNITY_EDITOR_WIN && !UNITY_STANDALONE_WIN && !VIRTUAL_ROOM
using GooglePlayGames.BasicApi;
#endif
using UnityEngine.SocialPlatforms;

public class GoogleManager : MonoBehaviour 
{
	/// <summary>
	/// Callback to be called when Google login process completes.
	/// </summary>
	ProjectDelegates.GoogleLoginCallback OnGoogleLoginCompletedCallback;
	
	/// <summary>
	/// User's facebook name.
	/// </summary>
	string googlekName="";
	
	/// <summary>
	/// User's facebook picture URL.
	/// </summary>
	string googlePictureUrl="";
	
	void Start(){
		#if !UNITY_EDITOR_WIN && !UNITY_STANDALONE_WIN && !VIRTUAL_ROOM
		GooglePlayGames.PlayGamesPlatform.Activate();
		#endif
	}
	/// <summary>
	/// Function used to initialize facebook.
	/// </summary>
	public void Initialize(ProjectDelegates.GoogleLoginCallback OnLoginCompletedCallback)
	{
		this.OnGoogleLoginCompletedCallback = OnLoginCompletedCallback;
		
		if (!Social.localUser.authenticated) 
		{
			Social.localUser.Authenticate((bool success) => {
				if (success) {

					OnLoginCompleted(success);

				} //else {
					//Debug.Log("G+ login failed");
				//}
			});
		}
		else
		{
			OnProfileInfoLoaded(Social.localUser.authenticated);
		}
	}

	/// <summary>
	/// Callback used when Facebook login is complete.
	/// </summary>
	/// <param name="result">Login result</param>
	void OnLoginCompleted(bool isLoggedIn)
	{
		if (!isLoggedIn)
		{
			//Debug.Log("login cancelled, by user or error");
		}
		else
		{
			//Debug.Log("login successful");
			OnProfileInfoLoaded(isLoggedIn);
		}
	}

	/// <summary>
	/// Callback used when user's facebook picture loads.
	/// </summary>
	/// <param name="result"></param>
	void OnProfileInfoLoaded(bool authenticated)
	{
		if(authenticated){
			if (Social.localUser.image == null) {
				//Debug.Log("problem with getting profile picture!");
				OnProfileInfoLoaded(Social.localUser.authenticated);
				return;
			} 
			googlekName = Social.localUser.userName;
			#if !UNITY_EDITOR_WIN && !UNITY_STANDALONE_WIN && !VIRTUAL_ROOM
			googlePictureUrl = ((GooglePlayGames.PlayGamesLocalUser)Social.localUser).AvatarURL;

			string token = GooglePlayGames.PlayGamesPlatform.Instance.GetToken();

			this.OnGoogleLoginCompletedCallback(token);
			#endif

		}
	}

	/// <summary>
	/// Function needed to handle application win/lose focus
	/// </summary>
	/// <param name="isGameShown">Is focus on app?</param>
	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			// pause the game - we will need to hide
			Time.timeScale = 0;
		}
		else
		{
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}

	/// <summary>
	/// Google Logout
	/// </summary>
	public void Logout()
	{
		#if !UNITY_EDITOR_WIN && !UNITY_STANDALONE_WIN && !VIRTUAL_ROOM
		((GooglePlayGames.PlayGamesPlatform) Social.Active).SignOut();
		#endif
	}
}
