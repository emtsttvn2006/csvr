﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Horus.ClientModels;

public class AccountManager : MonoBehaviour
{
    /// <summary>
    /// Static instance.
    /// </summary>
    public static AccountManager instance;

    /// <summary>
    /// Experience amount needed for each level.
    /// </summary>
    int[] _experienceForEveryLevel = new int[] { 0, 1000, 4153, 7325, 10520, 13740, 16989, 20270, 23587, 26946, 
                                        31487, 36098, 40788, 45569, 50450, 55446, 60572, 65844, 71282, 76906, 
                                        84201, 91793, 99723, 108034, 116775, 126002, 135779, 146177, 157276, 169169, 
                                        184515, 201076, 219011, 238496, 259735, 282955, 308414, 336402, 367248, 401325, 445339, 
                                        494165, 548429, 608837, 676188, 751384, 835446, 929526, 1034927, 1203773 };

    /// <summary>
    /// Property reference for experience amount needed for each level.
    /// </summary>
    public int[] experienceForEveryLevel
    {
        get { return _experienceForEveryLevel; }
    }

    /// <summary>
    /// Login completed callback.
    /// </summary>
    ProjectDelegates.SimpleCallback OnLoginCompleted;

	/// <summary>
	/// Facebook manager reference.
	/// </summary>
	CSVRManager csvrkManager;

    /// <summary>
    /// Facebook manager reference.
    /// </summary>
    FacebookManager facebookManager;

	/// <summary>
	/// Facebook manager reference.
	/// </summary>
	GoogleManager googlekManager;

	/// <summary>
	/// PlayFab manager reference.
	/// </summary>
	HorusManager horusManager;

    /// <summary>
    /// Player inventory.
    /// </summary>
    //List<ItemInstance> inventory;

    /// <summary>
    /// Player display name.
    /// </summary>
    public string displayName { get; set; }

    /// <summary>
    /// User's picture from facebook.
    /// </summary>
    public Sprite avatarPicture { get; set; }
	public Texture2D avatarPlayerTexture;

    /// <summary>
    /// Player virtual currency amount.
    /// </summary>
    public int gameCoinAmount { get; set; }
	/// <summary>
	/// Player virtual currency amount.
	/// </summary>
	public int gameDollarAmount { get; set; }

	public string[] roles { get; set; }
	public ItemSpecific gameMoneyAmount { get; set; }
	public Dictionary<string,Dictionary<string,int>> dailyGame { get; set; }

	public List<ItemInstance> itemsNew { get; set; }
	public List<ItemInstance> inventory { get; set; }
	public List<string> keyCards { get; set; }



    public int accountLevel { get; private set; }
    public ClanInfo clan { get; private set; }
    public int accountExp { get; private set; }
    public int killCount { get; set; }
    public int winsAmount { get; set; }
	public int deadAmount { get; set; }
	public int lostAmount { get; set; }
	public int headshotAmount { get; set; }


    /// <summary>
    /// Prevents object from being destroyed, gets necessary references.
    /// </summary>
    void Awake()
    {
	//	PlayerPrefs.DeleteAll();
        instance = this;
        DontDestroyOnLoad(this.gameObject);
		csvrkManager = this.GetComponent<CSVRManager>();
        facebookManager = this.GetComponent<FacebookManager>();
		googlekManager = this.GetComponent<GoogleManager> ();
       // playFabManager = this.GetComponent<PlayFabManager>();
		horusManager = this.GetComponent<HorusManager>();

//		if(PlayerPrefs.GetInt("CSVR_GameSetting.isAutoLogin") == 1){
//
//			CSVR_GameSetting.LoadSetting ();
//		}
    }
	void Start(){
		avatarPicture = Sprite.Create (avatarPlayerTexture, new Rect (0, 0, 128, 128), new Vector2 ());
		itemsNew = new List<ItemInstance> ();
		
		Application.targetFrameRate = 30;
		QualitySettings.vSyncCount  = 0; 
		QualitySettings.antiAliasing = 0;
		QualitySettings.shadowCascades = 0;
		QualitySettings.shadowDistance = 15;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Application.runInBackground = true;        

		//useGUILayout = false;

//		var tmp =new System.Object[1024];
//		//make allocations in smaller blocks to avoid them to be treated in a special way, which is designed for large blocks
//		for (int i=0; i<1024; i++) {
//			tmp [i] = new byte[1024];	
//		}
//		//release reference
//		tmp=null;

	}
	/// <summary>
	/// Starts CSVR login.
	/// </summary>
	/// <param name="OnLoginCompleted">Login completed callback.</param>
	public void RegisterWithCSVR(ProjectDelegates.SimpleCallback OnLoginCompleted)
	{
		this.OnLoginCompleted = OnLoginCompleted;
		csvrkManager.InitializeRes(OnPlayfabRegisterCompleted);
	}
	/// <summary>
	/// Starts CSVR login.
	/// </summary>
	/// <param name="OnLoginCompleted">Login completed callback.</param>
	public void LoginWithCSVR(ProjectDelegates.SimpleCallback OnLoginCompleted)
	{
		//UnityEngine.Debug.Log ("horusManager.Initialize(OnPlayfabLoginCompleted");
		this.OnLoginCompleted = OnLoginCompleted;
		csvrkManager.Initialize(OnPlayfabLoginCompleted);
		//horusManager.Initialize(OnPlayfabLoginCompleted);
	}

    /// <summary>
    /// Starts facebook login.
    /// </summary>
    /// <param name="OnLoginCompleted">Login completed callback.</param>
    public void LoginWithFacebook(ProjectDelegates.SimpleCallback OnLoginCompleted)
    {

        this.OnLoginCompleted = OnLoginCompleted;
        facebookManager.Initialize(OnFacebookLoginCompleted);
    }

	/// <summary>
	/// Starts google login.
	/// </summary>
	/// <param name="OnLoginCompleted">Login completed callback.</param>
	public void LoginWithGoogle(ProjectDelegates.SimpleCallback OnLoginCompleted)
	{
		this.OnLoginCompleted = OnLoginCompleted;
		googlekManager.Initialize(OnGoogleLoginCompleted);
	}

    /// <summary>
    /// Logs out from services.
    /// </summary>
    public void LogoutFromEverything()
    {
        //facebookManager.Logout();
      //  playFabManager.Logout();
    }

	/// <summary>
	/// Called when playfab login completes.
	/// </summary>
	/// <param name="username">User's username</param>
	/// <param name="password">User's password</param>
	void OnPlayfabRegisterCompleted(string username,string password,string email,string phoneNumber)
	{
//		playFabManager.CreateNewUser(username,password,email, OnPlayFabLoginCompleted);
		horusManager.CreateNewUser(username,password,email,phoneNumber, OnPlayFabLoginCompleted);
	}

	/// <summary>
	/// Called when playfab login completes.
	/// </summary>
	/// <param name="username">User's username</param>
	/// <param name="password">User's password</param>
	void OnPlayfabLoginCompleted(string username,string password)
	{
		//playFabManager.LoginWithPlayFab(username,password, OnPlayFabLoginCompleted);
//		Debug.Log("username."+username);
//		Debug.Log("password."+password);
		horusManager.LoginWithPlayFab(username,password, OnPlayFabLoginCompleted);


	}
    
    /// <summary>
    /// Called when facebook login completes.
    /// </summary>
    /// <param name="accessToken">Facebook's access token</param>
    /// <param name="facebookName">Facebook's user name</param>
    void OnFacebookLoginCompleted(string accessToken)
    {
		horusManager.LoginWithFacebook(accessToken, OnPlayFabLoginCompleted);
    }

	/// <summary>
	/// Called when google login completes.
	/// </summary>
	/// <param name="accessToken">Google's access token</param>
	/// <param name="FacebookName">Google's user name</param>
	void OnGoogleLoginCompleted(string accessToken)
	{
	//	playFabManager.LoginWithGoogle(accessToken, OnPlayFabLoginCompleted);
	}

    /// <summary>
    /// Called when PlayFab login completed.
    /// </summary>
    /// <param name="inventory">User inventory.</param>
    /// <param name="displayName">User display name.</param>
    /// <param name="currency">User currrency amount.</param>
    /// <param name="accountLevel">User account level.</param>
    /// <param namhttp://open.spotify.com/app/radioe="accountExp">User account experience points.</param>
	void OnPlayFabLoginCompleted(List<Horus.ClientModels.ItemInstance> inventory,string displayName,int gc,int gd,int accountExp, ClanInfo clan)
    {
		keyCards = new List<string> ();
		this.inventory = inventory;
        this.clan = clan;
        this.gameCoinAmount = gc;
		this.gameDollarAmount = gd;

        this.displayName = displayName;
		Debug.Log (displayName);
        this.accountExp = accountExp;

		//playFabManager.GetGameNews (21);

		string codeTest = horusManager.GetTitleValue ("Key_CodeTest");
		if(codeTest == "1")
			CheckCode ();
		else
			OnLoginCompleted();

    }
	public void CheckCode(){
		if (inventory.Count == 0) {
			//Debug.Log("CodeTest.instance.CheckCode ();");
			CodeTest.instance.CheckCode ();
		} else {
			for (int i = 0,lenght = inventory.Count; i < lenght; i++) {
				if (inventory [i].CatalogVersion == "AlphaTest") {
					CodeTest.instance.isValidate = true;
					//Debug.Log("	PlayFabManager.instance.RedeemCouponSystemInfo (OnUpdateCodeCompleted);");
					//PlayFabManager.instance.RedeemCouponSystemInfo (OnUpdateCodeCompleted);
					break;
				}
			}
			if(!CodeTest.instance.isValidate){
				CodeTest.instance.CheckCode ();
			}
		}
	}
	public void OnUpdateCodeCompleted(){
		OnLoginCompleted();
	}
    /// <summary>
    /// Method used to update user statistics, before sendinf to PlayFab.
    /// </summary>
    /// <param name="killCount">KillCount number.</param>
    /// <param name="winsCount">Number of wins.</param>
	public void UpdateUserStatistics(int killCount, int winsCount,int levelsCount,int deadsCount,int lostsCount,int headshotsCount)
    {
		this.accountLevel = levelsCount;

        this.killCount = killCount;
        this.winsAmount = winsCount;
		this.deadAmount = deadsCount;
		this.lostAmount = lostsCount;
		this.headshotAmount = headshotsCount;
    }


	
}