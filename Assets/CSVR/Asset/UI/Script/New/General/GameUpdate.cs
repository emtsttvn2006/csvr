﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.UI;
using System;

public class GameUpdate : MonoBehaviour {
	public Slider progressBar;
	public Text percentageText;
	double bytesIn;
	double totalBytes;
	double percentage;
	string apkname;
	string apkserver;
	void Start() {
		apkname = HorusManager.instance.GetTitleValue ("Key_APKName");

		apkserver = HorusManager.instance.GetTitleValue ("Key_APKServer");

		StartCoroutine ("DOWNLOAD");
	}
	IEnumerator DOWNLOAD()
	{
		string realPath = Application.persistentDataPath + "/" + apkname;

//		string uri = "http://103.53.168.68/" + apkname;
		string uri = apkserver+apkname;
		//Debug.Log ("FileExists " + FileExists (realPath));
//		yield return null;
		if (!FileExists (realPath)) {
			WebClient client = new WebClient ();
			client.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler (DownloadFileCallback);
			client.DownloadProgressChanged += new DownloadProgressChangedEventHandler (client_DownloadProgressChanged);
			client.DownloadFileAsync (new Uri (uri), Application.persistentDataPath + "/" + apkname);
			yield return null;
		} else {
			Application.OpenURL (realPath);
		}

	}

	void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
	{

		bytesIn =  double.Parse(e.BytesReceived.ToString())/1000000;
		totalBytes =  double.Parse(e.TotalBytesToReceive.ToString())/1000000;
		percentage = bytesIn / totalBytes * 100 ;
	}
	void DownloadFileCallback(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
	{
		if (e.Error == null)
		{
			string realPath = Application.persistentDataPath + "/" + apkname;
			Application.OpenURL (realPath);
		}
	}
	private bool FileExists(string sPathName) {
		return System.IO.File.Exists(sPathName) ? true : false;
//		try {
//			return (System.IO.Directory.Exists(sPathName));  //Exception for folder
//		} catch (Exception e) {
//			return (false);                                   //Error occured, return False
//		}
	}
	void Update(){
		percentageText.text = Math.Round(bytesIn,2) + " / " + Math.Round(totalBytes,2) +"Mb";
		progressBar.value = int.Parse(Math.Truncate(percentage).ToString());
	}
}
