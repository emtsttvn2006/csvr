﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TypeWeaponSniper{AWP, SCAR20, AUG}
public enum TypeWeapon{AWP, SCAR20, AUG}
public enum Scenes { Login, Home, Loading, Lobby }
public enum States{ UpdateName, Home, Lobby, EndGame, EndGamePVE,LevelUp }
public enum TeamTypes { Police = 0, Terrorist = 1, RoomHostPlayer = 2 }
public enum StatusItem { New, Vip, Hot, Normal, Disable}
#if DEV_DATA
[System.Serializable]
#endif
public class CSVR_ArmorInfo{
	public string Reliability { get; set;}
	public string Resilience { get; set;}
	
	public string Status { get; set;}
	public string ItemID { get; set;}
}

#if DEV_DATA
[System.Serializable]
#endif
public class CSVR_GunInfo{
	public float D0 { get; set;}
	
	//hop dan
	public float G1 { get; set;}
	//toc do ban
	public float G2 { get; set;}
	//trong luc
	public string G3 { get; set;}
	//nap dan
	//public string G4 { get; set;}
	public string G4;
	//do giat
	public float G5 { get; set;}
	//do choang
	public string G6 { get; set;}
	
	public float G7 { get; set;}
	
	public float R1X { get; set;}
	
	public float R1Y { get; set;}
	
	public float R1 { get; set;}
	
	public float R2X { get; set;}
	
	public float R2Y { get; set;}
	
	public float R2 { get; set;}
	
	public float R3X { get; set;}
	
	public float R3Y { get; set;}
	
	public float R3 { get; set;}
	
	public float R0 { get; set;}
	
	public float A1 { get; set;}
	
	public float A2 { get; set;}
	
	public float A3 { get; set;}
	
	public float A4 { get; set;}
	
	public int Level { get; set;}
	
	public int SellPrices { get; set;}
	
	public string Status { get; set;}

	public string ItemID; //De check backpack
	public string ScopeID; //De check backpack
	public string MagazineID; //De check backpack
	public string GripID; //De check backpack
	public string MuzzleID; //De check backpack
	public string SkinID; //De check backpack
	public string AccID; //De check backpack
}

#if DEV_DATA
[System.Serializable]
#endif
public class CSVR_GrenadeInfo{
	public float D0 { get; set;}
	//sat thuong
	public float Damge { get; set;}
	//sat thuong
	public float Ranges { get; set;}
	//xuyen giap
	public float AP { get; set;}
	//cân nặng
	public float Weight { get; set;}
	//Tốc độ
	public float Speed { get; set;}

	public int Level{ get; set;}
	
	public int SellPrices { get; set;}
	
	public string Status { get; set;}

	//public string ItemID { get; set;}
	public string ItemID; //De check backpack

	public float A1 { get; set;}

	public float A2 { get; set;}

	public float A3 { get; set;}

	public float A4 { get; set;}
}
#if DEV_DATA
[System.Serializable]
#endif
public class CSVR_MeleInfo{
	public float D0 { get; set;}
	//sat thuong
	public float Damge { get; set;}
	//sat thuong
	public float Ranges { get; set;}
	//xuyen giap
	public float AP { get; set;}
	//cân nặng
	public float Weight { get; set;}
	//Tốc độ
	public float Speed { get; set;}

	public int Level { get; set;}
	
	public int SellPrices { get; set;}
	
	public string Status { get; set;}

	//public string ItemID { get; set;}
	public string ItemID; //De check backpack

	public float A1 { get; set;}

	public float A2 { get; set;}

	public float A3 { get; set;}

	public float A4 { get; set;}
}


#if DEV_DATA
[System.Serializable]
#endif
public class CSVR_AccessoriInfo{

	//hop dan
	public ACInfo G1 { get; set;}
	//toc do ban
	public ACInfo G2 { get; set;}
	//trong luc
	public ACInfo G3 { get; set;}
	//nap dan
	//public string G4 { get; set;}
	public ACInfo G4;
	//do giat
	public ACInfo G5 { get; set;}
	//do choang
	public ACInfo G6 { get; set;}

	public ACInfo G7 { get; set;}

	public ACInfo R1X { get; set;}

	public ACInfo R1Y { get; set;}

	public ACInfo R1 { get; set;}

	public ACInfo R2X { get; set;}

	public ACInfo R2Y { get; set;}

	public ACInfo R2 { get; set;}

	public ACInfo R3X { get; set;}

	public ACInfo R3Y { get; set;}

	public ACInfo R3 { get; set;}

	public ACInfo R0 { get; set;}

	public ACInfo A1 { get; set;}

	public ACInfo A2 { get; set;}

	public ACInfo A3 { get; set;}

	public ACInfo A4 { get; set;}

	public string accessory_type { get; set;}
	public string Status { get; set;}
}
public class ACInfo{
	public string value { get; set;}
	public string action { get; set;}
}

public class CSVR_FriendInfo{
	public string level;
	//sat thuong
	public string rating;

	public Sprite avatar;
}
public class CSVR_DayInfo{
	public System.DateTime D1; // Extend day
	public System.DateTime D2; // Expiration
}

public class CSVR_Victim{
	public string Name = string.Empty;
	public string Level = string.Empty;
	public string Heath = string.Empty;
}

public class LobbyResultV2
{
	public Dictionary<string,string> ModeAvaiable { get; set; }
	public Dictionary<string,string> MapAvaiable { get; set; }
	public List<ItemModeV2> Lobby { get; set; }
}

public class ItemModeV2
{
	public string ModeName { get; set; }
	public bool Interactable { get; set; }
	public string[] Maps { get; set; }
}

public class CSVR_UIAIInfo
{
	public string AI_ID;
	public string AI_Name;
	public int AI_Avatar;
	public int AI_TeamNumber;
	public int AI_Level;
	public int AI_Kill;
	public int AI_Death;
	public int AI_HeadShot;
	public string AI_Weapon;
}
