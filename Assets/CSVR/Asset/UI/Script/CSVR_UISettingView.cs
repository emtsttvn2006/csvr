﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CSVR_UISettingView : SingletonBaseScreen <CSVR_UISettingView>{
    
	//cai dat chung
	public Toggle[] ngamtudong;
	public Toggle[] doivukhi;
	public Toggle[] ngoi;
	public Toggle[] nhay;
	public Toggle[] ngam;
	public Toggle[] ban;
	public Toggle[] camvukhi;
	public Toggle[] dichuyen;

	public Slider zoomSensitivitySpeed;
	public Slider lookSensitivitySpeed;
	//cai dat dieu khien
	public Toggle[] controls;
	public GameObject[] controlsHUD;

	//cai dat khac
	public Slider music;
	public Slider soundFX;
	public Slider qualitySeting;


	public AudioSource bgMusic;
	public AudioSource fxMusic;
	public bool inGame = false;
	public override void Awake()
    {
        //base.Awake();
		if (!inGame) {
			bgMusic = HomeScreen.instance.bgMusic;
			fxMusic	= HomeScreen.instance.fxMusic;
		}
		if (PlayerPrefs.HasKey ("CSVR_GameSetting.Quality")) {
			qualitySeting.value = (int)PlayerPrefs.GetFloat ("CSVR_GameSetting.Quality");
		}
		if (inGame) {
			if (PlayerPrefs.HasKey ("controlsToggle")) {
				if (PlayerPrefs.GetInt ("controlsToggle") == 1) {
					controls [0].isOn = true;
					UI_OnControllChangeClick (1);
				} else if (PlayerPrefs.GetInt ("controlsToggle") == 2) {
					controls [1].isOn = true;
					UI_OnControllChangeClick (2);
				} else if (PlayerPrefs.GetInt ("controlsToggle") == 3) {
					controls [2].isOn = true;
					UI_OnControllChangeClick (3);
				}
			} else {
				controls [0].isOn = true;
				UI_OnControllChangeClick (1);
			}
			this.gameObject.SetActive (false);
		} else {
			if (PlayerPrefs.HasKey ("controlsToggle")) {
				if (PlayerPrefs.GetInt ("controlsToggle") == 1) {
					controls [0].isOn = true;
				} else if (PlayerPrefs.GetInt ("controlsToggle") == 2) {
					controls [1].isOn = true;
				} else if (PlayerPrefs.GetInt ("controlsToggle") == 3) {
					controls [2].isOn = true;
				}
			} else {
				controls [0].isOn = true;
			}
		}

		if (PlayerPrefs.HasKey ("CSVR_GameSetting.music")) {
			music.value = PlayerPrefs.GetFloat ("CSVR_GameSetting.music");
		} else {
			music.value = 1;
		}
		if (PlayerPrefs.HasKey ("CSVR_GameSetting.soundEfx")) {
			soundFX.value = PlayerPrefs.GetFloat("CSVR_GameSetting.soundEfx");
		} else {
			soundFX.value = 1;
		}
		if (PlayerPrefs.HasKey ("lookSensitivitySpeed")) {
			CSVR_GameSetting.lookSensitivitySpeed = PlayerPrefs.GetFloat("lookSensitivitySpeed");
		} else {
			CSVR_GameSetting.lookSensitivitySpeed = 0.5f;
		}

		if (PlayerPrefs.HasKey ("zoomSensitivitySpeed")) {
			CSVR_GameSetting.zoomSensitivitySpeed = PlayerPrefs.GetFloat("zoomSensitivitySpeed");
		} else {
			CSVR_GameSetting.zoomSensitivitySpeed = 0.5f;
		}
		if (PlayerPrefs.HasKey ("CSVRChangeTayCamSung")) {
			CSVR_GameSetting.TayCamSung = PlayerPrefs.GetFloat("CSVRChangeTayCamSung");
		} else {
			CSVR_GameSetting.TayCamSung = 1;
		}

	}
	void Start(){

		lookSensitivitySpeed.value = CSVR_GameSetting.lookSensitivitySpeed;
		zoomSensitivitySpeed.value = CSVR_GameSetting.zoomSensitivitySpeed;
		if (CSVR_GameSetting.TayCamSung == 2) {
			camvukhi [0].isOn = true;
			camvukhi [1].isOn = false;
		} else {
			camvukhi[0].isOn = false;
			camvukhi[1].isOn = true;
		}
	}
	public void UI_OnControllChangeClickUIHome(int i){
		if (controls [0].isOn) {
			PlayerPrefs.SetInt ("controlsToggle", 1);
		} else if (controls [1].isOn) {
			PlayerPrefs.SetInt ("controlsToggle", 2);
		} else if (controls [2].isOn) {
			PlayerPrefs.SetInt ("controlsToggle", 3);
		}
	}
	public void UI_OnControllChangeClick(int i){
		if (controls [0].isOn) {
            CSVR_GameSetting.StateControl = 1;
			PlayerPrefs.SetInt ("controlsToggle", 1);
			controls [1].isOn = false;
			controls [2].isOn = false;
			controlsHUD [0].gameObject.SetActive(true);
			controlsHUD [1].gameObject.SetActive(false);
			controlsHUD [2].gameObject.SetActive(false);
		} else if (controls [1].isOn) {
            CSVR_GameSetting.StateControl = 2;
			PlayerPrefs.SetInt ("controlsToggle", 2);
			controls [0].isOn = false;
			controls [2].isOn = false;
			controlsHUD [0].gameObject.SetActive(false);
			controlsHUD [1].gameObject.SetActive(true);
			controlsHUD [2].gameObject.SetActive(false);
		} else if (controls [2].isOn) {
            CSVR_GameSetting.StateControl = 3;
			PlayerPrefs.SetInt ("controlsToggle", 3);
			controls [0].isOn = false;
			controls [1].isOn = false;
			controlsHUD [0].gameObject.SetActive(false);
			controlsHUD [1].gameObject.SetActive(false);
			controlsHUD [2].gameObject.SetActive(true);
		}
	}
//	public void UI_OnSettingSaveControllLookY(float f) {
//
//		PlayerPrefs.SetFloat("zoomSensitivitySpeed",f);
//
//	}
//	public void UI_OnSettingSaveControllLookX(float f) {
//		
//		PlayerPrefs.SetFloat("lookSensitivitySpeed",f);
//		
//	}
	//Setting Save Controll
	public void UI_OnSettingSaveControll() {
//		CSVR_GameSetting.gyroEnabled = gyroEnabled.isOn;
		CSVR_GameSetting.zoomSensitivitySpeed = zoomSensitivitySpeed.value;
		PlayerPrefs.SetFloat("zoomSensitivitySpeed",CSVR_GameSetting.zoomSensitivitySpeed);

		CSVR_GameSetting.lookSensitivitySpeed = lookSensitivitySpeed.value;
		PlayerPrefs.SetFloat("lookSensitivitySpeed",CSVR_GameSetting.lookSensitivitySpeed);
		this.gameObject.SetActive(false);
	}

	//Setting Save Music
	public void UI_OnSettingSaveMusic() {
		CSVR_GameSetting.music = music.value;
		CSVR_GameSetting.soundEfx = soundFX.value;
		CSVR_GameSetting.SaveSetting();
		this.gameObject.SetActive(false);
	}
	//Setting On
	public void UI_OnSettingHomeBackOn() {
		this.transform.gameObject.SetActive(true);
//		CSVR_GUITabController guiTab = this.transform.GetComponent<CSVR_GUITabController>();
//		guiTab.Run(guiTab.tabContentsPairs[3]);
	}
	//Setting On
	public void UI_OnSettingExitOn() {
		if (inGame) {
			if (PhotonNetwork.isMasterClient) {
				//PlayFabManager.instance.ChoiseMasterClient ();
			}
			gameObject.SetActive (false);
			ErrorView.instance.CoutDownQuitGame (5, "Bạn sẽ thoát game sau ");
		} else {
			Application.Quit();
		}
	}

	//Setting On
	public void UI_OnSettingOn() {
		this.transform.gameObject.SetActive(true);
	}

	//Setting Off
	public void UI_OnSettingOff() {
		this.transform.gameObject.SetActive(false);
	}

	public void UpdateSoundEfx (float value)
	{
		if(fxMusic != null)
			fxMusic.volume = value;
		PlayerPrefs.SetFloat("CSVR_GameSetting.soundEfx",value);


		//CSVR_MusicManager.instance.efxSource.volume = value;
	}
	public void UpdateMusic (float value)
	{
		if(bgMusic != null)
			bgMusic.volume = value;
		PlayerPrefs.SetFloat("CSVR_GameSetting.music",value);
		//CSVR_MusicManager.instance.musicSource.volume = value;
	}
	public void Logout(){
		PlayerPrefs.SetString ("CSVR_GameSetting.isAutoLogin", "false");
		AccountManager acc =  FindObjectOfType(typeof(AccountManager)) as AccountManager;
		Destroy(acc.gameObject);
		vp_MPConnection.StayConnected = false;
		PhotonNetwork.Disconnect ();
		Application.LoadLevel("Login");
	}
	public void ExitGame(){
		if (inGame) {
			if (PhotonNetwork.isMasterClient) {
				//PlayFabManager.instance.ChoiseMasterClient ();
			}
			//PlayFabManager.instance.OnReportPlayerQuitGame ();
			gameObject.SetActive (false);
			ErrorView.instance.CoutDownExitGame (5, "Bạn sẽ thoát trận sau ");
		}

	}

	public void ChangeQuality(float i) {
		PlayerPrefs.SetFloat("CSVR_GameSetting.Quality",i);
		QualitySettings.SetQualityLevel((int)i, true);

	}
	public void ChangeNgamTuDong(float i) {
		PlayerPrefs.SetFloat("CSVRChangeNgamTuDong",i);

	}
	public void ChangeTayCamSung(float i) {
		CSVR_GameSetting.TayCamSung = i;
		PlayerPrefs.SetFloat("CSVRChangeTayCamSung",i);
	}

	public void UI_OnCustomControllClick(){

		CSVR_UIEditControll.instance.Open ();

	}
	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}

//	private void OnApplicationQuit (){
//		if (inGame && PhotonNetwork.isMasterClient) {
//			ExitGame();
//		}
//	}
}
