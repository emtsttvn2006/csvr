//using UnityEngine;
//using UnityEngine.UI;
//using System.Collections;
//using System.Collections.Generic;
//using PlayFab.ClientModels;
//using UnityEngine.EventSystems;
//
//public class CSVR_UIHome_v2 : MonoBehaviour {
//	public static CSVR_UIHome_v2 instance;
//	public int currentGroup = 1;
//	int IdModel;
//	string idWeaponShop;
//	public GameObject Terrorism;
//	public GameObject Swat;
//	//	[SerializeField]
//	//	Text groupTxt;
//	public Text UI_CurrencyGCTxt;
//	public Text UI_CurrencyGDTxt;
//	public Text UI_PingTxt;
//	
//	public GameObject inventoryGroup;
//	public GameObject[] ListWeaponSwatShop;
//	public GameObject[] ListWeaponTerrorismShop;
//	[SerializeField]
//	Text nameTxt;
//	
//	[SerializeField]
//	Text levelTxt;
//	
//	[SerializeField]
//	Image expBarFill;
//	
//	public CSVR_GUITabController tabGunHome,tabGunShop;
//	//	public GameObject[] inventoryTabs;
//	
//	public GameObject itemPanel,knifePanel,armorPanel,grenadePanel,moreMainPanel,morePistolPanel,moreMelePanel,moreArmorPanel,moreGrenadePanel;
//	public GameObject mainGunInvContent,pistolInvContent,armorInvContent,meleInvContent,grenadeInvContent;
//	
//	private string itemClass,dayExpired;
//	private bool group1,group2,group3,isExpired = false;
//	public Sprite[] iconType;
//	CSVR_GunInfo infoGunTempCurrent,infoGunTempNext;
//	CSVR_ArmorInfo infoArmorTempCurrent,infoArmorTempNext;
//	CSVR_MeleInfo infoMeleeTempCurrent,infoMeleeTempNext;
//	CSVR_GrenadeInfo infoGrenadeTempCurrent,infoGrenadeTempNext;
//	CSVR_DayInfo infoDayTemp;
//	float fillAmount;
//	int lvGun = 0; 
//	
//	Text textCache;
//	GameObject destroyThis;
//	//cache equip
//	Button cacheEquip;
//	string cacheId;
//	string cacheClass;
//    Animator anim;
//	struct SwitchButton {
//		public string idWeapon;
//		public Button equip;
//		public Button unequip;
//		public int group;
//		public bool isEx;
//		public void SetButtonInteractable(bool b) {
//			if (unequip != null) {
//				unequip.gameObject.SetActive(!b);
//				equip.gameObject.SetActive(b);
//			}
//		}
//		public void SetButtonInteractableWithGroup(bool b,int g) {
//			//Debug.Log("group "+group);
//			if (group == g || group == 0) {
//				unequip.gameObject.SetActive(!b);
//				equip.gameObject.SetActive(b);
//			}
//		}
//	}
//	List<SwitchButton> switchButtonContentsPistol = new List<SwitchButton>();
//	List<SwitchButton> switchButtonContentsMainGun = new List<SwitchButton>();
//	List<SwitchButton> switchButtonContentsArmor = new List<SwitchButton>();
//	List<SwitchButton> switchButtonContentsMelee = new List<SwitchButton>();
//	List<SwitchButton> switchButtonContentsGrenade = new List<SwitchButton>();
//	public void ClearListWeapon()
//	{
//		switchButtonContentsPistol.Clear();
//		switchButtonContentsMainGun.Clear();
//		switchButtonContentsArmor.Clear();
//		switchButtonContentsMelee.Clear();
//		switchButtonContentsGrenade.Clear();
//	}
//	//temp equip
//	//	List<Button> tempEquips;
//	//	List<string> tempIds;
//	//	List<string> tempClasss;
//	
//	void Awake()
//	{
//		IdModel=1;
//		instance = this;
//		idWeaponShop = "4_04_M4A4";
//		ListWeaponSwatShop[5].SetActive(true);           
//		ListWeaponTerrorismShop[5].SetActive(true);
//	}
//	public void SetModel(int idModel)
//	{
//        
//		if (idModel == 1)
//		{
//			IdModel = 1;           
//			Swat.SetActive(true);
//            anim=Swat.GetComponent<Animator>();
//			Terrorism.SetActive(false);
//			for (int i = 0; i < ListWeaponSwatShop.Length; i++)
//			{
//				if (ListWeaponSwatShop[i].name == idWeaponShop)
//				{
//					ListWeaponSwatShop[i].SetActive(true);
//				}
//				else
//				{
//					ListWeaponSwatShop[i].SetActive(false);
//				}
//			}
//		}
//		else
//		{
//			IdModel = 2;
//			Swat.SetActive(false);
//			Terrorism.SetActive(true);
//            anim = Terrorism.GetComponent<Animator>();
//			for (int i = 0; i < ListWeaponTerrorismShop.Length; i++)
//			{
//				if (ListWeaponTerrorismShop[i].name == idWeaponShop)
//				{
//					ListWeaponTerrorismShop[i].SetActive(true);
//				}
//				else
//				{
//					ListWeaponTerrorismShop[i].SetActive(false);
//				}
//			}
//		}
//        switch (StateAnim)
//        {
//            case "MainGun.Weapon":
//                anim.SetBool("Rife", true);
//                anim.SetBool("Pistol", false);
//                anim.SetBool("Knife", false);
//                anim.SetBool("Grenade", false);
//                break;
//            case "Pistol.Weapon":
//                anim.SetBool("Rife", false);
//                anim.SetBool("Pistol", true);
//                anim.SetBool("Knife", false);
//                anim.SetBool("Grenade", false);
//                break;
//            case "Melee.Weapon":
//                anim.SetBool("Rife", false);
//                anim.SetBool("Pistol", false);
//                anim.SetBool("Knife", true);
//                anim.SetBool("Grenade", false);
//                break;
//            case "Grenade.Weapon":
//                anim.SetBool("Rife", false);
//                anim.SetBool("Pistol", false);
//                anim.SetBool("Knife", false);
//                anim.SetBool("Grenade", true);
//                break;
//        }
//	}
//	public void ShowLevel()
//	{
//		int lastLevelExp = CSVR_GameSetting.experienceForEveryLevel[CSVR_GameSetting.accountLevel];
//		int nextLevelExp = CSVR_GameSetting.experienceForEveryLevel[CSVR_GameSetting.accountLevel+1];
//		int actualExp = CSVR_GameSetting.accountExp;
//		
//		nameTxt.text = CSVR_ApplicationManager.PlayFabName.ToString();
//		levelTxt.text = CSVR_GameSetting.accountLevel.ToString();
//		//		expTxt.text = CSVR_GameSetting.accountExp.ToString();
//		
//		float fillAmount = (float)(actualExp - lastLevelExp) / (float)(nextLevelExp - lastLevelExp);
//		
//		//Debug.Log(lastLevelExp + " " + nextLevelExp + " " + actualExp + " " + fillAmount);
//		expBarFill.fillAmount = fillAmount;
//	}
//	
//	public void On_UIGroupClick(int group)
//	{
//		//CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.changeGroupClip);
//		currentGroup = group;
//	}
//	
//	public void HienThi()
//	{
//		for(int i = 0; i < CSVR_GameSetting.inventoryItems.Count; i++)
//		{
//			if(CSVR_GameSetting.inventoryItems[i].CatalogVersion == "2")
//			{
//				string itemClass = CSVR_GameSetting.inventoryItems[i].ItemClass;
//				ItemInstance item = CSVR_GameSetting.inventoryItems[i];
//				int? isUse = CSVR_GameSetting.inventoryItems[i].RemainingUses;
//				if(itemClass == "Pistol.Weapon" && isUse == 1)
//					ImportItemToMainGunInv(item,pistolInvContent,morePistolPanel,itemPanel);
//				else if(itemClass == "MainGun.Weapon" && isUse == 1)
//					ImportItemToMainGunInv(item,mainGunInvContent,moreMainPanel,itemPanel);
//				else if(itemClass == "Melee.Weapon" && isUse == 1)
//					ImportItemToMainGunInv(item,meleInvContent,moreMelePanel,knifePanel);
//				else if(itemClass == "Grenade.Weapon" && isUse == 1)
//					ImportItemToMainGunInv(item,grenadeInvContent,moreGrenadePanel,grenadePanel);
//				else if(itemClass == "Armor.Weapon" && isUse == 1)
//					ImportItemToMainGunInv(item,armorInvContent,moreArmorPanel,armorPanel);
//				else if(itemClass == "Bag.Item")
//					CSVR_GameSetting.bagCount++;
//			}
//		}
//	}
//	
//	public void HienThiWithClass(string classItem,string idItem,string insIdItem)
//	{
//		for(int i = 0; i < CSVR_GameSetting.inventoryItems.Count; i++)
//		{
//			if(CSVR_GameSetting.inventoryItems[i].CatalogVersion == "2")
//			{
//				string _itemClass = CSVR_GameSetting.inventoryItems[i].ItemClass;
//				ItemInstance item = CSVR_GameSetting.inventoryItems[i];
//				if(CSVR_GameSetting.inventoryItems[i].ItemId == idItem && CSVR_GameSetting.inventoryItems[i].ItemInstanceId == insIdItem)
//				{
//					if(_itemClass == classItem && _itemClass == "Pistol.Weapon")
//						ImportItemToMainGunInv(item,pistolInvContent,morePistolPanel,itemPanel);
//					else if(_itemClass == classItem && _itemClass == "MainGun.Weapon")
//						ImportItemToMainGunInv(item,mainGunInvContent,moreMainPanel,itemPanel);
//					else if(_itemClass == classItem && _itemClass == "Melee.Weapon")
//						ImportItemToMainGunInv(item,meleInvContent,moreMelePanel,knifePanel);
//					else if(_itemClass == classItem && _itemClass == "Grenade.Weapon")
//						ImportItemToMainGunInv(item,grenadeInvContent,moreGrenadePanel,grenadePanel);
//					else if(_itemClass == classItem && _itemClass == "Armor.Weapon")
//						ImportItemToMainGunInv(item,armorInvContent,moreArmorPanel,armorPanel);
//				}
//			}
//		}
//	}
//	
//	public void SwithTabGun(int index)
//	{
//		//Debug.Log("goi may lan" +this.gameObject.name);
//		switch (index)
//		{
//		case 0:
//			tabGunHome.Run(tabGunHome.tabContentsPairs[0]);
//			tabGunShop.Run(tabGunShop.tabContentsPairs[0]);
//			break;
//		case 1:
//			tabGunHome.Run(tabGunHome.tabContentsPairs[1]);
//			tabGunShop.Run(tabGunShop.tabContentsPairs[1]);
//			break;
//		case 2:
//			tabGunHome.Run(tabGunHome.tabContentsPairs[2]);
//			tabGunShop.Run(tabGunShop.tabContentsPairs[2]);
//			break;
//		case 3:
//			tabGunHome.Run(tabGunHome.tabContentsPairs[3]);
//			tabGunShop.Run(tabGunShop.tabContentsPairs[3]);
//			break;
//		case 4:
//			tabGunHome.Run(tabGunHome.tabContentsPairs[4]);
//			tabGunShop.Run(tabGunShop.tabContentsPairs[4]);
//			break;
//		default:
//			break;
//		}
//	}
//	public void SwithTabGunV2(int index)
//	{
//		//Debug.Log("goi may lan" +this.gameObject.name);
//		switch (index)
//		{
//		case 0:
//			tabGunShop.Run(tabGunShop.tabContentsPairs[0]);
//			break;
//		case 1:
//			tabGunShop.Run(tabGunShop.tabContentsPairs[1]);
//			break;
//		case 2:
//			tabGunShop.Run(tabGunShop.tabContentsPairs[2]);
//			break;
//		case 3:
//			tabGunShop.Run(tabGunShop.tabContentsPairs[3]);
//			break;
//		case 4:
//			tabGunShop.Run(tabGunShop.tabContentsPairs[4]);
//			break;
//		default:
//			break;
//		}
//	}
//	public void ImportItemToMainGunInv(ItemInstance item,GameObject container,GameObject more,GameObject _obj)
//	{
//		string idTemp,nameTemp;
//		
//		GameObject mainGunClone = Instantiate(_obj);
//		itemClass = item.ItemClass;
//		idTemp = item.ItemId;
//		nameTemp = item.DisplayName;
//		mainGunClone.name = nameTemp;
//		Button sellB =  mainGunClone.transform.GetChild(2).GetChild(8).GetComponent<Button>();
//		Button equipB =  mainGunClone.transform.GetChild(1).GetChild(6).GetComponent<Button>();
//		Button unequipB =  mainGunClone.transform.GetChild(1).GetChild(7).GetComponent<Button>();
//		
//		//Button extend =  mainGunClone.transform.GetChild(1).GetChild(8).GetComponent<Button>();
//		//kiem tra truong custom data ton tai khong
//		if(item.CustomData != null)
//		{
//			if(item.CustomData.ContainsKey("DayManager"))
//			{
//				infoDayTemp = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_DayInfo>(item.CustomData["DayManager"].ToString());
//				if(infoDayTemp.D2.ToString() == "0000"){
//					fillAmount = 1;
//					dayExpired = "Vĩnh Viễn";
//					isExpired = false;
//					//extend.gameObject.SetActive(false);
//				}else{
//					if(!CSVR_GameSetting.IsExpired(infoDayTemp.D2))
//					{
//						isExpired = false;
//						System.TimeSpan maxDay  =  infoDayTemp.D2 - infoDayTemp.D1;
//						System.TimeSpan interval  =  infoDayTemp.D2 - CSVR_GameSetting.LastLogin;
//						//						Debug.Log("dayExpired "+interval.Days);
//						fillAmount = float.Parse(interval.TotalHours.ToString()) / float.Parse(maxDay.TotalHours.ToString());
//						if(interval.Days == 0)
//							dayExpired = interval.Hours+"Hours "+interval.Minutes+"Minute";
//						else
//							dayExpired = interval.Days +" Day";
//					}
//					else
//					{
//						isExpired = true;
//						fillAmount = 0;
//						dayExpired = "Hết hạn";
//					}
//					
//				}
//			}
//			
//			if(item.CustomData.ContainsKey("Level"))
//			{
//				int startLenght = int.Parse(item.CustomData["Level"].ToString());
//				lvGun = startLenght;
//				for(int s = 0; s < 9; s++){
//					if(s < startLenght)
//						mainGunClone.transform.GetChild(0).GetChild(3).GetChild(s).gameObject.SetActive(true);
//					else
//						mainGunClone.transform.GetChild(0).GetChild(3).GetChild(s).gameObject.SetActive(false);
//				}
//			}
//			else
//				lvGun = 0;
//			
//			if(!item.CustomData.ContainsKey("EquippedGroup1"))
//			{
//				group1 = false;
//			}else{
//				if(isExpired){
//					UnEquipWeaponItem(idTemp,itemClass);
//					group1 = false;
//				}else{
//					group1 = bool.Parse(item.CustomData["EquippedGroup1"]);
//				}
//			}
//			
//			if(!item.CustomData.ContainsKey("EquippedGroup2"))
//			{
//				group2 = false;
//			}else{
//				if(isExpired){
//					UnEquipWeaponItem(idTemp,itemClass);
//					group2 = false;
//				}else{
//					group2 = bool.Parse(item.CustomData["EquippedGroup2"]);
//				}
//			}
//			
//			if(!item.CustomData.ContainsKey("EquippedGroup3"))
//			{
//				group3 = false;
//			}else{
//				if(isExpired){
//					UnEquipWeaponItem(idTemp,itemClass);
//					group3 = false;
//				}else{
//					group3 = bool.Parse(item.CustomData["EquippedGroup3"]);
//				}
//			}
//			
//			Text textDay = mainGunClone.transform.GetChild(0).GetChild(1).GetComponent<Text>();
//			textDay.text =  dayExpired;
//			
//			#region Equip and UnEquip
//			SwitchButton it = new SwitchButton();
//			it.idWeapon = idTemp;
//			it.equip = equipB;
//			it.unequip = unequipB;
//			it.isEx = isExpired;
//			
//			it.SetButtonInteractable(true);
//			it.group = 0;
//			if(group1){
//				ImportItemToGroup(1,itemClass,idTemp,nameTemp);
//				it.group =1;
//				it.SetButtonInteractable(false);
//			}
//			if(group2){
//				ImportItemToGroup(2,itemClass,idTemp,nameTemp);
//				it.group =2;
//				it.SetButtonInteractable(false);
//			}
//			if(group3){
//				ImportItemToGroup(3,itemClass,idTemp,nameTemp);
//				it.group =3;
//				
//				it.SetButtonInteractable(false);
//			}
//			
//			if(itemClass == "Pistol.Weapon")
//			{
//				CatalogItem pistolItem;
//				if(lvGun == 0){
//					pistolItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopPistolGun,idTemp);
//				}else{
//					pistolItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,idTemp+"_up"+lvGun);
//				}
//				//CatalogItem pistolItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopPistolGun,idCurrentItem);
//				infoGunTempCurrent = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GunInfo>(pistolItem.CustomData);
//				
//				it.equip.onClick.AddListener(() => {
//					if(it.isEx == true){
//						CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//						CSVR_UIMessageText.instance.SetAlphaText("Extend Item After equip");
//					}
//					if(it.isEx == false){
//						for( int i = 0; i < switchButtonContentsPistol.Count; i++ ) 
//						{
//							if(switchButtonContentsPistol[i].equip == null){
//								//	Debug.Log("Da xoa");
//							}else{
//								switchButtonContentsPistol[i].SetButtonInteractableWithGroup(true,currentGroup);
//								if(switchButtonContentsPistol[i].idWeapon == idTemp)
//								{
//									SwitchButton y = switchButtonContentsPistol[i];
//									y.group = currentGroup;
//									switchButtonContentsPistol[i] = y;
//								}
//							}
//						}
//						it.SetButtonInteractable(false);
//						EquipWeaponItem(idTemp,nameTemp,"Pistol.Weapon");
//					}
//				});
//				
//				it.unequip.onClick.AddListener(() => {
//					it.SetButtonInteractable(true);
//					for( int i = 0; i < switchButtonContentsPistol.Count; i++ ) 
//					{
//						if(switchButtonContentsPistol[i].equip == null){
//							//	Debug.Log("Da xoa");
//						}else{
//							if(switchButtonContentsPistol[i].idWeapon == idTemp){
//								SwitchButton y = switchButtonContentsPistol[i];
//								DisableItemToGroup(y.group,"Pistol.Weapon");
//								y.group = 0;
//								switchButtonContentsPistol[i] = y;
//								break;
//							}
//						}
//					}
//					UnEquipWeaponItem(idTemp,itemClass);
//				});
//				switchButtonContentsPistol.Add(it);
//			}
//			
//			if(itemClass == "MainGun.Weapon"){
//				CatalogItem mainGun;
//				if(lvGun == 0){
//					mainGun = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopMainGun,idTemp);
//				}else{
//					mainGun = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,idTemp+"_up"+lvGun);
//				}
//				infoGunTempCurrent = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GunInfo>(mainGun.CustomData);
//				//	Debug.Log("test2"+ infoGunTemp.G1 + " " +infoGunTemp.G2);
//				it.equip.onClick.AddListener(() => {
//					if(it.isEx == true){
//						CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//						CSVR_UIMessageText.instance.SetAlphaText("Extend Item After equip");
//					}
//					if(it.isEx == false){
//						for( int i = 0; i < switchButtonContentsMainGun.Count; i++ ) 
//						{
//							if(switchButtonContentsMainGun[i].equip == null){
//							}else{
//								switchButtonContentsMainGun[i].SetButtonInteractableWithGroup(true,currentGroup);
//								if(switchButtonContentsMainGun[i].idWeapon == idTemp)
//								{
//									SwitchButton y = switchButtonContentsMainGun[i];
//									y.group = currentGroup;
//									switchButtonContentsMainGun[i] = y;
//								}
//							}
//						}
//						it.SetButtonInteractable(false);
//						EquipWeaponItem(idTemp,nameTemp,"MainGun.Weapon");
//					}
//				});
//				
//				it.unequip.onClick.AddListener(() => {
//					it.SetButtonInteractable(true);
//					for( int i = 0; i < switchButtonContentsMainGun.Count; i++ ) {
//						if(switchButtonContentsMainGun[i].unequip == null){
//						}else{
//							if(switchButtonContentsMainGun[i].idWeapon == idTemp){
//								SwitchButton y = switchButtonContentsMainGun[i];
//								DisableItemToGroup(y.group,"MainGun.Weapon");
//								y.group = 0;
//								switchButtonContentsMainGun[i] = y;
//								break;
//							}
//						}
//					}
//					UnEquipWeaponItem(idTemp,itemClass);
//				});
//				switchButtonContentsMainGun.Add(it);
//			}
//			
//			if(itemClass == "Armor.Weapon")
//			{
//				CatalogItem armorItem;
//				if(lvGun == 0){
//					armorItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopArmorGun,idTemp);
//				}else{
//					armorItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,idTemp+"_up"+lvGun);
//				}
//				//CatalogItem pistolItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopPistolGun,idCurrentItem);
//				infoArmorTempCurrent = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_ArmorInfo>(armorItem.CustomData);
//				//Debug.Log("infoArmorTempCurrent "+infoArmorTempCurrent);
//				it.equip.onClick.AddListener(() => {
//					if(it.isEx == true){
//						CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//						CSVR_UIMessageText.instance.SetAlphaText("Extend Item After equip");
//					}
//					if(it.isEx == false){
//						for( int i = 0; i < switchButtonContentsArmor.Count; i++ ) 
//						{
//							if(switchButtonContentsArmor[i].equip == null){
//								//	Debug.Log("Da xoa");
//							}else{
//								switchButtonContentsArmor[i].SetButtonInteractableWithGroup(true,currentGroup);
//								if(switchButtonContentsArmor[i].idWeapon == idTemp)
//								{
//									SwitchButton y = switchButtonContentsArmor[i];
//									y.group = currentGroup;
//									switchButtonContentsArmor[i] = y;
//								}
//							}
//						}
//						it.SetButtonInteractable(false);
//						EquipWeaponItem(idTemp,nameTemp,"Armor.Weapon");
//					}
//				});
//				
//				it.unequip.onClick.AddListener(() => {
//					it.SetButtonInteractable(true);
//					for( int i = 0; i < switchButtonContentsArmor.Count; i++ ) 
//					{
//						if(switchButtonContentsArmor[i].equip == null){
//							//	Debug.Log("Da xoa");
//						}else{
//							if(switchButtonContentsArmor[i].idWeapon == idTemp){
//								SwitchButton y = switchButtonContentsArmor[i];
//								DisableItemToGroup(y.group,"Armor.Weapon");
//								y.group = 0;
//								switchButtonContentsArmor[i] = y;
//								break;
//							}
//						}
//					}
//					UnEquipWeaponItem(idTemp,itemClass);
//				});
//				switchButtonContentsArmor.Add(it);
//			}
//			
//			if(itemClass == "Melee.Weapon")
//			{
//				CatalogItem meleItem;
//				if(lvGun == 0){
//					meleItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopMeleGun,idTemp);
//				}else{
//					meleItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,idTemp+"_up"+lvGun);
//				}
//				//CatalogItem pistolItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopPistolGun,idCurrentItem);
//				infoMeleeTempCurrent = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_MeleInfo>(meleItem.CustomData);
//				
//				it.equip.onClick.AddListener(() => {
//					if(it.isEx == true){
//						CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//						CSVR_UIMessageText.instance.SetAlphaText("Extend Item After equip");
//					}
//					if(it.isEx == false){
//						for( int i = 0; i < switchButtonContentsMelee.Count; i++ ) 
//						{
//							if(switchButtonContentsMelee[i].equip == null){
//								//	Debug.Log("Da xoa");
//							}else{
//								switchButtonContentsMelee[i].SetButtonInteractableWithGroup(true,currentGroup);
//								if(switchButtonContentsMelee[i].idWeapon == idTemp)
//								{
//									SwitchButton y = switchButtonContentsMelee[i];
//									y.group = currentGroup;
//									switchButtonContentsMelee[i] = y;
//								}
//							}
//						}
//						it.SetButtonInteractable(false);
//						EquipWeaponItem(idTemp,nameTemp,"Melee.Weapon");
//					}
//				});
//				
//				it.unequip.onClick.AddListener(() => {
//					it.SetButtonInteractable(true);
//					for( int i = 0; i < switchButtonContentsMelee.Count; i++ ) 
//					{
//						if(switchButtonContentsMelee[i].equip == null){
//							//	Debug.Log("Da xoa");
//						}else{
//							if(switchButtonContentsMelee[i].idWeapon == idTemp){
//								SwitchButton y = switchButtonContentsMelee[i];
//								DisableItemToGroup(y.group,"Melee.Weapon");
//								y.group = 0;
//								switchButtonContentsMelee[i] = y;
//								break;
//							}
//						}
//					}
//					UnEquipWeaponItem(idTemp,itemClass);
//				});
//				switchButtonContentsMelee.Add(it);
//			}
//			
//			if(itemClass == "Grenade.Weapon")
//			{
//				CatalogItem grenadeItem;
//				if(lvGun == 0){
//					grenadeItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopGrenadeGun,idTemp);
//				}else{
//					grenadeItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.upgradeItems,idTemp+"_up"+lvGun);
//				}
//				//CatalogItem pistolItem = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopPistolGun,idCurrentItem);
//				infoGrenadeTempCurrent = PlayFab.Json.JsonConvert.DeserializeObject<CSVR_GrenadeInfo>(grenadeItem.CustomData);
//				
//				it.equip.onClick.AddListener(() => {
//					if(it.isEx == true){
//						CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//						CSVR_UIMessageText.instance.SetAlphaText("Extend Item After equip");
//					}
//					if(it.isEx == false){
//						for( int i = 0; i < switchButtonContentsGrenade.Count; i++ ) 
//						{
//							if(switchButtonContentsGrenade[i].equip == null){
//								//	Debug.Log("Da xoa");
//							}else{
//								switchButtonContentsGrenade[i].SetButtonInteractableWithGroup(true,currentGroup);
//								if(switchButtonContentsGrenade[i].idWeapon == idTemp)
//								{
//									SwitchButton y = switchButtonContentsGrenade[i];
//									y.group = currentGroup;
//									switchButtonContentsGrenade[i] = y;
//								}
//							}
//						}
//						it.SetButtonInteractable(false);
//						EquipWeaponItem(idTemp,nameTemp,"Grenade.Weapon");
//					}
//				});
//				
//				it.unequip.onClick.AddListener(() => {
//					it.SetButtonInteractable(true);
//					for( int i = 0; i < switchButtonContentsGrenade.Count; i++ ) 
//					{
//						if(switchButtonContentsGrenade[i].equip == null){
//							//	Debug.Log("Da xoa");
//						}else{
//							if(switchButtonContentsGrenade[i].idWeapon == idTemp){
//								SwitchButton y = switchButtonContentsGrenade[i];
//								DisableItemToGroup(y.group,"Grenade.Weapon");
//								y.group = 0;
//								switchButtonContentsGrenade[i] = y;
//								break;
//							}
//						}
//					}
//					UnEquipWeaponItem(idTemp,itemClass);
//				});
//				switchButtonContentsGrenade.Add(it);
//			}
//			
//			#endregion
//			
//			#region setup extend panel
//			
//			//			if(itemClass == "Pistol.Weapon" || itemClass == "MainGun.Weapon" || itemClass == "Melee.Weapon" || itemClass == "Grenade.Weapon" )
//			//			{
//			GameObject _extend = mainGunClone.transform.GetChild(4).gameObject;
//			string itemId3Day = idTemp+"_exA";
//			string itemId7Day = idTemp+"_exB";
//			string itemId15Day = idTemp+"_exC";
//			string itemId30Day = idTemp+"_exD";
//			string itemIdForeverDay = idTemp+"_exE";
//			//3day
//			int itemId3DayPrices = CSVR_GameSetting.extendPricesB[itemId3Day];
//			//Debug.Log("itemId3DayPrices: "+itemId3DayPrices);
//			_extend.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = itemId3DayPrices.ToString();
//			AddListenerExtendItems(_extend.transform.GetChild(0).GetChild(2).GetComponent<Button>(),item,itemId3Day,itemId3DayPrices,"GC",infoDayTemp,textDay,nameTemp,mainGunClone);
//			//_extend.transform.GetChild(0).GetChild(2).GetComponent<Button>().onClick.AddListener(()=> ExtendWeaponItem(itemId3Day,"GC",itemId3DayPrices,infoDayTemp,textDay,mainGunClone));
//			
//			//7 day
//			if(CSVR_GameSetting.extendPricesB.ContainsKey(itemId7Day))
//			{
//				int itemId7DayPrices = CSVR_GameSetting.extendPricesB[itemId7Day];
//				_extend.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = itemId7DayPrices.ToString();
//				_extend.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = iconType[1];
//				AddListenerExtendItems(_extend.transform.GetChild(1).GetChild(2).GetComponent<Button>(),item,itemId7Day,itemId7DayPrices,"GC",infoDayTemp,textDay,nameTemp,mainGunClone);
//				//_extend.transform.GetChild(1).GetChild(2).GetComponent<Button>().onClick.AddListener(()=> ExtendWeaponItem(itemId7Day,"GC",itemId7DayPrices,infoDayTemp,textDay,mainGunClone));
//			}
//			if(CSVR_GameSetting.extendPricesV.ContainsKey(itemId7Day))
//			{
//				int itemId7DayPrices = CSVR_GameSetting.extendPricesV[itemId7Day];
//				_extend.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = itemId7DayPrices.ToString();
//				_extend.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = iconType[0];
//				AddListenerExtendItems(_extend.transform.GetChild(1).GetChild(2).GetComponent<Button>(),item,itemId7Day,itemId7DayPrices,"GD",infoDayTemp,textDay,nameTemp,mainGunClone);
//				//_extend.transform.GetChild(1).GetChild(2).GetComponent<Button>().onClick.AddListener(()=> ExtendWeaponItem(itemId7Day,"GD",itemId7DayPrices,infoDayTemp,textDay,mainGunClone));
//			}
//			//15 day
//			int itemId15DayPrices = CSVR_GameSetting.extendPricesV[itemId15Day];
//			_extend.transform.GetChild(2).GetChild(0).GetComponent<Text>().text =  itemId15DayPrices.ToString();
//			//_extend.transform.GetChild(2).GetChild(2).GetComponent<Button>().onClick.AddListener(()=> ExtendWeaponItem(itemId15Day,"GD",itemId15DayPrices,infoDayTemp,textDay,mainGunClone));
//			AddListenerExtendItems(_extend.transform.GetChild(2).GetChild(2).GetComponent<Button>(),item,itemId7Day,itemId15DayPrices,"GD",infoDayTemp,textDay,nameTemp,mainGunClone);
//			//30 day
//			int itemId30DayPrices = CSVR_GameSetting.extendPricesV[itemId30Day];
//			_extend.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = itemId30DayPrices.ToString();
//			//_extend.transform.GetChild(3).GetChild(2).GetComponent<Button>().onClick.AddListener(()=> ExtendWeaponItem(itemId30Day,"GD",itemId15DayPrices,infoDayTemp,textDay,mainGunClone));
//			AddListenerExtendItems(_extend.transform.GetChild(3).GetChild(2).GetComponent<Button>(),item,itemId7Day,itemId30DayPrices,"GD",infoDayTemp,textDay,nameTemp,mainGunClone);
//			//forever day
//			int itemIdForeverDayPrices = CSVR_GameSetting.extendPricesV[itemIdForeverDay];
//			_extend.transform.GetChild(4).GetChild(0).GetComponent<Text>().text = itemIdForeverDayPrices.ToString();
//			//_extend.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(()=> ExtendWeaponItem(itemIdForeverDay,"GD",itemIdForeverDayPrices,infoDayTemp,textDay,mainGunClone));
//			AddListenerExtendItems(_extend.transform.GetChild(4).GetComponent<Button>(),item,itemId7Day,itemIdForeverDayPrices,"GD",infoDayTemp,textDay,nameTemp,mainGunClone);
//			//			}
//			#endregion
//			
//			#region setup detail and upgrade panel Grenade
//			if(itemClass == "Grenade.Weapon")
//			{
//				//detail
//				GameObject _detailGrenade = mainGunClone.transform.GetChild(2).gameObject;
//				//upgrade
//				float upgradeLevelMaxGrenade = float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_UpgradeLevelMax")); 
//				GameObject _upgradeGrenade = mainGunClone.transform.GetChild(3).gameObject;
//				string itemIdLevelUpgradeGrenade = idTemp+"_up" +(lvGun + 1);
//				
//				//Damages
//				float damageFillAmount = infoGrenadeTempCurrent.Damge/1000;
//				_detailGrenade.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = damageFillAmount;
//				_detailGrenade.transform.GetChild(0).GetChild(2).GetComponent<Text>().text = infoGrenadeTempCurrent.Damge.ToString();
//				
//				_upgradeGrenade.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = damageFillAmount;
//				_upgradeGrenade.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGrenadeTempCurrent.Damge.ToString();
//				
//				//Ranges
//				float rangeFillAmount = infoGrenadeTempCurrent.Ranges/50;
//				_detailGrenade.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = rangeFillAmount;
//				_detailGrenade.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = infoGrenadeTempCurrent.Ranges.ToString();
//				
//				_upgradeGrenade.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = rangeFillAmount;
//				_upgradeGrenade.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGrenadeTempCurrent.Ranges.ToString();
//				
//				//AP
//				float apFillAmount = infoGrenadeTempCurrent.AP/100;
//				_detailGrenade.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = apFillAmount;
//				_detailGrenade.transform.GetChild(2).GetChild(2).GetComponent<Text>().text = infoGrenadeTempCurrent.AP.ToString();
//				
//				_upgradeGrenade.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = apFillAmount;
//				_upgradeGrenade.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGrenadeTempCurrent.AP.ToString();
//				#region setup upgrade
//				
//				if((lvGun + 1) <= upgradeLevelMaxGrenade){
//					CSVR_GrenadeInfo grenadeInfoUpgrade = CSVR_GameSetting.FindGrenadeInfoInUpgradeItems(itemIdLevelUpgradeGrenade);
//					//dame plus
//					if(grenadeInfoUpgrade.Damge != infoGrenadeTempCurrent.Damge){
//						_upgradeGrenade.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = grenadeInfoUpgrade.Damge/1000;
//						_upgradeGrenade.transform.GetChild(0).GetChild(2).GetChild(1).GetComponent<Text>().text = "+"+(grenadeInfoUpgrade.Damge-infoGrenadeTempCurrent.Damge).ToString();
//					}
//					//range
//					if(grenadeInfoUpgrade.Ranges != infoGrenadeTempCurrent.Ranges){
//						_upgradeGrenade.transform.GetChild(3).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = grenadeInfoUpgrade.Ranges/50;
//						_upgradeGrenade.transform.GetChild(3).GetChild(2).GetChild(1).GetComponent<Text>().text ="+" +(grenadeInfoUpgrade.Ranges - infoGrenadeTempCurrent.Ranges).ToString();
//					}
//					//ap
//					if(grenadeInfoUpgrade.AP != infoGrenadeTempCurrent.AP){
//						_upgradeGrenade.transform.GetChild(5).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = grenadeInfoUpgrade.AP/100;
//						_upgradeGrenade.transform.GetChild(5).GetChild(2).GetChild(1).GetComponent<Text>().text = grenadeInfoUpgrade.AP.ToString();
//					}
//					
//					int itemIdUpgradeBPrices = CSVR_GameSetting.upgradePricesB[itemIdLevelUpgradeGrenade];
//					_upgradeGrenade.transform.GetChild(9).GetChild(1).GetComponent<Text>().text = itemIdUpgradeBPrices.ToString();
//					//_upgrade.transform.GetChild(9).GetComponent<Button>().onClick.AddListener(()=> UpgradeWeaponItem(itemIdLevelUpgrade,"GC",itemIdUpgradeBPrices,itemClass,mainGunClone));
//					AddListenerUpgradeItems(_upgradeGrenade.transform.GetChild(9).GetComponent<Button>(),item,itemIdLevelUpgradeGrenade,itemIdUpgradeBPrices,"GC",nameTemp,mainGunClone);
//					
//					int itemIdUpgradeVPrices = CSVR_GameSetting.upgradePricesV[itemIdLevelUpgradeGrenade];
//					_upgradeGrenade.transform.GetChild(8).GetChild(1).GetComponent<Text>().text = itemIdUpgradeVPrices.ToString();
//					AddListenerUpgradeItems(_upgradeGrenade.transform.GetChild(8).GetComponent<Button>(),item,itemIdLevelUpgradeGrenade,itemIdUpgradeVPrices,"GD",nameTemp,mainGunClone);
//					
//					//					}
//					
//				}
//				else
//				{
//					_upgradeGrenade.transform.GetChild(8).gameObject.SetActive(false);
//					_upgradeGrenade.transform.GetChild(9).gameObject.SetActive(false);
//					
//				}
//				#endregion
//			}
//			#endregion
//			
//			#region setup detail and upgrade panel Melee
//			if(itemClass == "Melee.Weapon")
//			{
//				//detail
//				GameObject _detailMelee = mainGunClone.transform.GetChild(2).gameObject;
//				//upgrade
//				float upgradeLevelMaxMelee = float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_UpgradeLevelMax")); 
//				GameObject _upgradeMelee = mainGunClone.transform.GetChild(3).gameObject;
//				string itemIdLevelUpgradeMelee = idTemp+"_up" +(lvGun + 1);
//				
//				//Damages
//				float damageFillAmount = infoMeleeTempCurrent.Damge/500;
//				_detailMelee.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = damageFillAmount;
//				_detailMelee.transform.GetChild(0).GetChild(2).GetComponent<Text>().text = infoMeleeTempCurrent.Damge.ToString();
//				
//				_upgradeMelee.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = damageFillAmount;
//				_upgradeMelee.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().text = infoMeleeTempCurrent.Damge.ToString();
//				
//				//Ranges
//				float rangeFillAmount = infoMeleeTempCurrent.Ranges/10;
//				_detailMelee.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = rangeFillAmount;
//				_detailMelee.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = infoMeleeTempCurrent.Ranges.ToString();
//				
//				_upgradeMelee.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = rangeFillAmount;
//				_upgradeMelee.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = infoMeleeTempCurrent.Ranges.ToString();
//				
//				//Ranges
//				float apFillAmount = infoMeleeTempCurrent.AP/100;
//				_detailMelee.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = apFillAmount;
//				_detailMelee.transform.GetChild(2).GetChild(2).GetComponent<Text>().text = infoMeleeTempCurrent.AP.ToString();
//				
//				_upgradeMelee.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = apFillAmount;
//				_upgradeMelee.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Text>().text = infoMeleeTempCurrent.AP.ToString();
//				
//				
//				#region setup upgrade
//				
//				if((lvGun + 1) <= upgradeLevelMaxMelee){
//					CSVR_MeleInfo meleeInfoUpgrade = CSVR_GameSetting.FindMeleeInfoInUpgradeItems(itemIdLevelUpgradeMelee);
//					//dame plus
//					if(meleeInfoUpgrade.Damge != infoMeleeTempCurrent.Damge){
//						_upgradeMelee.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = meleeInfoUpgrade.Damge/500;
//						_upgradeMelee.transform.GetChild(0).GetChild(2).GetChild(1).GetComponent<Text>().text = "+"+(meleeInfoUpgrade.Damge-infoMeleeTempCurrent.Damge).ToString();
//					}
//					//range
//					if(meleeInfoUpgrade.Ranges != infoMeleeTempCurrent.Ranges){
//						_upgradeMelee.transform.GetChild(3).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = meleeInfoUpgrade.Ranges/10;
//						_upgradeMelee.transform.GetChild(3).GetChild(2).GetChild(1).GetComponent<Text>().text ="+" +(meleeInfoUpgrade.Ranges - infoMeleeTempCurrent.Ranges).ToString();
//					}
//					//ap
//					if(meleeInfoUpgrade.AP != infoMeleeTempCurrent.AP){
//						_upgradeMelee.transform.GetChild(5).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = meleeInfoUpgrade.AP/100;
//						_upgradeMelee.transform.GetChild(5).GetChild(2).GetChild(1).GetComponent<Text>().text = meleeInfoUpgrade.AP.ToString();
//					}
//					
//					int itemIdUpgradeBPrices = CSVR_GameSetting.upgradePricesB[itemIdLevelUpgradeMelee];
//					_upgradeMelee.transform.GetChild(9).GetChild(1).GetComponent<Text>().text = itemIdUpgradeBPrices.ToString();
//					//_upgrade.transform.GetChild(9).GetComponent<Button>().onClick.AddListener(()=> UpgradeWeaponItem(itemIdLevelUpgrade,"GC",itemIdUpgradeBPrices,itemClass,mainGunClone));
//					AddListenerUpgradeItems(_upgradeMelee.transform.GetChild(9).GetComponent<Button>(),item,itemIdLevelUpgradeMelee,itemIdUpgradeBPrices,"GC",nameTemp,mainGunClone);
//					
//					int itemIdUpgradeVPrices = CSVR_GameSetting.upgradePricesV[itemIdLevelUpgradeMelee];
//					_upgradeMelee.transform.GetChild(8).GetChild(1).GetComponent<Text>().text = itemIdUpgradeVPrices.ToString();
//					AddListenerUpgradeItems(_upgradeMelee.transform.GetChild(8).GetComponent<Button>(),item,itemIdLevelUpgradeMelee,itemIdUpgradeVPrices,"GD",nameTemp,mainGunClone);
//					
//					//					}
//					
//				}
//				else
//				{
//					_upgradeMelee.transform.GetChild(8).gameObject.SetActive(false);
//					_upgradeMelee.transform.GetChild(9).gameObject.SetActive(false);
//					
//				}
//				#endregion
//				
//			}
//			#endregion
//			
//			#region setup detail and upgrade panel Armor
//			if(itemClass == "Armor.Weapon")
//			{
//				//detail
//				GameObject _detailArmor = mainGunClone.transform.GetChild(2).gameObject;
//				//upgrade
//				float upgradeLevelMaxArmor = float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_UpgradeLevelMax")); 
//				GameObject _upgradeArmor = mainGunClone.transform.GetChild(3).gameObject;
//				string itemIdLevelUpgradeArmor = idTemp+"_up" +(lvGun + 1);
//				
//				//Reliability
//				float reliabilityFillAmount = infoArmorTempCurrent.Reliability/1000;
//				_detailArmor.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = reliabilityFillAmount;
//				_detailArmor.transform.GetChild(0).GetChild(2).GetComponent<Text>().text = infoArmorTempCurrent.Reliability.ToString();
//				
//				_upgradeArmor.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = reliabilityFillAmount;
//				_upgradeArmor.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().text = infoArmorTempCurrent.Reliability.ToString();
//				
//				//Resilience
//				
//				float resilienceFillAmount = infoArmorTempCurrent.Resilience/100;
//				_detailArmor.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = resilienceFillAmount;
//				_detailArmor.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = infoArmorTempCurrent.Resilience.ToString();
//				
//				_upgradeArmor.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = resilienceFillAmount;
//				_upgradeArmor.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = infoArmorTempCurrent.Resilience.ToString();
//				#region setup upgrade
//				
//				if((lvGun + 1) <= upgradeLevelMaxArmor){
//					CSVR_ArmorInfo armorInfoUpgrade = CSVR_GameSetting.FindArmorInfoInUpgradeItems(itemIdLevelUpgradeArmor);
//					//dame plus
//					if(armorInfoUpgrade.Reliability != infoArmorTempCurrent.Reliability){
//						_upgradeArmor.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = armorInfoUpgrade.Reliability/1000;
//						_upgradeArmor.transform.GetChild(0).GetChild(2).GetChild(1).GetComponent<Text>().text = "+"+(armorInfoUpgrade.Reliability-infoArmorTempCurrent.Reliability).ToString();
//					}
//					//range
//					if(armorInfoUpgrade.Resilience != infoArmorTempCurrent.Resilience){
//						_upgradeArmor.transform.GetChild(3).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = armorInfoUpgrade.Resilience/100;
//						_upgradeArmor.transform.GetChild(3).GetChild(2).GetChild(1).GetComponent<Text>().text ="+" +(armorInfoUpgrade.Resilience - infoArmorTempCurrent.Resilience).ToString();
//					}
//					
//					int itemIdUpgradeBPrices = CSVR_GameSetting.upgradePricesB[itemIdLevelUpgradeArmor];
//					_upgradeArmor.transform.GetChild(9).GetChild(1).GetComponent<Text>().text = itemIdUpgradeBPrices.ToString();
//					//_upgrade.transform.GetChild(9).GetComponent<Button>().onClick.AddListener(()=> UpgradeWeaponItem(itemIdLevelUpgrade,"GC",itemIdUpgradeBPrices,itemClass,mainGunClone));
//					AddListenerUpgradeItems(_upgradeArmor.transform.GetChild(9).GetComponent<Button>(),item,itemIdLevelUpgradeArmor,itemIdUpgradeBPrices,"GC",nameTemp,mainGunClone);
//					
//					int itemIdUpgradeVPrices = CSVR_GameSetting.upgradePricesV[itemIdLevelUpgradeArmor];
//					_upgradeArmor.transform.GetChild(8).GetChild(1).GetComponent<Text>().text = itemIdUpgradeVPrices.ToString();
//					AddListenerUpgradeItems(_upgradeArmor.transform.GetChild(8).GetComponent<Button>(),item,itemIdLevelUpgradeArmor,itemIdUpgradeVPrices,"GD",nameTemp,mainGunClone);
//					
//					//					}
//					
//				}
//				else
//				{
//					_upgradeArmor.transform.GetChild(8).gameObject.SetActive(false);
//					_upgradeArmor.transform.GetChild(9).gameObject.SetActive(false);
//					
//				}
//				#endregion
//			}
//			#endregion
//			
//			#region setup detail and upgrade panel Pistol and MainGun
//			if(itemClass == "Pistol.Weapon" || itemClass == "MainGun.Weapon")
//			{
//				//detail
//				GameObject _detail = mainGunClone.transform.GetChild(2).gameObject;
//				//upgrade
//				float upgradeLevelMax = float.Parse(CSVR_ApplicationManager.GetTitleValue("Key_UpgradeLevelMax")); 
//				GameObject _upgrade = mainGunClone.transform.GetChild(3).gameObject;
//				string itemIdLevelUpgrade = idTemp+"_up" +(lvGun + 1);
//				
//				//dame
//				float dameFillAmount = float.Parse(infoGunTempCurrent.G1.ToString())/500;
//				
//				_detail.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = dameFillAmount;
//				_detail.transform.GetChild(0).GetChild(2).GetComponent<Text>().text = infoGunTempCurrent.G1.ToString();
//				
//				_upgrade.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = dameFillAmount;
//				_upgrade.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGunTempCurrent.G1.ToString();
//				//accuracy
//				
//				float min = infoGunTempCurrent.A1;
//				float max = infoGunTempCurrent.A2;
//				
//				float AC = (1-(min+max)/2)*100;
//				_detail.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = Mathf.Abs(AC/1000);
//				_detail.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = AC.ToString();
//				
//				_upgrade.transform.GetChild(1).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = Mathf.Abs(AC/1000);
//				_upgrade.transform.GetChild(1).GetChild(2).GetChild(0).GetComponent<Text>().text = AC.ToString();
//				
//				
//				//recoil
//				
//				float R1X = Mathf.Abs(infoGunTempCurrent.R1X);
//				float R1Y = Mathf.Abs(infoGunTempCurrent.R1Y);
//				float R2X = Mathf.Abs(infoGunTempCurrent.R2X);
//				float R2Y = Mathf.Abs(infoGunTempCurrent.R2Y);
//				float R3X = Mathf.Abs(infoGunTempCurrent.R3X);
//				float R3Y = Mathf.Abs(infoGunTempCurrent.R3Y);
//				float RC = (int)((3-(R1X+R1Y+R2X+R2Y+R3X+R3Y)/6)*100);
//				_detail.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = RC/300;
//				_detail.transform.GetChild(2).GetChild(2).GetComponent<Text>().text = RC.ToString();
//				
//				_upgrade.transform.GetChild(2).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = RC/300;
//				_upgrade.transform.GetChild(2).GetChild(2).GetChild(0).GetComponent<Text>().text = RC.ToString();
//				
//				
//				//rate of fire
//				float ROFText = (int)(60/float.Parse(infoGunTempCurrent.G2.ToString()));
//				float ROFFillAmount = (ROFText/1000) ;
//				//Debug.Log("ROFText "+ROFText+" ROFFillAmount "+ROFFillAmount);
//				_detail.transform.GetChild(3).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = ROFFillAmount;
//				_detail.transform.GetChild(3).GetChild(2).GetComponent<Text>().text = ROFText+" RPM";
//				
//				_upgrade.transform.GetChild(3).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = ROFFillAmount;
//				_upgrade.transform.GetChild(3).GetChild(2).GetChild(0).GetComponent<Text>().text = ROFText.ToString();
//				
//				//capacity
//				_detail.transform.GetChild(6).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGunTempCurrent.G4.ToString();
//				_upgrade.transform.GetChild(6).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGunTempCurrent.G4.ToString();
//				
//				//reload
//				float reloadFillAmount = float.Parse(infoGunTempCurrent.G3.ToString())/10;
//				string reloadText = infoGunTempCurrent.G3;
//				_detail.transform.GetChild(4).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = reloadFillAmount;
//				_detail.transform.GetChild(4).GetChild(2).GetComponent<Text>().text = reloadText+ " s";
//				
//				_upgrade.transform.GetChild(4).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = reloadFillAmount;
//				_upgrade.transform.GetChild(4).GetChild(2).GetChild(0).GetComponent<Text>().text = reloadText;
//				
//				//ap
//				float apFillAmount = float.Parse(infoGunTempCurrent.G6.ToString())/100;
//				_detail.transform.GetChild(5).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = apFillAmount;
//				_detail.transform.GetChild(5).GetChild(2).GetComponent<Text>().text = infoGunTempCurrent.G6.ToString();
//				
//				_upgrade.transform.GetChild(5).GetChild(1).GetChild(0).GetComponent<Image>().fillAmount = apFillAmount;
//				_upgrade.transform.GetChild(5).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGunTempCurrent.G6.ToString();
//				
//				//weight
//				_detail.transform.GetChild(7).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGunTempCurrent.G5.ToString()+" kg";
//				_upgrade.transform.GetChild(7).GetChild(2).GetChild(0).GetComponent<Text>().text = infoGunTempCurrent.G5.ToString()+" kg";
//				
//				
//				
//				#region setup upgrade
//				
//				if((lvGun + 1) <= upgradeLevelMax){
//					CSVR_GunInfo gunInfoUpgrade = CSVR_GameSetting.FindGunInfoInUpgradeItems(itemIdLevelUpgrade);
//					//dame plus
//					if(gunInfoUpgrade.G1 != infoGunTempCurrent.G1){
//						_upgrade.transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = float.Parse(gunInfoUpgrade.G1.ToString())/500;
//						_upgrade.transform.GetChild(0).GetChild(2).GetChild(1).GetComponent<Text>().text = "+"+(gunInfoUpgrade.G1-infoGunTempCurrent.G1).ToString();
//					}
//					//rate of fire
//					if(gunInfoUpgrade.G2 != infoGunTempCurrent.G2){
//						_upgrade.transform.GetChild(3).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = (int)(60/float.Parse((gunInfoUpgrade.G2).ToString()))/1000;
//						_upgrade.transform.GetChild(3).GetChild(2).GetChild(1).GetComponent<Text>().text ="+" +((int)(60/float.Parse((gunInfoUpgrade.G2).ToString()))-(int)(60/float.Parse(infoGunTempCurrent.G2.ToString())));
//					}
//					//capacity
//					if(gunInfoUpgrade.G4 != infoGunTempCurrent.G4){
//						_upgrade.transform.GetChild(6).GetChild(2).GetChild(1).GetComponent<Text>().text = gunInfoUpgrade.G4.ToString();
//					}
//					//reload
//					if(gunInfoUpgrade.G6 != infoGunTempCurrent.G6){
//						_upgrade.transform.GetChild(4).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = float.Parse(gunInfoUpgrade.G3.ToString())/10;
//						_upgrade.transform.GetChild(4).GetChild(2).GetChild(1).GetComponent<Text>().text = gunInfoUpgrade.G3.ToString();
//					}
//					//ap
//					if(gunInfoUpgrade.G6 != infoGunTempCurrent.G6){
//						_upgrade.transform.GetChild(5).GetChild(1).GetChild(1).GetComponent<Image>().fillAmount = float.Parse(gunInfoUpgrade.G6.ToString())/100;
//						_upgrade.transform.GetChild(5).GetChild(2).GetChild(1).GetComponent<Text>().text = gunInfoUpgrade.G6.ToString();
//					}
//					//weight
//					if(gunInfoUpgrade.G5 != infoGunTempCurrent.G5){
//						_detail.transform.GetChild(7).GetChild(2).GetChild(1).GetComponent<Text>().text = gunInfoUpgrade.G5.ToString();
//					}
//					
//					//					if(itemClass == "Pistol.Weapon" || itemClass == "MainGun.Weapon")
//					//					{
//					int itemIdUpgradeBPrices = CSVR_GameSetting.upgradePricesB[itemIdLevelUpgrade];
//					_upgrade.transform.GetChild(9).GetChild(1).GetComponent<Text>().text = itemIdUpgradeBPrices.ToString();
//					//_upgrade.transform.GetChild(9).GetComponent<Button>().onClick.AddListener(()=> UpgradeWeaponItem(itemIdLevelUpgrade,"GC",itemIdUpgradeBPrices,itemClass,mainGunClone));
//					AddListenerUpgradeItems(_upgrade.transform.GetChild(9).GetComponent<Button>(),item,itemIdLevelUpgrade,itemIdUpgradeBPrices,"GC",nameTemp,mainGunClone);
//					
//					int itemIdUpgradeVPrices = CSVR_GameSetting.upgradePricesV[itemIdLevelUpgrade];
//					_upgrade.transform.GetChild(8).GetChild(1).GetComponent<Text>().text = itemIdUpgradeVPrices.ToString();
//					AddListenerUpgradeItems(_upgrade.transform.GetChild(8).GetComponent<Button>(),item,itemIdLevelUpgrade,itemIdUpgradeVPrices,"GD",nameTemp,mainGunClone);
//					
//					//					}
//					
//				}
//				else
//				{
//					_upgrade.transform.GetChild(8).gameObject.SetActive(false);
//					_upgrade.transform.GetChild(9).gameObject.SetActive(false);
//					
//				}
//				#endregion
//				
//			}
//			
//			
//			#endregion
//			
//			#region setup info
//			mainGunClone.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = nameTemp;
//			mainGunClone.transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = CSVR_AssetManager.itemIconDic[idTemp];
//			mainGunClone.transform.position = Vector3.zero;
//			mainGunClone.transform.SetParent(container.transform,false);
//			mainGunClone.transform.localScale = Vector3.one;
//			Button b = mainGunClone.transform.GetChild(0).GetComponent<Button>();
//			AddListenerItems(b,mainGunClone,idTemp,container);
//			#endregion
//			if(itemClass == "Pistol.Weapon" || itemClass == "MainGun.Weapon")
//			{
//				AddListenerSellItems(sellB,item,infoGunTempCurrent.SellPrices,nameTemp,CSVR_AssetManager.itemIconDic[idTemp],mainGunClone);
//			}
//			if(itemClass == "Melee.Weapon")
//			{
//				AddListenerSellItems(sellB,item,infoMeleeTempCurrent.SellPrices,nameTemp,CSVR_AssetManager.itemIconDic[idTemp],mainGunClone);
//			}
//			if(itemClass == "Grenade.Weapon")
//			{
//				AddListenerSellItems(sellB,item,infoGrenadeTempCurrent.SellPrices,nameTemp,CSVR_AssetManager.itemIconDic[idTemp],mainGunClone);
//			}
//			if(itemClass == "Armor.Weapon")
//			{
//				AddListenerSellItems(sellB,item,infoArmorTempCurrent.SellPrices,nameTemp,CSVR_AssetManager.itemIconDic[idTemp],mainGunClone);
//			}
//			more.transform.SetSiblingIndex(container.transform.childCount);
//			
//			
//			
//		}
//	}
//	
//	public void ChangeDayExtendSuccess(string day)
//	{
//		textCache.text = day;
//	}
//	public void DeleteItemUpdate()
//	{
//		if(destroyThis != null)
//			Destroy(destroyThis);
//	}
//	
//	
//	
//	
//	
//	public void EquipWeaponItem(string id,string name,string itemClass)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		CSVR_ApplicationManager.instance.OnEquipItem(id,itemClass);
//		ImportItemToGroup(currentGroup,itemClass,id,name);
//	}
//	
//	public void UnEquipWeaponItem(string id,string itemClass)
//	{
//		//CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		//CSVR_ApplicationManager.instance.OnUnEquipItem(id);
//		//		DisableItemToGroup(currentGroup,itemClass);
//	}
//	
//	private void AddListenerExtendItems(Button s,ItemInstance item,string id,int extendPrices,string type,CSVR_DayInfo day,Text textDay,string name,GameObject gb)
//	{
//		s.onClick.RemoveAllListeners();
//		s.onClick.AddListener(()=> {
//			if(CSVR_GameSetting.virtualCurrency [type] < extendPrices){
//				//CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.warringClip);
//				CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//				CSVR_UIMessageText.instance.SetAlphaText("Khong du tien");
//			}else{
//				
//				ExtendWeaponItem(item.ItemInstanceId,id,name,type,extendPrices,day,textDay,item.ItemClass,gb);
//				//ExtendWeaponItem(item.ItemInstanceId,lv,name,type,extendPrices,item.ItemClass,gb);
//			}
//		});
//	}
//	private void ExtendWeaponItem(string idIns,string id,string name,string type,int prices,CSVR_DayInfo day,Text d,string classs,GameObject gb)
//	{
//		//Debug.Log("Extend");
//		//		textCache = d;
//		//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		//		CSVR_ApplicationManager.instance.OnExtendGunItemV2(id,type,prices,day);
//		//		destroyThis = gb;
//		textCache = d;
//		//Debug.Log("textCache.text: "+textCache.text);
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		CSVR_UIPopupConfirmShop.instance.ShowPopUpExtend(id,type,prices,name,day,CSVR_AssetManager.itemIconDic[id.Substring(0,id.Length - 4)],iconType[1]);
//		CSVR_ApplicationManager._itemID = id.Substring(0,id.Length - 4);
//		CSVR_ApplicationManager._itemCLASS = classs;
//		CSVR_ApplicationManager._itemInsID = idIns;
//		destroyThis = gb;
//	}
//	
//	private void AddListenerUpgradeItems(Button s,ItemInstance item,string lv,int sellPrices,string type,string name,GameObject gb)
//	{
//		s.onClick.RemoveAllListeners();
//		s.onClick.AddListener(()=> {
//			
//			if(CSVR_GameSetting.virtualCurrency [type] < sellPrices){
//				//CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.warringClip);
//				CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//				CSVR_UIMessageText.instance.SetAlphaText("Khong du tien");
//			}else{
//				//Debug.Log("abc: "+item.ItemClass);
//				CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_UpgradeWeapon);
//				UpgradeWeaponItem(item.ItemInstanceId,lv,name,type,sellPrices,item.ItemClass,gb);
//			}
//		});
//	}
//	private void UpgradeWeaponItem(string idIns,string id,string name,string type,int prices,string classs,GameObject gb)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;
//		CSVR_UIPopupConfirmShop.instance.ShowPopUpUpgrade(id,type,prices,name,CSVR_AssetManager.itemIconDic[id.Substring(0,id.Length - 4)],iconType[1]);
//		CSVR_ApplicationManager._itemID = id.Substring(0,id.Length - 4);
//		CSVR_ApplicationManager._itemCLASS = classs;
//		CSVR_ApplicationManager._itemInsID = idIns;
//		destroyThis = gb;
//	}
//	
//	
//	private void AddListenerSellItems(Button s,ItemInstance item,int sellPrices,string name,Sprite icon,GameObject gb)
//	{
//		s.onClick.RemoveAllListeners();
//		//s.onClick.AddListener(()=> SellWeapon(item,name,icon,gb));
//		s.onClick.AddListener(()=> {
//			if(gb.transform.GetChild(1).GetChild(6).gameObject.activeSelf == false){
//				//CSVR_MusicManager.instance.Source.PlayOneShot(CSVR_MusicManager.instance.warringClip);
//				CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.eff_Error);
//				CSVR_UIMessageText.instance.SetAlphaText("UnEquip Item After Sell Gun");
//				
//			}
//			if(gb.transform.GetChild(1).GetChild(7).gameObject.activeSelf == false)
//				SellWeapon(item,sellPrices,name,icon,gb);
//		});
//	}
//	
//	private void SellWeapon(ItemInstance i,int sellPrices,string name,Sprite icon,GameObject gb)
//	{
//		CSVR_ApplicationManager.currentState = CSVR_ApplicationState.HOME;			
//		string id = i.ItemInstanceId;
////		CSVR_UIPopupConfirmShop.instance.ShowPopUpSell(id,sellPrices.ToString(),name,icon,iconType[1]);
//		destroyThis = gb;
//		
//		
//	}
//	
//	private void AddListenerItems(Button b,GameObject _obj,string id,GameObject container)
//	{
//		b.onClick.RemoveAllListeners();
//		b.onClick.AddListener(()=> ShowButtonGroup(_obj,id,container));
//	}
//    string StateAnim = "";
//    CatalogItem item;
//	private void ShowButtonGroup(GameObject _obj,string id,GameObject container)
//	{
//		CSVR_MusicManager.instance.efxSource.PlayOneShot(CSVR_MusicManager.instance.home_WeaponInfo);
//		//		GameObject _group = _obj.transform.GetChild(1).gameObject;
//		//		GameObject _detail = _obj.transform.GetChild(2).gameObject;
//		//		GameObject _upgrade = _obj.transform.GetChild(3).gameObject;
//		//		GameObject _extend = _obj.transform.GetChild(4).gameObject;
//		//		LayoutElement _layout = _obj.GetComponent<LayoutElement>();
//		//		if(_group.gameObject.activeSelf){
//		//			_layout.minHeight = 225;
//		//			_group.gameObject.SetActive(false);
//		//			_detail.gameObject.SetActive(false);
//		//			_upgrade.gameObject.SetActive(false);
//		//			_extend.gameObject.SetActive(false);
//		//		}else{
//		//			_layout.minHeight = 310;
//		//			_group.gameObject.SetActive(true);
//		//		}
//		
//		for(int i=0;i< container.transform.childCount-1;i++){
//			container.transform.GetChild(i).GetComponent<LayoutElement>().minHeight = 225 ;
//			container.transform.GetChild(i).transform.GetChild(1).gameObject.SetActive(false);
//			container.transform.GetChild(i).transform.GetChild(2).gameObject.SetActive(false);
//			container.transform.GetChild(i).transform.GetChild(3).gameObject.SetActive(false);
//			container.transform.GetChild(i).transform.GetChild(4).gameObject.SetActive(false);
//		}
//		GameObject _group = _obj.transform.GetChild(1).gameObject;
//		LayoutElement _layout = _obj.GetComponent<LayoutElement>();
//		_layout.minHeight = 310;
//		_group.gameObject.SetActive(true);
//        item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopItems, id);
//        switch (item.ItemClass)
//        {
//            case "MainGun.Weapon":
//                StateAnim = "MainGun.Weapon";
//                anim.SetBool("Rife",true);
//                anim.SetBool("Pistol", false);
//                anim.SetBool("Knife", false);
//                anim.SetBool("Grenade", false);
//                break;
//            case "Pistol.Weapon":
//                StateAnim = "Pistol.Weapon";
//                anim.SetBool("Rife", false);
//                anim.SetBool("Pistol", true);
//                anim.SetBool("Knife", false);
//                anim.SetBool("Grenade", false);
//                break;
//            case "Melee.Weapon":
//                StateAnim = "Melee.Weapon";
//                anim.SetBool("Rife", false);
//                anim.SetBool("Pistol", false);
//                anim.SetBool("Knife", true);
//                anim.SetBool("Grenade", false);
//                break;
//            case "Grenade.Weapon":
//                StateAnim = "Grenade.Weapon";
//                anim.SetBool("Rife", false);
//                anim.SetBool("Pistol", false);
//                anim.SetBool("Knife", false);
//                anim.SetBool("Grenade", true);
//                break;
//        }
//		if (IdModel == 1)
//		{
//			for (int i = 0; i < ListWeaponSwatShop.Length; i++)
//			{
//				if (ListWeaponSwatShop[i].name == id)
//				{
//					idWeaponShop = id;
//					ListWeaponSwatShop[i].SetActive(true);
//				}
//				else
//				{
//					ListWeaponSwatShop[i].SetActive(false);
//				}
//				
//			}
//		}
//		else
//		{
//			for (int i = 0; i < ListWeaponTerrorismShop.Length; i++)
//			{
//				if (ListWeaponTerrorismShop[i].name == id)
//				{
//					idWeaponShop = id;
//					ListWeaponTerrorismShop[i].SetActive(true);
//				}
//				else
//				{
//					ListWeaponTerrorismShop[i].SetActive(false);
//				}
//				
//			}
//		}
//	}
//	
//	public void ImportItemToGroup(int groupId,string type,string id,string name)
//	{
//		if (groupId == 1 && type == "MainGun.Weapon") {
//			CSVR_GameSetting.mainGun = id;
//		}
//        StateAnim = type;
//        idWeaponShop = id;
//        SetModel(1);
//		inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).gameObject.SetActive(true);
//		inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = name;
//		inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(1).gameObject.SetActive(true);
//		inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(1).GetComponent<Image>().sprite = CSVR_AssetManager.itemIconDic[id];
//	}
//	
//	public void DisableItemToGroup(int groupId,string type)
//	{
//		//Debug.Log("INVENTORY" + groupId +" type "+type);
//		//inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).gameObject.SetActive(false);
//		switch (type)
//		{
//		case "MainGun.Weapon":
//			inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = "Súng Trường";
//			break;
//		case "Pistol.Weapon":
//			inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = "Súng Lục";
//			break;
//		case "Melee.Weapon":
//			inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = "Dao";
//			break;
//		case "Grenade.Weapon":
//			inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = "Lựu Đạn";
//			break;
//		case "Armor.Weapon":
//			inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = "Giáp";
//			break;
//		default:
//			inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).gameObject.SetActive(false);
//			break;
//		}
//		//inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(0).GetComponent<Text>().text = name;
//		inventoryGroup.transform.FindChild("INVENTORY" + groupId).transform.FindChild(type).transform.GetChild(1).gameObject.SetActive(false);
//	}
//
//	// Update is called once per frame
//	void Update () {
//
//		if(CSVR_GameSetting.virtualCurrency != null)
//		if(CSVR_GameSetting.virtualCurrency.ContainsKey("GC") && CSVR_GameSetting.virtualCurrency.ContainsKey("GD")){
//			UI_CurrencyGCTxt.text = CSVR_GameSetting.virtualCurrency ["GC"].ToString ();
//			UI_CurrencyGDTxt.text = CSVR_GameSetting.virtualCurrency ["GD"].ToString ();
//			UI_PingTxt.text = PhotonNetwork.GetPing() + "ms";
//		}
//	}
//
//}
