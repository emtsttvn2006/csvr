﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using MarchingBytes;
using Horus.ClientModels;
using Horus;

public class CSVR_UIClanList_BoNhiem : SingletonBaseScreen<CSVR_UIClanList_BoNhiem>
{
    public GameObject ContentHolderConfirm;
    public Button ButtonBoNhiem;

    public Text TextTen;
    public Text TextLV;
    public Text TextDiemHoatDong;
    public Text TextVip;

    public Toggle ToggleTruongclan;
    public Toggle ToggleChihuy;
    public Toggle TogglePhoclan;
    public Toggle ToggleNgoaigiao;

    [HideInInspector]
    public UserInfo UserInfo;

    public override void Open()
    {
        base.Open();
        this.TextTen.text = "--";
        this.TextLV.text = "--";
        this.TextDiemHoatDong.text = "--";
        this.TextVip.text = "--";
    }

    public void Show(string UserName)
    {
        GetUserInfoRequest GetUserInfoRequest = new GetUserInfoRequest();
        GetUserInfoRequest.Add(UserName);

        HorusClientAPI.GetUserInfo(GetUserInfoRequest,
            (result) =>
            {
                this.UserInfo = result.users[0];
                this.TextTen.text = this.UserInfo.UserName;
                this.TextLV.text = this.UserInfo.AccountLevel.ToString()                ;
                this.TextDiemHoatDong.text = "--";
                this.TextVip.text = "--";
                ButtonBoNhiem.gameObject.SetActive(true);
            }, (error) =>
            {
                if (DebugError) Debug.Log("Cập nhật thông tin User Lỗi");
                ErrorView.instance.ShowPopupError("Cập nhật thông tin User Lỗi");
                ButtonBoNhiem.gameObject.SetActive(false);
            }, null);
    }

    void ResetToggle()
    {
        if (ToggleChihuy.isOn) {
            ErrorView.instance.ShowPopupError("CHƯA CÓ TÍNH NĂNG NÀY!!");
            ToggleTruongclan.isOn = true;
        } else if (ToggleNgoaigiao.isOn)
        {
            ErrorView.instance.ShowPopupError("CHƯA CÓ TÍNH NĂNG NÀY!!");
            ToggleTruongclan.isOn = true;
        }
    }

    public void Button_BoNhiem_Click()
    {
        ContentHolderConfirm.gameObject.SetActive(true);
    }

    public void Button_BoNhiemClose_Click()
    {
        Close();
    }

    public void Button_BoNhiemConfirmClose_Click()
    {
        ContentHolderConfirm.gameObject.SetActive(false);
    }

    public void Button_BoNhiemConfirm_Click()
    {
        if (this.UserInfo != null)
        {
            if (ToggleTruongclan.isOn)
            {
                ClanChiefUpdateRequest ClanChiefUpdateRequest = new ClanChiefUpdateRequest();
                ClanChiefUpdateRequest.username = this.UserInfo.UserName;

                HorusClientAPI.ClanChiefUpdateProcess(ClanChiefUpdateRequest,
                    (result) =>
                    {
                        ErrorView.instance.ShowPopupError("Bổ Nhiệm THÀNH CÔNG!!");

                        ContentHolderConfirm.gameObject.SetActive(false);
                        CSVR_UICLanList.instance.Close();                        
                        CSVR_UIClanList_XemMyClan.instance.Close();
                        Close();
                    },
                    (error) =>
                    {
                        if (DebugError) Debug.Log("Bổ Nhiệm Trưởng Clan lỗi");
                        ErrorView.instance.ShowPopupError("Bổ Nhiệm Trưởng Clan lỗi");
                    }
                    , null);
            } else if (TogglePhoclan.isOn)
            {
                ClanViceUpdateRequest ClanViceUpdateRequest = new ClanViceUpdateRequest();
                ClanViceUpdateRequest.username = this.UserInfo.UserName;
                ClanViceUpdateRequest.action = "add";

                HorusClientAPI.ClanViceUpdateProcess(ClanViceUpdateRequest,
                    (result) =>
                    {
                        ContentHolderConfirm.gameObject.SetActive(false);
                        CSVR_UICLanList.instance.MyClan = result.Clan;
                        CSVR_UICLanList.instance.UpdateClanList(CSVR_UICLanList.instance.currentClanListBy, CSVR_UICLanList.instance.currentClanOrder, CSVR_UICLanList.instance.currentClanPage);
                        CSVR_UIClanList_XemMyClan.instance.Show(CSVR_UICLanList.instance.MyClan);
                        Close();
                    },
                    (error) =>
                    {
                        if (DebugError) Debug.Log("Bổ Nhiệm Phó Clan lỗi");
                        ErrorView.instance.ShowPopupError("Bổ Nhiệm Phó Clan lỗi");
                    }
                    , null);
            }
        }
    }
}
