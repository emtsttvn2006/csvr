﻿using UnityEngine;
using System.Collections;
using Horus;
using Horus.ClientModels;
using UnityEngine.UI;
using UnityEngine.Events;

public class ChatView_GlobalUserCallBack : MonoBehaviour {
    
    private Color ColorOffline = Color.yellow;
    private Color ColorOnline = Color.white;

    public Text TextName;
    public Text TextLevel;
    public Image ImgLevel;
    public Image ImgAvatar;    
    public Text TextStatus;
    public Image ImgNotification;

    [HideInInspector]
    public GlobalUser GlobalUser { get; set; }    

    void Start()
    {
        ImgNotification.gameObject.SetActive(false);
    }

    void ScrollCellIndex(int idx)
    {        
        this.GlobalUser = ChatCache.instance.GlobalCaches.GlobalUsers[idx];
        TextName.text = GlobalUser.Name;
        TextLevel.text = "Cấp độ " + GlobalUser.Level;
        ImgAvatar.sprite = AccountManager.instance.avatarPicture;
        ImgLevel.sprite = UISpriteManager.instance.GetLevelSprite(int.Parse(GlobalUser.Level));
        TextStatus.text = string.Empty;        
    }

    public void ChatView_GlobalUser_Click()
    {
        AddTemplate_XemThongTin();
        AddTemplate_Chat();
        AddTemplate_Ketban();
    }

    public void AddTemplate_XemThongTin()
    {
        AButton AButton = new AButton();
        AButton.tittle = "THÔNG TIN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIBuddy_ThongTinUserClan.instance.Open();
            TopBar.instance.SetBackButton(new UnityAction(() =>
            {
                CSVR_UIBuddy_ThongTinUserClan.instance.Close();
                CSVR_UIChat.instance.Open();
            }));

            CSVR_UIBuddy_ThongTinUserClan.instance.SetBackButton(new UnityAction(() =>
            {
                CSVR_UIBuddy_ThongTinUserClan.instance.Close();
                CSVR_UIChat.instance.Open();
            }));

            CSVR_UIBuddy_ThongTinUserClan.instance.Show(this.GlobalUser.Name);
            CSVR_UIChat.instance.Close();
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }

    public void AddTemplate_Chat()
    {
        AButton AButton = new AButton();
        AButton.tittle = "NÓI CHUYỆN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIClanList_XemClan.instance.Close();
            CSVR_UICLanList.instance.Close();            

            if (this.GlobalUser.Name != HorusManager.instance.playerUserName)
            {
                if (CSVR_UIChat.instance.GetIndexUserNameInInputText(this.GlobalUser.Name) == -1)
                    CSVR_UIChat.instance.AddUserToSend(this.GlobalUser.Name);
            }

            CSVR_UIChat.instance.InitPrivateChat(this.GlobalUser.Name);
            CSVR_UIListButton.instance.Close();            
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }

    public void AddTemplate_Ketban()
    {
        AButton AButton = new AButton();
        AButton.tittle = "KẾT BẠN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIBuddy.instance.Open();
            CSVR_UIChat.instance.Close();

            CSVR_UIBuddy.instance.InviteAFriend(this.GlobalUser.Name);
            CSVR_UIBuddy.instance.ToggleWaiting.isOn = true;

            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }
}