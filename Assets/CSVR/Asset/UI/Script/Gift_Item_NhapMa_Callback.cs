﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;

public class Gift_Item_NhapMa_Callback : MonoBehaviour {

    public Image ImageItem;

    [HideInInspector]
    public ItemInstance ItemInstance { get; set; }

    void ScrollCellIndex(int idx)
    {
     //   this.ItemInstance = CSVR_UIGift.instance.ListRedeemCouponReward[idx];
        ImageItem.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/" + ItemInstance.ItemIdModel);
        Vector2 v2 = new Vector2(ImageItem.sprite.textureRect.width, ImageItem.sprite.textureRect.height);
        if (v2.x > v2.y)
        {
            ImageItem.GetComponent<RectTransform>().localScale = new Vector3(1, v2.y / v2.x, 1);
        } else
        {
            ImageItem.GetComponent<RectTransform>().localScale = new Vector3(v2.x / v2.y, 1, 1);
        }
    }

    public void Gift_Item_NhapMa_Callback_Click()
    {

    }
}
