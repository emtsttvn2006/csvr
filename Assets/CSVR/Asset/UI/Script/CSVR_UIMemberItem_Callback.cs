﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horus.ClientModels;
using Horus;
using UnityEngine.Events;

public class CSVR_UIMemberItem_Callback : MonoBehaviour
{

    public Image Avata;
    public Text TextName;
    public Image ImgLV;
    public Text TextLV;
    public Text TextStatus;

    public Button ButtonList;

    [HideInInspector] UserInfo UserInfo;
    [HideInInspector] string2 string2;

    void Start()
    {
        this.TextName.text = "--";
        this.TextLV.text = "--";
        this.UserInfo = new UserInfo();
    }

    
    void ScrollCellIndex(int idx)
    {
        this.string2 = CSVR_UIClanList_XemMyClan.instance.listString_Members[idx];

        int index = CSVR_UIClanList_XemMyClan.instance.GetIndex(string2.Value,
                CSVR_UIClanList_XemMyClan.instance.listUser);

        if (index != -1) this.UserInfo = CSVR_UIClanList_XemMyClan.instance.listUser[index];

        if (UserInfo != null)
        {
            if (this.UserInfo.DisplayName != null && this.UserInfo.DisplayName != "")
            {
                this.TextName.text = this.UserInfo.DisplayName;
            } else
            {
                this.TextName.text = "Không xác định!!";
            }
            this.TextLV.text = this.UserInfo.AccountLevel.ToString();
            //this.ImgLV.sprite =
            this.TextStatus.text = string2.Key;

            if (HorusManager.instance.playerUserName == string2.Value)
            {
                ButtonList.gameObject.SetActive(false);
            } else
            {
                ButtonList.gameObject.SetActive(true);
            }
        }       
    }

    public void Button_List_Click()
    {
        switch (string2.Key)
        {
            case "TRƯỞNG CLAN":
                if (HorusManager.instance.playerUserName == CSVR_UIClanList_XemMyClan.instance.ClanResult.chief)
                {
                    AddTemplate_XemThongTin();
                }
                else if (CSVR_UIClanList_XemMyClan.instance.ClanResult.GetViceIndex(HorusManager.instance.playerUserName) != -1)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();
                }
                else if (CSVR_UIClanList_XemMyClan.instance.ClanResult.GetMemberIndex(HorusManager.instance.playerUserName) != -1)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();
                }
                break;
            case "PHÓ CLAN":
                if (HorusManager.instance.playerUserName == CSVR_UIClanList_XemMyClan.instance.ClanResult.chief)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();
                    AddTemplate_BoNhiem();
                }
                else if (CSVR_UIClanList_XemMyClan.instance.ClanResult.GetViceIndex(HorusManager.instance.playerUserName) != -1)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();                    
                }
                else if (CSVR_UIClanList_XemMyClan.instance.ClanResult.GetMemberIndex(HorusManager.instance.playerUserName) != -1)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();                    
                }
                break;
            case "THÀNH VIÊN":
                if (HorusManager.instance.playerUserName == CSVR_UIClanList_XemMyClan.instance.ClanResult.chief)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();
                    AddTemplate_BoNhiem();
                }
                else if (CSVR_UIClanList_XemMyClan.instance.ClanResult.GetViceIndex(HorusManager.instance.playerUserName) != -1)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();
                    AddTemplate_BoNhiem();
                }
                else if (CSVR_UIClanList_XemMyClan.instance.ClanResult.GetMemberIndex(HorusManager.instance.playerUserName) != -1)
                {
                    AddTemplate_XemThongTin();
                    AddTemplate_Chat();                    
                }
                break;
        }
    }

    public void AddTemplate_XemThongTin()
    {
        AButton AButton = new AButton();
        AButton.tittle = "THÔNG TIN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIBuddy_ThongTinUserClan.instance.Open();
            CSVR_UIBuddy_ThongTinUserClan.instance.Show(string2.Value);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }

    public void AddTemplate_Chat()
    {
        AButton AButton = new AButton();
        AButton.tittle = "NÓI CHUYỆN";
        AButton.acion = new UnityAction(() => {
            CSVR_UIMember.instance.Close();
            CSVR_UICLanList.instance.Close();
            CSVR_UIChat.instance.Open();
            CSVR_UIChat.instance.InitPrivateChat(string2.Value);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }

    public void AddTemplate_BoNhiem()
    {
        AButton AButton = new AButton();
        AButton.tittle = "BỔ NHIỆM";
        AButton.acion = new UnityAction(() => { 
            CSVR_UIClanList_BoNhiem.instance.Open();
            CSVR_UIClanList_BoNhiem.instance.Show(string2.Value);
            CSVR_UIListButton.instance.Close();
        });
        CSVR_UIListButton.instance.Open();
        CSVR_UIListButton.instance.SetSpawnPoint(Camera.main.ScreenToViewportPoint(Input.mousePosition));
        CSVR_UIListButton.instance.AddAButton(AButton);
    }
}
