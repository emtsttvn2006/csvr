﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;

public class CSVR_UIEndGamePVE : SingletonBaseScreen<CSVR_UIEndGamePVE> {
	[SerializeField] private Image mission;
	[SerializeField] private Sprite[] missionIcon;

	[SerializeField] private Image avatar_Icon;
	[SerializeField] private Text player_Name;
	[SerializeField] private Text player_Level;
	[SerializeField] private Image level_Icon;

	void Start(){
		avatar_Icon.sprite = AccountManager.instance.avatarPicture;
		player_Name.text = AccountManager.instance.displayName;
		player_Level.text ="Cấp độ: "+ CSVR_GameSetting.accountLevel.ToString();
		level_Icon.sprite = UISpriteManager.instance.GetLevelSprite(CSVR_GameSetting.accountLevel);
		if (PhotonNetwork.player.customProperties ["EndGame"].ToString () == "Thắng") {
			mission.sprite = missionIcon [1];
		} else {
			mission.sprite = missionIcon [0];
		}
	}
	public override void OnEnable()
	{
		TopBar.instance.SetBackButton(new UnityAction(() =>{	
			CSVR_UISanLungOne.instance.Open();
			CharacterView.instance.Close ();
			UI_OnDestroyThis_Click();
		}));   

		TopBar.instance.SetShopOrInVButton(new UnityAction(() =>{			
			CSVR_UIShop.instance.Open();
			AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
			UI_OnDestroyThis_Click();
		}));

		TopBar.instance.ShopOrInVText.text = "Cửa hàng";
	}
	public void UI_OnDestroyThis_Click(){

		Destroy (this.gameObject);

		if(PhotonNetwork.isMasterClient){
			PhotonNetwork.room.open = false;
			PhotonNetwork.DestroyAll();
		}

		PhotonNetwork.LeaveRoom ();
		HorusManager.instance.gameModeInstance.transform.FindChild (CSVR_GameSetting.modeName).gameObject.SetActive(false);
		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.InvOpen_Clip);
	}
}
