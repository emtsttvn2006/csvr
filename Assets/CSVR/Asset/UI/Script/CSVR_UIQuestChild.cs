﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;


public class CSVR_UIQuestChild : MonoBehaviour {

	[SerializeField] private Image Mission_Icon;
	[SerializeField] private Text Mission_Name;
	[SerializeField] private Text Mission_Content;
	[SerializeField] private Text Mission_Process;
	[SerializeField] private Button Button_Reward;
	[SerializeField] private Transform Content_Parent;
	[SerializeField] private CSVR_Gift_Item_Moi_Ngay_Callback Gift_Item_Moi_Ngay;

	private QuestInfo questInfo { get; set; }
	string key;
	void ScrollCellIndex(int idx)
	{
		if(transform.parent.name == "DailyContent")
			this.questInfo = CSVR_UIQuest.instance.questNhiemVuHangNgay[idx];
		else if(transform.parent.name == "MainContent")
			this.questInfo = CSVR_UIQuest.instance.questNhiemVuChinhTuyen[idx];

		key = CSVR_UIQuest.instance.dailyGameKey;
		HUD_ShowInfo_Run ();

	}

	private void HUD_ShowInfo_Run(){
		if(questInfo.questId.Contains("daily_kill"))
			Mission_Icon.sprite = UISpriteManager.instance.GetMissionSprite ("daily_kill");
		
		Mission_Content.text = questInfo.description;
		Mission_Name.text = questInfo.displayName;

		foreach (KeyValuePair<string, int> pParent in CSVR_UIQuest.instance.dailyGame[key]) {
			foreach (KeyValuePair<string, int> pChild in questInfo.required.dailyGame) {
				if (pParent.Key.Contains (pChild.Key)) {
					if (pParent.Value >= pChild.Value) {
						Mission_Process.text = "(" + pChild.Value + "/" + pChild.Value + ")";
						Button_Reward.interactable = true;
					} else {
						Mission_Process.text = "(" + pParent.Value + "/" + pChild.Value + ")";
					}

					break;
				} 
				if(string.IsNullOrEmpty(Mission_Process.text))
					Mission_Process.text = "(" + 0 + "/" + pChild.Value + ")";
			}
		}


		foreach (KeyValuePair<string, ItemSlot> pChild in questInfo.reward.items) {
			Gift_Item_Moi_Ngay.ImageItem.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
			string idModel = pChild.Value.ItemId;
			string soluong = (pChild.Value.number.min == pChild.Value.number.max) ? pChild.Value.number.min.ToString() : (pChild.Value.number.min + "-" + pChild.Value.number.max);

			if (pChild.Value.ItemId == GameConstants.VNITEMID_Coin) {
				Gift_Item_Moi_Ngay.TextItem.text = soluong + " (vàng)";
			}else if (pChild.Value.ItemId == GameConstants.VNITEMID_Gold) {
				Gift_Item_Moi_Ngay.TextItem.text = soluong+ " (kim cương)";
			} else {
				Gift_Item_Moi_Ngay.ImageItem.transform.localScale = new Vector3(0.4f,0.4f,0.4f);

				if (pChild.Value.ItemId.Contains (GameConstants.VNITEMPREFIX_3Day)) {
					idModel = idModel.Replace (GameConstants.VNITEMPREFIX_3Day, GameConstants.VNITEMPREFIX_Empty);
					Gift_Item_Moi_Ngay.TextItem.text =  soluong+ " (03Ngày)";
				} 
				else if (pChild.Value.ItemId.Contains (GameConstants.VNITEMPREFIX_7Day)) {
					idModel = idModel.Replace (GameConstants.VNITEMPREFIX_3Day, GameConstants.VNITEMPREFIX_Empty);
					Gift_Item_Moi_Ngay.TextItem.text = soluong+ " (7Ngày)";
				}
				else if (pChild.Value.ItemId.Contains (GameConstants.VNITEMPREFIX_30Day)) {
					idModel = idModel.Replace (GameConstants.VNITEMPREFIX_30Day, GameConstants.VNITEMPREFIX_Empty);
					Gift_Item_Moi_Ngay.TextItem.text =  soluong+ " (30Ngày)";
				} 
				else {
					Gift_Item_Moi_Ngay.TextItem.text =  soluong+ " (Vĩnh Viễn)";
				}
			}
	
			Gift_Item_Moi_Ngay.ImageItem.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+idModel);
			Gift_Item_Moi_Ngay.ImageItem.SetNativeSize ();
			Gift_Item_Moi_Ngay.gameObject.SetActive (true);

			break;
		}

		
		if (questInfo.reward == null || questInfo.reward.items == null)
			return;
		
	}
	public void HUD_RewardItem_Click(){
		HorusClientAPI.GetQuestProcess(new GetQuestProcessRequest(){
			questId = questInfo.questId
		}, 
			(result) => {
				if(result.quest.questProcess.Contains(GameConstants.QUEST_FINISH)){
					//if(result.quest.itemsOwn.Count > 1){
						CSVR_UIRewardItem.instance.Open();
						CSVR_UIRewardItem.instance.ResetListItemReward();
					//}
					for (int i = 0; i < result.quest.itemsOwn.Count; i++) {

						if(result.quest.itemsOwn [i].CatalogVersion != "BundleGiftCode" && (int)result.quest.itemsOwn [i].RemainingUses != 0){
							CSVR_UIRewardItem.instance.RewardItem(i,result.quest.itemsOwn [i]);
						}
					}
					CSVR_UIRewardItem.instance.UI_OpenCSVR_UIRewardItem_Click();
					TopBar.instance.UpdateVirtualCurrency();
				}else if(result.quest.questProcess.Contains(GameConstants.QUEST_DONE)){
					ErrorView.instance.ShowPopupError("Bạn đã nhận thưởng.");
				}else if(result.quest.questProcess.Contains(GameConstants.QUEST_DOING)){
					ErrorView.instance.ShowPopupError("Đang trong quá trình làm.");
				}else {
					//Debug.Log(result.quest.startDay);
					//Debug.Log(int.Parse(key));
					if(result.quest.startDay - int.Parse(key) >= 86400){
						ErrorView.instance.ShowPopupError("Phần thưởng đã nhận, mai hãy quay lại nhé.");
					}else
						ErrorView.instance.ShowPopupError("Chưa thế nhận thưởng.");
				}

			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Lỗi nhận thưởng");
			}
		);
	}
}
