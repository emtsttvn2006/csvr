using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.Json;
using Horus.ClientModels;
using CnControls;
using UnityEngine.Events;

public class CSVR_UIPopupConfirmShop : SingletonBaseScreen<CSVR_UIPopupConfirmShop> {
    
	public GameObject cPanel;
	//public Image pIcon;
	public GameObject cubeOne;
	public GameObject cubeTwo;
	public Text pTitle;
	public Text pContent;

	public Button cOk;
	public Button cSell;
	public Button cNo;

	private string itemID;

    public override void Open()
    {
        base.Open();
        GotoBeforeLayer("CSVR_UITutorial");
    }

    public override void OnEnable(){
        base.OnEnable();
		cubeOne.transform.rotation = Quaternion.Euler (Vector3.zero);
		cubeTwo.transform.rotation = Quaternion.Euler (Vector3.zero);
		cubeOne.transform.localScale = new Vector3 (10, 10, 10);
	}

	public void ShowPopUpGrandItem(ItemInstance item)
	{
		this.gameObject.SetActive (true);
		cPanel.gameObject.SetActive (true);
		cOk.gameObject.SetActive (true);
		cSell.gameObject.SetActive (false);
		cNo.gameObject.SetActive (false);
		pTitle.gameObject.SetActive (true);
		pContent.gameObject.SetActive (true);
		Debug.Log (item.ItemIdModel);
		//pIcon.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		cubeTwo.transform.FindChild (item.ItemIdModel).gameObject.SetActive (true);
		itemID = item.ItemIdModel;

		pTitle.text = "Nhận quà thành công";

		if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_3Day)) {
			pContent.text = item.DisplayName + "(03Ngày)";
		} 
		else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_7Day)) {
			pContent.text = item.DisplayName + "(7Ngày)";
		}
		else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_30Day)) {
			pContent.text = item.DisplayName + "(30Ngày)";
		} 
		else {
			pContent.text = item.DisplayName + "(Vĩnh Viễn)";
		}
		
	}
	public void ShowPopUpPurchase(CatalogItem item)
	{
        Open();
		this.gameObject.SetActive (true);
		cPanel.gameObject.SetActive (true);
		cOk.gameObject.SetActive (true);
		cSell.gameObject.SetActive (false);
		cNo.gameObject.SetActive (false);
		pTitle.gameObject.SetActive (true);
		pContent.gameObject.SetActive (true);

		string idModel = item.ItemId;

		if (idModel.Contains (GameConstants.VNITEMPREFIX_3Day)) {
			idModel = idModel.Replace ("_3day", "");
			pContent.text = item.DisplayName + "(03Ngày)";
		} 
		else if (idModel.Contains (GameConstants.VNITEMPREFIX_7Day)) {
			idModel = idModel.Replace ("_7day", "");
			pContent.text = item.DisplayName + "(7Ngày)";
		}
		else if (idModel.Contains (GameConstants.VNITEMPREFIX_30Day)) {
			idModel = idModel.Replace ("_30day", "");
			pContent.text = item.DisplayName + "(30Ngày)";
		} 
		else {
			pContent.text = item.DisplayName + "(Vĩnh Viễn)";
		}

		try{
		//pIcon.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		cubeTwo.transform.FindChild (idModel).gameObject.SetActive (true);
		}catch{
		}
		itemID = idModel;

		pTitle.text = "Mua thành công";

		AudioManager.instance.audio.PlayOneShot (AudioManager.instance.buySSEff_Clip);

		OnPurchaseWeapon (item);

	}
	public void ShowPopUpSell(ItemInstance item,CatalogItem info)
	{
		this.gameObject.SetActive (true);
		cPanel.gameObject.SetActive (true);
		cOk.gameObject.SetActive (false);
		cSell.gameObject.SetActive (true);
		cNo.gameObject.SetActive (true);
		pTitle.gameObject.SetActive (true);
		pContent.gameObject.SetActive (true);

		//pIcon.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
		try{
			cubeTwo.transform.FindChild (item.ItemIdModel).gameObject.SetActive (true);
		}catch{
		}
		//cubeTwo.transform.FindChild (item.ItemIdModel).gameObject.SetActive (true);
		itemID = item.ItemIdModel;

		cSell.onClick.RemoveAllListeners ();
		cSell.onClick.AddListener(() => {

			cSell.gameObject.SetActive (false);
			cNo.gameObject.SetActive (false);
			pTitle.gameObject.SetActive (false);
			pContent.gameObject.SetActive (false);
			OnSellWeapon(item);

		});
		pTitle.text = "Bạn có đồng ý bán ";
//		try{
//			pTitle.text = "Bạn có đồng ý bán với giá "+ (int)(int.Parse(info.VirtualCurrencyPrices ["coin"].ToString())*0.3);
//		}catch{
//			pTitle.text = "Bạn có đồng ý bán với giá "+ (int)(int.Parse(info.VirtualCurrencyPrices ["gold"].ToString())*0.3);
//		}
			
//		if (item.ItemId.Contains ("_3day")) {
//			pContent.text = item.DisplayName + "(3 Ngày)";
//		} else if (item.ItemId.Contains ("_7day")) {
//			pContent.text = item.DisplayName + "(7 Ngày)";
//		} else if (item.ItemId.Contains ("_30day")) {
//			pContent.text = item.DisplayName + "(30 Ngày)";
//		} else {
//			pContent.text = item.DisplayName + "(Vĩnh Viễn)";
//		}
		if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_3Day)) {
			pContent.text = item.DisplayName + "(03Ngày)";
		} 
		else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_7Day)) {
			pContent.text = item.DisplayName + "(7Ngày)";
		}
		else if (item.ItemId.Contains (GameConstants.VNITEMPREFIX_30Day)) {
			pContent.text = item.DisplayName + "(30Ngày)";
		} 
		else {
			pContent.text = item.DisplayName + "(Vĩnh Viễn)";
		}
	}
		

	void OnPurchaseWeapon(CatalogItem item) {
	
		string moneyUse = "coin";
		if (item.VirtualCurrencyPrices.ContainsKey ("coin")) {
			moneyUse = "coin";
			AccountManager.instance.gameCoinAmount -= int.Parse(item.VirtualCurrencyPrices["coin"].ToString());
		
		} else {
			moneyUse = "gold";
			AccountManager.instance.gameDollarAmount -= int.Parse(item.VirtualCurrencyPrices["gold"].ToString());
		}
		Debug.Log ("<b>-1. ItemId </b>" + item.ItemId);
		PurchaseItemRequest request = new PurchaseItemRequest () {
			Purchases = new List<ItemPurchase> () {
				new ItemPurchase () {
					ItemId = item.ItemId,
					//CatalogVersion = item.CatalogVersion,
					moneyUse = moneyUse,
					RemainingUses = 1,
					UseOnBuy = 0
				}
			}
		};
				
		HorusClientAPI.PurchaseItem(request, 
			(result) => {
				Debug.Log ("<b>0. ItemPurchase.Count </b>" + result.ItemPurchase.Count);
				for (int i = 0; i < result.ItemPurchase.Count; i++) {
					if(result.ItemPurchase[i].RemainingUses > 0){
						AccountManager.instance.itemsNew.Add (result.ItemPurchase [i]);
						AccountManager.instance.inventory.Add (result.ItemPurchase [i]);

						CSVR_UIInventory.instance.InstanceItemPurchase (result.ItemPurchase [i]);
						CSVR_UIInventory.instance.ShowNotificationPurchase(result.ItemPurchase[i],true);
					}
				}
				TopBar.instance.UpdateVirtualCurrency();
			},
			(error) => {
				ErrorView.instance.ShowPopupError ("Mua trang bị thất bại");
			}
		);
	}
		
	void OnSellWeapon(ItemInstance _idItem){
		SellItemRequest request = new SellItemRequest () {
			_id = _idItem._id
		};

		HorusClientAPI.SellItem(request, 
			(result) => {
				switch (_idItem.ItemClass) {
				case GameConstants.VNITEMCLASS_DefaultCharacter:	
					{
						CSVR_UIInventory.instance.DestroyItemCraftOrSell (_idItem, 3,false);
						break;
					}
				case GameConstants.VNITEMCLASS_MuzzleAccessories:
				case GameConstants.VNITEMCLASS_GripAccessories:
				case GameConstants.VNITEMCLASS_MagazineAccessories:
				case GameConstants.VNITEMCLASS_ScopeAccessories:
				case GameConstants.VNITEMCLASS_SkinAccessories:
				case GameConstants.VNITEMCLASS_AccAccessories:
					{
						CSVR_UIInventory.instance.DestroyItemCraftOrSell (_idItem, 2,false);
						break;
					}
				case GameConstants.VNITEMCLASS_RifleWeapon:
				case GameConstants.VNITEMCLASS_SniperWeapon:
				case GameConstants.VNITEMCLASS_ShotGunWeapon:
				case GameConstants.VNITEMCLASS_SMGWeapon:
				case GameConstants.VNITEMCLASS_MachineWeapon:
				case GameConstants.VNITEMCLASS_PistolWeapon:
				case GameConstants.VNITEMCLASS_MeleeWeapon:
				case GameConstants.VNITEMCLASS_GrenadeWeapon:
				case GameConstants.VNITEMCLASS_OneGrenadeWeapon:
				case GameConstants.VNITEMCLASS_HelmetWeapon:	
				case GameConstants.VNITEMCLASS_ArmorWeapon:	
					{
						CSVR_UIInventory.instance.DestroyItemCraftOrSell (_idItem, 1,false);
						break;
					}
				}

				Debug.Log ("<b>1. inventory.Count </b>" + AccountManager.instance.inventory.Count);
				AccountManager.instance.inventory.Remove (_idItem);
				Debug.Log ("<b>2. inventory.Count </b>" + AccountManager.instance.inventory.Count);
				AccountManager.instance.gameCoinAmount = result.Inventory.coin;
				AccountManager.instance.gameDollarAmount =  result.Inventory.gold;

				TopBar.instance.UpdateVirtualCurrency();

				SellSuccess ();
			},
			(error) => {
				SellSuccess();
				ErrorView.instance.ShowPopupError ("Bán trang bị thất bại");
			}
		);
	}


	public void SellSuccess()
	{
		cPanel.gameObject.SetActive (false);
	}
	public void UI_DisableItemId(){
		try{
			//pIcon.sprite = Resources.Load<Sprite>("Sprites/UIWeapon/"+item.ItemId);
			cubeTwo.transform.FindChild (itemID).gameObject.SetActive (false);
		}catch{
		}
		//cubeTwo.transform.FindChild (itemID).gameObject.SetActive (false);
	}

	private void Update()
	{

		if(CnInputManager.GetAxis("Fire Vertical") > 10 ||CnInputManager.GetAxis("Fire Vertical") < -10) 
		{
			cubeOne.transform.Rotate(new Vector3(-CnInputManager.GetAxis("Fire Vertical"),0, 0) * Time.deltaTime * 5 );
		}
		if(CnInputManager.GetAxis("Fire Horizontal") > 10 ||CnInputManager.GetAxis("Fire Horizontal") < -10) {
			cubeTwo.transform.Rotate(new Vector3(0,-CnInputManager.GetAxis("Fire Horizontal"), 0) * Time.deltaTime * 5 );
		}
		else if(CnInputManager.GetAxis("Fire Horizontal") == 0) {
			cubeTwo.transform.Rotate(new Vector3(0,1, 0) * Time.deltaTime * 10 );
		}
		if (Input.touchCount == 2 )
		{
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			float deltaMagnitudeDiff = -0.2f*(prevTouchDeltaMag - touchDeltaMag);

			cubeOne.transform.localScale += new Vector3(deltaMagnitudeDiff,deltaMagnitudeDiff, deltaMagnitudeDiff); ;

			cubeOne.transform.localScale = new Vector3(Mathf.Clamp(cubeOne.transform.localScale.x, 5, 15), Mathf.Clamp(cubeOne.transform.localScale.y, 5, 15), Mathf.Clamp(cubeOne.transform.localScale.z, 5, 15));
		}
	}
}
