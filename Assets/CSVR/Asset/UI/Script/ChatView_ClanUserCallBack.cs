﻿using UnityEngine;
using System.Collections;
using Horus;
using Horus.ClientModels;
using UnityEngine.UI;

public class ChatView_ClanUserCallBack : MonoBehaviour {
    
    private Color ColorOffline = Color.yellow;
    private Color ColorOnline = Color.white;

    public Text TextName;
    public Text TextLevel;
    public Image ImgLevel;
    public Image ImgAvatar;    
    public Text TextStatus;
    public Image ImgNotification;

    [HideInInspector]
    public GlobalUser GlobalUser { get; set; }    

    void Start()
    {
        ImgNotification.gameObject.SetActive(false);
    }

    void ScrollCellIndex(int idx)
    {        
        this.GlobalUser = ChatCache.instance.ClanCaches.GlobalUsers[idx];
        TextName.text = GlobalUser.Name;
        TextLevel.text = "Cấp độ " + GlobalUser.Level;
        ImgAvatar.sprite = AccountManager.instance.avatarPicture;
        ImgLevel.sprite = UISpriteManager.instance.GetLevelSprite(int.Parse(GlobalUser.Level));
        TextStatus.text = string.Empty;        
    }

    public void ChatView_ClanUser_Click()
    {
        /*
        if (this.ChatUser.Name != CSVR_IRCPlayerMessenger.instance.ChatUser.Name)
        {
            if (CSVR_UIChat.instance.GetIndexUserNameInInputText(this.ChatUser.Name) == -1)
                CSVR_UIChat.instance.AddUserToSend(this);
        }*/
    }
}