using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Horus;
using Horus.ClientModels;


public class CSVR_UIFriend : MonoBehaviour {
	public static CSVR_UIFriend instance;
	
	public GameObject FriendUIPrefab = null;
	public Transform PanelList = null;
	public Text FriendsCountText = null;
	public InputField PlayerNameInput;
	
	[HideInInspector]
	public List<string> Friends = new List<string>();
	public List<bl_FriendInfo> cacheFriendsInfo = new List<bl_FriendInfo>();
	private int OnlineCount = 0;
	bool WaitForEvent = false;

//	public GameObject loading;

	void Awake()
	{
		instance = this;
	}
	void OnDestroy(){
		if (instance != null)
			instance = null;
	}
	void Start(){
		
		HorusClientAPI.GetRelationShip(new GetRelationShipRequest(), 
			(result) => {
				CSVR_GameSetting.relationship = result.RelationshipList;
				Debug.Log(CSVR_GameSetting.relationship.Count);
				for(int i=0; i<CSVR_GameSetting.relationship.Count; i++)
				{
					if (CSVR_GameSetting.relationship [i].CreateId == HorusManager.instance.playerID) {
						Friends.Add (CSVR_GameSetting.relationship [i].PartnerId);
						CSVR_GameSetting.friendsListName.Add(CSVR_GameSetting.relationship[i].PartnerId);
					} else if (CSVR_GameSetting.relationship [i].PartnerId == HorusManager.instance.playerID) {
						Friends.Add (CSVR_GameSetting.relationship [i].CreateId);
						CSVR_GameSetting.friendsListName.Add(CSVR_GameSetting.relationship[i].CreateId);
					}
				}
				Debug.Log(CSVR_GameSetting.friendsListName.Count);
				if (CSVR_GameSetting.friendsListName.Count > 0) {
					PhotonNetwork.FindFriends (CSVR_GameSetting.friendsListName.ToArray()); 
				}  
  
			},
			(error) => {

				ErrorView.instance.ShowPopupError ("Xảy ra lỗi trong quá trình lấy giữ liệu, vui lòng đăng nhập lại");
			}
		);



		OnJoinedLobbyV2 ();
	}
	void OnEnable(){
		UpdateList ();
	}
	public void UI_OnFriendOn() {
		this.transform.gameObject.SetActive(true);
	}
	
	//Setting Off
	public void UI_OnFriendOff() {
		this.transform.gameObject.SetActive(false);
	}
	
	public void GetFriendsStore()
	{
		//CSVR_UIChatManager.instance.friendList.Clear();
//		Friends.Clear();
//		for(int i=0; i<CSVR_GameSetting.relationship.Count; i++)
//		{
//			if (CSVR_GameSetting.relationship [i].CreateId == HorusManager.instance.playerID) {
//				Friends.Add (CSVR_GameSetting.relationship [i].PartnerId);
//				CSVR_GameSetting.friendsListName.Add(CSVR_GameSetting.relationship[i].PartnerId);
//			} else if (CSVR_GameSetting.relationship [i].PartnerId == HorusManager.instance.playerID) {
//				Friends.Add (CSVR_GameSetting.relationship [i].CreateId);
//				CSVR_GameSetting.friendsListName.Add(CSVR_GameSetting.relationship[i].CreateId);
//			}
//		}
//
		//Find all friends names in photon list.
		if (Friends.Count > 0)
		{
			//Debug.Log("Friends "+ Friends.Count );
			//CSVR_UIChatManager.instance.StartChat();
			//PhotonNetwork.FindFriends(Friends.ToArray());
			//Update the list UI 
			//Debug.Log("GetFriendsStore()");
			UpdateFriendList(true);
		}
		else
		{
			
			//CSVR_ApplicationManager.instance.ChangeHomeState();
			return;
		}
	}
	
	public void UpdateList()
	{
		
		if (!PhotonNetwork.connected || PhotonNetwork.Friends == null || !PhotonNetwork.insideLobby){
			CancelInvoke("UpdateList");
			return;
		}
		
		if (Friends.Count > 0)
		{
			if (Friends.Count > 1 && Friends.Contains("Null"))
			{
				Friends.Remove("Null");
				Friends.Remove("Null");
			}
			//PhotonNetwork.FindFriends(Friends.ToArray());
		}
		
	}
	
	public void OnUpdatedFriendList(){
		//		Debug.Log("co chay khong");
		if (!PhotonNetwork.connected || PhotonNetwork.Friends == null || !PhotonNetwork.insideLobby)
		{
			if (PhotonNetwork.Friends.Count <= 0) { CleanCacheList(); }
			return;
		}
		if (cacheFriendsInfo.Count <= 0 && PhotonNetwork.Friends != null || WaitForEvent)
		{
			
			UpdateFriendList(true);
			WaitForEvent = false;
		}
		else
		{
			UpdateFriendList();
		}
	}
	
	public void UpdateFriendList(bool instance = false)
	{
		if (PhotonNetwork.Friends == null || !PhotonNetwork.insideLobby)
		{
			return;
		}
		
		FriendInfo[] friends = PhotonNetwork.Friends.ToArray();
		
		if (instance)
		{
			CleanCacheList();
			if (friends.Length > 0)
			{
				for (int i = 0; i < friends.Length; i++)
				{
					if (friends[i].Name != "Null")
					{
						GameObject f = Instantiate(FriendUIPrefab) as GameObject;
						bl_FriendInfo info = f.GetComponent<bl_FriendInfo>();
						info.GetInfo(friends[i]);
						cacheFriendsInfo.Add(info);
						
						f.transform.SetParent(PanelList, false);
						
					}
				}
			}
		}
		else//Just update list
		{
			for (int i = 0; i < cacheFriendsInfo.Count; i++)
			{
				if (cacheFriendsInfo[i] != null && friends[i].Name != "Null")
				{
					cacheFriendsInfo[i].GetInfo(friends[i]);
				}
			}
		}
		UpdateCount(friends);
	}
	
	void UpdateCount(FriendInfo[] friends)
	{
		OnlineCount = 0;
		foreach (FriendInfo info in friends)
		{
			if (info.IsOnline)
			{
				OnlineCount++;
			}
		}
		if (FriendsCountText != null)
		{
			FriendsCountText.text = "Bạn trực tuyến: "+OnlineCount + "/" + friends.Length;
		}
	}
	
	
	void CleanCacheList()
	{
		if (cacheFriendsInfo.Count > 0)
		{
			for (int i = 0; i < cacheFriendsInfo.Count; i++)
			{
				Destroy(cacheFriendsInfo[i].gameObject);
			}
		}
		cacheFriendsInfo.Clear();
		
	}
	
	public void UI_ChoiseShowAll()
	{
		if (cacheFriendsInfo.Count > 0)
		{
			for (int i = 0; i < cacheFriendsInfo.Count; i++)
			{
				cacheFriendsInfo[i].gameObject.SetActive(true);
			}
		}
	}
	
	public void UI_ChoiseShowOnline()
	{
		if (cacheFriendsInfo.Count > 0)
		{
			for (int i = 0; i < cacheFriendsInfo.Count; i++)
			{
				if(cacheFriendsInfo[i].StatusText.text == "Offline" )
					cacheFriendsInfo[i].gameObject.SetActive(false);
			}
		}
	}
	
	public void RemoveFriend(string friend)
	{
		if (Friends.Contains(friend))
		{
			Friends.Remove(friend);
			if (Friends.Count > 0)
			{
				if(Friends.Count > 1 && Friends.Contains("Null"))
				{
					Friends.Remove("Null");
					Friends.Remove("Null");
				}
				//PhotonNetwork.FindFriends(Friends.ToArray());
			}
			else
			{
				AddFriend("Null");
				//PhotonNetwork.FindFriends(Friends.ToArray());
			}
			for(int i = 0;i< PhotonNetwork.Friends.Count;i++){
				if(PhotonNetwork.Friends[i].Name == friend){
					PhotonNetwork.Friends.Remove(PhotonNetwork.Friends[i]);
					break;
				}
			}
			
			UpdateFriendList(true);
			WaitForEvent = true;
		}
		
	}
	
	public void AddFriend(string friend)
	{
		
		Friends.Add(friend);
		//PhotonNetwork.FindFriends(Friends.ToArray());
		UpdateFriendList(true);
		WaitForEvent = true;
	}
	public void UI_AddFriend()
	{
		if(PlayerNameInput.text != ""){
			ErrorView.instance.Loading(true);
			//PlayFabManager.instance.AddFriend(PlayerNameInput.text);
		}
	}
	
	public void UI_AddFriendResult(string messageStr)
	{
		ErrorView.instance.ShowPopupError (messageStr);
	}
	public void UI_RemoveFriendResult(string messageStr)
	{

		ErrorView.instance.ShowPopupError (messageStr);
	}
	public void OnJoinedLobbyV2()
	{
		GetFriendsStore();
		UpdateList ();

	}
	public void UI_OnEnable(){
		this.gameObject.SetActive (true);
	}
	public void UI_OnDisable(){
		this.gameObject.SetActive (false);
	}

//	private void LoadFriendList()
//	{
//		GetFriendsListRequest request = new GetFriendsListRequest();
//		request.IncludeFacebookFriends = true;
//		PlayFabClientAPI.GetFriendsList (request, OnLoadFriendListSuccess,OnLoadFriendListFail);
//	}
//	private void OnLoadFriendListSuccess(GetFriendsListResult result) {
//		CSVR_GameSetting.friends = result.Friends;
//		for(int i=0;i<CSVR_GameSetting.friends.Count;i++){
//			if(CSVR_GameSetting.friends[i].FriendPlayFabId != ""){
//				OnFriendInfo(CSVR_GameSetting.friends[i].FriendPlayFabId);
//				CSVR_GameSetting.friendsListName.Add(CSVR_GameSetting.friends[i].FriendPlayFabId);
//			}
//		}
//
//	}

}
