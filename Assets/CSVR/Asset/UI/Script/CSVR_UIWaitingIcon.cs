﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CSVR_UIWaitingIcon : SingletonBaseScreen<CSVR_UIWaitingIcon> {

    public Button ButtonOk;

    public override void Open()
    {
        base.Open();
        ShowButtonOk(false);
    }

    public void ShowButtonOk(bool On)
    {
        ButtonOk.gameObject.SetActive(On);
    }

    public void SetAction(UnityAction UnityAction)
    {
        ButtonOk.onClick.RemoveAllListeners();
        ButtonOk.onClick.AddListener(UnityAction);
    }
}
