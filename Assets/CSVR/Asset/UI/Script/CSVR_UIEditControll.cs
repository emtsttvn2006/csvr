using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class CSVR_UIEditControll : SingletonBaseScreen<CSVR_UIEditControll> {
	public Image[] settings;
	public Image[] stopZooms;
	public Image[] startZooms;
	public Image[] crouchs;
	public Image[] heaths;
	public Image[] backpacks;
	public Image[] pickUps;
	public Image[] reloads;
	public Image[] changes;
	public Image[] weapons;
	public Image[] wifis;
	public Image[] grenades;
	public Image[] jumps;
	public Image[] results;
	public Image[] moveControls;
	public Image[] fireControls;

	public UIWindowBase[] eidts;
	public UIWindowBaseV2[] controls;
	public GameObject[] canvas;
	public bool isHome = false;

	public override void Awake(){
		//trong game
		if (!isHome) {
			if (PlayerPrefs.HasKey ("settingslocalPosition")) {
				settings [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("settingslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("stopZoomslocalPosition")) {
				stopZooms [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("stopZoomslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("startZoomslocalPosition")) {
				startZooms [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("startZoomslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("crouchsslocalPosition")) {
				crouchs [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("crouchsslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("heathslocalPosition")) {
				heaths [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("heathslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("backpackslocalPosition")) {
				backpacks [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("backpackslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("pickUpslocalPosition")) {
				pickUps [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("pickUpslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("reloadslocalPosition")) {
				reloads [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("reloadslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("changeslocalPosition")) {
				changes [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("changeslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("weaponslocalPosition")) {
				weapons [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("weaponslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("wifislocalPosition")) {
				wifis [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("wifislocalPosition"));
			}
			if (PlayerPrefs.HasKey ("grenadeslocalPosition")) {
				grenades [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("grenadeslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("jumpslocalPosition")) {
				jumps [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("jumpslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("resultslocalPosition")) {
				results [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("resultslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("moveControlslocalPosition")) {
				moveControls [2].transform.localPosition = getVector3 (PlayerPrefs.GetString ("moveControlslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("fireControlslocalPosition")) {
				fireControls [0].transform.localPosition = getVector3 (PlayerPrefs.GetString ("fireControlslocalPosition"));
				fireControls [2].transform.localPosition = fireControls [0].transform.localPosition;
			}
			#region scale
			if (PlayerPrefs.HasKey ("settingslocalScale")) {
				settings [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("settingslocalScale"));
			}
			if (PlayerPrefs.HasKey ("stopZoomslocalScale")) {
				stopZooms [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("stopZoomslocalScale"));
			}
			if (PlayerPrefs.HasKey ("startZoomslocalScale")) {
				startZooms [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("startZoomslocalScale"));
			}
			if (PlayerPrefs.HasKey ("crouchsslocalScale")) {
				crouchs [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("crouchsslocalScale"));
			}
			if (PlayerPrefs.HasKey ("heathslocalScale")) {
				heaths [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("heathslocalScale"));
			}
			if (PlayerPrefs.HasKey ("backpackslocalScale")) {
				backpacks [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("backpackslocalScale"));
			}
			if (PlayerPrefs.HasKey ("pickUpslocalScale")) {
				pickUps [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("pickUpslocalScale"));
			}
			if (PlayerPrefs.HasKey ("reloadslocalScale")) {
				reloads [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("reloadslocalScale"));
			}
			if (PlayerPrefs.HasKey ("changeslocalScale")) {
				changes [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("changeslocalScale"));
			}
			if (PlayerPrefs.HasKey ("weaponslocalScale")) {
				weapons [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("weaponslocalScale"));
			}
			if (PlayerPrefs.HasKey ("wifislocalScale")) {
				wifis [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("wifislocalScale"));
			}
			if (PlayerPrefs.HasKey ("grenadeslocalScale")) {
				grenades [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("grenadeslocalScale"));
			}
			if (PlayerPrefs.HasKey ("jumpslocalScale")) {
				jumps [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("jumpslocalScale"));
			}
			if (PlayerPrefs.HasKey ("resultslocalScale")) {
				results [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("resultslocalScale"));
			}
			if (PlayerPrefs.HasKey ("moveControlslocalScale")) {
				moveControls [2].transform.localScale = getVector3 (PlayerPrefs.GetString ("moveControlslocalScale"));
				moveControls [1].transform.localScale = moveControls [2].transform.localScale;
				moveControls [0].transform.localScale = moveControls [2].transform.localScale;
			}
			if (PlayerPrefs.HasKey ("fireControlslocalScale")) {
				fireControls [0].transform.localScale = getVector3 (PlayerPrefs.GetString ("fireControlslocalScale"));
				fireControls [1].transform.localScale = fireControls [0].transform.localScale;
				fireControls [2].transform.localScale = fireControls [0].transform.localScale;
			}
			#endregion
			canvas [1].gameObject.SetActive (false);
		} 

	}
	void OnEnable(){
		//trong game
		if (!isHome) {
			settings [1].transform.localPosition = settings [0].transform.localPosition;
			settings [1].transform.localScale = settings [0].transform.localScale;

			stopZooms [1].transform.localPosition = stopZooms [0].transform.localPosition;
			stopZooms [1].transform.localScale = stopZooms [0].transform.localScale;

			startZooms [1].transform.localPosition = startZooms [0].transform.localPosition;
			startZooms [1].transform.localScale = startZooms [0].transform.localScale;

			crouchs [1].transform.localPosition = crouchs [0].transform.localPosition;
			crouchs [1].transform.localScale = crouchs [0].transform.localScale;

			heaths [1].transform.localPosition = heaths [0].transform.localPosition;
			heaths [1].transform.localScale = heaths [0].transform.localScale;

			backpacks [1].transform.localPosition = backpacks [0].transform.localPosition;
			backpacks [1].transform.localScale = backpacks [0].transform.localScale;

			pickUps [1].transform.localPosition = pickUps [0].transform.localPosition;
			pickUps [1].transform.localScale = pickUps [0].transform.localScale;

			reloads [1].transform.localPosition = reloads [0].transform.localPosition;
			reloads [1].transform.localScale = reloads [0].transform.localScale;

			changes [1].transform.localPosition = changes [0].transform.localPosition;
			changes [1].transform.localScale = changes [0].transform.localScale;

			weapons [1].transform.localPosition = weapons [0].transform.localPosition;
			weapons [1].transform.localScale = weapons [0].transform.localScale;

			wifis [1].transform.localPosition = wifis [0].transform.localPosition;
			wifis [1].transform.localScale = wifis [0].transform.localScale;

			grenades [1].transform.localPosition = grenades [0].transform.localPosition;
			grenades [1].transform.localScale = grenades [0].transform.localScale;

			jumps [1].transform.localPosition = jumps [0].transform.localPosition;
			jumps [1].transform.localScale = jumps [0].transform.localScale;

			results [1].transform.localPosition = results [0].transform.localPosition;
			results [1].transform.localScale = results [0].transform.localScale;

			moveControls [3].transform.localPosition = moveControls [2].transform.localPosition;
			moveControls [3].transform.localScale = moveControls [2].transform.localScale;

			fireControls [3].transform.localPosition = fireControls [0].transform.localPosition;
			fireControls [3].transform.localScale = fireControls [0].transform.localScale;
		}else {
			if (PlayerPrefs.HasKey ("settingslocalPosition")) {
				settings [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("settingslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("stopZoomslocalPosition")) {
				stopZooms [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("stopZoomslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("startZoomslocalPosition")) {
				startZooms [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("startZoomslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("crouchsslocalPosition")) {
				crouchs [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("crouchsslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("heathslocalPosition")) {
				heaths [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("heathslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("backpackslocalPosition")) {
				backpacks [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("backpackslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("pickUpslocalPosition")) {
				pickUps [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("pickUpslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("reloadslocalPosition")) {
				reloads [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("reloadslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("changeslocalPosition")) {
				changes [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("changeslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("weaponslocalPosition")) {
				weapons [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("weaponslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("wifislocalPosition")) {
				wifis [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("wifislocalPosition"));
			}
			if (PlayerPrefs.HasKey ("grenadeslocalPosition")) {
				grenades [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("grenadeslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("jumpslocalPosition")) {
				jumps [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("jumpslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("resultslocalPosition")) {
				results [1].transform.localPosition = getVector3 (PlayerPrefs.GetString ("resultslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("moveControlslocalPosition")) {
				moveControls [3].transform.localPosition = getVector3 (PlayerPrefs.GetString ("moveControlslocalPosition"));
			}
			if (PlayerPrefs.HasKey ("fireControlslocalPosition")) {
				fireControls [3].transform.localPosition = getVector3 (PlayerPrefs.GetString ("fireControlslocalPosition"));
			}

			if (PlayerPrefs.HasKey ("settingslocalScale")) {
				settings [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("settingslocalScale"));
			}
			if (PlayerPrefs.HasKey ("stopZoomslocalScale")) {
				stopZooms [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("stopZoomslocalScale"));
			}
			if (PlayerPrefs.HasKey ("startZoomslocalScale")) {
				startZooms [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("startZoomslocalScale"));
			}
			if (PlayerPrefs.HasKey ("crouchsslocalScale")) {
				crouchs [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("crouchsslocalScale"));
			}
			if (PlayerPrefs.HasKey ("heathslocalScale")) {
				heaths [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("heathslocalScale"));
			}
			if (PlayerPrefs.HasKey ("backpackslocalScale")) {
				backpacks [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("backpackslocalScale"));
			}
			if (PlayerPrefs.HasKey ("pickUpslocalScale")) {
				pickUps [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("pickUpslocalScale"));
			}
			if (PlayerPrefs.HasKey ("reloadslocalScale")) {
				reloads [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("reloadslocalScale"));
			}
			if (PlayerPrefs.HasKey ("changeslocalScale")) {
				changes [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("changeslocalScale"));
			}
			if (PlayerPrefs.HasKey ("weaponslocalScale")) {
				weapons [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("weaponslocalScale"));
			}
			if (PlayerPrefs.HasKey ("wifislocalScale")) {
				wifis [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("wifislocalScale"));
			}
			if (PlayerPrefs.HasKey ("grenadeslocalScale")) {
				grenades [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("grenadeslocalScale"));
			}
			if (PlayerPrefs.HasKey ("jumpslocalScale")) {
				jumps [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("jumpslocalScale"));
			}
			if (PlayerPrefs.HasKey ("resultslocalScale")) {
				results [1].transform.localScale = getVector3 (PlayerPrefs.GetString ("resultslocalScale"));
			}
			if (PlayerPrefs.HasKey ("moveControlslocalScale")) {
				moveControls [3].transform.localScale = getVector3 (PlayerPrefs.GetString ("moveControlslocalScale"));
			}
			if (PlayerPrefs.HasKey ("fireControlslocalScale")) {
				fireControls [3].transform.localScale = getVector3 (PlayerPrefs.GetString ("fireControlslocalScale"));
			}
		}
	}
	public void UI_OnEditResetClick(){
		for (int i = 0; i < eidts.Length; i++) {
			eidts[i].ResetCoords = true;
		}
		//trong game
		if (!isHome) {
			controls [0].ResetCoords = true;
		}
		controls [1].ResetCoords = true;
	}
	public void UI_OnEditCanelClick(){
		canvas [1].gameObject.SetActive (false);
	}
	public void UI_OnEditSaveClick(){
		//trong game
		if (!isHome) {
			settings [0].transform.localPosition = settings [1].transform.localPosition;
			settings [0].transform.localScale = settings [1].transform.localScale;

			stopZooms [0].transform.localPosition = stopZooms [1].transform.localPosition;
			stopZooms [0].transform.localScale = stopZooms [1].transform.localScale;

			startZooms [0].transform.localPosition = startZooms [1].transform.localPosition;
			startZooms [0].transform.localScale = startZooms [1].transform.localScale;

			crouchs [0].transform.localPosition = crouchs [1].transform.localPosition;
			crouchs [0].transform.localScale = crouchs [1].transform.localScale;

			heaths [0].transform.localPosition = heaths [1].transform.localPosition;
			heaths [0].transform.localScale = heaths [1].transform.localScale;

			backpacks [0].transform.localPosition = backpacks [1].transform.localPosition;
			backpacks [0].transform.localScale = backpacks [1].transform.localScale;

			pickUps [0].transform.localPosition = pickUps [1].transform.localPosition;
			pickUps [0].transform.localScale = pickUps [1].transform.localScale;

			reloads [0].transform.localPosition = reloads [1].transform.localPosition;
			reloads [0].transform.localScale = reloads [1].transform.localScale;

			changes [0].transform.localPosition = changes [1].transform.localPosition;
			changes [0].transform.localScale = changes [1].transform.localScale;

			weapons [0].transform.localPosition = weapons [1].transform.localPosition;
			weapons [0].transform.localScale = weapons [1].transform.localScale;

			wifis [0].transform.localPosition = wifis [1].transform.localPosition;
			wifis [0].transform.localScale = wifis [1].transform.localScale;

			grenades [0].transform.localPosition = grenades [1].transform.localPosition;
			grenades [0].transform.localScale = grenades [1].transform.localScale;

			jumps [0].transform.localPosition = jumps [1].transform.localPosition;
			jumps [0].transform.localScale = jumps [1].transform.localScale;

			results [0].transform.localPosition = results [1].transform.localPosition;
			results [0].transform.localScale = results [1].transform.localScale;

			moveControls [2].transform.localPosition = moveControls [3].transform.localPosition;
			moveControls [0].transform.localScale = moveControls [3].transform.localScale;
			moveControls [1].transform.localScale = moveControls [3].transform.localScale;
			moveControls [2].transform.localScale = moveControls [3].transform.localScale;


			fireControls [0].transform.localPosition = fireControls [3].transform.localPosition;
			fireControls [2].transform.localPosition = fireControls [3].transform.localPosition;
			fireControls [0].transform.localScale = fireControls [3].transform.localScale;
			fireControls [1].transform.localScale = fireControls [3].transform.localScale;
			fireControls [2].transform.localScale = fireControls [3].transform.localScale;
		}
		PlayerPrefs.SetString ("settingslocalPosition", settings [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("settingslocalScale", settings [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("stopZoomslocalPosition", stopZooms [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("stopZoomslocalScale", stopZooms [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("startZoomslocalPosition", startZooms [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("startZoomslocalScale", startZooms [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("crouchsslocalPosition", crouchs [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("crouchsslocalScale", crouchs [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("heathslocalPosition", heaths [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("heathslocalScale", heaths [1].transform.localScale.ToString ());


		PlayerPrefs.SetString ("backpackslocalPosition", backpacks [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("backpackslocalScale", backpacks [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("pickUpslocalPosition", pickUps [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("pickUpslocalScale", pickUps [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("reloadslocalPosition", reloads [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("reloadslocalScale", reloads [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("changeslocalPosition", changes [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("changeslocalScale", changes [1].transform.localScale.ToString ());


		PlayerPrefs.SetString ("weaponslocalPosition", weapons [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("weaponslocalScale", weapons [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("wifislocalPosition", wifis [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("wifislocalScale", wifis [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("grenadeslocalPosition", grenades [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("grenadeslocalScale", grenades [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("jumpslocalPosition", jumps [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("jumpslocalScale", jumps [1].transform.localScale.ToString ());

		PlayerPrefs.SetString ("resultslocalPosition", results [1].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("resultslocalScale", results [1].transform.localScale.ToString ());


		PlayerPrefs.SetString ("moveControlslocalPosition", moveControls [3].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("moveControlslocalScale", moveControls [3].transform.localScale.ToString ());

		PlayerPrefs.SetString ("fireControlslocalPosition", fireControls [3].transform.localPosition.ToString ());
		PlayerPrefs.SetString ("fireControlslocalScale", fireControls [3].transform.localScale.ToString ());


		canvas [1].gameObject.SetActive (false);
	}
	public Vector3 getVector3(string rString){
		string[] temp = rString.Substring(1,rString.Length-2).Split(',');
		float x = float.Parse(temp[0]);
		float y = float.Parse(temp[1]);
		float z = float.Parse(temp[2]);
		Vector3 rValue = new Vector3(x,y,z);
		return rValue;
	}
}
