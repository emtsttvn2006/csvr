﻿using UnityEngine;
using System.Collections;
using MarchingBytes;
using UnityEngine.UI;
using Horus.ClientModels;
using System.Collections.Generic;

public class CSVR_UIMember : SingletonBaseScreen<CSVR_UIMember>
{
    public LoopVerticalScrollRect scrollMemberList;

    public override void Open()
    {
        base.Open();
        if (scrollMemberList.prefabPool == null) scrollMemberList.prefabPool = HomeScreen.instance.EasyPool.GetComponent<EasyObjectPool>();
        UpdateUI();
    }    

    public void UpdateUI()
    {
        scrollMemberList.totalCount = CSVR_UIClanList_XemMyClan.instance.listString_Members.Count;
        scrollMemberList.RefillCells();
    }
}
