﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using CnControls;
using Horus.ClientModels;

public class ControlWeapon : MonoBehaviour
{

 //   public Light MainLight;
    //khai báo sử dụng anim và load súng
    public Animator animator;
    string NameWeapon, stateAnim;
    public GameObject[] ListWeapon;

	bool isEnable = false;
	string idWeaponTemp;
	ItemInstance weaponTemp;

    void OnEnable()
    {
		isEnable = true;
//		if (weaponTemp != null)
//			SetWeapon (weaponTemp);
    }
	void OnDisable()
	{
		isEnable = false;
	}

	public void SetWeapon(ItemInstance weapon)
	{
		weaponTemp = weapon;
		if (isEnable) {
			for (int i = 0; i < ListWeapon.Length; i++) {
				if (ListWeapon [i].name == weapon.ItemIdModel) {
					ListWeapon [i].SetActive (true);
					SetupAccessories (weapon, ListWeapon [i].transform);
                    SetStateAnim(weapon.ItemIdModel); 
				} else {
					ListWeapon [i].SetActive (false);
				}
			}
		}
	}
	private void SetupAccessories( ItemInstance idWeapon,Transform transformParent){
		vp_WeaponAccessories acsTransform;
		if (transformParent.childCount == 0) {
			GameObject foundThirdWeapon = Resources.Load ("Weapon/Remote/Third/" + idWeapon.ItemIdModel) as GameObject;
            //foundThirdWeapon.SetActive (true);
           // Yo.Log ("1. active", foundThirdWeapon.activeSelf.ToString());
			if (foundThirdWeapon == null)
				return;
            
			GameObject newWeapon = Instantiate (foundThirdWeapon);
           // Yo.Log ("2. active", newWeapon.activeSelf.ToString());
            newWeapon.SetActive (true);
			SetLayerRecursively(newWeapon,5);
			newWeapon.transform.SetParent (transformParent);
			newWeapon.transform.localScale = Vector3.one;
			newWeapon.transform.localPosition = Vector3.zero;
			newWeapon.transform.localRotation = Quaternion.identity;
			
			acsTransform = newWeapon.GetComponent<vp_WeaponAccessories> ();
		} else {
			acsTransform = transformParent.GetChild(0).GetComponent<vp_WeaponAccessories> ();
		}

		if (acsTransform == null)
			return;
        
		try{
			string _idAcModel = idWeapon.accessories.scope["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			SetLayerRecursively(_obj,5);
			_obj.transform.SetParent(acsTransform.accessoriesScopeTransform);
			acsTransform.accessoriesScopeTransform.GetChild(0).gameObject.SetActive(false);
			if(acsTransform.accessoriesScopeTransform.childCount > 2){
				Destroy(acsTransform.accessoriesScopeTransform.GetChild(1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive(true);

		}catch{
			acsTransform.accessoriesScopeTransform.GetChild(0).gameObject.SetActive(true);
			if(acsTransform.accessoriesScopeTransform.childCount > 1){
				Destroy(acsTransform.accessoriesScopeTransform.GetChild(1).gameObject);
			}
		}

		try{
			string _idAcModel = idWeapon.accessories.magazine["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			SetLayerRecursively(_obj,5);
			_obj.transform.SetParent(acsTransform.accessoriesMagazineTransform);
			acsTransform.accessoriesMagazineTransform.GetChild(0).gameObject.SetActive(false);
			if(acsTransform.accessoriesMagazineTransform.childCount > 2){
				Destroy(acsTransform.accessoriesMagazineTransform.GetChild(1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive(true);

		}catch{
			acsTransform.accessoriesMagazineTransform.GetChild(0).gameObject.SetActive(true);
			if(acsTransform.accessoriesMagazineTransform.childCount > 1){
				Destroy(acsTransform.accessoriesMagazineTransform.GetChild(1).gameObject);
			}
		}

		try {
			string _idAcModel = idWeapon.accessories.grip["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			SetLayerRecursively(_obj,5);
			_obj.transform.SetParent (acsTransform.accessoriesGripTransform);
			acsTransform.accessoriesGripTransform.GetChild (0).gameObject.SetActive (false);
			if (acsTransform.accessoriesGripTransform.childCount > 2) {
				Destroy (acsTransform.accessoriesGripTransform.GetChild (1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive (true);

		} catch {
			acsTransform.accessoriesGripTransform.GetChild (0).gameObject.SetActive (true);
			if (acsTransform.accessoriesGripTransform.childCount > 1) {
				Destroy (acsTransform.accessoriesGripTransform.GetChild (1).gameObject);
			}
		}


		try {
			string _idAcModel = idWeapon.accessories.muzzle["pluginModelId"];
			if (_idAcModel == null)
				return;
			GameObject foundAccessories = Resources.Load ("Accessories/Local/Model/" + _idAcModel) as GameObject;
			GameObject _obj = Instantiate( foundAccessories);

			SetLayerRecursively(_obj,5);
			_obj.transform.SetParent (acsTransform.accessoriesMuzzleTransform);
			acsTransform.accessoriesMuzzleTransform.GetChild (0).gameObject.SetActive (false);
			if (acsTransform.accessoriesMuzzleTransform.childCount > 2) {
				Destroy (acsTransform.accessoriesMuzzleTransform.GetChild (1).gameObject);
			}
			_obj.transform.localPosition = Vector3.zero;
			_obj.transform.localRotation = Quaternion.identity;
			_obj.transform.localScale = Vector3.one;
			_obj.transform.gameObject.SetActive (true);

		} catch {
			acsTransform.accessoriesMuzzleTransform.GetChild (0).gameObject.SetActive (true);
			if (acsTransform.accessoriesMuzzleTransform.childCount > 1) {
				Destroy (acsTransform.accessoriesMuzzleTransform.GetChild (1).gameObject);
			}
		}

	}
	private void SetLayerRecursively(GameObject go, int layerNumber)
	{
		foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
		{
			trans.gameObject.layer = layerNumber;
		}
	}
    public void SetWeapon(string nameWeapon)
    {
		idWeaponTemp = nameWeapon;
		if (isEnable) {
			for (int i = 0; i < ListWeapon.Length; i++) {
				if (ListWeapon [i].name == nameWeapon) {
					ListWeapon [i].SetActive (true);
					SetupAccessories (nameWeapon, ListWeapon [i].transform);
				} else {
					ListWeapon [i].SetActive (false);
				}
			}
		}
    }
	private void SetupAccessories( string idWeapon,Transform transformParent){
		if (transformParent.childCount == 0 && transformParent.GetComponent<MeshRenderer>() == null) {
			GameObject foundThirdWeapon = Resources.Load ("Weapon/Remote/Third/" +idWeapon) as GameObject;
			if (foundThirdWeapon == null)
				return;
			GameObject newWeapon = Instantiate (foundThirdWeapon);
            //Yo.Log ("3. active", newWeapon.activeSelf.ToString());
            newWeapon.SetActive (true);
			SetLayerRecursively(newWeapon,5);
			newWeapon.transform.SetParent (transformParent);
			newWeapon.transform.localScale = Vector3.one;
			newWeapon.transform.localPosition = Vector3.zero;
			newWeapon.transform.localRotation = Quaternion.identity;

		}
	}

    public void SetStateAnim(string nameAnim)
    {
		//Yo.Log ("Anim", nameAnim);
		if (isEnable) {
			switch (nameAnim) {
			case "66_959_Desert_Eagle":
			case "66_999_P320":
            case "66_949_P99":
            case "66_939_DEG":
            case "66_929_DEC":
				animator.SetTrigger ("Pistol");
                animator.SetBool("isPistol",true);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
			case "11_999_AK47":
			case "11_969_AK12":
            case "11_939_AK337":
            case "11_929_AK47C":
            case "11_949_M4A1":
            case "11_979_M4A4":
            case "11_919_M4A1U":
            case "11_909_M4A1C":
            case "11_889_M4A1G":
            case "11_899_AR15":
				animator.SetTrigger ("Rifle");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", true);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
            case "11_959_AUG":
                animator.SetTrigger("AUG");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", true);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isDieuCay", false);
                break;
            case "44_959_HK556":
                animator.SetTrigger ("HKG56");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", true);
                animator.SetBool("isDieuCay", false);
                break;
			case "44_999_MP5":
            case "44_969_HK308":
				animator.SetTrigger ("SMG");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", true);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
			case "55_999_M249":
            case "55_989_M60":
				animator.SetTrigger ("Machine");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", true);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
			case "33_989_Nova":
            case "22_999_SS":
                animator.SetTrigger ("Nova");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", true);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
                break;
			case "33_999_XM1014":
            case "33_959_Jackal":
				animator.SetTrigger ("Shotgun");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", true);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
			case "77_999_Knife":
            case "77_989_M9B":
            case "77_929_DerK":
            case "77_939_CamK":
				animator.SetTrigger ("Knife");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", true);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
			case "88_989_CSG":
            case "88_959_HMXG":
            case "88_969_Smoke":
				animator.SetTrigger ("Grenade");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", true);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
			case "22_989_AWP":
			case "22_979_SCAR20":
            case "22_939_M82":            
            case "22_949_D338":
            case "22_909_MR56":
				animator.SetTrigger ("Sniper");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", true);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", false);
				break;
            case "77_919_DC":
                animator.SetTrigger("DieuCay");
                animator.SetBool("isPistol", false);
                animator.SetBool("isRifle", false);
                animator.SetBool("isAUG", false);
                animator.SetBool("isSMG", false);
                animator.SetBool("isMachine", false);
                animator.SetBool("isShotgun", false);
                animator.SetBool("isKnife", false);
                animator.SetBool("isGrenade", false);
                animator.SetBool("isSniper", false);
                animator.SetBool("isNova", false);
                animator.SetBool("isHKG56", false);
                animator.SetBool("isDieuCay", true);
                break;
            default:
				break;

			}
		}
    }
	public float RotationSpeed = 15f;
	public Transform OriginTransform;
	
	private void Update()
	{
		var horizontalMovement = CnInputManager.GetAxis("Touch Horizontal")*0.3f;
		
		OriginTransform.Rotate(Vector3.down, horizontalMovement * Time.deltaTime * RotationSpeed);
	}

}
