﻿using UnityEngine;
using System.Collections;

public class BloodSplatFX : MonoBehaviour
{

    public Sprite[] RandomSprite;
    private int SpriteIndex;
    public GameObject BloodOnWallFX;
    private ParticleSystem part;
    private ParticleCollisionEvent[] collisionEvents;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new ParticleCollisionEvent[16];
    }
    void OnParticleCollision(GameObject other)
    {
        int safeLength = part.GetSafeCollisionEventSize();
        if (collisionEvents.Length < safeLength)
            collisionEvents = new ParticleCollisionEvent[safeLength];

        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        int i = 0;
        while (i < numCollisionEvents)
        {
            Vector3 pos = collisionEvents[i].intersection + collisionEvents[i].normal.normalized * 0.01f;
            Quaternion quat = Quaternion.LookRotation(-collisionEvents[i].normal.normalized);
            GameObject fxx = Instantiate(BloodOnWallFX, pos, quat) as GameObject;
            float randomscale = Random.Range(0.2f, 0.3f);
            fxx.transform.localScale = new Vector3(randomscale, randomscale, 1f);
            SpriteRenderer render = fxx.gameObject.GetComponent<SpriteRenderer>();
            if (RandomSprite.Length > 0)
            {
                SpriteIndex = Random.Range(0, RandomSprite.Length);
                render.sprite = RandomSprite[SpriteIndex];
            }
            i++;
        }
    }
}
