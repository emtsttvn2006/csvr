﻿using UnityEngine;
using System.Collections;

public class CSVR_DestroyHeathEffect : MonoBehaviour
{
    public float time;
    // Use this for initialization
    void Start()
    {
        Invoke("DestroyObject", time);
    }
    void DestroyObject()
    {
        Destroy(gameObject);
    }
}
