﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SetWeaponInfo : MonoBehaviour {
	public string PlayfabID;
	public int DroppedGunID;


    public int Count, Capacity, InternalCount;

	//Step 1
	public Vector2 AimAngle_Step1;
	public float AimAngleIncrease_Step1;
	
	//Step 2
	public Vector2 AimAngle_Step2;
	public float AimAngleIncrease_Step2;
	
	//Step 3
	public Vector2 AimAngle_Step3;
	public float AimAngleIncrease_Step3;
	
	
	public float AimAngleDecrease;
	

	public float BulletAngleMin;
	public float BulletAngleMax;
	public float BulletAngleIncrease;
	public float BulletAngleDecrease;

	public float WeaponDamage = 0f;
	public float WeaponRange = 1.5f;


	public float ReloadDuration;
	public float weightWeapon;
	public float ProjectileFiringRate;
	public float ProjectileTapFiringRate;



    public string WeaponName, WeaponID,BulletName,ClassGun;
    
	// Use this for initialization
	void Start () {
        StartCoroutine(PlessDestroy());
	}
    IEnumerator PlessDestroy()
    {
        yield return new WaitForSeconds(15f);
        Destroy(gameObject);
    }
    void Update()
    {
        transform.Rotate(transform.up * 15 * Time.deltaTime);
    }
}
