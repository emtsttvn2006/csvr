﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

[CustomEditor(typeof(HorusImage))]
[CanEditMultipleObjects]
public class HorusImageEditor : Editor {

    public SerializedProperty Editing;
    public SerializedProperty sprites;
    public SerializedProperty sprite_child;

    public void OnEnable()
    {
        Editing = serializedObject.FindProperty("Editing");
        sprites = serializedObject.FindProperty("sprite");
        sprite_child = serializedObject.FindProperty("sprite_child");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        DrawDefaultInspector();

        if (Editing.boolValue)
        {
            if (GUILayout.Button("LoadImage"))
            {
                ((HorusImage)target).LoadImage(sprite_child.stringValue);                
            }

            if (GUILayout.Button("Get Child Name"))
            {
                ((HorusImage)target).UpdateImage();
            }
        }

        serializedObject.ApplyModifiedProperties();
    }
}
