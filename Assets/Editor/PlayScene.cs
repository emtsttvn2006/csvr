﻿// IN YOUR EDITOR FOLDER, have SimpleEditorUtils.cs.
// paste in this text.
// to play, HIT COMMAND-ZERO rather than command-P
// (the zero key, is near the P key, so it's easy to remember)
// simply insert the actual name of your opening scene
// "__preEverythingScene" on the second last line of code below.

using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public static class PlayScene
{
    // click command-0 to go to the prelaunch scene and then play

    [MenuItem("Edit/SceneManage/Open Intro %`")]
    public static void OpenIntro()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/CSVR/Core/Game/Scene/New/Intro.unity");        
    }

    [MenuItem("Edit/SceneManage/Open Login %1")]
    public static void OpenLogin()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/CSVR/Core/Game/Scene/New/Login.unity");        
    }

    [MenuItem("Edit/SceneManage/Open %2")]
    public static void OpenHome()
    {
        if (EditorApplication.isPlaying == true)
        {
            EditorApplication.isPlaying = false;
            return;
        }
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/CSVR/Core/Game/Scene/New/Home.unity");        
    }

    [MenuItem("Edit/SceneManage/Open %g")]
    public static void GabageCollect()
    {
        Debug.Log("Gabage Collect");
        System.GC.Collect();
    }
}