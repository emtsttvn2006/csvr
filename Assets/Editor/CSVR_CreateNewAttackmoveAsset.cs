﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class CSVR_CreateNewAttackmoveAsset
{
	[MenuItem("Assets/Create/CSVR/AI Jump Attackmove")]
	public static void CreateMyJumpAttackmove()
	{
		CSVR_AIJumpAttackmove asset = ScriptableObject.CreateInstance<CSVR_AIJumpAttackmove>();

		AssetDatabase.CreateAsset(asset, "Assets/New AI Jump Attackmove.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}

	[MenuItem("Assets/Create/CSVR/AI Projectile Attackmove")]
	public static void CreateMyProjectileAttackmove()
	{
		CSVR_AIProjectileAttackmove asset = ScriptableObject.CreateInstance<CSVR_AIProjectileAttackmove>();

		AssetDatabase.CreateAsset(asset, "Assets/New AI Projectile Attackmove.asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = asset;
	}
}