﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

//[AddComponentMenu("MiniMap/Map marker")]
public class MapBackground : MonoBehaviour
{
	
	#region Public
	
	/* Sprite that will be shown on the map
     */
//	public Sprite markerSprite;

	//Origin capture size of ortho camera
	public float CaptureSize = 32f;
    public Sprite imgMiniMap;
	//transform of capture cam
	public Transform CapturePosition;

	/* Size of the marker on the map (width & height)
     */
	[HideInInspector]
	public float markerSize = 6.5f;
	
	/* Enables or disables this marker on the map
     */
	public bool isActive = true;
	
	public Image MarkerImage
	{
		get
		{
			return markerImage;
		}
	}
	
	#endregion
	
	#region Private
	
	private Image markerImage;
	
	#endregion
	
	#region Unity methods
	MapCanvasController controller;

	void Start () {

		controller = MapCanvasController.Instance;
		if (!controller)
		{
			Destroy(this);
			return;
		}
		markerImage = GetComponent<Image>();
        markerImage.sprite = imgMiniMap;
		markerImage.rectTransform.localPosition = Vector3.zero;
		markerImage.rectTransform.localScale = Vector3.one;
		markerImage.rectTransform.sizeDelta = new Vector2(CaptureSize, CaptureSize);
		markerImage.gameObject.SetActive(true);

	}
	
	
	void Update () {

		if (!controller)
		{
			return;
		}
		MapCanvasController.Instance.checkInMap(this);

	}
	
	void OnDestroy()
	{
		if (markerImage)
		{
			Destroy(markerImage.gameObject);
		}
	}
	
	#endregion
	
	#region Custom methods
	
	public void show()
	{
		markerImage.gameObject.SetActive(true);
	}
	
	public void hide()
	{
		markerImage.gameObject.SetActive(false);
	}
	
	public Vector3 getPosition()
	{
		return gameObject.transform.position;
	}
	
	public void setLocalPos(Vector3 pos)
	{
		markerImage.rectTransform.localPosition = pos;
		
	}

	public void setMapSize(float newsize){
		markerImage.rectTransform.sizeDelta = new Vector2 (newsize, newsize);
	}

	
	#endregion
}
