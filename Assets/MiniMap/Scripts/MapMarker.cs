﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

[AddComponentMenu("MiniMap/Map marker")]
public class MapMarker : MonoBehaviour
{
	
	#region Public
	public string markerName;

	/* Sprite that will be shown on the map
     */
	public Sprite markerSprite;
	
	/* Size of the marker on the map (width & height)
     */
	public float markerSize = 6.5f;
	
	/* Enables or disables this marker on the map
     */
	public bool isActive = true;

	//-1 la ca 2 benh deu thay
	public int teamNumber = 0;

	//public bool hideOutOfRadius = true;
	public Image MarkerImage
	{
		get
		{
			return markerImage;
		}
	}
	
	#endregion
	
	#region Private
	
	private Image markerImage;
	
	#endregion
	
	#region Unity methods
	MapCanvasController controller;
	void Start () {
		Init ();
	}
	byte InitCount = 5;
	void Init(){

		controller = MapCanvasController.Instance;
		if (controller == null)
		{
			InitCount--;
			if (InitCount == 0)
			{
				Destroy(this);
				return;
			}
			Invoke("Init",2f);
			return;
		}
//		Debug.Log (controller.teamNumber + "/" + teamNumber);
//		if (controller.teamNumber != teamNumber && teamNumber != -1)
//		{
//			Destroy(this);
//			return;
//		}
		GameObject markerImageObject = new GameObject("Marker");
		markerImageObject.AddComponent<Image>();
		
		
		markerImageObject.transform.SetParent(controller.MarkerGroup.MarkerGroupRect);
		markerImage = markerImageObject.GetComponent<Image>();
		markerImage.sprite = markerSprite;
		markerImage.rectTransform.localPosition = Vector3.zero;
		markerImage.rectTransform.localScale = Vector3.one;
		markerImage.rectTransform.sizeDelta = new Vector2(markerSize, markerSize);
		markerImage.name = markerName;
		markerImage.gameObject.SetActive(false);
	}
	
	void Update () {
		
		if (controller == null || (controller.teamNumber != teamNumber && teamNumber != -1))
		{
			return;
		}
		MapCanvasController.Instance.checkIn(this);
		markerImage.rectTransform.rotation = Quaternion.identity;
	}
	
	void OnDestroy()
	{
		if (markerImage)
		{
			Destroy(markerImage.gameObject);
		}
	}
	
	#endregion
	
	#region Custom methods
	
	public void show()
	{
		markerImage.gameObject.SetActive(true);
	}
	
	public void hide()
	{
		markerImage.gameObject.SetActive(false);
	}
	
	public bool isVisible()
	{
		return markerImage.gameObject.activeSelf;
	}
	
	public Vector3 getPosition()
	{
		return gameObject.transform.position;
	}
	
	public void setLocalPos(Vector3 pos)
	{
		markerImage.rectTransform.localPosition = pos;
		
	}
	
	public void setOpacity(float opacity)
	{
		markerImage.color = new Color(1.0f, 1.0f, 1.0f, opacity);
	}
	
	#endregion
}
