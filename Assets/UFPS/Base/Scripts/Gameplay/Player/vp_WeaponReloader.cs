﻿/////////////////////////////////////////////////////////////////////////////////
//
//	vp_WeaponReloader.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	this component adds firearm reload logic, sound and reload
//					duration to an FPWeapon. it doesn't handle ammo max caps
//					or levels. instead this should be governed by an inventory
//					system via the event handler
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
//Horus begin
using System.Collections.Generic;
//Horus end


public class vp_WeaponReloader : MonoBehaviour
{

	protected vp_Weapon m_Weapon = null;
	protected vp_PlayerEventHandler m_Player = null;

	protected AudioSource m_Audio = null;
	public AudioClip[] SoundReload = null;
//    CSVR_UIPlay uiPlay;
	public float ReloadDuration = 1.0f;
    public float TimeWait1, TimeWait2;
    public enum TypeGunClass
    {
        ShotGun,
        Rifle,
        Pistol
    }
    public TypeGunClass GunClassType;
	/// <summary>
	/// 
	/// </summary>
	protected virtual void Awake()
	{

		m_Audio = GetComponent<AudioSource>();

		// store the first player event handler found in the top of our transform hierarchy
		m_Player = (vp_PlayerEventHandler)transform.root.GetComponentInChildren(typeof(vp_PlayerEventHandler));

	}


	/// <summary>
	/// 
	/// </summary>
	protected virtual void Start()
	{
		// store a reference to the FPSWeapon
		m_Weapon = transform.GetComponent<vp_Weapon>();
		
	}


	//Horus begin
	protected List<vp_Event> m_RegisteredEvent  = new List<vp_Event>();

	/// <summary>
	/// registers this component with the event handler (if any)
	/// </summary>
	protected virtual void OnEnable()
	{
		/*
		// allow this monobehaviour to talk to the player event handler

		if (m_Player != null)
			m_Player.Register(this);
			*/

		if (m_Player != null) {
			m_RegisteredEvent.Clear ();
			m_Player.Register (this,out m_RegisteredEvent);
		}
	


	}


	/// <summary>
	/// unregisters this component from the event handler (if any)
	/// </summary>
	protected virtual void OnDisable()
	{

		// unregister this monobehaviour from the player event handler
		/*if (m_Player != null)
			m_Player.Unregister(this);*/

		if (m_Player != null)
			m_Player.Unregister(this, m_RegisteredEvent);

	}

	//Horus end

	/// <summary>
	/// adds a condition (a rule set) that must be met for the
	/// event handler 'Reload' activity to successfully activate.
	/// NOTE: other scripts may have added conditions to this
	/// activity aswell
	/// </summary>
	protected virtual bool CanStart_Reload()
	{

		// can't reload if current weapon isn't fully wielded
		if (m_Player.CurrentWeaponWielded.Get() == false)
			return false;

		// can't reload if weapon is full
		if (m_Player.CurrentWeaponMaxAmmoCount.Get() != 0 &&	// only check if max capacity is reported
			(m_Player.CurrentWeaponAmmoCount.Get() == m_Player.CurrentWeaponMaxAmmoCount.Get()))
			return false;

		// can't reload if the inventory has no additional ammo for this weapon
		if (m_Player.CurrentWeaponClipCount.Get() < 1)
		{
			return false;
		}

		return true;

	}


	/// <summary>
	/// this callback is triggered right after the 'Reload' activity
	/// has been approved for activation
	/// </summary>
	protected virtual void OnStart_Reload()
	{
		//Horus begin
		m_Player.WaitingToAttack = false;
		//Horus end

		// end the Reload activity in 'ReloadDuration' seconds
		m_Player.Reload.AutoDuration = m_Player.CurrentWeaponReloadDuration.Get();
		
		if (m_Audio != null)
		{
            if (TimeWait1 > 0)
            {
                Invoke("PlaySoundStep2", TimeWait1);
            }
			m_Audio.pitch = Time.timeScale;
			m_Audio.PlayOneShot(SoundReload[0]);
		}
        //Invoke("ResetAmmo", m_Player.Reload.AutoDuration + 0.5f);
	}
    void PlaySoundStep2()
    {
        if (TimeWait2 > 0)
        {
            Invoke("PlaySoundStep3", TimeWait2);
        }
        m_Audio.pitch = Time.timeScale;
        m_Audio.PlayOneShot(SoundReload[1]);
    }
    void PlaySoundStep3()
    {
        m_Audio.pitch = Time.timeScale;
        m_Audio.PlayOneShot(SoundReload[2]);
    }
	/// <summary>
	/// this callback is triggered when the 'Reload' activity
	/// deactivates
	/// </summary>
	protected virtual void OnStop_Reload()
	{

		//Horus begin
		if (!gameObject.GetActive())
			return;
        if (GunClassType == TypeGunClass.ShotGun)// WeaponShooter.ClassGunID.ToLower().Contains("shotgun")
        {

			if (!m_Player.WaitingToAttack) {
				m_Player.RefillOneBullet.Try();
			}
			

		} else {
			m_Player.RefillCurrentWeapon.Try();
		}
		//Horus end


	}
	/// <summary>
	/// returns the reload duration of the current weapon
	/// </summary>
	protected virtual float OnValue_CurrentWeaponReloadDuration
	{
		get
		{
			return ReloadDuration;
		}
	}


	//Horus begin
    //protected CSVR_WeaponShooter mWeaponShooter = null;
    //protected CSVR_WeaponShooter WeaponShooter
    //{
    //    get
    //    {
    //        if (mWeaponShooter == null)
    //        {
    //            mWeaponShooter = GetComponent<CSVR_WeaponShooter>();
    //        }

    //        return mWeaponShooter;
    //    }
    //}



}

