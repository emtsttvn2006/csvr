﻿/////////////////////////////////////////////////////////////////////////////////
//
//	vp_FPWeaponReloader.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	this component adds firearm reload logic, sound, animation and
//					reload duration to an FPWeapon. it doesn't handle ammo max caps
//					or levels. instead this should be governed by an inventory
//					system via the event handler
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;


[RequireComponent(typeof(vp_FPWeapon))]

public class vp_FPWeaponReloader : vp_WeaponReloader
{

	public Animation AnimationPlayer ;

	public AnimationClip AnimationReload;

	public AnimationClip BeginReload;
	public AnimationClip EndReload;


    public  float weightWeapon;
    public static float Factor;

	//De lam shotgun
	bool KeepReloading = false;

   
    protected override void Awake()
    {
        Factor = (1 - (weightWeapon / 30));
        //ReloadDuration = 2.0f;
        m_Audio = GetComponent<AudioSource>();

        // store the first player event handler found in the top of our transform hierarchy
        m_Player = (vp_PlayerEventHandler)transform.root.GetComponentInChildren(typeof(vp_PlayerEventHandler));

        
    }

	/// <summary>
	/// 
	/// </summary>
    protected override void Start()
	{
		base.Start ();
		AnimationPlayer = ((vp_FPWeapon)m_Weapon).WeaponModel.GetComponent<Animation> ();
	}
    
	/// <summary>
	/// this callback is triggered right after the 'Reload' activity
	/// has been approved for activation
	/// </summary>
	protected override void OnStart_Reload()
	{
		base.OnStart_Reload();
        m_Player.Attack.TryStop();
		//Horus begin
        if (GetComponent<CSVR_Snipper>() != null)
        {
            GetComponent<CSVR_Snipper>().OnStop_Zoom();
        }
        //if (FPWeaponShooter.ClassGunID.ToLower ().Contains ("shotgun")) 
        if (GunClassType == TypeGunClass.ShotGun)
        {
			// Nếu là lần nạp đạn đầu
			if (!KeepReloading) {
				if (BeginReload != null) {
					m_Player.Reload.AutoDuration = BeginReload.length;
					AnimationPlayer.CrossFade (BeginReload.name);

				}

			

				if (AnimationReload != null) {
					m_Player.Reload.AutoDuration += AnimationReload.length;
					//Debug.Log(m_Player.Reload.AutoDuration);
					AnimationPlayer.CrossFadeQueued(AnimationReload.name);				
				}



			}
			else {
				if (AnimationReload != null) {
					m_Player.Reload.AutoDuration = AnimationReload.length;
					AnimationPlayer.CrossFade(AnimationReload.name);				
				}
			}


		} else {
			// Nếu khong phai shotgun thi play animation nhu binh thuong
			if (AnimationReload == null)
				return;
			
			// if reload duration is zero, fetch duration from the animation
		
			m_Player.Reload.AutoDuration = AnimationReload.length;			
			AnimationPlayer.CrossFade(AnimationReload.name);

		}

		//Horus end

	}
    protected override void OnStop_Reload()
    {
        /*
        //Horus begin
        */
		if (!gameObject.GetActive())
            return;
        //if (FPWeaponShooter.ClassGunID.ToLower().Contains("shotgun"))
        if (GunClassType == TypeGunClass.ShotGun)
        {
          //  Debug.Log("thay dan shotgun");
			if (!m_Player.WaitingToAttack) {			
				m_Player.RefillOneBullet.Try();
			}

			if (m_Player.CurrentWeaponAmmoCount.Get () < m_Player.CurrentWeaponMaxAmmoCount.Get ()) {
				//Debug.Log ("vp_FPWeaponReloader.OnStop_Reload 1");
				if (m_Player.WaitingToAttack && m_Player.CurrentWeaponAmmoCount.Get () > 0) {
					//Debug.Log ("vp_FPWeaponReloader.OnStop_Reload 2");
					m_Player.WaitingToAttack = false;
					KeepReloading = false;
				} else {
				//	Debug.Log ("vp_FPWeaponReloader.OnStop_Reload 3");
					KeepReloading = true;
				}
			} else {
			//	Debug.Log ("vp_FPWeaponReloader.OnStop_Reload 4");
				KeepReloading = false;
			}

		} else {
			m_Player.RefillCurrentWeapon.Try();
			KeepReloading = false;
		}

		if (!KeepReloading) {
			if (EndReload != null) {
				AnimationPlayer.CrossFade(EndReload.name);				
			}
		}
		else {
			m_Player.Reload.TryStart ();
		}
		//Horus end  
    }
    
	//Horus begin
    
    //CSVR_FPWeaponShooter mFPWeaponShooter = null;
    //CSVR_FPWeaponShooter FPWeaponShooter {
    //    get {
    //        if (mFPWeaponShooter == null) {
    //            mFPWeaponShooter = GetComponent<CSVR_FPWeaponShooter> ();
    //        }

    //        return mFPWeaponShooter;
    //    }
    //}
    

    /*
    void Update()
    {
    /*	//Testing. Nên hạ time xuống để có slow motion
        if (AnimationPlayer.IsPlaying(BeginReload.name)){
            Debug.Log("BeginLoad playing");
        } 

        if (AnimationPlayer.IsPlaying(EndReload.name)){
            Debug.Log("EndReload playing");
        }

        if (AnimationPlayer.IsPlaying(AnimationReload.name)){
            Debug.Log("AnimationReload playing");
        }

    }
    
    */


    /// <summary>
	/// registers this component with the event handler (if any)
	/// </summary>
    protected override void OnEnable()
	{

		base.OnEnable ();
		KeepReloading = false;
		//mFPWeaponShooter = null;

	}




	//Horus end
     
}

