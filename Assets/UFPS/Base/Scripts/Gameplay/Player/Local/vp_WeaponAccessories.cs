﻿using UnityEngine;
using System.Collections;

public class vp_WeaponAccessories : MonoBehaviour {

	public Transform accessoriesScopeTransform = null;	

	public Transform accessoriesMagazineTransform = null;	

	public Transform accessoriesGripTransform = null;	

	public Transform accessoriesMuzzleTransform = null;	

	public Transform accessoriesSkinTransform = null;	

	public Transform accessoriesAccTransform = null;	

}
