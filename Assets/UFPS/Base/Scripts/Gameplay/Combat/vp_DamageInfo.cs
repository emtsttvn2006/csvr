/////////////////////////////////////////////////////////////////////////////////
//
//	vp_DamageInfo.cs
//	� VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	carries information about a single instance of damage done,
//					typically to a vp_DamageHandler-derived component. this class
//					is a long term work in progress
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;

public class vp_DamageInfo
{
	public float Damage;				// how much damage was done?
	public Transform Source;			// from what object did it come (directly)? common use: HUD / GUI
	public Transform OriginalSource;	// what object initially caused this to happen? common use: game logic, score
	public DamageType Type;				// what type of damage is this?
    public float DamageAmor;
	//Horus begin: Them thong tin ve vien dan
	public string ShooterPlayfabID = "";
	public string ShooterName = "";
	public string GunID = "";
    public string GunName = "";
    public string Level = "";
    public int idphoton = 0;
    public Vector3 vec;
    public int team;
	public int HitPart = 1; //0 - Head, 1 - Body
	//Horus end

	public enum DamageType
	{
		Unknown,
		KillZone,
		Fall,
		Impact,
		Bullet,
		Explosion,
		// the above are the types represented in the UFPS demo but can be easily
		// extended: e.g. blunt, electrical, cutting, piercing, freezing, crushing
		// drowning, gas, acid, freezing, burning, scolding, magical, plasma etc.
	}

	/// <summary>
	/// 
	/// </summary>
	public vp_DamageInfo(float damage, Transform source, DamageType type = DamageType.Unknown, string shooterPlayfabID = "", string shooterName = "", string gunID ="" , int hitPart = 1,string level="",int _team=0,string gunName="",int idPhotonView=0,Vector3 pos=new Vector3())
	{
        
		Damage = damage;
		Source = source;
		OriginalSource = source;
		Type = type;
		//Horus begin
		ShooterPlayfabID = shooterPlayfabID;
		ShooterName = shooterName;
		GunID = gunID;
		HitPart = hitPart;
        Level = level;
        team = _team;
        GunName = gunName;
        idphoton = idPhotonView;
        vec = pos;
		//Horus end
	}


	//Horus end


	/// <summary>
	/// 
	/// </summary>
	public vp_DamageInfo(float damage, Transform source, Transform originalSource, DamageType type = DamageType.Unknown)
	{
		Damage = damage;
		Source = source;
		OriginalSource = originalSource;
		Type = type;
	}


}

