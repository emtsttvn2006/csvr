///////////////////////////////////////////////////////////////////////////////////
////
////	vp_SimpleHUDMobile.cs
////	Â© VisionPunk. All Rights Reserved.
////	https://twitter.com/VisionPunk
////	http://www.visionpunk.com
////
////	description:	a version of the vp_SimpleHUD with a classic mobile FPS layout
////
///////////////////////////////////////////////////////////////////////////////////
//
//using UnityEngine;
//
//[RequireComponent(typeof(AudioSource))]
//
//public class vp_SimpleHUDMobile : MonoBehaviour
//{
//    protected vp_FPPlayerEventHandler m_PlayerEventHandler = null;
//    protected int m_Health
//    {
//        get
//        {
//            int health = (int)(m_PlayerEventHandler.Health.Get());
//            return health < 0 ? 0 : health;
//        }
//    }
//    /*
//    protected int m_Amor
//    {
//        get
//        {
//            int amor = (int)(m_PlayerEventHandler.Amor.Get());
//            return amor < 0 ? 0 : amor;
//        }
//    }
//
//    */
//    /// <summary>
//    ///
//    /// </summary>
//    void Awake()
//    {
//
//        m_PlayerEventHandler = transform.root.GetComponentInChildren<vp_FPPlayerEventHandler>();
//
//    }
//
//
//    /// <summary>
//    /// registers this component with the event handler (if any)
//    /// </summary>
//    void OnEnable()
//    {
//
//        if (m_PlayerEventHandler != null)
//            m_PlayerEventHandler.Register(this);
//
//    }
//
//
//    /// <summary>
//    /// unregisters this component from the event handler (if any)
//    /// </summary>
//    void OnDisable()
//    {
//
//
//        if (m_PlayerEventHandler != null)
//            m_PlayerEventHandler.Unregister(this);
//
//    }
//
//
//}
//
