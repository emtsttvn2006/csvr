/////////////////////////////////////////////////////////////////////////////////
//
//	vp_MPMaster.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	this component is king of the multiplayer game!
//					it manages overall game logic of the master client. implements
//					multiplayer game time cycles, assembles and broadcasts full
//					or partial game states with game phase, clock and player stats.
//					allocates team and initial spawnpoint to joining players, plus
//					broadcasts our own version of the simulation in case of a local
//					master client handover
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.UI;
using Horus;
using Horus.ClientModels;

public class vp_MPMaster : Photon.MonoBehaviour
{

	public List<CSVR_Victim> Victims;
	protected CSVR_Victim _victim;
	protected bool roomHidden = false;
	//hiệp đấu
	public static int Round = 0;

	//AI data
	public bool AIEnabled = false;
	public GameObject AIController;

	public CSVR_AITeam[] AITeamList;
	protected string EnemyName;
	protected CSVR_AIPlayerSetup EnemySetup;
	protected int maxCount = 0;
	protected GameObject[,] AIPInstances;

	//Ham nay de truyen thong tin cho tat ca nguoi choi, ke ca chinh object chay ham nay
	public void TransmitDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeam, int victimTeams,int idPhoton,int idPhotonVictim)
	{
//		Debug.Log ("TransmitDeathInfo " + shooter_PlayfabID + " " +shooter_Name + " with gun " + gunID + " killed " + victim_PlayfabID + " " + victim_Name + " part " + hitPart.ToString()  + " Damage " + Damage.ToString());
        photonView.RPC("ReceiveDeathInfo", PhotonTargets.All, shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeam, victimTeams, idPhoton, idPhotonVictim);
	}

	//Ham nhan thong tin ve 1 su kien nguoi chet
	// Cac xu ly khac nhu bang diem, medal, ... duoc xu ly o day
	[PunRPC]
    public virtual void ReceiveDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams, int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
//		Debug.Log (transform.name + "vp_MPMaster.ReceiveDeathInfo");
		try{
			if (gunID != "")
			{
//				Debug.Log (transform.name + "vp_MPMaster.ReceiveDeathInfo 1");
                CSVR_UIPlayManage.init.InitPlayerKill(shooter_Name, victim_Name, Resources.Load<Sprite>("Sprites/UIWeapon/" + gunID + "_icon"), hitPart, idTeams, victimTeams);
				CSVR_FPGetGun.getgun.TransmitKillerInfo(victim_PlayfabID, gunID, shooter_Name, level, nameWeapom, hitPart);
				CSVR_FPGetGun.getgun.ShowHuyHieu(hitPart, Damage, shooter_PlayfabID, victim_PlayfabID,gunID);
			}
		}catch{
		}
		//tattrycatch
	}
	
	//Horus end
	private float _gameLength;
	public float GameLength{
		get
		{
			return _gameLength = CSVR_GameSetting.timeMatch;
		}
		set
		{
			_gameLength = value;
		}
	}
	private string _currentLevel;
	public string CurrentLevel{
		get
		{
			return _currentLevel = CSVR_GameSetting.mapName;
		}
		set
		{
			_currentLevel = value;
		}
	}
	//public float GameLength = (5 * 60);		// default: 5 minutes
	public float PauseLength = 0;			// default: 20 seconds
	//public string CurrentLevel = System.String.Empty;		// current level loaded on the master and enforced on all clients. master will load this on login
	
	#if UNITY_EDITOR
	[vp_HelpBox("'CurrentLevel' will be loaded by the master on login. Joining players will fetch this string from the master and proceed to load the correct level. At any time, the master method 'TransmitLoadLevel' can be called to make everyone load a new level.", UnityEditor.MessageType.None, typeof(vp_MPMaster), null, false, vp_PropertyDrawerUtility.Space.Nothing)]
	public float currentLevelHelp;
	#endif
	
	protected bool m_TookOverGame = false;	// will always be false as long as we're a regular client. will be true if we
	// join as master, or if we become master (as soon as our first full game
	// state has been broadcast) and will never go false again in the same game
	
	public static vp_MPMaster m_Instance;
	public static vp_MPMaster Instance
	{
		get
		{
			if (m_Instance == null)
				m_Instance = Component.FindObjectOfType<vp_MPMaster>();
			return m_Instance;
		}
	}
	
	public enum GamePhase
	{
		NotStarted,
		Playing,
		BetweenGames
	}
	public static GamePhase Phase = GamePhase.NotStarted;
	
	protected static Dictionary<Transform, int> m_ViewIDsByTransform = new Dictionary<Transform, int>();
	protected static Dictionary<int, Transform> m_TransformsByViewID = new Dictionary<int, Transform>();
	
	
	/// <summary>
	/// 
	/// </summary>
	protected virtual void Awake()
	{
		DeactivateOtherMasters();
		
		
	}
	
	/// <summary>
	/// 
	/// </summary>
	protected virtual void OnEnable()
	{
		isLoaded = false;
		IsEndGame = false;
		roomHidden = false;
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	protected virtual void OnDisable()
	{

	}
	
	
	/// <summary>
	/// 
	/// </summary>
	protected virtual void Update()
	{
		
		// set game over
		if ((Phase == GamePhase.Playing) && (vp_MPClock.Running == false))
			StopGame();
		
		// SNIPPET: here is an example of loading a level on the master. all clients
		// will automatically load the new level and the game mode will reset
		//if (Input.GetKeyUp(KeyCode.L))
		//	TransmitLoadLevel(CurrentLevel);	// 'CurrentLevel' can be changed to the name of any level that has been added (!) to the Build Settings
		
		// start new game
		if ((Phase == GamePhase.BetweenGames) && (vp_MPClock.Running == false))
		{
			ResetGame();
			StartGame();
		}
		
		if (m_TookOverGame && (PhotonNetwork.connectionStateDetailed != PeerState.Joined))
			m_TookOverGame = false;
		
	}
	
	
	/// <summary>
	/// respawns all players, unfreezes the local player, restarts
	/// the game clock and broadcasts the game state
	/// </summary>
	public virtual void StartGame()
	{
		Debug.Log ("vp_MPMaster.StartGame");
		
		if (!PhotonNetwork.isMasterClient)
			return;

		Phase = GamePhase.Playing;
		
		vp_MPNetworkPlayer.TransmitRespawnAll();
		
		vp_MPNetworkPlayer.TransmitUnFreezeAll();
		

		vp_MPClock.Set(GameLength);

		TransmitGameState();

	}

	//RoomVIP: xử lý độ trễ do người chơi thứ nhất vào sau vài giây
	public virtual void ResetClock() {
		if (!PhotonNetwork.isMasterClient)
			return;

		vp_MPClock.Set(GameLength + 2); //Cong them thoi gian tre

		TransmitGameState();
	}

	/// <summary>
	/// freezes the local player, pauses game time and broadcasts
	/// the game state
	/// </summary>
	public virtual void StopGame()
	{
		
		if (!PhotonNetwork.isMasterClient)
			return;
		
		photonView.RPC("ReceiveFreeze", PhotonTargets.All);
		
		Phase = GamePhase.BetweenGames;
		vp_MPClock.Set(PauseLength);
		
		TransmitGameState();
		
	}
	
	
	/// <summary>
	/// restores health, shots, inventory and death on all players
	/// </summary>
	public void ResetGame()
	{
		
		vp_MPPlayerStats.FullResetAll();
		
	}
	
	
	/// <summary>
	/// caches and returns the photonview id of the given transform.
	/// ids are stored in a dictionary that resets on level load
	/// </summary>
	public static int GetViewIDOfTransform(Transform t)
	{
		
		if (t == null)
			return 0;
		
		int id = 0;
		
		if (!m_ViewIDsByTransform.TryGetValue(t, out id))
		{
			PhotonView p = t.GetComponent<PhotonView>();
			if (p != null)
				id = p.viewID;
			m_ViewIDsByTransform.Add(t, id);	// add (even if '0' to prevent searching again)
		}
		
		return id;
		
	}
	
	
	/// <summary>
	/// caches and returns the transform of the given photonview id.
	/// transforms are stored in a dictionary that resets on level load
	/// </summary>
	public static Transform GetTransformOfViewID(int id)
	{
		
		Transform t = null;
		
		if (!m_TransformsByViewID.TryGetValue(id, out t))
		{
			foreach (PhotonView p in FindObjectsOfType<PhotonView>())
			{
				if (p.viewID == id)
				{
					t = p.transform;
					m_TransformsByViewID.Add(id, p.transform);
					return p.transform;
				}
			}
			m_TransformsByViewID.Add(id, t);	// add (even if not found, to avoid searching again)
		}
		
		return t;
		
	}
	
	
	/// <summary>
	/// pushes the master client's version of the game state and all
	/// player stats onto another client. if 'player' is null, the game
	/// state will be pushed onto _all_ other clients. 'gameState' can
	/// optionally be provided for cases where only a partial game state
	/// (a few stats on a few players) needs to be sent. by default the
	/// method will assemble and broadcast all stats of all players.
	/// </summary>


	public void TransmitGameState(PhotonPlayer targetPlayer, ExitGames.Client.Photon.Hashtable gameState = null)
	{
//		Debug.Log ("TransmitGameState");
		if (!PhotonNetwork.isMasterClient)
			return;

				
		//DumpGameState(gameState);
		
		// if no (partial) gamestate has been provided, assemble and
		// broadcast the entire gamestate
		if (gameState == null)
			gameState = AssembleGameState();
		
		//DumpGameState(gameState);
		
		if (targetPlayer == null)
		{
			//Debug.Log("sending to all" + Time.time);
			photonView.RPC("ReceiveGameState", PhotonTargets.Others, (ExitGames.Client.Photon.Hashtable)gameState);
		}
		else
		{
			//Debug.Log("sending to " + targetPlayer + "" + Time.time);
			photonView.RPC("ReceiveGameState", targetPlayer, (ExitGames.Client.Photon.Hashtable)gameState);
		}
		
		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();
		
	}
	
	
	/// <summary>
	/// pushes the master client's version of the game state and all
	/// player stats onto all other clients. 'gameState' can optionally
	/// be provided for cases where only a partial game state (a few
	/// stats on a few players) needs to be sent. by default the method
	/// will assemble and broadcast all stats of all players.
	/// </summary>
	public void TransmitGameState(ExitGames.Client.Photon.Hashtable gameState = null)
	{
		TransmitGameState(null, gameState);
	}
	
	
	/// <summary>
	/// broadcasts a game state consisting of a certain array of
	/// stats extracted from the specified players. parameters
	/// 2 and up should be strings identifying the included stats.
	/// the returned gamestate will report the same list of stat
	/// names for all players
	/// </summary>
	public void TransmitPlayerState(int[] playerIDs, params string[] stats)
	{
		
		if (!PhotonNetwork.isMasterClient)
			return;
		
		ExitGames.Client.Photon.Hashtable playerState = AssembleGameStatePartial(playerIDs, stats);
		if (playerState == null)
		{
			
			return;
		}
		
		photonView.RPC("ReceivePlayerState", PhotonTargets.Others, (ExitGames.Client.Photon.Hashtable)playerState);
		
	}
	
	
	/// <summary>
	/// broadcasts a game state consisting of an individual array
	/// of stats extracted from each specified player. parameters
	/// 2 and up should be arrays of strings identifying the stats
	/// included for each respective player. the returned gamestate
	/// may include unique stat names for each player
	/// </summary>
	public void TransmitPlayerState(int[] playerIDs, params string[][] stats)
	{
		
		if (!PhotonNetwork.isMasterClient)
			return;
		
		ExitGames.Client.Photon.Hashtable state = AssembleGameStatePartial(playerIDs, stats);
		if (state == null)
		{
			
			return;
		}
		
		photonView.RPC("ReceivePlayerState", PhotonTargets.Others, (ExitGames.Client.Photon.Hashtable)state);
		
	}
	
	
	/// <summary>
	/// the game state can only be pushed out by the master client.
	/// however it is stored and kept in sync across clients in the
	/// form of the properties on the actual network players.
	/// in case a client becomes master it pushes out a new game
	/// state based on the network players in its own scene
	/// </summary>
	protected virtual  ExitGames.Client.Photon.Hashtable AssembleGameState()
	{
		
		// NOTE: don't add custom integer keys, since ints are used
		// for player identification. for example, adding a key '5'
		// might result in a crash when player 5 tries to join.
		// adding string (or other type) keys should be fine
		
		if (!PhotonNetwork.isMasterClient)
			return null;
		
		vp_MPPlayerStats.EraseStats();	// NOTE: sending an RPC with a re-used gamestate will crash! we must create new gamestates every time
		
		vp_MPNetworkPlayer.RefreshPlayers();
		
		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();
		
		// -------- add game phase, game time and duration --------
		
		state.Add("Phase", Phase);
		state.Add("TimeLeft", vp_MPClock.TimeLeft);
		state.Add("Duration", vp_MPClock.Duration);
		
		//Horus begin
		//		state.Add("Team1KillCount", Team1KillCount);
		//		state.Add("Team2KillCount", Team2KillCount);
		//Horus end
		
		// -------- add the stats of all players (includes health) --------
		
		foreach (vp_MPNetworkPlayer player in vp_MPNetworkPlayer.Players.Values)
		{
			if (player == null)
				continue;
			// add a player stats hashtable with the key 'player.ID'
			ExitGames.Client.Photon.Hashtable stats = player.Stats.All;
			if (stats != null)
				state.Add(player.ID, stats);
		}
		
		// -------- add the health of all non-player damagehandlers --------
	/*	
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d is vp_PlayerDamageHandler)
				continue;
			if (d == null)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			// add the view id for a damagehandler photon view, along with its health.
			// NOTE: we send and unpack the view id negative since some will potentially
			// be the same as existing player id:s in the hashtable (starting at 1)
			state.Add(-p.viewID, d.CurrentHealth);
			
		}
		*/
		// -------- add note of any disabled pickups --------
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			if (vp_Utility.IsActive(p[0].transform.gameObject))
				continue;
			
			// there are two predicted cases were an ID might already be in the state:
			// 1) a player ID is the same as a pickup ID. this is highly unlikely since
			//		player IDs start at 1 and pickup IDs are ~six figure numbers. also,
			//		only currently disabled pickups are included in the state making it
			//		even more unlikely
			// 2: a pickup has two vp_ItemPickup components with the same ID which is
			//		the case with throwing weapons (grenades). this is highly likely,
			//		but in this case it's fine to ignore the ID second time around
			if (!state.ContainsKey(id))
				state.Add(id, false);
			
		}
		
		if (state.Count == 0)
			UnityEngine.Debug.LogError("Failed to get gamestate.");
		
		return state;
		
	}
	
	
	/// <summary>
	/// assembles a game state consisting of a certain array of
	/// stats extracted from the specified players. parameters
	/// 2 and up should be strings identifying the included stats.
	/// the returned gamestate will report the same array of stat
	/// names for all players
	/// </summary>
	protected virtual ExitGames.Client.Photon.Hashtable AssembleGameStatePartial(int[] playerIDs, params string[] stats)
	{
		
		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();
		
		for (int v = 0; v < playerIDs.Length; v++)
		{
			if (state.ContainsKey(playerIDs[v]))	// safety measure in case int array has duplicate id:s
			{
				
				continue;
			}
			state.Add(playerIDs[v], ExtractPlayerStats(vp_MPNetworkPlayer.Get(playerIDs[v]), stats));
		}
		
		return state;
		
	}
	
	
	/// <summary>
	/// assembles a game state consisting of an individual array
	/// of stats extracted from each specified player. parameters
	/// 2 and up should be arrays of strings identifying the stats
	/// included for each respective player. the returned gamestate
	/// may include unique stat names for every player
	/// </summary>
	protected virtual ExitGames.Client.Photon.Hashtable AssembleGameStatePartial(int[] playerIDs, params string[][] stats)
	{
		
		if (playerIDs.Length != stats.Length)
		{
			
			return null;
		}
		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();
		
		
		for (int v = 0; v < playerIDs.Length; v++)
		{
			if (state.ContainsKey(playerIDs[v]))	// safety measure in case int array has duplicate id:s
			{
				
				continue;
			}
			state.Add(playerIDs[v], ExtractPlayerStats(vp_MPNetworkPlayer.Get(playerIDs[v]), stats[v]));
			
		}
		
		return state;
		
	}
	
	
	/// <summary>
	/// creates a hashtable with only a few of the stats of a
	/// certain player
	/// </summary>
	protected virtual ExitGames.Client.Photon.Hashtable ExtractPlayerStats(vp_MPNetworkPlayer player, params string[] stats)
	{
		
		if (!PhotonNetwork.isMasterClient)
			return null;
		
		// create a player hashtable with only the given stats
		ExitGames.Client.Photon.Hashtable table = new ExitGames.Client.Photon.Hashtable();
		//string str = "Extracting stats for player: " + vp_MPNetworkPlayer.GetName(player.ID);
		foreach (string s in stats)
		{
			object o = player.Stats.Get(s);
			if (o == null)
			{
				
				continue;
			}
			table.Add(s, o);
			//str += ": " + s + "(" + o + ")";
		}
		
		//Debug.Log(str);
		
		return table;
		
	}
	
	
	/// <summary>
	/// broadcasts a command to load a new level on all clients (master
	/// included) then resets the game which will prompt all player stats
	/// to be reset and players to respawn on new spawnpoints.
	/// NOTES: 1) a scene being loaded must not have a vp_MPConnection
	/// in it. the connection component should be loaded in a startup
	/// scene with 'DontDestroyOnLoad' set to true. 2) the game clock will
	/// be reset as a direct result of loading a new level
	/// </summary>
	public void TransmitLoadLevel(string levelName)
	{
		
		if (!PhotonNetwork.isMasterClient)
			return;
		
		vp_MPMaster.Phase = vp_MPMaster.GamePhase.NotStarted;
		
		photonView.RPC("ReceiveLoadLevel", PhotonTargets.All, levelName);
		
	}
	
	
	/// <summary>
	/// sends a command to load a new level to a specific client. this is
	/// typically sent to players who join an existing game. if the joining
	/// player is the master, the game state will start up for the first time
	/// </summary>
	public void TransmitLoadLevel(PhotonPlayer targetPlayer, string levelName)
	{
		
		if (!PhotonNetwork.isMasterClient)
			return;
		
		if (string.IsNullOrEmpty(levelName))
		{
			return;
		}
		
		photonView.RPC("ReceiveLoadLevel", targetPlayer, levelName);
		
	}
	
	
	/// <summary>
	/// loads a new level on this machine as commanded by the master. the
	/// game mode will typically be reset by the master when this happens,
	/// prompting a player stat reset and respawn
	/// </summary>
	[PunRPC]
	public void ReceiveLoadLevel(string levelName, PhotonMessageInfo info)
	{
		
		if (info.sender != PhotonNetwork.masterClient)
			return;
		
		//        PhotonNetwork.isMessageQueueRunning = false;
		
		PhotonNetwork.LoadLevel(levelName);
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();

		if (Application.isLoadingLevel==false)
		{
			Debug.LogError("Error (" + this + ") Failed to load level: '" + levelName + "'. You may need to 1) add it to the Build Settings, or 2) verify the default level name on the master component.");
			return;
		}
		
		CurrentLevel = levelName;
		
		if (PhotonNetwork.isMasterClient)
		{
			ResetGame();
			StartGame();
		}
		
	}
	//	IEnumerator LoadNewScene(string levelName) {
	//		
	//		// This line waits for 3 seconds before executing the next line in the coroutine.
	//		// This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
	//		yield return new WaitForSeconds(3);
	//		
	//		// Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
	//		AsyncOperation async = Application.LoadLevelAsync(levelName);
	//		
	//		// While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
	//		while (!async.isDone) {
	//			yield return null;
	//		}
	//		PhotonNetwork.isMessageQueueRunning = false;
	//
	//	}
	
	public bool isLoaded = false;
	public bool IsEndGame = false;
	/// <summary>
	/// every client sends this RPC from 'vp_MPConnection -> 'OnJoinedRoom'.
	/// its purpose is to allocate a team and player type to the joinee, use
	/// this info to figure out a matching spawn point, and respond with a
	/// 'permission to spawn' at a certain position, with a certain team and
	/// a suitable player prefab
	/// </summary>
	[PunRPC]
	public void RequestInitialSpawnInfo(PhotonPlayer player, int id, string name)
	{
		//Truong 21042016
		IsEndGame = false;
		
		//PhotonNetwork.room.open = true;
		
		if (!PhotonNetwork.isMasterClient)
			return;
		// make every joining player load the current level
		TransmitLoadLevel(player, CurrentLevel);
		
		//        isLoaded = false;
		
		if (PhotonNetwork.room.playerCount > 1)
		{
			//Sua loi nguoi choi so 1 khoổiổing khoi tao duoc spawnpoint va load sung
//#if VIRTUAL_ROOM
			//TransmitInitialSpawnInfo(player, id, name);
//#else
			StartCoroutine(WaitAndPrint(player, id, name));
//#endif
		}
		else
		{
			// if we're the FIRST player in the game we take over the game, but
			// we must wait sending spawn info to ourself until 'OnLevelWasLoaded'
			// triggers (because there are no spawnpoints yet)
			m_TookOverGame = true;
		}
		
	}
	
	IEnumerator WaitAndPrint(PhotonPlayer player, int id, string name)
	{
		
		yield return new WaitForSeconds(0.1f);
		
		if (isLoaded)
		{
			TransmitInitialSpawnInfo(player, id, name);
		}
		else
			StartCoroutine(WaitAndPrint(player, id, name));
		
	}
	
	/// <summary>
	/// 
	/// </summary>
	public void TransmitInitialSpawnInfo(PhotonPlayer player, int id, string name)
	{

		// allocate team number, player type and spawnpoint
		int teamNumber = 0;
		string playerTypeName = null;
		vp_Placement placement = null;
		
		if (vp_MPTeamManager.Exists) {
			//teamNumber = ((vp_MPTeamManager.Instance.Teams.Count <= 1) ? 0 : vp_MPTeamManager.Instance.GetSmallestTeam());
			teamNumber = int.Parse (player.customProperties ["Team"].ToString ()) + 1;


			//Debug.Log ("Ten nhan vat " + playerTypeName);
		/*	if (string.IsNullOrEmpty (playerTypeName)) {
				playerTypeName = vp_MPTeamManager.Instance.GetTeamPlayerTypeName (teamNumber);
			} 
*/
			placement = vp_MPPlayerSpawner.GetRandomPlacement (vp_MPTeamManager.GetTeamName (teamNumber));

		} 
		
		if (placement == null)
			placement = vp_MPPlayerSpawner.GetRandomPlacement();

		playerTypeName = player.customProperties ["PlayerTypeName"].ToString ();

		//Kiem tra xem dung doi chua
		if (teamNumber != 0) {
			if (vp_MPTeamManager.Instance.FindTeamNumber (playerTypeName) != teamNumber) {
				if (teamNumber == 1) {
					playerTypeName = vp_MPTeamManager.Instance.FindPair (playerTypeName, 2, 1);
				} else {
					playerTypeName = vp_MPTeamManager.Instance.FindPair (playerTypeName, 1, 2);
				}
			}
		}

	/*	if (string.IsNullOrEmpty(playerTypeName))
		{
			vp_MPPlayerType playerType = vp_MPPlayerSpawner.GetDefaultPlayerType();
			if (playerType != null)
				playerTypeName = playerType.name;
		}*/

		//Cho room VIP
		if ( int.Parse(player.customProperties["Team"].ToString()) == 2) {
			teamNumber = 3;
		}
		// Tr 07.03.2016
		//int playerLevel = int.Parse(player.customProperties["Level"].ToString());
		//Debug.Log ("id " + id + " player " + player + " placement.Position " + placement.Position + " placement.Rotation " + " placement.Rotation " + placement.Rotation + " playerTypeName " + playerTypeName + " teamNumber " + teamNumber);
		// spawn
		photonView.RPC("ReceiveInitialSpawnInfo", PhotonTargets.All, id, player, placement.Position, placement.Rotation, playerTypeName, teamNumber);

		// if JOINING player is the master, refresh the game clock since
		// there are no other players and the game needs to get started
		if (player.isMasterClient)
			StartGame();
		
		// send the entire game state to the joining player
		// NOTE: we don't need to send the game state of the joinee to all
		// the other players since it has just spawned in the form of a
		// fresh, clean copy of the remote player prefab in question
		TransmitGameState(player);
	}

	/// <summary>
	/// the master client sends this RPC to push its version of the game
	/// state onto all other clients. 'game state' can mean the current
	/// game time & phase + all stats of all players, or it can mean a
	/// partial game state, such as an updated score + frag count for a
	/// sole player. also, instantiates any missing player prefabs
	/// </summary>
	[PunRPC]
	protected virtual void ReceiveGameState(ExitGames.Client.Photon.Hashtable gameState, PhotonMessageInfo info)
	{
		
		//vp_MPDebug.Log("GOT FULL GAMESTATE!");
		
		//DumpGameState(gameState);
		
		if ((info.sender != PhotonNetwork.masterClient) ||
		    (info.sender.isLocal))
			return;
		
		//vp_MPDebug.Log("Gamestate updated @ " + info.timestamp);
		//Debug.Log("Gamestate updated @ " + info.timestamp);
		
		// -------- extract game phase, game time and duration --------
		
		// TODO: make generic method 'ExtractStat' that does this
		object phase;
		if ((gameState.TryGetValue("Phase", out phase) && (phase != null)))
			Phase = (GamePhase)phase;
		
		object timeLeft;
		object duration;
		if ((gameState.TryGetValue("TimeLeft", out timeLeft) && (timeLeft != null))
		    && (gameState.TryGetValue("Duration", out duration) && (duration != null)))
			vp_MPClock.Set((float)timeLeft, (float)duration);
		
		//Horus begin
		//		object Team1KillCount_temp;
		//		if ((gameState.TryGetValue("Team1KillCount", out Team1KillCount_temp) && (Team1KillCount_temp != null)))
		//			Team1KillCount = (int)Team1KillCount_temp;
		//
		//		object Team2KillCount_temp;
		//		if ((gameState.TryGetValue("Team2KillCount", out Team2KillCount_temp) && (Team2KillCount_temp != null)))
		//			Team2KillCount = (int)Team2KillCount_temp;
		//
		
		//Horus end
		// -------- instantiate missing player prefabs --------
		
		vp_MPPlayerSpawner.Instance.InstantiateMissingPlayerPrefabs(gameState);
		
		// -------- refresh stats of all players --------
		
		ReceivePlayerState(gameState, info);
		
		// -------- refresh health of all non-player damage handlers --------
	/*	
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d == null)
				continue;
			if (d is vp_PlayerDamageHandler)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			object currentHealth;
			if (gameState.TryGetValue(-p.viewID, out currentHealth) && (currentHealth != null))
			{
				d.CurrentHealth = (float)currentHealth;
				if (d.CurrentHealth <= 0.0f)
					vp_Utility.Activate(d.gameObject, false);
			}
		}
		*/
		// -------- disable any pickups noted as currently disabled in the state --------
		
		//vp_MPDebug.Log("DISABLED PICKUPS: " + vp_MPPickupManager.Instance.Pickups.Keys.Count);
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			object isDisabled;
			if (gameState.TryGetValue(id, out isDisabled) && (isDisabled != null))
				vp_Utility.Activate(p[0].transform.gameObject, false);
			
		}
		
		// -------- refresh all teams --------
		
		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();
		
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	[PunRPC]
	protected virtual void ReceivePlayerState(ExitGames.Client.Photon.Hashtable gameState, PhotonMessageInfo info)
	{
		
		//Debug.Log("GOT PLAYER STATE!");
		
		if ((info.sender != PhotonNetwork.masterClient) ||
		    (info.sender.isLocal))
			return;
		
		// -------- refresh stats of all included players --------
		
		foreach (vp_MPNetworkPlayer player in vp_MPNetworkPlayer.Players.Values)
		{
			
			if (player == null)
				continue;
			
			object stats;
			if (gameState.TryGetValue(player.ID, out stats) && (stats != null))
				player.Stats.SetFromHashtable((ExitGames.Client.Photon.Hashtable)stats);
			//else
			//    vp_MPDebug.Log("Failed to extract player " + player.ID + " stats from gamestate");
			
		}
		
		// -------- refresh all teams --------
		
		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();
		
	}
	
	
	/// <summary>
	/// disarms, stops and locks the local player so that it
	/// cannot move. used when starting non-gameplay game phases,
	/// such as between deathmatch games
	/// </summary>
	[PunRPC]
	protected virtual void ReceiveFreeze(PhotonMessageInfo info)
	{
		
		if (!info.sender.isMasterClient)
			return;
		
		//vp_MPDebug.Log("Time's up! Restarting game ...");
		
		vp_MPLocalPlayer.Freeze();
		
	}
	
	
	/// <summary>
	/// allows local player to move again and tries to wield the
	/// first weapon. used when ending non-gameplay game phases,
	/// such as when starting a new deathmatch game
	/// </summary>
	[PunRPC]
	protected virtual void ReceiveUnFreeze(PhotonMessageInfo info)
	{
		
		if (!info.sender.isMasterClient)
			return;
		
		vp_MPLocalPlayer.UnFreeze();
		
	}
	
	
	/// <summary>
	/// dumps a game state hashtable to the console
	/// </summary>
	//    public static void DumpGameState(ExitGames.Client.Photon.Hashtable gameState)
	//    {
	//
	//        string s = "--- GAME STATE ---\n(click to view)\n\n";
	//
	//        if (gameState == null)
	//        {
	//
	//            gameState = AssembleGameState();
	//        }
	//
	//        foreach (object key in gameState.Keys)
	//        {
	//            if (key.GetType() == typeof(int))
	//            {
	//                object player;
	//                gameState.TryGetValue(key, out player);
	//                s += vp_MPNetworkPlayer.GetName((int)key) + ":\n";
	//
	//                foreach (object o in ((ExitGames.Client.Photon.Hashtable)player).Keys)
	//                {
	//                    s += "\t\t" + (o.ToString()) + ": ";
	//                    object val;
	//                    ((ExitGames.Client.Photon.Hashtable)player).TryGetValue(o, out val);
	//                    s += val.ToString().Replace("(System.String)", "") + "\n";
	//                }
	//            }
	//            else
	//            {
	//                object val;
	//                gameState.TryGetValue(key, out val);
	//                s += key.ToString() + ": " + val.ToString();
	//            }
	//            s += "\n";
	//        }
	//
	//    }
	
	
	/// <summary>
	/// detects and responds to a master client handover
	/// </summary>
	protected virtual void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		if (!IsEndGame) 
		{
			// refresh master control of every vp_MPRigidbody in the scene (done here
			// rather than in the objects themselves because we need to iterate any
			// deactivated / disabled ones too)
			vp_MPRigidbody.RefreshMasterControlAll();
			
			if (!PhotonNetwork.isMasterClient)
				return;
			
			// if this machine becomes master, broadcast our gamestate!
			if (!m_TookOverGame)
			{
				
				// reinitialize every respawner in this scene or anything not currently
				// alive will fail to respawn (respawn timer being local to prev master)
				vp_Respawner.ResetAll(true);	// give a new respawn time to anything waiting to respawn
				
				// force our gamestate onto everyone else
				TransmitGameState();
				
				// remember that we have taken over the game. this can only happen once
				m_TookOverGame = true;
				
			}
		}
		
	}
	
	//Horus begin
	void OnMasterClientSwitched() {
		
		if (!IsEndGame) {
//			if (PhotonNetwork.isMasterClient) {
//				Debug.Log (transform.name + "Master thay doi. Bay gio toi la master");
//			} else {
//				Debug.Log (transform.name + "Master thay doi. Bay gio toi khong la master");
//			}
			
			vp_Gameplay.isMaster = PhotonNetwork.isMasterClient;
			
			//Giong nhu khi Master disconnected;
			vp_MPRigidbody.RefreshMasterControlAll ();
			
			if (PhotonNetwork.isMasterClient) {
				
				// if this machine becomes master, broadcast our gamestate!
				if (!m_TookOverGame) {
					ErrorView.instance.CoutDownChangeMaster(5,"Đang chuyển kết nối trong ");
					// reinitialize every respawner in this scene or anything not currently
					// alive will fail to respawn (respawn timer being local to prev master)
					vp_Respawner.ResetAll (true);	// give a new respawn time to anything waiting to respawn
					
					// force our gamestate onto everyone else
					TransmitGameState ();
					
					// remember that we have taken over the game. this can only happen once
					m_TookOverGame = true;
					
				}
			}
		}
		//Debug.Log("vp_MPNetworkPlayer.RefreshPlayers from vp_MPMaster");
		//vp_MPNetworkPlayer.RefreshPlayers();
		//vp_Gameplay.isMaster = PhotonNetwork.isMasterClient;
		
	}
	
	//Horus end
	
	
	
	/// <summary>
	/// when a new level is loaded, clears any cached scene objects.
	/// also transmits initial spawn info to the first ever player
	/// </summary>
	public virtual void OnLevelWasLoaded(int id)
	{
		if (!IsEndGame) 
		{
			isLoaded = true;
			//        PhotonNetwork.isMessageQueueRunning = true;
		
			if (vp_MPNetworkPlayer.instance = null)
				return;
		
			// clear any cached objects from the previous scene
			m_ViewIDsByTransform.Clear ();
			m_TransformsByViewID.Clear ();
			vp_MPRigidbody.Instances.Clear ();
		

			#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && VIRTUAL_ROOM
			#else
			// the first player in the game must send itself spawn info here.
			// this can't be done in 'RequestInitialSpawnInfo' because the
			// spawnpoints are unknown until 'OnLevelWasLoaded'
			if ((!vp_MPNetworkPlayer.PlayersByID.ContainsKey (PhotonNetwork.player.ID)) && (PhotonNetwork.room.playerCount == 1)) {
				//			PhotonNetwork.isMessageQueueRunning = true;
				TransmitInitialSpawnInfo (PhotonNetwork.player, PhotonNetwork.player.ID, PhotonNetwork.player.name);
			}
			#endif
		}


		
	}

	protected virtual void EnterNextRound()
	{
	}

	public virtual void InstanceAI(){
		//AI prepration
		if (AIEnabled && PhotonNetwork.isMasterClient) 
		{
			AIController = Instantiate (Resources.Load ("AIController_"+CSVR_GameSetting.mapName) as GameObject);

			for(int i = 0; i < AITeamList.Length; i++)
			{
				if(AITeamList[i].TeamCount > maxCount)
				{
					maxCount = AITeamList [i].TeamCount;
				}
			}

			AIPInstances = new GameObject[AITeamList.Length, maxCount];
			int k = 0;
			for(int i = 0; i < AITeamList.Length; i++)
			{
				if (AITeamList [i].TeamCount < 1)
					continue;
				for(int j = 0; j < AITeamList[i].TeamCount; j++)
				{
					EnemyName = AITeamList[i].AvailableCharacterName[Random.Range (0, AITeamList[i].AvailableCharacterName.Length)];
					if (EnemyName == "")
						continue;
					AIPInstances[i, j] = (GameObject)PhotonNetwork.InstantiateSceneObject (EnemyName, Vector3.zero, Quaternion.identity, 0, null);
					EnemySetup = AIPInstances[i, j].GetComponent<CSVR_AIPlayerSetup> ();
					EnemySetup.TeamNumber = CSVR_GameSetting.AI_InGame [k].AI_TeamNumber;
					EnemySetup.allyTeamIDs = AITeamList [i].AllyTeamID;
					EnemySetup.enemyTeamIDs = AITeamList [i].EnemyTeamID;
					EnemySetup.SpawnPointTag = AITeamList [i].SpawnPointTag;
					EnemySetup.CurrentName = CSVR_GameSetting.AI_InGame [k].AI_Name;
					EnemySetup.CurrentID = CSVR_GameSetting.AI_InGame [k].AI_ID;
					EnemySetup.transform.name = CSVR_GameSetting.AI_InGame [k].AI_Name;
					CSVR_GameSetting.AI_InGame [k].AI_Weapon = EnemySetup.machineGunsAI [Random.Range (0, EnemySetup.machineGunsAI.Length)].GunName;
					//CSVR_GameSetting.AI_InGame [k].ai_
					//EnemySetup.machineGunsAI
					CSVR_UIPlay.instance.PlayerJoined( CSVR_GameSetting.AI_InGame [k].AI_Name);
					k++;
				}
			}
		}
	}

	
	/// <summary>
	/// makes sure this game state is the only one operating on this
	/// scene. for when using multiple game mode master prefabs in a
	/// scene and potentially forgetting to having just one enabled
	/// </summary>
	protected virtual void DeactivateOtherMasters()
	{
		
		if (!enabled)
			return;
		
		vp_MPMaster[] masters = Component.FindObjectsOfType<vp_MPMaster>() as vp_MPMaster[];
		foreach (vp_MPMaster g in masters)
		{
			if (g.gameObject != gameObject)
				vp_Utility.Activate(g.gameObject, false);	// there can be only one!
		}
		
	}
	
	
	/// <summary>
	/// dumps all network players to the console
	/// </summary>
	protected virtual void DebugDumpPlayers()
	{
		
		string debugMsg = "Players (excluding self): ";
		
		for (int p = 0; p < PhotonNetwork.playerList.Length; p++)
		{
			if (PhotonNetwork.playerList[p].ID == PhotonNetwork.player.ID)
				continue;
			
			PhotonView[] views = Component.FindObjectsOfType(typeof(PhotonView)) as PhotonView[];
			bool hasView = false;
			foreach (PhotonView f in views)
			{
				
				if (f.ownerId == PhotonNetwork.playerList[p].ID)
					hasView = true;
				
			}
			
			debugMsg += PhotonNetwork.playerList[p].ID.ToString() + (hasView ? " (has view)" : " (has no view)") + ", ";
			
		}
		
		if (debugMsg.Contains(", "))
			debugMsg = debugMsg.Remove(debugMsg.LastIndexOf(", "));
		
	}

	protected virtual  CSVR_UIAIInfo GetAi(string id)
	{
		foreach (CSVR_UIAIInfo a in CSVR_GameSetting.AI_InGame)
		{
			if (a.AI_ID == id)
			{
				return a;
			}
		}

		return null;
	}

	public virtual void Endgame()
	{
		Invoke("GoHome", 5f);
		FinishGame();
	}
	public virtual void GoHome()
	{
		HomeScreen.states = States.EndGame;
		PhotonNetwork.LoadLevel("GameOver");
	}

	protected List<GameRewardsPlayerInfo> playersRW;
	public virtual void FinishGame()
	{		
	}

	protected virtual void OnGameReward(){
		HorusClientAPI.GameRewards(
			new GameRewardsRequest(){
				apiKey = HorusManager.instance.userAPIKey,
				mode = CSVR_GameSetting.modeName,
				map = CSVR_GameSetting.mapName,
				players = playersRW
			}, 
			(result) => {
				//Debug.Log(Horus.Json.JsonWrapper.SerializeObject (result, Horus.Internal.HorusUtil.ApiSerializerStrategy));

				for(int i = 0; i < result.players.Length; i++){
					foreach(PhotonPlayer player in PhotonNetwork.playerList){
						if (result.players[i].name == player.customProperties ["UserName"].ToString ()) {
							ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
							properties.Add("Coin", result.players[i].coin);
							properties.Add("Gold", result.players[i].gold);
							properties.Add("Exp", result.players[i].exp);
							properties.Add("Level", result.players[i].level);
							//properties.Add("MainGun",CSVR_GameSetting.mainGun);

							List<string> keysName = new List<string>();
							if(result.players[i].items.Length > 0){
								for(int k = 0; k < result.players[i].items.Length; k++){
									//se con cac item khac su li sau, hien tai chi su li key lat bai
									if(result.players[i].items[k].CatalogVersion == GameConstants.VNITEMCATALOG_Key){
										keysName.Add(result.players[i].items[k].ItemId);
									}
								}
							}
							string str_Keys = Horus.Json.JsonWrapper.SerializeObject (keysName, Horus.Internal.HorusUtil.ApiSerializerStrategy);
							properties.Add("KeyList",str_Keys );
							player.SetCustomProperties(properties);
						}
					}
				}
			},
			(error) => {
				//ErrorView.instance.ShowPopupError ("Không lấy được dữ liệu cá nhân");
			}
		);
	}

	public virtual void CloseRoomVIP()
	{		
		Phase = GamePhase.NotStarted;
		IsEndGame = true;
		Endgame();
	}

}

[System.Serializable]
public class CSVR_AITeam
{
	public string TeamName;
	public int UniqueTeamID;
	[Range(0,8)]public int TeamCount = 3;
	public string[] AvailableCharacterName;
	public int[] AllyTeamID;
	public int[] EnemyTeamID;
	public string SpawnPointTag;
}