/////////////////////////////////////////////////////////////////////////////////
//
//	vp_DMMaster.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	an example of how to extend the base (vp_MPMaster) class
//					with a call to show the deathmatch scoreboard when the game
//					pauses on end-of-match, and to restore it when game resumes
//
//					TIP: study the base class to learn how the game state works
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Horus;
using Horus.ClientModels;

public class vp_AITDMMaster : vp_MPMaster
{
	
	public static  int Team1KillCount = 0;
	public static int Team2KillCount = 0;

	private bool roomVipClose = false;

	protected override void OnDisable()
	{
		base.OnDisable ();
		Team1KillCount = 0;
		Team2KillCount = 0;
		roomVipClose = false;
		if(Victims != null)
		{
			Victims.Clear ();
		}

	}
	

	protected override void  Update()
	{

		//Xử lý ẩn room ở đây
		if (Phase == GamePhase.Playing && !roomHidden && vp_MPClock.TimeLeft < 150f )
		{
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
			roomHidden = true;
		}
#if VIRTUAL_ROOM
		if (Phase == GamePhase.Playing && !roomVipClose && roomHidden && PhotonNetwork.playerList.Length == 1)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			roomVipClose = true;
			Endgame();
		}
#endif
	//	if (CSVR_GetInfo.instance != null)
		//{
		//CSVR_GameSetting.DebugLog ("Team1KillCount  "+Team1KillCount+"Team2KillCount  "+Team2KillCount);
			if ((Phase == GamePhase.Playing) && (Team2KillCount == CSVR_GameSetting.maxSkill || Team1KillCount == CSVR_GameSetting.maxSkill) && !IsEndGame)
			{
			
				vp_Timer.DestroyAll();
				Phase = GamePhase.NotStarted;
				IsEndGame = true;
				Endgame();
			}
		//}
		
		if ((Phase == GamePhase.Playing) && (vp_MPClock.Running == false || vp_MPClock.TimeLeft < 0) && !IsEndGame)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			Endgame();
		}

		if (m_TookOverGame && (PhotonNetwork.connectionStateDetailed != PeerState.Joined))
			m_TookOverGame = false;
		
	}


	[PunRPC]
	public override void ReceiveDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		base.ReceiveDeathInfo (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);

//		SetScoreByMPNP (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);

		SetScore (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);

	}

	//	cách set score dựa trên MPNetworkPlayer của nạn nhân và người bắn
	void SetScoreByMPNP(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get (idPhotonVictim);
		vp_MPNetworkPlayer source = vp_MPNetworkPlayer.Get(idPhoton);

		if (source == null)
			return;
		print ("Player kill player");
		if (target != source)
		{
			if ((target.TeamNumber != source.TeamNumber)					// inter-team kill
				|| ((target.TeamNumber == 0) && (source.TeamNumber == 0))	// or there are no teams!
			)
			{

				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + 1);
				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);

				if (idTeams == 1) {
					Team1KillCount++;
				} else if (idTeams == 2) {
					Team2KillCount++;
				}
			}
			else	// intra-team kill
			{

				// you loose one 'Score' for every friendly kill
				// the teammate's stats are not affected
				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);

				if (idTeams == 1) {
					Team2KillCount++;
				} else if (idTeams == 2) {
					Team1KillCount++;
				}

			}
		}
		// killing yourself shall always award one 'Death' and minus one 'Score'
		else
		{
			//tự sát
			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);

			if (idTeams == 1) {
				Team2KillCount++;
			} else if (idTeams == 2) {
				Team1KillCount++;
			}
		}

		if (target != source)	// kill
		{
			TransmitPlayerState(new int[] { target.ID, source.ID },
				new string[] { "Deaths", "Score" },
				new string[] { "Frags", "Score" });
		}
		else	// suicide
		{
			TransmitPlayerState(new int[] { target.ID },
				new string[] { "Deaths", "Score" });
		}
		//thêm nạn nhân vào list nạn nhân để hiện thị khi chết.
		if (shooter_PlayfabID == HorusManager.instance.playerID) {
			if(Victims == null){
				Victims = new List<CSVR_Victim> ();
			}
			_victim = new CSVR_Victim ();
			_victim.Name = victim_Name;
			_victim.Level = level;
			_victim.Heath = ((int)Damage).ToString ();
			Victims.Add (_victim);
		}
	}


//	cách set score dựa trên ID team, id Photon của nạn nhân và người bắn
	void SetScore(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		//Debug.Log ("shooter_PlayfabID " + shooter_Name + " idPhoton " + idPhoton + " victim_PlayfabID " + victim_Name + " idPhotonVictim " + idPhotonVictim);
		//neu la tu sat
		if (idPhoton == idPhotonVictim)
		{
			SetPlayerScore (shooter_PlayfabID,idPhoton,victim_PlayfabID, idPhotonVictim, -1, 1);
			if (idTeams == 1)
				Team2KillCount++;
			if (idTeams == 2)
				Team1KillCount++;
		}
		else
		{
			SetPlayerScore (shooter_PlayfabID,idPhoton,victim_PlayfabID, idPhotonVictim, 1, 1);
			if (idTeams == 1)
				Team1KillCount++;
			if (idTeams == 2)
				Team2KillCount++;
		}
		if (shooter_PlayfabID == HorusManager.instance.playerID) {
			if(Victims == null){
				Victims = new List<CSVR_Victim> ();
			}
			_victim = new CSVR_Victim ();
			_victim.Name = victim_Name;
			_victim.Level = level;
			_victim.Heath = ((int)Damage).ToString ();
			Victims.Add (_victim);
		}
	}

//	set diem so tren scoreboard
	void SetPlayerScore(string shooter_PlayfabID, int idPhoton,string victim_PlayfabID, int idPhotonVictim, int frags, int deaths)
	{
		vp_MPNetworkPlayer source = vp_MPNetworkPlayer.Get(idPhoton);
		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get (idPhotonVictim);
		CSVR_UIAIInfo aiSource = GetAi (shooter_PlayfabID);
		CSVR_UIAIInfo aiTarget = GetAi (victim_PlayfabID);

		if(source != null && !shooter_PlayfabID.Contains("BKMAI_"))
		{
			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + frags);
		}

		if(aiSource != null && shooter_PlayfabID.Contains("BKMAI_"))
		{
			aiSource.AI_Kill += frags;
		}

		if(target != null && !victim_PlayfabID.Contains("BKMAI_"))
		{
			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + deaths);
		}

		if(aiTarget != null && victim_PlayfabID.Contains("BKMAI_"))
		{
			aiTarget.AI_Death += deaths;
		}

		if (source != null && target != null && !shooter_PlayfabID.Contains("BKMAI_") && !victim_PlayfabID.Contains("BKMAI_"))
		{
			if (target != source)	// kill
			{
				TransmitPlayerState(new int[] { target.ID, source.ID },
					new string[] { "Deaths", "Score" },
					new string[] { "Frags", "Score" });
			}
			else	// suicide
			{
				TransmitPlayerState(new int[] { target.ID },
					new string[] { "Deaths", "Score" });
			}
		}
	}

	//end kien

	protected override  ExitGames.Client.Photon.Hashtable AssembleGameState()
	{
		
		// NOTE: don't add custom integer keys, since ints are used
		// for player identification. for example, adding a key '5'
		// might result in a crash when player 5 tries to join.
		// adding string (or other type) keys should be fine
		
		if (!PhotonNetwork.isMasterClient)
			return null;
		
		vp_MPPlayerStats.EraseStats();	// NOTE: sending an RPC with a re-used gamestate will crash! we must create new gamestates every time
		
		vp_MPNetworkPlayer.RefreshPlayers();
		
		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();
		
		// -------- add game phase, game time and duration --------
		
		state.Add("Phase", Phase);
		state.Add("TimeLeft", vp_MPClock.TimeLeft);
		state.Add("Duration", vp_MPClock.Duration);
		
		//Horus begin
		state.Add("Team1KillCount", Team1KillCount);
		state.Add("Team2KillCount", Team2KillCount);
		//Horus end
		
		// -------- add the stats of all players (includes health) --------
		
		foreach (vp_MPNetworkPlayer player in vp_MPNetworkPlayer.Players.Values)
		{
			if (player == null)
				continue;
			// add a player stats hashtable with the key 'player.ID'
			ExitGames.Client.Photon.Hashtable stats = player.Stats.All;
			if (stats != null)
				state.Add(player.ID, stats);
		}
		
		// -------- add the health of all non-player damagehandlers --------
		/*
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d is vp_PlayerDamageHandler)
				continue;
			if (d == null)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			// add the view id for a damagehandler photon view, along with its health.
			// NOTE: we send and unpack the view id negative since some will potentially
			// be the same as existing player id:s in the hashtable (starting at 1)
			state.Add(-p.viewID, d.CurrentHealth);
				
		}
*/
		// -------- add note of any disabled pickups --------
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			if (vp_Utility.IsActive(p[0].transform.gameObject))
				continue;
			
			// there are two predicted cases were an ID might already be in the state:
			// 1) a player ID is the same as a pickup ID. this is highly unlikely since
			//		player IDs start at 1 and pickup IDs are ~six figure numbers. also,
			//		only currently disabled pickups are included in the state making it
			//		even more unlikely
			// 2: a pickup has two vp_ItemPickup components with the same ID which is
			//		the case with throwing weapons (grenades). this is highly likely,
			//		but in this case it's fine to ignore the ID second time around
			if (!state.ContainsKey(id))
				state.Add(id, false);
			
		}
		
		//if (state.Count == 0)
		//    UnityEngine.Debug.LogError("Failed to get gamestate.");
		
		return state;
		
	}
	
	[PunRPC]
	protected override void ReceiveGameState(ExitGames.Client.Photon.Hashtable gameState, PhotonMessageInfo info)
	{
		
		//vp_MPDebug.Log("GOT FULL GAMESTATE!");
		
		//DumpGameState(gameState);
		
		if ((info.sender != PhotonNetwork.masterClient) ||
		    (info.sender.isLocal))
			return;
		
		//vp_MPDebug.Log("Gamestate updated @ " + info.timestamp);
		//Debug.Log("Gamestate updated @ " + info.timestamp);
		
		// -------- extract game phase, game time and duration --------
		
		// TODO: make generic method 'ExtractStat' that does this
		object phase;
		if ((gameState.TryGetValue("Phase", out phase) && (phase != null)))
			Phase = (GamePhase)phase;
		
		object timeLeft;
		object duration;
		if ((gameState.TryGetValue("TimeLeft", out timeLeft) && (timeLeft != null))
		    && (gameState.TryGetValue("Duration", out duration) && (duration != null)))
			vp_MPClock.Set((float)timeLeft, (float)duration);
		
		//Horus begin
		object Team1KillCount_temp;
		if ((gameState.TryGetValue("Team1KillCount", out Team1KillCount_temp) && (Team1KillCount_temp != null)))
			Team1KillCount = (int)Team1KillCount_temp;
		
		object Team2KillCount_temp;
		if ((gameState.TryGetValue("Team2KillCount", out Team2KillCount_temp) && (Team2KillCount_temp != null)))
			Team2KillCount = (int)Team2KillCount_temp;
		
		
		//Horus end
		// -------- instantiate missing player prefabs --------
		
		vp_MPPlayerSpawner.Instance.InstantiateMissingPlayerPrefabs(gameState);
		
		// -------- refresh stats of all players --------
		
		ReceivePlayerState(gameState, info);
		
		// -------- refresh health of all non-player damage handlers --------
		/*
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d == null)
				continue;
			if (d is vp_PlayerDamageHandler)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			object currentHealth;
			if (gameState.TryGetValue(-p.viewID, out currentHealth) && (currentHealth != null))
			{
				d.CurrentHealth = (float)currentHealth;
				if (d.CurrentHealth <= 0.0f)
					vp_Utility.Activate(d.gameObject, false);
			}
		}
*/
		// -------- disable any pickups noted as currently disabled in the state --------
		
		//vp_MPDebug.Log("DISABLED PICKUPS: " + vp_MPPickupManager.Instance.Pickups.Keys.Count);
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			object isDisabled;
			if (gameState.TryGetValue(id, out isDisabled) && (isDisabled != null))
				vp_Utility.Activate(p[0].transform.gameObject, false);
			
		}
		
		// -------- refresh all teams --------
		
		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();
		
	}
		


	public override void FinishGame()
	{

		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{
			try{
				if (System.String.Compare(vp_MPNetworkPlayer.GetName(p.photonView.ownerId), AccountManager.instance.displayName, true) == 0)
				{
					int teamWin = Team1KillCount > Team2KillCount ? 1 : 2;
					int	newFrags1 = int.Parse(p.Stats.Get("Frags").ToString()) ;
					int	newDeaths1 = int.Parse(p.Stats.Get("Deaths").ToString());

					if (p.TeamNumber == teamWin){
						CSVR_UIPlayManage.init.MissionSuccess();
					}else{
						CSVR_UIPlayManage.init.MissionFaile();
					}
					ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
					properties.Add("KDA", newFrags1 + "/" + newDeaths1);
					properties.Add("MainGun",CSVR_GameSetting.mainGun);
					properties.Add("EndGame", (p.TeamNumber == teamWin) ? "Thắng" :  "Thua");
					PhotonNetwork.player.SetCustomProperties(properties);
				}
			}catch{
			}
		}
	}

}
