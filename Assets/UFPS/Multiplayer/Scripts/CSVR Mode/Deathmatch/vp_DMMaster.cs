/////////////////////////////////////////////////////////////////////////////////
//
//	vp_DMMaster.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	an example of how to extend the base (vp_MPMaster) class
//					with a call to show the deathmatch scoreboard when the game
//					pauses on end-of-match, and to restore it when game resumes
//
//					TIP: study the base class to learn how the game state works
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Horus;
using Horus.ClientModels;

public class vp_DMMaster : vp_MPMaster
{
	private bool roomVipClose = false;
	protected override void OnDisable()
	{
		roomVipClose = false;
		if(Victims != null){
			Victims.Clear ();
		}
	}
	protected override void  Update()
	{
		if (Phase == GamePhase.Playing && !roomHidden && vp_MPClock.TimeLeft < 150f )
		{
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
			roomHidden = true;
		}
#if VIRTUAL_ROOM
		if (Phase == GamePhase.Playing && !roomVipClose && roomHidden && PhotonNetwork.playerList.Length == 1)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			roomVipClose = true;
			Endgame();
		}
#endif
		if ((Phase == GamePhase.Playing) && (vp_MPClock.Running == false || vp_MPClock.TimeLeft < 0) && !IsEndGame)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			Endgame();
		}

		if (m_TookOverGame && (PhotonNetwork.connectionStateDetailed != PeerState.Joined))
			m_TookOverGame = false;
		
	}

	[PunRPC]
    public override void ReceiveDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idphotonVictim, PhotonMessageInfo info)
	{
		base.ReceiveDeathInfo(shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams, victimTeams,idPhoton,idphotonVictim, info);

		//SetScore (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);
		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get(idphotonVictim);
		vp_MPNetworkPlayer source = vp_MPNetworkPlayer.Get(idPhoton);
		CSVR_UIAIInfo aiSource = GetAi (shooter_PlayfabID);
		CSVR_UIAIInfo aiTarget = GetAi (victim_PlayfabID);


		if(source != null && !shooter_PlayfabID.Contains("BKMAI_"))
		{
			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + 1);
		}

		if(aiSource != null && shooter_PlayfabID.Contains("BKMAI_"))
		{
			aiSource.AI_Kill += 1;
		}

		if(target != null && !victim_PlayfabID.Contains("BKMAI_"))
		{
			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
		}

		if(aiTarget != null && victim_PlayfabID.Contains("BKMAI_"))
		{
			aiTarget.AI_Death += 1;
		}

		if (source != null && target != null && !shooter_PlayfabID.Contains("BKMAI_") && !victim_PlayfabID.Contains("BKMAI_"))
		{
			if (target != source)	// kill
			{
				TransmitPlayerState(new int[] { target.ID, source.ID },
					new string[] { "Deaths", "Score" },
					new string[] { "Frags", "Score" });
			}
			else	// suicide
			{
				TransmitPlayerState(new int[] { target.ID },
					new string[] { "Deaths", "Score" });
			}
		}

//		if (target != source)
//		{
//			if ((target.TeamNumber != source.TeamNumber)					// inter-team kill
//			    || ((target.TeamNumber == 0) && (source.TeamNumber == 0))	// or there are no teams!
//			    )
//			{
//				
//				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + 1);
//				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
//			}
//			else	// intra-team kill
//			{
//				
//				// you loose one 'Score' for every friendly kill
//				// the teammate's stats are not affected
//				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
//				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
//				
//			}
//		}
//		// killing yourself shall always award one 'Death' and minus one 'Score'
//		else
//		{
//			//tự sát
//			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
//			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
//		}
//		
//		if (target != source)	// kill
//		{
//			TransmitPlayerState(new int[] { target.ID, source.ID },
//			new string[] { "Deaths", "Score" },
//			new string[] { "Frags", "Score" });
//		}
//		else	// suicide
//		{
//			TransmitPlayerState(new int[] { target.ID },
//			new string[] { "Deaths", "Score" });
//		}

		//thêm nạn nhân vào list nạn nhân để hiện thị khi chết.
		if (shooter_PlayfabID == HorusManager.instance.playerID) {
			if(Victims == null){
				Victims = new List<CSVR_Victim> ();
			}
			_victim = new CSVR_Victim ();
			_victim.Name = victim_Name;
			_victim.Level = level;
			_victim.Heath = ((int)Damage).ToString ();
			Victims.Add (_victim);
		}

        
	}

	public override void InstanceAI(){
		//AI prepration
		if (AIEnabled && PhotonNetwork.isMasterClient) 
		{
			AIController = Instantiate (Resources.Load ("AIController_"+CSVR_GameSetting.mapName) as GameObject);

			for(int i = 0; i < AITeamList.Length; i++)
			{
				if(AITeamList[i].TeamCount > maxCount)
				{
					maxCount = AITeamList [i].TeamCount;
				}
			}

			AIPInstances = new GameObject[AITeamList.Length, maxCount];
			int k = 0;
			for(int i = 0; i < AITeamList.Length; i++)
			{
				if (AITeamList [i].TeamCount < 1)
					continue;
				for(int j = 0; j < AITeamList[i].TeamCount; j++)
				{
					EnemyName = AITeamList[i].AvailableCharacterName[Random.Range (0, AITeamList[i].AvailableCharacterName.Length)];
					if (EnemyName == "")
						continue;
					AIPInstances[i, j] = (GameObject)PhotonNetwork.InstantiateSceneObject (EnemyName, Vector3.zero, Quaternion.identity, 0, null);
					EnemySetup = AIPInstances[i, j].GetComponent<CSVR_AIPlayerSetup> ();
					EnemySetup.TeamNumber = 0;
					EnemySetup.allyTeamIDs = AITeamList [i].AllyTeamID;
					EnemySetup.enemyTeamIDs = AITeamList [i].EnemyTeamID;
					EnemySetup.SpawnPointTag = AITeamList [i].SpawnPointTag;
					EnemySetup.CurrentName = CSVR_GameSetting.AI_InGame [k].AI_Name;
					EnemySetup.CurrentID = CSVR_GameSetting.AI_InGame [k].AI_ID;
					EnemySetup.transform.name = CSVR_GameSetting.AI_InGame [k].AI_Name;
					CSVR_GameSetting.AI_InGame [k].AI_Weapon = EnemySetup.machineGunsAI [Random.Range (0, EnemySetup.machineGunsAI.Length)].GunName;
					//Yo.Log ("log weapon", EnemySetup.machineGunsAI[Random.Range (0, EnemySetup.machineGunsAI.Length)].GunName);
					CSVR_UIPlay.instance.PlayerJoined( CSVR_GameSetting.AI_InGame [k].AI_Name);

					k++;
				}
			}
		}
	}

	public override void FinishGame()
	{
		playersRW = new List<GameRewardsPlayerInfo> ();
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{
			if (PhotonNetwork.isMasterClient) {
				foreach(PhotonPlayer player in PhotonNetwork.playerList){
					if (player.ID == p.ID) {
						GameRewardsPlayerInfo playerRW = new GameRewardsPlayerInfo();
						playerRW.name = player.customProperties ["UserName"].ToString ();
						playerRW.kill = int.Parse(p.Stats.Get("Frags").ToString());
						playerRW.death = int.Parse(p.Stats.Get("Deaths").ToString());
						playerRW.headshot = int.Parse(p.Stats.Get ("HeadShot").ToString());
						playerRW.win = ((playerRW.kill - playerRW.death) > 0) ? 1 : 0;
						playersRW.Add (playerRW);
						break;
					}
				}
			}
			try{
				if (System.String.Compare(vp_MPNetworkPlayer.GetName(p.photonView.ownerId), AccountManager.instance.displayName, true) == 0)
				{
					
					int	newFrags1 = int.Parse(p.Stats.Get("Frags").ToString()) ;
					int	newDeaths1 = int.Parse(p.Stats.Get("Deaths").ToString());
					int teamWin = ((newFrags1 - newDeaths1) > 0) ? 1 : 0;

					if (teamWin == 1){
						CSVR_UIPlayManage.init.MissionSuccess();
					}else{
						CSVR_UIPlayManage.init.MissionFaile();
					}
					ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
					properties.Add("KDA", newFrags1 + "/" + newDeaths1);
					properties.Add("MainGun",CSVR_GameSetting.mainGun);
					properties.Add("EndGame", (teamWin == 1) ? "Thắng" :  "Thua");
					PhotonNetwork.player.SetCustomProperties(properties);
				}
			}catch{
			}
		}
		if (PhotonNetwork.isMasterClient) {
			OnGameReward ();
		}
	}
}
