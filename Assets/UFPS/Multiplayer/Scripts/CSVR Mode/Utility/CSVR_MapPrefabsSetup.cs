﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_MapPrefabsSetup : MonoBehaviour {

	[System.Serializable]
	public class MapPrefabs{
		public string PrefabName;
		public Vector3 PrefabPosition;
		public Vector3 PrefabRotation;
		public Vector3 PrefabScale;
	}
	
	[System.Serializable]
	public class MapUsed{
		public string MapSceneName = "";
		public List<MapPrefabs> Prefabs = new List<MapPrefabs>();
		public void InstantiateMapPrefabs(){
			foreach (MapPrefabs lala in Prefabs){
//				print ("tao prefab " + lala.PrefabName);
				try{
					if(lala.PrefabName != ""){
						GameObject prefab = Instantiate(Resources.Load ("Mode/"+lala.PrefabName, typeof(GameObject))) as GameObject;
						prefab.transform.position = lala.PrefabPosition;
						prefab.transform.localRotation = Quaternion.Euler(lala.PrefabRotation);
						prefab.transform.localScale = lala.PrefabScale;
					}
				}
				catch{
					continue;
				}

			}
		}
	}

	public List<MapUsed> MapList = new List<MapUsed>();
	void OnLevelWasLoaded(int level){
//		print (Application.loadedLevelName);
		foreach (MapUsed lele in MapList) {
			if(lele.MapSceneName == Application.loadedLevelName)
//			print ("bat dau instant prefab");
				lele.InstantiateMapPrefabs ();
		}
			
	}

}
