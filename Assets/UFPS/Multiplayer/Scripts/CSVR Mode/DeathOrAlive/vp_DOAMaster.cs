using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Horus;
using Horus.ClientModels;

//mode sinh tử
public class vp_DOAMaster : vp_MPMaster
{

	private  int Team1WinCount = 0;
	private  int Team2WinCount = 0;

	//đếm thành viên trong team đã chết
	private  int Team1PlayerDead = 1;
	private  int Team2PlayerDead = 1;

	//biến check trạng thái team 1 hay 2 thua
	private  bool Team1Death = false;
	private  bool Team2Death = false;
	//biến check trạng thái ReceiveGameState ở máy không phải master, tính điểm Team1WinCount, Team2WinCount trên local
	private  bool isReceiveGameState = false;
	private bool roomVipClose = false;
	private void ResetMode(){
		//reset các biến trong mode
		CSVR_GameSetting.policeWin = 0;
		CSVR_GameSetting.terroristWin = 0;
		Team1WinCount = 0;
		Team2WinCount = 0;

		Team1PlayerDead = 1;
		Team2PlayerDead = 1;

		Round = 0;

		Team1Death = false;
		Team2Death = false;
		isReceiveGameState = false;
		roomVipClose = false;
		if(Victims != null){
			Victims.Clear ();
		}
		//vp_Timer.DestroyAll();
	}
	protected override void OnDisable(){

		base.OnDisable ();

		ResetMode ();

	}

	protected override void  Update()
	{
		//Xử lý ẩn room ở đây
		if (Phase == GamePhase.Playing && !roomHidden && (Team1WinCount > 3 || Team2WinCount > 3) )
		{
//		if (Phase == GamePhase.Playing && !roomHidden && vp_MPClock.TimeLeft < 20f )
//		{
			//Khong phai master cung dong, de phong master bi treo
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
			roomHidden = true;
		}
#if VIRTUAL_ROOM
		if (Phase == GamePhase.Playing && !roomVipClose && roomHidden && PhotonNetwork.playerList.Length == 1)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			roomVipClose = true;
			Endgame();
		}
#endif
		//trận hết giờ thì hoặc 1 trong 2 team thắng hoặc 
		if ((Phase == GamePhase.Playing) && ((vp_MPClock.Running == false || Team1Death || Team2Death)) && !IsEndGame){
			
			if (!PhotonNetwork.isMasterClient)
				return;

			photonView.RPC("Receive_EndRound", PhotonTargets.All, Team1Death, Team2Death);
		}
		// start new game
		if ((Phase == GamePhase.BetweenGames) && (vp_MPClock.Running == false))
		{
			ResetGame();
			StartGame();
		}

		if (m_TookOverGame && (PhotonNetwork.connectionStateDetailed != PeerState.Joined))
			m_TookOverGame = false;

	}
	public override void StartGame() {
		base.StartGame ();
		CearWeapon ();

	}
	void CearWeapon(){
		foreach (GameObject w in  GameObject.FindGameObjectsWithTag ("Weapon")) {
			//Debug.Log ("Tim thay va destroy weapon");
			DestroyImmediate (w);
		}
	}
	[PunRPC]
	private void Receive_EndRound(bool _Team1Death,bool _Team2Death){
		Team1Death = _Team1Death;
		Team2Death = _Team2Death;

		//lấy team của player
		int team = int.Parse (PhotonNetwork.player.customProperties ["Team"].ToString ());

		if (Team1Death) {
			Team2WinCount++;

			if (team == 0) {
				CSVR_UIPlayManage.init.MissionFaile ();
			} else {
				CSVR_UIPlayManage.init.MissionSuccess ();
			}
		} else {
			Team1WinCount++;

			if (team == 1) {
				CSVR_UIPlayManage.init.MissionFaile ();
			} else {
				CSVR_UIPlayManage.init.MissionSuccess ();
			}
		}

		Team1PlayerDead = 1;
		Team2PlayerDead = 1;
		Team1Death = false;
		Team2Death = false;
		//cộng điểm win cho team thắng HUD
		CSVR_GetInfo.instance.DrawSearchAnDestroy (Team1WinCount.ToString (), Team2WinCount.ToString ());

		if (Team1WinCount == CSVR_GameSetting.maxWin || Team2WinCount == CSVR_GameSetting.maxWin)
		{
			IsEndGame = true;
			Endgame();
		}else{
			//clear victims in new round
			if (Victims != null) {
				Victims.Clear ();
			}
			StopGame();
		}

	}

	public override void StopGame()
	{

		if (!PhotonNetwork.isMasterClient)
			return;

		//photonView.RPC("ReceiveFreeze", PhotonTargets.All);

		Phase = GamePhase.BetweenGames;
		vp_MPClock.Set(PauseLength);

		TransmitGameState();

	}
		
	[PunRPC]
	public override void ReceiveDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		base.ReceiveDeathInfo (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);

		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get (idPhotonVictim);
		if (target == null)
			return;

		if (victim_PlayfabID == HorusManager.instance.playerID) {
			CSVR_UIPlayManage.init.InfoKiller.SetActive (true);
			CSVR_UIPlay.instance.UIPlayState (false);
			vp_Timer.In (3, delegate() {
				CSVR_UIPlayManage.init.InfoKiller.SetActive (false);
				CSVR_UIPlay.instance.UI_OnDisable ();
				if (CSVR_UIKillCam.instance == null) {
					GameObject instance = Instantiate (Resources.Load ("UI/CSVR_KillCam") as GameObject);
					instance.transform.localScale = Vector3.one;
					instance.GetComponent<CSVR_UIKillCam> ().Provide = target.gameObject.transform;
					instance.transform.position = new Vector3(target.gameObject.transform.position.x,target.gameObject.transform.position.y+3,target.gameObject.transform.position.z);
					int team = int.Parse (PhotonNetwork.player.customProperties ["Team"].ToString ());
					if (team == 0) {
						instance.GetComponent<CSVR_UIKillCam> ().TagTargets = "PolicePlayer";
					} else {
						instance.GetComponent<CSVR_UIKillCam> ().TagTargets = "TerrorisPlayer";
					}
					instance.gameObject.SetActive (true);
				} else {
					CSVR_UIKillCam.instance.UI_OnEnable ();
				}
			});
		}

		if (target.TeamNumber == 1) {
			//nếu thành viên trong team chưa chết hết 
			//Debug.Log("Team size = "+vp_MPTeamManager.Instance.GetTeamSize(target.Team));
			if (Team1PlayerDead < vp_MPTeamManager.Instance.GetTeamSize(target.Team)) {
				Team1PlayerDead++;
			}else {
				// nếu trận vẫn đang chơi và team 2 chưa thua
				if(Phase == GamePhase.Playing && !Team2Death){
					Team1Death = true;
				}
			}
		} else {
			if (Team2PlayerDead < vp_MPTeamManager.Instance.GetTeamSize(target.Team) ) {
				Team2PlayerDead++;
			}else {
				if(Phase == GamePhase.Playing && !Team1Death){
					Team2Death = true;
				}
			}
		}

		vp_MPNetworkPlayer source = vp_MPNetworkPlayer.Get(idPhoton);
		if (source == null)
			return;

		if (target != source)
		{
			if ((target.TeamNumber != source.TeamNumber)					// inter-team kill
				|| ((target.TeamNumber == 0) && (source.TeamNumber == 0))	// or there are no teams!
			)
			{
				// giết địch được +1 kill và điểm chết của địch +1
				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + 1);
				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
			}
			else
			{
				// giết đồng đội bị -1 kill và điểm chết đồng đội +1
				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
			}
		}
		else
		{
			//tự sát bị -1 kill và chết +1
			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);

		}

		if (target != source)	// kill
		{
			TransmitPlayerState(new int[] { target.ID, source.ID },
				new string[] { "Deaths", "Score" },
				new string[] { "Frags", "Score" });
		}
		else	// suicide
		{
			TransmitPlayerState(new int[] { target.ID },
				new string[] { "Deaths", "Score" });
		}

		//thêm nạn nhân vào list nạn nhân để hiện thị khi chết.
		if (shooter_PlayfabID == HorusManager.instance.playerID) {
			if(Victims == null){
				Victims = new List<CSVR_Victim> ();
			}
			_victim = new CSVR_Victim ();
			_victim.Name = victim_Name;
			_victim.Level = level;
			_victim.Heath = ((int)Damage).ToString ();
			Victims.Add (_victim);
		}



		//tattrycatch
	}

	protected override  ExitGames.Client.Photon.Hashtable AssembleGameState()
	{

		// NOTE: don't add custom integer keys, since ints are used
		// for player identification. for example, adding a key '5'
		// might result in a crash when player 5 tries to join.
		// adding string (or other type) keys should be fine

		if (!PhotonNetwork.isMasterClient)
			return null;

		vp_MPPlayerStats.EraseStats();	// NOTE: sending an RPC with a re-used gamestate will crash! we must create new gamestates every time

		vp_MPNetworkPlayer.RefreshPlayers();

		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();

		// -------- add game phase, game time and duration --------

		state.Add("Phase", Phase);
		state.Add("TimeLeft", vp_MPClock.TimeLeft);
		state.Add("Duration", vp_MPClock.Duration);

		//Horus begin
		state.Add("Team1WinCount", Team1WinCount);
		state.Add("Team2WinCount", Team2WinCount);
		Round = Team1WinCount + Team2WinCount;
		//Horus end
		// -------- add the stats of all players (includes health) --------

		foreach (vp_MPNetworkPlayer player in vp_MPNetworkPlayer.Players.Values)
		{
			if (player == null)
				continue;
			// add a player stats hashtable with the key 'player.ID'
			ExitGames.Client.Photon.Hashtable stats = player.Stats.All;
			if (stats != null)
				state.Add(player.ID, stats);
		}

		// -------- add the health of all non-player damagehandlers --------
		/*
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d is vp_PlayerDamageHandler)
				continue;
			if (d == null)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			// add the view id for a damagehandler photon view, along with its health.
			// NOTE: we send and unpack the view id negative since some will potentially
			// be the same as existing player id:s in the hashtable (starting at 1)
			state.Add(-p.viewID, d.CurrentHealth);

		}
*/
		// -------- add note of any disabled pickups --------

		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{

			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;

			if (vp_Utility.IsActive(p[0].transform.gameObject))
				continue;

			// there are two predicted cases were an ID might already be in the state:
			// 1) a player ID is the same as a pickup ID. this is highly unlikely since
			//		player IDs start at 1 and pickup IDs are ~six figure numbers. also,
			//		only currently disabled pickups are included in the state making it
			//		even more unlikely
			// 2: a pickup has two vp_ItemPickup components with the same ID which is
			//		the case with throwing weapons (grenades). this is highly likely,
			//		but in this case it's fine to ignore the ID second time around
			if (!state.ContainsKey(id))
				state.Add(id, false);

		}

		//if (state.Count == 0)
		//    UnityEngine.Debug.LogError("Failed to get gamestate.");

		return state;

	}

	[PunRPC]
	protected override void ReceiveGameState(ExitGames.Client.Photon.Hashtable gameState, PhotonMessageInfo info)
	{
		if ((info.sender != PhotonNetwork.masterClient) ||
			(info.sender.isLocal))
			return;

		//vp_MPDebug.Log("Gamestate updated @ " + info.timestamp);
		//Debug.Log("Gamestate updated @ " + info.timestamp);

		// -------- extract game phase, game time and duration --------

		// TODO: make generic method 'ExtractStat' that does this
		object phase;
		if ((gameState.TryGetValue("Phase", out phase) && (phase != null)))
			Phase = (GamePhase)phase;

		object timeLeft;
		object duration;
		if ((gameState.TryGetValue("TimeLeft", out timeLeft) && (timeLeft != null))
			&& (gameState.TryGetValue("Duration", out duration) && (duration != null)))
			vp_MPClock.Set((float)timeLeft, (float)duration);

		if (!isReceiveGameState) 
		{
			//Horus begin
			object Team1WinCount_temp;
			if ((gameState.TryGetValue ("Team1WinCount", out Team1WinCount_temp) && (Team1WinCount_temp != null))){
				Team1WinCount = (int)Team1WinCount_temp;

			}

			object Team2WinCount_temp;
			if ((gameState.TryGetValue ("Team2WinCount", out Team2WinCount_temp) && (Team2WinCount_temp != null))){
				Team2WinCount = (int)Team2WinCount_temp;
			}

			//HUD
			CSVR_GameSetting.policeWin = Team1WinCount;
			CSVR_GameSetting.terroristWin = Team2WinCount;
			if(CSVR_GetInfo.instance != null){
				CSVR_GetInfo.instance.DrawSearchAnDestroy(Team1WinCount.ToString(),Team2WinCount.ToString());
			}
		}

		//Horus end
		// -------- instantiate missing player prefabs --------

		vp_MPPlayerSpawner.Instance.InstantiateMissingPlayerPrefabs(gameState);

		// -------- refresh stats of all players --------

		ReceivePlayerState(gameState, info);

		// -------- refresh health of all non-player damage handlers --------
		/*
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d == null)
				continue;
			if (d is vp_PlayerDamageHandler)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			object currentHealth;
			if (gameState.TryGetValue(-p.viewID, out currentHealth) && (currentHealth != null))
			{
				d.CurrentHealth = (float)currentHealth;
				if (d.CurrentHealth <= 0.0f)
					vp_Utility.Activate(d.gameObject, false);
			}
		}
*/
		// -------- disable any pickups noted as currently disabled in the state --------

		//vp_MPDebug.Log("DISABLED PICKUPS: " + vp_MPPickupManager.Instance.Pickups.Keys.Count);

		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{

			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;

			object isDisabled;
			if (gameState.TryGetValue(id, out isDisabled) && (isDisabled != null))
				vp_Utility.Activate(p[0].transform.gameObject, false);

		}

		// -------- refresh all teams --------

		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();

		isReceiveGameState = true;
		Round = Team1WinCount + Team2WinCount;

	}

	public override void FinishGame()
	{
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{

			if (PhotonNetwork.isMasterClient) {
				foreach(PhotonPlayer player in PhotonNetwork.playerList){
					if (player.ID == p.ID) {
						GameRewardsPlayerInfo playerRW = new GameRewardsPlayerInfo();
						playerRW.name = player.customProperties ["UserName"].ToString ();
						playerRW.kill = int.Parse(p.Stats.Get("Frags").ToString());
						playerRW.death = int.Parse(p.Stats.Get("Deaths").ToString());
						playerRW.headshot = int.Parse(p.Stats.Get ("HeadShot").ToString());
						playerRW.win = ((Team1WinCount > Team2WinCount ? 1 : 2) == p.TeamNumber) ? 1 : 0;
						playersRW.Add (playerRW);
						break;
					}
				}
			}
			try{
				if (System.String.Compare(vp_MPNetworkPlayer.GetName(p.photonView.ownerId), AccountManager.instance.displayName, true) == 0)
				{
					int teamWin = Team1WinCount > Team1WinCount ? 1 : 2;
					int	newFrags1 = int.Parse(p.Stats.Get("Frags").ToString()) ;
					int	newDeaths1 = int.Parse(p.Stats.Get("Deaths").ToString());

					if (p.TeamNumber == teamWin){
						CSVR_UIPlayManage.init.MissionSuccess();
					}else{
						CSVR_UIPlayManage.init.MissionFaile();
					}
					ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
					properties.Add("KDA", newFrags1 + "/" + newDeaths1);
					properties.Add("MainGun",CSVR_GameSetting.mainGun);
					properties.Add("EndGame", (p.TeamNumber == teamWin) ? "Thắng" :  "Thua");
					PhotonNetwork.player.SetCustomProperties(properties);
				}
			}catch{
			}
		}
		if (PhotonNetwork.isMasterClient) {
			OnGameReward ();
		}
	}

	protected override void OnPhotonPlayerDisconnected(PhotonPlayer player)
	{
		base.OnPhotonPlayerDisconnected (player);

		//thêm trường họp chỉ còn nguoười trong phòng thì thằng đó win.
		if (PhotonNetwork.playerList.Length < 2 && (Team1WinCount > 2|| Team2WinCount > 2)) {

			// ẩn room
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
			roomHidden = true;
			// thoát trận
			IsEndGame = true;
			Endgame();
		}
	}

}
