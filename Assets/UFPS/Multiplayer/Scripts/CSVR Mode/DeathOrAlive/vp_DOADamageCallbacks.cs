﻿/////////////////////////////////////////////////////////////////////////////////
//
//	vp_DMDamageCallbacks.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	an example of how to extend the base (vp_MPDamageCallbacks)
//					class with additional callback logic for 'Damage' events. here,
//					we refresh the 'Deaths', 'Frags' and 'Score' stats declared in
//					vp_DMPlayerStats every time a player dies, and broadcast a new
//					gamestate (with these stats only) to reflect it on all machines.
//
//					TIP: study the base class to see how the 'TransmitKill' callback works
//					
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class vp_DOADamageCallbacks : vp_MPDamageCallbacks
{
	
	/// <summary>
	/// 
	/// </summary>
	protected override void TransmitDamage(Transform targetTransform, Transform sourceTransform, float damage)
	{
		
		if (!PhotonNetwork.isMasterClient)
			return;
		
		if (damage <= 0.0f)
			return;
		
		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get(targetTransform);
		if ((target == null)									// if target is an object (not a player) ...
		    || (target.DamageHandler.CurrentHealth > 0.0f))		// ... or it's a player that was only damaged (not killed) ...
		{
			// ... transmit a simple health update (update remote clients with
			// its health from the master scene) and bail out
			base.TransmitDamage(targetTransform, sourceTransform, damage);
		}
	}
	protected override void OnEnable()
	{
		vp_GlobalEvent<Transform, Transform, float>.Register("TransmitDamage", TransmitDamage);		// sent by vp_DamageHandler
		vp_GlobalEvent<Transform>.Register("TransmitKill", TransmitKill);							// sent by vp_DamageHandler and vp_PlayerDamageHandler
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	protected override void OnDisable()
	{
		vp_GlobalEvent<Transform, Transform, float>.Unregister("TransmitDamage", TransmitDamage);
		vp_GlobalEvent<Transform>.Unregister("TransmitKill", TransmitKill);

	}

	[PunRPC]
	public override void ReceiveObjectRespawn(int viewId, Vector3 position, Quaternion rotation, PhotonMessageInfo info)
	{
		//Debug.Log ("ReceiveObjectRespawn");
		if (info.sender != PhotonNetwork.masterClient)
			return;
		
		vp_Respawner r = GetRespawnerOfViewID(viewId);
		if ((r == null) || (r is vp_PlayerRespawner))
			return;
		
		// make object temporarily invisible so we don't see it 'pos-lerping'
		// across the map to its respawn position
		if (r.Renderer != null)
			r.Renderer.enabled = false;
		
		//r.Respawn();
		
		// restore visibility in half a sec
//		if (r.Renderer != null)
//			vp_Timer.In(0.5f, () => { r.Renderer.enabled = true; });
//		
	}
}
