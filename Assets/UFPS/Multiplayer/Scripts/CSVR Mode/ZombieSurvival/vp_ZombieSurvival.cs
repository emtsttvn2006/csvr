/////////////////////////////////////////////////////////////////////////////////
//
//	vp_DMMaster.cs
//	© VisionPunk. All Rights Reserved.
//	https://twitter.com/VisionPunk
//	http://www.visionpunk.com
//
//	description:	an example of how to extend the base (vp_MPMaster) class
//					with a call to show the deathmatch scoreboard when the game
//					pauses on end-of-match, and to restore it when game resumes
//
//					TIP: study the base class to learn how the game state works
//
/////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Horus;
using Horus.ClientModels;

public class vp_ZombieSurvival : vp_MPMaster
{
	[Range(1, 31)] public int MaxRound = 4;
	public int RoundsPassed;
	[Range(1, 31)] public int RoundStart = 1;

	public int RoundBestRecorded;
	public int LivesLeft = 3;
	public HardLevel hardLevel = HardLevel.EASY;

	private bool roomVipClose = false;

	protected override void OnDisable()
	{
		base.OnDisable ();
		RoundsPassed = 0;
		LivesLeft = 3;
		roomVipClose = false;
		if(Victims != null)
		{
			Victims.Clear ();
		}
	}

	protected override void  Update()
	{

		//Xử lý ẩn room ở đây
		if (Phase == GamePhase.Playing && !roomHidden && vp_MPClock.TimeLeft > 0 )
		{
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
			roomHidden = true;
		}
#if VIRTUAL_ROOM
		if (Phase == GamePhase.Playing && !roomVipClose && roomHidden && PhotonNetwork.playerList.Length == 1)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			roomVipClose = true;
			Endgame();
		}
#endif

		if (!IsEndGame && (Phase == GamePhase.Playing) && (vp_MPClock.Running == false || vp_MPClock.TimeLeft < 0))
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			Endgame();
		}


		if (!IsEndGame && (RoundsPassed == MaxRound || LivesLeft == 0))
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			Endgame();
		}


		if (m_TookOverGame && (PhotonNetwork.connectionStateDetailed != PeerState.Joined))
			m_TookOverGame = false;
		
	}

	protected override void EnterNextRound()
	{
		if(CSVR_UIPlayManage.init == null || CSVR_GetInfo.instance == null)
			return;
		CSVR_UIPlayManage.init.DisableMission ();
		CSVR_GetInfo.instance.DrawZombieSurvival (RoundsPassed.ToString(), LivesLeft.ToString());
		if(RoundsPassed < MaxRound)
			CSVR_GetInfo.instance.NewRound (RoundsPassed);
	}
		
	[PunRPC]
	private void Receive_EndRound(bool _Team1Death,bool _Team2Death)
	{
		RoundsPassed += 1;
		TransmitGameState();
		Invoke ("EnterNextRound", PauseLength);
		if(CSVR_UIPlayManage.init == null || CSVR_GetInfo.instance == null)
			return;
		CSVR_UIPlayManage.init.MissionSuccess ();
		CSVR_GetInfo.instance.DrawZombieSurvival (RoundsPassed.ToString(), LivesLeft.ToString());
	}


	[PunRPC]
	public override void ReceiveDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		SetScore (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);
	}

	void SetScore(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		Debug.Log ("shooter_PlayfabID " + shooter_Name + " idPhoton " + idPhoton + " victim_PlayfabID " + victim_Name + " idPhotonVictim " + idPhotonVictim);
		//neu la tu sat
		if (idPhoton == idPhotonVictim)
		{
			SetPlayerScore (shooter_PlayfabID,idPhoton,victim_PlayfabID, idPhotonVictim, 0, 1);
		}
		else
		{
			SetPlayerScore (shooter_PlayfabID,idPhoton,victim_PlayfabID, idPhotonVictim, 1, 1);
		}

		if (victim_PlayfabID == CSVR_ServerConnector.instance.PlayfabID)
		{
			LivesLeft--;
			CSVR_GetInfo.instance.DrawZombieSurvival (RoundsPassed.ToString(), LivesLeft.ToString());
		}
	
	}

	//	set diem so tren scoreboard
	void SetPlayerScore(string shooter_PlayfabID, int idPhoton,string victim_PlayfabID, int idPhotonVictim, int frags, int deaths)
	{
		vp_MPNetworkPlayer source = vp_MPNetworkPlayer.Get(idPhoton);
		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get (idPhotonVictim);
		CSVR_UIAIInfo aiSource = GetAi (shooter_PlayfabID);
		CSVR_UIAIInfo aiTarget = GetAi (victim_PlayfabID);

		if(source != null && !shooter_PlayfabID.Contains("BKMAI_"))
		{
			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + frags);
		}

		if(aiSource != null && shooter_PlayfabID.Contains("BKMAI_"))
		{
			aiSource.AI_Kill += frags;
		}

		if(target != null && !victim_PlayfabID.Contains("BKMAI_"))
		{
			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + deaths);
		}

		if(aiTarget != null && victim_PlayfabID.Contains("BKMAI_"))
		{
			aiTarget.AI_Death += deaths;
		}

		if (source != null && target != null && !shooter_PlayfabID.Contains("BKMAI_") && !victim_PlayfabID.Contains("BKMAI_"))
		{
			if (target != source)	// kill
			{
				TransmitPlayerState(new int[] { target.ID, source.ID },
					new string[] { "Deaths", "Score" },
					new string[] { "Frags", "Score" });
			}
			else	// suicide
			{
				TransmitPlayerState(new int[] { target.ID },
					new string[] { "Deaths", "Score" });
			}
		}
	}


	protected override  ExitGames.Client.Photon.Hashtable AssembleGameState()
	{
		
		// NOTE: don't add custom integer keys, since ints are used
		// for player identification. for example, adding a key '5'
		// might result in a crash when player 5 tries to join.
		// adding string (or other type) keys should be fine
		
		if (!PhotonNetwork.isMasterClient)
			return null;
		
		vp_MPPlayerStats.EraseStats();	// NOTE: sending an RPC with a re-used gamestate will crash! we must create new gamestates every time
		
		vp_MPNetworkPlayer.RefreshPlayers();
		
		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();
		
		// -------- add game phase, game time and duration --------
		
		state.Add("Phase", Phase);
		state.Add("TimeLeft", vp_MPClock.TimeLeft);
		state.Add("Duration", vp_MPClock.Duration);
		
		//Horus begin
		state.Add("RoundsPassed", RoundsPassed);
		state.Add("LivesLeft", LivesLeft);
		//Horus end
		
		// -------- add the stats of all players (includes health) --------
		
		foreach (vp_MPNetworkPlayer player in vp_MPNetworkPlayer.Players.Values)
		{
			if (player == null)
				continue;
			// add a player stats hashtable with the key 'player.ID'
			ExitGames.Client.Photon.Hashtable stats = player.Stats.All;
			if (stats != null)
				state.Add(player.ID, stats);
		}

		// -------- add note of any disabled pickups --------
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			if (vp_Utility.IsActive(p[0].transform.gameObject))
				continue;
			
			// there are two predicted cases were an ID might already be in the state:
			// 1) a player ID is the same as a pickup ID. this is highly unlikely since
			//		player IDs start at 1 and pickup IDs are ~six figure numbers. also,
			//		only currently disabled pickups are included in the state making it
			//		even more unlikely
			// 2: a pickup has two vp_ItemPickup components with the same ID which is
			//		the case with throwing weapons (grenades). this is highly likely,
			//		but in this case it's fine to ignore the ID second time around
			if (!state.ContainsKey(id))
				state.Add(id, false);
			
		}
		
		//if (state.Count == 0)
		//    UnityEngine.Debug.LogError("Failed to get gamestate.");
		
		return state;
		
	}
	
	[PunRPC]
	protected override void ReceiveGameState(ExitGames.Client.Photon.Hashtable gameState, PhotonMessageInfo info)
	{

		//vp_MPDebug.Log("GOT FULL GAMESTATE!");
		
		//DumpGameState(gameState);
		
		if ((info.sender != PhotonNetwork.masterClient) ||
		    (info.sender.isLocal))
			return;
		
		//vp_MPDebug.Log("Gamestate updated @ " + info.timestamp);
		//Debug.Log("Gamestate updated @ " + info.timestamp);
		
		// -------- extract game phase, game time and duration --------
		
		// TODO: make generic method 'ExtractStat' that does this
		object phase;
		if ((gameState.TryGetValue("Phase", out phase) && (phase != null)))
			Phase = (GamePhase)phase;
		
		object timeLeft;
		object duration;
		if ((gameState.TryGetValue("TimeLeft", out timeLeft) && (timeLeft != null))
		    && (gameState.TryGetValue("Duration", out duration) && (duration != null)))
			vp_MPClock.Set((float)timeLeft, (float)duration);
		
		//Horus begin
		object RoundsPassed_temp;
		if ((gameState.TryGetValue("Team1KillCount", out RoundsPassed_temp) && (RoundsPassed_temp != null)))
			RoundsPassed = (int)RoundsPassed_temp;
		
		object LivesLeft_temp;
		if ((gameState.TryGetValue("Team2KillCount", out LivesLeft_temp) && (LivesLeft_temp != null)))
			LivesLeft = (int)LivesLeft_temp;
		
		
		//Horus end
		// -------- instantiate missing player prefabs --------
		
		vp_MPPlayerSpawner.Instance.InstantiateMissingPlayerPrefabs(gameState);
		
		// -------- refresh stats of all players --------
		
		ReceivePlayerState(gameState, info);


		// -------- disable any pickups noted as currently disabled in the state --------
		
		//vp_MPDebug.Log("DISABLED PICKUPS: " + vp_MPPickupManager.Instance.Pickups.Keys.Count);
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			object isDisabled;
			if (gameState.TryGetValue(id, out isDisabled) && (isDisabled != null))
				vp_Utility.Activate(p[0].transform.gameObject, false);
			
		}
		
		// -------- refresh all teams --------
		
		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();

	}
	public override void GoHome()
	{
		HomeScreen.states = States.EndGamePVE;
		PhotonNetwork.LoadLevel("GameOver");
	}

	public override void FinishGame()
	{
		playersRW = new List<GameRewardsPlayerInfo> ();
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{
			if (PhotonNetwork.isMasterClient) 
			{
				foreach(PhotonPlayer player in PhotonNetwork.playerList)
				{
					if (player.ID == p.ID) 
					{
						GameRewardsPlayerInfo playerRW = new GameRewardsPlayerInfo();
						playerRW.name = player.customProperties ["UserName"].ToString ();
						playerRW.kill = int.Parse(p.Stats.Get("Frags").ToString());
						playerRW.death = int.Parse(p.Stats.Get("Deaths").ToString());
						playerRW.headshot = int.Parse(p.Stats.Get ("HeadShot").ToString());
						playerRW.win = 1;
						playersRW.Add (playerRW);
						break;
					}
				}
			}
			try
			{
				if (System.String.Compare(vp_MPNetworkPlayer.GetName(p.photonView.ownerId), AccountManager.instance.displayName, true) == 0)
				{
					int teamWin = 1;
					int	newFrags1 = int.Parse(p.Stats.Get("Frags").ToString()) ;
					int	newDeaths1 = int.Parse(p.Stats.Get("Deaths").ToString());

					if (p.TeamNumber == teamWin)
					{
						CSVR_UIPlayManage.init.MissionSuccess();
					}
					else
					{
						CSVR_UIPlayManage.init.MissionFaile();
					}
					ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
					properties.Add("KDA", newFrags1 + "/" + newDeaths1);
					properties.Add("MainGun",CSVR_GameSetting.mainGun);
					properties.Add("EndGame", (p.TeamNumber == teamWin) ? "Thắng" :  "Thua");
					PhotonNetwork.player.SetCustomProperties(properties);
				}
			}
			catch
			{
			}
		}
		if (PhotonNetwork.isMasterClient)
		{
			OnGameReward ();
		}
	}

}
