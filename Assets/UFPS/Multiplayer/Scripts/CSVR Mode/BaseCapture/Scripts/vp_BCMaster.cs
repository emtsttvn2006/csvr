﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Horus;
using Horus.ClientModels;

public class vp_BCMaster : vp_MPMaster 
{

	public static int BCTeam1Scores = 0;
	public static int BCTeam2Scores = 0;
	public static int BCGoalPoints = 1000;

	private int Team1KillCount = 0;
	private int Team2KillCount = 0;

	protected override void OnDisable()
	{
		base.OnDisable ();
		Team1KillCount = 0;
		Team2KillCount = 0;
		BCTeam1Scores = 0;
		BCTeam2Scores = 0;
		if(Victims != null){
			Victims.Clear ();
		}
	}

	protected override void  Update()
	{
		
		//Xử lý ẩn room ở đây
		//Kien: an room khi 1 trong 2 team dat 75% so diem
		if( Phase == GamePhase.Playing && !roomHidden && (BCTeam1Scores > (BCGoalPoints * 0.75f) || BCTeam2Scores > (BCGoalPoints * 0.75f))){
			PhotonNetwork.room.open = false;
			PhotonNetwork.room.visible = false;
			roomHidden = true;
		}
		
		
//		if (CSVR_GetInfo.instance != null)
//		{
			if ((Phase == GamePhase.Playing) && (BCTeam1Scores == BCGoalPoints || BCTeam2Scores == BCGoalPoints) && !IsEndGame)
			{
				vp_Timer.DestroyAll();
				Phase = GamePhase.NotStarted;
				IsEndGame = true;
				Endgame();
			}
		//}
		
		if ((Phase == GamePhase.Playing) && (vp_MPClock.Running == false || vp_MPClock.TimeLeft < 0) && !IsEndGame)
		{
			vp_Timer.DestroyAll();
			Phase = GamePhase.NotStarted;
			IsEndGame = true;
			Endgame();
		}
		
		if (m_TookOverGame && (PhotonNetwork.connectionStateDetailed != PeerState.Joined))
			m_TookOverGame = false;
	}

	[PunRPC]
	public override void ReceiveDeathInfo(string shooter_PlayfabID, string shooter_Name, string gunID, string victim_PlayfabID, string victim_Name, int hitPart, float Damage, string level, string nameWeapom, int idTeams, int victimTeams,int idPhoton,int idPhotonVictim, PhotonMessageInfo info)
	{
		base.ReceiveDeathInfo (shooter_PlayfabID, shooter_Name, gunID, victim_PlayfabID, victim_Name, hitPart, Damage, level, nameWeapom, idTeams,victimTeams,idPhoton,idPhotonVictim, info);

		vp_MPNetworkPlayer target = vp_MPNetworkPlayer.Get (idPhotonVictim);
		vp_MPNetworkPlayer source = vp_MPNetworkPlayer.Get(idPhoton);
		if (source == null)
			return;
		
		if (target != source)
		{
			if ((target.TeamNumber != source.TeamNumber)					// inter-team kill
			    || ((target.TeamNumber == 0) && (source.TeamNumber == 0))	// or there are no teams!
			    )
			{
				
				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") + 1);
				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
				
				if (idTeams == 1) {
					Team1KillCount++;
				} else if (idTeams == 2) {
					Team2KillCount++;
				}
			}
			else	// intra-team kill
			{
				
				// you loose one 'Score' for every friendly kill
				// the teammate's stats are not affected
				source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
				target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
				
				if (idTeams == 1) {
					Team2KillCount++;
				} else if (idTeams == 2) {
					Team1KillCount++;
				}
				
			}
		}
		// killing yourself shall always award one 'Death' and minus one 'Score'
		else
		{
			//tự sát
			source.Stats.Set("Frags", (int)source.Stats.Get("Frags") - 1);
			target.Stats.Set("Deaths", (int)target.Stats.Get("Deaths") + 1);
			
			if (idTeams == 1) {
				Team2KillCount++;
			} else if (idTeams == 2) {
				Team1KillCount++;
			}
		}
		
		if (target != source)	// kill
		{
			TransmitPlayerState(new int[] { target.ID, source.ID },
			new string[] { "Deaths", "Score" },
			new string[] { "Frags", "Score" });
		}
		else	// suicide
		{
			TransmitPlayerState(new int[] { target.ID },
			new string[] { "Deaths", "Score" });
		}
		//thêm nạn nhân vào list nạn nhân để hiện thị khi chết.
		if (shooter_PlayfabID == HorusManager.instance.playerID) {
			if(Victims == null){
				Victims = new List<CSVR_Victim> ();
			}
			_victim = new CSVR_Victim ();
			_victim.Name = victim_Name;
			_victim.Level = level;
			_victim.Heath = ((int)Damage).ToString ();
			Victims.Add (_victim);
		}
		

		
		//tattrycatch
	}
	protected override  ExitGames.Client.Photon.Hashtable AssembleGameState()
	{
		
		// NOTE: don't add custom integer keys, since ints are used
		// for player identification. for example, adding a key '5'
		// might result in a crash when player 5 tries to join.
		// adding string (or other type) keys should be fine
		
		if (!PhotonNetwork.isMasterClient)
			return null;
		
		vp_MPPlayerStats.EraseStats();	// NOTE: sending an RPC with a re-used gamestate will crash! we must create new gamestates every time
		
		vp_MPNetworkPlayer.RefreshPlayers();
		
		ExitGames.Client.Photon.Hashtable state = new ExitGames.Client.Photon.Hashtable();
		
		// -------- add game phase, game time and duration --------
		
		state.Add("Phase", Phase);
		state.Add("TimeLeft", vp_MPClock.TimeLeft);
		state.Add("Duration", vp_MPClock.Duration);
		
		//Horus begin
		state.Add("Team1KillCount", Team1KillCount);
		state.Add("Team2KillCount", Team2KillCount);
		//Horus end
		
		//kien
		state.Add ("BCTeam1Scores", BCTeam1Scores);
		state.Add ("BCTeam2Scores", BCTeam2Scores);
		state.Add ("BCGoalPoints", BCGoalPoints);
		//endkien
		
		// -------- add the stats of all players (includes health) --------
		
		foreach (vp_MPNetworkPlayer player in vp_MPNetworkPlayer.Players.Values)
		{
			if (player == null)
				continue;
			// add a player stats hashtable with the key 'player.ID'
			ExitGames.Client.Photon.Hashtable stats = player.Stats.All;
			if (stats != null)
				state.Add(player.ID, stats);
		}

	/*	
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d is vp_PlayerDamageHandler)
				continue;
			if (d == null)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;

			state.Add(-p.viewID, d.CurrentHealth);
			
		}
		*/
		// -------- add note of any disabled pickups --------
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			if (vp_Utility.IsActive(p[0].transform.gameObject))
				continue;

			if (!state.ContainsKey(id))
				state.Add(id, false);
			
		}

		return state;
	}

	
	[PunRPC]
	protected override void ReceiveGameState(ExitGames.Client.Photon.Hashtable gameState, PhotonMessageInfo info)
	{
		
		//vp_MPDebug.Log("GOT FULL GAMESTATE!");
		
		//DumpGameState(gameState);
		
		if ((info.sender != PhotonNetwork.masterClient) ||
		    (info.sender.isLocal))
			return;
		
		//vp_MPDebug.Log("Gamestate updated @ " + info.timestamp);
		//Debug.Log("Gamestate updated @ " + info.timestamp);
		
		// -------- extract game phase, game time and duration --------
		
		// TODO: make generic method 'ExtractStat' that does this
		object phase;
		if ((gameState.TryGetValue("Phase", out phase) && (phase != null)))
			Phase = (GamePhase)phase;
		
		object timeLeft;
		object duration;
		
		if ((gameState.TryGetValue("TimeLeft", out timeLeft) && (timeLeft != null))
		    && (gameState.TryGetValue("Duration", out duration) && (duration != null)))
			vp_MPClock.Set((float)timeLeft, (float)duration);
		
		//Horus begin
		object Team1KillCount_temp;
		if ((gameState.TryGetValue("Team1KillCount", out Team1KillCount_temp) && (Team1KillCount_temp != null)))
			Team1KillCount = (int)Team1KillCount_temp;
		
		object Team2KillCount_temp;
		if ((gameState.TryGetValue("Team2KillCount", out Team2KillCount_temp) && (Team2KillCount_temp != null)))
			Team2KillCount = (int)Team2KillCount_temp;
		//Horus end
		
		//kien
		object BCTeam1Scores_temp;
		if ((gameState.TryGetValue("BCTeam1Scores", out BCTeam1Scores_temp) && (BCTeam1Scores_temp != null)))
			BCTeam1Scores = (int)BCTeam1Scores_temp;
		object BCTeam2Scores_temp;
		if ((gameState.TryGetValue("BCTeam2Scores", out BCTeam2Scores_temp) && (BCTeam2Scores_temp != null)))
			BCTeam2Scores = (int)BCTeam2Scores_temp;
		object BCGoalPoints_temp;
		if ((gameState.TryGetValue("BCGoalPoints", out BCGoalPoints_temp) && (BCGoalPoints_temp != null)))
			BCGoalPoints = (int)BCGoalPoints_temp;
		//endkien
		
		// -------- instantiate missing player prefabs --------
		
		vp_MPPlayerSpawner.Instance.InstantiateMissingPlayerPrefabs(gameState);
		
		// -------- refresh stats of all players --------
		
		ReceivePlayerState(gameState, info);
		
		// -------- refresh health of all non-player damage handlers --------
	/*	
		foreach (vp_DamageHandler d in vp_DamageHandler.Instances.Values)
		{
			if (d == null)
				continue;
			if (d is vp_PlayerDamageHandler)
				continue;
			PhotonView p = d.GetComponent<PhotonView>();
			if (p == null)
				continue;
			object currentHealth;
			if (gameState.TryGetValue(-p.viewID, out currentHealth) && (currentHealth != null))
			{
				d.CurrentHealth = (float)currentHealth;
				if (d.CurrentHealth <= 0.0f)
					vp_Utility.Activate(d.gameObject, false);
			}
		}
		*/
		// -------- disable any pickups noted as currently disabled in the state --------
		
		//vp_MPDebug.Log("DISABLED PICKUPS: " + vp_MPPickupManager.Instance.Pickups.Keys.Count);
		
		foreach (int id in vp_MPPickupManager.Instance.Pickups.Keys)
		{
			
			List<vp_ItemPickup> p;
			vp_MPPickupManager.Instance.Pickups.TryGetValue(id, out p);
			if ((p == null) || (p.Count < 1) || p[0] == null)
				continue;
			
			object isDisabled;
			if (gameState.TryGetValue(id, out isDisabled) && (isDisabled != null))
				vp_Utility.Activate(p[0].transform.gameObject, false);
			
		}
		
		// -------- refresh all teams --------
		
		if (vp_MPTeamManager.Exists)
			vp_MPTeamManager.Instance.RefreshTeams();
		
		//kien
		//khi nhan state ve thi update diem so tren UI luon
        CSVR_GetInfo.instance.Update_BCScoreUI1(BCTeam1Scores);
        CSVR_GetInfo.instance.Update_BCScoreUI2(BCTeam2Scores);
		//endkien
		
	}


	public override void Endgame()
	{
		FinishGame();
		Invoke("GoHome", 5f);
	}
	public override void GoHome()
	{
		HomeScreen.states = States.EndGame;
		PhotonNetwork.LoadLevel("GameOver");
	}
	public override void FinishGame()
	{
		foreach (vp_MPNetworkPlayer p in vp_MPNetworkPlayer.Players.Values)
		{
			if (p == null)
				continue;

			int teamWin;
			//neu het gio ma 2 team ghi diem bang nhau va giet bang nhau thi ca 2 team cung thua
			if(BCTeam1Scores == BCTeam2Scores){
				if(Team1KillCount == Team2KillCount){
					teamWin = 0;
				}else
					teamWin = Team1KillCount > Team2KillCount ? 1 : 2;//bang diem nhau thi tinh kill
			}else
				teamWin = BCTeam1Scores > BCTeam2Scores ? 1 : 2;//so sanh diem truoc

			int	newFrags1 = int.Parse(p.Stats.Get("Frags").ToString()) ;
			int	newDeaths1 = int.Parse(p.Stats.Get("Deaths").ToString());

			if (PhotonNetwork.isMasterClient) {



				foreach(PhotonPlayer player in PhotonNetwork.playerList){
					if (player.ID == p.ID) {
						HorusClientAPI.GameReward(
							new GameRewardRequest(){
								name = player.customProperties["UserName"].ToString(),
//								apiKey = HorusManager.instance.userAPIKey,
								level = int.Parse(player.customProperties["Level"].ToString()),
								kill = newFrags1,
								death = newDeaths1,
								headshot = (int)p.Stats.Get ("HeadShot"),
								win = (p.TeamNumber == teamWin) ? 1 : 0
							}, 
							(result) => {

								ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
								properties.Add("KDA", newFrags1 + "/" + newDeaths1);
								properties.Add("Coin", result.Inventory.coin);
								properties.Add("Gold", result.Inventory.gold);
								properties.Add("EndGame", (p.TeamNumber == teamWin) ? "Thắng" :  "Thua");
								properties.Add("Exp", result.Inventory.exp);
								properties.Add("Level", result.Inventory.level);
								player.SetCustomProperties(properties);

							},
							(error) => {
								//ErrorView.instance.ShowPopupError ("Trang bị thất bại");
							}
						);
						break;
					}
				}
			}

			if (System.String.Compare(vp_MPNetworkPlayer.GetName(p.photonView.ownerId), AccountManager.instance.displayName, true) == 0)
			{

				ExitGames.Client.Photon.Hashtable properties = new ExitGames.Client.Photon.Hashtable();
				properties.Add("MainGun",CSVR_GameSetting.mainGun);

				if (p.TeamNumber == teamWin)
				{
					CSVR_UIPlayManage.init.MissionSuccess();
				}
				else
				{
					CSVR_UIPlayManage.init.MissionFaile();
				}

				PhotonNetwork.player.SetCustomProperties(properties);
			}
			
		}
	}
}
