﻿using UnityEngine;
using System.Collections;

public class vp_BCHeart : MonoBehaviour {

	public vp_BCBase BaseEntered = null;
	
	void Start(){
		if (transform.root.tag == "PolicePlayer")
			tag = "PoliceHeart";
		else if (transform.root.tag == "TerrorisPlayer")
			tag = "TerrorisHeart";
		
	}
	
	void OnDestroy(){
		if(BaseEntered){
			if(tag == "PoliceHeart")
				BaseEntered.Team1Count -= 1;
			if(tag == "TerrorisHeart")
				BaseEntered.Team2Count -= 1;
		}
	}
}
