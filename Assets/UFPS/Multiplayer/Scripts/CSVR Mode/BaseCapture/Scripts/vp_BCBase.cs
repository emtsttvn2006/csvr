﻿using UnityEngine;
using System.Collections;

public class vp_BCBase : MonoBehaviour {
	private float BCLastUpdate1 = 0;
	private float BCLastUpdate2 = 0;
	public float Interval = 0.25f;
	public int ScoreRate = 10;
	
	public int Team1Count = 0;
	public int Team2Count = 0;
	
	void Start(){
		if(CSVR_GameSetting.modeName != "CSVR_BaseCapture"){
			DestroyImmediate(this.gameObject);
		}
	}
	
	void OnTriggerEnter(Collider enterme){
		if(enterme.tag == "PoliceHeart"){
            
			enterme.GetComponent<vp_BCHeart>().BaseEntered = this;
			Team1Count += 1;
            if(Team2Count == 0)
                BCLastUpdate1 = Time.time;
		}
		if(enterme.tag == "TerrorisHeart"){
            
			enterme.GetComponent<vp_BCHeart>().BaseEntered = this;
			Team2Count += 1;
            if (Team1Count == 0)
                BCLastUpdate2 = Time.time;
		}
	}
	
	void OnTriggerExit(Collider exitme){
		if(exitme.tag == "PoliceHeart"){
			exitme.GetComponent<vp_BCHeart>().BaseEntered = null;
			Team1Count = Team1Count > 0 ? Team1Count -= 1 : 0;
            
		}
		if(exitme.tag == "TerrorisHeart"){
			exitme.GetComponent<vp_BCHeart>().BaseEntered = null;
			Team2Count = Team2Count > 0 ? Team2Count -= 1 : 0;
            
		}
	}
	
	void OnTriggerStay(Collider insideme){
		if (insideme.tag == "PoliceHeart") {
			if(Time.time > BCLastUpdate1 + Interval && Team2Count == 0){//base thuoc ve team 1
				vp_BCMaster.BCTeam1Scores += Team1Count * ScoreRate;
				if( vp_BCMaster.BCTeam1Scores > vp_BCMaster.BCGoalPoints)
					vp_BCMaster.BCTeam1Scores = vp_BCMaster.BCGoalPoints;
				CSVR_GetInfo.instance.Update_BCScoreUI1(vp_BCMaster.BCTeam1Scores);
				BCLastUpdate1 = Time.time;
			}
		}
		
		if(insideme.tag == "TerrorisHeart"){
			if(Time.time > BCLastUpdate2 + Interval && Team1Count == 0){//base thuoc ve team 2
				vp_BCMaster.BCTeam2Scores += Team2Count * ScoreRate;
				if( vp_BCMaster.BCTeam2Scores > vp_BCMaster.BCGoalPoints)
					vp_BCMaster.BCTeam2Scores = vp_BCMaster.BCGoalPoints;
				CSVR_GetInfo.instance.Update_BCScoreUI2(vp_BCMaster.BCTeam2Scores);
				BCLastUpdate2 = Time.time;
			}
		}
	}
}
