﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class LongPressEventTrigger : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

	public float durationThresholdPolice = 7.0f;
	public float durationThresholdTerrorist = 5.0f;
	public string bombSiteName;
	private float durationThreshold;

	public int playerTeamId = 2;

	[SerializeField]
	private Image loadingBar;

	[SerializeField]
	private GameObject loadingBarParent;

	[SerializeField]
	private Sprite[] loadingBarSprites;

	[SerializeField]
	private Text txtStatus;

	private bool isPointerDown = false;
	private bool longPressTriggered = false;
	private float timePressStarted;


	private CSVR_ServerConnector serverConnector;
	public vp_FPPlayerEventHandler FPPlayer;


	public UnityEvent onLongPress = new UnityEvent( );

	public void Init( int team){
		playerTeamId = team;
		serverConnector = CSVR_FPGetGun.getgun.transform.root.GetComponent<CSVR_ServerConnector>();
		durationThreshold = (playerTeamId == 2) ? durationThresholdTerrorist : durationThresholdPolice;
		this.GetComponent<Image>().sprite = (playerTeamId == 2) ? loadingBarSprites[0] : loadingBarSprites[1];
	}

	private void Update( ) {
		if ( isPointerDown && !longPressTriggered ) {
			if (Time.time - timePressStarted > durationThreshold) {
				longPressTriggered = true;
				onLongPress.Invoke ();
				this.gameObject.SetActive(false);
				loadingBarParent.SetActive (false);

				if (playerTeamId == 2) {
					//dat bomb
					FPPlayer.Attack.TryStart ();
					//ten cho dat bomb
					Request_BombSiteName (bombSiteName);
				} else {
					//go bomb
					if (vp_SADMaster.C4IsPlanted) {
						Request_DefuseBomb (true, true);
					}
				}
			} else {
				loadingBar.fillAmount = (Time.time - timePressStarted) / durationThreshold;
			}
		}
	}

	public void OnPointerDown( PointerEventData eventData ) {
		timePressStarted = Time.time;
		isPointerDown = true;
		longPressTriggered = false;

		loadingBarParent.gameObject.SetActive(true);

		if (playerTeamId == 2 && CSVR_GrenadeShooter.instanceC4 != null && !vp_SADMaster.C4IsPlanted) 
		{
			//Debug.Log ("OnPointerDown pause");
			//FPPlayer.SetWeaponByName.Try (GameConstants.VNMODESAD_BombC4);
			txtStatus.text = "Đang đặt bomb ....";
			CSVR_GrenadeShooter.instanceC4.PleassWaitC4 ();
		} 
		if (playerTeamId == 1  && vp_SADMaster.C4IsPlanted) 
		{
			txtStatus.text = "Đang gỡ bomb ....";
			Request_DefuseBomb (true,false);
		}
	}

	public void OnPointerUp( PointerEventData eventData ) {
		isPointerDown = false;

		loadingBarParent.gameObject.SetActive(false);

		if (playerTeamId == 2 && !longPressTriggered && CSVR_GrenadeShooter.instanceC4 != null && !vp_SADMaster.C4IsPlanted) {
			CSVR_GrenadeShooter.instanceC4.StopAnimC4();
			FPPlayer.Attack.TryStop ();
		}
		else if (playerTeamId == 1  && vp_SADMaster.C4IsPlanted) 
		{
			Request_DefuseBomb (false,false);
		}
	}


	public void OnPointerExit( PointerEventData eventData ) {
		isPointerDown = false;

		loadingBarParent.gameObject.SetActive(false);

		if (playerTeamId == 2 && !longPressTriggered && CSVR_GrenadeShooter.instanceC4 != null && !vp_SADMaster.C4IsPlanted) {
			CSVR_GrenadeShooter.instanceC4.StopAnimC4 ();
			FPPlayer.Attack.TryStop ();
		}
		else if (playerTeamId == 1  && vp_SADMaster.C4IsPlanted) 
		{
			Request_DefuseBomb (false,false);
		}
	}
	void Request_BombSiteName(string bombSiteName) {
		serverConnector.photonView.RPC("Receive_BombSite", PhotonTargets.All,bombSiteName);
	}
	void Request_DefuseBomb(bool isDefuse,bool isSuccess) {
		serverConnector.photonView.RPC("Receive_DefuseBomb", PhotonTargets.All,isDefuse,isSuccess);
	}

}