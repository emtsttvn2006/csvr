﻿using System;
using Horus.ClientModels;
using Horus.Internal;
using Horus.Json;
using UnityEngine;

namespace Horus{
	
	public static class HorusClientAPI  {
		
		//public delegate void ProcessApiCallback<in TResult>(TResult result) where TResult : HorusResultCommon;
		//public delegate void LoginWithHorusRequestCallback(string urlPath, int callId, LoginWithHorusRequest request, object customData);
		//public delegate void LoginWithHorusResponseCallback(string urlPath, int callId, LoginWithHorusRequest request, LoginResult result, HorusError error, object customData);
		private static string _authKey = null;

		public static void RegisterHorusUser(RegisterHorusUserRequest request, HorusResultCommon.ProcessApiCallback<RegisterHorusResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<RegisterHorusResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			HorusHttp.Post("/client/register", serializedJson, null, null, callback, request, customData);
		}
			
		public static void LoginWithFacebook(LoginWithFacebookRequest request, HorusResultCommon.ProcessApiCallback<LoginResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<LoginResult>.HandleResults(requestContainer, resultCallback, errorCallback, LoginWithFacebookResultAction);
			};
			HorusHttp.Post("/client/login-facebook", serializedJson, null, null, callback, request, customData);
		}
		public static void LoginWithFacebookResultAction(LoginResult result, CallRequestContainer requestContainer)
		{
			_authKey = requestContainer.AuthKey ?? _authKey;
		}

		public static void LoginWithHorus(LoginWithHorusRequest request, HorusResultCommon.ProcessApiCallback<LoginResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<LoginResult>.HandleResults(requestContainer, resultCallback, errorCallback,LoginWithHorusResultAction );
			};
			UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/login", serializedJson, null, null, callback, request, customData);
		}
		public static void LoginWithHorusResultAction(LoginResult result, CallRequestContainer requestContainer)
		{
			_authKey = requestContainer.AuthKey ?? _authKey;
		}

		public static void GetCatalogItems(GetCatalogItemsRequest request, HorusResultCommon.ProcessApiCallback<GetCatalogItemsResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetCatalogItemsResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/get-catalog-items", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void GetUserInventory(GetUserInventoryInGameRequest request, HorusResultCommon.ProcessApiCallback<GetUserInventoryInGameResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetUserInventoryInGameResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/get-user-inventory-in-game", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void PurchaseItem(PurchaseItemRequest request, HorusResultCommon.ProcessApiCallback<PurchaseItemResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<PurchaseItemResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/purchase-item", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void CraftItem(CraftItemRequest request, HorusResultCommon.ProcessApiCallback<CraftItemResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<CraftItemResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			UnityEngine.Debug.Log (serializedJson);
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (_authKey);
			HorusHttp.Post("/client/craft-item", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void PluginTo(PluginToItemRequest request, HorusResultCommon.ProcessApiCallback<PluginToResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<PluginToResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/plugin-item", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void SellItem(SellItemRequest request, HorusResultCommon.ProcessApiCallback<SellItemResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<SellItemResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/sell-item", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void EquipItem(EquipItemRequest request, HorusResultCommon.ProcessApiCallback<EquipItemResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<EquipItemResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/equip-item", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void GetTitleData(GetTitleDataRequest request, HorusResultCommon.ProcessApiCallback<GetTitleDataResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			//if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetTitleDataResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/get-title-data", serializedJson,  null, null, callback, request, customData);
		}

		public static void GetRelationShip(GetRelationShipRequest request, HorusResultCommon.ProcessApiCallback<GetRelationShipResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetRelationShipResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			if (MainView.instance.DebugError) Debug.Log (serializedJson);
			HorusHttp.Post("/client/get-relationship", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void UpdateRelationship(UpdateRelationshipRequest request, HorusResultCommon.ProcessApiCallback<UpdateRelationshipResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<UpdateRelationshipResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/update-relationship", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void GetUserInfo(GetUserInfoRequest request, HorusResultCommon.ProcessApiCallback<GetUserInfoResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetUserInfoResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/get-users-info", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void UpdateUserInfo(UpdateUserInfoRequest request, HorusResultCommon.ProcessApiCallback<UpdateUserInfoResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<UpdateUserInfoResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/update-user-info", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void GameReward(GameRewardRequest request, HorusResultCommon.ProcessApiCallback<GameRewardResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GameRewardResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			//UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/game-reward", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}
		public static void GameRewards(GameRewardsRequest request, HorusResultCommon.ProcessApiCallback<GameRewardsResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GameRewardsResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/game-rewards", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}
		public static void GetLeaderboard(GetLeaderBoardRequest request, HorusResultCommon.ProcessApiCallback<GetLeaderBoardResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetLeaderBoardResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/user-top", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void RedeemCoupon(GetRedeemCouponRequest request, HorusResultCommon.ProcessApiCallback<GetRedeemCouponResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetRedeemCouponResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/game-gift-code", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void KeepLogin(KeepLoginRequest request, HorusResultCommon.ProcessApiCallback<KeepLoginResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<KeepLoginResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/keep-login", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void GetAPIKey(GetAPIKeyRequest request, HorusResultCommon.ProcessApiCallback<GetAPIKeyResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetAPIKeyResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/api-key", serializedJson,  "Cookie", _authKey, callback, request, customData);
		}

		public static void GetStatusImei(GetStatusImeiRequest request, HorusResultCommon.ProcessApiCallback<GetStatusImeiResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate(CallRequestContainer requestContainer)
			{
				ResultContainer<GetStatusImeiResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/imei", serializedJson, null, null, callback, request, customData);
		}

        public static void ClanCreateProcess(ClanCreateRequest request, HorusResultCommon.ProcessApiCallback<ClanCreateResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanCreateResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-create", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanSearchProcess(ClanSearchRequest request, HorusResultCommon.ProcessApiCallback<ClanSearchResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanSearchResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-search", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanListProcess(ClanListRequest request, HorusResultCommon.ProcessApiCallback<ClanListResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanListResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-list", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanOutProcess(ClanOutRequest request, HorusResultCommon.ProcessApiCallback<ClanOutResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanOutResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-out", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanUpdateProcess(ClanUpdateRequest request, HorusResultCommon.ProcessApiCallback<ClanUpdateResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanUpdateResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-update", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanInActiveProcess(ClanInActiveRequest request, HorusResultCommon.ProcessApiCallback<ClanInActiveResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanInActiveResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-in-active", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanMemberAddProcess(ClanMemberAddRequest request, HorusResultCommon.ProcessApiCallback<ClanMemberAddResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanMemberAddResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-member-add", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanMemberRemoveProcess(ClanMemberRemoveRequest request, HorusResultCommon.ProcessApiCallback<ClanMemberRemoveResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanMemberRemoveResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-member-remove", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanJoiningProcess(ClanJoiningRequest request, HorusResultCommon.ProcessApiCallback<ClanJoiningResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanJoiningResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-joining", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanViceUpdateProcess(ClanViceUpdateRequest request, HorusResultCommon.ProcessApiCallback<ClanViceUpdateResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanViceUpdateResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-vice-update", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanChiefUpdateProcess(ClanChiefUpdateRequest request, HorusResultCommon.ProcessApiCallback<ClanChiefUpdateResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanChiefUpdateResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-chief-update", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

        public static void ClanDonateProcess(ClanDonateRequest request, HorusResultCommon.ProcessApiCallback<ClanDonateResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
                ResultContainer<ClanDonateResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
            HorusHttp.Post("/client/clan-donate", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

		public static void GetRoleList(RoleListRequest request, HorusResultCommon.ProcessApiCallback<RoleListResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
			{
				ResultContainer<RoleListResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log (serializedJson);
			HorusHttp.Post("/client/role-list", serializedJson, "Cookie", _authKey, callback, request, customData);
		}
        
		public static void GetQuestSerial(GetQuestSerialRequest request, HorusResultCommon.ProcessApiCallback<GetQuestSerialResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
			{
				ResultContainer<GetQuestSerialResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
            if (MainView.instance.DebugError) UnityEngine.Debug.Log(serializedJson);
			HorusHttp.Post("/client/quest-serial", serializedJson, "Cookie", _authKey, callback, request, customData);
		}
		public static void GetQuestProcess(GetQuestProcessRequest request, HorusResultCommon.ProcessApiCallback<GetQuestProcessResult> resultCallback, ErrorCallback errorCallback, object customData = null)
        {
            if (_authKey == null) throw new Exception("Must be logged in to call this method");

            string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
            Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
            {
				ResultContainer<GetQuestProcessResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
            };
            if (MainView.instance.DebugError) UnityEngine.Debug.Log(serializedJson);
            HorusHttp.Post("/client/quest-process", serializedJson, "Cookie", _authKey, callback, request, customData);
        }

		public static void RoomUpdate(RoomUpdateRequest request, HorusResultCommon.ProcessApiCallback<RoomUpdateResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
			{
				ResultContainer<RoomUpdateResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			if (MainView.instance.DebugError) UnityEngine.Debug.Log(serializedJson);
			HorusHttp.Post("/client/room-update", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void RoomStatic(RoomStaticRequest request, HorusResultCommon.ProcessApiCallback<RoomStaticResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
			{
				ResultContainer<RoomStaticResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			if (MainView.instance.DebugError) UnityEngine.Debug.Log(serializedJson);
			HorusHttp.Post("/client/room-static", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

		public static void RoomProfile(RoomProfileRequest request, HorusResultCommon.ProcessApiCallback<RoomProfileResult> resultCallback, ErrorCallback errorCallback, object customData = null)
		{
			if (_authKey == null) throw new Exception("Must be logged in to call this method");

			string serializedJson = JsonWrapper.SerializeObject(request, HorusUtil.ApiSerializerStrategy);
			Action<CallRequestContainer> callback = delegate (CallRequestContainer requestContainer)
			{
				ResultContainer<RoomProfileResult>.HandleResults(requestContainer, resultCallback, errorCallback, null);
			};
			if (MainView.instance.DebugError) UnityEngine.Debug.Log(serializedJson);
			HorusHttp.Post("/client/room-profile", serializedJson, "Cookie", _authKey, callback, request, customData);
		}

    }
}
