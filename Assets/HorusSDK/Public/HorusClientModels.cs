﻿using Horus.Internal;
using LitJson;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;
using System.Linq;

namespace Horus.ClientModels
{
	
	public class RegisterHorusUserRequest
	{
		public string UserName { get; set;}
		public string Password { get; set;}
		//public string Email { get; set;}
		//public string PhoneNumber { get; set;}
	}
	public class RegisterHorusResult : HorusResultCommon
	{
		public string _id { get; set;}
		public string UserName { get; set;}
		public string Status { get; set;}
		public string LastLoginTime { get; set;}
	}

    public class UserInfo
    {
        public string _id { get; set; }
        public string DisplayName { get; set; }
        public string UserName { get; set; }        
        public string status { get; set; }
        public int AccountLevel { get; set; }
        public int AccountExp { get; set; }
        public int AccountDead { get; set; }
        public int AccountHeadShot { get; set; }
        public int AccountKill { get; set; }
        public int AccountLost { get; set; }
        public int AccountWin { get; set; }
        public string last_api_login { get; set; }
        public string LastLoginTime { get; set; }
        public string DiemHoatDong { get; set; }
        public string Vip { get; set; }
        public object clan { get; set; }

        public UserInfo()
        {
            this._id = "";
            this.DisplayName = "";
            this.UserName = "";            
            this.status = "";
            this.AccountLevel = -1;
            this.AccountExp = -1;
            this.AccountDead = -1;
            this.AccountHeadShot = -1;
            this.AccountKill = -1;
            this.AccountLost = -1;
            this.AccountWin = -1;
            this.last_api_login = "";
            this.LastLoginTime = "";
            this.DiemHoatDong = "";
            this.Vip = "";
            this.clan = null;
        }        
    }

    public class ClanInfo
    {
        public string clanName { get; set; }

        public ClanInfo()
        {
            this.clanName = "";
        }

        public ClanInfo(string clanName)
        {
            this.clanName = clanName;
        }
    }

    public class GetUserInfoRequest
    {
        public List<string> UserIds { get; set; }

        public GetUserInfoRequest()
        {
            this.UserIds = new List<string>();
        }

        public void Add(string Name)
        {
            this.UserIds.Add(Name);
        }
    }   

    public class GetUserInfoResult : HorusResultCommon
    {
        public List<UserInfo> users { get; set; }
        public string error { get; set; }
    }
    
    public class LoginWithFacebookRequest
	{
		public string access_token { get; set;}
	}

	public class LoginWithHorusRequest
	{
		public string UserName { get; set;}
		public string Password { get; set;}
	}
	public class LoginResult : HorusResultCommon
	{
		public string _id { get; set;}
		public int AccountLevel { get; set;}
		public int AccountExp { get; set;}
		public int AccountDead { get; set;}
		public int AccountHeadShot { get; set;}
		public int AccountKill { get; set;}
		public int AccountLost { get; set;}
		public int AccountWin { get; set;}
		public string UserName { get; set;}
		public string DisplayName { get; set;}
		public string imei { get; set;}
		public string Status { get; set;}
		public string[] roles { get; set;}
		public string last_api_login { get; set;}
		public Dictionary<string,Dictionary<string,int>> dailyGame { get; set;}
		public List<ItemSpecific> accounts { get; set;}
		public List<ItemInstance> Inventory { get; set;}
        public object clan { get; set; }
        public ClanInfo Clan { get; set; }        
    }

	public class ItemSpecific
	{

		public int money { get; set;}
		public string money_type { get; set;}

		public ItemSpecific()
		{
			this.money = 0;
			this.money_type = string.Empty;
		}
	}



	//lấy data trên server về
	public class GetTitleDataRequest
	{
		public List<string> Keys { get; set;}
	}

	public class GetTitleDataResult : HorusResultCommon
	{
		public Dictionary<string,string> Data { get; set;}
	}

	//lấy danh sách trang bị trong shop
	public class GetCatalogItemsRequest
	{
		public string CatalogVersion { get; set;}
	}

	public class GetCatalogItemsResult : HorusResultCommon
	{
		public List<CatalogItem> Catalog { get; set;}
	}
		
	//lấy danh sách trang bị trong game của user
	public class GetUserInventoryInGameRequest
	{
		public string _id { get; set;}
	}
	public class GetUserInventoryInGameResult : HorusResultCommon
	{
		public List<ItemInstance> Inventory { get; set;}
	}

	//ép đồ.
	public class CraftItemRequest
	{
		public string ItemId { get; set;}
		public string useItem { get; set;}
	}
	public class CraftItemResult : HorusResultCommon
	{
		public string Status { get; set;}
		public List<ItemInstance> ItemCraft { get; set;}
	}
		
	//trang bị phụ kiện cho súng.
	public class PluginToItemRequest
	{
		public string objId { get; set;}		//id của súng 
		public string pluginObjId { get; set;}	//id của phụ kiện
	}
	public class PluginToResult : HorusResultCommon
	{
		public string Status { get; set;}
		public ItemInstance ItemPlugin { get; set;}
	}

	//mua trang bị
	public class PurchaseItemRequest
	{
		public List<ItemPurchase> Purchases { get; set;}
	}
	public class PurchaseItemResult : HorusResultCommon
	{
		public List<ItemInstance> ItemPurchase { get; set;}
	}
	public class ItemPurchase
	{
		public string ItemId { get; set;}
		//public string CatalogVersion { get; set;}
		public string moneyUse { get; set;}
		public int RemainingUses { get; set;}
		public int UseOnBuy { get; set;}
	}

	//mua trang bị
	public class SellItemRequest
	{
		public string _id { get; set;}
	}
	public class SellItemResult : HorusResultCommon
	{
		public InventoryReward Inventory { get; set;}
	}



	//trang bị hoặc gỡ trang bị
	public class EquipItemRequest
	{
		public string _id { get; set;}
		//public string ItemClass { get; set;}
		public string EquipGroup { get; set;}
	}
	public class EquipItemResult : HorusResultCommon
	{
		public List<ItemInstance> ItemEuip { get; set;}
	}



	public class CatalogItem:IComparable<CatalogItem>
	{
		public string _id { get; set;}

		public string ItemId { get; set;}

		public string ItemClass { get; set;}

		public string CatalogVersion { get; set;}

		public string DisplayName { get; set;}

		public string Description { get; set;}

		public Dictionary<string,uint> VirtualCurrencyPrices { get; set;}

		public string UsagePeriod { get; set;}

		public string CustomData { get; set;}

		public RecipeInfo Recipe { get; set;}

		public RequireInfo required { get; set;}

		public CatalogItemAccessoriesInfo accessories { get; set;}

		public int CompareTo(CatalogItem other) {
			return ItemId.CompareTo(other.ItemId);
		}
	}


	public class CatalogItemAccessoriesInfo
	{
		public string[] itemIdList { get; set;}

		public string[] itemClassList { get; set;}
	}

	public class RequireInfo{
		public int level { get; set;}
		public string[] role { get; set;}
	}

	public class RecipeInfo
	{
		public string success_rate { get; set;}

		public Dictionary<string,string> required { get; set;}
	}

	public class ItemInstance
	{
		public string _id { get; set;}

		public string ItemId { get; set;}

		public string ItemIdModel { get; set;}

		public string ItemClass { get; set;}

		public string CatalogVersion { get; set;}

		public string DisplayName { get; set;}

		public string Expiration { get; set;}

		public string CustomData { get; set;}

		public int? RemainingUses { get; set;}

		public string EquipGroup { get; set;}

		//danh cho weapon
		public ItemInstanceAccessoriesInfo accessories { get; set;}

		//danh cho phu kien
		public string pluginTo { get; set;}
	}

	public class ItemInstanceAccessoriesInfo
	{
		public Dictionary<string,string> scope { get; set;}//111_
		public Dictionary<string,string> magazine { get; set;}//222_
		public Dictionary<string,string> grip { get; set;}//333_
		public Dictionary<string,string> muzzle { get; set;}//444_
		public Dictionary<string,string> skin { get; set;}
		public Dictionary<string,string> acc { get; set;}
	}

	public class GetRelationShipRequest{}

	public class GetRelationShipResult : HorusResultCommon
	{
		public List<RelationshipInfo> RelationshipList { get; set;}
	}

	public class RelationshipInfo
	{
		public string _id { get; set;}
		public string CreateId { get; set;}
		public string PartnerId { get; set;}
		public int Status { get; set;}
		public UserInfo Partner { get; set;}
        public bool Online { get; set; }

        public RelationshipInfo()
        {
            this._id = "";
            this.CreateId = "";
            this.PartnerId = "";
            this.Status = -1;
            this.Partner = null;
            this.Online = false;
        }
    }
//	public class RelationshipPartnerInfo
//	{
//		public string UserName { get; set;}
//
//		public string AccountLost { get; set;}
//
//		public string PartnerId { get; set;}
//
//		public string Status { get; set;}
//	}


	//them, dong y hoac huy ket ban
	public class UpdateRelationshipRequest
	{
		public string PartnerId { get; set;}
		public int Status { get; set;}
	}
	public class UpdateRelationshipResult : HorusResultCommon
	{
		public List<RelationshipInfo> Relationships { get; set;}
	}

	


	public class UpdateUserInfoRequest
	{
		public Dictionary<string,string> UserInfo { get; set;}
	}

	public class UpdateUserInfoResult : HorusResultCommon
	{
	}
	//game reward nhieu nguoi
	public class GameRewardsRequest
	{
		public string apiKey { get; set;}
		public string mode { get; set;}
		public string map { get; set;}
		public List<GameRewardsPlayerInfo> players { get; set;}
	}
	public class GameRewardsPlayerInfo
	{

		public string name { get; set;}
		public int level { get; set;}
		public int kill { get; set;}
		public int death { get; set;}
		public int headshot { get; set;}
		public int win { get; set;}

	}
	public class GameRewardsResult : HorusResultCommon
	{
		public InventoryReward[] players { get; set;}
	}
	//end

	//game reward 1 nguoi
	public class GameRewardRequest
	{

		//public string apiKey { get; set;}
		public string name { get; set;}
		public int level { get; set;}
		public int kill { get; set;}
		public int death { get; set;}
		public int headshot { get; set;}
		public int win { get; set;}

	}
	public class GameRewardResult : HorusResultCommon
	{
		public InventoryReward Inventory { get; set;}
	}
	public class InventoryReward
	{
		public string name { get; set;}
		public int coin { get; set;}
		public int gold { get; set;}
		public int exp { get; set;}
		public int level { get; set;}
		public int kill { get; set;}
		public int death { get; set;}
		public int win { get; set;}
		public int lot { get; set;}
		public ItemInstance[] items { get; set;}
	}
	//end

	public class GetLeaderBoardRequest
	{
		public string order { get; set;}
		public string by { get; set;}
		public int count { get; set;}

	}
	public class GetLeaderBoardResult : HorusResultCommon
	{
		public List<UserInfo> UserList { get; set;}
	}

	public class GetRedeemCouponRequest
	{
		public string code { get; set;}
		public string useItem { get; set;}


	}
	public class GetRedeemCouponResult : HorusResultCommon
	{
		public List<ItemInstance> Inventory { get; set;}
	}

	public class KeepLoginRequest
	{
	}
	public class KeepLoginResult : HorusResultCommon
	{
	}

	public class GetAPIKeyRequest
	{
	}
	public class GetAPIKeyResult : HorusResultCommon
	{
		public string APIKey { get; set;}
	}

	public class GetStatusImeiRequest
	{
		public string imei { get; set;}
	}

	public class GetStatusImeiResult : HorusResultCommon
	{
		public int exist { get; set;}
	}

    public class ClanResult : HorusResultCommon
    {
        public string _id { get; set; }
        public string roleId { get; set; }
        public int level { get; set; }
        public int memberMax { get; set; }
        public int memberCount { get; set; }
        public string description { get; set; }
        public string logo { get; set; }
        public string creater { get; set; }
        public string chief { get; set; }
        public object vice { get; set; }
        public List<string> Vice { get; set; }
        public object members { get; set; }
        public List<string> Members { get; set; }
        public object joining { get; set; }
        public List<string> Joining { get; set; }
        public object invite { get; set; }
        public List<string> Invite { get; set; }
        public int status { get; set; }
        public string created_at { get; set; }

        public ClanResult()
        {
            this._id = "";
            this.roleId = "";
            this.level = -1;
            this.memberMax = -1;
            this.memberCount = -1;
            this.description = "";
            this.creater = "";
            this.chief = "";
            this.vice = new List<string>();
            this.members = new List<string>();
            this.joining = new List<string>();
            this.invite = new List<string>();
            this.status = -1;
            this.created_at = "";
        }

        public List<string> UpdateFrom(object o)
        {
            List<string> ReturnValue = new List<string>();

            if (o != null && o.ToString().Length > 3)
            {
                ReturnValue.Clear();
                try
                {
                    IList iList = (IList) o;
                    IEnumerable<string> list2 = iList.Cast<string>();
                    foreach (string str in list2)
                    {
                        ReturnValue.Add(str);
                    }
                } catch
                {
                    if (ReturnValue.Count <= 0)
                    {
                        Debug.Log("Json bị thay đổi!!");
                        ReturnValue.Clear();
                        Dictionary<string, string> dic = JsonMapper.ToObject<Dictionary<string, string>>(o.ToString());
                        foreach (KeyValuePair<string, string> KeyValuePair in dic)
                        {
                            ReturnValue.Add(KeyValuePair.Value);
                        }
                    }
                }                
            }

            return ReturnValue;
        }

        public void UpdateAllMemberArray()
        {
            UpdateVices();
            UpdateMembers();
            UpdateJoining();
            UpdateInvite();
        }

        public void UpdateVices()
        {
            this.Vice = new List<string>();
            this.Vice = UpdateFrom(this.vice);
        }

        public void UpdateMembers()
        {
            this.Members = new List<string>();
            this.Members = UpdateFrom(this.members);
        }

        public void UpdateJoining()
        {
            this.Joining = new List<string>();
            this.Joining = UpdateFrom(this.joining);
        }

        public void UpdateInvite()
        {
            this.Invite = new List<string>();
            this.Invite = UpdateFrom(this.invite);
        }

        public int GetViceIndex(string Name)
        {
            return this.Vice.FindIndex((ViceName) => ViceName == Name);
        }

        public int GetMemberIndex(string Name)
        {
            return this.Members.FindIndex((MemberName) => MemberName == Name);
        }

        public int GetJoiningIndex(string Name)
        {
            return this.Joining.FindIndex((JoiningName) => JoiningName == Name);
        }

        public int GetInvteIndex(string Name)
        {
            return this.Invite.FindIndex((InviteName) => InviteName == Name);
        }
    }

    public class ClanCreateRequest
    {
        public string roleId { get; set; }
    }

    public class ClanCreateResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanCreateResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanSearchRequest
    {
        public string roleId { get; set; }
    }

    public class ClanSearchResult : HorusResultCommon
    {
        public List<ClanResult> Clan { get; set; }

        public ClanSearchResult()
        {
            this.Clan = new List<ClanResult>();
        }

        public ClanResult GetClan(string Name)
        {
            int index = this.Clan.FindIndex((Clan) => Clan.roleId == Name);
            if (index != -1)
            {
                return this.Clan[index];
            } else
            {
                return null;
            }
        }
    }    

    public class ClanListRequest
    {
        public string listBy { get; set; }
        public string order { get; set; }
        public string page { get; set; }
    }

    public class ClanListResult : HorusResultCommon
    {
        public List<ClanResult> Clan { get; set; }

        public ClanListResult()
        {
            this.Clan = new List<ClanResult>();
        }

        public ClanListResult(List<ClanResult> list)
        {
            this.Clan = list;
        }

        public void UpdateAllClanArray()
        {
            foreach (ClanResult clan in this.Clan)
            {
                clan.UpdateAllMemberArray();
            }
        }        
    }

    public class ClanOutRequest
    {
        
    }

    public class ClanOutResult : HorusResultCommon
    {
        public List<ClanResult> Clan { get; set; }

        public ClanOutResult()
        {
            this.Clan = new List<ClanResult>();
        }
    }

    public class ClanUpdateRequest {
        public string description { get; set; }
        public string logo { get; set; }
    }

    public class ClanUpdateResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanUpdateResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanInActiveRequest
    {
        
    }

    public class ClanInActiveResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanInActiveResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanMemberAddRequest
    {
        public List<string> usernames { get; set; }

        public ClanMemberAddRequest()
        {
            this.usernames = new List<string>();
        }
    }

    public class ClanMemberAddResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }
        public string error { get; set; }

        public ClanMemberAddResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanMemberRemoveRequest
    {
        public List<string> nameList { get; set; }

        public ClanMemberRemoveRequest()
        {
            this.nameList = new List<string>();
        }

        public void Add(string Name)
        {
            this.nameList.Add(Name);
        }
    }

    public class ClanMemberRemoveResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanMemberRemoveResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanJoiningRequest
    {
        public string clan { get; set; }
    }

    public class ClanJoiningResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanJoiningResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanViceUpdateRequest
    {
        public string username { get; set; }
        public string action { get; set; }
    }

    public class ClanViceUpdateResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanViceUpdateResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanChiefUpdateRequest
    {
        public string username { get; set; }        
    }

    public class ClanChiefUpdateResult : HorusResultCommon
    {
        public ClanResult Clan { get; set; }

        public ClanChiefUpdateResult()
        {
            this.Clan = new ClanResult();
        }
    }

    public class ClanDonateRequest
    {
        public string itemId { get; set; }
        public int number { get; set; }

        public ClanDonateRequest()
        {
            this.itemId = "";
            this.number = -1;
        }
    }

    public class UserDonate
    {
        public string OwnerName { get; set; }
        public string ItemId { get; set; }
        public string ItemIdModel { get; set; }
        public string ItemClass { get; set; }
        public string CatalogVersion { get; set; }
        public string DisplayName { get; set; }
        public string Expiration { get; set; }
        public string RemainingUses { get; set; }
        public string CustomData { get; set; }
        public string accessories { get; set; }
        public string EquipGroup { get; set; }
        public string UseCount { get; set; }
        public string status { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
        public string _id { get; set; }
    }
    
    public class ClanWarehouse
    {
        public string _id { get; set; }
        public string OwnerName { get; set; }
        public string ItemId { get; set; }
        public string ItemIdModel { get; set; }
        public string ItemClass { get; set; }
        public string CatalogVersion { get; set; }
        public string DisplayName { get; set; }
        public string Expiration { get; set; }
        public string RemainingUses { get; set; }
        public string CustomData { get; set; }
        public string accessories { get; set; }
        public string EquipGroup { get; set; }
        public string UseCount { get; set; }
        public string status { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
    }

    public class ClanDonateResult : HorusResultCommon
    {
        public List<UserDonate> clanDonate { get; set; }
        public List<ClanWarehouse> clanWarehouse { get; set; }

        public ClanDonateResult()
        {
            this.clanDonate = new List<UserDonate>();
            this.clanWarehouse = new List<ClanWarehouse>();
        }
    }

	public class RoleListRequest
	{
		public string type { get; set; }
		public string listBy { get; set; }
		public string order { get; set; }
		public string page { get; set; }
	}

	public class RoleListResult : HorusResultCommon
	{
		public List<RoleInfo> role { get; set; }
	}
	public class RoleInfo{
		public string roleId { get; set; }
		public RoleInfoBenefit benefit { get; set; }
		public RoleInfoRequired required { get; set; }
	}
	public class RoleInfoBenefit{
		public int exp_percent { get; set; }
		public int coin_percent { get; set; }
		public int gold_percent { get; set; }
		public SpinResult items { get; set; }
	}
	public class RoleInfoRequired{
		public Dictionary<string,int> charge { get; set; }

	}


	public class SpinResult
	{
		public Dictionary<string,ItemSlot> items { get; set; }
	}

	public class ItemSlot
	{
		public string ItemId { get; set; }
		public string rate { get; set; }
		public ItemSlotNumber number { get; set; }
	}
	public class ItemSlotNumber
	{
		public int min { get; set; }
		public int max { get; set; }
	}

	//thuc hien quest
	public class GetQuestProcessRequest
	{
		public string questId { get; set; }
	}
	public class GetQuestProcessResult: HorusResultCommon
	{
		public QuestProcessResult quest { get; set; }
	}
	//danh sach quest
	public class QuestProcessResult
	{
		public List<ItemInstance> itemsOwn { get; set; }
		public string questProcess { get; set; }
		public int startDay { get; set; }
	}
	//danh sach quest
	public class GetQuestSerialRequest
	{
		public string serial { get; set; }
	}
	public class GetQuestSerialResult:HorusResultCommon
	{
		public List<QuestInfo> questList { get; set; }
	}
	public class QuestInfo:IComparable<QuestInfo>
	{
		public string questId { get; set; }
		public string displayName { get; set; }
		public string description { get; set; }

		public SpinResult reward { get; set; }
		public QuestInfoRequire required { get; set; }

		public int CompareTo(QuestInfo other ) {
			return questId.CompareTo(other.questId);
		}
	}
	public class QuestInfoRequire
	{
		public Dictionary<string,int> dailyGame { get; set; }
	}

	//room update
	public class RoomUpdateRequest
	{
		public string apiKey { get; set; }
		public string serverId { get; set; }
		public string lobbyId { get; set; }
		public string roomId { get; set; }
		public int playerCount { get; set; }
		public Dictionary<string,Dictionary<string,string>> players { get; set; }
		public ExitGames.Client.Photon.Hashtable customParams { get; set; }
	}

	public class RoomUpdateResult: HorusResultCommon
	{
	}

	//room static
	public class RoomStaticRequest
	{
		public string serverId { get; set; }
		public string lobbyId { get; set; }
	}

	public class RoomStaticResult: HorusResultCommon
	{
		public Dictionary<string, Dictionary<string,int>> roomStatic { get; set; }
	}
		
	//room profile
	public class RoomProfileRequest
	{
		//khong can truyen thong tin
	}

	public class RoomProfileResult: HorusResultCommon
	{
		public Dictionary<string,RP_ServerProfile> roomStatic { get; set; }
	}
	public class RP_ServerProfile
	{
		public string name { get; set;}
		public Dictionary<string,RP_LobbyProfile> lobbyList { get; set; }
	}
	public class RP_LobbyProfile
	{
		public string name { get; set;}
		public string ip { get; set;}
		public int max { get; set;}
	}
}
