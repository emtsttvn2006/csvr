﻿using Horus;

namespace Horus.Internal
{
	public class Log
	{
		public static void Debug(string text, params object[] args)
		{
			if ((HorusSettings.LogLevel & HorusLogLevel.Debug) != 0)
			{
				UnityEngine.Debug.Log(HorusUtil.timeStamp + " DEBUG: " + HorusUtil.Format(text, args));
			}
		}

		public static void Info(string text, params object[] args)
		{
			if ((HorusSettings.LogLevel & HorusLogLevel.Info) != 0)
			{
				UnityEngine.Debug.Log(HorusUtil.timeStamp + " INFO: " + HorusUtil.Format(text, args));
			}
		}

		public static void Warning(string text, params object[] args)
		{
			if ((HorusSettings.LogLevel & HorusLogLevel.Warning) != 0)
			{
				UnityEngine.Debug.LogWarning(HorusUtil.timeStamp + " WARNING: " + HorusUtil.Format(text, args));
			}
		}

		public static void Error(string text, params object[] args)
		{
			if ((HorusSettings.LogLevel & HorusLogLevel.Error) != 0)
			{
				UnityEngine.Debug.LogError(HorusUtil.timeStamp + " ERROR: " + HorusUtil.Format(text, args));
			}
		}
	}
}
