﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
#if !UNITY_WSA && !UNITY_WP8
using System.IO;
using Ionic.Zlib;
#endif

namespace Horus.Internal
{
	internal class HorusWww : IHorusHttp
	{
		private int _pendingWwwMessages = 0;

		public void Awake() { }
		public void Update() { }

		public void Post(CallRequestContainer requestContainer)
		{
			HorusHttp.instance.StartCoroutine(MakeRequestViaUnity(requestContainer));
		}

		public int GetPendingMessages()
		{
			return _pendingWwwMessages;
		}

		// This is the old Unity WWW class call.
		private IEnumerator MakeRequestViaUnity(CallRequestContainer requestContainer)
		{
			_pendingWwwMessages += 1;
			string fullUrl = HorusSettings.GetFullUrl(requestContainer.UrlPath);
			byte[] payload = Encoding.UTF8.GetBytes(requestContainer.Data);

			#if UNITY_4_4 || UNITY_4_3 || UNITY_4_2 || UNITY_4_2 || UNITY_4_0 || UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5
			// Using hashtable for compatibility with Unity < 4.5
			Hashtable headers = new Hashtable ();
			#else
			Dictionary<string, string> headers = new Dictionary<string, string>();
			#endif

			#if !UNITY_WSA && !UNITY_WP8
			if (HorusSettings.CompressApiData)
			{
				headers.Add("Content-Encoding", "GZIP");
				headers.Add("Accept-Encoding", "GZIP");

				using (var stream = new MemoryStream())
				{
					using (
						GZipStream zipstream = new GZipStream(stream, CompressionMode.Compress,
							CompressionLevel.BestCompression))
					{
						zipstream.Write(payload, 0, payload.Length);
					}
					payload = stream.ToArray();
				}
			}
			#endif

			headers.Add("Content-Type", "application/json");
			
			if (requestContainer.AuthType != null) {
				headers.Add (requestContainer.AuthType, requestContainer.AuthKey);
			}


			WWW www = new WWW(fullUrl, payload, headers);
			HorusSettings.InvokeRequest(requestContainer.UrlPath, requestContainer.CallId, requestContainer.Request, requestContainer.CustomData);

			yield return www;

			requestContainer.ResultStr = null;
			requestContainer.Error = null;


			string finalWwwText = "";

			#if !UNITY_WSA && !UNITY_WP8
			if (HorusSettings.CompressApiData)
			{
				try
				{
					var stream = new MemoryStream(www.bytes);
					using (var gZipStream = new GZipStream(stream, CompressionMode.Decompress, false))
					{
						var buffer = new byte[4096];
						using (var output = new MemoryStream())
						{
							var read = 0;
							while ((read = gZipStream.Read(buffer, 0, buffer.Length)) > 0)
							{
								output.Write(buffer, 0, read);
							}
							output.Seek(0, SeekOrigin.Begin);
							var streamReader = new System.IO.StreamReader(output);
							finalWwwText = streamReader.ReadToEnd();
						}
					}
				}
				catch
				{
					// if this was not a valid GZip response, then send the message back as text to the call back.
					requestContainer.Error = HorusHttp.GeneratePfError(HttpStatusCode.PreconditionFailed, HorusErrorCode.Unknown, www.text);
					finalWwwText = www.text;
				}
			}
			else
			{
			#endif
				finalWwwText = www.text;
				#if !UNITY_WSA && !UNITY_WP8
			}
				#endif

			//string debug = string.Format("www.text: {0}", www.text);
			//UnityEngine.Sys (finalWwwText);

			// get the cookie and keep it
			if (www.responseHeaders.ContainsKey ("SET-COOKIE")) {
				requestContainer.AuthKey = www.responseHeaders ["SET-COOKIE"];
				//Debug.Log("requestContainer.AuthKey "+ requestContainer.AuthKey );
			}



			requestContainer.ResultStr = finalWwwText;

			requestContainer.InvokeCallback();

			_pendingWwwMessages -= 1;
		}
	}
}
