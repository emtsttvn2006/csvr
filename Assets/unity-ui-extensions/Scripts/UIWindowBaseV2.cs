﻿/// Credit GXMark, alexzzzz, CaoMengde777, TroyDavis  
/// Original Sourced from (GXMark) - http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/page-2#post-1834806 (with corrections)
/// Scaling fixed for non overlay canvases (alexzzzz) - http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/#post-1780612
/// Canvas border fix (CaoMengde777) - http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/#post-1781658
/// Canvas reset position fix (TroyDavis)- http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/#post-1782214


using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    /// <summary>
    /// Includes a few fixes of my own, mainly to tidy up duplicates, remove unneeded stuff and testing. (nothing major, all the crew above did the hard work!)
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("UI/Extensions/UI Window Base")]
    public class UIWindowBaseV2 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public Camera CurrentEventCamera { get; set; }
        RectTransform m_transform = null;
        private bool _isDragging = false;
        public  bool ResetCoords = false;
        private Vector3 m_originalCoods = Vector3.zero;
        public RectTransform m_canvasRectTransform;


        // Use this for initialization
        void Start()
        {
            m_transform = GetComponent<RectTransform>();
            m_originalCoods = m_transform.position;
//			m_canvasRectTransform = m_transform.GetComponentInParent<RectTransform>();
        }

        void Update()
        {
            if (ResetCoords)
                resetCoordinatePosition();

			if (Input.touchCount == 2 && _isDragging)
			{
				// Store both touches.
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne = Input.GetTouch(1);
				
				// Find the position in the previous frame of each touch.
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
				
				// Find the magnitude of the vector (the distance) between the touches in each frame.
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
				
				// Find the difference in the distances between each frame.
				float deltaMagnitudeDiff = -1*( prevTouchDeltaMag - touchDeltaMag);
				
				
				// Otherwise change the field of view based on the change in distance between the touches.
				transform.localScale += new Vector3(deltaMagnitudeDiff,deltaMagnitudeDiff, deltaMagnitudeDiff); ;
				
				// Clamp the field of view to make sure it's between 0 and 180.
				//transform.localScale = Mathf.Clamp(transform.localScale, 0.5f, 3f);
				transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x, 0.5F, 3.0F), Mathf.Clamp(transform.localScale.y, 0.5F, 3.0F), Mathf.Clamp(transform.localScale.z, 0.5F, 3.0F));
			}
        }

		bool isScreenPointInsideRectTransform( Vector2 screenPoint, RectTransform transform, Camera canvasCamera )
		{
			Vector2 localPoint;
			RectTransformUtility.ScreenPointToLocalPointInRectangle( transform, screenPoint, canvasCamera, out localPoint );
			return transform.rect.Contains( localPoint );
		}
        public void OnDrag(PointerEventData eventData)
        {
            if (_isDragging)
            {
	            CurrentEventCamera = eventData.pressEventCamera ?? CurrentEventCamera;

	            // We get the local position of the joystick
	            Vector3 worldJoystickPosition;
				if(isScreenPointInsideRectTransform( eventData.position,m_canvasRectTransform,CurrentEventCamera)){

					RectTransformUtility.ScreenPointToWorldPointInRectangle( m_transform, eventData.position, CurrentEventCamera, out worldJoystickPosition );
					m_transform.position = worldJoystickPosition;
				}

            }
        }

        //Note, the begin drag and end drag aren't actually needed to control the drag.  However, I'd recommend keeping it in case you want to do somethind else when draggging starts and stops
        public void OnBeginDrag(PointerEventData eventData)
        {

            if (eventData.pointerCurrentRaycast.gameObject == null)
                return;

            if (eventData.pointerCurrentRaycast.gameObject.name == name)
            {
                _isDragging = true;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _isDragging = false;
        }

        void resetCoordinatePosition()
        {
			transform.localScale = Vector3.one;
            m_transform.position = m_originalCoods;
            ResetCoords = false;
        }
    }
}