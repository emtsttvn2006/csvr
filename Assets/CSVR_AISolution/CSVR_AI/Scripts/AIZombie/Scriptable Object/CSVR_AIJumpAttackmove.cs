﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

/// <summary>
/// sử dụng đòn thế này, AI sẽ di chuyển đến 1 vị trí và gây damage tại đó bằng cách instantiate ra 1 prefab gây damage
/// </summary>
public class CSVR_AIJumpAttackmove : CSVR_AIAttackmove {

	public enum JumpType
	{
		Teleport,
		Parabol,
		Straight
	}
		
	public JumpType jumpType = JumpType.Parabol;

	IEnumerator<float> checkLandingcoroutine;

	/// <summary>
	/// pass cái này vào delegate CurrentAttackmoveDelegate
	/// </summary>
	/// <param name="baseScript">Base script.</param>
	public override void AttackMove(CSVR_AIBaseScript baseScript)
	{
		baseScript.Navi.autoTraverseOffMeshLink = true;
		baseScript.AIOffMeshLink.endTransform = baseScript.TargetToChase.transform;
		baseScript.AIOffMeshLink.activated = true;
		checkLandingcoroutine = checkLanding (baseScript);
		Timing.RunCoroutine (checkLandingcoroutine);
		Timing.RunCoroutine (checkLandingcoroutine);
	}

	/// <summary>
	/// Sets the bullet info.
	/// </summary>
	/// <param name="info">Info.</param>
	/// <param name="atkMove">Atk move.</param>
	protected override void SetBulletInfo(CSVR_AIBaseScript baseScript, CSVR_HitscanBullet info, CSVR_AIAttackmove atkMove)
	{
		
		info.Shooter_PlayfabID = "AIPlayfabID";
		info.Shooter_Name = baseScript.transform.root.name;
		info.GunID = "AIGunID";
		info.GunName = "AIGunName";
		info.Level = "10";
		info.idTeam = baseScript.MyTargetInfo.TeamID;
		info.SetSource(baseScript.transform.root);
		info.DamTest = atkMove.Damage;
		info.Range = atkMove.EffectiveRange;
		info.idPhotonView = -1;

	}

	RaycastHit groundHit;
	int groundCheckMask = (vp_Layer.Default | vp_Layer.Ragdoll);
	bool isGrounded (CSVR_AIBaseScript baseScript)
	{
		if (Physics.Raycast (baseScript.transform.position, Vector3.down, 0.1f, groundCheckMask))
		{
			return true;
		}
		else
			return false;
	}

	IEnumerator<float> checkLanding(CSVR_AIBaseScript baseScript)
	{
		yield return Timing.WaitForSeconds (0.25f);
		if(isGrounded(baseScript) == true)
		{
			yield break;
		}
	}

	IEnumerator<float> doAttack(CSVR_AIBaseScript baseScript)
	{
		yield return Timing.WaitUntilDone (checkLanding(baseScript));
		SpawnBullet (baseScript);
		baseScript.AnimationScript.PlayClip (AttackAnimationClipName);
		SpawnBullet (baseScript);
		LastTimeUsed = Time.time;
		baseScript.AIOffMeshLink.activated = false;
	}
		
	protected override void SpawnBullet (CSVR_AIBaseScript baseScript)
	{
		foreach(Transform spawner in baseScript.BulletSpawnersTransform)
		{
			spawner.rotation = Quaternion.LookRotation (baseScript.TargetToChase.HeartTransform.position - spawner.position);
			GameObject bullet = (GameObject)vp_Utility.Instantiate (Bullet, spawner.position, spawner.rotation);
			CSVR_HitscanBullet bulletinfo = bullet.GetComponent<CSVR_HitscanBullet> ();
			SetBulletInfo(baseScript, bulletinfo, this);
		}
	}
}
