﻿using UnityEngine;
using System.Collections;

/// <summary>
/// dùng chiêu này, AI sẽ tạo ra 1 prefab có khả năng gây damage, prefab đó có thể castray hoặc phóng tới mục tiêu và gây damage
/// </summary>
public class CSVR_AIProjectileAttackmove : CSVR_AIAttackmove {

	/// <summary>
	/// pass cái này vào delegate CurrentAttackmoveDelegate
	/// </summary>
	/// <param name="baseScript">Base script.</param>
	public override void AttackMove(CSVR_AIBaseScript baseScript){
		baseScript.AnimationScript.PlayClip (AttackAnimationClipName);
		SpawnBullet (baseScript);
		LastTimeUsed = Time.time;
	}


	/// <summary>
	/// Sets the bullet info.
	/// </summary>
	/// <param name="info">Info.</param>
	/// <param name="atkMove">Atk move.</param>
	protected override void SetBulletInfo(CSVR_AIBaseScript baseScript, CSVR_HitscanBullet info, CSVR_AIAttackmove atkMove)
	{

		info.Shooter_PlayfabID = "AIPlayfabID";
		info.Shooter_Name = baseScript.transform.root.name;
		info.GunID = "AIGunID";
		info.GunName = "AIGunName";
		info.Level = "10";
		info.idTeam = baseScript.MyTargetInfo.TeamID;
		info.SetSource(baseScript.transform.root);
		info.DamTest = atkMove.Damage;
		info.Range = atkMove.EffectiveRange;
		info.idPhotonView = -1;

	}

	protected override void SpawnBullet (CSVR_AIBaseScript baseScript)
	{
		foreach(Transform spawner in baseScript.BulletSpawnersTransform)
		{
			spawner.rotation = Quaternion.LookRotation (baseScript.TargetToChase.HeartTransform.position - spawner.position);
			GameObject bullet = (GameObject)vp_Utility.Instantiate (Bullet, spawner.position, spawner.rotation);
			CSVR_HitscanBullet bulletinfo = bullet.GetComponent<CSVR_HitscanBullet> ();
			SetBulletInfo(baseScript, bulletinfo, this);
		}
	}

}
