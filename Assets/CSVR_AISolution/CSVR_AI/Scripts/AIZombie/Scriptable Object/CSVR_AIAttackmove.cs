﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public abstract class CSVR_AIAttackmove : ScriptableObject
{

	public float Cooldown = 2f;
	public float Damage = 20f;
	public float EffectiveRange = 3f;
	public float Duration = 2f;

	public string AttackAnimationClipName;
	public GameObject Bullet;

	public float LastTimeUsed = 0f;

	public bool AvailableToCast
	{
		get
		{
			if (Time.time > (LastTimeUsed + Cooldown))
				return true;
			else
				return false;
		}
	}

	/// <summary>
	/// pass cái này vào delegame CurrentAttackmoveDelegate
	/// </summary>
	/// <param name="baseScript">Base script.</param>
	public abstract void AttackMove(CSVR_AIBaseScript baseScript);

	protected abstract void SetBulletInfo (CSVR_AIBaseScript baseScript, CSVR_HitscanBullet info, CSVR_AIAttackmove atkMove);

	protected abstract void SpawnBullet (CSVR_AIBaseScript baseScript);
}
