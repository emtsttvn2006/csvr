﻿using UnityEngine;
using System.Collections;

public interface CSVR_IAIState
{
	string name ();

	void Register ();

	void UpdateState ();

	void ToPatrolState ();

	void ToChaseState ();

	void ToIdleState ();

	void ToDieState ();

	void ToAttackState ();

	void ToAlertState ();
}
