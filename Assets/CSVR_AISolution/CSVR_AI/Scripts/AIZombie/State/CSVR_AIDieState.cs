﻿using UnityEngine;
using System.Collections;

public class CSVR_AIDieState : MonoBehaviour, CSVR_IAIState
{

	CSVR_AITargetInfo MyTargetInfo;
	CSVR_AIAnimationScript AnimationScript;
	CSVR_AIBaseScript BaseScript;

	void Awake()
	{

		BaseScript = GetComponent<CSVR_AIBaseScript> ();
		MyTargetInfo = BaseScript.MyTargetInfo;
		AnimationScript = BaseScript.AnimationScript;

		//		Register ();
	}

	//interface
	public string name()
	{
		return "Die";
	}

	public void Register()
	{
		BaseScript.DieState = this;
	}

	public void UpdateState ()
	{
	}

	public void ToPatrolState ()
	{
	}

	public void ToChaseState ()
	{
		BaseScript.Navi.stoppingDistance = BaseScript.AttackRange;
		BaseScript.Navi.speed = BaseScript.ChaseSpeed;
		BaseScript.CurrentState = BaseScript.ChaseState;
	}

	public void ToIdleState()
	{
	}

	public void ToDieState ()
	{
	}

	public void ToAttackState ()
	{
	}


	public void ToAlertState ()
	{
		BaseScript.CurrentState = BaseScript.AlertState;
	}
}