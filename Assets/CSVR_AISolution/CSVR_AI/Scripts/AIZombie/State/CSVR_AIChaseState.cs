﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AIChaseState : MonoBehaviour, CSVR_IAIState
{

	CSVR_AIBaseScript BaseScript;
	CSVR_AITargetInfo MyTargetInfo;
	CSVR_AIAnimationScript AnimationScript;

	void Awake()
	{
		
		BaseScript = GetComponent<CSVR_AIBaseScript> ();
		MyTargetInfo = BaseScript.MyTargetInfo;
		AnimationScript = BaseScript.AnimationScript;

//		Register ();
	}


	void Chase()
	{
		BaseScript.Navi.SetDestination (BaseScript.TargetToChase.transform.position);
		BaseScript.Navi.Resume ();
		AnimationScript.PlayChaseClip ();
	}


	RaycastHit Hit;
	/// <summary>
	/// AI sẽ liên tục nhìn vào kẻ địch mình đang chase, nếu không nhìn thấy kẻ địch này nữa thì bắt đầu mất dấu vết
	/// </summary>
	void PointAtTarget()
	{
		if (Time.time >= BaseScript.LastTimeSeenTarget + BaseScript.FocusTargetDuration)
		{
			ToAlertState ();
		}
			
		BaseScript.EyeTransform.rotation = Quaternion.LookRotation (BaseScript.CurrentNoticedTarget.HeartTransform.position - BaseScript.EyeTransform.position);
		//nếu nhìn thấy cái gì đó
		if (Physics.Raycast (BaseScript.EyeTransform.position, BaseScript.EyeTransform.forward, out Hit, BaseScript.VisionRange, BaseScript.EyeVisionMask))
		{
			//nếu nhìn trúng thân người
			if (Hit.collider.gameObject.layer != vp_Layer.Ragdoll)//neu khong nhin trung nguoi
				return;
			//nếu người nhìn trúng là địch
			if (!MyTargetInfo.EnemiesTeamIDs.Contains (Hit.transform.root.gameObject.GetComponent<CSVR_AITargetInfo> ().TeamID))
				return;
			BaseScript.TargetToChase = BaseScript.CurrentNoticedTarget;
			BaseScript.LastTimeSeenTarget = Time.time;
		}

	}



	void CheckAttack()
	{
		if(Vector3.Distance(transform.position, BaseScript.TargetToChase.transform.position) < BaseScript.AttackRange)
		{
			BaseScript.Navi.Stop ();
			ToAttackState ();
		}
	}


	//interface
	public string name()
	{
		return "Chase";
	}

	public void Register()
	{
		BaseScript.ChaseState = this;
	}

	public void UpdateState ()
	{
		PointAtTarget ();
		Chase ();
		CheckAttack ();
	} 

	public void ToPatrolState ()
	{
		BaseScript.Navi.speed = BaseScript.PatrolSpeed;
		BaseScript.CurrentState = BaseScript.PatrolState;
	}

	public void ToChaseState ()
	{
		Debug.LogError ("Cannot transition to same state" + name());
	}

	public void ToIdleState()
	{
	}

	public void ToDieState ()
	{
	}

	public void ToAttackState ()
	{
		BaseScript.CurrentState = BaseScript.AttackState;
	}

	public void ToAlertState ()
	{
		BaseScript.LastTimeSearchTarget = Time.time;
		BaseScript.CurrentState = BaseScript.AlertState;
	}
}