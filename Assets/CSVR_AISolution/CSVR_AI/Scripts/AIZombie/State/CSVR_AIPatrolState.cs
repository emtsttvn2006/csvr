﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AIPatrolState : MonoBehaviour, CSVR_IAIState
{
	CSVR_AIBaseScript BaseScript;
	CSVR_AITargetInfo MyTargetInfo;

	CSVR_AIAnimationScript AnimationScript;

	private int nextPatrolPoint = 0;

	public CSVR_AIPatrolState(CSVR_AIBaseScript myBase)
	{
		BaseScript = myBase;
	}

	void Awake()
	{
		
		BaseScript = GetComponent<CSVR_AIBaseScript> ();
		MyTargetInfo = BaseScript.MyTargetInfo;
		AnimationScript = BaseScript.AnimationScript;

//		Register ();
		currentClosest = BaseScript.ScanRadius;
		FOV = BaseScript.FieldOfView / 2f;
	}

	/// <summary>
	/// AI sẽ tuần tra lần lượt các điểm trong list PatrolPoints
	/// </summary>
	void Patrol()
	{
		if (BaseScript.PatrolPoints.Length < 1)
		{
			ToIdleState ();
		}
		BaseScript.Navi.destination = BaseScript.PatrolPoints[nextPatrolPoint].position;
		BaseScript.Navi.Resume ();

		if(BaseScript.Navi.remainingDistance <= BaseScript.Navi.stoppingDistance && !BaseScript.Navi.pathPending)
		{
			nextPatrolPoint = (nextPatrolPoint + 1) % BaseScript.PatrolPoints.Length;
		}

		AnimationScript.PlayRunClip ();
	}
		
	RaycastHit Hit;
	CSVR_AITargetInfo TargetInfoSeen;
	float FOV;
	/// <summary>
	/// AI sẽ liên tục nhìn vào kẻ địch đang được chú ý, nếu không bị che lấp thì sẽ chuyển sang chase kẻ địch này
	/// </summary>
	void Look()
	{
		if (BaseScript.CurrentNoticedTarget == null)
			return;
		
		BaseScript.EyeTransform.rotation = Quaternion.LookRotation (BaseScript.CurrentNoticedTarget.HeartTransform.position - BaseScript.EyeTransform.position);
		//nếu nhìn thấy cái gì đó
		if (Physics.Raycast (BaseScript.EyeTransform.position, BaseScript.EyeTransform.forward, out Hit, BaseScript.VisionRange, BaseScript.EyeVisionMask))
		{
			//nếu nhìn trúng thân người
			if (Hit.collider.gameObject.layer != vp_Layer.Ragdoll)//neu khong nhin trung nguoi
				return;
			//nếu người nhìn trúng là địch
			if (!MyTargetInfo.EnemiesTeamIDs.Contains (Hit.transform.root.gameObject.GetComponent<CSVR_AITargetInfo> ().TeamID))
				return;
			BaseScript.LastTimeSeenTarget = Time.time;
			ToAlertState ();
		}
	}


	float currentDistance;
	float currentClosest;
	/// <summary>
	/// Scan: AI sẽ tìm kiếm kẻ địch gần nhất và chú ý đến kẻ địch này, chưa có bất kì hành động nào
	/// </summary>
	void Scan()
	{

		foreach(CSVR_AITargetInfo lala in CSVR_AIController.AITargetInfoList)
		{
			currentDistance = Vector3.Distance (transform.position, lala.transform.position);
			if(MyTargetInfo.EnemiesTeamIDs.Contains(lala.TeamID) && currentDistance < BaseScript.ScanRadius && currentDistance < currentClosest)
			{
				currentClosest = currentDistance;
				BaseScript.CurrentNoticedTarget = lala;
//				print ("tim thay enemy");
			}
		}
	}


	//Interface
	public string name()
	{
		return "Patrol";
	}

	public void Register()
	{
		BaseScript.PatrolState = this;
	}

	public void UpdateState ()
	{
		Scan ();
		Look ();
		Patrol ();
	}

	public void ToPatrolState ()
	{
		Debug.LogError ("Cannot transition to same state" + name());
	}

	public void ToChaseState ()
	{
		BaseScript.Navi.stoppingDistance = BaseScript.AttackRange;
		BaseScript.Navi.speed = BaseScript.ChaseSpeed;
		BaseScript.CurrentState = BaseScript.ChaseState;
	}

	public void ToIdleState ()
	{
	}

	public void ToDieState ()
	{
	}

	public void ToAttackState ()
	{
	}

	public void ToAlertState ()
	{
		BaseScript.LastTimeSearchTarget = Time.time;
		BaseScript.CurrentState = BaseScript.AlertState;
	}
}