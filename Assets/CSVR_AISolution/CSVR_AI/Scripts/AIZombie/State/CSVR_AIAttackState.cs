﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class CSVR_AIAttackState : MonoBehaviour, CSVR_IAIState
{

	List<CSVR_AIAttackmove> NormalAttackMoves = new List<CSVR_AIAttackmove> ();
	List<CSVR_AIAttackmove> UltimateAttackMoves = new List<CSVR_AIAttackmove> ();

	List<CSVR_AIAttackmove> NormalAttackMovesAvailable = new List<CSVR_AIAttackmove> ();
	List<CSVR_AIAttackmove> UltimateAttackMovesAvailable = new List<CSVR_AIAttackmove> ();

	CSVR_AITargetInfo MyTargetInfo;
	CSVR_AIBaseScript BaseScript;

	Transform BodyAttackTransform;
	CSVR_AIAnimationScript AnimationScript;
	GameObject BulletObject;

	bool isAttacking = false;


	void Awake ()
	{
		
		BaseScript = GetComponent<CSVR_AIBaseScript> ();
		MyTargetInfo = BaseScript.MyTargetInfo;
		AnimationScript = BaseScript.AnimationScript;

//		Register ();

		foreach (CSVR_AIAttackmove atkmove in BaseScript.NormalAttackMoves)
		{
			NormalAttackMoves.Add ((CSVR_AIAttackmove)Instantiate(atkmove));
		}

		foreach (CSVR_AIAttackmove atkmove in BaseScript.UltimateAttackMoves)
		{
			UltimateAttackMoves.Add ((CSVR_AIAttackmove)Instantiate(atkmove));
		}

		BodyAttackTransform = BaseScript.BodyAttackTransform == null ? transform : BaseScript.BodyAttackTransform;
	}


	public CSVR_AIAttackmove CurrentAttackmove = null;
	CSVR_AIAttackmove LastAttackmove = null;
	/// <summary>
	/// Gets a value indicating whether this instance can attack.
	/// </summary>
	/// <value><c>true</c> if this instance can attack; otherwise, <c>false</c>.</value>
	bool CanAttack
	{
		get
		{ 
			if(CurrentAttackmove == null)
			{
				return false;
			}

			if (Vector3.Distance (BodyAttackTransform.position, BaseScript.TargetToChase.transform.position) > BaseScript.AttackRange)
			{
				ToChaseState ();
				return false;
			}
			else
				return true;
		}
	}
		
	float LastTimeAttack = 0f;
	float AttackDuration = 0f;
	delegate void DoAttackMoveDelegate(CSVR_AIBaseScript scrpt);
	DoAttackMoveDelegate CurrentAttackmoveDelegate;

	/// <summary>
	/// Check cooldown va thuc hien attack
	/// </summary>
	void Attack()
	{
		if (BaseScript.TargetToChase == null)
			return;
		
		if(Time.time > LastTimeAttack + BaseScript.AttackCooldown + AttackDuration)
		{
//			Debug.Log ("move = " + NormalAttackMovesAvailable.Count + ", doAtk func = " + doAttackMoveDelegate);
			if(CanAttack)
			{
				BaseScript.Navi.Stop ();
				CurrentAttackmoveDelegate (BaseScript);
				AttackDuration = CurrentAttackmove.Duration;
				CurrentAttackmove = null;
				LastTimeAttack = Time.time;
			}
		}

	}


	IEnumerator<float> doAttack()
	{
		
		yield return 0f;
	}
		
	/// <summary>
	/// Updates the attackmove.
	/// </summary>
	void UpdateAttackmove()
	{
		UpdateNormalAttackmoveAvailableList ();
		UpdateUltimateAttackmoveAvailableList ();
		CurrentAttackmove = GetNextAttackMove ();
	}
		
	CSVR_AIAttackmove _atkMove;
	/// <summary>
	/// Gets the next attack move.
	/// </summary>
	/// <returns>The next attack move.</returns>
	CSVR_AIAttackmove GetNextAttackMove()
	{
		if (UltimateAttackMovesAvailable.Count > 0)
		{
			_atkMove = UltimateAttackMovesAvailable [Random.Range (0, UltimateAttackMovesAvailable.Count)];
			CurrentAttackmoveDelegate = _atkMove.AttackMove;
			return _atkMove;
		}

		if (NormalAttackMovesAvailable.Count > 0)
		{
			_atkMove = NormalAttackMovesAvailable [Random.Range (0, NormalAttackMovesAvailable.Count)];
			CurrentAttackmoveDelegate = _atkMove.AttackMove;
			return _atkMove;
		}
		else
			return null;
	}
		
	/// <summary>
	/// Updates the normal attack move available list.
	/// </summary>
	void UpdateNormalAttackmoveAvailableList()
	{
		foreach(CSVR_AIAttackmove atkMove in NormalAttackMoves)
		{
			if (atkMove.AvailableToCast)
			{
				if (!NormalAttackMovesAvailable.Contains (atkMove))
					NormalAttackMovesAvailable.Add (atkMove);
			}
			else
			{
				if (NormalAttackMovesAvailable.Contains (atkMove))
					NormalAttackMovesAvailable.Remove (atkMove);
			}
		}
	}


	/// <summary>
	/// Updates the ultimave attackmove available list.
	/// </summary>
	void UpdateUltimateAttackmoveAvailableList()
	{
		foreach(CSVR_AIAttackmove atkMove in UltimateAttackMoves)
		{
			if (atkMove.AvailableToCast)
			{
				if (!UltimateAttackMovesAvailable.Contains (atkMove))
					UltimateAttackMovesAvailable.Add (atkMove);
			}
			else
			{
				if (UltimateAttackMovesAvailable.Contains (atkMove))
					UltimateAttackMovesAvailable.Remove (atkMove);
			}
		}
	}

	/// <summary>
	/// AI sẽ quay người sang hướng mục tiêu
	/// </summary>
	void AimToAttack()
	{
		if (BaseScript.TargetToChase == null)
			return;
		BaseScript.EyeTransform.rotation = Quaternion.LookRotation (BaseScript.TargetToChase.HeartTransform.position - BaseScript.EyeTransform.position);

		BodyAttackTransform.rotation = Quaternion.LookRotation (BaseScript.TargetToChase.transform.position - BodyAttackTransform.position);
	}


	void CheckTarget()
	{
		if(BaseScript.TargetToChase == null)
		{
			BaseScript.Navi.Stop ();
			ToAlertState ();
		}
	}


	//interface
	public string name()
	{
		return "Attack";
	}

	public void Register()
	{
		BaseScript.AttackState = this;
	}


	public void UpdateState ()
	{
		CheckTarget ();
		UpdateAttackmove ();
		AimToAttack ();
		Attack ();
	}

	public void ToPatrolState ()
	{
		BaseScript.Navi.speed = BaseScript.PatrolSpeed;
		BaseScript.CurrentState = BaseScript.PatrolState;
	}

	public void ToChaseState ()
	{
		BaseScript.Navi.stoppingDistance = BaseScript.AttackRange;
		BaseScript.Navi.speed = BaseScript.ChaseSpeed;
		BaseScript.CurrentState = BaseScript.ChaseState;
	}

	public void ToIdleState()
	{
	}

	public void ToDieState ()
	{
	}

	public void ToAttackState ()
	{
		Debug.LogError ("Cannot transition to same state" + name());
	}
		
	public void ToAlertState ()
	{
		BaseScript.LastTimeSearchTarget = Time.time;
		BaseScript.CurrentState = BaseScript.AlertState;
	}
}