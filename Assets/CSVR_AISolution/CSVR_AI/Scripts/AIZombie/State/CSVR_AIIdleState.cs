﻿using UnityEngine;
using System.Collections;

public class CSVR_AIIdleState : MonoBehaviour, CSVR_IAIState
{


	CSVR_AITargetInfo MyTargetInfo;
	CSVR_AIAnimationScript AnimationScript;
	CSVR_AIBaseScript BaseScript;

	void Awake()
	{

		BaseScript = GetComponent<CSVR_AIBaseScript> ();
		MyTargetInfo = BaseScript.MyTargetInfo;
		AnimationScript = BaseScript.AnimationScript;

		//		Register ();
	}

	void Idle()
	{
		
	}

	//interface
	public string name()
	{
		return "Idle";
	}

	public void Register()
	{
		BaseScript.IdleState = this;
	}

	public void UpdateState ()
	{
		
	}

	public void ToPatrolState ()
	{
		BaseScript.CurrentState = BaseScript.PatrolState;
	}

	public void ToChaseState ()
	{
		BaseScript.Navi.stoppingDistance = BaseScript.AttackRange;
		BaseScript.Navi.speed = BaseScript.ChaseSpeed;
		BaseScript.CurrentState = BaseScript.ChaseState;
	}

	public void ToIdleState()
	{
		Debug.LogError ("Cannot transition to same state" + name());
	}

	public void ToDieState ()
	{
	}


	public void ToAttackState ()
	{
	}


	public void ToAlertState ()
	{
		BaseScript.CurrentState = BaseScript.AlertState;
	}
}