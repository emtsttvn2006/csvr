﻿using UnityEngine;
using System.Collections;

public class CSVR_AIAlertState : MonoBehaviour, CSVR_IAIState
{

	CSVR_AITargetInfo MyTargetInfo;
	CSVR_AIAnimationScript AnimationScript;
	CSVR_AIBaseScript BaseScript;

	void Awake()
	{
		
		BaseScript = GetComponent<CSVR_AIBaseScript> ();
		MyTargetInfo = BaseScript.MyTargetInfo;
		AnimationScript = BaseScript.AnimationScript;

//		Register ();
	}


	RaycastHit Hit;
	/// <summary>
	/// AI sẽ liên tục "nhìn" vào mục tiêu bị chú ý bằng chiếu raycast
	/// </summary>
	void PointAtTarget()
	{
		if (BaseScript.CurrentNoticedTarget == null)
			return;

		BaseScript.EyeTransform.rotation = Quaternion.LookRotation (BaseScript.CurrentNoticedTarget.HeartTransform.position - BaseScript.EyeTransform.position);
		//nếu nhìn thấy cái gì đó
		if (Physics.Raycast (BaseScript.EyeTransform.position, BaseScript.EyeTransform.forward, out Hit, BaseScript.VisionRange, BaseScript.EyeVisionMask))
		{
			//nếu nhìn trúng thân người
			if (Hit.collider.gameObject.layer != vp_Layer.Ragdoll)//neu khong nhin trung nguoi
				return;
			//nếu người nhìn trúng là địch
			if (!MyTargetInfo.EnemiesTeamIDs.Contains (Hit.transform.root.gameObject.GetComponent<CSVR_AITargetInfo> ().TeamID))
				return;
			BaseScript.LastTimeSeenTarget = Time.time;
			ToChaseState ();
		}
	}

	bool isSearching = false;
	/// <summary>
	/// Đếm ngược việc bắn liên tục ray vào mục tiều, sẽ dừng và chuyển qua Patrol nếu hết thời gian tìm mà vẫn không bắn trúng
	/// </summary>
	void Search()
	{
		if (Time.time >= BaseScript.LastTimeSearchTarget + BaseScript.SearchDuration)
		{
			ToPatrolState ();
		}
		BaseScript.Navi.Stop ();
		AnimationScript.PlaySearchClip ();
	}

	void CheckTarget()
	{
		if(BaseScript.CurrentNoticedTarget == null)
		{
			BaseScript.Navi.Stop ();
			ToPatrolState ();
		}
	}


	//interface
	public string name()
	{
		return "Alert";
	}

	public void Register()
	{
		BaseScript.AlertState = this;
	}

	public void UpdateState ()
	{
		CheckTarget ();
		PointAtTarget ();
		Search ();
	}

	public void ToPatrolState ()
	{
		BaseScript.Navi.speed = BaseScript.PatrolSpeed;
		BaseScript.CurrentState = BaseScript.PatrolState;
	}

	public void ToChaseState ()
	{
		BaseScript.Navi.stoppingDistance = BaseScript.AttackRange;
		BaseScript.Navi.speed = BaseScript.ChaseSpeed;
		BaseScript.CurrentState = BaseScript.ChaseState;
	}

	public void ToIdleState()
	{
	}

	public void ToDieState ()
	{
	}

	public void ToAttackState ()
	{
		
	}


	public void ToAlertState ()
	{
		Debug.LogError ("Cannot transition to same state" + name());
	}
}