﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AIBaseScript : MonoBehaviour {
	public CSVR_IAIState CurrentState;

	[HideInInspector] public CSVR_AIIdleState IdleState;
	[HideInInspector] public CSVR_AIChaseState ChaseState;
	[HideInInspector] public CSVR_AIAlertState AlertState;
	[HideInInspector] public CSVR_AIPatrolState PatrolState;
	[HideInInspector] public CSVR_AIDieState DieState;
	[HideInInspector] public CSVR_AIAttackState AttackState;

	[HideInInspector] public NavMeshAgent Navi;
	[HideInInspector] public OffMeshLink AIOffMeshLink;
	[HideInInspector] public CSVR_AITargetInfo MyTargetInfo;
	[HideInInspector] public CSVR_AIAnimationScript AnimationScript;

	public CSVR_AITargetInfo CurrentNoticedTarget;
	public CSVR_AITargetInfo TargetToChase;

	#if UNITY_EDITOR
	[Header("Thông số di chuyển")]
	#endif
	public float PatrolSpeed = 3f;
	public float ChaseSpeed = 7f;
	public Transform[] PatrolPoints;

	#if UNITY_EDITOR
	[vp_Separator]	public vp_Separator s2;
	[Header("Thông số tìm kiếm")]
	#endif
	public Transform EyeTransform;
	public float ScanRadius = 40f;
	public float VisionRange = 50f;
	[Range(0f, 360f)]public float FieldOfView = 170f;

	[HideInInspector] public float LastTimeSeenTarget = 0f;
	[HideInInspector] public float LastTimeSearchTarget = 0f;
	public float FocusTargetDuration = 5f;
	public float SearchDuration = 3f;

	#if UNITY_EDITOR
	[vp_Separator]	public vp_Separator s3;
	[Header("Thông số tấn công")]
	#endif
	[Tooltip("Danh sách đòn đánh bình thường")]
	public CSVR_AIAttackmove[] NormalAttackMoves;
	[Tooltip("Danh sách đòn đánh đặc biệt")]
	public CSVR_AIAttackmove[] UltimateAttackMoves;
	public float AttackRange = 2f;
	public float AttackCooldown = 1f;
	public Transform[] BulletSpawnersTransform;
	public Transform BodyAttackTransform;


	#if UNITY_EDITOR
	[vp_Separator]	public vp_Separator s4;
	[Header("Thông số cho chu kì update trạng thái của AI")]
	#endif
	public float AICycleTime = 0.5f;


	[HideInInspector]public int EyeVisionMask = (1 << vp_Layer.Ragdoll | 1 << vp_Layer.Default);

	void Awake(){
		Navi = GetComponent<NavMeshAgent> ();
		Navi.autoTraverseOffMeshLink = false;
		AIOffMeshLink = GetComponent<OffMeshLink> ();
		AIOffMeshLink.activated = false;
		AnimationScript = GetComponent<CSVR_AIAnimationScript> ();
		MyTargetInfo = GetComponent<CSVR_AITargetInfo> ();

		PatrolState = gameObject.AddComponent<CSVR_AIPatrolState>();
		IdleState = gameObject.AddComponent<CSVR_AIIdleState>();
		ChaseState = gameObject.AddComponent<CSVR_AIChaseState>();
		AlertState = gameObject.AddComponent<CSVR_AIAlertState>();
		DieState = gameObject.AddComponent<CSVR_AIDieState>();
		AttackState = gameObject.AddComponent<CSVR_AIAttackState>();

	}

	void Start()
	{
		CurrentState = IdleState;
		StartCoroutine (UpdateState());
	}


	IEnumerator UpdateState()
	{
		while(CurrentState != null)
		{
			yield return new WaitForSeconds (AICycleTime);
			CurrentState.UpdateState ();
			Debug.Log (CurrentState.name());
		}
	}

	void Die()
	{
		
	}

}
