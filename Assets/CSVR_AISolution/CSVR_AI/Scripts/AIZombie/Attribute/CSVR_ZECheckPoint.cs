﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(CSVR_ZEGate))]
public class CSVR_ZECheckPoint : MonoBehaviour 
{
	public CSVR_ZEGate Gate;
	public Transform NewSpawnPointPosition;
	public GameObject PlayerSpawnPoint;

	public enum CheckPointType
	{
		Trigger,
		Destroy
	}

	public CheckPointType checkPointType = CheckPointType.Destroy;
	// Use this for initialization
	void Awake ()
	{
		if(checkPointType == CheckPointType.Trigger)
		{
			Collider thisCol = GetComponent<Collider> ();
			if(thisCol)
				thisCol.isTrigger = true;

			CSVR_AutotargetMarker marker = GetComponent<CSVR_AutotargetMarker> ();
			if (marker)
				Destroy (marker);
		}

		if(PlayerSpawnPoint == null)
		{
			PlayerSpawnPoint = GameObject.FindWithTag ("Police");
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.transform.CompareTag ("PolicePlayer") || other.transform.CompareTag ("TerrorisPlayer"))
		{
			vp_DamageHandler damageHandler = GetComponent<vp_DamageHandler> ();
			if (damageHandler)
			{
				damageHandler.Damage (9999);
				if(damageHandler.CurrentHealth <= 0)
					PlayerSpawnPoint.transform.position = NewSpawnPointPosition.position;
			}
		}
	}
}
