﻿using UnityEngine;
using System.Collections;
using ParagonAI;
using System.Collections.Generic;

public class CSVR_ZBDamageHandler : vp_DamageHandler {

	//	public Transform[] AIGunList;
	//	int CurrentGunIndex;
	//	public Transform CurrentGun;

	public CSVR_AutotargetMarker autotargetScript;

	public bool isInvicible = false;
	public GameObject hitEffect;

	Animator animBody;

	PhotonView myPhotonView;

	protected override void Awake()
	{
		if(autotargetScript == null)
			autotargetScript = GetComponent<CSVR_AutotargetMarker> ();
		if(myPhotonView == null)
			myPhotonView = GetComponent<PhotonView> ();

		somauBiMat = 0;
		if (MaxAmor > 0)
		{
			checkArmor = true;
		}
		m_Audio = GetComponent<AudioSource>();

		CurrentHealth = MaxHealth;
		autotargetScript.CurrentHP = CurrentHealth/MaxHealth * 100f;
		CurrentAmor = MaxAmor;
		CheckForObsoleteParams();
		Collider col = GetComponent<Collider>();
		if(!Instances.ContainsKey(col) && Instances.ContainsValue(this))
			Instances.Add(col, this);
	}

	public override void Damage(float damage)
	{
		CurrentHealth = Mathf.Min(CurrentHealth - damage, MaxHealth);
		CurrentHealth = Mathf.Max (CurrentHealth, 0f);
		CurrentAmor = Mathf.Min(CurrentAmor - DamageAmor, MaxAmor);
		AntiCheat_CurrentHealth = CurrentHealth; 
		if (CurrentHealth <= 0.0f)
		{
			SendMessage("Die",SendMessageOptions.DontRequireReceiver);
		}
	}


	public override void Damage(vp_DamageInfo damageInfo)
	{
		if (isInvicible)
			return;
		if(autotargetScript.TeamNumber != 0 && autotargetScript.TeamNumber == damageInfo.team)
			return;
		if (!enabled)
			return;
		if (!vp_Utility.IsActive(gameObject))
			return;
		if (!vp_Gameplay.isMaster)
			return;
		if (CurrentHealth <= 0.0f)
			return ;

		if (damageInfo != null)
		{
			if (damageInfo.Source != null)
				Source = damageInfo.Source;
			if (damageInfo.OriginalSource != null)
				OriginalSource = damageInfo.OriginalSource;
			//            Debug.Log("Damage! Source: " + damageInfo.Source + " ... " + "OriginalSource: " + damageInfo.OriginalSource);
		}

		// if we somehow shot ourselves with a bullet, ignore it
		if ((damageInfo.Type == vp_DamageInfo.DamageType.Bullet) && (m_Source == Transform))
			return;
		if(hitEffect != null)
			vp_Utility.Instantiate (hitEffect, damageInfo.vec, Quaternion.identity);
		CurrentHealth = Mathf.Min(CurrentHealth - damageInfo.Damage, MaxHealth);
		CurrentHealth = Mathf.Max (CurrentHealth, 0f);

		CurrentAmor = Mathf.Min(CurrentAmor - DamageAmor, MaxAmor);

		//Horus begin

		AntiCheat_CurrentHealth = CurrentHealth; //Chong cheat
		//Horus end

		autotargetScript.CurrentHP =  CurrentHealth/MaxHealth * 100f;
		SyncHP ();


		/*	// in multiplayer, report damage for score tracking purposes
		if (vp_Gameplay.isMultiplayer && (damageInfo.Source != null))
		{

			vp_GlobalEvent<Transform, Transform, float>.Send("TransmitDamage", Transform.root, damageInfo.OriginalSource, damageInfo.Damage);
		}
		*/
		// detect and transmit death as event
		if (CurrentHealth <= 0.0f)
		{
			//			Debug.Log (transform.name + " Mau = 0");
//			if (vp_MPMaster.Instance != null) 
//			{
//				//				Debug.Log (transform.name + " call TransmitDeathInfo");
//				vp_MPMaster.Instance.TransmitDeathInfo(damageInfo.ShooterPlayfabID, damageInfo.ShooterName,damageInfo.GunID,
//					autotargetScript.transform.root.name,strNamePlayer,
//					damageInfo.HitPart, damageInfo.Damage, damageInfo.Level, damageInfo.GunName,
//					damageInfo.team, autotargetScript.TeamNumber, damageInfo.idphoton, myPhotonView.viewID);
//			}
			SendMessage("Die",SendMessageOptions.DontRequireReceiver);
		}

	}

	public void SyncHP()
	{
		//Truyen thong tin mau bi mat sang remote
		//		if (syncLocal == null)
		//		{
		//			syncLocal = GetComponent<CSVR_AISyncLocal> ();
		//		}
		//		syncLocal.currentHealth = CurrentHealth;

		if (autotargetScript == null)
		{
			autotargetScript = GetComponent<CSVR_AutotargetMarker> ();
		}
		autotargetScript.CurrentHP = CurrentHealth/MaxHealth * 100f;
	}

	public override void Die()
	{
		if (PhotonNetwork.isMasterClient)
		{
			print ("THE BOSS HAS DIED!");
			vp_MPMaster.Instance.photonView.RPC("Receive_EndRound", PhotonTargets.All, true, true);
		}

		if (!enabled || !vp_Utility.IsActive(gameObject))
			return;

		if (m_Audio != null)
		{
			m_Audio.pitch = Time.timeScale;
			m_Audio.PlayOneShot(DeathSound);
		}

		foreach (GameObject o in DeathSpawnObjects)
		{
			if (o != null)
			{               
				GameObject g = (GameObject)vp_Utility.Instantiate(o, Transform.position, Transform.rotation);
				if ((Source != null) && (g != null))
					vp_TargetEvent<Transform>.Send(g.transform, "SetSource", OriginalSource);
			}
		}

		if (Respawner == null)
		{
			vp_Utility.Destroy(gameObject);
		}
		else
		{
			RemoveBulletHoles();
		}

		m_InstaKill = false;

				if (vp_Gameplay.isMultiplayer && vp_Gameplay.isMaster)
				{
					vp_GlobalEvent<Transform>.Send("TransmitKill", transform.root);
				}
	}



}

