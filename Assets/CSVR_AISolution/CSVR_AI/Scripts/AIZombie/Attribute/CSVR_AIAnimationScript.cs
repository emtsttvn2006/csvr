﻿using UnityEngine;
using System.Collections;

public class CSVR_AIAnimationScript : MonoBehaviour {

	public string IdleClipName = "Idle";
	public string RunClipName = "Run";
	public string AttackClipName = "Attack";
	public string DieClipName = "Die";
	public string WalkClipName = "Run";
	public string SearchClipName = "Search";
	public string ChaseClipName = "Chase";
	public string AlertClipName = "Alert";

	Animation Anim;

	void Awake()
	{
		Anim = GetComponent<Animation> ();
	}

	public void PlayRunClip()
	{
		if (!Anim.IsPlaying(RunClipName))
			Anim.Play (RunClipName);
	}

	public void PlayIdleClip()
	{
		if (!Anim.IsPlaying(IdleClipName))
			Anim.Play (IdleClipName);
	}

	public void PlayAttackClip()
	{
		if (!Anim.IsPlaying(AttackClipName))
			Anim.Play (AttackClipName);
	}

	public void PlayDieClip()
	{
		if (!Anim.IsPlaying(DieClipName))
			Anim.Play (DieClipName);
	}

	public void PlayWalkClip()
	{
		if (!Anim.IsPlaying(WalkClipName))
			Anim.Play (WalkClipName);
	}

	public void PlaySearchClip()
	{
		if (!Anim.IsPlaying(SearchClipName))
			Anim.Play (SearchClipName);
	}

	public void PlayChaseClip()
	{
		if (!Anim.IsPlaying(ChaseClipName))
			Anim.Play (ChaseClipName);
	}

	public void PlayAlertClip()
	{
		if (!Anim.IsPlaying(AlertClipName))
			Anim.Play (AlertClipName);
	}

	public void PlayClip(string lele)
	{
		Anim.Play (lele, PlayMode.StopAll);
	}

}
