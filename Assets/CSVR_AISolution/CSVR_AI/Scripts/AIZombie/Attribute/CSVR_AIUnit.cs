﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ParagonAI;
using Horus;
using Horus.ClientModels;
using Horus.Internal;
using Horus.Json;

public class CSVR_AIUnit : MonoBehaviour
{
	[System.Serializable]
	public class AIGunList
	{
		public Transform GunTransform;
		public AudioClip BulletSound;
	}
	public int TeamID;
	public int Shots = 0;									// amount of times the player has spawned projectiles. used for establishing deterministic
	public int _HeadShot=0;
	public bool DeadActive = false;
	CSVR_GunInfo gunManager;
	CatalogItem item;
	int DroppedGunID = 0;
	public GunScript AIGunScript;
	string strWeaponPrefabID;
	public Transform CurrentGun;
	public int BulletCount;
	public string strWeaponName;
	public string GunID;
	Animator animBody;
	public AIGunList[] aIGunList;
//	public Transform[] AIGunList;
	Collider RootCollider;

	public Transform ShieldEffect;
	public bool isInvicible = false;
	public float ShieldEffectDuration;

	[HideInInspector] public Collider[] BodyColliders;
	[HideInInspector] public Rigidbody[] BodyRigidbodies;

//	public static Dictionary<Transform, CSVR_AIUnit> m_AIEnemy = null;
//	///<summary>
//	/// kienbb: tao dictionary cho AI enemy, su dung trong autotarget
//	/// </summary>
//	public static Dictionary<Transform, CSVR_AIUnit> AIPlayer
//	{
//		get{ 
//			if (m_AIEnemy == null)
//				m_AIEnemy = new Dictionary<Transform, CSVR_AIUnit> ();
//			return m_AIEnemy;
//		}
//	}

	public CSVR_AutotargetMarker autotargetScript;

	//endkien

	public virtual void Awake()
	{
		animBody = GetComponentInChildren<Animator> ();
		BodyColliders = GetComponentsInChildren<Collider> ();
		BodyRigidbodies = GetComponentsInChildren<Rigidbody> ();
		RootCollider = GetComponent<Collider> ();
		if(autotargetScript != null)
		{
			autotargetScript.AddAutotarget ();
		}
		foreach(Rigidbody lele in BodyRigidbodies)
		{
			lele.isKinematic = true;
		}
	}

	public virtual void Start()
	{
	}

	public virtual void Die()
	{
		if(CurrentGun != null)
			CurrentGun.gameObject.SetActive (false);
		DeadActive = true;
		autotargetScript.DeadActive = true;
		DisableAllRigidBody ();
		AIDropWeapon (GunID);
//		RemoveFromAIPlayerList ();
		autotargetScript.RemoveAutotarget ();
//		Debug.Log (transform.root.name + " has died!");
	}

//	public virtual void AddToAIEnemyList()
//	{
//		if (!AIPlayer.ContainsKey (transform.root) && !AIPlayer.ContainsValue (this)) {
//			AIPlayer.Add(transform.root, this);
//		}
//	}
//
//	public virtual void RemoveFromAIPlayerList()//bo di khoi auto target
//	{
//		AIPlayer.Remove(transform.root);
//	}

	public void AIDropWeapon(string nameWeapon)
	{
//		Debug.Log (transform.root.name + " drops weapon " + nameWeapon);
		if (CSVR_GameSetting.shopItems == null || CSVR_GameSetting.shopItems.Count == 0) {
			return;
		}


		if (nameWeapon == "") return;
		item = CSVR_GameSetting.FindItemInListShopItems(CSVR_GameSetting.shopItems, nameWeapon);
		try
		{
			gunManager = HorusSimpleJson.DeserializeObject<CSVR_GunInfo>(item.CustomData.ToString());	
		}
		catch
		{
		}



		switch (item.ItemClass)
		{
		case "Rifle.Weapon":
		case "Sniper.Weapon":
		case "SMG.Weapon":
		case "ShotGun.Weapon":
		case "Machine.Weapon":
		case "Pistol.Weapon":
			DroppedGunID++;

			GameObject objSung = Resources.Load (nameWeapon + "/" + nameWeapon) as GameObject;
			GameObject DroppedGun = GameObject.Instantiate (objSung, new Vector3 (transform.position.x, transform.position.y + 0.1f, transform.position.z), transform.rotation) as GameObject;
			SetWeaponInfo wi = DroppedGun.GetComponent<SetWeaponInfo> ();
//			BulletCount = int.Parse (gunManager.G4);//xem sau nha, bh set cung theo code
			wi.Count = BulletCount;//so dan hien tai
			wi.Capacity = BulletCount;//so dan trong 1 bang dan
			wi.InternalCount = BulletCount * 2;//tong so dan
			wi.DroppedGunID = DroppedGunID;
			wi.ClassGun = item.ItemClass;
			wi.PlayfabID = strWeaponPrefabID;

			wi.WeaponDamage = gunManager.G1;
			wi.WeaponRange = 500f;


			wi.AimAngle_Step1.x = gunManager.R1X; 
			wi.AimAngle_Step1.y = gunManager.R1Y;
			wi.AimAngleIncrease_Step1 = gunManager.R1;
			wi.AimAngle_Step2.x = gunManager.R2X;
			wi.AimAngle_Step2.y = gunManager.R2Y;
			wi.AimAngleIncrease_Step2 = gunManager.R2;
			wi.AimAngle_Step3.x = gunManager.R3X;
			wi.AimAngle_Step3.y = gunManager.R3Y;
			wi.AimAngleIncrease_Step3 = gunManager.R3;
			wi.AimAngleDecrease = gunManager.R0;
			wi.BulletAngleMin = gunManager.A1;
			wi.BulletAngleMax = gunManager.A2;
			wi.BulletAngleIncrease = gunManager.A3;
			wi.BulletAngleDecrease = gunManager.A2;

			wi.ReloadDuration =  float.Parse(gunManager.G3);
			wi.weightWeapon = gunManager.G5;
			wi.ProjectileFiringRate = gunManager.G2;
			wi.ProjectileTapFiringRate = gunManager.G2;


			break;
		default:
			break;
		}

	}

	void DisableAllRigidBody()
	{
		int i;
		for(i = 0; i < BodyRigidbodies.Length; i++)
		{
			BodyRigidbodies[i].isKinematic = false;
		}
		if(animBody)
			animBody.enabled = false;
		RootCollider.enabled = false;
//		print ("AI fall");
	}

	public void RebornAI()
	{
		int i;

		for(i = 0; i < BodyRigidbodies.Length; i++)
		{
			BodyRigidbodies[i].isKinematic = true;
		}
		if(animBody)
			animBody.enabled = true;	
//		AddToAIEnemyList ();
		autotargetScript.AddAutotarget ();
		DeadActive = false;
		autotargetScript.DeadActive = false;
		RootCollider.enabled = true;

		if (ShieldEffect)
		{
			ShieldEffect.gameObject.SetActive (true);
			isInvicible = true;
			Invoke ("DeactiveShieldEffect", ShieldEffectDuration);
		}
	}

	void DeactiveShieldEffect()
	{
		if(ShieldEffect)
		{
			ShieldEffect.gameObject.SetActive(false);
			isInvicible = false;
		}
	}



}

