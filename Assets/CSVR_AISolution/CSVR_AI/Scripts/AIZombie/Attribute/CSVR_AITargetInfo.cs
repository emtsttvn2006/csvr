﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AITargetInfo : MonoBehaviour {

	public int TeamID;
	public int[] AlliesTeamIDs;
	public int[] EnemiesTeamIDs;

	public CSVR_AITargetInfo(int teamID, Transform heartTransform)
	{
		TeamID = teamID;
		HeartTransform = heartTransform;
	}

	public Transform HeartTransform = null;
	//	public Transform CurrentTargetTransform = null;

	public virtual void Awake()
	{
		InvokeRepeating ("AddMeToTargetlist", 0f, 1f);
		if (!HeartTransform)
			HeartTransform = transform;
	}

	void AddMeToTargetlist()
	{
		if (CSVR_AIController.currentController != null)
		{
			CSVR_AIController.currentController.AddToTargetList (this);
			CancelInvoke ("AddMeToTargetlist");
		}
		//else
		//	print ("chua co Controller");
	}

	public virtual void OnDisable()
	{
		if (CSVR_AIController.currentController != null)
			CSVR_AIController.currentController.RemoveFromTargetList (this);
	}

	public virtual void OnDestroy()
	{
		if (CSVR_AIController.currentController != null)
			CSVR_AIController.currentController.RemoveFromTargetList (this);
	}


}
