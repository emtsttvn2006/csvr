﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_ZESpawnPointAssistant : MonoBehaviour {

	private vp_ZombieSurvival zombieModeInstance;
	private Transform myCheckPoint;

	private void Awake()
	{
		zombieModeInstance = (vp_ZombieSurvival)vp_MPMaster.Instance;
		CSVR_ZECheckPoint[] listCheckPoint = FindObjectsOfType (typeof(CSVR_ZECheckPoint)) as CSVR_ZECheckPoint[];

		List<CSVR_ZECheckPoint> tempCP = new List<CSVR_ZECheckPoint> ();
		foreach(CSVR_ZECheckPoint cp in listCheckPoint)
		{
			print ("CHECKPOINT ROUNDID: " + cp.Gate.RoundID);
			if (zombieModeInstance.RoundStart >= cp.Gate.RoundID)
			{
				print ("ROUNDSTART > ROUNDID");
				tempCP.Add (cp);
				print ("DA ADD CP NAY");
			}

		}

		print ("CHECKPOINT: " + tempCP.Count + " ALL: " + listCheckPoint.Length);
		CSVR_ZECheckPoint chosenCP = tempCP[0];

		foreach(CSVR_ZECheckPoint tcp in tempCP)
		{
			if(tcp.Gate.RoundID > chosenCP.Gate.RoundID)
			{
				chosenCP = tcp;
			}
		}
		print ("CHECKPOINT CHOSEN: " + chosenCP.transform.name);
		transform.position = chosenCP.NewSpawnPointPosition.position;
	}
}
