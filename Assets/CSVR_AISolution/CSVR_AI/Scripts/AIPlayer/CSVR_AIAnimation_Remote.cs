﻿using UnityEngine;
using System.Collections;

public class CSVR_AIAnimation_Remote : MonoBehaviour {

	private int currentAngleHash;
	private int engagingHash;
	private int crouchingHash;
	private int reloadingHash;
	private int meleeHash;
	private int fireHash;
	private int fireGrenadeHash;
	private int forwardsMoveHash;
	private int sidewaysMoveHash;
	private int sprintingHash;
	private int JumpHash;

	public Animator animator;
	public Vector2 animSyncInfo = Vector2.zero;
	public Transform GroundCheckTrasform;
	public float GroundCheckHeight = 0.2f;

	float animationDampTime = 0.15f;
	public float maxMovementSpeed = -1f;
	bool setHashes = false;

	public virtual void Awake()
	{
		animator = GetComponentInChildren<Animator> ();
		SetHashes();
	}

	void LateUpdate()
	{
		AnimateAI();
	}

	void AnimateAI()
	{
		animator.SetFloat(forwardsMoveHash, animSyncInfo.x, animationDampTime, Time.deltaTime);
		animator.SetFloat(sidewaysMoveHash, animSyncInfo.y, animationDampTime, Time.deltaTime);
	}

	public void PlayReloadAnimation()
	{
		animator.SetTrigger(reloadingHash);
	}

	public void PlayFiringAnimation()
	{
		animator.SetTrigger(fireHash);
	}

	public void PlayFiringGrenadeAnimation()
	{
		animator.SetTrigger(fireGrenadeHash);
	}

	public void PlayJumpAnimation()
	{
		animator.SetTrigger (JumpHash);
	}

	void SetHashes()
	{
		crouchingHash = Animator.StringToHash("Crouching");
		engagingHash = Animator.StringToHash("Engaging");
		reloadingHash = Animator.StringToHash("Reloading");
		meleeHash = Animator.StringToHash("Melee");
		fireHash = Animator.StringToHash("Fire");
		fireGrenadeHash = Animator.StringToHash("FireGrenade");
		sidewaysMoveHash = Animator.StringToHash("Horizontal");
		forwardsMoveHash = Animator.StringToHash("Forwards");
		sprintingHash = Animator.StringToHash("Sprinting");
		JumpHash = Animator.StringToHash("Jump");
		setHashes = true;
	}
}
