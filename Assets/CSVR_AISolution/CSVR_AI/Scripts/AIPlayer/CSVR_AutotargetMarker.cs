﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AutotargetMarker : MonoBehaviour 
{
	public int TeamNumber;
	public float CurrentHP;
	public string CurrentName = "Bot Unit";
	public bool DeadActive = false;
	public Transform MiddleBodyTransform;

	[HideInInspector] public Collider[] BodyColliders;
	[HideInInspector] public Rigidbody[] BodyRigidbodies;

	public static Dictionary<Transform, CSVR_AutotargetMarker> m_AIEnemy = null;
	public static Dictionary<Transform, CSVR_AutotargetMarker> AIPlayer
	{
		get{ 
			if (m_AIEnemy == null)
				m_AIEnemy = new Dictionary<Transform, CSVR_AutotargetMarker> ();
			return m_AIEnemy;
		}
	}

	void Awake()
	{
		BodyColliders = GetComponentsInChildren<Collider> ();
		BodyRigidbodies = GetComponentsInChildren<Rigidbody> ();
		AddAutotarget ();
	}

	public void AddAutotarget()
	{
		if (!AIPlayer.ContainsKey (transform) && !AIPlayer.ContainsValue (this)) {
			AIPlayer.Add(transform, this);
		}
	}	
	
	public void RemoveAutotarget()
	{
		AIPlayer.Remove(transform.root);
	}	

	void OnDestroy ()
	{
		RemoveAutotarget ();
	}

	void OnDisable ()
	{
//		RemoveAutotarget ();
	}
}
