﻿using UnityEngine;
using System.Collections;


public class CSVR_AISyncLocal : CSVR_AISync {

	public CSVR_AIAnimation_Local animationScript;
	public ParagonAI.RotateToAimGunScript rotateScript;

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (!stream.isWriting)
			return;
		stream.SendNext ((Vector3)transform.position);
		stream.SendNext ((float)rotateScript.RotationY);
		stream.SendNext ((string)currentWeapon);
		stream.SendNext ((float)currentHealth);
		stream.SendNext ((Vector2)animationScript.animSyncInfo);
	}
}
