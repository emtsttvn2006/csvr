﻿using UnityEngine;
using System.Collections;

public class CSVR_AIDamageHandler_Remote : vp_DamageHandler
{
	public CSVR_AIUnit aiunitScript;
	public GameObject hitEffect;

	public CSVR_AutotargetMarker autotargetScript;

	protected virtual void Awake()
	{
		base.Awake ();
	}


	public override void Damage(vp_DamageInfo damageInfo)
	{
//		if(GetComponent<CSVR_AIPlayerSetup>().TeamNumber != 0 && GetComponent<CSVR_AIPlayerSetup>().TeamNumber == damageInfo.team)
//			return;
//		if (!enabled)
//			return;
//		if (!vp_Utility.IsActive(gameObject))
//			return;
//		
//		if (CurrentHealth <= 0.0f)
//			return ;

		autotargetScript.CurrentHP = CurrentHealth/MaxHealth * 100f;

		if(GetComponent<CSVR_AIPlayerSetup>().enemyTeamIDs.Contains(damageInfo.team))
			vp_Utility.Instantiate (hitEffect, damageInfo.vec, Quaternion.identity);
	
//
//		CurrentHealth = Mathf.Min(CurrentHealth - damageInfo.Damage, MaxHealth);	
//		AntiCheat_CurrentHealth = CurrentHealth; //Chong cheat		
	}


	public override void Die()
	{
		aiunitScript.Die ();
	}


	protected override void Reset()
	{
		MaxHealth = 100f;
		autotargetScript.CurrentHP = 100f;
		base.Reset ();
	}

	public void ResetHp()
	{
		Reset ();
	}
}
