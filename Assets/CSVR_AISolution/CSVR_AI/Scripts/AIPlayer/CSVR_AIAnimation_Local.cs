﻿using UnityEngine;
using System.Collections;

public class CSVR_AIAnimation_Local : ParagonAI.AnimationScript 
{
	//sync cai nay
	public Vector2 animSyncInfo
	{
		get
		{
			return new Vector2 (animForward, animStrafe);
		}
	}

	public override void Awake() {

		myBaseScript = GetComponent<ParagonAI.BaseScript> ();
		myBaseScript.animationScript = this;
		myAIBodyTransform = GetComponent<CSVR_AIPlayerSetup> ().myAIBodyTransform;
		gunScript =  GetComponent<ParagonAI.GunScript> ();
		animator = myAIBodyTransform.GetComponent<Animator> ();
		rotateGunScript =  GetComponent<ParagonAI.RotateToAimGunScript> ();
		base.Awake ();
	}

	protected override void LateUpdate ()
	{
		base.LateUpdate ();
	}


}
