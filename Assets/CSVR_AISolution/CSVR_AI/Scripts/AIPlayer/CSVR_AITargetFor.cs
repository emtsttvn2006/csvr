﻿/// <summary>
/// kienbb: viet dua tren script TargetScript cua Paragon AI
/// </summary>
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ParagonAI;

public class CSVR_AITargetFor : TargetScript {

	protected float AddToAITargetListTimeout = 20f;
	protected float StartTimeAddToAITargetList;
	CSVR_AITargetInfo myZombieTargetInfo = null;

	public override void Awake ()
	{
		if (!targetObjectTransform)
			targetObjectTransform = transform;

		if (!myLOSTarget)
			myLOSTarget = targetObjectTransform;


		myTeamID = GetComponent<vp_MPNetworkPlayer> ().TeamNumber;
		targetPriority = 1f;
		switch (myTeamID) 
		{
		case 1:
			alliedTeamsIDs = new int[1]{1};
			enemyTeamsIDs = new int[1]{2};
			break;
		case 2:
			alliedTeamsIDs = new int[1]{2};
			enemyTeamsIDs = new int[1]{1};
			break;

		}
		Collider[] bodyParts = transform.GetComponentsInChildren<Collider> ();
		foreach(Collider part in bodyParts)
		{
			if(part.gameObject.tag == "Chest")
			{
				targetObjectTransform = part.transform;
				break;
			}
		}
		enabled = true;

		myZombieTargetInfo = gameObject.AddComponent<CSVR_AITargetInfo> ();
		myZombieTargetInfo.TeamID = myTeamID;
		myZombieTargetInfo.HeartTransform = targetObjectTransform;
		myZombieTargetInfo.AlliesTeamIDs = alliedTeamsIDs;
		myZombieTargetInfo.EnemiesTeamIDs = enemyTeamsIDs;
	}

	public override void Start()
	{
//		Transform Player = this.transform.root;

		myTeamID = GetComponent<vp_MPNetworkPlayer> ().TeamNumber;
		targetPriority = 1f;
		switch (myTeamID) 
		{
		case 1:
			alliedTeamsIDs = new int[1]{1};
			enemyTeamsIDs = new int[1]{2};
			break;
		case 2:
			alliedTeamsIDs = new int[1]{2};
			enemyTeamsIDs = new int[1]{1};
			break;

		}
		Collider[] bodyParts = transform.GetComponentsInChildren<Collider> ();
		foreach(Collider part in bodyParts)
		{
			if(part.gameObject.tag == "Chest")
			{
				targetObjectTransform = part.transform;
				break;
			}
		}
		enabled = true;

		StartTimeAddToAITargetList = Time.time;
		InvokeRepeating ("AddThisToTargetsList", 0f, 1f);
		InvokeRepeating ("AddThisToZombieTargetsList", 0f, 1f);
	}

	public virtual void AddThisToTargetsList()
	{
		
		if (ParagonAI.ControllerScript.currentController && AddedIntoList == false)
		{
			myUniqueID = ParagonAI.ControllerScript.currentController.AddTarget (myTeamID, targetObjectTransform, this);
			AddedIntoList = true;
			CancelInvoke ("AddThisToTargetsList");
		}

	}

	void AddThisToZombieTargetsList()
	{
		if (CSVR_AIController.currentController)
		{
			CSVR_AIController.currentController.AddToTargetList (myZombieTargetInfo);
			CancelInvoke ("AddThisToZombieTargetsList");
		}
	//	else
		//	print ("KHONG CO ZOMBIE COTROLLER");
	}

	void RemoveThisFromZombieTarget()
	{
		if (CSVR_AIController.currentController)
		{
			CSVR_AIController.currentController.RemoveFromTargetList (myZombieTargetInfo);
		}

	}

	public void Die ()
	{
		RemoveThisTargetFromPLay ();
		RemoveThisFromZombieTarget ();
	}

	public void Reset()
	{
//		print ("Player reset AITarget");
		AddThisToTargetsList ();
		AddThisToZombieTargetsList ();
		Awake ();
	}

	public override void ApplyDamage (float h)
	{
		
	}

}
	


