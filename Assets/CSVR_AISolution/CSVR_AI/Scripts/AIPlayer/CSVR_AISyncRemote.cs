﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class CSVR_AISyncRemote : CSVR_AISync
{
	public CSVR_AIPlayerSetup setupScript;
	public CSVR_AIDamageHandler_Remote damageHandler;
	public CSVR_AIAnimation_Remote animScript;
	public CSVR_AIUnit_Remote aiunitScript;
	public CSVR_AIPlayerSetup AISetupScript;

	public Transform myAIBodyTransform;

	public float currentRotY;
	public string newWeapon;

	bool isDead()
	{
		if (currentHealth <= 0)
			return true;
		else
			return false;
	}

	void Update()
	{
		if(!photonView.isMine)
		{
			try
			{
				transform.position = Vector3.Lerp(transform.position, currentPos, Time.deltaTime * 5);
				myAIBodyTransform.rotation = Quaternion.Lerp(myAIBodyTransform.rotation, currentRot, Time.deltaTime * 5);
				animSyncInfo = Vector2.Lerp (animScript.animSyncInfo, animSyncInfo, Time.deltaTime * 5);
			}
			catch
			{
			}

			if (currentWeapon != aiunitScript.GunID)
			{
				aiunitScript.GunID = currentWeapon;
				aiunitScript.SetWeapon (currentWeapon);
			}
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting)
			return;
		currentPos = (Vector3)stream.ReceiveNext();
		currentRotY = (float)stream.ReceiveNext();
		newWeapon = (string)stream.ReceiveNext ();
		currentHealth = (float)stream.ReceiveNext ();
		animSyncInfo = (Vector2)stream.ReceiveNext();

		currentRot.eulerAngles = new Vector3(0.0f, currentRotY, 0.0f);

		if(aiunitScript == null)
		{
			aiunitScript = GetComponent<CSVR_AIUnit_Remote> ();
		}
		currentWeapon = newWeapon;

		if(animScript == null)
		{
			animScript = GetComponent<CSVR_AIAnimation_Remote> ();
		}
		animScript.animSyncInfo = animSyncInfo;

		if (damageHandler == null)
		{
			damageHandler = GetComponent<CSVR_AIDamageHandler_Remote> ();
		}
		damageHandler.CurrentHealth = currentHealth;
		if (currentHealth <= 0f && isDead() == false)
		{
			damageHandler.Die ();
		}

		if (AISetupScript == null)
		{
			AISetupScript = GetComponent<CSVR_AIPlayerSetup> ();
		}
		AISetupScript.CurrentHP = currentHealth;

	}


}
