﻿using UnityEngine;
using System.Collections;

public class CSVR_AIMPNetworkPlayer : vp_MPNetworkPlayer 
{

	CSVR_AIPlayerSetup AISetupScript;

	public override void Awake()
	{	
		AISetupScript = GetComponent<CSVR_AIPlayerSetup> ();
		TeamNumber = AISetupScript.TeamNumber;
		Debug.Log (Players.Count + " " + PlayersByID.Count + " " + IDs.Count);

		if(!PlayersByID.ContainsKey(ID))
		{
			PlayersByID.Add (ID, this);
		}


		base.Awake ();
	}


	public override void Start()
	{


	}

	public override void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		
	}



	protected override void ForceSyncWeapon(PhotonStream stream)
	{
		
	}


	public override void InitShooters()
	{
		
	}
}
