﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ParagonAI;

public class CSVR_AIPlayerSetup : MonoBehaviour
{
	[System.Serializable]
	public class MachineGunsAI
	{
		public string GunName;
		public AudioClip BulletSound;
	}

	public Transform myAIBodyTransform;
	public Transform spineBone;
	public Transform targetTransform;

	public int TeamNumber;
	public int[] allyTeamIDs;
	public int[] enemyTeamIDs;

	public Transform gunBone;
	public Transform BulletSpawnTransform;

	public MachineGunsAI[] machineGunsAI;
	public GameObject MuzzleFlashPrefab;
	public GameObject bulletPrefab;
	public GameObject GrenadePrefab;
	public GameObject HitEffectPrefab;//prefab toe mau
	public GameObject BulletTracer;
	public AudioClip BulletSound;
	public AudioClip ReloadSound;

	public Transform ShieldEffect;
	public float ShieldEffectDuration = 4f;

	public float HP = 100f;
	public string SpawnPointTag;

	public float CurrentHP;
	public string CurrentID = "Bot Player";
	public string CurrentName = "Bot Player";

	PhotonView myPhotonView;

	List<vp_SpawnPoint> myFirstSpawnPoint = new List<vp_SpawnPoint>();

	CSVR_AutotargetMarker autotargetScript;

	void Awake() 
	{
//		if (PhotonNetwork.isMasterClient)
//		{
//			GenerateName ();
//		}
		myPhotonView = GetComponent<PhotonView> ();
		UnityEngine.Object.DontDestroyOnLoad(gameObject);


		if(PhotonNetwork.isMasterClient)
		{
			vp_SpawnPoint mySpawnPoint =  vp_SpawnPoint.SpawnPoints[Random.Range(0, vp_SpawnPoint.SpawnPoints.Count)];
			transform.position = mySpawnPoint.transform.position;
//			print ("AI spawns at " + vp_SpawnPoint.SpawnPoints.Count + " positions");
		}

	}

	// Use this for initialization
	void Start () 
	{
		
		myPhotonView.onSerializeTransformOption = OnSerializeTransform.PositionAndRotation;
		myPhotonView.ObservedComponents = new List<Component>();
		myPhotonView.synchronization = ViewSynchronization.UnreliableOnChange;
		myPhotonView.ownerId = -1000;

		if (PhotonNetwork.isMasterClient)
		{

			CSVR_AISyncLocal syncLocal = gameObject.AddComponent<CSVR_AISyncLocal> ();
			myPhotonView.ObservedComponents.Add(syncLocal);


			AudioSource audioSource = gameObject.GetComponent<AudioSource> ();

			NavMeshAgent Navi = gameObject.AddComponent<NavMeshAgent> ();
			Navi.angularSpeed = 9999f;
			Navi.acceleration = 30f;

			ParagonAI.BaseScript baseScript = gameObject.AddComponent<ParagonAI.BaseScript> ();


			CSVR_AIRotateToAimGun_Local rotateScript = gameObject.AddComponent<CSVR_AIRotateToAimGun_Local> ();
			baseScript.headLookScript = rotateScript;
			syncLocal.rotateScript = rotateScript;

			CSVR_AIGun_Local gunScript = gameObject.AddComponent<CSVR_AIGun_Local> ();
			baseScript.gunScript = gunScript;
			gunScript.audioSource = audioSource;
			gunScript.reloadSound = ReloadSound;
			gunScript.bulletObject = bulletPrefab;
			gunScript.secondaryFireObject = GrenadePrefab;
			gunScript.bulletSpawn = BulletSpawnTransform;
			gunScript.muzzleFlash = MuzzleFlashPrefab;

			CSVR_AIHealth_Local healthScript = gameObject.AddComponent<CSVR_AIHealth_Local> ();
			healthScript.myAIBaseScript = baseScript;
			healthScript.rotateToAimGunScript = rotateScript;
			healthScript.animator = myAIBodyTransform.GetComponent<Animator> ();
			healthScript.gunScript = gunScript;
			SetHP (healthScript);

			ParagonAI.SoundScript soundScript = gameObject.AddComponent<ParagonAI.SoundScript> ();
			baseScript.audioScript = soundScript;

			ParagonAI.CoverFinderScript coverScript = gameObject.AddComponent<ParagonAI.CoverFinderScript> ();
			baseScript.coverFinderScript = coverScript;

			CSVR_AIAnimation_Local animScript = gameObject.AddComponent<CSVR_AIAnimation_Local> ();
			baseScript.animationScript = animScript;
			gunScript.animationScript = animScript;
			syncLocal.animationScript = animScript;

			CSVR_AITarget_Local targetScript = gameObject.AddComponent<CSVR_AITarget_Local> ();
			gunScript.AITargetScript = targetScript;

			CSVR_AIDamageHandler damageHandler = gameObject.AddComponent<CSVR_AIDamageHandler> ();
			gunScript.AIInfo = damageHandler;
			damageHandler.hitEffect = HitEffectPrefab;
			SetHP (damageHandler);

			CSVR_AILocalPlayer localAI = gameObject.AddComponent<CSVR_AILocalPlayer> ();
			localAI.ShieldEffect = ShieldEffect;
			localAI.ShieldEffectDuration = ShieldEffectDuration;

			CSVR_AIUnit.AIGunList[] tempGun = new CSVR_AIUnit.AIGunList[machineGunsAI.Length];
			for(int i = 0; i < machineGunsAI.Length; i++)
			{
				tempGun [i] = new CSVR_AIUnit.AIGunList ();
				GameObject newGun = Instantiate (Resources.Load ("Weapon/Remote/Third/" + machineGunsAI[i].GunName) as GameObject);
				newGun.name = machineGunsAI[i].GunName;
				newGun.transform.SetParent (gunBone);
				newGun.transform.localPosition = Vector3.zero;
				newGun.transform.localRotation = Quaternion.identity;
				tempGun[i].BulletSound = machineGunsAI[i].BulletSound;
				tempGun[i].GunTransform = newGun.transform;
			}

			localAI.aIGunList = tempGun;

			gunScript.AILocalPlayerInfo = localAI;
			damageHandler.AILocalPlayerInfo = localAI;

			CSVR_AIRespawner respawnerScript = gameObject.AddComponent<CSVR_AIRespawner> ();
			respawnerScript.SpawnPointTag = SpawnPointTag;
			respawnerScript.m_SpawnMode = vp_Respawner.SpawnMode.SpawnPoint;
			respawnerScript.m_ObstructionSolver = vp_Respawner.ObstructionSolver.AdjustPlacement;

			autotargetScript = gameObject.AddComponent<CSVR_AutotargetMarker> ();
			autotargetScript.TeamNumber = TeamNumber;
			autotargetScript.CurrentName = CurrentName;
			autotargetScript.CurrentHP = 100f;
			if(autotargetScript.MiddleBodyTransform == null)
			autotargetScript.MiddleBodyTransform = spineBone;
			damageHandler.autotargetScript = autotargetScript;
			localAI.autotargetScript = autotargetScript;
			respawnerScript.autotargetScript = autotargetScript;


		}
		else 
		{
			CSVR_AISyncRemote syncRemote = gameObject.AddComponent<CSVR_AISyncRemote> ();
			syncRemote.myAIBodyTransform = myAIBodyTransform;
			myPhotonView.ObservedComponents.Add(syncRemote);

			AudioSource audioSource = gameObject.GetComponent<AudioSource> ();

			CSVR_AIUnit_Remote aiunitScript = gameObject.AddComponent<CSVR_AIUnit_Remote> ();
			syncRemote.aiunitScript = aiunitScript;
			aiunitScript.ShieldEffect = ShieldEffect;
			aiunitScript.ShieldEffectDuration = ShieldEffectDuration;

			CSVR_AIUnit.AIGunList[] tempGun = new CSVR_AIUnit.AIGunList[machineGunsAI.Length];
			for(int i = 0; i < machineGunsAI.Length; i++)
			{
				tempGun [i] = new CSVR_AIUnit.AIGunList ();
				GameObject newGun = Instantiate (Resources.Load ("Weapon/Remote/Third/" + machineGunsAI[i].GunName) as GameObject);
				newGun.name = machineGunsAI[i].GunName;
				newGun.transform.SetParent (gunBone);
				newGun.transform.localPosition = Vector3.zero;
				newGun.transform.localRotation = Quaternion.identity;
				tempGun[i].BulletSound = machineGunsAI[i].BulletSound;
				tempGun[i].GunTransform = newGun.transform;
			}
			aiunitScript.aIGunList = tempGun;

			CSVR_AIDamageHandler_Remote damageHandlerScript = gameObject.AddComponent<CSVR_AIDamageHandler_Remote> ();
			damageHandlerScript.aiunitScript = aiunitScript;
			damageHandlerScript.hitEffect = HitEffectPrefab;

			CSVR_AIAnimation_Remote animScript = gameObject.AddComponent<CSVR_AIAnimation_Remote> ();

			CSVR_GunScript_Remote gunScript = gameObject.AddComponent<CSVR_GunScript_Remote> ();
			gunScript.MuzzleFlashPrefab = MuzzleFlashPrefab;
			gunScript.MuzzleFlashTransform = gunBone.FindChild ("MuzzleFlash");
			gunScript.ReloadSound = ReloadSound;
			gunScript.myAudioSource = audioSource;
			gunScript.animationScript = animScript;
			gunScript.BulletTracerPrefab = BulletTracer;
			gunScript.secondaryFireObject = GrenadePrefab;
			aiunitScript.gunScript = gunScript;

			CSVR_AIRespawner_Remote respawnerRemote = gameObject.GetComponent<CSVR_AIRespawner_Remote> ();
			respawnerRemote.aiunitScript = aiunitScript;
			respawnerRemote.damageHandler = damageHandlerScript;

			autotargetScript = gameObject.AddComponent<CSVR_AutotargetMarker> ();
			if(autotargetScript != null)
			autotargetScript.TeamNumber = TeamNumber;
			autotargetScript.CurrentName = CurrentName;
			autotargetScript.CurrentHP = 100f;
			if(autotargetScript.MiddleBodyTransform == null)
			autotargetScript.MiddleBodyTransform = spineBone;
			damageHandlerScript.autotargetScript = autotargetScript;
			aiunitScript.autotargetScript = autotargetScript;

			RequestAIInfo ();

		}

	}

	void SetHP(CSVR_AIDamageHandler damageHandler)
	{
		damageHandler.AntiCheat_CurrentHealth = HP;
		damageHandler.MaxHealth = HP;
		damageHandler.CurrentHealth = HP;
	}

	void SetHP(ParagonAI.HealthScript healthScript)
	{
		healthScript.maxHealth = HP;
		healthScript.health = HP;
	}
		

	void RequestAIInfo()
	{
		myPhotonView.RPC("TransmitAIInfo", PhotonTargets.MasterClient);
	}
		
	[PunRPC]
	void TransmitAIInfo()
	{
		if(PhotonNetwork.isMasterClient)
		{
			myPhotonView.RPC ("ReceiveInfo", PhotonTargets.Others, TeamNumber, CurrentName);
		}
			
	}
		
	[PunRPC]
	void ReceiveInfo(int teamNumber, string currentName)
	{
		TeamNumber = teamNumber;
		CurrentName = currentName;
		transform.name = currentName;
		autotargetScript.CurrentName = currentName;
		autotargetScript.TeamNumber = teamNumber;
	}

//	[PunRPC]
//	void ReceiveAIRespawn(Vector3 newPos)
//	{
//		print ("receive AI Respawn");
//		transform.root.position = newPos;
//
//		aiunitScript.RebornAI ();
//		Reset ();
//	}

//	public void GenerateName()
//	{
//		CurrentName = MainName [(int)Random.Range (0, MainName.Length)] + " " + NickName [(int)Random.Range (0, NickName.Length)];
//		transform.name = CurrentName;
//	}
//
//	private string[] MainName = new string[]{
//		"Phát",
//		"Trường",
//		"Đức",
//		"Tuân",
//		"Phương",
//		"Thực",
//		"Khuê",
//		"An",
//		"Cường",
//		"Hùng",
//		"Sang",
//		"Dũng",
//		"Thịnh",
//		"Chung",
//		"Long",
//		"Linh",
//		"Minh",
//		"Hường",
//		"Đạt",
//		"Nghĩa",
//	};
//	private string[] NickName = new string[]{
//		"Ruồi",
//		"Sẹo",
//		"Khét",
//		"Bướm",
//		"Lá Ngón",
//		"Lòng Lợn",
//		"Dê",
//		"Sếch",
//		"Bồ Câu",
//		"Bí Bo",
//		"Kute",
//		"Nện",
//		"VIP",
//		"Bạch Tạng",
//		"Chim",
//		"Bánh Bèo",
//		"Núp",
//		"Gâu Su",
//		"Đẹp Trai",
//		"Cụt",
//		"Cá Chà Bặc",
//		"Cào Cào",
//	};


}
