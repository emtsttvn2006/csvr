﻿using UnityEngine;
using System.Collections;

public class CSVR_AITarget_Local : ParagonAI.TargetScript {

	// Use this for initialization
	public override void Awake () {
		
		myAIBaseScript = GetComponent<ParagonAI.BaseScript>();
		targetObjectTransform = GetComponent<CSVR_AIPlayerSetup>().targetTransform;
		GetComponent<ParagonAI.HealthScript>().myTargetScript = this;
		myTeamID =  GetComponent<CSVR_AIPlayerSetup>().TeamNumber;
		alliedTeamsIDs =  GetComponent<CSVR_AIPlayerSetup>().allyTeamIDs;
		enemyTeamsIDs =  GetComponent<CSVR_AIPlayerSetup>().enemyTeamIDs;

		base.Awake ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
