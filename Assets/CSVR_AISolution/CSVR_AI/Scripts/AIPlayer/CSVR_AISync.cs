﻿using UnityEngine;
using System.Collections;

public class CSVR_AISync : Photon.MonoBehaviour {

	public string currentWeapon = "";
	public float currentHealth = 100;
	public Vector3 currentPos;
	public Quaternion currentRot;
	public Vector2 animSyncInfo = Vector2.zero;
}
