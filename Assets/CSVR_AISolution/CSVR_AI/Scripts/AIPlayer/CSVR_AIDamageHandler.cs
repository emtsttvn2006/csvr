﻿using UnityEngine;
using System.Collections;
using ParagonAI;
using System.Collections.Generic;

public class CSVR_AIDamageHandler : vp_DamageHandler {

//	public Transform[] AIGunList;
//	int CurrentGunIndex;
//	public Transform CurrentGun;

	public CSVR_AutotargetMarker autotargetScript;

	public float KarmaPriorityAccumulative = 0.033f;
	public float KarmaPriorityInstant = 0.33f;

	public TargetScript myTargetScript;
	public BaseScript myAIBaseScript;
	public HealthScript myHealthScript;
	public CSVR_AILocalPlayer AILocalPlayerInfo;
	public GunScript AIGunScript;
	public CSVR_AIPlayerSetup AISetupScript;
	public CSVR_AISyncLocal syncLocal;
	public bool isInvicible = false;
	public GameObject hitEffect;

	Animator animBody;

	PhotonView myPhotonView;

	protected override void Awake()
	{
		if(AILocalPlayerInfo == null)
			AILocalPlayerInfo = GetComponent<CSVR_AILocalPlayer> ();
		if(myTargetScript == null)
			myTargetScript = GetComponent<TargetScript> ();
		if(myAIBaseScript == null)
			myAIBaseScript = GetComponent<BaseScript> ();
		if(myHealthScript == null)
			myHealthScript = GetComponent<HealthScript> ();
		if(AIGunScript == null)
			AIGunScript = GetComponent<GunScript> ();
		if(myPhotonView == null)
			myPhotonView = GetComponent<PhotonView> ();

		base.Awake ();
	}


	public override void Damage(vp_DamageInfo damageInfo)
	{
		isInvicible = AILocalPlayerInfo.isInvicible;
		if (isInvicible)
			return;
		if(GetComponent<CSVR_AIPlayerSetup>().TeamNumber != 0 && GetComponent<CSVR_AIPlayerSetup>().TeamNumber == damageInfo.team && damageInfo.Type == vp_DamageInfo.DamageType.Bullet)
			return;
		if (!enabled)
			return;
		if (!vp_Utility.IsActive(gameObject))
			return;
		if (!vp_Gameplay.isMaster)
			return;
		if (CurrentHealth <= 0.0f)
			return ;
		
		if (damageInfo != null)
		{
			if (damageInfo.Source != null)
				Source = damageInfo.Source;
			if (damageInfo.OriginalSource != null)
				OriginalSource = damageInfo.OriginalSource;
			//            Debug.Log("Damage! Source: " + damageInfo.Source + " ... " + "OriginalSource: " + damageInfo.OriginalSource);
		}
		
		// if we somehow shot ourselves with a bullet, ignore it
		if ((damageInfo.Type == vp_DamageInfo.DamageType.Bullet) && (m_Source == Transform))
			return;
		
		vp_Utility.Instantiate (hitEffect, damageInfo.vec, Quaternion.identity);
		CurrentHealth = Mathf.Min(CurrentHealth - damageInfo.Damage, MaxHealth);
		CurrentHealth = Mathf.Max (CurrentHealth, 0f);

		CurrentAmor = Mathf.Min(CurrentAmor - DamageAmor, MaxAmor);
		
		//Horus begin
		
		AntiCheat_CurrentHealth = CurrentHealth; //Chong cheat
		//Horus end

		autotargetScript.CurrentHP = CurrentHealth/MaxHealth * 100f;
		SyncHP ();

		
	/*	// in multiplayer, report damage for score tracking purposes
		if (vp_Gameplay.isMultiplayer && (damageInfo.Source != null))
		{
			
			vp_GlobalEvent<Transform, Transform, float>.Send("TransmitDamage", Transform.root, damageInfo.OriginalSource, damageInfo.Damage);
		}
		*/
		// detect and transmit death as event
		if (CurrentHealth <= 0.0f)
		{
//			Debug.Log (transform.name + " Mau = 0");
			if (vp_MPMaster.Instance != null) {
//				Debug.Log (transform.name + " call TransmitDeathInfo");
			vp_MPMaster.Instance.TransmitDeathInfo(damageInfo.ShooterPlayfabID, damageInfo.ShooterName,damageInfo.GunID,
					AISetupScript.CurrentID,AISetupScript.CurrentName,
				damageInfo.HitPart, damageInfo.Damage, damageInfo.Level, damageInfo.GunName,
					damageInfo.team, myTargetScript.myTeamID, damageInfo.idphoton, myPhotonView.viewID);
			}
			
			if (myTargetScript) {
				myTargetScript.CheckForLOSAwareness(true);
//				myTargetScript.targetPriority = 1f;
			}


//			TargetScript enemyTargetScript = damageInfo.OriginalSource.gameObject.GetComponent<TargetScript> ();
//			if(enemyTargetScript){
//				enemyTargetScript.targetPriorityKarma += KarmaPriorityAccumulative;
//				enemyTargetScript.targetPriority += KarmaPriorityInstant;
//			}
			CSVR_AITargetFor playerTargetScript = damageInfo.OriginalSource.gameObject.GetComponent<CSVR_AITargetFor> ();
			if(playerTargetScript)
			{
				playerTargetScript.targetPriorityKarma += KarmaPriorityAccumulative;
				playerTargetScript.targetPriority += KarmaPriorityInstant;
			}
			myAIBaseScript.CheckToSeeIfWeShouldDodge();
			myHealthScript.DeathCheck();

			SendMessage("Die",SendMessageOptions.DontRequireReceiver);
		}
		
	}

	public void SyncHP()
	{
		//Truyen thong tin mau bi mat sang remote
		if (syncLocal == null)
		{
			syncLocal = GetComponent<CSVR_AISyncLocal> ();
		}
		syncLocal.currentHealth = CurrentHealth;

		if (AISetupScript == null)
		{
			AISetupScript = GetComponent<CSVR_AIPlayerSetup> ();
		}
		AISetupScript.CurrentHP = CurrentHealth;
	}

	protected override void RemoveBulletHoles()
	{

		BulletScript[] bullets = GetComponentsInChildren<BulletScript>(true);
		for (int i = 0; i < bullets.Length; i++)
			vp_Utility.Destroy(bullets[i].gameObject);

	}

	public override void Die()
	{

		if (!enabled || !vp_Utility.IsActive(gameObject))
			return;

		if (m_Audio != null)
		{
			m_Audio.pitch = Time.timeScale;
			m_Audio.PlayOneShot(DeathSound);
		}

		if (Respawner == null)
		{
			vp_Utility.Destroy(gameObject);
		}
		else
		{
			RemoveBulletHoles();

		}

		m_InstaKill = false;

		if (vp_Gameplay.isMultiplayer && vp_Gameplay.isMaster)
		{
			//Debug.Log("sending kill event from master scene to vp_MasterClient");
			vp_GlobalEvent<Transform>.Send("TransmitKill", transform.root);
		}
	}



}

