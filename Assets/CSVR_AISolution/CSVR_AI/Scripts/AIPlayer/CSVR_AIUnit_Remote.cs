﻿using UnityEngine;
using System.Collections;

public class CSVR_AIUnit_Remote : CSVR_AIUnit 
{
	public CSVR_GunScript_Remote gunScript;


	public void SetWeapon(string newGunName)
	{
		AIGunList NewGun = findGunInList (newGunName);

		if (NewGun != null) 
		{
			foreach (AIGunList g in aIGunList) 
			{
				g.GunTransform.gameObject.SetActive (false);
			}

			NewGun.GunTransform.gameObject.SetActive (true);
			CurrentGun = NewGun.GunTransform;
			gunScript.MuzzleFlashTransform.position = NewGun.GunTransform.FindChild (newGunName + "/Muzzle").position;
			gunScript.BulletSound = NewGun.BulletSound;
		}
//		else 
//		{
//			foreach (Transform g in AIGunList) 
//			{
//				g.gameObject.SetActive (false);
//			}
//			GameObject newGun = Instantiate (Resources.Load ("Weapon/Remote/Third/" + newGunName) as GameObject);
//			newGun.name = newGunName;
//			newGun.transform.SetParent (GetComponent<CSVR_AIPlayerSetup> ().gunBone);
//			newGun.transform.localPosition = Vector3.zero;
//			newGun.transform.localRotation = Quaternion.identity;
//			CurrentGun = newGun.transform;
//			gunScript.MuzzleFlashTransform.position = NewGun.GunTransform.FindChild (newGunName + "/Muzzle").position;
//			Debug.LogError("Setup sai sung AI");
//		}

	}

	AIGunList findGunInList(string newGunName)
	{
		foreach (AIGunList gun in aIGunList)
		{
			if (gun.GunTransform.name == newGunName)
				return gun;
		}
		return null;
	}

}
