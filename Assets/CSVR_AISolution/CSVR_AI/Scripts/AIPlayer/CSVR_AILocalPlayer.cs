﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ParagonAI;
using Horus;
using Horus.ClientModels;
using Horus.Internal;
using Horus.Json;

public class CSVR_AILocalPlayer : CSVR_AIUnit {

	public CSVR_AISyncLocal syncLocal;
	[Range (1, 10)]public int BotDifficulty = 5;
//	private int m_BotDifficulty;
	public float MaxChanceToLevelUp = 3f;
	int CurrentGunIndex;
	public string GeneratedName;

	public TargetScript AITargetScript;
	public RotateToAimGunScript AIRotateToAimScript;
	public CSVR_AIDamageHandler AIDamageHandler;

	public override void Awake()
	{

		syncLocal = GetComponent<CSVR_AISyncLocal> ();
		
		AIDamageHandler = GetComponent<CSVR_AIDamageHandler> ();
		AIGunScript = GetComponent<GunScript> ();
		AITargetScript = GetComponent<TargetScript> ();
		AIRotateToAimScript = GetComponent<RotateToAimGunScript> ();
		transform.parent = null;

		TeamID = GetComponent<TargetScript> ().myTeamID;

//		AddToAIEnemyList ();


		base.Awake();
	}

	public override void Start()
	{
		BotDifficulty = Random.Range (4, 7);
		Debug.Log (transform.root.name + " starts with level " + BotDifficulty + "!");
		PickRandomGun ();
	}


	public override void Die()
	{
		base.Die ();
	}

	public void Reset(){
		SetGrowDifficulty (MaxChanceToLevelUp);
		PickRandomGun ();
	}

	public void SetGrowDifficulty(float maxchance)
	{
		float chance = maxchance * (1 - BotDifficulty * 0.1f);
		if (BotDifficulty < 10 && Random.value < chance)
		{
			//		m_BotDifficulty = Random.Range (1, 10);
			//		if (m_BotDifficulty > BotDifficulty)
			//		{
			//			BotDifficulty = m_BotDifficulty;
			//			Debug.Log (transform.root.name + " lvlups to level " + BotDifficulty + "!");
			//		}
			BotDifficulty += 1;
			//Debug.Log (transform.root.name + " lvlups to level " + BotDifficulty + "!");
		}
	}

	public void PickRandomGun()
	{
		//random rut sung

		foreach(AIGunList GunAdded in aIGunList)
		{
			GunAdded.GunTransform.gameObject.SetActive (false);
		}
		CurrentGunIndex = Random.Range(0, aIGunList.Length);
		CurrentGun = aIGunList [CurrentGunIndex].GunTransform;
		CurrentGun.gameObject.SetActive (true);
		AIGunScript.muzzleFlashSpawn = CurrentGun.transform.FindChild(CurrentGun.name + "/Muzzle");
		AIGunScript.bulletSound = aIGunList [CurrentGunIndex].BulletSound;
		SetAIGunInfo (CurrentGun.gameObject.name);
		AIDamageHandler.strWeaponName = strWeaponName;
//		Debug.Log(transform.root.name + " pick gun " + CurrentGun.name + ", find muzzle at " + CurrentGun.name + "/Muzzle");
	}


	public void SetAIGunInfo(string GunName)
	{
		switch(GunName){
		///SMG
		/// 
		/// 
		case "44_999_MP5":
//			GunID = "44_999_MP5";
			strWeaponName = "MP 5";
			BulletCount = 30;

			AIGunScript.DamageGun = 93f;
			AIGunScript.inaccuracy = 2.2f;
			AIGunScript.maxFiringAngle = 5f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.3f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 2.6f;
			AIGunScript.rateOfFire = 11.35f;
			AIGunScript.bulletsUntilReload = 30;
			AIGunScript.minPauseTime = 1f;
			AIGunScript.randomPauseTimeAdd = 1f;
			AIGunScript.minRoundsPerVolley = 8;
			AIGunScript.maxRoundsPerVolley = 30;

			AITargetScript.distToLoseAwareness = 15f;
			AITargetScript.timeBeforeTargetExpiration = 5;

			break;

			///RIFLE
			/// 
			/// 
		case "11_979_M4A4":
//			GunID = "11_979_M4A4";
			strWeaponName = "M4A4";
			BulletCount = 30;

			AIGunScript.DamageGun = 125f;
			AIGunScript.inaccuracy = 2.1f;
			AIGunScript.maxFiringAngle = 5f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.3f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 3f;
			AIGunScript.rateOfFire = 10f;
			AIGunScript.bulletsUntilReload = 30;
			AIGunScript.minPauseTime = 1f;
			AIGunScript.randomPauseTimeAdd = 1f;
			AIGunScript.minRoundsPerVolley = 4;
			AIGunScript.maxRoundsPerVolley = 15;

			AITargetScript.distToLoseAwareness = 25f;
			AITargetScript.timeBeforeTargetExpiration = 5;
			break;


		case "11_999_AK47":
//			GunID = "11_999_AK47";
			strWeaponName = "AK 47";
			BulletCount = 30;

			AIGunScript.DamageGun = 125f;
			AIGunScript.inaccuracy = 2.0f;
			AIGunScript.maxFiringAngle = 5f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.3f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 2.8f;
			AIGunScript.rateOfFire = 9.5f;
			AIGunScript.bulletsUntilReload = 30;
			AIGunScript.minPauseTime = 1f;
			AIGunScript.randomPauseTimeAdd = 1f;
			AIGunScript.minRoundsPerVolley = 4;
			AIGunScript.maxRoundsPerVolley = 15;

			AITargetScript.distToLoseAwareness = 25f;
			AITargetScript.timeBeforeTargetExpiration = 5;
			break;

		case "11_959_AUG":
//			GunID = "11_959_AUG";
			strWeaponName = "AUG";
			BulletCount = 30;

			AIGunScript.DamageGun = 110f;
			AIGunScript.inaccuracy = 2.0f;
			AIGunScript.maxFiringAngle = 5f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.3f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 3.07f;
			AIGunScript.rateOfFire = 10.87f;
			AIGunScript.bulletsUntilReload = 30;
			AIGunScript.minPauseTime = 1f;
			AIGunScript.randomPauseTimeAdd = 1f;
			AIGunScript.minRoundsPerVolley = 6;
			AIGunScript.maxRoundsPerVolley = 20;

			AITargetScript.distToLoseAwareness = 25f;
			AITargetScript.timeBeforeTargetExpiration = 5;

			break;

		case "11_939_AK337":
//			GunID = "11_939_AK337";
			strWeaponName = "AK 337";
			BulletCount = 30;

			AIGunScript.DamageGun = 136f;
			AIGunScript.inaccuracy = 1.9f;
			AIGunScript.maxFiringAngle = 5f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.3f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 3.6f;
			AIGunScript.rateOfFire = 10f;
			AIGunScript.bulletsUntilReload = 30;
			AIGunScript.minPauseTime = 1f;
			AIGunScript.randomPauseTimeAdd = 1f;
			AIGunScript.minRoundsPerVolley = 4;
			AIGunScript.maxRoundsPerVolley = 15;

			AITargetScript.distToLoseAwareness = 25f;
			AITargetScript.timeBeforeTargetExpiration = 5;
			break;



			///SNIPER
			/// 
			/// 
		case "22_939_M82":
//			GunID = "22_939_M82";
			strWeaponName = "Barrett";
			BulletCount = 10;

			AIGunScript.DamageGun = 334;
			AIGunScript.inaccuracy = 0.4f;
			AIGunScript.maxFiringAngle = 3f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.0f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 3.7f;
			AIGunScript.rateOfFire = 1.5f;
			AIGunScript.bulletsUntilReload = 10;
			AIGunScript.minPauseTime = 2f;
			AIGunScript.randomPauseTimeAdd = 2f;
			AIGunScript.minRoundsPerVolley = 1;
			AIGunScript.maxRoundsPerVolley = 1;

			AITargetScript.distToLoseAwareness = 40f;
			AITargetScript.timeBeforeTargetExpiration = 15;
			break;

		case "22_989_AWP":
//			GunID = "22_989_AWP";
			strWeaponName = "AWP";
			BulletCount = 10;

			AIGunScript.DamageGun = 260f;
			AIGunScript.inaccuracy = 0.4f;
			AIGunScript.maxFiringAngle = 3f;
			AIGunScript.inaccuracy /= (BotDifficulty * 0.1f);
			AIGunScript.oddsToJumpFire = 0.0f;
			AIGunScript.oddsToJumpFire *= (BotDifficulty * 0.1f);

			AIGunScript.reloadTime = 3.7f;
			AIGunScript.rateOfFire = 1.5f;
			AIGunScript.bulletsUntilReload = 10;
			AIGunScript.minPauseTime = 2f;
			AIGunScript.randomPauseTimeAdd = 2f;
			AIGunScript.minRoundsPerVolley = 1;
			AIGunScript.maxRoundsPerVolley = 1;

			AITargetScript.distToLoseAwareness = 40f;
			AITargetScript.timeBeforeTargetExpiration = 15;
			break;


		default:
			break;
		}

		GunID = GunName;
		syncLocal.currentWeapon = GunName;
	}

}
