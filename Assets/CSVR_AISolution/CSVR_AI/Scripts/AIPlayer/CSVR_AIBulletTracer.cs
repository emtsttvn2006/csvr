﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;

public class CSVR_AIBulletTracer : MonoBehaviour
{
	public float Speed = 500f;
	float CurrentReach = 0f;
	float NextReach = 0f;
	void Start () 
	{
		transform.SetParent (null);
	}

	public void BulletFly(float rch)
	{
		StartCoroutine (Fly(Speed, rch));
	}

	IEnumerator Fly(float spd, float rch)
	{
		while (CurrentReach < rch)
		{
			NextReach = CurrentReach += (spd * Time.deltaTime);
			if (NextReach < rch)
			{
				transform.Translate (Vector3.forward * (spd * Time.deltaTime));
				CurrentReach = NextReach;
				yield return CurrentReach;
			} else
			{
				transform.Translate (Vector3.forward * (rch - CurrentReach));
				vp_Utility.Destroy (gameObject);
			}
		}

	}

}
