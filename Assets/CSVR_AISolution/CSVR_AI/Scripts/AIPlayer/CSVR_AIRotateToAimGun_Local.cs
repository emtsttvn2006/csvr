﻿using UnityEngine;
using System.Collections;

public class CSVR_AIRotateToAimGun_Local : ParagonAI.RotateToAimGunScript {


	CSVR_AIPlayerSetup setupScript;
//	 Use this for initialization
	public override void Awake () 
	{
		setupScript = GetComponent<CSVR_AIPlayerSetup> ();

		spineBone = setupScript.spineBone;
		bulletSpawnTransform = setupScript.BulletSpawnTransform;
	}
	

}
