﻿using UnityEngine;
using System.Collections;

public class CSVR_GunScript_Remote : MonoBehaviour {
	public GameObject MuzzleFlashPrefab;
	public GameObject BulletTracerPrefab;
	public GameObject secondaryFireObject;
	public Transform MuzzleFlashTransform;
	public AudioClip BulletSound;
	public AudioClip ReloadSound;
	public AudioSource myAudioSource;
	public CSVR_AIAnimation_Remote animationScript;


	void Start()
	{
		if(MuzzleFlashPrefab && MuzzleFlashTransform)
		{
		GameObject myMuzzleFlash = Instantiate (MuzzleFlashPrefab) as GameObject;
			myMuzzleFlash.transform.SetParent (MuzzleFlashTransform);
			myMuzzleFlash.transform.localPosition = Vector3.zero;
			myMuzzleFlash.transform.localRotation = Quaternion.identity;
			MuzzleFlashTransform.gameObject.SetActive (false);
		}
	}

	[PunRPC]
	void FireOneShotRemote(float bulletTravel, bool isJumpingFire)
	{
		//play sound
		myAudioSource.PlayOneShot(BulletSound);
		ShowMuzzleFlash ();
		PlayFireAnimation ();
		if(BulletTracerPrefab)
		{
			GameObject Tracer = vp_Utility.Instantiate (BulletTracerPrefab, MuzzleFlashTransform.position, MuzzleFlashTransform.rotation) as GameObject;
			Tracer.GetComponent<CSVR_AIBulletTracer> ().BulletFly(bulletTravel);
			vp_Utility.Destroy (Tracer, 4f);
		}
		if(animationScript)	
		{
			animationScript.PlayFiringAnimation();
			if (isJumpingFire == true)
				animationScript.PlayJumpAnimation ();
		}
	}

	[PunRPC]
	void FireOneGrenadeRemote(Vector3 lastPosTargetSeen)
	{
		if(secondaryFireObject)
		{
			GameObject currentGrenade = (GameObject)(Instantiate(secondaryFireObject, MuzzleFlashTransform.position, MuzzleFlashTransform.rotation));
			currentGrenade.transform.LookAt(lastPosTargetSeen);
			ParagonAI.GrenadeScript gre = currentGrenade.gameObject.GetComponent<ParagonAI.GrenadeScript> ();
			if (gre)
			{
				gre.SetTarget(lastPosTargetSeen);
			}
			if(animationScript)	
			{
				animationScript.PlayFiringGrenadeAnimation();
			}
		}

	}


	[PunRPC]
	void ReloadGunAIRemote()
	{
		myAudioSource.PlayOneShot (ReloadSound);
		PlayReloadAnimation ();
	}

	void ShowMuzzleFlash()
	{
		if (MuzzleFlashTransform == null)
			return;
		MuzzleFlashTransform.gameObject.SetActive (true);
		Invoke ("hideMuzzle", 0.06f);
	}

	void hideMuzzle()
	{
		MuzzleFlashTransform.gameObject.SetActive (false);
	}

	void PlayFireAnimation()
	{
		animationScript.PlayFiringAnimation ();
	}

	void PlayReloadAnimation()
	{
		animationScript.PlayReloadAnimation ();
	}
}
