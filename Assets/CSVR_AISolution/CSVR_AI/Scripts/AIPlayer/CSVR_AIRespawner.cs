using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using ParagonAI;

public class CSVR_AIRespawner : vp_Respawner {


	public BaseScript AIBaseScript;
	public TargetScript AITargetScript;
	public GunScript AIGunScript;
	public AnimationScript AIAnimationScript;
	public RotateToAimGunScript AIRotateToAimGunScript;
	public HealthScript AIHealth;
	public NavMeshAgent AINavMeshAgent;
	public CoverFinderScript AICoverFinderScript;
	public CSVR_AIDamageHandler DamageHandlerScript;
	public CSVR_AILocalPlayer aiUnitScript;
	public PhotonView myPhotonView;
	public CSVR_AutotargetMarker autotargetScript;

	protected override void Awake()
	{
		if(AIBaseScript == null)
			AIBaseScript = GetComponent<BaseScript>();
		if(AITargetScript == null)
			AITargetScript = GetComponent<TargetScript>();
		if(AIGunScript == null)
			AIGunScript = GetComponent<GunScript>();
		if(AIAnimationScript == null)
			AIAnimationScript = GetComponent<AnimationScript>();
		if(AIRotateToAimGunScript == null)
			AIRotateToAimGunScript = GetComponent<RotateToAimGunScript>();
		if(AIHealth == null)
			AIHealth = GetComponent<HealthScript>();
		if(AINavMeshAgent == null)
			AINavMeshAgent = GetComponent<NavMeshAgent>();
		if(AICoverFinderScript == null)
			AICoverFinderScript = GetComponent<CoverFinderScript>();
		if(aiUnitScript == null)
			aiUnitScript = GetComponent<CSVR_AILocalPlayer>();
		if(DamageHandlerScript == null)
			DamageHandlerScript = GetComponent<CSVR_AIDamageHandler>();
		if(myPhotonView == null)
			myPhotonView = GetComponent<PhotonView>();
		if(autotargetScript == null)
			autotargetScript = GetComponent<CSVR_AutotargetMarker>();
//		switch(AITargetScript.myTeamID)
//		{
//		case 1:
//			SpawnPointTag = "Police";
//			break;
//		case 2:
//			SpawnPointTag = "Terrorist";
//			break;
//		}

		m_Transform = transform;
		m_Audio = GetComponent<AudioSource>();


//		vp_SpawnPoint mySpawnPoint = vp_SpawnPoint.GetRandomSpawnPoint(SpawnPointTag);
//		print (transform.root.name + " at " + mySpawnPoint.transform.position);
//		transform.root.position = mySpawnPoint.transform.position;
	}


	void Start()
	{
		PickSpawnPoint ();
	}


	protected override void Die()
	{	
		vp_Timer.In(UnityEngine.Random.Range(MinRespawnTime, MaxRespawnTime), PickSpawnPoint, m_RespawnTimer);

		AINavMeshAgent.Stop ();
		AIBaseScript.enabled = false;
		AIGunScript.enabled = false;
		AIAnimationScript.enabled = false;
		AIRotateToAimGunScript.enabled = false;

		AITargetScript.RemoveThisTargetFromPLay();//de AI ko nham ban nua
	}

	public override void PickSpawnPoint ()
	{
		base.PickSpawnPoint ();
		if (PhotonNetwork.isMasterClient)
			myPhotonView.RPC ("ReceiveAIRespawn", PhotonTargets.Others, transform.root.position);
	}

	public override void Reset()
	{
//		print ("so luong spawnpoint dang co la: "+vp_SpawnPoint.SpawnPoints.Count);
//		Debug.Log ("CSVR_AIRespawner.Reset");
		AIBaseScript.enabled = true;
		AINavMeshAgent.Resume ();
		AIGunScript.enabled = true;
		AIAnimationScript.enabled = true;
		AIRotateToAimGunScript.enabled = true;

		aiUnitScript.RebornAI();
		AIRotateToAimGunScript.Awake ();
		AIGunScript.Awake ();

	
		AITargetScript.Start ();
		AIBaseScript.Start ();
		AIGunScript.Start ();
		AIAnimationScript.Start ();

		AIHealth.health = AIHealth.maxHealth;
		m_Transform = transform;
		autotargetScript.CurrentHP = 100f;

		DamageHandlerScript.SyncHP ();

		AITargetScript.AddThisToTargetsList ();
//		print (ParagonAI.ControllerScript.currentController.currentTargets.Count);
		base.Reset();
	}
		
}
