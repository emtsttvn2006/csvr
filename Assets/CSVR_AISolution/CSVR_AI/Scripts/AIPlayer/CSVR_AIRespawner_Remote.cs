﻿using UnityEngine;
using System.Collections;

public class CSVR_AIRespawner_Remote : MonoBehaviour {

	[HideInInspector] public CSVR_AIUnit aiunitScript;
	[HideInInspector] public CSVR_AIDamageHandler_Remote damageHandler;

	protected void Awake ()
	{
		if(PhotonNetwork.isMasterClient)
		{
			Destroy(this);
		}
	}

	[PunRPC]
	void ReceiveAIRespawn(Vector3 newPos)
	{
//		print ("receive AI Respawn");
		transform.root.position = newPos;
		try
		{
			aiunitScript.RebornAI ();
			damageHandler.ResetHp ();
		}
		catch
		{
		}
	}
}
