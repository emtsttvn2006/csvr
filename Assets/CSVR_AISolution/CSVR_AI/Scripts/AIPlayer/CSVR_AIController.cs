﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AIController : MonoBehaviour 
{
	public static CSVR_AIController currentController = null;

	public static List<CSVR_AITargetInfo> AITargetInfoList = new List<CSVR_AITargetInfo> ();

	void Awake()
	{
		currentController = this;
	}

	public void AddToTargetList(CSVR_AITargetInfo Info)
	{
		//		if (CSVR_AIControllerScript.CurrentController) 
		//		{
		if (!CSVR_AIController.AITargetInfoList.Contains (Info)) 
		{
			CSVR_AIController.AITargetInfoList.Add (Info);
			print (Info.transform.root.name + " is added to target infos list");
		}
		//		}
	}

	public void RemoveFromTargetList(CSVR_AITargetInfo Info)
	{
		if (CSVR_AIController.AITargetInfoList.Contains (Info)) 
		{
			CSVR_AIController.AITargetInfoList.Remove (Info);
		}
	}

	void OnDestroy()
	{
		AITargetInfoList.Clear ();
	}
}
