﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;
/*
 * This script aims the grenade and launches it.
 *  It will also warn other agents of itself when it lands, usually prompting them to move out of the way.
 * */

namespace ParagonAI
{
    public class GrenadeScript : MonoBehaviour
    {
        //Timer starts after the grenade hits the ground
        public float timeTilExplode = 3;
        public GameObject explosion;

        private Vector3 target;

        //This was to give the grenade consistant force instead of adjusting the force and throwing at a 45 degree angle
        //var alterAngle : boolean = false;	
        public float warningRadius = 7;
        public float timeUntilWarningCanBeGiven = 1;

        private bool hasTarget = false;


        //public bool makeSureWeDontGoThroughObjects = true;
        //private Vector3 lastPos;
        public LayerMask layerMask;

        private bool warned = false;
        private bool canBeWarnedYet = false;

        private Rigidbody myRigidBody;

        public float runAwayBuffer = 3;

		public float ExplosionDamage = 200f;
		public Transform Source = null;
		public string Shooter_PlayfabID = "botFakeID";
		public string Shooter_Name;
		public string Gun_ID = "88_989_CSG";
		public int Hit_Part = 1;
		public string Level = "10";
		public int TeamID;
		public string Gun_Name = "88_989_CSG";
		public int idPhotonView = -1;


        void Go()
        {
            float throwForce = 0;
            //Aims grenade at Target
            if (hasTarget)
            {
                float xDist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(target.x, target.z));
                float yDist = -(transform.position.y - target.y);

                //Calculate force required
                throwForce = xDist / (Mathf.Sqrt(Mathf.Abs((yDist - xDist) / (0.5f * (-Physics.gravity.y)))));
                throwForce = 1.414f * throwForce / Time.fixedDeltaTime * GetComponent<Rigidbody>().mass;

                //Always fire on a 45 degree angle, this makes it easier to calculate the force required.
                transform.Rotate(-45, 0, 0);
            }
            myRigidBody = GetComponent<Rigidbody>();
            myRigidBody.AddRelativeForce(Vector3.forward * throwForce);

            //Start the time on the grenade
            StartCoroutine(StartDetonationTimer());
//			Timing.RunCoroutine(StartDetonationTimer());
            //Because the grenade may skim the colliders of the agent before detonating, we want to wait a moment or two before being able to "warn" agents of the grenade
            //Warning will cause surrounding agents to attempt to escape from the grenade.
//            StartCoroutine(SetTimeUntilWarning()); 	
        }

        void Warn()
        {
            //Only send out one warning
            if(!warned)
                {
                    warningRadius = warningRadius*warningRadius;
                    ParagonAI.Target[] targets = GameObject.FindGameObjectWithTag("AI Controller").GetComponent<ParagonAI.ControllerScript>().GetCurrentTargets();
				
                    for(int i = 0; i < targets.Length; i++)
                        {
                            //If the grenade is close enough and has clear line of sight (ie: they are not on the other side of a wall), then warn them
                            if(Vector3.SqrMagnitude(myRigidBody.position - targets[i].transform.position) < warningRadius && !Physics.Linecast(targets[i].transform.position, myRigidBody.position, layerMask))
                                {
                                    targets[i].targetScript.WarnOfGrenade(transform, warningRadius + runAwayBuffer);
                                }
                        }
				
				
                    warned = true;
                }	
        }

        void DetonateGrenade()
        {
			if (explosion) {
				GameObject m_explosion =  Instantiate(explosion, transform.position, transform.rotation) as GameObject;
				//kienbb
				ExplosionScript exp = m_explosion.gameObject.GetComponent<ExplosionScript> ();
				SetExplosionInfo (exp);
				//endkien
			}
				
            else
                Debug.LogWarning("No explosion object assigned to grenade!");

            Destroy(gameObject);
        }
		//kienbb



		void SetExplosionInfo(ExplosionScript exp){
			exp.ExplosionDamage = ExplosionDamage;
			exp.Shooter_PlayfabID = Shooter_PlayfabID;
			exp.Shooter_Name = Shooter_Name;
			exp.Gun_ID = Gun_ID;
			exp.Hit_Part = Hit_Part;
			exp.Level = Level;
			exp.TeamID = TeamID;
			exp.Gun_Name = Gun_Name;
			exp.idPhotonView = idPhotonView;
		}
		//endkien


		IEnumerator StartDetonationTimer()
//		IEnumerator<float> StartDetonationTimer()
        {
			yield return new WaitForSeconds(timeTilExplode);
//			yield return Timing. WaitForSeconds(timeTilExplode);
            DetonateGrenade();
        }

		IEnumerator SetTimeUntilWarning()
//		IEnumerator<float> SetTimeUntilWarning()
        {
			yield return new WaitForSeconds(timeUntilWarningCanBeGiven);
//			yield return Timing. WaitForSeconds(timeUntilWarningCanBeGiven);
            canBeWarnedYet = true;
        }

//        void OnCollisionEnter(Collision collision)
//        {
//            if (canBeWarnedYet)
//                 Warn();
//        }

        public void SetTarget(Vector3 pos)
        {
            target = pos;
            hasTarget = true;
            Go();
        }
    }
}
