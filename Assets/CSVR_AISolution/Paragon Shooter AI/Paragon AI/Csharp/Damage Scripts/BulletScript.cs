﻿using UnityEngine;
using System.Collections;

/*
 * Moves an object until it hits a target, at which time it calls the Damage(float) method on all scripts on the hit object
 * Can also home in on targets and "detonate" when in close proximity.
 * */

namespace ParagonAI
{
    public class BulletScript : MonoBehaviour
    {	


//		//kienbb
		public GameObject BulletTracer = null;

//		private CSVR_HitscanBullet CSVRBullet = new CSVR_HitscanBullet ();
//		public string Shooter_Name;
//		public string GunName;
		public float DamAmor;
		public float DamTest;
		[Range(0, 100f)]public float XG = 25f;
		public string Shooter_PlayfabID = "botFakeID";
		public string Shooter_Name = "bot";
		public string GunID = "fakeGunID";
		public string GunName = "botGun";
		public string Level = "10";
		public int idPhotonView = -1;
		public int TeamID;

		
		public Transform m_Source;
		protected vp_DamageHandler m_TargetDHandler = null;

		int HitPart = 1;//0 - Head, 1 - Body . Thong tin ve cho va cham

		float damageToAi;

//		//endkien


		public string damageMethodName = "Damage";

        public float speed = 1000;
        public LayerMask layerMask;
        public float maxLifeTime = 3;

        //Bullet Stuff
        public float damage = 16;

        //use for shotguns
        public float bulletForce = 100;

        //Hit Effects
        public GameObject hitEffect;
        public float hitEffectDestroyTime = 1;
//        public string hitEffectTag = "RagDoll";
        public GameObject missEffect;
//        public float missEffectDestroyTime = 1;
        public float timeToDestroyAfterHitting = 0.5f;
        private RaycastHit hit;
        private Transform myTransform;

        //Rotation
        private Quaternion targetRotation;

        private Transform target = null;
        public float homingTrackingSpeed = 3;

        public float minDistToDetonate = 0.5f;
        private float minDistToDetonateSqr;
        
        //kienbb: lam cho no giong nhu CSVR_HitscanBullet



		//endkien
		//pooling
		void OnEnable(){
			myTransform = transform;
//			Move ();
		}


		//endpooling
//        void Awake()
//        {
//            myTransform = transform;
//        }
//
		void Start(){
			transform.SetParent (null);
		}



        //Automatically destroy the bullet after a certain amount of time
        //Especially useful for missiles, which may end up flying endless circles around their target,
        //long after the appropriate sound effects have ended.
//        IEnumerator SetTimeToDestroy()
//        {
//            yield return new WaitForSeconds(maxLifeTime);
//
//            if (target)
//            {
//				vp_Utility.Instantiate(hitEffect, myTransform.position, myTransform.rotation);
//            }
//
//			vp_Utility.Destroy(transform);
//        }
//
//        IEnumerator ApplyDamage()
//        {
//			//kienbb
////			DamAmor = DamTest * 0.2f;
////			damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
//			damageToAi = damage;
//			//endkien
//
//            //Reduce the enemy's health
//            //Does NOT travel up the heirarchy.  
//			hit.collider.SendMessage(damageMethodName, damageToAi, SendMessageOptions.DontRequireReceiver);
//
//            //Produce the appropriate special effect
//		
////            this.enabled = false;
//            yield return null;
//
//            //Wait a fram to apply forces as we need to make sure the thing is dead
//            if (hit.rigidbody)
//                hit.rigidbody.AddForceAtPosition(myTransform.forward * bulletForce, hit.point, ForceMode.Impulse);
//
//            //Linger around for a while to let the trail renderer dissipate (if the bullet has one.)
//			vp_Utility.Destroy(gameObject, timeToDestroyAfterHitting);
//
//			//kienbb
//
//			//endkien
//        }

        // Update is called once per frame
//        void Update()
//        {
//            Move();
//        }

        //Move is a seperate method as the bullet must move IMMEDIATELY.
        //If it does not, the shooter may literally outrun the bullet-momentarily.
        //If the shooter passes the bullet, the bullet will then start moving and hit the shooter in the back.
        //That would not be good.
        public void Move()
        {
            //Check to see if we're going to hit anything.  If so, move right to it and deal damage
            if (Physics.Raycast(myTransform.position, myTransform.forward, out hit, speed, layerMask.value))
            {
              
				if(BulletTracer){
					GameObject Tracer = vp_Utility.Instantiate (BulletTracer, transform.position, transform.rotation) as GameObject;
					Tracer.GetComponent<CSVR_AIBulletTracer> ().BulletFly(Vector3.Distance(transform.position, hit.point));
					vp_Utility.Destroy (Tracer, 4f);
				}

				if (hit.transform.gameObject.layer == vp_Layer.Ragdoll && hitEffect)
				{
					//				print ("hit Effect");    
					GameObject currentHitEffect = (GameObject)(vp_Utility.Instantiate(hitEffect, hit.point, myTransform.rotation));
					//                    GameObject.Destroy(currentHitEffect, hitEffectDestroyTime);
				}
				else if (hit.transform.gameObject.layer == 0 && missEffect)
				{	
//					print ("miss eff");
					GameObject currentMissEffect = (GameObject)(vp_Utility.Instantiate(missEffect, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal)));
					//                GameObject.Destroy(currentMissEffect, missEffectDestroyTime);
				}


				//kienbb

				m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider (hit.collider);

//				if(m_TargetDHandler != null)
//					print (m_TargetDHandler.gameObject.name);

//				print (m_Source);

				switch (hit.collider.tag) {
				case "Head":
					//	Debug.Log("Head");
					//	Debug.Log(DamTest);
					//	Debug.Log(m_TargetDHandler.MaxAmor);
					//	Debug.Log(XG);
					//					Damage = (DamTest * 1) * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
					HitPart = 0;
					DamAmor = DamTest * 1f;
					damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
					if (damage > 0) {
						if ((m_TargetDHandler != null) && (m_Source != null)) {
//							print ("dau " + damage);
							//Ham nay truyen thong tin ve damage va nguoi ban va GunID
							m_TargetDHandler.Damage (new vp_DamageInfo (damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, TeamID, GunName, idPhotonView, hit.point));
						}
					}
					vp_Utility.Destroy (gameObject);
//					EZ_PoolManager.Despawn (transform);
					break;
				case "Femoral":
					HitPart = 1;
					DamAmor = DamTest * 0.25f;
					damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
					vp_DamageHandler.DamageAmor = DamAmor;
					if (damage > 0) {
						if ((m_TargetDHandler != null) && (m_Source != null)) {
//							print ("bap " + damage);
							m_TargetDHandler.Damage(new vp_DamageInfo(damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, TeamID, GunName, idPhotonView, hit.point));
						}
					}                
					vp_Utility.Destroy (gameObject);
//					EZ_PoolManager.Despawn (transform);
					break;
				case "Leg":
					HitPart = 1;
					//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(hit.collider);
					DamAmor = DamTest * 0.2f;
					damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
					vp_DamageHandler.DamageAmor = DamAmor;
					if (damage > 0) {
						if ((m_TargetDHandler != null) && (m_Source != null)) {
//							print ("canh " + damage);
							//Ham nay truyen thong tin ve damage va nguoi ban va GunID
							m_TargetDHandler.Damage(new vp_DamageInfo(damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, TeamID, GunName, idPhotonView, hit.point));
						}
					}                    
					vp_Utility.Destroy (gameObject);
//					EZ_PoolManager.Despawn (transform);
					break;
				case "Chest":
					HitPart = 1;
					//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(hit.collider);
					DamAmor = DamTest * 0.4f;
					damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
					vp_DamageHandler.DamageAmor = DamAmor;
					if (damage > 0) {
						if ((m_TargetDHandler != null) && (m_Source != null)) {
//							print ("nguc " + damage);
							//Ham nay truyen thong tin ve damage va nguoi ban va GunID
							m_TargetDHandler.Damage(new vp_DamageInfo(damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, TeamID, GunName, idPhotonView, hit.point));

						}
					}
					
					vp_Utility.Destroy (gameObject);
//					EZ_PoolManager.Despawn (transform);
					break;
				case "Stomach":
					HitPart = 1;
					//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(hit.collider);
					DamAmor = DamTest * 0.3f;
					damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
					vp_DamageHandler.DamageAmor = DamAmor;
					if (damage > 0) {
						if ((m_TargetDHandler != null) && (m_Source != null)) {
//							print ("bung " + damage);
							//Ham nay truyen thong tin ve damage va nguoi ban va GunID
							m_TargetDHandler.Damage(new vp_DamageInfo(damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, TeamID, GunName, idPhotonView, hit.point));

						}
					}
					
					vp_Utility.Destroy (gameObject);
//					EZ_PoolManager.Despawn (transform);
					break;
				default:
//					//Nếu là đạn xuyên tường thì phải sinh ra 1 bullet moi
//					//					if (DamTest > MinDamXuyenTuong && CanPenetrate) {
//					//						//Debug.Log ("Chuẩn bị xuyên tường");
//					//						m_RangeEngPoint = ray.GetPoint (Range);
//					//						m_InverseRay = new Ray (m_RangeEngPoint, -(m_Transform.forward)); //Raycast chiều ngược lại
//					//						//Debug.DrawRay(m_RangeEngPoint, -(m_Transform.forward.normalized*Range), Color.red, 20);
//					//						
//					//						//Chỉ raycast vào object này. Nếu không raycast được tức là tường quá dày và bỏ qua
//					//						if (hit.collider.Raycast(m_InverseRay, out m_InverseHit, Range)) {
//					//							//Debug.Log ("Raycast ngược lại");
//					//							m_WallThickness = Vector3.Distance (hit.point, m_InverseHit.point);
//					//							//						Debug.Log (m_WallThickness);
//					//							m_NewRange =  Range - Vector3.Distance(m_Transform.position, hit.point) - MaxWallThickNess; 
//					//							
//					//							//Neu m_NewRange > 0, tuc la vien dan co the di tiep
//					//							if (m_NewRange > 0 && m_WallThickness < MaxWallThickNess) {
//					//								/*	//Tạo 1 vết đạn ở chỗ ben kia tuong
//					//							m_InverseBullet = Instantiate(transform.root.gameObject).transform;
//					//							m_InverseBullet.GetComponent<CSVR_HitscanBullet> ().m_NoHit = true; //Bullet nay chi de tao vet dan ben kia tuong
//					//							
//					//							//Dua vet dan den dung vi tri
//					//							m_InverseBullet.parent = m_InverseHit.transform;
//					//							m_InverseBullet.localPosition = m_InverseHit.transform.InverseTransformPoint (m_InverseHit.point);
//					//							m_InverseBullet.rotation = Quaternion.LookRotation (m_InverseHit.normal);	
//					//							
//					//							Destroy (m_InverseBullet.gameObject, 5); //Hen gio de huy vet dan nay
//					//							*/
//					//								//Tao 1 vien dan moi de di tiep
//					//								m_NewBullet = Instantiate(transform.root.gameObject).transform;
//					//								m_NewBullet.position = m_InverseHit.point;
//					//								
//					//								//Range bi ngan lai, tuy theo cho bi trung dan, do day cua tuong va do cung cua tuong
//					//								m_NewBullet.GetComponent<CSVR_HitscanBullet> ().Range = m_NewRange;
//					//								m_NewBullet.GetComponent<CSVR_HitscanBullet> ().CanPenetrate = false; //Dan moi nay ko xuyen tuong nua
//					//							}											
//					//						}					
//					//					}
//					Destroy (gameObject);
//					HitPart = 1;
//					//m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(hit.collider);
//					DamAmor = DamTest * 0.2f;
//					damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));
//					vp_DamageHandler.DamageAmor = DamAmor;
//					if (damage > 0) {
//						if ((m_TargetDHandler != null) && (m_Source != null)) {
//							print ("truyen thong tin vao dan ban vao abc");
//							//Ham nay truyen thong tin ve damage va nguoi ban va GunID
//							m_TargetDHandler.Damage(new vp_DamageInfo(damage, m_Source, vp_DamageInfo.DamageType.Bullet, Shooter_PlayfabID, Shooter_Name, GunID, HitPart, Level, TeamID, GunName, idPhotonView, hit.point));
//						}
//					}                    
					vp_Utility.Destroy (gameObject, 0.1f);
//					EZ_PoolManager.Despawn (transform);
					break;
				}
				//endkien
//				myTransform.position = hit.point;
//				StartCoroutine(ApplyDamage());
            }
            else
            {
                //Move the bullet forwards
//                transform.Translate(Vector3.forward * Time.deltaTime * speed);
				vp_Utility.Destroy (gameObject);
            }

            //Home in on the target
            if (target != null)
            {
                //Firgure out the rotation required to move directly towards the target
                targetRotation = Quaternion.LookRotation(target.position - transform.position);
                //Smoothly rotate to face the target over several frames.  The slower the rotation, the easier it is to dodg.
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, homingTrackingSpeed * Time.deltaTime);

                Debug.DrawRay(transform.position, (target.position - transform.position).normalized * minDistToDetonate,Color.red);
                //Projectile will "detonate" upon getting close enough to the target..
                if (Vector3.SqrMagnitude(target.position - transform.position) < minDistToDetonateSqr)
                {
                    //The hitEffect should be your explosion.
					vp_Utility.Instantiate(hitEffect, myTransform.position, myTransform.rotation);
//					vp_Utility.Destroy(gameObject);
//					EZ_PoolManager.Despawn (transform);
                }
            }
        }

        //To avoid costly SqrRoot in Vector3.Distance
        public void SetDistToDetonate(float x)
        {
            minDistToDetonateSqr = x * x;
        }

        //Call this method upon instantating the bullet in order to make it home in on a target.
        public void SetAsHoming(Transform t)
        {
            target = t;
            SetDistToDetonate(minDistToDetonate);  
        }
    }
}
