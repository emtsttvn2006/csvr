﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MovementEffects;
/*
 * Applies damage and force to all colliders within a given radius.
 * */

namespace ParagonAI
{
    public class ExplosionScript : MonoBehaviour
    {
    	public string damageMethodName = "Damage";
    
        public float explosionRadius = 5.0f;
        public float explosionPower = 10.0f;
        public float upwardsPower = 10.0f;
        public float damage = 200.0f;
        public LayerMask layerMask;
		float explosionTime = 0.1f;
        public bool scaleDamageByDistance = true;

        public bool showBlastRadius = false;

        public bool shouldDoSingleHitboxDamage = false;


		//kienbb
		public float ExplosionDamage = 200f;
		public Transform Source = null;
		public string Shooter_PlayfabID;
		public string Shooter_Name;
		public string Gun_ID = "BotGrenade";
		public int Hit_Part = 1;
		public string Level = "10";
		public int TeamID;
		public string Gun_Name = "BotGrenade";
		public int idPhotonView = -1;
	


		protected static vp_DamageHandler m_TargetDHandler = null;
		protected Collider m_TargetCollider = null;

		public GameObject ExplosionEffect = null;

		//endkien
        void Start()
        {
			if (PhotonNetwork.isMasterClient)
			{
				Source = transform;
				StartCoroutine (Go ());
				//			Timing.RunCoroutine(Go());
			}
			else
			{
				Instantiate (ExplosionEffect, transform.position, Quaternion.identity);
				Destroy (gameObject, explosionTime);
			}
        }

		IEnumerator Go()
//		IEnumerator<float> Go()
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
            Collider hit;
            List<Rigidbody> hitBodies = new List<Rigidbody>();
            int i = 0;
            float damageThisTime = damage;

            for (i = 0; i < colliders.Length; i++)
            {	
				m_TargetCollider = colliders[i];
				m_TargetDHandler = vp_DamageHandler.GetDamageHandlerOfCollider(m_TargetCollider);
                hit = colliders[i];
                //Make sure we have line of sight to the collider
				if (!Physics.Linecast(transform.position, hit.transform.position, layerMask) && m_TargetDHandler != null)
                {
                    //Make damage fall off if further away.  Scaling is linear
                    if(scaleDamageByDistance){
                        damageThisTime = damage * Vector3.Distance(transform.position , hit.transform.position) / explosionRadius;
                    }
                    //Ideally, you should use single hitbox damage, but non-paragon AI scripts may not support it.
//                    if (shouldDoSingleHitboxDamage)
//                    {
//                        hit.GetComponent<Collider>().SendMessage("SingleHitBoxDamage", damageThisTime, SendMessageOptions.DontRequireReceiver);
//                    }
//                    else
//                    {
//                        //Will damage the same agent once for each collider
//                        hit.GetComponent<Collider>().SendMessage(damageMethodName, damageThisTime, SendMessageOptions.DontRequireReceiver);
//                    }


					m_TargetDHandler.Damage(new vp_DamageInfo(damageThisTime, Source, vp_DamageInfo.DamageType.Explosion, Shooter_PlayfabID, Shooter_Name, Gun_ID, 1, Level, TeamID, Gun_Name, idPhotonView));
//					print (Shooter_Name + " nem bom " + Gun_ID + " vao " + m_TargetDHandler.strNamePlayer);
//					m_TargetDHandler.Damage(new vp_DamageInfo(damageThisTime, Source, vp_DamageInfo.DamageType.Explosion, Shooter_PlayfabID, Shooter_Name, GunID, 1, Level, TeamID, GunName, idPhotonView));
//					
//					hit.GetComponent<Collider>().SendMessage(damageMethodName, damageThisTime, SendMessageOptions.DontRequireReceiver);//kienbb

                    if (hit.GetComponent<Rigidbody>())
                        hitBodies.Add(hit.GetComponent<Rigidbody>());
                }
            }

            //Make sure things are dead.
            yield return 0f;
            for (i = 0; i < hitBodies.Count; i++)
            {
                //Make sure the rigid body still exists so we don't get an error (it may be destroyed if the target is killed)
                if (hitBodies[i])
                    hitBodies[i].AddExplosionForce(explosionPower, transform.position, explosionRadius, upwardsPower, ForceMode.Impulse);
            }



            //Destroy the explosion object, given some time afterwards to let the special effect play out.
			Instantiate (ExplosionEffect, transform.position, Quaternion.identity);
			Destroy (gameObject, explosionTime);
        }

//		void Explode(){
//			Instantiate (ExplosionEffect, transform.position, Quaternion.identity);
//			Destroy (gameObject, 0.1f);
//		}

        void OnDrawGizmos()
        {
            if (showBlastRadius)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(transform.position, explosionRadius);
            }
        }
    }
}
