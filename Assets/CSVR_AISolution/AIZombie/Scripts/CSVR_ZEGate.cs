﻿using UnityEngine;
using System.Collections;

public class CSVR_ZEGate : MonoBehaviour 
{
	public SpawnerObject ControllerEasy;
	public SpawnerObject ControllerNormal;
	public SpawnerObject ControllerHard;

	public int RoundID;

	private SpawnerObject myController;
	private vp_ZombieSurvival zombieModeInstance;

	private Collider myCollider;
	private Renderer myRenderer;

	private void Awake()
	{
		myCollider = GetComponent<Collider> ();
		myRenderer = GetComponent<Renderer> ();
		zombieModeInstance = (vp_ZombieSurvival)vp_MPMaster.Instance;
		if(zombieModeInstance.RoundStart > RoundID)
		{
			if(myCollider)
				myCollider.enabled = false;
			if(myRenderer)
				myRenderer.enabled = false;
		}
	}

	private void Start()
	{
		switch (zombieModeInstance.hardLevel)
		{
		case HardLevel.EASY:
			myController = ControllerEasy;
			break;
		case HardLevel.NORMAL:
			myController = ControllerNormal;
			break;
		case HardLevel.HARD:
			myController = ControllerHard;
			break;
		}
		if(myController != null)
			myController.Gate = this;
	}

	public void OpenGate()
	{
		if (zombieModeInstance.RoundsPassed >= RoundID)
		{
			print ("CANNOT OPEN GATE");
			return;
		}
			
		vp_MPMaster.Instance.photonView.RPC("Receive_EndRound", PhotonTargets.All, true, true);
		if (myCollider)
			myCollider.enabled = false;
		if (myRenderer)
			myRenderer.enabled = false;
	}

	#if UNITY_EDITOR

	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.red;
		try
		{
			Gizmos.DrawLine(transform.position, ControllerEasy.transform.position);
			Gizmos.DrawLine(transform.position, ControllerNormal.transform.position);
			Gizmos.DrawLine(transform.position, ControllerHard.transform.position);
		}
		catch
		{
		}
	}

	#endif

}
