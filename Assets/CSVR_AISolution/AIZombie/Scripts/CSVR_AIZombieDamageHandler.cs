﻿using UnityEngine;
using System.Collections;

public class CSVR_AIZombieDamageHandler : vp_DamageHandler {

	private float HeathTemp = 0f;
	public int MyTeamID = 1;
	CSVR_AutotargetMarker auto;

	protected override void Awake ()
	{
		auto = GetComponent<CSVR_AutotargetMarker> ();
		auto.CurrentHP = CurrentHealth;
	}

	protected override void OnEnable ()
	{
		
	}

	protected override void OnDisable ()
	{
		
	}
		
	public override void Damage(vp_DamageInfo damageInfo)
	{

		HeathTemp = CurrentHealth;
		if (CurrentHealth <= 0.0f)
			return;

		base.Damage(damageInfo);

		print (transform.name + " nhan sat thuong " + damageInfo.Damage + " at " + Time.time);

		Debug.Log (CurrentHealth);


	}

	void Update()
	{
		auto.CurrentHP = CurrentHealth / MaxHealth * 100;
	}

}
