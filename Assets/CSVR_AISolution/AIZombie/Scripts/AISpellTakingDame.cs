﻿using UnityEngine;
using System.Collections;

public class AISpellTakingDame : MonoBehaviour {

	public bool isTargetAttack = false;

	public Transform target;
	private float timer = 0f;
	private float totalDuration = 20f;

	Vector3 temp;

	Vector3 direction;

	// Use this for initialization
	void Start () {
//		StartCoroutine (DestroyGameObject());

		temp = new Vector3 (target.position.x,target.position.y + 1f,target.position.z);

		direction = transform.position - temp;
		direction = direction.normalized;

		Rigidbody rig = GetComponent<Rigidbody> ();

		if (!isTargetAttack) {
//			transform.position = Vector3.Slerp (transform.position,target.position,2);
			rig.AddForce(direction* -300);
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (target);

		timer += Time.deltaTime;
		float t = timer / totalDuration;
		if (isTargetAttack) {
			
			transform.position = Vector3.Slerp (transform.position, target.position, t);	
		} else {
//			transform.position = Vector3.Slerp (transform.position,temp,t);
		}
	}

	IEnumerator DestroyGameObject()
	{
		yield return new WaitForSeconds (2);
		Destroy (gameObject);
	}

	int HitPart = 1;//0 - Head, 1 - Body . Thong tin ve cho va cham

	public float DamAmor;
	public float DamTest;

	public float damage = 100;

	[Range(0, 100f)]public float XG = 25f;

	public vp_DamageHandler m_TargetDHandler = null;

	public Transform m_Source;

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="col">Col.</param>
	void OnTriggerEnter(Collider col)
	{
		// Rest of the code
		if (col.gameObject.tag == "PolicePlayer") 
		if (iSPlayer(col.gameObject)) 
		{
//			m_TargetDHandler = CSVR_FPPlayerDamageHandler.GetDamageHandlerOfCollider (col);

			// Find player collider
			GetPlayerDamageHandler (col.gameObject);

			HitPart = 0;
			DamAmor = DamTest * 1f;
			damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));

			Debug.Log ("damage = "+damage +" m_TargetDHandler =  "+m_TargetDHandler+" m_Source = "+m_Source);

			if (damage > 0) {
				if ((m_TargetDHandler != null) && (m_Source != null)) {
					Debug.Log ("Damage 1");
					//							print ("dau " + damage);
					//Ham nay truyen thong tin ve damage va nguoi ban va GunID
					m_TargetDHandler.Damage (new vp_DamageInfo (damage, m_Source));
					Destroy (gameObject);
				}
			}
		}
		if (col.gameObject.isStatic) {
			Destroy (gameObject);
		}
	}

	/// <summary>
	/// Is the S player.
	/// </summary>
	/// <returns><c>true</c>, if S player was ied, <c>false</c> otherwise.</returns>
	/// <param name="go">Go.</param>
	private bool iSPlayer(GameObject go)
	{
		foreach (var item in go.transform.GetComponentsInParent<Transform>()) 
		{
			if (item.tag == "PolicePlayer") 
			{
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Gets the player damage handler.
	/// </summary>
	/// <returns>The player damage handler.</returns>
	/// <param name="go">Go.</param>
	private vp_DamageHandler GetPlayerDamageHandler(GameObject go)
	{
		foreach (var item in go.transform.GetComponentsInParent<Transform>()) 
		{
			if (item.tag == "PolicePlayer") 
			{
				m_TargetDHandler = CSVR_FPPlayerDamageHandler.GetDamageHandlerOfCollider (item.GetComponent<CharacterController> ().GetComponent<Collider>());
				return m_TargetDHandler;
			}
		}
		return null;
	}

}
