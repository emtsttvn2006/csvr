﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_AIBoss : MonoBehaviour {

	/// <summary>
	/// The m animator.
	/// </summary>
	Animator m_animator;

	/// <summary>
	/// AI zombie type.
	/// </summary>
	public enum AIZombieType
	{
		AI_ZOMBIE_MELEE,
		AI_ZOMBIE_RANGER,
		AI_ZOMBIE_BOSS
	}

	/// <summary>
	/// The animations.
	/// </summary>
	private string[] animations = { "changeToRun", "changeToIdle"};

	/// <summary>
	/// Animation type.
	/// </summary>
	enum animationType
	{
		changetorun = 0,
		changetoiidle = 1,
	}

	/// <summary>
	/// The animation attack.
	/// </summary>
	private string[] animationAttack = { "punch_left", "punch_right", "swipping_left", "swipping_righ", "jump_attack" };

	/// <summary>
	/// Animation attack type.
	/// </summary>
	enum animationAttackType
	{
		punch_left = 0,
		punch_right = 1,
		swipping_left = 2,
		swipping_righ = 3,
		jump_attack = 4
	}

	/// <summary>
	/// The type.
	/// </summary>
	public AIZombieType type;

	public float hitPoints = 100f;
	public float stopDistance = 4f;
	public float speed = 2f;
	public float destroyDelay = 7.0f;
	public Animation actorAnimation;
	public AnimationClip idle;
	public AnimationClip run;
	public AnimationClip[] attackAnimations;
	public AnimationClip[] deathAnimations;
	public Texture2D[] textures;
	public SkinnedMeshRenderer render;
	public Color materialColir;

	bool isDead = false;
	public Transform target;
	NavMeshAgent nma;
	int attackAnimIndex;
	float damage;
	public bool inAttack = false;
	public SpawnSystem spawnerSystem;

	public GameObject Speller;

	int[] EnemyTeamID = { 1, 2};

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
		InitialAIZombie ();
		m_animator = GetComponent<Animator> ();

		getClipNames ();
	}

	/// <summary>
	/// Initials the AI zombie.
	/// </summary>
	private void InitialAIZombie()
	{
//		Material material = new Material(Shader.Find("Diffuse"));
//		material.SetTexture("_MainTex", textures[Random.Range(0, textures.Length - 1)]);
//		material.color = materialColir;
//		render.material = material;

		nma = gameObject.GetComponent<NavMeshAgent>();
		nma.stoppingDistance = stopDistance * 0.5f;
//		actorAnimation.AddClip(idle, idle.name);
//		actorAnimation.AddClip(run, run.name);
//
//		foreach(AnimationClip clip in attackAnimations){
//			actorAnimation.AddClip(clip, clip.name);
//			actorAnimation[clip.name].wrapMode = WrapMode.Once;
//		}
//
//		foreach(AnimationClip clip in deathAnimations){
//			actorAnimation.AddClip(clip, clip.name);
//			actorAnimation[clip.name].wrapMode = WrapMode.Once;
//		}
//		actorAnimation[idle.name].wrapMode = WrapMode.Loop;
//		actorAnimation[run.name].wrapMode = WrapMode.Loop;
//		attackAnimIndex = Random.Range(0, attackAnimations.Length);
		damage = Random.Range(3.0f, 7.0f);
		SearchTarget ();
		if(target == null)
			target = GameObject.FindGameObjectWithTag("PolicePlayer").transform;

		//		SearchTarget ();
	}

	/// <summary>
	/// Raises the GU event.
	/// </summary>
	void OnGUI()
	{
		
	}

	/// <summary>
	/// Searchs the target.
	/// </summary>
	private void SearchTarget()
	{
		int maxLength = CSVR_AIController.AITargetInfoList.Count;
//		target = CSVR_AIController.AITargetInfoList [randomIndex (maxLength)].transform;

		for(int i = 0; i < maxLength; i++)
		{
			if(EnemyTeamID.Contains(CSVR_AIController.AITargetInfoList[i].TeamID))
			{
				target = CSVR_AIController.AITargetInfoList [i].transform;
				break;
			}
		}

	}

	/// <summary>
	/// Randoms the index.
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="maxLength">Max length.</param>
	private int randomIndex(int maxLength)
	{
		return Random.Range (0, maxLength);
	}

	/// <summary>
	/// AIs the type of the zombie setup.
	/// </summary>
	/// <param name="zombie_type">Zombie type.</param>
	public void AIZombieSetupType(AIZombieType zombie_type)
	{
		switch (zombie_type) 
		{
		case AIZombieType.AI_ZOMBIE_BOSS:
			break;
		case AIZombieType.AI_ZOMBIE_MELEE:
			break;
		case AIZombieType.AI_ZOMBIE_RANGER:
			break;
		default:
			break;
		}
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void LateUpdate () {
		if(Input.GetKeyDown(KeyCode.H)){
			if(!isDead)
				ApplyDamage1(15);
		}

		if(target){
			if(!isDead){
				nma.destination = target.position;
				nma.updateRotation = DistanceToTarget(target.position) > stopDistance ? true : false;
				nma.speed = DistanceToTarget(target.position) > stopDistance ? speed : 0.0f;

				if(DistanceToTarget(target.position) > stopDistance){
					m_animator.SetTrigger(animations[(int)animationType.changetorun]);
					nma.Resume ();
					m_animator.SetBool(animations[(int)animationType.changetoiidle], false);
				}

				if(DistanceToTarget(target.position) <= stopDistance){
					RotateTowards(target.position);
					nma.Stop ();
					m_animator.SetBool(animations[(int)animationType.changetoiidle], true);
					if (!inAttack)
					{
						m_animator.SetBool(animations[(int)animationType.changetoiidle], false);
						BossAttack();
						StartCoroutine (Attack(damage + Random.Range(-1.0f, 3.0f)));
					}
						
				}
			}
			else{
				nma.speed = 0.0f;
			}
		}
		else{

		}

	}

	/// <summary>
	/// The name of the list clips.
	/// </summary>
	public List<AnimationClip> listClipsName = new List<AnimationClip>();

	/// <summary>
	/// Checks the in attack.
	/// </summary>
	private void getClipNames()
	{
		AnimationClip[] animClips = m_animator.runtimeAnimatorController.animationClips;
		Debug.Log (animClips.Length);

		for (int i = 0; i <= animClips.Length-1; i++) {
			listClipsName.Add (animClips[i]);
		}
	}

	int temp = 0;

	/// <summary>
	/// Bosses the attack.
	/// </summary>
	void BossAttack()
	{
		RandomState:
		int ran = Random.Range (0, animationAttack.Length);

		if (ran == temp)
			goto RandomState;
		
		temp = ran;

		setCurrentClipLength(ran);
		m_animator.SetTrigger(animationAttack[ran]);
	}

	public float currentClipLength = 0;
	public string currentClipName = "";

	/// <summary>
	/// Sets the name of the current state.
	/// </summary>
	/// <param name="index">Index.</param>
	void setCurrentClipLength(int index)
	{
		switch (index) {
		case 0:
			currentClipLength = listClipsName [2].length;
			currentClipName = listClipsName [2].name;
			break;
		case 1:
			currentClipLength = listClipsName [3].length;
			currentClipName = listClipsName [3].name;
			break;
		case 2:
			currentClipLength = listClipsName [4].length;
			currentClipName = listClipsName [4].name;
			break;
		case 3:
			currentClipLength = listClipsName [5].length;
			currentClipName = listClipsName [5].name;
			break;
		case 4:
			currentClipLength = listClipsName [6].length;
			currentClipName = listClipsName [6].name;
			break;
		default:
			break;
		}

		Debug.Log ("currentClipLength ="+currentClipLength);
	}

	/// <summary>
	/// Applies the damage1.
	/// </summary>
	/// <param name="dmg">Dmg.</param>
	void ApplyDamage1(int dmg){
		if(hitPoints <= dmg){
			isDead = true;
			actorAnimation.CrossFade(deathAnimations[Random.Range(0, deathAnimations.Length)].name);
			spawnerSystem.CurrentZombieCount--;
			Destroy(gameObject);
		}
		else
			hitPoints -= dmg;
	}

	/// <summary>
	/// Applies the damage A.
	/// </summary>
	/// <param name="msg">Message.</param>
	void ApplyDamageAI(DamageMSG msg){
		if(isDead)
			return;
		if(hitPoints <= msg.damage){
			msg.sendFrom.credits += Random.Range(3, 20);
			msg.sendFrom.killsAI++;
			isDead = true;
			actorAnimation.CrossFade(deathAnimations[Random.Range(0, deathAnimations.Length)].name);
			Invoke("SendDestroyEvent", destroyDelay);
		}
		else
			hitPoints -= msg.damage;
	}

	/// <summary>
	/// Sends the destroy event.
	/// </summary>
	void SendDestroyEvent(){
		GetComponent<NetworkView>().RPC("Destroy", RPCMode.All);
	}

	/// <summary>
	/// Attack the specified finalDamage.
	/// </summary>
	/// <param name="finalDamage">Final damage.</param>
	IEnumerator Attack(float finalDamage){
		inAttack = true;
		target.gameObject.SendMessage("ApplyDamageAI", finalDamage, SendMessageOptions.DontRequireReceiver);
		yield return new WaitForSeconds (currentClipLength);
		inAttack = false;
		yield return new WaitForEndOfFrame ();
	}

	/// <summary>
	/// Rotates the towards.
	/// </summary>
	/// <param name="pos">Position.</param>
	public void RotateTowards(Vector3 pos){
		Vector3 targetDirection = new Vector3(pos.x, transform.position.y, pos.z);
		transform.LookAt(targetDirection) ;
	}

	/// <summary>
	/// Determines whether this instance is animation enabled the specified stateName.
	/// </summary>
	/// <returns><c>true</c> if this instance is animation enabled the specified stateName; otherwise, <c>false</c>.</returns>
	/// <param name="stateName">State name.</param>
	public bool IsAnimationEnabled(string stateName){
		return actorAnimation[stateName].enabled;
	}

	/// <summary>
	/// Distances to target.
	/// </summary>
	/// <returns>The to target.</returns>
	/// <param name="pos">Position.</param>
	public float DistanceToTarget(Vector3 pos){
		return Vector3.Distance(transform.position, pos);
	}

	/// <summary>
	/// Destroy this instance.
	/// </summary>
	[PunRPC]
	void Destroy(){
		Network.RemoveRPCs(GetComponent<NetworkView>().viewID);
		Network.Destroy(GetComponent<NetworkView>().viewID);
	}

}
