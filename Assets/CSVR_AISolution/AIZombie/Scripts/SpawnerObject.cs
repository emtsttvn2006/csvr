﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum HardLevel{
	EASY,
	NORMAL,
	HARD
}

public class SpawnerObject : MonoBehaviour {

	Color color;
	public float GizmoSize = 1f;
	/// <summary>
	/// The spawn system.
	/// </summary>
	public SpawnSystem spawnSystem;

	/// <summary>
	/// The enemy.
	/// </summary>
	public GameObject enemy;

	/// <summary>
	/// The spawner burst.
	/// </summary>
	public float SpawnerBurst;

	/// <summary>
	/// The spawner delay.
	/// </summary>
	public float SpawnerDelay;

	/// <summary>
	/// The spawn rate.
	/// </summary>
	public float SpawnRate;

	public enum LimitType
	{
		Time,
		Cycle,
		Count
	}

	public LimitType SpawnLimitType = LimitType.Cycle;
	/// <summary>
	/// The spawner limit time.
	/// </summary>
	public float SpawnerLimitTime = 60f;
	public int SpawnLimitCount = 20;
	public int SpawnLimitCycles = 5;

	public int UndestroyedCount;

	private float SpawnStartTime;
	private int CurrentCount;
	private int CurrentCycle;
	/// <summary>
	/// The spawner distance.
	/// </summary>
	public float SpawnerDistance;

	/* Condition for call spawn objects */
	[HideInInspector]
	public enum SpawnTrigger
	{
		NONE,
		NORMAL,
		TRIGGER,
		CONTAINER
	}

	/* Default no spawner init*/
	public SpawnTrigger spawnTrigger = SpawnTrigger.NORMAL;

	/// <summary>
	/// The player.
	/// </summary>
	private GameObject Player;

	/// <summary>
	/// The is spawned.
	/// </summary>
	[HideInInspector] public bool isSpawned = false;

	/// <summary>
	/// The is paused.
	/// </summary>
	public bool isPaused = false;

	/// <summary>
	/// The is stopped.
	/// </summary>
	public bool isStopped = false;

	public bool FinishedSpawning = false;
	public bool isBoss = false;
	public CSVR_ZEGate Gate;
	/// <summary>
	/// The hard level.
	/// </summary>
	public HardLevel hardLevel;

	// Use this for initialization
	private void Start () 
	{
		Player = GameObject.FindGameObjectWithTag ("Player");

		UndestroyedCount = 0;
		if(spawnTrigger == SpawnTrigger.NORMAL)
		{
			StartSpawner();
		}
//		StartCoroutine (WaitDelayTime());
	}

	/// <summary>
	/// Waits the delay time.
	/// </summary>
	/// <returns>The delay time.</returns>
	IEnumerator WaitDelayTime()
	{
		yield return new WaitForSeconds (SpawnerDelay);

		if(spawnTrigger == SpawnTrigger.NORMAL)
		{
			StartSpawner();
		}
	}

	/// <summary>
	/// Starts the spawner.
	/// </summary>
	public void StartSpawner()
	{
		Debug.Log ("demo");
		isSpawned = true;
		SpawnStartTime = Time.time;
		if (SpawnerLimitTime < 0)
		{
			InvokeRepeating ("CallSpawner", SpawnerDelay, SpawnRate);
		}
		else if (SpawnerLimitTime == 0)
		{
			CallSpawner ();
		}
		else
		{
			InvokeRepeating ("CallSpawner", SpawnerDelay, SpawnRate);
			Invoke ("CancelSpawner", SpawnerLimitTime);
		}
	}

	/// <summary>
	/// Determines whether this instance cancel spawner.
	/// </summary>
	/// <returns><c>true</c> if this instance cancel spawner; otherwise, <c>false</c>.</returns>
	public void CancelSpawner()
	{
		CancelInvoke ();
	}

	// Update is called once per frame
//	private void Update () 
//	{
//		float time = Time.time;
//		if (time >= (SpawnerStartTime + SpawnerLimitTime) && !isStopped) {
//			isStopped = true;
//
//			CancelSpawner();
//		}
//
//		/* If distance less than 10 metters and this spawn system has not spawn yet */
//		/*if (CalculateRemainingDistance() <= SpawnerDistance &&
//			!isSpawned) 
//		{
//			spawnTrigger = SpawnTrigger.NORMAL;
//
//			isSpawned = true;
//		}*/
//
//		/* If have a trigger */
////		if (spawnTrigger == SpawnTrigger.TRIGGER) 
////		{
////
////			/* Call spawner */
////			CallSpawner ();
////
////			spawnTrigger = SpawnTrigger.NONE;
////		}
//	}

	/// <summary>
	/// Calls the spawner for instant all enemy around this point.
	/// </summary>
	public void CallSpawner()
	{
		print ("START CALLING SPAWNER");
		if(FinishedSpawning == true)
		{
			CancelSpawner ();
			return;
		}
		if (isBoss)
		{
			print ("IS BOOS TURN!");
			for (int i = 0; i < SpawnerBurst; i++)
			{
				GameObject go = GameObject.Instantiate (enemy, RandomPosition (), Quaternion.identity) as GameObject;
				FinishedSpawning = true;
			}
			return;
		}
		print ("IS NORMAL TURN");
		for (int i = 0; i < SpawnerBurst; i++)
		{
			if (spawnSystem.CurrentZombieCount >= spawnSystem.MaximumZombieCount ||	spawnSystem.CurrentZombieCount >= spawnSystem.MaximumZombieCountInScene)
			{
				break;
			}
			if(SpawnLimitType == LimitType.Count)
			{
				if (CurrentCount >= SpawnLimitCount)
				{
					print ("COUNT LIMIT");
					FinishedSpawning = true;
					break;
				}
					
			}
			else if(SpawnLimitType == LimitType.Time)
			{
				if(Time.time >= SpawnStartTime + SpawnerLimitTime)
				{
					print ("TIME LIMIT");
					FinishedSpawning = true;
					break;
				}
			}
			else if(SpawnLimitType == LimitType.Cycle)
			{
				if(CurrentCycle >= SpawnLimitCycles)
				{
					print ("CYCLE LIMIT");
					FinishedSpawning = true;
					break;
				}
			}
			GameObject go = GameObject.Instantiate (enemy, RandomPosition (), Quaternion.identity) as GameObject;
			AI aiScript = go.GetComponent<AI> ();
			if(aiScript)
				go.GetComponent<AI> ().spawnerSystem = spawnSystem;
			spawnSystem.CurrentZombieCount++;
			CurrentCount++;
			CurrentCycle++;
			UndestroyedCount++;
			CSVR_ZCDamageHandler damageHandler = go.GetComponent<CSVR_ZCDamageHandler> ();
			if (damageHandler)
				damageHandler.mySpawner = this;
		}
	}

	/// <summary>
	/// Randoms the position for each enemy which has spawned near this point.
	/// </summary>
	/// <returns>The position.</returns>
	private Vector3 RandomPosition()
	{
		float x = transform.position.x + Random.Range (0,0);
		float z = transform.position.z + Random.Range (0,0);
		float y = transform.position.y;

		return new Vector3 (x,y,z);
	}

	/// <summary>
	/// Calculates the remaining distance.
	/// </summary>
	/// <returns>The remaining distance.</returns>
	private float CalculateRemainingDistance()
	{
		float distance = Vector3.Distance (this.transform.position, Player.transform.position);

		return distance;
	}
		
	private void OnDestroy()
	{
		CancelInvoke ();
		StopAllCoroutines ();
	}

	public void OpenGate()
	{
		if(Gate)
		{
			Gate.OpenGate ();
		}
	}


	#if UNITY_EDITOR

	void OnDrawGizmos()
	{	
		switch(hardLevel)
		{
		case HardLevel.EASY:
			color = Color.green;
			break;
		case HardLevel.NORMAL:
			color = Color.yellow;
			break;
		case HardLevel.HARD:
			color = Color.red;
			break;
		}
		Gizmos.color = color;
		Gizmos.DrawWireSphere(transform.position, GizmoSize);
	}

	void OnDrawGizmosSelected() 
	{
		switch(hardLevel)
		{
		case HardLevel.EASY:
			color = Color.green;
			break;
		case HardLevel.NORMAL:
			color = Color.yellow;
			break;
		case HardLevel.HARD:
			color = Color.red;
			break;
		}
		Gizmos.color = color;
		Gizmos.DrawWireSphere(transform.position, GizmoSize);
		foreach (TriggerObject trigger in TriggerObject.AllTriggerObjects)
		{
			try
			{
				if(trigger.spawnerObject.Contains(this))
					Gizmos.DrawLine(transform.position, trigger.transform.position);
			}
			catch{}

		}
	}

	#endif
}
