﻿using UnityEngine;
using System.Collections;

public class AIDamageDealer : MonoBehaviour {

	/// <summary>
	/// The m_TargetDHandler.
	/// </summary>
	public vp_DamageHandler m_TargetDHandler = null;

	private bool isColliding = false;

	// Use this for initialization
	void Start () {

		gameObject.tag = "TeamAI";

		m_Source = this.transform;
	}

	void FixedUpdate()
	{
		
		RaycastHit hit;

		if (Physics.Raycast (transform.position, -transform.right, out hit, 0.5f, vp_Layer.Ragdoll)) 
		{
			Debug.DrawLine(transform.position, hit.point, Color.red);
		}


	}

	int HitPart = 1;//0 - Head, 1 - Body . Thong tin ve cho va cham

	public float DamAmor;
	public float DamTest;

	public float damage = 100;

	[Range(0, 100f)]public float XG = 25f;

	public Transform m_Source;

	void OnTriggerEnter(Collider col)
	{
		// Rest of the code
		if (col.gameObject.tag == "PolicePlayer") 
		{
			if(isColliding) return;
			StartCoroutine (ResetAttact());
			m_TargetDHandler = CSVR_FPPlayerDamageHandler.GetDamageHandlerOfCollider (col);

			HitPart = 0;
			DamAmor = DamTest * 1f;
			damage = DamAmor * (1 - (m_TargetDHandler.MaxAmor / 100) * (1 - XG / 100));

			Debug.Log ("damage = "+damage +" m_TargetDHandler =  "+m_TargetDHandler+" m_Source = "+m_Source);

			if (damage > 0) {
				if ((m_TargetDHandler != null) && (m_Source != null)) {
					Debug.Log ("Damage 1");
					//							print ("dau " + damage);
					//Ham nay truyen thong tin ve damage va nguoi ban va GunID
					m_TargetDHandler.Damage (new vp_DamageInfo (damage, m_Source));
				}
			}
		}
	}

	IEnumerator ResetAttact()
	{
		Debug.Log ("Attack!");

		isColliding = true;
		yield return new WaitForSeconds (1);
		isColliding = false;
	}

}
