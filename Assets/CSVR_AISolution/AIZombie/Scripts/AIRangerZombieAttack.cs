﻿using UnityEngine;
using System.Collections;

public class AIRangerZombieAttack : MonoBehaviour {

	public GameObject ps;

	public Transform target;

	// Use this for initialization
	void Start () {
		gameObject.tag = "TeamAI";

		InvokeRepeating ("Attact",1,3);
	}

	// Update is called once per frame
	void FixedUpdate () {
		RaycastHit hit;

		if (Physics.Raycast (transform.position, transform.forward, out hit)) 
		{
			if (hit.collider.tag == "PolicePlayer") 
			{
				target = hit.collider.gameObject.transform;	
			}
		}

		Debug.DrawLine(transform.position, hit.point, Color.red);
	}

	void Attact()
	{
		if (target) 
		{
			if (Vector3.Distance (transform.position, target.position) < 6) 
			{
				GameObject go = Instantiate (ps, gameObject.transform.position, Quaternion.FromToRotation(transform.position,target.position)) as GameObject;
				go.GetComponent<AISpellTakingDame> ().target = target;
				go.GetComponent<AISpellTakingDame> ().m_Source = transform;	
			}
		}
			
	}
}
