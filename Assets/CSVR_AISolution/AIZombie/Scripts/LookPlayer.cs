﻿using UnityEngine;
using System.Collections;

public class LookPlayer : MonoBehaviour {
	GameObject go;
	// Use this for initialization
	void Start () {
		go = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (go.transform);
	}
}
