using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
	public Color color;
	public float size;
	SpawnerObject Spawner;

	public enum Difficulty
	{
		Easy,
		Medium,
		Hard
	}
	public Difficulty difficulty;

	HardLevel hardLevel;

	void OnDrawGizmos()
	{	
		
		switch(difficulty)
		{
		case Difficulty.Easy:
			color = Color.green;
			break;
		case Difficulty.Medium:
			color = Color.yellow;
			break;
		case Difficulty.Hard:
			color = Color.red;
			break;
		}
		Gizmos.color = color;
		Gizmos.DrawWireSphere(transform.position, size);
	}
}
