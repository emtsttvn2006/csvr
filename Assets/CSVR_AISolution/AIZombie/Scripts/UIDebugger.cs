﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIDebugger : MonoBehaviour {

	public SpawnSystem ss;

	public Text TotalZombieCountText;
	public Text SpawnTimeText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		TotalZombieCountText.text = "Total zombie count : " + ss.CurrentZombieCount + "/" + ss.MaximumZombieCount;
		SpawnTimeText.text = "Time elapsed : " + (int)Time.time;
	}
}
