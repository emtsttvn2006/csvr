﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CSVR_ZombieTutorialManager : MonoBehaviour {
	/// <summary>
	/// The list game object arrow.
	/// </summary>
	public List<GameObject> listGameObjectArrows;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(listGameObjectArrows.Count>0)
			listGameObjectArrows [0].SetActive (true);
	}
}
