﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnSystem : MonoBehaviour {

	public static SpawnSystem Instance = null;
	/* Where the spawner placed */
	private Transform[] ListOfSpawner;
	public List<Transform> SpawnerList;

	/// <summary>
	/// The current zombie count.
	/// </summary>
	public int CurrentZombieCount;

	/// <summary>
	/// The maximum zombie count.
	/// </summary>
	public int MaximumZombieCount = 1000;
	public int MaximumZombieCountInScene = 20;

	/// <summary>
	/// The is paused.
	/// </summary>
	public bool isPaused = false;

	/// <summary>
	/// The is stopped.
	/// </summary>
	public bool isStopped = false;

	/// <summary>
	/// The hard level.
	/// </summary>
	public HardLevel hardLevel;

	/// <summary>===================
	/// The system level.
	/// </summary>
	public Transform SystemLevel_1;
	public Transform SystemLevel_2;
	public Transform SystemLevel_3;
	//==============================

	[System.Serializable]
	public class CheckPointGate
	{
		public CSVR_ZECheckPoint CheckPoint;
		public int LevelCap;
	}

	public CheckPointGate[] Gate;

	private void Awake()
	{
		Instance = this;
		hardLevel = ((vp_ZombieSurvival)vp_MPMaster.Instance).hardLevel;
	}


	/// <summary>
	/// Start this instance.
	/// </summary>
	private void Start () 
	{
		GetRealSpawnerList ();
	}

	/// <summary>
	/// Gets the real spawner list.
	/// </summary>
	private void GetRealSpawnerList()
	{
		ListOfSpawner = transform.GetComponentsInChildren<Transform> ();

		for (int i = 0; i < ListOfSpawner.Length; i++) 
		{
			if (ListOfSpawner [i].GetComponent<SpawnerObject> ()) 
			{
				ListOfSpawner [i].GetComponent<SpawnerObject> ().spawnSystem = this.GetComponent<SpawnSystem>();
				SpawnerList.Add (ListOfSpawner[i]);	
			}
		}

		SetupSpawnSystemLevel ();
	}

	/// <summary>
	/// Setups the spawn system level.
	/// </summary>
	private void SetupSpawnSystemLevel()
	{
		foreach (var item in SpawnerList) 
		{
			switch (item.GetComponent<SpawnerObject>().hardLevel) 
			{
				case HardLevel.EASY:item.parent = SystemLevel_1;	break;
				case HardLevel.NORMAL:item.parent = SystemLevel_2;	break;
				case HardLevel.HARD:item.parent = SystemLevel_3;	break;
				default:break;
			}
		}

		switch (hardLevel) 
		{
			case HardLevel.EASY:
				SystemLevel_2.gameObject.SetActive (false);
				SystemLevel_3.gameObject.SetActive (false);
				break;
			case HardLevel.NORMAL:
				SystemLevel_3.gameObject.SetActive (false);
				break;
			case HardLevel.HARD:
				break;
			default:
				break;
		}
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	private void Update () 
	{
		
	}

	/// <summary>
	/// Sends the message end spawn.
	/// </summary>
	/// <param name="tran">Tran.</param>
	private void SendMessageEndSpawn(Transform tran)
	{
		SendMessageEndSpawnItem (tran);
	}

	/// <summary>
	/// Sends the message end spawn item.
	/// </summary>
	/// <param name="tran">Tran.</param>
	private void SendMessageEndSpawnItem(Transform tran)
	{
		foreach (var item in tran.GetComponentsInChildren<Transform>()) 
		{
			if (!item.gameObject.GetComponent<SpawnerObject> ()) 
			{
				continue;
			}
			item.gameObject.GetComponent<SpawnerObject>().CancelSpawner ();
		}
	}

}
