﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerObject : MonoBehaviour {

	public static List<TriggerObject> AllTriggerObjects = new List<TriggerObject> ();
	/// <summary>
	/// The spawner object.
	/// </summary>
	[SerializeField]
	public List<SpawnerObject> spawnerObject = new List<SpawnerObject> ();

	/// <summary>
	/// The is triggered.
	/// </summary>
	private bool isTriggered = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="col">Col.</param>
	private void OnTriggerEnter(Collider col)
	{
		print ("PLAYER DA CHAM VAO TRIGGER!");
		if (!isTriggered) 
		{
			isTriggered = true;
			for (int i = 0; i < spawnerObject.Count; i++) {

				/*Spawner Object can be active or inactive in hdierarchy
				 because maybe it was placed in another group level. */
				if (spawnerObject [i] == null)
					continue;
				if(spawnerObject[i].gameObject.activeInHierarchy && spawnerObject[i].isSpawned == false)
					spawnerObject[i].StartSpawner ();
			}
		}
	}


	#if UNITY_EDITOR

	void OnDrawGizmos()
	{
		if (!AllTriggerObjects.Contains (this))
			AllTriggerObjects.Add (this);
	}

	void OnDrawGizmosSelected() 
	{
		Gizmos.color = Color.cyan;
		foreach (SpawnerObject spawner in spawnerObject)
		{
			try
			{
				Gizmos.DrawLine(transform.position, spawner.transform.position);
			}
			catch
			{
			}

		}
	}

	#endif
}
