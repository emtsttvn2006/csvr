﻿using UnityEngine;
using System.Collections;

public class CSVR_TutorialArrow : MonoBehaviour {

	public bool isEnd = false;

	// Use this for initialization
	void Start () 
	{
		if (isEnd) 
		{
			iTween.PunchPosition (gameObject, iTween.Hash ("y", transform.position.y + 0.5f, "time", 2, "looptype", iTween.LoopType.loop));
		} else 
		{
			Vector3 v3 = transform.right;
			iTween.PunchPosition (gameObject, iTween.Hash ("amount",v3 , "time", 2, "looptype", iTween.LoopType.loop));
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (isEnd) 
		{
			transform.Rotate (0, -1, 0);
		}	
	}

	private void OnTriggerExit(Collider col)
	{
		if (col.tag == "PolicePlayer") 
		{
//			transform.parent.transform.parent.GetComponent<CSVR_ZombieTutorialManager> ().listGameObjectArrows.Remove (gameObject);
			Destroy (gameObject);
		}
			
	}

	private void OnTriggerEnter(Collider col)
	{
		if (col.tag == "PolicePlayer") 
		{
			transform.parent.transform.parent.GetComponent<CSVR_ZombieTutorialManager> ().listGameObjectArrows.Remove (gameObject);
//			Destroy (gameObject);
		}

	}
}
