﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class uGUITools : MonoBehaviour 
{
    
    [MenuItem("Tools/uGui Anchors to Corners %&a")]
	static void AnchorsToCorners(){
		foreach(Transform transform in Selection.transforms){
			RectTransform t = transform as RectTransform;
			RectTransform pt = Selection.activeTransform.parent as RectTransform;

			if(t == null || pt == null) return;

			float newAnchorsMinX = (float)System.Math.Round((t.anchorMin.x + t.offsetMin.x / pt.rect.width),3);
			float newAnchorsMinY = (float)System.Math.Round((t.anchorMin.y + t.offsetMin.y / pt.rect.height),3);

			float newAnchorsMaxX = (float)System.Math.Round((t.anchorMax.x + t.offsetMax.x / pt.rect.width),3);
			float newAnchorsMaxY = (float)System.Math.Round((t.anchorMax.y + t.offsetMax.y / pt.rect.height),3);
			if(newAnchorsMinX < 0)
				newAnchorsMinX = 0;
			if(newAnchorsMinY < 0)
				newAnchorsMinY = 0;
			if(newAnchorsMaxX < 0)
				newAnchorsMaxX = 0;
			if(newAnchorsMaxY < 0)
				newAnchorsMaxY = 0;

			if(newAnchorsMinX > 1)
				newAnchorsMinX = 1;
			if(newAnchorsMinY > 1)
				newAnchorsMinY = 1;
			if(newAnchorsMaxX > 1)
				newAnchorsMaxX = 1;
			if(newAnchorsMaxY > 1)
				newAnchorsMaxY = 1;

			Vector2 newAnchorsMin = new Vector2(newAnchorsMinX, newAnchorsMinY);
			Vector2 newAnchorsMax = new Vector2(newAnchorsMaxX, newAnchorsMaxY );

			t.anchorMin = newAnchorsMin;
			t.anchorMax = newAnchorsMax;
			t.offsetMin = t.offsetMax = new Vector2(0, 0);
		}
	}

    [MenuItem("Tools/uGui Corners to Anchors %&c")]
	static void CornersToAnchors(){
		foreach(Transform transform in Selection.transforms){
			RectTransform t = transform as RectTransform;

			if(t == null) return;

			t.offsetMin = t.offsetMax = new Vector2(0, 0);
		}
	}
}
#endif
